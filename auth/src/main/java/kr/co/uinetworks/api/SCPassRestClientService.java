package kr.co.uinetworks.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.dataalliance.scpass.Log;
import com.dataalliance.scpass.SCPassService;
import com.dataalliance.scpass.bc.KeyWallet;
import com.dataalliance.scpass.mileage.MileageCharge;
import com.dataalliance.scpass.oauth.AccessToken;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("scpassRestClient")
public class SCPassRestClientService {
	@Value("${da.oauth.url}")
    private String oauthURL;
	@Value("${da.scpass.url}")
    private String platformURL;
	
	@Value("${da.blockchain.address}")
    private String scoreAddress;
	@Value("${da.blockchain.nid}")
    private String nid;
	
	private RestTemplate restTemplate;
	private SCPassService scpassService;
	
	public SCPassRestClientService(){
		List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
		interceptors.add(new RestRequestInterceptor());
		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
		factory.setConnectTimeout(10*1000); //10초
		factory.setReadTimeout(10*1000); //10초
		this.restTemplate = new RestTemplate(new BufferingClientHttpRequestFactory(factory));
		this.restTemplate.setInterceptors(interceptors);
		
		this.scpassService = new SCPassService(platformURL, oauthURL, true);
	}
	
	/**
	 * 사용자 정보 조회
	 * @param phone
	 * @return
	 * @throws RestException
	 */
	public String getUserInfo(AccessToken token, String symmetricKey , String cleanTownDID, String phone) throws RestException {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.add("Authorization", token.getToken_type()+" "+token.getAccess_token());
			headers.add("x-gatekeeper-did", cleanTownDID);
			
			Map<String, Object> params = new HashMap<String, Object>();

			Log.debug("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[phone]]]]]]]]]]]]]]]]]]]]]]]" + phone);

			params.put("phone", phone);
			String encryptData = scpassService.encrypt(symmetricKey, new ObjectMapper().writeValueAsString(params));
			HttpEntity<String> requestEntity = new HttpEntity<String>(encryptData, headers);
			
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(platformURL+"/manageuser/api/user/search");
	
			ResponseEntity<String> result = restTemplate.postForEntity(builder.build().encode().toUri(), requestEntity, String.class);

			if (result.getStatusCode().equals(HttpStatus.OK)) {
				String json = scpassService.decrypt(symmetricKey, result.getBody());
				return json;
			}else {
				throw new RestException(result.getStatusCode()+"", result.getBody());
			}
		} catch (Exception e) {
			throw new RestException("error", e.getMessage());
		}
	}
	/**
	 * 마일리지 적립
	 * @param wallet 깨끗한 마을 지갑
	 * @param token  깨끗한 마을 인증 토큰
	 * @param symmetricKey 대칭키
	 * @param cleanTownDID 깨끗한 마을 DID
	 * @param did 사용자 DID
	 * @param mileageType 마일리지 유형 cleantown 고정
	 * @param chargeMileage 적립금액
	 * @return
	 * @throws RestException
	 */
	public String chargeMileage(KeyWallet wallet, AccessToken token, String symmetricKey , String cleanTownDID, String did, String mileageType, int chargeMileage) throws RestException {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.add("Authorization", token.getToken_type()+" "+token.getAccess_token());
			headers.add("x-gatekeeper-did", cleanTownDID);
			
			// 블록체인에 등록할 데이터 생성
			// parameter 
			//    wallet : 깨끗한 마을 지갑
			//    scoreAddress : 마일리지적립 스마트컨트랙트 주소(개발계 : cx2d56257b25c6931da734270f336aacfd9b4d4a1f, 운영계 : 향후배포)
			//    nid : 블록체인 네트워크 ID ( 개발계 : 3, 운영계 : 1)
			//    did : 사용자 DID
			//    mileageType : 마일리지 유형 (cleantown : 고정값)
			//    chargeMileage : 마일리지 적립 금액 (원)
			// MileageCharge
			// 	  did : 사용자 DID
			//    mileageType : 마일리지 유형 (cleantown : 고정값)
			//    chargeMileage : 마일리지 적립 금액 (원)
			//    time : 마일리지 적립 요청 시간
			//    address : 깨끗한 마을 지갑 주소
			//    signature : 깨끗한 마을 데이터 서명값
			//    txTimestamp : 트랜젝션 타임스탬프
			Log.debug("scoreAddress : "+scoreAddress);
			Log.debug("nid : "+nid);
			Log.debug("did : "+did);
			Log.debug("mileageType : "+mileageType);
			Log.debug("chargeMileage : "+chargeMileage);

			MileageCharge mileage = scpassService.generateMileageCharge(wallet, scoreAddress, nid, did, mileageType, chargeMileage);
			Log.debug("MileageCharge : " +mileage.toString());
			
			String encryptData = scpassService.encrypt(symmetricKey, new ObjectMapper().writeValueAsString(mileage));
			HttpEntity<String> requestEntity = new HttpEntity<String>(encryptData, headers);
			
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(platformURL+"/integratedmileage/api/mileage/charge");
	
			ResponseEntity<String> result = restTemplate.postForEntity(builder.build().encode().toUri(), requestEntity, String.class);

			if (result.getStatusCode().equals(HttpStatus.OK)) {
				String json = scpassService.decrypt(symmetricKey, result.getBody());
				return json;
			}else {
				throw new RestException(result.getStatusCode()+"", result.getBody());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RestException("error", e.getMessage());
		}
	}
	
	/**
	 * 마일리지 잔액 조회
	 * @param token  깨끗한 마을 인증 토큰
	 * @param symmetricKey 대칭키
	 * @param cleanTownDID 깨끗한 마을 DID
	 * @param did 사용자 DID
	 * @return
	 * @throws RestException
	 */
	public String getMileageBalance(KeyWallet wallet, AccessToken token, String symmetricKey , String cleanTownDID, String did) throws RestException {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.add("Authorization", token.getToken_type()+" "+token.getAccess_token());
			headers.add("x-gatekeeper-did", cleanTownDID);
			
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(platformURL+"/integratedmileage/api/mileage/balance")
					.queryParam("did", did);

			ResponseEntity<String> result = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, new HttpEntity<String>(headers), String.class);
			
			if (result.getStatusCode().equals(HttpStatus.OK)) {
				String json = scpassService.decrypt(symmetricKey, result.getBody());
				return json;
			}else {
				throw new RestException(result.getStatusCode()+"", result.getBody());
			}
		} catch (Exception e) {
			throw new RestException("error", e.getMessage());
		}
	}
	
	/**
	 * 마일리지 이력조회
	 * @param token  깨끗한 마을 인증 토큰
	 * @param symmetricKey 대칭키
	 * @param cleanTownDID 깨끗한 마을 DID
	 * @param did 사용자 DID
	 * @param type 유형 payment(결제), charge(충전)
	 * @param page 페이지 번호
	 * @param scale 출력 범위
	 * @return
	 * @throws RestException
	 */
	public String getMileageHistory(KeyWallet wallet, AccessToken token, String symmetricKey , String cleanTownDID, String did, String type, int page, int scale) throws RestException {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.add("Authorization", token.getToken_type()+" "+token.getAccess_token());
			headers.add("x-gatekeeper-did", cleanTownDID);
			
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(platformURL+"/integratedmileage/api/mileage/history")
					.queryParam("did", did)
					.queryParam("type", type)
					.queryParam("page", page)
					.queryParam("scale", scale);

			ResponseEntity<String> result = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, new HttpEntity<String>(headers), String.class);
			
			if (result.getStatusCode().equals(HttpStatus.OK)) {
				String json = scpassService.decrypt(symmetricKey, result.getBody());
				return json;
			}else {
				throw new RestException(result.getStatusCode()+"", result.getBody());
			}
		} catch (Exception e) {
			throw new RestException("error", e.getMessage());
		}
	}
}
