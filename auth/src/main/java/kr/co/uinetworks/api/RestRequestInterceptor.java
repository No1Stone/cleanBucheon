package kr.co.uinetworks.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import com.dataalliance.scpass.Log;


public class RestRequestInterceptor implements ClientHttpRequestInterceptor {


    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        traceRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);
        traceResponse(response);
        return response;
    }

    private void traceRequest(HttpRequest request, byte[] body) throws IOException {
        Log.debug("===========================rest request begin============================================");
        Log.debug("URI         : "+ request.getURI());
        Log.debug("Method      : "+ request.getMethod());
        Log.debug("Headers     : "+ request.getHeaders() );
        Log.debug("Request body: "+ new String(body, "UTF-8"));
        Log.debug("============================rest request end=============================================");
    }

    private void traceResponse(ClientHttpResponse response) throws IOException {
        StringBuilder inputStringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getBody(), "UTF-8"));
        String line = bufferedReader.readLine();
        while (line != null) {
            inputStringBuilder.append(line);
            inputStringBuilder.append('\n');
            line = bufferedReader.readLine();
        }
        Log.debug("============================rest response begin==========================================");
        Log.debug("Status code  : "+ response.getStatusCode());
        Log.debug("Status text  : "+ response.getStatusText());
        Log.debug("Headers      : "+ response.getHeaders());
        Log.debug("Response body: "+ inputStringBuilder.toString());
        Log.debug("============================rest response end============================================");
    }

}
