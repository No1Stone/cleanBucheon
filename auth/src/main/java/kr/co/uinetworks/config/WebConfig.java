package kr.co.uinetworks.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;



@EnableWebMvc
@Configuration
@RequiredArgsConstructor
@ComponentScan(basePackages = {"kr.co.uinetworks"})
public class WebConfig implements WebMvcConfigurer {


//	@Bean
//    public SpringResourceTemplateResolver templateResolver() {
//        SpringResourceTemplateResolver  templateResolver = new SpringResourceTemplateResolver ();
//        templateResolver.setPrefix("classpath:templates/");
//        templateResolver.setCharacterEncoding("UTF-8");
//        templateResolver.setSuffix(".html");
//        templateResolver.setTemplateMode("HTML5");
//        templateResolver.setCacheable(false);
//        return templateResolver;
//    }
//
//
//
//    @Bean
//    @Autowired
//    public ViewResolver viewResolver(MessageSource messageSource) {
//        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
//       // viewResolver.setTemplateEngine(templateEngine(messageSource));
//        viewResolver.setCharacterEncoding("UTF-8");
//      //  viewResolver.setOrder(0);
//        return viewResolver;
//    }
//    @Bean
//	public ViewResolver getViewResolver() {
//		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//		viewResolver.setPrefix("/");
//		viewResolver.setSuffix(".jsp");
//		return viewResolver;
//	}





//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		registry.addResourceHandler("/**").addResourceLocations(new String[] {"classpath:/static/", "classpath:/templates/", "classpath:/META-INF/resources/"});
//	}
//
//	@Override
//    public void addViewControllers( ViewControllerRegistry registry ) {
//        registry.addViewController( "/" ).setViewName( "index" );
//        //registry.setOrder( Ordered.HIGHEST_PRECEDENCE );
//    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns("*")
                .allowedMethods("GET", "POST")
                .maxAge(3000);
        WebMvcConfigurer.super.addCorsMappings(registry);
    }


}
