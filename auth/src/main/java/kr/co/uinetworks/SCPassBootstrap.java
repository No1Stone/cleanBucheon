package kr.co.uinetworks;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import com.dataalliance.scpass.Log;
import com.dataalliance.scpass.SCPass;
import com.dataalliance.scpass.SCPassService;
import com.dataalliance.scpass.auth.AuthData;
import kr.co.uinetworks.api.RestException;
import kr.co.uinetworks.api.SCPassRestClientService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.dataalliance.scpass.bc.KeyWallet;
import com.dataalliance.scpass.bc.crypto.KeystoreException;
import com.dataalliance.scpass.exception.OAuthException;
import com.dataalliance.scpass.exception.SEPException;

@Component
public class SCPassBootstrap {
	@Value("${da.scpass.url}")
    private String platformURL;
	@Value("${da.oauth.url}")
    private String oauthURL;
	@Value("${da.oauth.username}")
	private String username;
	@Value("${da.oauth.password}")
	private String password;
	@Value("${da.wallet.keystore.password}")
    private String keystorePass;
	
	@Autowired private SCPassRestClientService scpassRestClient;

	public void start() {
			Log.info("==========================================================");
			Log.info("BOOTING CLEANTOWN");
			Log.info("==========================================================");
			
			
			try {
				ClassPathResource cpr = new ClassPathResource("cleantown.keystore");
				InputStream inputStream = cpr.getInputStream();
				File temp = File.createTempFile("cleantown", "keystore");
				FileUtils.copyInputStreamToFile(inputStream, temp);
				SCPassService service = new SCPassService(platformURL, oauthURL, true);
				// Wallet load
				KeyWallet wallet = service.loadKeywallet(temp, keystorePass);

				if(wallet != null) {
					// 서버<->플랫폼 간 상호인증
					SCPass pass = service.connect(wallet, username, password);
					// 서버에서 따로 저장해야 한다.
					SCPassAuth.wallet = wallet;
					SCPassAuth.pass = pass;

					Log.debug("========================================================");
					Log.debug("[WALLET ADDRESS] "+ wallet.getAddress().toString());
					Log.debug("[WALLET PUBLIC KEY] "+ wallet.getPublicKey().toHexString(false));
					Log.debug("[CLEANTOWN DID] "+ pass.getClientDID());
					Log.debug("[CITYPASS DID] "+ pass.getPlatformDID());
					Log.debug("[SYMMETRIC KEY] "+ pass.getSymmetricKey());
					Log.debug("[ACCESS TOKEN] "+ pass.getAccessToken().toString());
					Log.debug("========================================================");

					AuthData authData = new AuthData();
					authData.setDid(pass.getClientDID());
					authData.setType("login");
					authData.setPortalCode("CleanTown2022");
					authData.setCallback("");

					service.addAuthData(pass.getSymmetricKey(), pass.getAccessToken(), authData);

					String userDid = scpassRestClient.getUserInfo(pass.getAccessToken(),pass.getSymmetricKey(),pass.getClientDID(),"01092097741");

					Log.debug("========================================================");
					Log.debug("[TEST USER DID] "+ userDid);
					Log.debug("========================================================");

//						String did = "사용자 did";
//						String mileageType = "cleantown";
//						int chargeMileage = 1000;
//
//						try {
//							String res = scpassRestClient.chargeMileage(wallet, pass.getAccessToken(), pass.getSymmetricKey(), pass.getClientDID(), did, mileageType, chargeMileage);
//							Log.debug(res);
//
//							res = scpassRestClient.getMileageBalance(wallet, pass.getAccessToken(), pass.getSymmetricKey(), pass.getClientDID(), did);
//							Log.debug(res);
//
//							res = scpassRestClient.getMileageHistory(wallet, pass.getAccessToken(), pass.getSymmetricKey(), pass.getClientDID(), did, null, 1, 10);
//							Log.debug(res);
//						} catch (RestException e) {
//							e.printStackTrace();
//						}
				}
				
				
				
			} catch (IOException e) { // Wallet Keystore load 오류
				e.printStackTrace();
			} catch (KeystoreException e) { // Wallet Keystore load 오류
				e.printStackTrace();
			} catch (OAuthException e) { // OAUTH2.0 인증 오류
				e.printStackTrace();
			} catch (SEPException e) { // 플랫폼  API 오류
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RestException e) {
				e.printStackTrace();
			}


	}
	
	
	
	public void stop() {
		try{
		}catch(Exception e){
			Log.error(e);
		}finally {
			Log.info("==========================================================");
			Log.info("EXITING CLEANTOWN");
			Log.info("==========================================================");
		}
	}
}
