package kr.co.uinetworks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean(initMethod="start", destroyMethod="stop")
    public SCPassBootstrap bootstrap() {
		SCPassBootstrap bootstrap = new SCPassBootstrap();
        return bootstrap;
    }
}
