package kr.co.uinetworks.auth.biz.rest;

import com.dataalliance.scpass.Log;
import kr.co.uinetworks.SCPassAuth;
import kr.co.uinetworks.api.RestException;
import kr.co.uinetworks.api.SCPassRestClientService;
import kr.co.uinetworks.auth.biz.rest.model.MileageRequestVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/rest")
public class AuthRestController {

    private final Logger logger = LoggerFactory.getLogger(AuthRestController.class);
    @Autowired
    private SCPassRestClientService scpassRestClient;

    @GetMapping(path = "/get/userinfo/{phone}")
    public Object getUserInfo(@PathVariable String phone) {
        try{

            String userinfo = scpassRestClient.getUserInfo(SCPassAuth.pass.getAccessToken(), SCPassAuth.pass.getSymmetricKey(), SCPassAuth.pass.getClientDID(), phone);
            return userinfo;

        }catch (RestException rex){
            return "not found";
        }
    }

    @PostMapping(path = "/put/mileage")
    public Object putMileage(@RequestBody MileageRequestVO mileageRequestVO) {

        try{

            Log.debug("-- SCPassAuth.wallet : " + SCPassAuth.wallet);
            Log.debug("-- SCPassAuth.pass.getAccessToken() : " + SCPassAuth.pass.getAccessToken());
            Log.debug("-- SCPassAuth.pass.getSymmetricKey() : " + SCPassAuth.pass.getSymmetricKey());
            Log.debug("-- SCPassAuth.spass.getClientDID() : " + mileageRequestVO.getDid());
            Log.debug("-- mileageType : " + "cleantown");
            Log.debug("-- mileageRequestVO.getCharge() : " + mileageRequestVO.getCharge());

            String responseStr = scpassRestClient.chargeMileage(SCPassAuth.wallet,SCPassAuth.pass.getAccessToken(), SCPassAuth.pass.getSymmetricKey(), SCPassAuth.pass.getClientDID(),mileageRequestVO.getDid(),"cleantown",mileageRequestVO.getCharge());
            return responseStr;

        }catch (RestException rex){
            return "error";
        }
    }

    @PostMapping(path = "/get/mileage")
    public Object getMileage(@RequestBody MileageRequestVO mileageRequestVO) {

        try{

            String responseStr = scpassRestClient.getMileageBalance(SCPassAuth.wallet,SCPassAuth.pass.getAccessToken(), SCPassAuth.pass.getSymmetricKey(), SCPassAuth.pass.getClientDID(),mileageRequestVO.getDid());
            return responseStr;

        }catch (RestException rex){
            return "error";
        }
    }
}
