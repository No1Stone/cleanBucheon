package com.bucheon.sch.common.type;

public class Definition {
	
	/* 버튼의 타입 */
	public static final String MSG_BTN_TYPE_BL = "bl";
	public static final String MSG_BTN_TYPE_UL = "ul";
	public static final String MSG_BTN_TYPE_TL = "tl";
	public static final String MSG_BTN_TYPE_MS = "ms";
	public static final String MSG_BTN_TYPE_AL = "al";
	
	/* 기본 res_type 코드 */
	public static final String SKILL_TYPE_ST = "ST";// SimpleText
	public static final String SKILL_TYPE_SI = "SI";// Simple Image
	public static final String SKILL_TYPE_BC = "BC";// Basic Card
	public static final String SKILL_TYPE_LC = "LC";// List Card
	public static final String SKILL_TYPE_CC = "CC";// Commerce CardsSS
	
	public static final String SKILL_BL_MSG = "blockId";
	public static final String SKILL_UL_MSG = "webLinkUrl";
	public static final String SKILL_TL_MSG = "phoneNumber";
	public static final String SKILL_MS_MSG = "messageText";
	public static final String SKILL_AL_MSG = "osLink";
	
}
