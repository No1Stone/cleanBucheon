package com.bucheon.sch.common.config;


import com.bucheon.sch.biz.operation.admin.v1.service.AdminMemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig
        extends WebSecurityConfigurerAdapter
{

    private final AdminMemberService adminMemberService;
    private final AuthSuccessHandler authSuccessHandler;
    private final AuthFailureHandler authFailureHandler;


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    //시큐리티가 로그인
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(adminMemberService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/**").permitAll()
//                .antMatchers("/","/login/**","/js/**","/img/**",
//                        "/css/**","/email_templates/**",
//                        "/fontawesome-free-6.1.1-web/**",
//                        "/fonts/**","/locales/**","/pdf/**").permitAll()
//                .anyRequest()
//                .authenticated()
                .and()
                .formLogin()
                .loginPage("/login/info")
                .defaultSuccessUrl("/home/info")
                .failureUrl("/login/info")
                .usernameParameter("userId")
                .passwordParameter("passwd")
                .loginProcessingUrl("/login/login_proc")
                .successHandler(authSuccessHandler)
                .failureHandler(authFailureHandler)
                .and()
                .logout()
                //.logoutRequestMatcher(new AndRequestMatcher("/logout"))
                .logoutSuccessUrl("/login/info")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID", "remember-me")
                .permitAll()
                .and()
                .sessionManagement()
                .maximumSessions(1)
                .maxSessionsPreventsLogin(false)
                .expiredUrl("/login/info/error=true&exception=Have been attempted to login from a new place. or session expired")
                .and()
                .and().rememberMe()
                .alwaysRemember(false)
                .tokenValiditySeconds(43200)
                .rememberMeParameter("remember-me")
                /*
                .successHandler(
                        new AuthenticationSuccessHandler() {
                            @Override
                            public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
                                    throws IOException, ServletException {
                                System.out.println("authentication : " + authentication.getName());
                                response.sendRedirect("/environmental/report/info"); // 인증이 성공한 후에는 root로 이동
                            }
                        }
                )
                .failureHandler(
                        new AuthenticationFailureHandler() {
                            @Override
                            public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
                                System.out.println("exception : " + exception.getMessage());
                                response.sendRedirect("/login");
                            }
                        }
                )
                 */

        ;
/*
        http.httpBasic().disable()
                .csrf().disable();

        http.csrf().disable()
                .authorizeRequests()
//                .antMatchers("/admin/**").permitAll()

                .anyRequest().authenticated()   // 나머지 API 는 전부 인증 필요
                .and()
                .loginPage("/login/info")
                .permitAll()

                .and()
                .logout()
                .permitAll()

                .and()
                .exceptionHandling().accessDeniedPage("/403")

                .and()
                .sessionManagement()
                .invalidSessionUrl("/login/info")
                .maximumSessions(1)
                .maxSessionsPreventsLogin(true)
                .expiredUrl("/login/info")
*/
        ;
    }


}
