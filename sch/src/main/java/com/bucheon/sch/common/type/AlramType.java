package com.bucheon.sch.common.type;

public enum AlramType {

    SC_001("SC_001", "접수"),
    SC_002("SC_002","담당자 배정"),
    SC_003("SC_003", "처리완료"),
    SC_004("SC_004", "처리불가")
    ;

    private String code;
    private String description;

    AlramType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    public String getName() {
        return name();
    }

}
