package com.bucheon.sch.common.type;

public enum PaymentType {

    W("WAITING", "지급대기"),
    C("COMPLETE","지급완료"),
    R("REJECT", "지급불가")
    ;

    private String code;
    private String description;

    PaymentType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    public String getName() {
        return name();
    }

}
