package com.bucheon.sch.common.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    private final Logger logger = LoggerFactory.getLogger(MvcConfig.class);

    @Value("${domain.file}")
    private String filePath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        Path path = Paths.get(filePath).toAbsolutePath().normalize();

        String patsString = path.toString();

        logger.info("path = {}",path);
        logger.info("pathString = {}",patsString);
        registry
                .addResourceHandler("/img/trash/**")
                .addResourceLocations("file:///"+patsString+"/trash/")
                .addResourceLocations("file:/data/cleantown/service/town/src/main/resources/static/img/trash/");
        registry
                .addResourceHandler("/img/menu/**")
                .addResourceLocations("file:///"+patsString+"/menu/")
                .addResourceLocations("file:/data/cleantown/service/town/src/main/resources/static/img/menu/");
        registry
                .addResourceHandler("/img/metaBoard/**")
                .addResourceLocations("file:///"+patsString+"/metaBoard/")
                .addResourceLocations("file:/data/cleantown/service/town/src/main/resources/static/img/metaBoard/");
        registry
                .addResourceHandler("/img/chatbot/**")
                .addResourceLocations("file:///"+patsString+"/chatbot/")
                .addResourceLocations("file:/data/cleantown/service/town/src/main/resources/static/img/chatbot/");
        registry
                .addResourceHandler("/img/metaNotice/**")
                .addResourceLocations("file:///"+patsString+"/metaNotice/")
                .addResourceLocations("file:/data/cleantown/service/town/src/main/resources/static/img/metaNotice/");
        registry
                .addResourceHandler("/img/report/chatbot/**")
                .addResourceLocations("file:///"+"D:/bucheon"+"/")
                .addResourceLocations("file:/data/cleantown/service/chatbot/img/");
        registry
                .addResourceHandler("/img/report/clean/**")
                .addResourceLocations("file:///"+"D:/bucheon"+"/")
                .addResourceLocations("file:/data/cleantown/service/app/img/");
        registry
                .addResourceHandler("/upload/**")
                .addResourceLocations("file:/data/cleantown/service/town/src/main/resources/upload/")
                .addResourceLocations("file:///"+"C:\\project\\bucheun\\smartchallenge\\town\\src\\main\\resources\\upload"+"/")
                .addResourceLocations("file:///"+"C:\\Users\\DELL\\Desktop\\부천시\\upload"+"/");

        registry
                .addResourceHandler("/firebase/**")
                .addResourceLocations("file:/data/cleantown/service/firebase/")
                .addResourceLocations("file:///"+"C:\\project\\bucheun\\smartchallenge\\town\\src\\main\\resources\\firebase"+"/")
                .addResourceLocations("file:///"+"C:\\Users\\DELL\\Desktop\\부천시\\firebase"+"/");

    }

}