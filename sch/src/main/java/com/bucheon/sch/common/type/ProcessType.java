package com.bucheon.sch.common.type;

public enum ProcessType {

    D("DECLARATION", "접수"),
    C("COMPLETE","완료"),
    P("PROGRESS", "처리중"),
    R("REJECT", "처리불가")
    ;

    private String code;
    private String description;

    ProcessType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    public String getName() {
        return name();
    }

}
