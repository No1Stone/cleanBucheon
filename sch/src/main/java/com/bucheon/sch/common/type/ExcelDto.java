package com.bucheon.sch.common.type;

import java.util.List;

public interface ExcelDto {

    List<String> mapToList();
    List<Integer> mapToCoulmn();
}
