package com.bucheon.sch.common.util;

import com.bucheon.sch.biz.environmental.report.v1.band.BandService;
import com.bucheon.sch.biz.metabus.metaPush.v1.service.MetaPushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.logging.Logger;

@Component
public class SchApplicationRunner implements ApplicationRunner {

    Logger logger = Logger.getLogger(String.valueOf(SchApplicationRunner.class));

    @Autowired
    private BandService bandService;

    @Autowired
    private MetaPushService metaPushService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("ApplicationRunner Args: " + Arrays.toString(args.getSourceArgs()));

        schBandBoardGet();
        schReprotCommentProc();
        //reservationPush();
    }

    //밴드 자료 가져오기 스케줄
    @Scheduled(cron = "0 35 7-17 * * ?")
    public void schBandBoardGet(){

        LocalDateTime now = LocalDateTime.now();
        int nowHour = now.getHour();

        try {
            logger.info("--- 밴드 자료 가져오기 시작 ---------------------------------");
            bandService.BandBoardGet();
            logger.info("--- 밴드 자료 가져오기 끝 ---------------------------------");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //밴드 댓글 자료 가져오기 스케줄
    @Scheduled(cron = "0 15 7-17 * * ?")
    public void schReprotCommentProc(){

        LocalDateTime now = LocalDateTime.now();
        int nowHour = now.getHour();

        try {
            logger.info("--- 밴드 댓글 가져오기 시작 ---------------------------------");
            bandService.ReprotCommentProc();
            logger.info("--- 밴드 댓글 가져오기 끝 ---------------------------------");
        } catch (Exception e) {
            e.printStackTrace();
        }



    }


    //
//    @Scheduled(cron = "0 */10 * * * *")
//    public void reservationPush(){
//
//        LocalDateTime now = LocalDateTime.now();
//        int nowHour = now.getHour();
//
//        try {
//            logger.info("--- 푸쉬 자료 가져오기 시작 ---------------------------------");
//            //metaPushService.MetaPusSelectTimeListService();
//            logger.info("--- 푸쉬 자료 가져오기 끝 ---------------------------------");
//        } catch (Exception e) {
//            logger.info("--- 푸쉬 자료 가져오기 실패 ---------------------------------");
//            e.printStackTrace();
//        }
//
//    }


}
