package com.bucheon.sch.common.type;

public enum ReceptionType {
    A("APPCLEANTOWN","앱깨끗한마을"),
    B("BAND","네이버밴드"),
    C("CHATBOT","챗봇"),
    P("PHONE","전화접수"),
    V("VISIT","방문")
    ;
    private String code;
    private String description;

    ReceptionType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }
    public String getDescription() {
        return this.description;
    }
    public String getName() {
        return name();
    }

}
