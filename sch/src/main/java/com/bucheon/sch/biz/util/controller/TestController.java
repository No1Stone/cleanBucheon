package com.bucheon.sch.biz.util.controller;

import com.bucheon.sch.biz.chatbot.alarmtalk.v1.service.AlaramtalkService;
import com.bucheon.sch.biz.environmental.report.v1.band.BandService;
import com.bucheon.sch.biz.util.service.UtilService;
import com.bucheon.sch.db.repository.environmental.ReportLogRepository;
import com.bucheon.sch.db.repository.environmental.ReportRepository;
import com.bucheon.sch.db.repository.environmental.band.TB_BandBoardListRepository;
import com.bucheon.sch.db.repository.etc.DongBandDamdangRepository;
import com.bucheon.sch.db.repository.etc.DongDamdangRepository;
import com.bucheon.sch.db.repository.operation.AdminRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Controller
@RequestMapping(path = "/testC")
@RequiredArgsConstructor
public class TestController {

    private final Logger logger = LoggerFactory.getLogger(TestController.class);
    private final UtilService utilService;
    private final TB_BandBoardListRepository tb_BandBoardListRepository;
    private final DongBandDamdangRepository dongBandDamdangRepository;
    private final ReportRepository reportRepository;
    private final ReportLogRepository reportLogRepository;
    private final DongDamdangRepository dongDamdangRepository;
    private final AdminRepository adminRepository;
    private final AlaramtalkService alaramtalkService;
    private final BandService bandService;

    @GetMapping(path = "/")
    public ModelAndView view() {
        return new ModelAndView("test/testfileUpload");
    }

    @PostMapping(path = "/upload")
    public Object testfileUpload(@RequestParam MultipartFile[] uploadfile, Model model) throws IllegalStateException, IOException {
        utilService.FileSaveService(uploadfile, "trash");
        return new ModelAndView("test/testfileUpload");
    }

    @GetMapping(path = "/shorturl")
    @ResponseBody
    public Object shortURLReturn() throws UnsupportedEncodingException {
        return  utilService.CreateShortURLService("http://ws.uinetworks.kr:9506/admin/img/report/chatbot/9eff31b1-a430-46e5-8df6-48dd9898edcb_img.jpg");
    }

    @GetMapping(path = "/fu")
    @ResponseBody
    public void fu() throws UnsupportedEncodingException, InterruptedException {
        ExecutorService es = Executors.newCachedThreadPool();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Hello");
        Thread.sleep(3000);
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Exit");
    }

    @GetMapping(path = "/chatbot")
    @ResponseBody
    public void chatbot() {
        alaramtalkService.AutoReportStatusPChage();
    }

    @GetMapping(path = "/bandComentCheck")
    @ResponseBody
    public void bandComentCheck() {

        bandService.ReprotCommentProc();
    }

    @GetMapping(path = "/bandListGet")
    @ResponseBody
    public void bandListGet() {
        bandService.BandBoardGet();
    }
}
