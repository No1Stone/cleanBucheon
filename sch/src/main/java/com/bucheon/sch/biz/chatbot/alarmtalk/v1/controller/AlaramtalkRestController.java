package com.bucheon.sch.biz.chatbot.alarmtalk.v1.controller;

import com.bucheon.sch.biz.chatbot.alarmtalk.v1.model.AlaramBoardRes;
import com.bucheon.sch.biz.chatbot.alarmtalk.v1.model.ChatbotData;
import com.bucheon.sch.biz.chatbot.alarmtalk.v1.service.AlaramtalkService;
import com.bucheon.sch.db.entity.chatbot.TB_AlaramBoard;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/chatbot/alarmtalk/rest")
@RequiredArgsConstructor
public class AlaramtalkRestController {

    private final Logger logger = LoggerFactory.getLogger(AlaramtalkRestController.class);
    private final AlaramtalkService alaramtalkService;

    @PostMapping(path = "/save")
    public void TalkGetController(@RequestBody ChatbotData dto) throws Exception {
        logger.info("dto = {}", new Gson().toJson(dto));
        alaramtalkService.SaveService(dto);
    }

    @GetMapping(path = "/info/{seq}")
    public AlaramBoardRes select(@PathVariable(name = "seq") Long seq) {
        return alaramtalkService.TB_AlaramBoardSelectService(seq);
    }

    @GetMapping(path = "/info-phone-d/{phone}/{sdate}/{edate}/{page}")
    public List<AlaramBoardRes> selectPhone(@PathVariable(name = "phone") String phone,
                                                  @PathVariable("sdate") String sdate,
                                                  @PathVariable("edate") String edate,
                                                  @PathVariable("page") int pageno
    ) {
        PageRequest pageRequest = PageRequest.of(pageno, 10);
        return alaramtalkService.TB_AlaramBoardSelectPhoneService(phone, sdate, edate, pageRequest);
    }


    @GetMapping(path = "/test11")
    public TB_AlaramBoard test1111() {

        return alaramtalkService.jsonvaluechk();
    }


}
