package com.bucheon.sch.biz.operation.declarationPoint.v1.controller;

import com.bucheon.sch.biz.operation.declarationPoint.v1.model.DeclarationPointVO;
import com.bucheon.sch.biz.operation.declarationPoint.v1.service.DeclarationPointService;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.sch.biz.operation.menu.v1.service.MenuService;
import com.bucheon.sch.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.sch.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.HashMap;

@Controller
@RequestMapping(path = "/operation/declarationPoint")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DeclarationPointController {

    private final Logger logger = LoggerFactory.getLogger(DeclarationPointController.class);
    private final DeclarationPointService declarationPointService;
    private final PermissionService permissionService;
    private final MenuService menuService;

    @Value("${domain.file}")
    private String path;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/operation/declarationPoint/declarationPointManage");
    }

    //기본틀
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(DeclarationPointVO dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(24L);

        return new ModelAndView("operation/declarationPoint/declarationPointManage")
                .addObject("declarationPointOne", declarationPointService.DeclarationPointSelectOneService())
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(DeclarationPointVO dto) throws IllegalStateException, IOException {
        HashMap<String, Object> map = new HashMap<String, Object>();

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

//        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + dto.getDeclarationSeq());
//        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + dto.getDeclarationPoint());

        map.put("result", declarationPointService.DeclarationPointSaveService(dto));
        return map;
    }

}
