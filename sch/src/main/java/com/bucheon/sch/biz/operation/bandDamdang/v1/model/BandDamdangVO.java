package com.bucheon.sch.biz.operation.bandDamdang.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BandDamdangVO {


    private String dong;
    private String damdang;
    private String damdangPhone;

}
