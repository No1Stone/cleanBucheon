package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CarouselItemsThumbnail {

	private String imageUrl;

	private String fixedRatio;
	
	private int width;
	
	private int height;

	
}
