package com.bucheon.sch.biz.chatbot.answer.v1.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class ImageTitle {

	private String title;
	private String description;
	private String imageUrl;

}
