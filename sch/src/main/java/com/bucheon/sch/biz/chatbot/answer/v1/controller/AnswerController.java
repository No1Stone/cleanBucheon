package com.bucheon.sch.biz.chatbot.answer.v1.controller;

import com.bucheon.sch.biz.chatbot.answer.v1.model.*;
import com.bucheon.sch.biz.chatbot.answer.v1.service.AnswerService;
import com.bucheon.sch.biz.chatbot.uploadImage.v1.model.UploadImageVO;
import com.bucheon.sch.biz.chatbot.uploadImage.v1.service.UploadImageService;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.sch.biz.operation.menu.v1.service.MenuService;
import com.bucheon.sch.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.sch.db.entity.operation.Admin;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/chatbot/answer")
@RequiredArgsConstructor
public class AnswerController {

    private final Logger logger = LoggerFactory.getLogger(AnswerController.class);
    private final AnswerService answerService;
    private final UploadImageService uploadImageService;
    private final PermissionService permissionService;
    private final MenuService menuService;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/chatbot/answer/answerManage");
    }

    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info( HttpServletRequest request){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(8L);

        Map<Object, String> answerPaging = new HashMap<>();

        return new ModelAndView("chatbot/answer/answerManage")
                .addObject("answerPaging", answerPaging)
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(AnswerReq dto){
        return new ModelAndView("chatbot/answer/answerListAjax")
                .addObject("answerList",answerService.SkllSelectList(dto))
                .addObject("answerPaging",answerService.SkllSelectListPagingService(dto))
                ;
    }

    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm(){

        SkillManageRVO skillManageRVO = new SkillManageRVO();

        List<SimpleTextVO> stListVo = new ArrayList<SimpleTextVO>();
        SimpleTextVO STVO1 = new SimpleTextVO();
        SimpleTextVO STVO2 = new SimpleTextVO();
        SimpleTextVO STVO3 = new SimpleTextVO();
        stListVo.add(STVO1);
        stListVo.add(STVO2);
        stListVo.add(STVO3);

        List<SimpleImageVO> siListVo = new ArrayList<SimpleImageVO>();
        SimpleImageVO SIVO1 = new SimpleImageVO();
        SimpleImageVO SIVO2 = new SimpleImageVO();
        SimpleImageVO SIVO3 = new SimpleImageVO();
        siListVo.add(SIVO1);
        siListVo.add(SIVO3);
        siListVo.add(SIVO1);

        List<List<BasicCardVO>> bcListVo = new ArrayList<List<BasicCardVO>>();
        List<BasicCardVO> bc1 = new ArrayList<BasicCardVO>();
        List<BasicCardVO> bc2 = new ArrayList<BasicCardVO>();
        List<BasicCardVO> bc3 = new ArrayList<BasicCardVO>();
        bcListVo.add(bc1);
        bcListVo.add(bc2);
        bcListVo.add(bc3);

        List<List<CommerceCardVO>> ccListVo = new ArrayList<List<CommerceCardVO>>();
        List<CommerceCardVO> CCVO1 = new ArrayList<CommerceCardVO>();
        List<CommerceCardVO> CCVO2 = new ArrayList<CommerceCardVO>();
        List<CommerceCardVO> CCVO3 = new ArrayList<CommerceCardVO>();
        ccListVo.add(CCVO1);
        ccListVo.add(CCVO2);
        ccListVo.add(CCVO3);


        List<ListCardVO> lcListVo = new ArrayList<ListCardVO>();
        ListCardVO LCVO1 = new ListCardVO();
        ListCardVO LCVO2 = new ListCardVO();
        ListCardVO LCVO3 = new ListCardVO();
        lcListVo.add(LCVO1);
        lcListVo.add(LCVO2);
        lcListVo.add(LCVO3);


        List<QuickRepliesVO> qrListVo = new ArrayList<QuickRepliesVO>();

        return new ModelAndView("chatbot/answer/answerManagePopupAjax")
                .addObject("CRUDFLAG", "C")
                .addObject("skillManageRVO", skillManageRVO)
                .addObject("simpleTextVO", stListVo)
                .addObject("simpleImageVO", siListVo)
                .addObject("basicCardVO", bcListVo)
                .addObject("commerceCardVO", ccListVo)
                .addObject("listCardVO", lcListVo)
                .addObject("quickRepliesVO", qrListVo)
                ;
    }

    @SneakyThrows
    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView modifyForm(@ModelAttribute("pVO") SkillManagePVO pVO) throws IOException {
//        SkillManageRVO skillManageRVO = new SkillManageRVO();
//        SimpleTextVO[] simpleTextVO = new SimpleTextVO[3];
//        SimpleImageVO[] simpleImageVO = new SimpleImageVO[3];
//        BasicCardVO[] basicCardVO = new BasicCardVO[3];
//        CommerceCardVO[] commerceCardVO = new CommerceCardVO[3];
//        ListCardVO[] listCardVO = new ListCardVO[3];
//        QuickRepliesVO quickRepliesVO = new QuickRepliesVO();

//        System.out.println(">>>>>>>>>>>>>>>>>>>>>> pVO.getSkillCode() : " + pVO.getSkillCode());

        SkillManageRVO RVO = (SkillManageRVO) answerService.SkllSelectOneService(pVO.getSkillCode());

        List<SimpleTextVO> stListVo = new ArrayList<SimpleTextVO>();
        List<SimpleImageVO> siListVo = new ArrayList<SimpleImageVO>();
        List<List<BasicCardVO>> bcListVo = new ArrayList<List<BasicCardVO>>();
        List<List<CommerceCardVO>> ccListVo = new ArrayList<List<CommerceCardVO>>();
        List<ListCardVO> lcListVo = new ArrayList<ListCardVO>();
        List<QuickRepliesVO> qrListVo = new ArrayList<QuickRepliesVO>();

        String[] resType = { RVO.getResType1(), RVO.getResType2(), RVO.getResType3() };

        for (int i = 0; i < resType.length; i++) {
            if (!(resType[i] == null || resType[i].equals(""))) {
                RVO.setResSeq(i + 1);

                System.out.println(">>>>>>>>>>>>>>>>>>>>>>  RVO.getResSeq() : " +  RVO.getResSeq());

                SimpleTextVO STVO = new SimpleTextVO();
                SimpleImageVO SIVO = new SimpleImageVO();
                List<BasicCardVO> BCVO = new ArrayList<BasicCardVO>();
                List<CommerceCardVO> CCVO = new ArrayList<CommerceCardVO>();
                ListCardVO LCVO = new ListCardVO();
                switch (resType[i]) {
                    case "ST":
                        STVO = (SimpleTextVO) answerService.selectSkillSTInfo(RVO.getSkillCode(), RVO.getResSeq());
                        break;
                    case "SI":
                        SIVO = (SimpleImageVO) answerService.selectSkillSIInfo(RVO.getSkillCode(), RVO.getResSeq());
                        break;
                    case "BC":
                        BCVO = (List<BasicCardVO>) answerService.selectSkillBCInfo(RVO.getSkillCode(), RVO.getResSeq());
                        break;
                    case "CC":
                        CCVO = (List<CommerceCardVO>) answerService.selectSkillCCInfo(RVO.getSkillCode(), RVO.getResSeq());
                        break;
                    case "LC":
                        LCVO = (ListCardVO) answerService.selectSkillLCInfo(RVO.getSkillCode(), RVO.getResSeq());
                        break;
                    default:
                        break;
                }
                stListVo.add(STVO);
                siListVo.add(SIVO);
                bcListVo.add(BCVO);
                ccListVo.add(CCVO);
                lcListVo.add(LCVO);
            } else {
                SimpleTextVO STVO = new SimpleTextVO();
                SimpleImageVO SIVO = new SimpleImageVO();
                List<BasicCardVO> BCVO = new ArrayList<BasicCardVO>();
                List<CommerceCardVO> CCVO = new ArrayList<CommerceCardVO>();
                ListCardVO LCVO = new ListCardVO();

                stListVo.add(STVO);
                siListVo.add(SIVO);
                bcListVo.add(BCVO);
                ccListVo.add(CCVO);
                lcListVo.add(LCVO);
            }
        }

        qrListVo = (List<QuickRepliesVO>) answerService.selectQuickReplies(RVO.getSkillCode());

        return new ModelAndView("chatbot/answer/answerManagePopupAjax")
                .addObject("CRUDFLAG", "U")
                .addObject("skillManageRVO", RVO)
                .addObject("simpleTextVO", stListVo)
                .addObject("simpleImageVO", siListVo)
                .addObject("basicCardVO", bcListVo)
                .addObject("commerceCardVO", ccListVo)
                .addObject("listCardVO", lcListVo)
                .addObject("quickRepliesVO", qrListVo)
                ;
    }

    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(@RequestBody String jsonString, HttpServletRequest request,
                             HttpServletResponse response) throws Exception {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();

        ObjectMapper mapper = new ObjectMapper();
        SkillVO skillVo = mapper.readValue(jsonString, SkillVO.class);

        //코드 검사
        int skllCodeCnt = answerService.countBySkllCode(skillVo.getSkillManageRVO().getSkillCode());

        if (skllCodeCnt == 0) {
            answerService.InsertSkill(skillVo);
            map.put("isSuccess", true);
        } else {
            map.put("isSuccess", false);
        }


        return map;
    }

    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(@RequestBody String jsonString, HttpServletRequest request,
                                   HttpServletResponse response) throws Exception {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();

        ObjectMapper mapper = new ObjectMapper();
        SkillVO skillVo = mapper.readValue(jsonString, SkillVO.class);

        //코드 검사
        int skllCodeCnt = answerService.countBySkllCode(skillVo.getSkillManageRVO().getSkillCode());

        if (skllCodeCnt == 1) {
            answerService.UpdateSkill(skillVo);
            map.put("isSuccess", true);
        } else {
            map.put("isSuccess", false);
        }


        return map;
    }

    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(String[] answerSeq){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result",   answerService.DeleteAnswerService(answerSeq));
        return map;
    }

    @RequestMapping(path = "/imageInfo", method = RequestMethod.POST)
    public ModelAndView imageInfo( HttpServletRequest request){

        Map<Object, String> answerImagePaging = new HashMap<>();

        return new ModelAndView("chatbot/answer/answerImageListPopupAjax")
                .addObject("answerImagePaging", answerImagePaging)
                ;
    }

    @RequestMapping(path = "/imageListAjax", method = RequestMethod.POST)
    public ModelAndView imageListAjax(UploadImageVO dto){
        return new ModelAndView("chatbot/answer/UploadImageListAjax")
                .addObject("answerImagerList",uploadImageService.UploadImageSelectListService(dto))
                .addObject("answerImagePaging",uploadImageService.UploadImageSelectListPagingInfoService(dto))
                ;
    }

}
