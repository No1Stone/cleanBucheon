package com.bucheon.sch.biz.util.controller;

import com.bucheon.sch.biz.operation.point.v1.model.TrashTypeRes;
import com.bucheon.sch.biz.util.service.UtilService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/util")
@RequiredArgsConstructor
public class UtileRestController {
    private final UtilService utilService;
    private final Logger logger = LoggerFactory.getLogger(UtileRestController.class);

    @GetMapping(path = "/get-img/{imgPath}")
    public ResponseEntity imgGet(@PathVariable(name = "imgPath") String imgPath) {
        ResponseEntity rettt = null;
        try {
            rettt = utilService.imgGetService(imgPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rettt;
    }

    @GetMapping(path = "/trash")
    public List<TrashTypeRes> trashSelect() {
        return utilService.trashSelectService();
    }

//    @GetMapping(path = "/base-img/{img}")
//    public ResponseEntity<Resource> BaseImgGetServiceController(@PathVariable(name = "img") String img) throws IOException {
//
//        logger.info("img - {}",img);
//        return utilService.BaseImgGetService(img);
//    }
//    @GetMapping(path = "/img/{dir}/{img}")
//    public ResponseEntity<Resource> ImgGetServiceController(
//            @PathVariable(name = "dir") String dir,
//            @PathVariable(name = "img") String img) throws IOException {
//        logger.info("dir - {}",dir);
//        logger.info("img - {}",img);
//        return utilService.ImgSaveAndDownloadService(dir,img);
//    }


}
