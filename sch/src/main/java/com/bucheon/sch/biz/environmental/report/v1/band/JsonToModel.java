package com.bucheon.sch.biz.environmental.report.v1.band;

import com.google.gson.Gson;
import org.json.JSONObject;

import java.util.List;
import java.util.stream.Collectors;

/**
 * RESPONSE DATA 바인딩 모듈입니다.
 * 체이닝 메소드로 사용 가능합니다.
 */
public class JsonToModel {

    private String s;
    private List<Object> sl;

    private void setS(Object s) {
        this.s = String.valueOf(s);
    }

    private void setS(String s) {
        this.s = s;
    }

    public String getS() {
        return s;
    }

    public List<Object> getSl() {
        return sl;
    }

    public static JsonToModel builder(Object s) {
        JsonToModel j = new JsonToModel();
        j.setS(String.valueOf(s));
        return j;
    }

    public JsonToModel ofObject(String objName) {
        this.s = String.valueOf(new JSONObject(this.s).getJSONObject(objName));
        return this;
    }

    public JsonToModel ofArray(String objName) {
       this.sl = new JSONObject(this.s).getJSONArray(objName).toList();
        return this;
    }
    public <T> List<T> ofListModel(String objName, Class<T> T) {
        List<T> lt = this.sl.stream().map(e -> new Gson().fromJson(e.toString(), T)).collect(Collectors.toList());
        return lt;
    }

    public <T> T ofModel(Class<T> T) {
//        return new Gson().fromJson(this.s, T);
    return new Gson().fromJson(new Gson().toJson(this.s), T);
    }


    public <T> List<T> ofArrayModel(String objName, Class<T> T) {
        List<Object> o = new JSONObject(this.s).getJSONArray(objName).toList();
        System.out.println("name = "+new Gson().toJson(o));
        if (o.size() > 0) {
            return new JSONObject(this.s).getJSONArray(objName).toList().stream()
                    .map(e -> new Gson().fromJson(new Gson().toJson(e), T))
                    .collect(Collectors.toList())
                    ;
//                .map(e -> new Gson().fromJson(new Gson().toJson(String.valueOf(e)), T))
//                .map(e -> new Gson().fromJson(String.valueOf(e), T))
//                .map(e -> new ModelMapper().map(String.valueOf(e), T))
        } else {
            return null;
        }


    }

}
