package com.bucheon.sch.biz.metabus.metaUser.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MetaUserPointReq {

    private String userId;
    private Long trashSeq;
    private String processLocation;

}
