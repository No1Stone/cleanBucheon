package com.bucheon.sch.biz.chatbot.alarmtalk.v1.model;

import com.bucheon.sch.common.type.PaymentType;
import com.bucheon.sch.common.type.ProcessType;
import com.bucheon.sch.common.type.ReceptionType;
import com.bucheon.sch.common.type.YnType;
import com.bucheon.sch.common.util.ShortURL;
import com.bucheon.sch.db.entity.chatbot.TB_AlaramBoard;
import com.bucheon.sch.db.entity.chatbot.TB_AlaramBoardPoto;
import com.bucheon.sch.db.entity.environmental.Report;
import com.bucheon.sch.db.entity.environmental.ReportImg;
import com.bucheon.sch.db.entity.operation.User;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class ChatbotData {

    @Value("${naver.developer.client.id}")
    private String naverDevId;
    @Value("${naver.developer.client.secret}")
    private String naverDevSecret;
    @Value("${naver.developer.url}")
    private String naverDevUrl;

    private String chatbotUserId;
    private String phoneNumber;
    private String email;
    private String name;
    private String reportType;
    private String newAddress;
    private String oldAddress;
    private String reportPointCompleteYn;
    private String gridx;
    private String gridy;
    private String contents;
    private String agreePersonInfoCollection;
    private String agreePersonInfoHandling;
    private List<ChatbotPoto> chatbotPotos;

    ChatbotData() {
    }

    public ChatbotData(String chatbotUserId, String phoneNumber, String reportType, String newAddress,
                       String oldAddress, String reportPointCompleteYn, String gridx, String gridy, String contents,
                       String agreePersonInfoCollection, String agreePersonInfoHandling,
                       String email, String name,
                       List<ChatbotPoto> chatbotPotos) {
        this.chatbotUserId = chatbotUserId;
        this.phoneNumber = phoneNumber;
        this.reportType = reportType;
        this.newAddress = newAddress;
        this.oldAddress = oldAddress;
        this.reportPointCompleteYn = reportPointCompleteYn;
        this.gridx = gridx;
        this.gridy = gridy;
        this.contents = contents;
        this.agreePersonInfoCollection = agreePersonInfoCollection;
        this.agreePersonInfoHandling = agreePersonInfoHandling;
        this.email = email;
        this.name = name;
        this.chatbotPotos = chatbotPotos;
    }

    public TB_AlaramBoard ofTB_AlaramBoard(Long seq) {
        TB_AlaramBoard ab = new TB_AlaramBoard();
        if(seq != null) {
            ab.setBoardSeqId(seq);
        }
        ab.setChatbotUserId(this.chatbotUserId);
        ab.setPhoneNumber(this.phoneNumber);
        ab.setReportType(this.reportType);
        ab.setNewAddress(this.newAddress);
        ab.setOldAddress(this.oldAddress);
        ab.setGridx(this.gridx);
        ab.setGridy(this.gridy);
        ab.setContents(this.contents);
        ab.setAgreePersonInfoCollection(this.agreePersonInfoCollection);
        ab.setAgreePersonInfoHandling(this.agreePersonInfoHandling);
        ab.setAlaramBoardDt(LocalDateTime.now());
        ab.setName(this.name);
        ab.setEmail(this.email);
        ab.setBandProcess("S");
        ab.setFeedbackStatus("N");
        ab.setFeedbackDetail("N");
        return ab;
    }

    public List<TB_AlaramBoardPoto> ofTB_AlaramBoardPoto(Long seq) {
        List<TB_AlaramBoardPoto> abps = new ArrayList<>();

        this.chatbotPotos.stream().forEach(e -> {
            try {
                abps.add(TB_AlaramBoardPoto.builder()
                        .boardSeqId(seq)
                        .classUrl(e.getClassUrl())
                        .fileName(e.getFileName())
                        .fileSize(e.getFileSize())
                        .fileType(e.getFileType())
                        .build());
            } catch (Exception ex) {
                abps.add(TB_AlaramBoardPoto.builder()
                        .boardSeqId(seq)
                        .classUrl(e.getClassUrl())
                        .fileName(e.getFileName())
                        .fileSize(e.getFileSize())
                        .fileType(e.getFileType())
                        .build());
            }
        });
        return abps;
    }

    public Report ofReport(String areaName) {
        Report r = new Report();
        r.setReportContent(this.contents);
        r.setReceptionNum(String.valueOf(Timestamp.valueOf(LocalDateTime.now()).getTime()));
        r.setReportType(Long.parseLong(this.reportType));
        r.setReportReception(ReceptionType.C.getName());
        r.setReportLocationX(this.gridx);
        r.setReportLocationY(this.gridy);
        r.setReportAddrNew(this.newAddress);
        r.setReportAddrOld(this.oldAddress);
        r.setReportPointCompleteYn(PaymentType.W.getName());
        r.setReportStatus(ProcessType.D.getName());
        r.setReportUserName(this.name);
        r.setReportUserPhone(this.phoneNumber);
        r.setReportUserEmail(this.email);
        r.setDelYn(YnType.N.getName());
        r.setRegUserId(this.chatbotUserId);
        r.setRegDt(LocalDateTime.now());
        r.setModDt(LocalDateTime.now());
//        String bb = this.oldAddress;
//        bb = bb.replaceAll(" ", "");
//        bb = bb.replaceAll("경기도", "");
//        bb = bb.replaceAll("경기", "");
//        bb = bb.replaceAll("부천시", "");
//        bb = bb.split("동")[0]+"동";
        r.setAreaName(areaName);
        return r;
    }

    public User ofUser() {
        User u = new User();
        u.setDid("");
        u.setChatbotUserId(this.chatbotUserId);
        return u;
    }

    public List<ReportImg> ofofReportImg(Long reportSeq) {
        List<ReportImg> ri = new ArrayList<>();
        this.chatbotPotos.stream().forEach(e -> ri.add(ReportImg.builder().reportSeq(reportSeq).reportImg(e.getClassUrl()).build()));
        return ri;
    }

    public String createShortURL(String PageURL) throws UnsupportedEncodingException {
        String originalURL = PageURL;

        originalURL = URLEncoder.encode(originalURL,"UTF-8");

        String apiURL = naverDevUrl + "/v1/util/shorturl?url=" + originalURL;
        String clientId = naverDevId; //애플리케이션 클라이언트 아이디값
        String clientSecret = naverDevSecret; //애플리케이션 클라이언트 시크릿값

        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("X-Naver-Client-Id", clientId);
        requestHeaders.put("X-Naver-Client-Secret", clientSecret);
        String responseBody = ShortURL.get(apiURL,requestHeaders);

        Gson gson = new Gson();
        String jsonObject = new Gson().toJson(responseBody);
        JsonElement jsonElement = gson.fromJson(responseBody, JsonElement.class);
        jsonElement = jsonElement.getAsJsonObject().get("result");

        String result = String.valueOf(jsonElement.getAsJsonObject().get("url"));
        result = result.substring(1);
        result = result.substring(0,result.length()-1);

        return result;
    }
}
