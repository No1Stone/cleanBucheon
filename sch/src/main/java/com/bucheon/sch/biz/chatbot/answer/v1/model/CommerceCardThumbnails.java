package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.Data;

import java.util.Map;

@Data
public class CommerceCardThumbnails {

	private String imageUrl;
	
	private Map<String, Object> link;

}
