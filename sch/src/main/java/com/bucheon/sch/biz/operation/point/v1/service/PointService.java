package com.bucheon.sch.biz.operation.point.v1.service;

import com.bucheon.sch.biz.operation.point.v1.model.TrashTypeReq;
import com.bucheon.sch.biz.operation.point.v1.model.TrashTypeUpdate;
import com.bucheon.sch.biz.util.service.UtilService;
import com.bucheon.sch.common.type.YnType;
import com.bucheon.sch.common.util.Paging;
import com.bucheon.sch.db.entity.operation.Admin;
import com.bucheon.sch.db.entity.operation.TrashType;
import com.bucheon.sch.db.repository.operation.TrashTypeRepository;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PointService {

    private final Logger logger = LoggerFactory.getLogger(PointService.class);
    private final HttpServletRequest httpServletRequest;
    private final TrashTypeRepository trashTypeRepository;
    private final ModelMapper modelmapper;
    private final UtilService utilService;

    public Object TrashSelectUseAllListService() {
        return trashTypeRepository.findAllByUseYnAndDelYn("Y","N");
    }

    public Object TrashSelectListService(TrashTypeReq dto) {
        return trashTypeRepository.TrashTypeSelectList(dto);
    }

    public Object TrashSelectListPagingService(TrashTypeReq dto) {
        QueryResults<TrashType> result = trashTypeRepository.TrashTypeSelectListPaging(dto);

        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public TrashType TrashSelectOneService(Long seq) {
        return trashTypeRepository.findById(seq).orElseThrow(() -> new IllegalArgumentException());
    }

    @Transactional
    public Object TrashSaveService(TrashTypeUpdate dto) throws IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;

        int count = 0;
        int up = 0;

        if (dto.getTrashSeq() == null) {
            /*
            MultipartFile[] mf = new MultipartFile[1];
            mf[0] = dto.getIconUrl();
            List<String > fl = utilService.FileSaveService(mf,"trash");
            if(fl.size() == 1){
                TrashType tt = trashTypeRepository.save(dto.ofTrashType(fl.get(0)));
                if (tt.getTrashSeq() != null) {
                    count = 1;
                }
            }
            */

            dto.setRegSeq(admin.getAdminSeq());
            dto.setModSeq(admin.getAdminSeq());
            dto.setRegDt(LocalDateTime.now());
            dto.setModDt(LocalDateTime.now());

            TrashType tt = trashTypeRepository.save(dto.ofTrashType(dto.getIconUrl()));


        } else {

            dto.setModSeq(admin.getAdminSeq());

            if (dto.getTrashName() != null) {
                up = trashTypeRepository.UpdateTrashTypeName(dto.getTrashName(), dto.getTrashSeq());
                count += up;
            }
            if (dto.getUseYn() != null) {
                up = trashTypeRepository.UpdateTrashTypeUseYn(dto.getUseYn(), dto.getTrashSeq());
                count += up;
            }
            if (dto.getDelYn() != null) {
                for (Long e : dto.getDelYn()) {
                    up = trashTypeRepository.UpdateTrashTypeDelYn(YnType.N.getName(), e);
                    count += up;
                }
            }
            if (dto.getIconUrl() != null) {
                /*
                MultipartFile[] mf = new MultipartFile[1];
                mf[0] = dto.getIconUrl();
                List<String > fl = utilService.FileSaveService(mf,"trash");
                if(fl.size() == 1){
                    up = trashTypeRepository.UpdateTrashTypeIconURL(fl.get(0), dto.getTrashSeq());
                    count += up;
                }
                */
                up = trashTypeRepository.UpdateTrashTypeIconURL(dto.getIconUrl(), dto.getTrashSeq());
                count += up;
            }
            if (dto.getDeclarationMileage() != 0) {
                up = trashTypeRepository.UpdateTrashTypeDeclarationMileage(dto.getDeclarationMileage(), dto.getTrashSeq());
                count += up;
            }
            if (dto.getProcessMileage() != 0) {
                up = trashTypeRepository.UpdateTrashTypeProcessMileage(dto.getProcessMileage(), dto.getTrashSeq());
                count += up;
            }

            if (count > 0) {
                trashTypeRepository.UpdateMod(dto.getModSeq(), dto.getTrashSeq());
            }
        }
        if (count > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    public int UpdatePointDelService(Long[] reportSeql) {
        List<Integer> result = Arrays.stream(reportSeql).map(e -> trashTypeRepository
                .UpdateTrashTypeDelYn(YnType.Y.getName(), e)).collect(Collectors.toList());
        return result.size();
    }

    public List<String> UploadService(MultipartFile[] uploadfile) throws IOException {
        List<String> uploadFilename = utilService.FileSaveService(uploadfile, "trash");
        return uploadFilename;
    }

}
