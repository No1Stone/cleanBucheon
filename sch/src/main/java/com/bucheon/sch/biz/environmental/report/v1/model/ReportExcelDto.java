package com.bucheon.sch.biz.environmental.report.v1.model;

import com.bucheon.sch.common.type.ExcelColumnName;
import com.bucheon.sch.common.type.ExcelDto;
import com.bucheon.sch.common.type.ExcelFileName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@ExcelFileName
public class ReportExcelDto implements ExcelDto {
    @ExcelColumnName(headerName = "접수처")
    @JsonProperty("접수처")
    private String reportReception;

    @ExcelColumnName(headerName = "쓰레기 유형")
    @JsonProperty("쓰레기 유형")
    private String trashName;

    @ExcelColumnName(headerName = "내용")
    @JsonProperty("내용")
    private String reportContent;

    @ExcelColumnName(headerName = "신주소")
    @JsonProperty("신주소")
    private String reportAddrNew;

    @ExcelColumnName(headerName = "구주소")
    @JsonProperty("구주소")
    private String reportAddrOld;

    @ExcelColumnName(headerName = "진행상태")
    @JsonProperty("진행상태")
    private String reportStatus;

    @ExcelColumnName(headerName = "처리일자")
    @JsonProperty("처리일자")
    private LocalDateTime comDt;

    @ExcelColumnName(headerName = "접수일자")
    @JsonProperty("접수일자")
    private LocalDateTime regDt;

    @ExcelColumnName(headerName = "접수자")
    @JsonProperty("접수자")
    private String reportUserName;

    @ExcelColumnName(headerName = "접수자 연락처")
    @JsonProperty("접수자 연락처")
    private String reportUserPhone;

    @ExcelColumnName(headerName = "접수자 Email")
    @JsonProperty("접수자 Email")
    private String reportUserEmail;

    @ExcelColumnName(headerName = "행정동")
    @JsonProperty("행정동")
    private String areaName;

    @ExcelColumnName(headerName = "지급포인트")
    @JsonProperty("지급포인트")
    private String reportPoint;

    @ExcelColumnName(headerName = "지급거절이유")
    @JsonProperty("지급거절이유")
    private String reportPointRejectReason;

    @Override
    public List<String> mapToList() {

        LocalDateTime regDt1 = regDt;
        LocalDateTime comDt1 = comDt;
        String regDtToString = "";
        String comDtToString = "";

        if(regDt1 != null) {
            regDtToString = regDt1.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }
        if(comDt1 != null) {
            comDtToString = comDt1.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }

        return Arrays.asList(reportReception, trashName, reportContent, reportAddrNew, reportAddrOld,
                reportStatus, regDtToString, comDtToString,
                reportUserName, reportUserPhone, reportUserEmail,
                areaName, reportPoint, reportPointRejectReason);
    }

    @Override
    public List<Integer> mapToCoulmn() {

        return Arrays.asList(4000, 4000, 12000, 8000, 8000,
                4000, 4000, 4000,
                4000, 4000, 6000,
                4000, 4000, 6000);
    }

}
