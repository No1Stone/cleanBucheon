package com.bucheon.sch.biz.metabus.metaBoard.v1.controller;

import com.bucheon.sch.biz.metabus.metaBoard.v1.model.*;
import com.bucheon.sch.biz.metabus.metaBoard.v1.service.MetaBoardService;
import com.bucheon.sch.biz.metabus.metaBoard.v1.service.MetaNoticePopupService;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.sch.biz.operation.menu.v1.service.MenuService;
import com.bucheon.sch.biz.operation.user.v1.service.UserService;
import com.bucheon.sch.biz.util.service.UtilService;
import com.bucheon.sch.common.util.Paging;
import com.bucheon.sch.db.entity.metabus.MetaBoard;
import com.bucheon.sch.db.entity.metabus.MetaBoardAtt;
import com.bucheon.sch.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/metaBus/board")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MetaBoardController {

    private final Logger logger = LoggerFactory.getLogger(MetaBoardController.class);
    private final MetaBoardService metaBoardService;
    private final MetaNoticePopupService metaNoticePopupService;
    private final UserService userService;
    private final UtilService utilService;
    private final MenuService menuService;

    @Value("${domain.file}")
    private String filePath;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        return new ModelAndView("redirect:/metaBus/board/info/A");
    }

    //기본틀 
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(MetaBoardReq dto) throws IllegalStateException, Exception {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == "anonymousUser") {
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        String pageTitle = "";
        MenuRes menu = menuService.MenuSelectOneService(28L);

        return new ModelAndView("metaBus/board/metaBoardManage")
                .addObject("boardPaging", metaBoardService.MetaBoardSelectListPagingInfoService(dto))
                .addObject("adminInfo", admin)
                .addObject("selectMenu", menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(MetaBoardReq dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == "anonymousUser") {
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        String sessionAdministrativeDivision = ((Admin) principal).getAdministrativeDivision();

        List<MetaBoardRes> boardList = null;
        Paging boardPaging = null;
        if (dto.getOrderStatus() == null) {
            dto.setOrderStatus("now");
        }

        if (dto.getOrderStatus().equals("now")) {
            boardList = metaBoardService.MetaBoardSelectNowListService(dto);
            boardPaging = metaBoardService.MetaBoardSelectNowListPagingInfoService(dto);
        } else if (dto.getOrderStatus().equals("tocome")) {
            boardList = metaBoardService.MetaBoardSelectTocomeListService(dto);
            boardPaging = metaBoardService.MetaBoardSelectTocmoeListPagingInfoService(dto);
        } else if (dto.getOrderStatus().equals("past")) {
            boardList = metaBoardService.MetaBoardSelectPastListService(dto);
            boardPaging = metaBoardService.MetaBoardSelectPastListPagingInfoService(dto);
        }

        return new ModelAndView("metaBus/board/metaBoardListAjax")
                .addObject("boardList", boardList)
                .addObject("boardPaging", boardPaging)
                ;
    }

    //최초 입력 페이지 팝업 - 필요없음
    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm(String boardType) {

        MetaBoardRes metaBoard = new MetaBoardRes();
        metaBoard.setBoardType(boardType);

        return new ModelAndView("metaBus/board/metaBoardManagePopupAjax")
                .addObject("metaBoardOne", metaBoard)
                ;
    }

    //상세보기 팝업
    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView viewForm(Long seq) {

        MetaBoardRes metaBoard = metaBoardService.MetaBoardSelectOneService(seq);
        List<MetaBoardAtt> metaBoardAttList = metaBoardService.MetaBoardAttSelectListService(seq);

        return new ModelAndView("metaBus/board/metaBoardManagePopupAjax")
                .addObject("metaBoardOne", metaBoard)
                .addObject("metaBoardAttList", metaBoardAttList)
                ;
    }

    //저장처리
    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(@RequestParam("uploadfile") MultipartFile[] uploadfile, MetaBoardVO dto) throws IllegalStateException, IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == "anonymousUser") {
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        Long cnt = metaBoardService.ConfirmTermService(dto.ofMetaBoardVO());

        if (cnt == 0) {

            MetaBoard metaBoard = metaBoardService.MetaBoardSaveService(dto);

            try {
                MetaBoardAttVO metaBoardAttVO = new MetaBoardAttVO();

                List<Map<String, Object>> fileNameList = utilService.FileSaveService2(uploadfile, "metaBoard");

                int i = 0;
                for (Map<String, Object> fileName : fileNameList) {
                    metaBoardAttVO.setBoardAtt((String) fileName.get("link"));
                    metaBoardAttVO.setBoardAttOri(uploadfile[i].getOriginalFilename());
                    metaBoardAttVO.setBoardAttType(uploadfile[i].getContentType());
                    metaBoardAttVO.setBoardAttSize(uploadfile[i].getSize());
                    metaBoardAttVO.setBoardAttPath("/metaBoard/" + (String) fileName.get("fileName"));
                    metaBoardAttVO.setBoardSeq(metaBoard.getBoardSeq());
                    metaBoardService.MetaBoardAttSaveService(metaBoardAttVO);
                    i++;
                }


            } catch (Exception e) {
                //
            }
            map.put("result", "success");
        } else {
            map.put("result", "overlap");
        }
        return map;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(@RequestParam("uploadfile") MultipartFile[] uploadfile, MetaBoardVO dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == "anonymousUser") {
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        Long cnt = metaBoardService.ConfirmTermService(dto.ofMetaBoardVO());

        if (cnt == 0) {

            //내용 업데이트
            metaBoardService.UpdateMetaBoard(dto);

            MetaBoardRes metaBoard = metaBoardService.MetaBoardSelectOneService(dto.getBoardSeq());

            if (uploadfile != null || !uploadfile.equals("")) {

                try {

                    MetaBoardAttVO metaBoardAttVO = new MetaBoardAttVO();

                    List<Map<String, Object>> fileNameList = utilService.FileSaveService2(uploadfile, "metaBoard");

                    int i = 0;
                    for (Map<String, Object> fileName : fileNameList) {
                        metaBoardAttVO.setBoardAtt((String) fileName.get("link"));
                        metaBoardAttVO.setBoardAttOri(uploadfile[i].getOriginalFilename());
                        metaBoardAttVO.setBoardAttType(uploadfile[i].getContentType());
                        metaBoardAttVO.setBoardAttSize(uploadfile[i].getSize());
                        metaBoardAttVO.setBoardAttPath("/metaBoard/" + (String) fileName.get("fileName"));
                        metaBoardAttVO.setBoardSeq(metaBoard.getBoardSeq());
                        metaBoardService.MetaBoardAttSaveService(metaBoardAttVO);
                        i++;
                    }

                } catch (Exception e) {
                    //
                }
            }

            map.put("result", "success");
        } else {
            map.put("result", "overlap");
        }

        return map;
    }

    //삭제처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(Long[] boardSeq) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == "anonymousUser") {
            return new ModelAndView("redirect:/login/info");
        }

        //첨부파일 포함 삭제
        metaBoardService.UpdateMetaBoardDelService(boardSeq);
        metaBoardService.UpdateMetaBoardAttDelService(boardSeq);

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //상세보기 팝업
    @RequestMapping(path = "/modifyNotice/{popupType}", method = RequestMethod.GET)
    public ModelAndView modifyNotice(@PathVariable String popupType) {

        String pageTitle = "";
        String actText = "";
        MenuRes menu = null;

        MetaNoticePopupVO meta = metaNoticePopupService.MetaNoticePopupSelectOneService(popupType);
        if (popupType.equals("image1")) {
            menu = menuService.MenuSelectOneService(39L);
            pageTitle = "메타버스 불편접수 팝업 관리";
            actText = "불편접수";
        } else if (popupType.equals("image2")) {
            menu = menuService.MenuSelectOneService(40L);
            pageTitle = "메타버스 줍기 팝업 관리";
            actText = "줍기";
        } else if (popupType.equals("image3")) {
            menu = menuService.MenuSelectOneService(41L);
            pageTitle = "메타버스 수거 팝업 관리";
            actText = "수거";
        }


        return new ModelAndView("metaBus/board/metaNoticePopupManage")
                .addObject("meta", meta)
                .addObject("popupType", popupType)
                .addObject("pageTitle", pageTitle)
                .addObject("actText", actText)
                .addObject("selectMenu", menu)
                ;
    }

    //수정처리
    @RequestMapping(path = "/updateNotice", method = RequestMethod.POST)
    @ResponseBody
    public Object updateNotice(@RequestParam("uploadfile") MultipartFile[] uploadfile, MetaNoticePopupVO dto) throws IllegalStateException, IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == "anonymousUser") {
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        try {
            List<String> fileName = utilService.FileSaveService(uploadfile, "metaNotice");
            dto.setPopupImageUrl(fileName.get(0));
        } catch (Exception e) {
            //
        }
        metaNoticePopupService.UpdateMetaNoticePopup(dto);

        MetaNoticePopupVO meta = metaNoticePopupService.MetaNoticePopupSelectOneService(dto.getPopupType());


        map.put("result", "success");
        map.put("img", meta.getPopupImageUrl());
        return map;
    }

    //첨부파일 삭제처리
    @RequestMapping(path = "/deleteAtt", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteAtt(Long boardAttSeq) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == "anonymousUser") {
            return new ModelAndView("redirect:/login/info");
        }
        logger.info(">>> boardAttSeq : {}", boardAttSeq);

        HashMap<String, Object> map = new HashMap<String, Object>();
        try {
            metaBoardService.DeleteAtt(boardAttSeq);
            map.put("result", "success");
        } catch (Exception e) {
            map.put("result", "fail");
        }

        return map;
    }
}
