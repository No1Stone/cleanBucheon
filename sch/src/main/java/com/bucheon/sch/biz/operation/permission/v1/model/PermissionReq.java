package com.bucheon.sch.biz.operation.permission.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PermissionReq {

    private int page;
    private int size;
    private Long permissionSeq;
    private String permissionName;
    private Long regSeq;
    private Long modSeq;
    private String sregDt;
    private String eregDt;
    private String smodDt;
    private String emodDt;
    private String searchSelect;
    private String searchString;

}
