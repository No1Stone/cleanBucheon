package com.bucheon.sch.biz.environmental.report.v1.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestEntity {
    private String access_token = "ZQAAAduXeWoR92Ojl_kWbWMgsE21Q2nSnygA0Rlhkl5eHOc0CKkQOn9SyiF-S-5tvJqDkBVtrC9rbQ2r9T32nBIvUX-JJ3uED311un_ckW_3i25r";
    private String band_key = "AABSSAGcTK9M_zhIJo0HRFd0";
    private String content = "콘텐 츠";
    private boolean do_push = true;

}
