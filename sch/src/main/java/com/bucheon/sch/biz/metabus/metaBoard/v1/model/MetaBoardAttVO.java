package com.bucheon.sch.biz.metabus.metaBoard.v1.model;

import com.bucheon.sch.db.entity.metabus.MetaBoardAtt;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetaBoardAttVO {

    private Long boardSeq;
    private String boardAtt;
    private String boardAttOri;
    private String boardAttPath;
    private String boardAttType;
    private Long boardAttSize;

    public MetaBoardAtt ofMetaBoardAtt(){
        MetaBoardAtt m = new MetaBoardAtt();
        m.setBoardSeq(this.boardSeq);
        m.setBoardAtt(this.boardAtt);
        m.setBoardAttOri(this.boardAttOri);
        m.setBoardAttPath(this.boardAttPath);
        m.setBoardAttType(this.boardAttType);
        m.setBoardAttSize(this.boardAttSize);
        return m;
    }

}
