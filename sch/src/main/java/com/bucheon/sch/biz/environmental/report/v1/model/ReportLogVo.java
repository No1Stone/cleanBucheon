package com.bucheon.sch.biz.environmental.report.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReportLogVo {

    private Long logSeq;
    private Long reportSeq;
    private LocalDateTime logTime;
    private String receptionNum;
    private String reportStatus;
    private String reportDamdang;
    private String msg;
    private String rejcetReason;


}
