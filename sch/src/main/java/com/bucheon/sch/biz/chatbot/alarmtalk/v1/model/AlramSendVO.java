package com.bucheon.sch.biz.chatbot.alarmtalk.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AlramSendVO {

    private String toPhone;
    private String fromPhone;
    private String alramId;
    private String sysCode;
    private List<String> arrDc;
    private List<String> arrBtn;

}
