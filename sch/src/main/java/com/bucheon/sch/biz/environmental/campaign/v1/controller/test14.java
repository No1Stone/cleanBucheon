package com.bucheon.sch.biz.environmental.campaign.v1.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class test14 {

    private String a;
    private String b;
    private String c;

}
