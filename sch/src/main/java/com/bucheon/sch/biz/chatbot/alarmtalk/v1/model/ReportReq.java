package com.bucheon.sch.biz.chatbot.alarmtalk.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReportReq {

    private Long reportSeq;
    private String reportContent;
    private int reportType;
    private String reportReception;
    private String reportLocationX;
    private String reportLocationY;
    private String reportAddrNew;
    private String reportAddrOld;
    private String reportStatus;
    private String reportUserName;
    private String reportUserPhone;
    private String reportUserEmail;
    private String delYn;
    private String regUserId;
    private LocalDateTime regDt;
    private LocalDateTime modDt;

}
