package com.bucheon.sch.biz.metabus.metaUser.v1.model;

import com.bucheon.sch.db.entity.metabus.MetaUser;

import java.time.LocalDateTime;

public class MetaUserVO {

    private String userId;
    private String nickname;
    private String email;
    private String provider;
    private String snsId;
    private int avatarType;
    private int cleanPoint;
    private String deviceToken;
    private String cleantownUid;
    private String delYn;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getSnsId() {
        return snsId;
    }

    public void setSnsId(String snsId) {
        this.snsId = snsId;
    }

    public int getAvatarType() {
        return avatarType;
    }

    public void setAvatarType(int avatarType) {
        this.avatarType = avatarType;
    }

    public int getCleanPoint() {
        return cleanPoint;
    }

    public void setCleanPoint(int cleanPoint) {
        this.cleanPoint = cleanPoint;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getCleantownUid() {
        return cleantownUid;
    }

    public void setCleantownUid(String cleantownUid) {
        this.cleantownUid = cleantownUid;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public MetaUser ofMetaUSer(){
        MetaUser m = new MetaUser();

        if (this.userId!=null) {
            m.setUserId(this.userId);
        }

        m.setNickname(this.nickname);
        m.setEmail(this.email);
        m.setProvider(this.provider);
        m.setSnsId(this.snsId);
        m.setAvatarType(this.avatarType);
        m.setCleanPoint(this.cleanPoint);
        m.setDeviceToken(this.deviceToken);
        m.setCleantownUid(this.cleantownUid);
        m.setDelYn(this.delYn);
        m.setCreatedAt(LocalDateTime.now());
        m.setUpdatedAt(LocalDateTime.now());
        return m;
    }

}
