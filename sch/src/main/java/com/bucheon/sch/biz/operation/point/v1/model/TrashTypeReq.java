package com.bucheon.sch.biz.operation.point.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TrashTypeReq {

    private int page;
    private int size;
    private String trashName;

}
