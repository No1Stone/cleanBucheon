package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class SkillVO {
	
	private String crudFlag;
	
	private String resType;
	
	private String skillCode;
	
	//유효성 체크를 위한 변수 사용하지 않을 데이터
	private Object byteChk; 
	
	//Skill Main
	private SkillManageRVO skillManageRVO;
	
	//Simple Text
	private List<SimpleTextVO> simpleTextVO;
	
	//Simple Image
	private List<SimpleImageVO> simpleImageVO;
	
	//Basic Card
	private List<List<BasicCardVO>> basicCardVO;
	
	//Commerce Card
	private List<List<CommerceCardVO>> commerceCardVO;
	
	//List Card
	private List<ListCardVO> listCardVO;
	
	//Quick Replies
	private List<QuickRepliesVO> quickRepliesVO;
	
	//프로필 인증 여부
	private String proflCrtfcAt;

	
}

