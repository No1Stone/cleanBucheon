package com.bucheon.sch.biz.metabus.metaUser.v1.controller;

import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaUserPointHistoryReq;
import com.bucheon.sch.biz.metabus.metaUser.v1.service.MetaUserPointHistoryService;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.sch.biz.operation.menu.v1.service.MenuService;
import com.bucheon.sch.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping(path = "/metaBus/pointHistory")
@RequiredArgsConstructor
public class MetaUserPointHistoryController {

    private final Logger logger = LoggerFactory.getLogger(MetaUserPointHistoryController.class);
    private final MetaUserPointHistoryService metaUserPointHistoryService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/metaBus/pointHistory/info");
    }

    //기본틀
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(MetaUserPointHistoryReq dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(43L);

        return new ModelAndView("metaBus/pointHistory/pointHistoryManage")
                .addObject("pointHistoryPaging", metaUserPointHistoryService.MetaUserPointHistorySelectListServicePaging(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(MetaUserPointHistoryReq dto){
        return new ModelAndView("metaBus/pointHistory/pointHistoryListAjax")
                .addObject("pointHistoryList", metaUserPointHistoryService.MetaUserPointHistorySelectListService(dto))
                .addObject("pointHistoryPaging", metaUserPointHistoryService.MetaUserPointHistorySelectListServicePaging(dto))
                ;
    }
}