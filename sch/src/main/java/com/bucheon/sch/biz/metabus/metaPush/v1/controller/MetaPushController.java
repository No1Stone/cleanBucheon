package com.bucheon.sch.biz.metabus.metaPush.v1.controller;

import com.bucheon.sch.biz.metabus.metaPush.v1.model.MetaPushReq;
import com.bucheon.sch.biz.metabus.metaPush.v1.model.MetaPushVO;
import com.bucheon.sch.biz.metabus.metaPush.v1.service.MetaPushService;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.sch.biz.operation.menu.v1.service.MenuService;
import com.bucheon.sch.biz.operation.user.v1.service.UserService;
import com.bucheon.sch.biz.util.service.UtilService;
import com.bucheon.sch.db.entity.metabus.MetaPush;
import com.bucheon.sch.db.entity.operation.Admin;
import com.google.firebase.messaging.FirebaseMessagingException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.HashMap;

@Controller
@RequestMapping(path = "/metaBus/push")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MetaPushController {

    private final Logger logger = LoggerFactory.getLogger(MetaPushController.class);
    private final MetaPushService metaPushService;
    private final UserService userService;
    private final UtilService utilService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        return new ModelAndView("redirect:/metaBus/push/info");
    }

    //기본틀 
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(MetaPushReq dto) throws IllegalStateException, Exception {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == "anonymousUser") {
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        String pageTitle = "";
        MenuRes menu = menuService.MenuSelectOneService(45L);

        return new ModelAndView("metaBus/push/metaPushManage")
                .addObject("pushPaging", metaPushService.MetaPushSelectListPagingInfoService(dto))
                .addObject("adminInfo", admin)
                .addObject("selectMenu", menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(MetaPushReq dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == "anonymousUser") {
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();

        return new ModelAndView("metaBus/push/metaPushListAjax")
                .addObject("pushList", metaPushService.MetaPusSelectListService(dto))
                .addObject("pushPaging", metaPushService.MetaPushSelectListPagingInfoService(dto))
                ;
    }

    //저장처리
    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(MetaPushVO dto) throws IllegalStateException, IOException, FirebaseMessagingException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == "anonymousUser") {
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();

        if(dto.getSendType().equals("I")){
            MetaPush metaPush = metaPushService.MetaPushSaveService(dto);
        } else {
            MetaPush metaPush = metaPushService.MetaPushReservationService(dto);
        }

        map.put("result", "success");
        return map;
    }

    //삭제처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(Long seq) {

        HashMap<String, Object> map = new HashMap<String, Object>();

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == "anonymousUser") {
            return new ModelAndView("redirect:/login/info");
        }

        //첨부파일 포함 삭제
        int r = metaPushService.DeleteMetaPushService(seq);

        if (r == 1) {
            map.put("result", "success");
        } else {
            map.put("result", "fail");
        }
        logger.info("----result : {}", map.get("result"));

        return map;
    }
}
