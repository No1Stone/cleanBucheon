package com.bucheon.sch.biz.environmental.report.v1.model;

import com.bucheon.sch.db.entity.operation.PersonalInfoLog;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonalSearchReq {

    private Long logSeq;
    private LocalDateTime logTime;
    private Long adminSeq;
    private String menuName;
    private String menuHanName;
    private Long seq;
    private String did;
    private String reason;

    public PersonalInfoLog ofPersonalInfoLog(){
        PersonalInfoLog p = new PersonalInfoLog();

        if (this.logSeq != null) {
            p.setLogSeq(this.logSeq);
        }
        p.setLogTime(LocalDateTime.now());
        p.setAdminSeq(this.adminSeq);
        p.setMenuName(this.menuName);
        p.setMenuHanName(this.menuHanName);
        p.setSeq(this.seq);
        p.setDid(this.did);
        p.setReason(this.reason);

        return p;
    }

}
