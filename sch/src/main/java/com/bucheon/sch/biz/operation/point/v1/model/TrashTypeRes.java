package com.bucheon.sch.biz.operation.point.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TrashTypeRes {

    private int trashSeq;
    private String trashName;
    private int declarationMileage;
    private int processMileage;
    private String iconUrl;
    private String useYn;


}
