package com.bucheon.sch.biz.environmental.report.v1.band.resModel;


import com.bucheon.sch.common.type.PaymentType;
import com.bucheon.sch.common.type.ProcessType;
import com.bucheon.sch.common.type.ReceptionType;
import com.bucheon.sch.common.type.YnType;
import com.bucheon.sch.db.entity.environmental.Report;
import com.bucheon.sch.db.entity.environmental.ReportImg;
import com.bucheon.sch.db.entity.environmental.band.TB_BandBoardList;
import com.bucheon.sch.db.entity.environmental.band.TB_BandBoardListComment;
import com.bucheon.sch.db.entity.environmental.band.TB_BandBoardListPoto;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class BandBoardList {
    private String content;
    private String post_key;
    private int comment_count;
    private long created_at;
    private int emotion_count;
    private String band_key;
    private String name;
    private String description;
    private String role;
    private String profile_image_url;
    private String user_key;
    private Author author;
    private String bandName;
    private List<BandBoardListPoto> photos;
    private List<BandBoardListComment> latest_comments;

    public BandBoardList() {
    }

    public BandBoardList(String content, String post_key, int comment_count, long created_at, int emotion_count, String band_key, String name, String description, String role, String profile_image_url, String user_key, Author author, List<BandBoardListComment> latest_comments, String bandName) {
        this.content = content;
        this.post_key = post_key;
        this.comment_count = comment_count;
        this.created_at = created_at;
        this.emotion_count = emotion_count;
        this.band_key = band_key;
        this.name = name;
        this.description = description;
        this.role = role;
        this.profile_image_url = profile_image_url;
        this.user_key = user_key;
        this.author = author;
        this.latest_comments = latest_comments;
        this.bandName = bandName;
    }

    public TB_BandBoardList getTB_BandBoardList() {
        TB_BandBoardList tb = new TB_BandBoardList();
        tb.setContent(this.content);
        tb.setPostKey(this.post_key);
        tb.setCommentCount(this.comment_count);
        tb.setCreatedAt(this.created_at);
        tb.setEmotionCount(this.emotion_count);
        tb.setBandKey(this.band_key);
        tb.setName(this.author.getName());
        tb.setDescription(this.author.getDescription());
        tb.setRole(this.author.getRole());
        tb.setProfileImageUrl(this.author.getProfile_image_url());
        tb.setUserKey(this.author.getUser_key());

        int count = 0;
        if (this.latest_comments != null) {
            for (BandBoardListComment a : this.latest_comments) {
                if (a.getBody().contains("#완료")) {
                    count += 1;
                }
            }
        }
        if (count > 0) {
            tb.setProcessStatus(ProcessType.C.getName());
        } else {
            tb.setProcessStatus(ProcessType.D.getName());
        }
        tb.setBandName(bandName);

        return tb;
    }

    public List<TB_BandBoardListComment> getTB_BandBoardListComment() {
        List<TB_BandBoardListComment> tb = new ArrayList<>();
        this.latest_comments.stream().forEach(e -> {

            //System.out.println("eeee : " + new Gson().toJson(e));

            TB_BandBoardListComment cm = new TB_BandBoardListComment();
            cm.setPostKey(this.post_key);
            cm.setCommentKey(e.getComment_key());
            cm.setBody(e.getBody());
            cm.setCreatedAt(e.getCreated_at());
            cm.setName(e.getAuthor().getName());
            cm.setDescription(e.getAuthor().getDescription());
            cm.setProfileImageUrl(e.getAuthor().getProfile_image_url());
            cm.setUserKey(e.getAuthor().getUser_key());
            tb.add(cm);
        });
        return tb;
    }

    public List<TB_BandBoardListPoto> getBandBoardListPoto() {
        List<TB_BandBoardListPoto> result = new ArrayList<>();
        this.photos.stream().forEach(e ->
                result.add(TB_BandBoardListPoto
                        .builder()
                        .photoKey(e.getPhoto_key())
                        .postKey(this.post_key)
                        .url(e.getUrl())
                        .build())
        ); return result;
    }

    public Report ofReport(String areaName) {
        Report report = new Report();
        report.setReceptionNum(String.valueOf(Timestamp.valueOf(LocalDateTime.now()).getTime()));
        report.setReportContent(this.content);
        report.setReportType(20L);
        report.setReportReception(ReceptionType.B.getName());
//        report.setReportLocationX();
//        report.setReportLocationY();
        report.setReportAddrNew(this.bandName);
//        report.setReportAddrOld();
        report.setReportStatus(ProcessType.D.getName());
        report.setReportUserName(this.name);
//        report.setReportUserPhone();
//        report.setReportUserEmail();
        report.setDelYn(YnType.N.getName());
//        report.setRegUserId(0L);
//        report.setComDt();
        report.setRegDt(LocalDateTime.now());
        report.setModDt(LocalDateTime.now());
        report.setBandKey(this.band_key);
        report.setSourcePk(this.post_key);
        report.setReportPointCompleteYn(PaymentType.W.getName());
        //report.setAreaName(this.bandName.substring(5).split("동")[0]+"동");
        report.setAreaName(areaName);
        return report;
    }

    public List<ReportImg> ofReportImg(Long seq) {
        List<ReportImg> reportImg = new ArrayList<>();
        photos.stream().forEach(e -> reportImg.add(ReportImg.builder().reportSeq(seq).reportImg(e.getUrl()).build()));
        return reportImg;
    }

}
