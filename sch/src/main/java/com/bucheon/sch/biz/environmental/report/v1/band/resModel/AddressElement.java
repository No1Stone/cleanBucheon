package com.bucheon.sch.biz.environmental.report.v1.band.resModel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressElement {
    private String[] types;
    private String longName;
    private String shortName;
    private String code;
}
