package com.bucheon.sch.biz.operation.admin.v1.service;

import com.bucheon.sch.db.entity.operation.Admin;
import com.bucheon.sch.db.repository.operation.AdminRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AdminMemberService implements UserDetailsService {

    private final Logger logger = LoggerFactory.getLogger(AdminMemberService.class);
    private final AdminRepository adminRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Admin admin = adminRepository.findByAdminId(username);

        if(admin == null) throw new UsernameNotFoundException("Not Found account");

        return admin;
    }
}
