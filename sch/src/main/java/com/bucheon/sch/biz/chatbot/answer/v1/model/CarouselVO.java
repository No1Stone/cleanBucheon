package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class CarouselVO {
	
	private String type;
	
	private List<Object> items;

}
