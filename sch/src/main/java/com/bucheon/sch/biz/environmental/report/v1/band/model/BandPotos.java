package com.bucheon.sch.biz.environmental.report.v1.band.model;


import com.bucheon.sch.biz.environmental.report.v1.band.JsonToModel;


public class BandPotos {

    private int height;
    private int width;
    private long created_at;
    private String url;
    private BandAuthor author;
    private String photo_album_key;
    private String photo_key;
    private int comment_count;
    private int emotion_count;
    private boolean is_video_thumbnail;

    public BandPotos(int height,
                     int width,
                     long created_at,
                     String url,
                     BandAuthor author,
                     String photo_album_key,
                     String photo_key,
                     int comment_count,
                     int emotion_count,
                     boolean is_video_thumbnail) {
        this.height = height;
        this.width = width;
        this.created_at = created_at;
        this.url = url;
        this.author = author;
        this.photo_album_key = photo_album_key;
        this.photo_key = photo_key;
        this.comment_count = comment_count;
        this.emotion_count = emotion_count;
        this.is_video_thumbnail = is_video_thumbnail;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public long getCreated_at() {
        return created_at;
    }

    public String getUrl() {
        return url;
    }

    public BandAuthor getAuthor() {
        return author;
    }

    public String getPhoto_album_key() {
        return photo_album_key;
    }

    public String getPhoto_key() {
        return photo_key;
    }

    public int getComment_count() {
        return comment_count;
    }

    public int getEmotion_count() {
        return emotion_count;
    }

    public boolean isIs_video_thumbnail() {
        return is_video_thumbnail;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setAuthor(BandAuthor author) {
        this.author = author;
    }
    public void setAuthor(Object author) {
        this.author = JsonToModel.builder(author).ofModel(BandAuthor.class);
    }

    public void setPhoto_album_key(String photo_album_key) {
        this.photo_album_key = photo_album_key;
    }

    public void setPhoto_key(String photo_key) {
        this.photo_key = photo_key;
    }

    public void setComment_count(int comment_count) {
        this.comment_count = comment_count;
    }

    public void setEmotion_count(int emotion_count) {
        this.emotion_count = emotion_count;
    }

    public void setIs_video_thumbnail(boolean is_video_thumbnail) {
        this.is_video_thumbnail = is_video_thumbnail;
    }
}
