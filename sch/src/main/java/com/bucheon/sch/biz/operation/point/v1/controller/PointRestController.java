package com.bucheon.sch.biz.operation.point.v1.controller;

import com.bucheon.sch.biz.operation.point.v1.model.TrashTypeReq;
import com.bucheon.sch.biz.operation.point.v1.model.TrashTypeUpdate;
import com.bucheon.sch.biz.operation.point.v1.service.PointService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/point/")
public class PointRestController {

    private final PointService pointService;

    @PostMapping(path = "/trashList")
    public Object TrashSelectList(@RequestBody TrashTypeReq dto){
        return pointService.TrashSelectListService(dto);
    }

    @PostMapping(path = "/trashInsert")
    public Object TrashInsert(@RequestBody TrashTypeUpdate dto) throws IOException {
        return pointService.TrashSaveService(dto);
    }

}
