package com.bucheon.sch.biz.chatbot.alarmtalk.v1.service;

import com.bucheon.sch.biz.chatbot.alarmtalk.v1.model.AlaramBoardRes;
import com.bucheon.sch.biz.chatbot.alarmtalk.v1.model.AlramSendVO;
import com.bucheon.sch.biz.chatbot.alarmtalk.v1.model.ChatbotData;
import com.bucheon.sch.biz.chatbot.alarmtalk.v1.model.ChatbotPoto;
import com.bucheon.sch.biz.environmental.report.v1.band.BandService;
import com.bucheon.sch.biz.operation.admin.v1.service.AdminService;
import com.bucheon.sch.biz.util.service.UtilService;
import com.bucheon.sch.common.type.ProcessType;
import com.bucheon.sch.common.type.ReceptionType;
import com.bucheon.sch.common.util.Masking;
import com.bucheon.sch.db.entity.chatbot.TB_AlaramBoard;
import com.bucheon.sch.db.entity.chatbot.TB_AlaramBoardPoto;
import com.bucheon.sch.db.entity.environmental.Report;
import com.bucheon.sch.db.entity.environmental.ReportLog;
import com.bucheon.sch.db.entity.etc.CmnCode;
import com.bucheon.sch.db.entity.etc.DongDamdang;
import com.bucheon.sch.db.entity.etc.DongName;
import com.bucheon.sch.db.entity.operation.Admin;
import com.bucheon.sch.db.entity.operation.User;
import com.bucheon.sch.db.repository.chatbot.*;
import com.bucheon.sch.db.repository.environmental.ReportImgRepository;
import com.bucheon.sch.db.repository.environmental.ReportLogRepository;
import com.bucheon.sch.db.repository.environmental.ReportRepository;
import com.bucheon.sch.db.repository.etc.CmnCodeRepository;
import com.bucheon.sch.db.repository.etc.DongDamdangRepository;
import com.bucheon.sch.db.repository.etc.DongNameRepository;
import com.bucheon.sch.db.repository.operation.AdminRepository;
import com.bucheon.sch.db.repository.operation.TrashTypeRepository;
import com.bucheon.sch.db.repository.operation.UserRepository;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AlaramtalkService {

    private final Logger logger = LoggerFactory.getLogger(AlaramtalkService.class);
    private final HttpServletRequest httpServletRequest;
    private final AlaramBoardRepository alaramBoardRepository;
    private final AlaramBoardPotoRepository alaramBoardPotoRepository;
    private final ReportRepository reportRepository;
    private final ReportLogRepository reportLogRepository;
    private final UserRepository userRepository;
    private final ReportImgRepository reportImgRepository;
    private final ModelMapper modelMapper;
    private final TrashTypeRepository trashTypeRepository;
    private final BandService bandService;
    private final AdminService adminService;
    private final AdminRepository adminRepository;
    private final UtilService utilService;
    private final CmnCodeRepository cmnCodeRepository;
    private final DongNameRepository dongNameRepository;
    private final DongDamdangRepository dongDamdangRepository;


    private final SkllRepository skllRepository;
    private final SkllCardRepository skllCardRepository;
    private final SkllImageRepository skllImageRepository;
    private final SkllItmRepository skllItmRepository;
    private final SkllListRepository skllListRepository;
    private final SkllRplyRepository skllRplyRepository;
    private final SkllTextRepository skllTextRepository;

    @Value("${chatbot.apikey}")
    String apikey;

    @Value("${chatbot.fromPhone}")
    String fromPhone;

    @Value("${chatbot.url}")
    String chatUrl;


    @Transactional
    public void SaveService(ChatbotData dto) throws Exception {

        String bb = dto.getOldAddress();
        bb = bb.replaceAll(" ", "");
        bb = bb.replaceAll("경기도", "");
        bb = bb.replaceAll("경기", "");
        bb = bb.replaceAll("부천시", "");
        bb = bb.split("동")[0]+"동";

        String receptionNum = String.valueOf(Timestamp.valueOf(LocalDateTime.now()).getTime());

        DongName dongName = dongNameRepository.findByName(bb).orElseThrow(IllegalArgumentException::new);

        //저장
        Report reportResult = reportRepository.save(dto.ofReport(dongName.getAdmin()));

        //로그 저장
        ReportLog r = new ReportLog();
        r.setReportSeq(reportResult.getReportSeq());
        r.setLogTime(LocalDateTime.now());
        r.setReceptionNum(String.valueOf(Timestamp.valueOf(LocalDateTime.now()).getTime()));
        r.setReportStatus(reportResult.getReportStatus());
        r.setReportDamdang(0L);
        r.setMsg("접수되었습니다.");
        ReportLog reportLogResult = reportLogRepository.save(r);

        //카운트가 0 이면 새로 등록한다
        int userCnt = userRepository.countByChatbotUserId(dto.ofUser().getChatbotUserId());
        if(userCnt == 0) {
            User user = userRepository.save(dto.ofUser());
        }

        //알람등록
        TB_AlaramBoard alaramsaveResult = alaramBoardRepository.save(dto.ofTB_AlaramBoard(reportResult.getReportSeq()));
        logger.info("saveresult = {}", new Gson().toJson(alaramsaveResult));
        dto.ofTB_AlaramBoardPoto(alaramsaveResult.getBoardSeqId()).stream().forEach(e -> alaramBoardPotoRepository.save(e));
        dto.ofofReportImg(reportResult.getReportSeq()).stream().forEach(e -> reportImgRepository.save(e));

        String chatContents = "[부천시 깨끗한마을]"
                + "\n"
                + "접수자 : " + Masking.phoneMasking(dto.getPhoneNumber())
                + "\n"
                + dto.getContents()
                + "\n"
                + dto.getChatbotPotos().stream().map(e -> {
            try {
                return "사진 : " + utilService.CreateShortURLService(e.getClassUrl());
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
            return e.getClassUrl();
        }).collect(Collectors.joining("\n"));

        String s = bandService.SendBandPost(dongName.getAdmin(), chatContents);
        Gson gson = new Gson();
        String jsonObject = new Gson().toJson(s);
        JsonElement jsonElement = gson.fromJson(s, JsonElement.class);
        jsonElement = jsonElement.getAsJsonObject().get("result_data");

        String bandKey = String.valueOf(jsonElement.getAsJsonObject().get("band_key"));
        String postKey = String.valueOf(jsonElement.getAsJsonObject().get("post_key"));
        logger.info("- 밴드 Response >>> bandKey : {}, postKey : {}", bandKey, postKey );

        reportResult.setBandKey(bandKey.replaceAll("\"",""));
        reportResult.setSourcePk(postKey.replaceAll("\"",""));
        reportRepository.save(reportResult);

        //챗봇일때만 알림톡 발송
        if(reportResult.getReportReception().equals(ReceptionType.C.getName())){
            AlramSendVO alramSendVO = new AlramSendVO();
            List<String> arrDc = new ArrayList<>();
            List<String> arrBtn = new ArrayList<>();
            arrBtn.add("1");

            alramSendVO.setToPhone(reportResult.getReportUserPhone());
            alramSendVO.setAlramId("SC_001");
            alramSendVO.setSysCode("SC");
            alramSendVO.setArrDc(arrDc);
            alramSendVO.setArrBtn(arrBtn);

            try {
                AlramSendService(alramSendVO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        ExecutorService es = Executors.newCachedThreadPool();
//        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Start");
//        Thread.sleep(10000);
//        int cnt = reportRepository.countByReportReceptionAndReportStatusAndDelYn("C", "D", "N");
//        if(cnt > 0) {
//            List<Report> reportCList = reportRepository.findAllByReportReceptionAndReportStatusAndDelYnOrderByReportSeqDesc("C", "D", "N");
//            var damdang = adminRepository.findByUseYnAndDelYnOrderByAdminNameAsc("Y","N")
//                    .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
//            for(Report reportCOne : reportCList){
//
//                try {
//
//                    int dongDamdangcnt = dongDamdangRepository.countByDong(reportCOne.getAreaName());
//                    if(dongDamdangcnt > 0) {
//
//                        logger.info("- reportCOne : " + reportCOne.getAreaName());
//
//                        //담당자를 구한다
//                        DongDamdang dongDamdang = dongDamdangRepository.findByDong(reportCOne.getAreaName()).orElseThrow(IllegalArgumentException::new);
//                        Admin admin = adminRepository.findByAdminSeq(dongDamdang.getDamdang()).orElseThrow(IllegalArgumentException::new);
//
//                        //업데이트 한다
//                        reportCOne.setReportStatus(ProcessType.P.getName());
//                        reportRepository.UpdateReportStatus(reportCOne.getReportStatus(), reportCOne.getReportSeq());
//
//                        //로그를 남긴다
//                        String msg = "";
//                        //로그 저장
//                        if(reportCOne.getReportStatus().equals(ProcessType.D.getName())){
//                            msg = "접수하였습니다";
//                        } else if(reportCOne.getReportStatus().equals(ProcessType.C.getName())){
//                            msg = "담당자 " + admin.getAdminName() + "님께서 처리완료로 변경하였습니다.";
//                        } else if(reportCOne.getReportStatus().equals(ProcessType.P.getName())){
//                            msg = "담당자 " + admin.getAdminName() + "님께서 처리중으로 변경하였습니다.";
//                        } else if(reportCOne.getReportStatus().equals(ProcessType.R.getName())){
//                            msg = "담당자 " + admin.getAdminName() + "님께서 처리불가로 변경하였습니다.";
//                        }
//                        logger.info("- msg : {}" + msg);
//
//                        int cnt2 = reportLogRepository.countByReportSeq(reportCOne.getReportSeq());
//                        String rn = String.valueOf(Timestamp.valueOf(LocalDateTime.now()).getTime());
//                        if (cnt2 > 0) {
//                            ReportLog reportLog = reportLogRepository.findTop1ByReportSeqOrderByLogSeqDesc(reportCOne.getReportSeq()).orElseThrow(IllegalArgumentException::new);
//                            rn = reportLog.getReceptionNum();
//                        }
//                        ReportLog r2 = new ReportLog();
//                        r2.setReportSeq(reportCOne.getReportSeq());
//                        r2.setLogTime(LocalDateTime.now());
//                        r2.setReceptionNum(rn);
//                        r2.setReportStatus(reportCOne.getReportStatus());
//                        r2.setReportDamdang(dongDamdang.getDamdang());
//                        r2.setMsg(msg);
//                        r2.setReportPointRejectReason(reportCOne.getReportPointRejectReason());
//
//                        reportLogRepository.save(r2);
//                    }
//
//                } catch (Exception e){
//                    e.printStackTrace();
//                }
//
//            }
//
//
//        }
//        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> End");
    }

    public TB_AlaramBoard jsonvaluechk() {
        TB_AlaramBoard aa = new TB_AlaramBoard();
        List<TB_AlaramBoardPoto> bb = new ArrayList<>();
        bb.add(TB_AlaramBoardPoto.builder().classUrl("qwe").build());
        aa.setFeedbackDetail("a");
        aa.setAlaramBoardPotos(bb);
        return aa;
    }

    @Transactional(readOnly = true)
    public AlaramBoardRes TB_AlaramBoardSelectService(Long seq) {
        AlaramBoardRes abr = modelMapper.map(alaramBoardRepository.findById(seq).orElse(null), AlaramBoardRes.class);
        Report report = reportRepository.findByReportSeq(seq);
        abr.setReceptionNum(report.getReceptionNum());
        abr.setReportStatus(report.getReportStatus());
        abr.setComDt(report.getComDt());
        abr.setReportDamdangName(report.getAreaName());

        CmnCode cmnCode = cmnCodeRepository.findByColumnAndCode("report_status", report.getReportStatus()).orElseThrow(IllegalArgumentException::new);
        abr.setReportStatusName(cmnCode.getName());

        abr.setChatbotPotos(alaramBoardPotoRepository.findByBoardSeqId(seq).stream()
                .map(e -> modelMapper.map(e, ChatbotPoto.class))
                .collect(Collectors.toList()));

        abr.setReportLogs(reportLogRepository.findByReportSeqOrderByLogSeqDesc(seq).stream()
                .map(e -> modelMapper.map(e, ReportLog.class))
                .collect(Collectors.toList()));
        var ress = trashTypeRepository.findByTrashSeqAndUseYnAndDelYn(Long.parseLong(abr.getReportType()), "Y", "N");
        abr.setReportImg(ress.getIconUrl());
        abr.setReportName(ress.getTrashName());
        return abr;
    }

    @Transactional(readOnly = true)
    public List<AlaramBoardRes> TB_AlaramBoardSelectPhoneService2(String phone) {
        List<AlaramBoardRes> res = alaramBoardRepository.findByPhoneNumberOrderByAlaramBoardDtDesc(phone)
                .stream().map(e -> modelMapper.map(e, AlaramBoardRes.class))
                .peek(f -> f.setChatbotPotos(alaramBoardPotoRepository.findByBoardSeqId(f.getBoardSeqId()).stream()
                        .map(e -> modelMapper.map(e, ChatbotPoto.class)).collect(Collectors.toList())))
                .collect(Collectors.toList());
        return res;
    }

    @Transactional(readOnly = true)
    public List<AlaramBoardRes> TB_AlaramBoardSelectPhoneService(String phone, String sdate, String edate, PageRequest page) throws IllegalArgumentException {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMdd");

        System.out.println("phone : " + phone);
        System.out.println("sdate : " + sdate);
        System.out.println("edate : " + edate);
        System.out.println("page : " + page);

        logger.info("sdate - {}", sdate);
        logger.info("edate - {}", edate);
        LocalDateTime ss = LocalDate.parse(sdate, format).atStartOfDay();
        LocalDateTime ee = LocalDate.parse(edate, format).atStartOfDay().plusDays(1L);
        logger.info("ss - {}", ss);
        logger.info("ee - {}", ee);
        Pageable pageable = page;
        List<AlaramBoardRes> res = alaramBoardRepository.findByPhoneNumberAndAlaramBoardDtGreaterThanAndAlaramBoardDtLessThanOrderByAlaramBoardDtDesc
                        (phone, ss, ee, pageable)
                .stream().map(e -> modelMapper.map(e, AlaramBoardRes.class))
                .peek(f -> {
                    var report = reportRepository.findByReportSeq(f.getBoardSeqId());
                    f.setReportStatus(report.getReportStatus());
                    var ress = trashTypeRepository.findByTrashSeqAndUseYnAndDelYn(Long.parseLong(f.getReportType()), "Y", "N");
                    try {
                        f.setReportImg(ress.getIconUrl());
                        f.setReportName(ress.getTrashName());
                        f.setChatbotPotos(alaramBoardPotoRepository.findByBoardSeqId(f.getBoardSeqId())
                                .stream().map(e -> modelMapper.map(e, ChatbotPoto.class)).collect(Collectors.toList()));
                        CmnCode cmnCode = cmnCodeRepository.findByColumnAndCode("report_status", report.getReportStatus()).orElseThrow(IllegalArgumentException::new);
                        f.setReportStatusName(cmnCode.getName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                })
                .collect(Collectors.toList());
        return res;
    }

    //@Scheduled(cron = "* 0/2 * * * ?")
    public void AutoReportStatusPChage() {

        LocalDateTime now = LocalDateTime.now();



        int cnt = reportRepository.countByReportReceptionAndReportStatusAndDelYn("C", "D", "N");
        if(cnt > 0) {
            List<Report> reportCList = reportRepository.findAllByReportReceptionAndReportStatusAndDelYnOrderByReportSeqDesc("C", "D", "N");
            var damdang = adminRepository.findByUseYnAndDelYnOrderByAdminNameAsc("Y","N")
                    .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
            for(Report reportCOne : reportCList){

                try {

                    int dongDamdangcnt = dongDamdangRepository.countByDong(reportCOne.getAreaName());
                    if(dongDamdangcnt > 0) {

                        logger.info("- reportCOne : " + reportCOne.getAreaName());

                        //담당자를 구한다
                        DongDamdang dongDamdang = dongDamdangRepository.findByDong(reportCOne.getAreaName()).orElseThrow(IllegalArgumentException::new);
                        Admin admin = adminRepository.findByAdminSeq(dongDamdang.getDamdang()).orElseThrow(IllegalArgumentException::new);

                        //업데이트 한다
                        reportCOne.setReportStatus(ProcessType.P.getName());
                        reportRepository.UpdateReportStatus(reportCOne.getReportStatus(), reportCOne.getReportSeq());

                        //로그를 남긴다
                        String msg = "";
                        if (reportCOne.getReportStatus().equals(ProcessType.D.getName())) {
                            msg = "담당자 " + admin.getAdminName() + "님께서 현재상태를 저장하였습니다";
                        } else if (reportCOne.getReportPointCompleteYn().equals(ProcessType.P.getName())) {
                            msg = "담당자 " + admin.getAdminName() + "님께서 마일리지를 지급하였습니다.";
                        } else if (reportCOne.getReportPointCompleteYn().equals(ProcessType.R.getName())) {
                            msg = "담당자 " + admin.getAdminName() + "님께서 마일리지를 지급불가로 변경하였습니다.";
                        }
                        logger.info("- msg : {}" + msg);

                        int cnt2 = reportLogRepository.countByReportSeq(reportCOne.getReportSeq());
                        String rn = String.valueOf(Timestamp.valueOf(LocalDateTime.now()).getTime());
                        if (cnt2 > 0) {
                            ReportLog reportLog = reportLogRepository.findTop1ByReportSeqOrderByLogSeqDesc(reportCOne.getReportSeq()).orElseThrow(IllegalArgumentException::new);
                            rn = reportLog.getReceptionNum();
                        }
                        ReportLog r2 = new ReportLog();
                        r2.setReportSeq(reportCOne.getReportSeq());
                        r2.setLogTime(LocalDateTime.now());
                        r2.setReceptionNum(rn);
                        r2.setReportStatus(reportCOne.getReportStatus());
                        r2.setReportDamdang(dongDamdang.getDamdang());
                        r2.setMsg(msg);
                        r2.setReportPointRejectReason(reportCOne.getReportPointRejectReason());

                        reportLogRepository.save(r2);
                    }

                } catch (Exception e){
                    e.printStackTrace();
                }

            }


        }
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> End");


    }

    public String AlramSendService(AlramSendVO dto) {

        if (dto.getFromPhone() == null || dto.getFromPhone().trim().equals("")) {
            dto.setFromPhone(fromPhone);
        }

        RestTemplate restTemplate = new RestTemplate();
        //String url = "localhost:9502/alram/alram";
        String url = chatUrl + "/alarm/alram";

        // Header set
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        //apiKey 추가
        httpHeaders.set("apiKey",apikey);

        // Body set
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        // String imageFileString = fileService.getBase64String(file);


        body.add("toPhone", dto.getToPhone());
        body.add("fromPhone", dto.getFromPhone());
        body.add("alramId", dto.getToPhone());
        body.add("sysCode", dto.getSysCode());
        body.add("arrDc", dto.getArrDc().toString());
        body.add("arrBtn", dto.getArrBtn().toString());

        // Combine Message
        HttpEntity<?> requestMessage = new HttpEntity<>(dto, httpHeaders);
        logger.info("---- body : {}" , new Gson().toJson(requestMessage));

        // Request and getResponse
        HttpEntity<String> response = restTemplate.postForEntity(url, requestMessage, String.class);

        // Response Body 파싱
        // FlaskResponseDto 란 DTO로 받을때 사용
        // 일반 String으로 받아 처리해서 아래처럼 처리
        //ObjectMapper objectMapper = new ObjectMapper();
        //objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
        //FlaskResponseDto dto = objectMapper.readValue(response.getBody(), FlaskResponseDto.class);
        String responseStr = response.getBody();

        return responseStr;
        //return null;
    }

}
