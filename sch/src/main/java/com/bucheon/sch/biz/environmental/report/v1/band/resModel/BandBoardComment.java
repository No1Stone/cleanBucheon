package com.bucheon.sch.biz.environmental.report.v1.band.resModel;

import com.bucheon.sch.biz.environmental.report.v1.band.model.BandAuthor;
import com.bucheon.sch.biz.environmental.report.v1.band.model.BandPotos;


public class BandBoardComment {
    /**
     * 댓글 API용 VO
     */

    private long created_at;
    private int emotion_count;
    private String comment_key;
    private String sticker;
    private String post_key;
    private String band_key;
    private boolean is_audio_included;
    private String content;

    private BandAuthor author;
    private BandPotos photo;
    private String processStatus;
    private String bandDamdang;


    public BandBoardComment() {
    }

    public BandBoardComment(long created_at, int emotion_count, String comment_key, String sticker, String post_key, String band_key, boolean is_audio_included, String content, BandAuthor author, BandPotos photo, String processStatus) {
        this.created_at = created_at;
        this.emotion_count = emotion_count;
        this.comment_key = comment_key;
        this.sticker = sticker;
        this.post_key = post_key;
        this.band_key = band_key;
        this.is_audio_included = is_audio_included;
        this.content = content;
        this.author = author;
        this.photo = photo;
        this.processStatus = processStatus;

//        if (this.content.contains("#완료") && this.author.toString().contains("손주현")) {
//            this.processStatus = ProcessType.C.getName();
//        } else {
//            this.processStatus = ProcessType.D.getName();
//        }
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public int getEmotion_count() {
        return emotion_count;
    }

    public void setEmotion_count(int emotion_count) {
        this.emotion_count = emotion_count;
    }

    public String getComment_key() {
        return comment_key;
    }

    public void setComment_key(String comment_key) {
        this.comment_key = comment_key;
    }

    public String getSticker() {
        return sticker;
    }

    public void setSticker(String sticker) {
        this.sticker = sticker;
    }

    public String getPost_key() {
        return post_key;
    }

    public void setPost_key(String post_key) {
        this.post_key = post_key;
    }

    public String getBand_key() {
        return band_key;
    }

    public void setBand_key(String band_key) {
        this.band_key = band_key;
    }

    public boolean isIs_audio_included() {
        return is_audio_included;
    }

    public void setIs_audio_included(boolean is_audio_included) {
        this.is_audio_included = is_audio_included;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

//    public BandAuthor getAuthor() {
//        return author;
//    }
    public BandAuthor getAuthor() {
    return author;
}

    public void setAuthor(BandAuthor author) {
        this.author = author;
    }

    public BandPotos getPhoto() {
        return photo;
    }

    public void setPhoto(BandPotos photo) {
        this.photo = photo;
    }

//    public String getProcessStatus() {
//        if (this.content.contains("#완료") && this.author.toString().contains("손주현")) {
//            this.processStatus = ProcessType.C.getName();
//        } else {
//            this.processStatus = ProcessType.D.getName();
//        }
//        return processStatus;
//    }
    public Object getProcessStatus() { return  processStatus; }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }
}
