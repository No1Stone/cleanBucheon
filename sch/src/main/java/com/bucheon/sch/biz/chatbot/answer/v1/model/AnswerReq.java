package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnswerReq {

    private int page;
    private int size;

    private String skllCode;
    private String skllNm;
    private String updtDe;
    private String skllLevel;
    private String rspnsTy1;
    private String rspnsTy2;
    private String rspnsTy3;

    private String searchSelect;
    private String searchString;




}
