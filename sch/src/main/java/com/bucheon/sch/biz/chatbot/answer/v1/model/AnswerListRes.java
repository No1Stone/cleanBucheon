package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnswerListRes {

    //코드
    private String skllCode;
    //수정일자
    private String updtDe;
    //레벨 합
    private String skllLevel;
    //응답타입1 글자
    private String rspnsTy1;
    //응답타입2 글자
    private String rspnsTy2;
    //응답타입3 글자
    private String rspnsTy3;


}
