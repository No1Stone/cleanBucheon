package com.bucheon.sch.biz.environmental.campaign.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CampaignVO {

    private Long campaignSeq;
    private String campaignNum; //접수번호
    private String campaignStatus;
    private String campaignProcess;
    private String campaignProcessRejectReason;
    private Long trashType;
    private String addrOld;
    private String addrNew;
    private String areaName;
    private String locationX;
    private String locationY;
    private String regUserDid;
    private String delYn;
    private int mileage;
    private String mileageStatus;
    private String mileageRejectReason;
    private LocalDateTime comDt;
    private LocalDateTime regDt;

}
