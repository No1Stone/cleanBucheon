package com.bucheon.sch.biz.metabus.metaPush.v1.model;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MetaPushRes {

    private Long sendSeq;
    private String sendType;
    private String sendTypeName;
    private String resultType;
    private String resultTypeName;
    private String title;
    private String body;
    private int sendCnt;
    private Long regSeq;
    private String regName;
    private LocalDateTime regDt;
    private LocalDateTime sendDt;

}
