package com.bucheon.sch.biz.metabus.metaBoard.v1.controller;

import com.bucheon.sch.biz.metabus.metaBoard.v1.model.MetaBoardRes;
import com.bucheon.sch.biz.metabus.metaBoard.v1.model.MetaNoticePopupVO;
import com.bucheon.sch.biz.metabus.metaBoard.v1.model.MetaUserVoteHistoryVO;
import com.bucheon.sch.biz.metabus.metaBoard.v1.service.MetaBoardService;
import com.bucheon.sch.biz.metabus.metaBoard.v1.service.MetaNoticePopupService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "/metaBus/board/rest")
@RequiredArgsConstructor
public class MetaBoardRestController {

    private final Logger logger = LoggerFactory.getLogger(MetaBoardRestController.class);
    private final MetaBoardService metaBoardService;
    private final MetaNoticePopupService metaNoticePopupService;



    @PostMapping(path = "put/vote/{boardSeq}")
    public Object putVote(@RequestBody MetaUserVoteHistoryVO dto) {
        Map<String, Object> res = new HashMap<>();

        int todayVote = metaBoardService.countTodayVote(dto);

        if(todayVote == 0) {
            try {

                if(metaBoardService.MetaBoardCountOneService(dto.getBoardSeq()) > 0) {
                    metaBoardService.UpdateMetaVote(dto);
                } else {
                    res.put("result", "not board");
                }

            } catch (Exception e) {
                res.put("result", "fail");
            }
            res.put("result", "success");
        } else {
            res.put("result", "already voted");
        }

        return res;
    }

    @GetMapping(path = "/get/board/{boardSeq}")
    public Object getBoardInfo(@PathVariable Long boardSeq ) {
        return metaBoardService.MetaBoardSelectOneService(boardSeq);
    }

    @GetMapping(path = "/get/vote")
    public Object getVote() {

        MetaBoardRes b =  metaBoardService.MetaBoardSelectTypeOneService("A");
        return metaBoardService.MetaBoardSelectOneService(b.getBoardSeq());
    }

    @GetMapping(path = "/get/card1")
    public Object getCard1() {

        MetaBoardRes b =  metaBoardService.MetaBoardSelectTypeOneService("B");
        return metaBoardService.MetaBoardSelectOneService(b.getBoardSeq());
    }

    @GetMapping(path = "/get/card2")
    public Object getCard2() {

        MetaBoardRes b =  metaBoardService.MetaBoardSelectTypeOneService("C");
        return metaBoardService.MetaBoardSelectOneService(b.getBoardSeq());
    }

    @GetMapping(path = "/get/movie")
    public Object getMovie() {

        MetaBoardRes b =  metaBoardService.MetaBoardSelectTypeOneService("D");
        return metaBoardService.MetaBoardSelectOneService(b.getBoardSeq());
    }

    //불편접수 팝업 이미지 URL
    @GetMapping(path = "/get/popup/image1")
    public Object popupImage1() {

        MetaNoticePopupVO m = metaNoticePopupService.MetaNoticePopupSelectOneService("image1");
        Map<String, Object> res = new HashMap<>();
        if(m.getUseYn().equals("Y")){
            res.put("result", "view Image");
            res.put("url", m.getPopupImageUrl());
        }
        else if(m.getUseYn().equals("N")){
            res.put("result", "No Image");
            res.put("url", "");
        }

        return res;
    }

    //줍기 팝업 이미지 URL
    @GetMapping(path = "/get/popup/image2")
    public Object popupImage2() {

        MetaNoticePopupVO m = metaNoticePopupService.MetaNoticePopupSelectOneService("image2");
        Map<String, Object> res = new HashMap<>();
        if(m.getUseYn().equals("Y")){
            res.put("result", "view Image");
            res.put("url", m.getPopupImageUrl());
        }
        else if(m.getUseYn().equals("N")){
            res.put("result", "No Image");
            res.put("url", "");
        }

        return res;
    }

    //수거 팝업 이미지 URL
    @GetMapping(path = "/get/popup/image3")
    public Object popupImage3() {

        MetaNoticePopupVO m = metaNoticePopupService.MetaNoticePopupSelectOneService("image3");
        Map<String, Object> res = new HashMap<>();
        if(m.getUseYn().equals("Y")){
            res.put("result", "view Image");
            res.put("url", m.getPopupImageUrl());
        }
        else if(m.getUseYn().equals("N")){
            res.put("result", "No Image");
            res.put("url", "");
        }

        return res;
    }


}
