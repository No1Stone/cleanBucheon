package com.bucheon.sch.biz.environmental.complaint.v1.controller;

import com.bucheon.sch.biz.environmental.complaint.v1.model.ComplaintsReq;
import com.bucheon.sch.biz.environmental.complaint.v1.service.ComplaintService;
import com.bucheon.sch.db.entity.environmental.Process;
import com.bucheon.sch.db.entity.environmental.Receipt;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/com")
@RequiredArgsConstructor
public class ComplaintTestController {
    private final Logger logger = LoggerFactory.getLogger(ComplaintTestController.class);
    private final ComplaintService complaintService;

    @PostMapping(path = "/test1")
    public Object gettestAll(@RequestBody ComplaintsReq dto){
        return complaintService.ReportSelectListInfoService(dto);
    }

    @GetMapping(path = "/test2")
    public List<Process> gettest2(){
        return complaintService.getTest2();
    }

    @GetMapping(path = "/test3")
    public List<Receipt> gettest3(){
        return complaintService.getTest3();
    }

}
