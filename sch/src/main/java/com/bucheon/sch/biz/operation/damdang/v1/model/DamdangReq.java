package com.bucheon.sch.biz.operation.damdang.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DamdangReq {


    private String dong;
    private Long damdang;

}
