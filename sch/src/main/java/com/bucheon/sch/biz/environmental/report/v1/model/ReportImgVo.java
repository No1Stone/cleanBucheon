package com.bucheon.sch.biz.environmental.report.v1.model;

import com.bucheon.sch.db.entity.environmental.ReportImg;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReportImgVo {

    private Long reportSeq;
    private String reportImg;

    public ReportImg ofReportImg(){
        ReportImg r = new ReportImg();
        r.setReportSeq(this.reportSeq);
        r.setReportImg(this.reportImg);
        return r;
    }

}
