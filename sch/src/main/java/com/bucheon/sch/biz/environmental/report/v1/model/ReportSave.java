package com.bucheon.sch.biz.environmental.report.v1.model;

import com.bucheon.sch.common.type.PaymentType;
import com.bucheon.sch.common.type.ProcessType;
import com.bucheon.sch.common.type.YnType;
import com.bucheon.sch.db.entity.environmental.Report;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReportSave {

    private String receptionNum;
    private String reportContent;
    //유형 트래쉬타입
    private Long reportType;
    //    private String reportTypeName;
    //    private String reportReceptionName;
    //주소
    private String reportAddrOld;
    //접수상태
    private String reportStatus;
    private String reportPointCompleteYn;
    private String reportReception;
    //    private String reportStatusName;
    private String reportLocationX;
    private String reportLocationY;
    private String reportAddrNew;
    private String bname;
    private String areaName;
    private String reportUserName;
    private String reportUserPhone;
    private String reportUserEmail;
//    private String delYn;
    private String regUserId;
//    private LocalDateTime comDt;
    private LocalDateTime regDt;
    private LocalDateTime modDt;

    public Report ofReport(){
        Report report = new Report();
        report.setReportContent(this.reportContent);

        if(this.reportType != null) {
            report.setReportType(this.reportType);
        } else {
            report.setReportType(20L);
        }

        report.setReceptionNum(this.receptionNum);
        report.setReportPointCompleteYn(PaymentType.W.getName());
        report.setReportReception(this.reportReception);
        report.setReportLocationX(this.reportLocationX);
        report.setReportLocationY(this.reportLocationY);
        report.setReportAddrNew(this.reportAddrNew);
        report.setReportAddrOld(this.reportAddrOld);
        report.setAreaName(this.areaName);
        report.setReportStatus(ProcessType.D.getName());
        report.setReportUserName(this.reportUserName);
        report.setReportUserPhone(this.reportUserPhone);
        report.setReportUserEmail(this.reportUserEmail);
        report.setDelYn(YnType.N.getName());
        report.setRegUserId(this.regUserId);
//        report.setComDt();
        report.setRegDt(this.regDt);
        report.setModDt(this.modDt);
        return report;
    }
}
