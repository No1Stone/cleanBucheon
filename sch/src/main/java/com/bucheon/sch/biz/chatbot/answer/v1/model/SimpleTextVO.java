package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 스킬관리  > Simple Text VO
 * @author user
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SimpleTextVO{

	/* 스킬 코드 */
	private String skillCode;
	
	/* 기본 텍스트 */
	private String text;
	
	/* 응답 순서 */
	private int resSeq;
	
	/* 공공예약 구분하는 값 */
	private String subSystem;
	
}
