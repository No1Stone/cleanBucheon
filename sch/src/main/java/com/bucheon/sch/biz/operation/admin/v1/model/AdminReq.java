package com.bucheon.sch.biz.operation.admin.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminReq {


    private int page;
    private int size;
    private String adminName;
    private String adminId;
    private String searchSelect;
    private String searchString;

}
