package com.bucheon.sch.biz.metabus.metaBoard.v1.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MetaBoardRes {

    private Long boardSeq;
    private String boardType;
    private String boardTypeName;
    private String boardTitle;
    private String boardContent;
    private int vote1;
    private int vote2;
    private int vote3;
    private LocalDateTime sdate;
    private LocalDateTime edate;
    private String sdateName;
    private String edateName;
    private String useYn;
    private Long regSeq;
    private String boardRegName;
    private LocalDateTime regDt;
    private Long modSeq;
    private String boardModName;
    private LocalDateTime modDt;
    private int boardAttCnt;
    private List<String> boardAttList;

}
