package com.bucheon.sch.biz.operation.menu.v1.model;

import com.bucheon.sch.common.type.YnType;
import com.bucheon.sch.db.entity.operation.Menu;

import java.time.LocalDateTime;

public class MenuUpdate {

    private Long menuSeq;
    private String menuName;
    private int menuLevel;
    private Long pmenuSeq;
    private String menuUrl;
    private String menuIcon;
    private String menuDesc;
    private String useYn;
    private Long[] delYn;
    private int orderNum;
    private Long regSeq;
    private LocalDateTime regDt;
    private Long modSeq;
    private LocalDateTime modDt;
    public MenuUpdate(){}

    public MenuUpdate(Long menuSeq, String menuName, int menuLevel, Long pmenuSeq, String menuUrl,
                      String menuIcon, String menuDesc, String useYn, Long[] delYn, int orderNum, Long regSeq, LocalDateTime regDt, Long modSeq, LocalDateTime modDt) {
        this.menuSeq = menuSeq;
        this.menuName = menuName;
        this.menuLevel = menuLevel;
        this.pmenuSeq = pmenuSeq;
        this.menuUrl = menuUrl;
        this.menuIcon = menuIcon;
        this.menuDesc = menuDesc;
        this.useYn = useYn;
        this.delYn = delYn;
        this.orderNum = orderNum;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.modSeq = modSeq;
        this.modDt = modDt;
    }

    public Long getMenuSeq() {
        return menuSeq;
    }

    public void setMenuSeq(Long menuSeq) {
        this.menuSeq = menuSeq;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public int getMenuLevel() {
        return menuLevel;
    }

    public void setMenuLevel(int menuLevel) {
        this.menuLevel = menuLevel;
    }

    public Long getPmenuSeq() {
        return pmenuSeq;
    }

    public void setPmenuSeq(Long pmenuSeq) {
        this.pmenuSeq = pmenuSeq;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public String getMenuDesc() {
        return menuDesc;
    }

    public void setMenuDesc(String menuDesc) {
        this.menuDesc = menuDesc;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public Long[] getDelYn() {
        return delYn;
    }

    public void setDelYn(Long[] delYn) {
        this.delYn = delYn;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public Long getRegSeq() {
        return regSeq;
    }

    public void setRegSeq(Long regSeq) {
        this.regSeq = regSeq;
    }

    public LocalDateTime getRegDt() {
        return regDt;
    }

    public void setRegDt(LocalDateTime regDt) {
        this.regDt = regDt;
    }

    public Long getModSeq() {
        return modSeq;
    }

    public void setModSeq(Long modSeq) {
        this.modSeq = modSeq;
    }

    public LocalDateTime getModDt() {
        return modDt;
    }

    public void setModDt(LocalDateTime modDt) {
        this.modDt = modDt;
    }

    public Menu ofMenu(String iconUrl){
        Menu m = new Menu();

        if (this.menuSeq!=null) {
            m.setMenuSeq(this.menuSeq);
        }

        m.setMenuName(this.menuName);
        m.setMenuLevel(this.menuLevel);
        m.setPmenuSeq(this.pmenuSeq);
        m.setMenuUrl(this.menuUrl);
        m.setMenuIcon(iconUrl);
        m.setMenuDesc(this.menuDesc);
        m.setUseYn(YnType.Y.getName());
        m.setDelYn(YnType.N.getName());
        m.setOrderNum(this.orderNum);
        m.setRegSeq(this.regSeq);
        m.setRegDt(LocalDateTime.now());
        m.setModSeq(this.modSeq);
        m.setModDt(LocalDateTime.now());
        return m;
    }
}
