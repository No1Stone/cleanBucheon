package com.bucheon.sch.biz.operation.permission.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PermissionRes {

    private int page;
    private int size;
    private Long permissionSeq;
    private String permissionName;
    private String permissionNameList;
    private String useYn;
    private Long regSeq;
    private String regSeqName;
    private Long modSeq;
    private String modSeqName;
    private LocalDateTime regDt;
    private LocalDateTime modDt;

}
