package com.bucheon.sch.biz.metabus.metaPush.v1.service;

import com.bucheon.sch.biz.metabus.metaPush.v1.model.MetaPushLogVO;
import com.bucheon.sch.biz.metabus.metaPush.v1.model.MetaPushReq;
import com.bucheon.sch.biz.metabus.metaPush.v1.model.MetaPushRes;
import com.bucheon.sch.biz.metabus.metaPush.v1.model.MetaPushVO;
import com.bucheon.sch.biz.metabus.metaUser.v1.service.MetaUserService;
import com.bucheon.sch.biz.util.model.RequestDTO;
import com.bucheon.sch.biz.util.service.FCMService2;
import com.bucheon.sch.common.util.Paging;
import com.bucheon.sch.db.entity.metabus.MetaPush;
import com.bucheon.sch.db.entity.metabus.MetaPushLog;
import com.bucheon.sch.db.entity.metabus.MetaUser;
import com.bucheon.sch.db.entity.operation.Admin;
import com.bucheon.sch.db.repository.metabus.MetaPushLogRepository;
import com.bucheon.sch.db.repository.metabus.MetaPushRepository;
import com.bucheon.sch.db.repository.metabus.MetaUserRepository;
import com.bucheon.sch.db.repository.operation.AdminRepository;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MetaPushService {

    private final Logger logger = LoggerFactory.getLogger(MetaPushService.class);
    private final HttpServletRequest httpServletRequest;
    private final MetaPushRepository metaPushRepository;
    private final MetaPushLogRepository metaPushLogRepository;
    private final AdminRepository adminRepository;
    private final MetaUserRepository metaUserRepository;
    private final MetaUserService metaUserService;
    private final ModelMapper modelMapper;
    private final FCMService2 fcmService;

    public List<MetaPushRes> MetaPusSelectListService(MetaPushReq dto) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        logger.info("dto  - - - -{}", new Gson().toJson(dto));
        Map<Long, String> tt = adminRepository.findAll()
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));

        //logger.info("getSessionAdministrativeDivision - {}", dto.getSessionAdministrativeDivision());
        List<MetaPushRes> metaPushRes = metaPushRepository.SelectListMetaPush(dto)
                .stream().map(e -> modelMapper.map(e, MetaPushRes.class))
                .peek(f -> {
                    try {
                        f.setRegName(tt.get(f.getRegSeq()));
                        if (f.getSendType().equals("I")) {
                            f.setSendTypeName("즉시");
                        } else {
                            f.setSendTypeName("예약");
                        }

                        if (f.getResultType().equals("C")) {
                            f.setResultTypeName("전송");
                        } else {
                            f.setResultTypeName("대기");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                })
                .collect(Collectors.toList());
        return metaPushRes;
    }

    public Paging MetaPushSelectListPagingInfoService(MetaPushReq dto) {
        QueryResults<MetaPush> result = metaPushRepository.SelectListMetaPushPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    @Transactional
    public MetaPush MetaPushSaveService(MetaPushVO dto) throws FirebaseMessagingException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;

        LocalDateTime regDt = LocalDateTime.now();
        LocalDateTime sendDt = LocalDateTime.now();
        dto.setRegDt(regDt);
        dto.setSendDt(sendDt);

        // 푸쉬 실제 보내기 ////////////////////////////////////////////////////////////////////
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setTitle(dto.getTitle());
        requestDTO.setBody(dto.getBody());
        Map<String, Object> res = fcmService.multipleSendToToken(requestDTO);

        // 푸쉬 결과 저장하기 ////////////////////////////////////////////////////////////////////
        String resultString = (String) res.get("result");
        List<MetaPushLogVO> resultLogList = (List<MetaPushLogVO>) res.get("metaPushLogVOList");
        int cnt = resultLogList.size();
        dto.setSendCnt(cnt);
        dto.setResultType("C");
        dto.setRegSeq(admin.getAdminSeq());

        MetaPush metaPush = metaPushRepository.save(dto.ofMetaPush());

        //
        for (MetaPushLogVO resultLog : resultLogList) {
            //로그 저장
            MetaPushLog log = new MetaPushLog();
            MetaUser u = metaUserRepository.findByDeviceToken(resultLog.getDeviceToken()).orElseThrow(IllegalArgumentException::new);
            log.setSendSeq(metaPush.getSendSeq());
            log.setUserId(u.getUserId());
            log.setDeviceToken(resultLog.getDeviceToken());
            log.setResultCode(resultLog.getResultCode());
            log.setSendDt(resultLog.getSendDt());
            metaPushLogRepository.save(log);
        }

        return metaPush;
    }

    @Transactional
    public MetaPush MetaPushReservationService(MetaPushVO dto) throws FirebaseMessagingException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;

        String date = dto.getSdate() + "T" + metaUserService.zeroPlus(Integer.parseInt(dto.getShour())) + ":" +  metaUserService.zeroPlus(Integer.parseInt(dto.getSminite())) + ":00";
        LocalDateTime sendDt = LocalDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME);
        LocalDateTime regDt = LocalDateTime.now();

        dto.setRegDt(regDt);
        dto.setSendDt(sendDt);
        dto.setResultType("W");
        dto.setSendCnt(0);
        dto.setRegSeq(admin.getAdminSeq());

        MetaPush metaPush = metaPushRepository.save(dto.ofMetaPush());

        return metaPush;
    }

    public int DeleteMetaPushService(Long seq) {
        int result = metaPushRepository.deleteBySendSeq(seq);
        return result;
    }

    public void MetaPusSelectTimeListService() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Map<Long, String> tt = adminRepository.findAll()
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));

        LocalDateTime sdate = LocalDateTime.now().minusMinutes(3);
        LocalDateTime edate = LocalDateTime.now().plusMinutes(3);

        //logger.info("getSessionAdministrativeDivision - {}", dto.getSessionAdministrativeDivision());
       metaPushRepository.findByResultTypeAndSendDtBetween("W", sdate, edate)
                .stream().forEach(e -> {
                    try {
                        int sendCnt = fcmService.schMultipleSendToToken(e.getBody(), e.getTitle(), e.getSendDt());
                        metaPushRepository.UpdateResultType(e.getSendSeq());
                        metaPushRepository.UpdateSendCnt(sendCnt, e.getSendSeq());
                    } catch (FirebaseMessagingException ex) {
                        ex.printStackTrace();
                    }
                });
        //return metaPushRes;
    }

}
