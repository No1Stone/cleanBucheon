package com.bucheon.sch.biz.metabus.metaBoard.v1.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MetaUserVoteHistoryVO {

    private Long voteSeq;
    private String userId;
    private String voteType;
    private String boardType;
    private Long boardSeq;
    private LocalDateTime regDt;

}
