package com.bucheon.sch.biz.util.model;

import lombok.Data;

@Data
public class CommResponse {

    private String cdResult;
    private String msgResult;
}
