package com.bucheon.sch.biz.metabus.metaPush.v1.model;

import com.bucheon.sch.db.entity.metabus.MetaPush;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetaPushVO {

    private Long sendSeq;
    private String sendType;
    private String resultType;
    private String title;
    private String body;
    private String sdate;
    private String shour;
    private String sminite;
    private int sendCnt;
    private Long regSeq;
    private LocalDateTime regDt;
    private LocalDateTime sendDt;

    public MetaPush ofMetaPush(){
        MetaPush m = new MetaPush();
        m.setSendSeq(this.sendSeq);
        m.setSendType(this.sendType);
        m.setResultType(this.resultType);
        m.setTitle(this.title);
        m.setBody(this.body);
        m.setSendCnt(this.sendCnt);
        m.setRegSeq(this.regSeq);
        m.setRegDt(this.regDt);
        m.setSendDt(this.sendDt);
        return m;
    }

}
