package com.bucheon.sch.biz.metabus.metaBoard.v1.model;

import com.bucheon.sch.db.entity.metabus.MetaBoard;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MetaBoardVO {

    private Long boardSeq;
    private String boardType;
    private String boardTitle;
    private String boardContent;
    private int vote1;
    private int vote2;
    private int vote3;
    private String sdateString;
    private String edateString;
    private LocalDateTime sdate;
    private LocalDateTime edate;
    private String useYn;
    private String delYn;
    private Long regSeq;
    private LocalDateTime regDt;
    private Long modSeq;
    private LocalDateTime modDt;

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public Long getBoardSeq() {
        return boardSeq;
    }

    public void setBoardSeq(Long boardSeq) {
        this.boardSeq = boardSeq;
    }

    public String getBoardType() {
        return boardType;
    }

    public void setBoardType(String boardType) {
        this.boardType = boardType;
    }

    public String getBoardTitle() {
        return boardTitle;
    }

    public void setBoardTitle(String boardTitle) {
        this.boardTitle = boardTitle;
    }

    public String getBoardContent() {
        return boardContent;
    }

    public void setBoardContent(String boardContent) {
        this.boardContent = boardContent;
    }

    public int getVote1() {
        return vote1;
    }

    public void setVote1(int vote1) {
        this.vote1 = vote1;
    }

    public int getVote2() {
        return vote2;
    }

    public void setVote2(int vote2) {
        this.vote2 = vote2;
    }

    public int getVote3() {
        return vote3;
    }

    public void setVote3(int vote3) {
        this.vote3 = vote3;
    }

    public String getSdateString() {
        return sdateString;
    }

    public void setSdateString(String sdateString) {
        this.sdateString = sdateString;
    }

    public String getEdateString() {
        return edateString;
    }

    public void setEdateString(String edateString) {
        this.edateString = edateString;
    }

    public LocalDateTime getSdate() {
        return sdate;
    }

    public void setSdate(LocalDateTime sdate) {
        this.sdate = sdate;
    }

    public LocalDateTime getEdate() {
        return edate;
    }

    public void setEdate(LocalDateTime edate) {
        this.edate = edate;
    }

    public Long getRegSeq() {
        return regSeq;
    }

    public void setRegSeq(Long regSeq) {
        this.regSeq = regSeq;
    }

    public LocalDateTime getRegDt() {
        return regDt;
    }

    public void setRegDt(LocalDateTime regDt) {
        this.regDt = regDt;
    }

    public Long getModSeq() {
        return modSeq;
    }

    public void setModSeq(Long modSeq) {
        this.modSeq = modSeq;
    }

    public LocalDateTime getModDt() {
        return modDt;
    }

    public void setModDt(LocalDateTime modDt) {
        this.modDt = modDt;
    }

    public MetaBoard ofMetaBoard(){
        MetaBoard m = new MetaBoard();

        if (this.boardSeq!=null) {
            m.setBoardSeq(this.boardSeq);
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        String sdate2 = this.sdateString + " 00:00:00.000";
        String edate2 = this.edateString + " 23:59:59.999";
        LocalDateTime sdate3 = LocalDateTime.parse(sdate2, formatter);
        LocalDateTime edate3 = LocalDateTime.parse(edate2, formatter);

        m.setBoardType(this.boardType.replaceAll(",",""));
        m.setBoardTitle(this.boardTitle);
        m.setBoardContent(this.boardContent);
        m.setVote1(0);
        m.setVote2(0);
        m.setVote3(0);
        m.setSdate(sdate3);
        m.setEdate(edate3);
        m.setUseYn(this.useYn);
        m.setDelYn(this.delYn);
        m.setRegSeq(this.regSeq);
        m.setRegDt(LocalDateTime.now());
        m.setModSeq(this.modSeq);
        m.setModDt(LocalDateTime.now());
        return m;
    }

    public MetaBoardVO ofMetaBoardVO(){
        MetaBoardVO m = new MetaBoardVO();

        if (this.boardSeq!=null) {
            m.setBoardSeq(this.boardSeq);
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        String sdate2 = this.sdateString + " 00:00:00.000";
        String edate2 = this.edateString + " 23:59:59.999";
        LocalDateTime sdate3 = LocalDateTime.parse(sdate2, formatter);
        LocalDateTime edate3 = LocalDateTime.parse(edate2, formatter);

        m.setBoardType(this.boardType.replaceAll(",",""));
        m.setBoardTitle(this.boardTitle);
        m.setBoardContent(this.boardContent);
        m.setVote1(0);
        m.setVote2(0);
        m.setVote3(0);
        m.setSdate(sdate3);
        m.setEdate(edate3);
        m.setUseYn(this.useYn);
        m.setDelYn(this.delYn);
        m.setRegSeq(this.regSeq);
        m.setRegDt(LocalDateTime.now());
        m.setModSeq(this.modSeq);
        m.setModDt(LocalDateTime.now());
        return m;
    }

}
