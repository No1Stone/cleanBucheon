package com.bucheon.sch.biz.environmental.campaign.v1.service;

import com.bucheon.sch.biz.environmental.campaign.v1.model.CampaignExcelDto;
import com.bucheon.sch.biz.environmental.campaign.v1.model.CampaignReq;
import com.bucheon.sch.biz.environmental.campaign.v1.model.CampaignRes;
import com.bucheon.sch.biz.environmental.campaign.v1.model.CampaignVO;
import com.bucheon.sch.biz.environmental.report.v1.band.BandService;
import com.bucheon.sch.biz.operation.mileage.v1.model.UserMileageHistoryVO;
import com.bucheon.sch.biz.operation.mileage.v1.service.UserMileageService;
import com.bucheon.sch.common.type.CampaignStatus;
import com.bucheon.sch.common.type.PaymentType;
import com.bucheon.sch.common.type.ProcessType;
import com.bucheon.sch.common.type.YnType;
import com.bucheon.sch.common.util.ExcelWriter;
import com.bucheon.sch.common.util.Masking;
import com.bucheon.sch.common.util.Paging;
import com.bucheon.sch.db.entity.environmental.Campaign;
import com.bucheon.sch.db.entity.environmental.CampaignLog;
import com.bucheon.sch.db.entity.operation.Admin;
import com.bucheon.sch.db.entity.operation.TrashType;
import com.bucheon.sch.db.entity.operation.User;
import com.bucheon.sch.db.repository.environmental.*;
import com.bucheon.sch.db.repository.etc.DongNameRepository;
import com.bucheon.sch.db.repository.operation.TrashTypeRepository;
import com.bucheon.sch.db.repository.operation.UserRepository;
import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CampaginService {

    private final Logger logger = LoggerFactory.getLogger(CampaginService.class);
    private final HttpServletRequest httpServletRequest;
    private final CampaignRepository campaignRepository;
    private final CampaignImgRepository campaignImgRepository;
    private final CampaignLogRepository campaignLogRepository;
    private final CampaignViewRepository campaignViewRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final TrashTypeRepository trashTypeRepository;
    private final BandService bandService;
    private final PetitionRepository petitionRepository;
    private final UserMileageService userMileageService;
    private final DongNameRepository dongNameRepository;


    public List<CampaignRes> CampaginSelectListService(CampaignReq dto) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        logger.info("dto  - - - -{}", new Gson().toJson(dto));
        Map<Long, String> tt = trashTypeRepository.findAll()
                .stream().collect(Collectors.toMap(TrashType::getTrashSeq, TrashType::getTrashName));
        logger.info("ttmap - {}", tt);

        List<CampaignRes> campaignRes = campaignRepository.SelectListCampaign(dto)
                .stream().map(e -> modelMapper.map(e, CampaignRes.class))
                .peek(f -> {
                    try {
                        f.setUserName(Masking.nameMasking(f.getUserName()));
                        f.setUserPhone(Masking.nameMasking(f.getUserPhone()));
                        f.setUserEmail(Masking.emailMasking(f.getUserEmail()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                })
                .collect(Collectors.toList());
        return campaignRes;
    }

    public Paging CampaginSelectListPagingInfoService(CampaignReq dto) {
        QueryResults<CampaignRes> result = campaignRepository.SelectListCampaignPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public CampaignRes CampaignSelectOneService(Long seq) {
        Map<Long, String> tt = trashTypeRepository.findAll()
                .stream().collect(Collectors.toMap(TrashType::getTrashSeq, TrashType::getTrashName));
        CampaignRes campaignResult = modelMapper.map(campaignRepository.findByCampaignSeqAndDelYnOrderByRegDtDesc(seq, "N").orElseThrow(IllegalArgumentException::new), CampaignRes.class);
        campaignResult.setTrashTypeName(tt.get(campaignResult.getTrashType()));
        campaignResult.setCampaignProcessName(ProcessType.valueOf(campaignResult.getCampaignProcess()).getDescription());
        campaignResult.setCampaignStatusName(CampaignStatus.valueOf(campaignResult.getCampaignStatus()).getDescription());
        if (campaignImgRepository.existsByCampaignSeq(campaignResult.getCampaignSeq())) {
            campaignResult.setCampaignImgs(campaignImgRepository
                    .findByCampaignSeq(campaignResult.getCampaignSeq()).stream().map(e -> e.getCampaignImg()).collect(Collectors.toList()));
        }
        try {
            User u = userRepository.findByDid(campaignResult.getRegUserDid()).orElseThrow(IllegalArgumentException::new);

            campaignResult.setUserName(Masking.nameMasking(u.getName()));
            campaignResult.setUserPhone(Masking.phoneMasking(u.getPhone()));
            campaignResult.setUserEmail(Masking.emailMasking(u.getEmail()));
        } catch (Exception e) {
            //아무것도 안함
        }
        return campaignResult;
    }

    //
    public CampaignRes CampaignSelectOneService2(Long seq) {
        Map<Long, String> tt = trashTypeRepository.findAll()
                .stream().collect(Collectors.toMap(TrashType::getTrashSeq, TrashType::getTrashName));
        CampaignRes campaignResult = modelMapper.map(campaignRepository.findByCampaignSeqAndDelYnOrderByRegDtDesc(seq, "N").orElseThrow(IllegalArgumentException::new), CampaignRes.class);
        campaignResult.setTrashTypeName(tt.get(campaignResult.getTrashType()));
        campaignResult.setCampaignProcessName(ProcessType.valueOf(campaignResult.getCampaignProcess()).getDescription());
        campaignResult.setCampaignStatusName(CampaignStatus.valueOf(campaignResult.getCampaignStatus()).getDescription());
        if (campaignImgRepository.existsByCampaignSeq(campaignResult.getCampaignSeq())) {
            campaignResult.setCampaignImgs(campaignImgRepository
                    .findByCampaignSeq(campaignResult.getCampaignSeq()).stream().map(e -> e.getCampaignImg()).collect(Collectors.toList()));
        }
        try {
            User u = userRepository.findByDid(campaignResult.getRegUserDid()).orElseThrow(IllegalArgumentException::new);
        } catch (Exception e) {
            //아무것도 안함
        }
        return campaignResult;
    }

    @Transactional
    public int UpdateCampaign(CampaignVO dto) {

        //로그 저장때문에 필요
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;
        String msg = "";

        Campaign campaign = campaignRepository.findByCampaignSeq(dto.getCampaignSeq());

        LocalDateTime comDt = LocalDateTime.now();
        dto.setComDt(comDt);


        int result = 0;

        try {
            //내용만 수정
            if (dto.getCampaignProcess() != null && !campaign.getCampaignProcess().equals(ProcessType.C.getName()) && !campaign.getCampaignProcess().equals(ProcessType.R.getName())) {
                if (!dto.getCampaignProcess().equals("") && dto.getCampaignProcess() != null) {
                    campaignRepository.UpdateCampaignProcess(dto.getCampaignProcess(), dto.getCampaignSeq());
                    //처리불가면 마일리지도 지급불가 처리
                    if (dto.getCampaignProcess().equals(ProcessType.R.getName())) {
                        campaignRepository.UpdateMileageStatus(PaymentType.R.getName(), dto.getCampaignSeq());
                    }
                }
                if (!dto.getCampaignProcessRejectReason().equals("") && dto.getCampaignProcessRejectReason() != null) {
                    campaignRepository.UpdateCampaignProcessRejectReason(dto.getCampaignProcessRejectReason(), dto.getCampaignSeq());
                }
                if (dto.getCampaignProcess().equals(ProcessType.C.getName()) || dto.getCampaignProcess().equals(ProcessType.R.getName())) {
                    campaignRepository.UpdateComDt(dto.getComDt(), dto.getCampaignSeq());
                }

                //로그 저장
                if (dto.getCampaignProcess().equals(ProcessType.D.getName())) {
                    msg = "접수하였습니다";
                } else if (dto.getCampaignProcess().equals(ProcessType.C.getName())) {
                    msg = "담당자 " + admin.getAdminName() + "님께서 처리완료로 변경하였습니다.";
                } else if (dto.getCampaignProcess().equals(ProcessType.R.getName())) {
                    msg = "담당자 " + admin.getAdminName() + "님께서 처리불가로 변경하였습니다.";
                }

                CampaignLog r = new CampaignLog();
                r.setCampaignSeq(dto.getCampaignSeq());
                r.setLogTime(LocalDateTime.now());
                r.setCampaignNum(campaign.getCampaignNum());
                r.setCampaignStatus(dto.getCampaignStatus());
                r.setCampaignDamdang(admin.getAdminSeq());
                r.setMsg(msg);
                r.setRejectReason(dto.getCampaignProcessRejectReason());

                CampaignLog campaignLogResult = campaignLogRepository.save(r);


                result++;
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        try {
            //마일리지만 수정
            if (
                    (campaign.getCampaignProcess().equals(ProcessType.C.getName()) || campaign.getCampaignProcess().equals(ProcessType.R.getName())) &&
                    (campaign.getMileageStatus().equals(PaymentType.W.getName()))
            ) {
                CampaignRes campaignRes = this.CampaignSelectOneService(dto.getCampaignSeq());
                campaignRepository.UpdateMileage(Integer.valueOf(dto.getMileage()), dto.getCampaignSeq());
                campaignRepository.UpdateMileageRejectReason(dto.getMileageRejectReason(), dto.getCampaignSeq());
                campaignRepository.UpdateMileageStatus(dto.getMileageStatus(), dto.getCampaignSeq());

                //추가 마일리지 적용 업데이트
                if (dto.getRegUserDid() != null && !dto.getRegUserDid().equals("")) {

                    String reason = (dto.getMileageStatus().equals("C")) ? "캠페인 참여 지급" : dto.getMileageRejectReason();

                    UserMileageHistoryVO userMileageHistoryVO = new UserMileageHistoryVO();
                    userMileageHistoryVO.setUserId(dto.getRegUserDid());
                    userMileageHistoryVO.setReportSeq(dto.getCampaignSeq());
                    userMileageHistoryVO.setMileage(Integer.valueOf(dto.getMileage()));
                    userMileageHistoryVO.setMileageType("campaign");
                    userMileageHistoryVO.setReason(reason);
                    userMileageHistoryVO.setComplaintsDt(campaign.getComDt());
                    userMileageHistoryVO.setSendDate(LocalDateTime.now());

                    userMileageService.UserMileageSaveService(userMileageHistoryVO);

                    //로그 저장
                    if (dto.getMileageStatus().equals(PaymentType.W.getName())) {
                        msg = "담당자 " + admin.getAdminName() + "님께서 현재상태를 저장하였습니다";
                    } else if (dto.getMileageStatus().equals(PaymentType.C.getName())) {
                        msg = "담당자 " + admin.getAdminName() + "님께서 마일리지를 지급하였습니다.";
                    } else if (dto.getMileageStatus().equals(ProcessType.R.getName())) {
                        msg = "담당자 " + admin.getAdminName() + "님께서 마일리지를 지급불가로 변경하였습니다.";
                    }

                    int cnt = campaignLogRepository.countByCampaignSeq(dto.getCampaignSeq());
                    String rn = String.valueOf(Timestamp.valueOf(LocalDateTime.now()).getTime());
                    if (cnt > 0) {
                        CampaignLog campaignLog = campaignLogRepository.findTop1ByCampaignSeqOrderByLogSeqDesc(dto.getCampaignSeq()).orElseThrow(IllegalArgumentException::new);
                        rn = campaignLog.getCampaignNum();
                    }
                    CampaignLog r = new CampaignLog();
                    r.setCampaignSeq(dto.getCampaignSeq());
                    r.setLogTime(LocalDateTime.now());
                    r.setCampaignNum(rn);
                    r.setCampaignStatus(campaignRes.getCampaignStatus());
                    r.setCampaignDamdang(admin.getAdminSeq());
                    r.setMsg(msg);
                    r.setRejectReason(dto.getMileageRejectReason());

                    CampaignLog campaignLogResult = campaignLogRepository.save(r);
                }

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }

    public int UpdateCampaignCompleteService(Long[] seql) {

        List<Integer> result = Arrays.stream(seql).map(
                e -> this.UpdateAllComplete(e)).collect(Collectors.toList());
        return result.size();
    }

    public int UpdateAllComplete(Long seq) {

        Campaign campaign = campaignRepository.findByCampaignSeq(seq);

        CampaignVO dto = new CampaignVO();
        dto.setCampaignSeq(campaign.getCampaignSeq());
        dto.setCampaignProcess(ProcessType.C.getName());
        dto.setCampaignProcessRejectReason("");

        logger.info(">>>>>>>> dto.setReportSeq(seq) : {}", dto.getCampaignSeq());

        return UpdateCampaign(dto);
    }

    public int UpdateCampaignDelService(Long[] seql) {
        List<Integer> result = Arrays.stream(seql).map(e -> campaignRepository
                .UpdateCampaignDel(YnType.Y.getName(), e)).collect(Collectors.toList());
        return result.size();
    }


    public List<CampaignLog> CampaignLogSelectService(Long Seq) {
        return campaignLogRepository.findByCampaignSeqOrderByLogSeqDesc(Seq);
    }


    public List<CampaignExcelDto> getExcelList(CampaignReq dto) {

        List<CampaignExcelDto> getExcelList = campaignViewRepository.findExcelList(dto);

        logger.info("getExcelList - {} ", getExcelList);

        return getExcelList;
    }

    //엑셀 다운로드 요청
    public Map<String, Object> downExcelList(CampaignReq dto) throws Exception {
        // 데이터 가져오기
        List<CampaignExcelDto> excelList = this.getExcelList(dto);

        logger.info("excelList - {} ", excelList);

        return ExcelWriter.createExcelData(excelList, CampaignExcelDto.class, "캠페인관리");
    }

    public List<String> SelectBubDongName() {
        return dongNameRepository.selectBubDongName();
    }

}
