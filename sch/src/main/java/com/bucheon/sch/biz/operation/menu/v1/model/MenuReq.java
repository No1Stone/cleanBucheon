package com.bucheon.sch.biz.operation.menu.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MenuReq {

    private int page;
    private int size;
    private Long menuSeq;
    private String menuName;
    private int menuLevel;
    private Long pmenuSeq;
    private String menuUrl;
    private String useYn;
    private int orderNum;

}
