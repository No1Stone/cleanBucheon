package com.bucheon.sch.biz.operation.menu.v1.service;

import com.bucheon.sch.biz.operation.menu.v1.model.MenuReq;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuUpdate;
import com.bucheon.sch.biz.util.service.UtilService;
import com.bucheon.sch.common.type.YnType;
import com.bucheon.sch.common.util.Paging;
import com.bucheon.sch.db.entity.operation.Admin;
import com.bucheon.sch.db.entity.operation.Menu;
import com.bucheon.sch.db.repository.operation.MenuRepository;
import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MenuService {

    private final Logger logger = LoggerFactory.getLogger(MenuService.class);
    private final HttpServletRequest httpServletRequest;
    private final MenuRepository menuRepository;
    private final ModelMapper modelmapper;
    private final UtilService utilService;

    public Object MenuSelectListService(MenuReq dto) {
        Map<Long, String> menu = menuRepository.findAll()
                .stream().collect(Collectors.toMap(Menu::getMenuSeq, Menu::getMenuName));
        var result = menuRepository.menuListSelect(dto).stream().map(e -> modelmapper.map(e, MenuRes.class))
                .peek(f -> {
                    f.setPmenuName(menu.get(f.getPmenuSeq()));
                    f.setUseYnName(YnType.valueOf(f.getUseYn()).getVal());
                })
                .collect(Collectors.toList());
        return result;
    }

    public Object PmenuSelectListService(MenuReq dto) {
        Map<Long, String> menu = menuRepository.findAll()
                .stream().collect(Collectors.toMap(Menu::getMenuSeq, Menu::getMenuName));
        var result = menuRepository.pmenuListSelect(dto).stream().map(e -> modelmapper.map(e, MenuRes.class))
                .peek(f -> {
                    f.setPmenuName(menu.get(f.getPmenuSeq()));
                    f.setUseYnName(YnType.valueOf(f.getUseYn()).getVal());
                })
                .collect(Collectors.toList());
        return result;
    }

    public Object CmenuSelectListService(MenuReq dto) {

        Map<Long, String> menu = menuRepository.findAll()
                .stream().collect(Collectors.toMap(Menu::getMenuSeq, Menu::getMenuName));
        var result = menuRepository.cmenuListSelect(dto).stream().map(e -> modelmapper.map(e, MenuRes.class))
                .peek(f -> {
                    f.setPmenuName(menu.get(f.getPmenuSeq()));
                    f.setUseYnName(YnType.valueOf(f.getUseYn()).getVal());
                })
                .collect(Collectors.toList());
        return result;
    }

    public Object UseMenuSelectListService(MenuReq dto) {
        Map<Long, String> menu = menuRepository.findAll()
                .stream().collect(Collectors.toMap(Menu::getMenuSeq, Menu::getMenuName));
        var result = menuRepository.menuUseListSelect(dto).stream().map(e -> modelmapper.map(e, MenuRes.class))
                .peek(f -> {
                    f.setPmenuName(menu.get(f.getPmenuSeq()));
                    f.setUseYnName(YnType.valueOf(f.getUseYn()).getVal());
                })
                .collect(Collectors.toList());
        return result;
    }

    public Paging MenuSelectListPagingInfoService(MenuReq dto) {
        QueryResults<Menu> result = menuRepository.SelectListMenuPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public MenuRes MenuSelectOneService(Long seq) {
        MenuRes menuResult = modelmapper.map(menuRepository.findByMenuSeqAndDelYnOrderByRegDtDesc(seq, "N")
                .orElseThrow(IllegalArgumentException::new), MenuRes.class);
//        reportResult.setReportStatusName(ProcessType.valueOf(reportResult.getReportStatus()).getDescription());
//        reportResult.setReportReceptionName(ReceptionType.valueOf(reportResult.getReportReception()).getDescription());


//                .peek(f -> {
//                    f.setReportTypeName(tt.get(f.getReportType()));
//                    f.setReportStatusName(ProcessType.valueOf(f.getReportStatus()).getDescription());
//                    f.setReportReceptionName(ReceptionType.valueOf(f.getReportReception()).getDescription());
//                })
        return menuResult;
    }

    @Transactional
    public String MenuSaveService(MenuUpdate dto) throws IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;

        int count = 0;
        int up = 0;

        logger.info(" menudto - - - -{}", new Gson().toJson(dto));

        if (dto.getMenuSeq() == null) {
            /*
            MultipartFile[] mf = new MultipartFile[0];
            mf[0] = dto.getMenuIcon();
            List<String> list = utilService.FileSaveService(mf, "menu");
            logger.info("list Size -{}", list.size());
            logger.info("list get - {}", list.get(0));
            if (list.size() == 1) {
                Menu menu = menuRepository.save(dto.ofMenu(list.get(0)));
                if (menu.getMenuSeq() != null) {
                    count = 1;
                }
            }
            */

            dto.setRegSeq(admin.getAdminSeq());
            dto.setModSeq(admin.getAdminSeq());
            dto.setRegDt(LocalDateTime.now());
            dto.setModDt(LocalDateTime.now());

            Menu menu = menuRepository.save(dto.ofMenu(dto.getMenuIcon()));
            if (menu.getMenuSeq() != null) {
                count = 1;
            }
        } else {

            dto.setModSeq(admin.getAdminSeq());

            if (!StringUtils.isEmptyOrWhitespace(dto.getMenuName())) {
                up = menuRepository.updateMenuName(dto.getMenuName(), dto.getMenuSeq());
                count += up;
            }
            if (dto.getMenuLevel() != 0) {
                up = menuRepository.updateMenuLevle(dto.getMenuLevel(), dto.getMenuSeq());
                count += up;
            }
            if (dto.getPmenuSeq() != null) {
                up = menuRepository.updateMenuPmenu(dto.getPmenuSeq(), dto.getMenuSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getMenuUrl())) {
                up = menuRepository.updateMenuURL(dto.getMenuUrl(), dto.getMenuSeq());
                count += up;
            }
            if (dto.getMenuIcon() != null) {
                /*
                MultipartFile[] mf = new MultipartFile[0];
                mf[0] = dto.getMenuIcon();
                List<String> list = utilService.FileSaveService(mf, "menu");
                logger.info("list Size -{}", list.size());
                logger.info("list get - {}", list.get(0));
                if (list.size() == 1) {
                    up = menuRepository.updateMenuIcon(list.get(0), dto.getMenuSeq());
                    count += up;
                }
                */
                up = menuRepository.updateMenuIcon(dto.getMenuUrl(), dto.getMenuSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getMenuDesc())) {
                up = menuRepository.updateMenuDesc(dto.getMenuDesc(), dto.getMenuSeq());
                count += up;
            }
            if (dto.getOrderNum() != 0) {
                up = menuRepository.updateMenuOrderNum(dto.getOrderNum(), dto.getMenuSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getUseYn())) {
                up = menuRepository.updateMenuUseYn(dto.getUseYn(), dto.getMenuSeq());
                count += up;
            }
            if (dto.getDelYn() != null) {
                for (Long e : dto.getDelYn()) {
                    up = menuRepository.updateMenuDelYn(YnType.Y.getName(), e);
                }
                count += up;
            }

            if (count > 0) {
                menuRepository.updateMod(dto.getModSeq(), dto.getMenuSeq());
            }
        }
        if (count > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    public int UpdateMenuDelService(Long[] menuSeql) {
        List<Integer> result = Arrays.stream(menuSeql).map(e -> menuRepository
                .updateMenuDelYn(YnType.Y.getName(), e)).collect(Collectors.toList());
        return result.size();
    }

    public List<String> UploadService(MultipartFile[] uploadfile) throws IOException {
        List<String> uploadFilename = utilService.FileSaveService(uploadfile, "menu");
        return uploadFilename;
    }
}
