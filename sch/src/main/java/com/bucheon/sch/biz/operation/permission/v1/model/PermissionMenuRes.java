package com.bucheon.sch.biz.operation.permission.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PermissionMenuRes {

    private Long permissionSeq;
    private Long menuSeq;
    private String useYn;

}
