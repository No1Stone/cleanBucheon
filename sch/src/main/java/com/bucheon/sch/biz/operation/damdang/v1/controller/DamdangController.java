package com.bucheon.sch.biz.operation.damdang.v1.controller;

import com.bucheon.sch.biz.operation.admin.v1.model.AdminReq;
import com.bucheon.sch.biz.operation.admin.v1.service.AdminService;
import com.bucheon.sch.biz.operation.damdang.v1.model.DamdangReq;
import com.bucheon.sch.biz.operation.damdang.v1.service.DamdangService;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.sch.biz.operation.menu.v1.service.MenuService;
import com.bucheon.sch.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
@RequestMapping(path = "/operation/damdang")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DamdangController {

    private final Logger logger = LoggerFactory.getLogger(DamdangController.class);
    private final AdminService adminService;
    private final MenuService menuService;
    private final DamdangService damdangService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/operation/damdang/damdangManage");
    }

    //기본틀
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(AdminReq dto, HttpServletRequest request) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(32L);

        return new ModelAndView("operation/damdang/damdangManage")
                .addObject("damdangList", damdangService.DamdangSelectListService())
                .addObject("adminList", adminService.AdminSelectAllListService())
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(DamdangReq dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result",   damdangService.DamdangUpdateAndSave(dto));
        return map;
    }

}
