package com.bucheon.sch.biz.operation.personalInfo.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonalInfoLogVO {

	private Long logSeq;
	private LocalDateTime logTime;
	private Long adminSeq;
	private String adminName;
	private String menuName;
	private String menuHanName;
	private String contents;
	private Long seq;
	private String did;
	private String reason;

}
