package com.bucheon.sch.biz.environmental.complaint.v1.service;

import com.bucheon.sch.biz.environmental.complaint.v1.model.ComplaintsReq;
import com.bucheon.sch.biz.environmental.complaint.v1.model.ComplaintsRes;
import com.bucheon.sch.common.util.Paging;
import com.bucheon.sch.db.entity.environmental.Petition;
import com.bucheon.sch.db.entity.environmental.Process;
import com.bucheon.sch.db.entity.environmental.Receipt;
import com.bucheon.sch.db.repository.environmental.PetitionImageRepository;
import com.bucheon.sch.db.repository.environmental.PetitionRepository;
import com.bucheon.sch.db.repository.environmental.ProcessRepository;
import com.bucheon.sch.db.repository.environmental.ReceiptRepository;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ComplaintService {

    private final Logger logger = LoggerFactory.getLogger(ComplaintService.class);
    private final HttpServletRequest httpServletRequest;
    private final PetitionRepository petitionRepository;
    private final PetitionImageRepository petitionImageRepository;
    private final ProcessRepository processRepository;
    private final ReceiptRepository receiptRepository;
    private final ModelMapper modelMapper;

    public Object ReportSelectListInfoService(ComplaintsReq dto) {
        List<Petition> li = petitionRepository.DynamicSelect(dto);
        List<ComplaintsRes> petiResult = petitionRepository.DynamicSelect(dto).stream().map(e -> {

                   return ComplaintsRes.builder()
                            .petiNo(e.getPetiNo())
                            .petiTitle(e.getTitle())
                            .petiContent(e.getReason())
                            .busiBranCodeName(e.getReceipts().getBusiBranCodeName())
                            .petiStatus(e.getProcesses().stream().map(f -> f.getStatusName()).collect(Collectors.toList()))
                            .regDtTime(e.getRegDateTime())
                            .ancName(e.getReceipts().getAncName())
                            .receiptDtTime(e.getReceipts().getRegDateTime())
                            //.dutyCode(e.getProcesses().get(0).getDutyDeptCode())
                            .dutyName(e.getProcesses().get(0).getDutyName())
                            //.comDtTime(LocalDateTime.parse(e.getProcesses().get(0).getDoDateTime(), DateTimeFormatter.ofPattern("yyyyMMddHHmmss")))
                            .comDtTime(e.getProcesses().get(0).getDoDateTime())
                            .build();
                })
                .collect(Collectors.toList());


        logger.info("======================================");
        return petiResult;
    }

    public Paging ReportSelectListPagingInfoService(ComplaintsReq dto) {
        QueryResults<Petition> result = petitionRepository.DynamicSelectPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public Petition ReportSelectOneService(String petiNo) throws Exception {

        Optional<Petition> petition = petitionRepository.findById(petiNo);
//        petition.get().setName(Masking.nameMasking(petition.get().getName()));
//        petition.get().setCellPhone(Masking.nameMasking(petition.get().getCellPhone()));

        return petition.orElseThrow(IllegalArgumentException::new);
    }


    public List<Process> getTest2() {
        return processRepository.findAll();
    }

    public List<Receipt> getTest3() {
        return receiptRepository.findAll();
    }
}
