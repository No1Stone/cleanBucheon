package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.Data;

import java.util.Map;

@Data
public class ListCardItems {

	private String title;
	
	private String description;
	
	private String imageUrl;
	
	private Map<String,Object> link;
	
	//private int lcSeq;
	
}
