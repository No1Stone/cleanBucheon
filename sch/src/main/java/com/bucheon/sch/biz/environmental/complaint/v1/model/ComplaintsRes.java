package com.bucheon.sch.biz.environmental.complaint.v1.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ComplaintsRes {

    private String petiNo;
    // 제목
    private String petiTitle;
    //내용
    private String petiContent;
    //유형 트래쉬타입
    private String petiType;
    //진행상태
    private List<String> petiStatus;
    //등록일
    private String regDtTime;



    ////추가 여기부터
    //접수자
    private String name;
    //접수자 연락처
    private String cellPhone;
    //접수자 주소
    private String zipCode;
    //접수자 주소
    private String address1;
    //접수자 주소
    private String address2;
    //접수자
    private String ancName;
    //접수사진
    private String petitionImgs;

    ////추가 여기까지



    // 접수쪽 /////////////////////////
    //
    private String busiBranCodeName;
    //접수일
    private String receiptDtTime;


    // 처리쪽 /////////////////////////
    //담당과
    private String dutyCode;
    //현재 코드데이터 없음.
    private String dutyCodeName;
    //담당이름
    private String dutyName;
    //처리완료일
    private LocalDateTime comDt;
    //등록일
    private LocalDateTime regDt;
    //처리일
    private String comDtTime;

}
