package com.bucheon.sch.biz.util.service;

import com.bucheon.sch.biz.metabus.metaPush.v1.model.MetaPushLogVO;
import com.bucheon.sch.biz.metabus.metaUser.v1.service.MetaUserService;
import com.bucheon.sch.biz.util.model.RequestDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class FCMService2 {

    private final Logger logger = LoggerFactory.getLogger(FCMService2.class);
    private final MetaUserService metaUserService;

    @Value("${project.properties.firebase-create-scoped}")
    String fireBaseCreateScoped;

    @Value("${project.properties.firebase-topic}")
    String topic;

    private FirebaseMessaging instance;


    private String API_URL = "https://fcm.googleapis.com/v1/projects/bucheon-meta/messages:send";
    private final ObjectMapper objectMapper;

    @PostConstruct
    public void firebaseSetting() throws IOException {
        GoogleCredentials googleCredentials = GoogleCredentials.fromStream(new ClassPathResource("firebase/firebase_service_key.json").getInputStream())
                .createScoped((Arrays.asList(fireBaseCreateScoped)));
        FirebaseOptions secondaryAppConfig = FirebaseOptions.builder()
                .setCredentials(googleCredentials)
                .build();
        FirebaseApp app = FirebaseApp.initializeApp(secondaryAppConfig);
        this.instance = FirebaseMessaging.getInstance(app);
    }



    public void pushAlarm(RequestDTO data) throws FirebaseMessagingException {
        Message message = getMessage(data);
        sendMessage(message);
    }

    private Message getMessage(RequestDTO data) {
        Notification notification = Notification.builder().setTitle(data.getTitle()).setBody(data.getBody()).build();
        Message.Builder builder = Message.builder();
        Message message = builder.setToken(data.getTargetToken()).setNotification(notification).build();
        return message;
    }

    public void pushAlarm2(RequestDTO data) throws FirebaseMessagingException {
        List<Message> messages = Arrays.asList(
                Message.builder()
                        .setNotification(Notification.builder()
                                        .setTitle(data.getTitle())
                                        .setBody(data.getBody()).build())
                                        .setToken(data.getTargetToken())
                                        .build(),
                                Message.builder()
                                        .setNotification(Notification.builder()
                                                .setTitle(data.getTitle())
                                                .setBody(data.getBody()).build())
                                        .setTopic(topic)
                                        .build()
                        );

        BatchResponse response = FirebaseMessaging.getInstance()
                .sendAll(messages);
        System.out.println(response.getSuccessCount() +
                " messages were sent successfully");
    }

    public Map<String , Object> multipleSendToToken(RequestDTO data)
            throws FirebaseMessagingException {
        Map<String, Object> result = new HashMap<>();
        List<String> tokenList2 = metaUserService.MetaUserSelectDeviceTokenHasListService().stream().map(e -> e.getDeviceToken()).collect(Collectors.toList());


        MulticastMessage message = MulticastMessage.builder()
                .setNotification(Notification.builder()
                        .setTitle(data.getTitle())
                        .setBody(data.getBody())
                        .build())
                .addAllTokens(tokenList2)
                .build();


        BatchResponse response = FirebaseMessaging.getInstance()
                .sendMulticast(message);

        List<SendResponse> responses = response.getResponses();
        List<String> failedTokens = new ArrayList<>();
        List<MetaPushLogVO> metaPushLog = new ArrayList<>();
        for (int i = 0; i < responses.size(); i++) {
            MetaPushLogVO m = new MetaPushLogVO();

            if (!responses.get(i).isSuccessful()) {
                failedTokens.add(tokenList2.get(i));

                m.setDeviceToken(tokenList2.get(i));
                m.setResultCode("F");
                metaPushLog.add(m);
            } else {
                m.setDeviceToken(tokenList2.get(i));
                m.setResultCode("S");
                metaPushLog.add(m);
            }
            result.put("metaPushLogVOList", metaPushLog);
        }

        if (response.getFailureCount() > 0) {
            result.put("result", "fail");

            System.out.println("List of tokens that caused failures: "
                    + failedTokens);
        } else {
            result.put("result", "suceess");
        }

        return result;
    }

    public int schMultipleSendToToken(String body, String title, LocalDateTime sendDt)
            throws FirebaseMessagingException {
//        List<Map<String, Object>> resultList = new ArrayList<>();
//        Map<String, Object> result = new HashMap<>();

            int cnt = 0;

            List<String> tokenList2 = metaUserService.MetaUserSelectDeviceTokenHasListService().stream().map(e -> e.getDeviceToken()).collect(Collectors.toList());


            MulticastMessage message = MulticastMessage.builder()
                    .setNotification(Notification.builder()
                            .setTitle(title)
                            .setBody(body)
                            .build())
                    .addAllTokens(tokenList2)
                    .build();


            BatchResponse response = FirebaseMessaging.getInstance()
                    .sendMulticast(message);

            List<SendResponse> responses = response.getResponses();
            List<String> failedTokens = new ArrayList<>();
            List<MetaPushLogVO> metaPushLog = new ArrayList<>();
            for (int i = 0; i < responses.size(); i++) {
                MetaPushLogVO m = new MetaPushLogVO();

                if (!responses.get(i).isSuccessful()) {
                    failedTokens.add(tokenList2.get(i));

                    m.setDeviceToken(tokenList2.get(i));
                    m.setResultCode("F");
                    m.setSendDt(sendDt);
                    metaPushLog.add(m);
                } else {
                    m.setDeviceToken(tokenList2.get(i));
                    m.setResultCode("S");
                    m.setSendDt(sendDt);
                    metaPushLog.add(m);
                }
                //result.put("metaPushLogVOList", metaPushLog);
                cnt++;
            }

            if (response.getFailureCount() > 0) {
                //result.put("result", "fail");

                System.out.println("List of tokens that caused failures: "
                        + failedTokens);
            } else {
                //result.put("result", "suceess");
            }

            //resultList.add(result);


        return cnt;

    }


    public String sendMessage(Message message) throws FirebaseMessagingException {
        return this.instance.send(message);
    }

    private String getAccessToken() throws IOException {
        String firebaseConfigPath = "firebase/firebase_service_key.json";

        GoogleCredentials googleCredentials = GoogleCredentials
                .fromStream(new ClassPathResource(firebaseConfigPath).getInputStream())
                .createScoped(List.of("https://www.googleapis.com/auth/cloud-platform"));

        googleCredentials.refreshIfExpired();
        return googleCredentials.getAccessToken().getTokenValue();
    }

    public void makeGudok() throws FirebaseMessagingException {
        // These registration tokens come from the client FCM SDKs.


        List<String> registrationTokens = Arrays.asList(


                "cAwku9p7QXCB3Ssm_1Me3L:APA91bEqEDIB0U4be3EOR_sK17wrjZ1ONk-Namka12GeRrgt7bnV7noRCyWEMKVKg6uZPRUu-AX88fs8lx2pDN6_xKr1jag8CqktcmuA51LKOzF7hxSjf7foAdIVU6mhfX0mGlSlnNhV",
                "czhZIm2eSBePhLHU5yc8I_:APA91bFCMeHkAGERU4UOAulax9k63TQJ1jnGfKcgo62P5PASQt3DkMzlIp_6DF9WekrFkKILk-qL8tUaKLYm0_KJcSYt8tabxgEap-7EUN33VQg11fpefby8cG4eHYvDGLDtT-UMDRto",
                "dP9cmB-TQ1-1P8wGiL-Vio:APA91bEizoGhItgQuJsap50Xy03Pgvakt41XWGiRMVfMXtzd44r2A4cFJDdXE9pQuGRKyvpcZpTEvwedG7JlquIYuwK0IIyal8iqrme7Y7FY7JhqUR8lgxw-czUJL_Z4x8VNOzRS0_8S",
                "dL1hgMjYTiOHzI6ZqT9hah:APA91bG2ALVNPycFF4jzFFoipVxmntrdsf1q6in8IwjIpIMlNnEC4sjf1GiugEsBzoIU0Vbv6IJhVYsr5RzcJIDDsSjiyORXrXtmYRTKBtpq3n007751ByvBvs0XxLhggErhckUL6ExZ",
                "cGaUeZAyQta35gPUpRo9eE:APA91bFPAZBKfLN6ba_8SH90fK9PqG6ridDUpA9THx1SiP5PXcGwNU7pKR0chgEARLd4_jn-zWdUGnkcJeyxfvxhpji4zeaG4kbno222WobRADHOMfxHnn4UyX_aKxvrfPuX5H0Eohay",
                "cU3Ivq16TBO4HKNldUJAUx:APA91bEYRAKjlB8jmQa_5nmHGMrq78qExvslI5WOBAaFTydtnLoREQZ8OCEfM9owTsHJp-gVBkz_8C3wkRv-1kt0qcJw3sBE1KiZ1aPwWEqaMzAc1DoJy9COJIk0ftBDCAWlNptHhJjl",
                "frV1aLuzQ469tZBEdRD6OD:APA91bHzx_GfouAIfSUDGkjRwiRboPkoP4-R-FTFNe5CCeF2WIS3Rfxkm89K0WvDiM3VIPxGAY7MN-MYuJXwjOrtZgMwZORzCejDDYZLFgvEDQ4vLw4gV7OpobOSNGaFnCCWJYFVHfEO"
        );

            // Subscribe the devices corresponding to the registration tokens to the
            // topic.
        TopicManagementResponse response = instance.getInstance().subscribeToTopic(
                registrationTokens, "news");
        // See the TopicManagementResponse reference documentation
        // for the contents of response.
        System.out.println(response.getSuccessCount() + " tokens were subscribed successfully");

    }


}
