package com.bucheon.sch.biz.metabus.metaUser.v1.service;

import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaUserPointHistoryReq;
import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaUserPointHistoryRes;
import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaUserRes;
import com.bucheon.sch.common.util.Paging;
import com.bucheon.sch.db.entity.metabus.MetaTrashType;
import com.bucheon.sch.db.entity.metabus.MetaUser;
import com.bucheon.sch.db.repository.metabus.MetaTrashTypeRepository;
import com.bucheon.sch.db.repository.metabus.MetaUserPointHistoryRepository;
import com.bucheon.sch.db.repository.metabus.MetaUserRepository;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MetaUserPointHistoryService {

    private final Logger logger = LoggerFactory.getLogger(MetaUserPointHistoryService.class);
    private final HttpServletRequest httpServletRequest;
    private final MetaUserRepository metaUserRepository;
    private final MetaTrashTypeRepository metaTrashTypeRepository;
    private final MetaUserPointHistoryRepository metaUserPointHistoryRepository;
    private final ModelMapper modelMapper;


    // 개인용 /////////////////////////////////////////////////////////////////////////////////////////////////
    public List<MetaUserPointHistoryRes> MyMetaUserPointHistorySelectListService(MetaUserPointHistoryReq dto) {
        logger.info("-------------- 내 포인트 내역 보기 시작 --------------------");
        Map<Long, String> tt = metaTrashTypeRepository.findAllByUseYnAndDelYn("Y", "N")
                .stream().collect(Collectors.toMap(MetaTrashType::getTrashSeq, MetaTrashType::getTrashName));
        Map<String, String> tt2 = metaUserRepository.findAllByDelYn("N")
                .stream().collect(Collectors.toMap(MetaUser::getUserId, MetaUser::getNickname));
        //logger.info("ttmap - {}", tt);
        List<MetaUserPointHistoryRes> res = metaUserPointHistoryRepository.SelectListMyMetaUserPointHistoryAll(dto)
                .stream().map(e -> modelMapper.map(e, MetaUserPointHistoryRes.class))
                .peek(f -> {
                    f.setUserNickname(tt2.get(f.getUserId()));
                    f.setTrashName(tt.get(f.getTrashSeq()));
                })
                .collect(Collectors.toList());

        logger.info(">> res : {}", new Gson().toJson(res));
        logger.info("-------------- 내 포인트 내역 보기 종료 --------------------");

        return res;
    }

    public Object MyMetaUserPointHistorySelectListServicePaging(MetaUserPointHistoryReq dto) {
        var result = metaUserPointHistoryRepository.SelectListMyMetaUserPointHistoryPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public List<MetaUserPointHistoryRes> MyMetaUserPointHistoryRecentSelectListService(MetaUserPointHistoryReq dto) {
        logger.info("-------------- 최근 내 포인트 내역 보기 시작 --------------------");
        Map<Long, String> tt = metaTrashTypeRepository.findAllByUseYnAndDelYn("Y", "N")
                .stream().collect(Collectors.toMap(MetaTrashType::getTrashSeq, MetaTrashType::getTrashName));
        Map<String, String> tt2 = metaUserRepository.findAllByDelYn("N")
                .stream().collect(Collectors.toMap(MetaUser::getUserId, MetaUser::getNickname));
        //logger.info("ttmap - {}", tt);
        List<MetaUserPointHistoryRes> res = metaUserPointHistoryRepository.SelectListMyMetaUserPointHistory(dto)
                .stream().map(e -> modelMapper.map(e, MetaUserPointHistoryRes.class))
                .peek(f -> {
                    f.setUserNickname(tt2.get(f.getUserId()));
                    f.setTrashName(tt.get(f.getTrashSeq()));
                })
                .collect(Collectors.toList());

        logger.info(">> res : {}", new Gson().toJson(res));
        logger.info("-------------- 최근 내 포인트 내역 보기 종료 --------------------");

        return res;
    }
    // 개인용 /////////////////////////////////////////////////////////////////////////////////////////////////


    // 통합관리자용 /////////////////////////////////////////////////////////////////////////////////////////////////
    public List<MetaUserPointHistoryRes> MetaUserPointHistorySelectListService(MetaUserPointHistoryReq dto) {
        logger.info("-------------- 포인트 내역 보기 시작 --------------------");
        //logger.info("ttmap - {}", tt);
        List<MetaUserPointHistoryRes> res = metaUserPointHistoryRepository.MetaUserPointHistorySelectList(dto);

        logger.info(">> res : {}", new Gson().toJson(res));
        logger.info("-------------- 포인트 내역 보기 종료 --------------------");

        return res;
    }

    public Object MetaUserPointHistorySelectListServicePaging(MetaUserPointHistoryReq dto) {
        var result = metaUserPointHistoryRepository.MetaUserPointHistorySelectListPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }
    // 통합관리자용 /////////////////////////////////////////////////////////////////////////////////////////////////

    public MetaUserRes MetaUserSelectOneUserIdService(String userId) {
        MetaUserRes result = modelMapper.map(metaUserRepository.findByUserId(userId).orElseThrow(IllegalArgumentException::new), MetaUserRes.class);
        return result;
    }


}
