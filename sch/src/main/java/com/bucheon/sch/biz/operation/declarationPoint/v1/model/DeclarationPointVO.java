package com.bucheon.sch.biz.operation.declarationPoint.v1.model;

import com.bucheon.sch.db.entity.operation.DeclarationPoint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DeclarationPointVO {

	/* SEQ */
	private Long declarationSeq;
	
	/* 접수 포인트 */
	private int declarationPoint;
	
	public DeclarationPoint ofMenu(){
		DeclarationPoint m = new DeclarationPoint();

		if (this.declarationSeq!=null) {
			m.setDeclarationSeq(this.declarationSeq);
		}

		m.setDeclarationPoint(this.declarationPoint);
		return m;
	}
	
}
