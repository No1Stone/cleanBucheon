package com.bucheon.sch.biz.metabus.metaUser.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetaRankingReq {

    private int rankingPage;
    private int rankingSize;
    private String yyyymm;
    private String sdate;

}
