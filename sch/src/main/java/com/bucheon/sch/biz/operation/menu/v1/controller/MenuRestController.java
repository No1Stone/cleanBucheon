package com.bucheon.sch.biz.operation.menu.v1.controller;

import com.bucheon.sch.biz.operation.menu.v1.model.MenuReq;
import com.bucheon.sch.biz.operation.menu.v1.service.MenuService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/menu/rest")
@RequiredArgsConstructor
public class MenuRestController {

    private final Logger logger = LoggerFactory.getLogger(MenuRestController.class);
    private final MenuService menuService;


    @PostMapping(path= "/test1")
    public Object MenuSelectList(@RequestBody MenuReq dto){
    return menuService.MenuSelectListService(dto);
    }


}
