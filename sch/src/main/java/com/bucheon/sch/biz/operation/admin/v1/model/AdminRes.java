package com.bucheon.sch.biz.operation.admin.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminRes {

    private Long adminSeq;
    private String adminId;
    private String adminName;
    private Long permissionSeq;
    private String permission;
    private String menuPermission;
    private String adminPhone;
    private String adminEmail;
    private String administrativeDivision;
    private String superYn;
    private LocalDateTime regDt;
    private LocalDateTime modDt;

}
