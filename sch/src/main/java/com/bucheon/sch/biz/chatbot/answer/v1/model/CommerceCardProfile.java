package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.Data;

@Data
public class CommerceCardProfile {

	private String imageUrl;
	
	private String nickname;
	
}
