package com.bucheon.sch.biz.operation.mileage.v1.controller;

import com.bucheon.sch.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.sch.biz.operation.menu.v1.service.MenuService;
import com.bucheon.sch.biz.operation.mileage.v1.model.UserMileageHistoryVO;
import com.bucheon.sch.biz.operation.mileage.v1.service.UserMileageHistoryService;
import com.bucheon.sch.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.sch.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "/operation/pointHistory")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserMileageController {

    private final Logger logger = LoggerFactory.getLogger(UserMileageController.class);
    private final UserMileageHistoryService userMileageHistoryService;
    private final PermissionService permissionService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/operation/pointHistory/info");
    }

    //기본틀
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(UserMileageHistoryVO dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(25L);

        return new ModelAndView("operation/pointHistory/pointHistoryManage")
                .addObject("pointHistoryPaging", userMileageHistoryService.UserMileageHistoryListSelectPagingService(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(UserMileageHistoryVO dto){
        return new ModelAndView("operation/pointHistory/pointHistoryListAjax")
                .addObject("pointHistoryList", userMileageHistoryService.UserMileageHistoryListSelectService(dto))
                .addObject("pointHistoryPaging", userMileageHistoryService.UserMileageHistoryListSelectPagingService(dto));

    }


}
