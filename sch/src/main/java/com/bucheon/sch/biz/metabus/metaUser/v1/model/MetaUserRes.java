package com.bucheon.sch.biz.metabus.metaUser.v1.model;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MetaUserRes {

    private String userId;
    private String nickname;
    private String email;
    private String provider;
    private String snsId;
    private int avatarType;
    private int cleanPoint;
    private String deviceToken;
    private String cleantownUid;
    private String delYn;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

}
