package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class CarouselItems {
	
	private String title;
	
	private String description;
	
	private CarouselItemsThumbnail thumbnail;
	
	private List<Object> buttons;

}
