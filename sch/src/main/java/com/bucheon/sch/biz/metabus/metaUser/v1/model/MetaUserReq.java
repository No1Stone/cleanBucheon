package com.bucheon.sch.biz.metabus.metaUser.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetaUserReq {

    private int page;
    private int size;
    private String userId;
    private String nickname;
    private String email;
    private String deviceToken;
    private String cleantownUid;
    private String delYn;
    private String searchDelYn;
    private String searchSelect;
    private String searchString;
    private String orderStatus;

}
