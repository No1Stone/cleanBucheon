package com.bucheon.sch.biz.chatbot.alarmtalk.v1.controller;

import com.bucheon.sch.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.sch.biz.operation.menu.v1.service.MenuService;
import com.bucheon.sch.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.sch.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/chatbot/alarmtalk")
@RequiredArgsConstructor
public class AlaramtalkController {

    private final Logger logger = LoggerFactory.getLogger(AlaramtalkController.class);
    private final PermissionService permissionService;
    private final MenuService menuService;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/chatbot/alarmtalk/alarmtalkManage");
    }

    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(HttpServletRequest request){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(9L);


        return new ModelAndView("chatbot/alarmtalk/alarmtalkManage")
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    @RequestMapping(path = "/infoAjax", method = RequestMethod.POST)
    public ModelAndView infoAjax(){
        return new ModelAndView("chatbot/alarmtalk/alarmtalkInfoAjax");
    }

}
