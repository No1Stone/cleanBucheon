package com.bucheon.sch.biz.metabus.metaUser.v1.controller;

import com.bucheon.sch.biz.metabus.metaUser.v1.service.MetaUserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping(path = "/metaBus/etc")
@RequiredArgsConstructor
public class MetaUserEtcController {

    private final Logger logger = LoggerFactory.getLogger(MetaUserEtcController.class);
    private final MetaUserService metaUserService;


    @GetMapping(path = "/termsOfService")
    public ModelAndView termsOfService() {
        return new ModelAndView("metaBus/etc/termsOfService");
    }

    @GetMapping(path = "/privacyPolicy")
    public ModelAndView privacyPolicy() {
        return new ModelAndView("metaBus/etc/privacyPolicy");
    }

}