package com.bucheon.sch.biz.metabus.metaUser.v1.model;

import lombok.Data;

@Data
public class MetaUserPointHistoryReq {

    private int page;
    private int size;

    private Long pointSeq;
    private String userId;
    private String month;

    private String searchSelect;
    private String searchString;
    private String searchSdate;
    private String searchEdate;

}
