package com.bucheon.sch.biz.operation.mileage.v1.service;

import com.bucheon.sch.biz.operation.mileage.v1.model.UserMileageHistoryVO;
import com.bucheon.sch.common.util.Masking;
import com.bucheon.sch.common.util.Paging;
import com.bucheon.sch.db.entity.operation.Admin;
import com.bucheon.sch.db.entity.operation.UserMileageHistory;
import com.bucheon.sch.db.repository.operation.UserMileageHistoryRepository;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserMileageHistoryService {

    private final Logger logger = LoggerFactory.getLogger(UserMileageHistoryService.class);
    private final HttpServletRequest httpServletRequest;
    private final UserMileageHistoryRepository userMileageHistoryRepository;
    private final ModelMapper modelMapper;

    public List<UserMileageHistoryVO> UserMileageHistoryListSelectService(UserMileageHistoryVO dto){
        return userMileageHistoryRepository.UserMileageHistorySelectList(dto)
                .stream().map(e -> modelMapper.map(e, UserMileageHistoryVO.class))
                .peek(f -> {
                    try {
                        f.setUserName(Masking.nameMasking(f.getUserName()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                })
                .collect(Collectors.toList());
    }

    public Paging UserMileageHistoryListSelectPagingService(UserMileageHistoryVO dto) {

        QueryResults<UserMileageHistoryVO> result = userMileageHistoryRepository.UserMileageHistorySelectListPaging(dto);

        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    //최근 10개 이력만 가져오기
    public List<UserMileageHistoryVO> UserMileageHistoryListRecentSelectService(String did){
        UserMileageHistoryVO dto = new UserMileageHistoryVO();
        dto.setUserId(did);
        dto.setPage(0);
        dto.setSize(20);

        return userMileageHistoryRepository.UserMileageHistorySelectList(dto);
    }

    public int UserMileageHistorySelectCountService(String userId) {
        int cnt = userMileageHistoryRepository.countByUserId(userId);
        return cnt;
    }

    public UserMileageHistoryVO UserMileageSelectOneService(Long seq) {
        UserMileageHistoryVO userMileageHistoryVO = modelMapper.map(userMileageHistoryRepository.findByMileageSeq(seq)
                .orElseThrow(IllegalArgumentException::new), UserMileageHistoryVO.class);

        return userMileageHistoryVO;
    }


    public UserMileageHistory UserMileageHistorySaveService(UserMileageHistoryVO dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;
        Long adminSeq = ((Admin) principal).getAdminSeq();
        dto.setAdminSeq(adminSeq);

        UserMileageHistory u = userMileageHistoryRepository.save(dto.ofUserMileageHistory());
        return u;
    }

}
