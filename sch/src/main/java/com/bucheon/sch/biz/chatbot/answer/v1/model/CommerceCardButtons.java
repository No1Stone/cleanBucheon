package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.Data;

@Data
public class CommerceCardButtons {
	/* 스킬 코드 */
	private String skillCode;
	
	private String action;
	
	private String label;
	
	private String link;
	
	private String messageText;
	
	private String webLinkUrl;
	
	private String blockId;
	
	private String phoneNumber;
	
	private String osLink;


}
