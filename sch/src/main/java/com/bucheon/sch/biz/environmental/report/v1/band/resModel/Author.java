package com.bucheon.sch.biz.environmental.report.v1.band.resModel;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Author {
    private String name;
    private String description;
    private String role;
    private String profile_image_url;
    private String user_key;
    private String member_type;
    private String member_certified;
    private boolean me;
    private boolean is_muted;
    private long created_at;

}
