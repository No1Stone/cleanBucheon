package com.bucheon.sch.biz.operation.damdang.v1.service;

import com.bucheon.sch.biz.operation.damdang.v1.model.DamdangReq;
import com.bucheon.sch.biz.operation.damdang.v1.model.DamdangRes;
import com.bucheon.sch.db.entity.operation.Admin;
import com.bucheon.sch.db.repository.etc.DongDamdangRepository;
import com.bucheon.sch.db.repository.operation.AdminRepository;
import com.bucheon.sch.db.repository.operation.MenuRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DamdangService {

    private final Logger logger = LoggerFactory.getLogger(DamdangService.class);
    private final AdminRepository adminRepository;
    private final MenuRepository menuRepository;
    private final DongDamdangRepository dongDamdangRepository;
    private final ModelMapper modelMapper;

    @Transactional(readOnly = true)
    public Object DamdangSelectListService() {
        var damdang = adminRepository.findByUseYnAndDelYnOrderByAdminNameAsc("Y","N")
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
        var result = dongDamdangRepository.findAll()
                .stream().map(e -> modelMapper.map(e, DamdangRes.class))
                .peek(f -> {
                    f.setDamdangName(damdang.get(f.getDamdang()));
                })
                .collect(Collectors.toList());
        return result;
    }

    @Transactional
    public String DamdangUpdateAndSave(DamdangReq dto) {

        int count = 0;
        int up = 0;
        if (dto.getDong() == null) {
            //아무것도 안함
        } else {

            if (dto.getDamdang() != null) {
                up = dongDamdangRepository.updateDamdang(dto.getDamdang(), dto.getDong());
                count += up;
            }

        }
        if (count > 0) {
            return "success";
        } else {
            return "fail";
        }
    }


}
