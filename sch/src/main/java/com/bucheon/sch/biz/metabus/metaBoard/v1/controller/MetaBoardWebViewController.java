package com.bucheon.sch.biz.metabus.metaBoard.v1.controller;

import com.bucheon.sch.biz.metabus.metaBoard.v1.model.MetaBoardRes;
import com.bucheon.sch.biz.metabus.metaBoard.v1.service.MetaBoardService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "/metaBus/web/board")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MetaBoardWebViewController {

    private final MetaBoardService metaBoardService;
    private final Logger logger = LoggerFactory.getLogger(MetaBoardWebViewController.class);

    //투표페이지
    @RequestMapping(path = "/votepage", method = RequestMethod.GET)
    public ModelAndView votepage() {

        MetaBoardRes metaBoardRes = metaBoardService.MetaBoardSelectTypeOneService("C");
        logger.info("--- board : {}",new Gson().toJson(metaBoardRes));

        if(metaBoardRes != null) {
            return new ModelAndView("metaBus/web/votepage")
                    .addObject("board", metaBoardRes)
                    ;
        }
        else {
            return new ModelAndView("metaBus/web/votepage")
                    .addObject("board", null)
                    ;
        }
    }

    //카드뉴스1 페이지
    @RequestMapping(path = "/cardpage1", method = RequestMethod.GET)
    public ModelAndView cardpage1() {

        MetaBoardRes metaBoardRes = metaBoardService.MetaBoardSelectTypeOneService("A");
        logger.info("--- board : {}",new Gson().toJson(metaBoardRes));

        if(metaBoardRes != null) {
            return new ModelAndView("metaBus/web/cardpage")
                    .addObject("board", metaBoardRes)
                    ;
        }
        else {
            return new ModelAndView("metaBus/web/cardpage")
                    .addObject("board", null)
                    ;
        }
    }

    //카드뉴스2 페이지
    @RequestMapping(path = "/cardpage2", method = RequestMethod.GET)
    public ModelAndView cardpage2() {

        MetaBoardRes metaBoardRes = metaBoardService.MetaBoardSelectTypeOneService("B");
        logger.info("--- board : {}",new Gson().toJson(metaBoardRes));

        if(metaBoardRes != null) {
            return new ModelAndView("metaBus/web/cardpage")
                    .addObject("board", metaBoardRes)
                    ;
        }
        else {
            return new ModelAndView("metaBus/web/cardpage")
                    .addObject("board", null)
                    ;
        }
    }

    //동영상
    @RequestMapping(path = "/movie", method = RequestMethod.GET)
    public ModelAndView movie() {

        MetaBoardRes metaBoardRes = metaBoardService.MetaBoardSelectTypeOneService("D");
        logger.info("--- board : {}",new Gson().toJson(metaBoardRes));

        if(metaBoardRes != null) {
            return new ModelAndView("metaBus/web/movie")
                    .addObject("board", metaBoardRes)
                    ;
        }
        else {
            return new ModelAndView("metaBus/web/movie")
                    .addObject("board", null)
                    ;
        }

    }

}
