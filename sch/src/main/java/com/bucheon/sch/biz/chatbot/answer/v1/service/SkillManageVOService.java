package com.bucheon.sch.biz.chatbot.answer.v1.service;

import com.bucheon.sch.biz.chatbot.answer.v1.model.*;
import com.bucheon.sch.common.type.Definition;
import com.bucheon.sch.common.util.JsonUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SkillManageVOService {

	JsonUtil jsonutil = new JsonUtil();

	/**
	 * input용 skillVo
	 * 
	 * @param vo
	 * @return
	 */
	public SkillVO setSkillVO(SkillVO vo) {
		// 여기까진 프로필 값있음
		SkillVO makeSkillVo = new SkillVO();

		SkillManageRVO skillmanagervo = vo.getSkillManageRVO();

		String skillCode = skillmanagervo.getSkillCode();
		String subSystem = vo.getSkillManageRVO().getSubSystem();

		String resType = "";
		String[] type = { vo.getSkillManageRVO().getResType1(), vo.getSkillManageRVO().getResType2(),
				vo.getSkillManageRVO().getResType3() };
		try {
			List<SimpleTextVO> newStListVo = new ArrayList<SimpleTextVO>();
			List<SimpleImageVO> newSiListVo = new ArrayList<SimpleImageVO>();
			List<List<BasicCardVO>> newBcListVo = new ArrayList<List<BasicCardVO>>();
			List<List<CommerceCardVO>> newCcListVo = new ArrayList<List<CommerceCardVO>>();
			List<ListCardVO> newLcListVo = new ArrayList<ListCardVO>();
			List<QuickRepliesVO> newQrListVo = new ArrayList<QuickRepliesVO>();

			List<SimpleTextVO> stListVo = vo.getSimpleTextVO();
			List<SimpleImageVO> siListVo = vo.getSimpleImageVO();
			List<List<BasicCardVO>> bcListVo = vo.getBasicCardVO();
			List<List<CommerceCardVO>> ccListVo = vo.getCommerceCardVO();
			List<ListCardVO> lcListVo = vo.getListCardVO();
			List<QuickRepliesVO> qrListVo = vo.getQuickRepliesVO();

			for (int i = 0; i < 3; i++) {
				if (!(type[i] == null || type[i].equals(""))) {
					int cnt = 1;

					if (i != 0) {
						resType += ",";
					}

					switch (type[i]) {
					case Definition.SKILL_TYPE_ST:
						SimpleTextVO stVo = makeSTVO(skillCode, stListVo.get(i), (i + 1), subSystem);
						newStListVo.add(stVo);

						resType += "R" + (i + 1) + ":" + Definition.SKILL_TYPE_ST + cnt;
						break;

					case Definition.SKILL_TYPE_SI:
						SimpleImageVO siVo = makeSIVO(skillCode, siListVo.get(i), (i + 1), subSystem);
						newSiListVo.add(siVo);

						resType += "R" + (i + 1) + ":" + Definition.SKILL_TYPE_SI + cnt;
						break;

					case Definition.SKILL_TYPE_BC:
						cnt = bcListVo.get(i).size();
						List<BasicCardVO> bcVo = makeBCVO(skillCode, bcListVo.get(i), (i + 1), subSystem);
						newBcListVo.add(bcVo);

						resType += "R" + (i + 1) + ":" + Definition.SKILL_TYPE_BC + cnt;
						break;

					case Definition.SKILL_TYPE_CC:
						cnt = ccListVo.get(i).size();
						List<CommerceCardVO> ccVo = makeCCVO(skillCode, ccListVo.get(i), (i + 1), subSystem);
						newCcListVo.add(ccVo);

						resType += "R" + (i + 1) + ":" + Definition.SKILL_TYPE_CC + cnt;
						break;

					case Definition.SKILL_TYPE_LC:
						ListCardVO lcVo = makeLCVO(skillCode, lcListVo.get(i), (i + 1), subSystem);
						newLcListVo.add(lcVo);

						resType += "R" + (i + 1) + ":" + Definition.SKILL_TYPE_LC + cnt;
						break;
					default:
						break;

					}
				}
			}

			if (qrListVo != null) {
				newQrListVo = makeQRVO(skillCode, qrListVo, subSystem);
			}
			makeSkillVo.setSimpleTextVO(newStListVo);
			makeSkillVo.setSimpleImageVO(newSiListVo);
			makeSkillVo.setBasicCardVO(newBcListVo);
			makeSkillVo.setCommerceCardVO(newCcListVo);
			makeSkillVo.setListCardVO(newLcListVo);
			makeSkillVo.setQuickRepliesVO(newQrListVo);

		} catch (Exception e) {
			e.printStackTrace();
		}

		skillmanagervo.setResType(resType);

		makeSkillVo.setResType(resType);
		makeSkillVo.setSkillManageRVO(skillmanagervo);
		return makeSkillVo;
	}

	/**
	 * SimpleTextVo 생성
	 * 
	 * @param skillCode ==> 등록 스킬코드
	 * @param tVo       ==> simpleTextVo
	 * @param resSeq    ==> 응답 순서
	 * @return
	 */
	public SimpleTextVO makeSTVO(String skillCode, SimpleTextVO tVo, int resSeq, String subSystem) {
		SimpleTextVO VO = new SimpleTextVO();

		VO.setSkillCode(skillCode);
		VO.setText(tVo.getText());
		VO.setResSeq(resSeq);
		VO.setSubSystem(subSystem);

		return VO;
	}

	/**
	 * SimpleImageVo 생성
	 * 
	 * @param skillCode ==> 등록 스킬코드
	 * @param iVo       ==> simpleImageVo
	 * @param resSeq    ==> 응답 순서
	 * @return
	 */
	public SimpleImageVO makeSIVO(String skillCode, SimpleImageVO iVo, int resSeq, String subSystem) {
		SimpleImageVO VO = new SimpleImageVO();

		VO.setSkillCode(skillCode);
		VO.setImageUrl(iVo.getImageUrl());
		VO.setAltText(iVo.getAltText());
		VO.setResSeq(resSeq);
		VO.setSubSystem(subSystem);

		return VO;
	}

	/**
	 * BasicCardVo List 생성
	 * 
	 * @param skillCode ==> 등록 스킬코드
	 * @param bVo       ==> BasicCardVo List
	 * @param resSeq    ==> 응답 순서
	 * @return
	 */
	public List<BasicCardVO> makeBCVO(String skillCode, List<BasicCardVO> bVo, int resSeq, String subSystem) {
		List<BasicCardVO> bcVoList = new ArrayList<BasicCardVO>();
		for (int i = 0; i < bVo.size(); i++) {
			BasicCardVO bcVo = new BasicCardVO();
			String jsonButton = "[]";

			if (bVo.get(i).getButtonVO() != null) {
				jsonButton = buttonToJsonString(skillCode, bVo.get(i).getButtonVO()); // 2019-12-26 수정 ( ' ==>> " )
			}

			bcVo.setSkillCode(skillCode);
			bcVo.setTitle(bVo.get(i).getTitle());
			bcVo.setDescription(bVo.get(i).getDescription());
			bcVo.setResSeq(resSeq);
			bcVo.setBcSeq(i + 1);

			bcVo.setButton(jsonButton);
			bcVo.setThumbnail(makeImageUrl(bVo.get(i))); // 2019-12-26 수정 ( ' ==>> " )
			bcVo.setOptnAt(bVo.get(i).getOptnAt());
			bcVo.setSubSystem(subSystem);

			bcVoList.add(bcVo);
		}

		return bcVoList;
	}

	public List<CommerceCardVO> makeCCVO(String skillCode, List<CommerceCardVO> cVo, int resSeq, String level2)
			throws JsonProcessingException {
		List<CommerceCardVO> ccVoList = new ArrayList<CommerceCardVO>();

		ObjectMapper mapper = new ObjectMapper();

		for (int i = 0; i < cVo.size(); i++) {
			CommerceCardVO ccVo = new CommerceCardVO();
			String jsonThumnail = "[]";
			String jsonProfile = "[]";
			String jsonButton = "[]";

			if (cVo.get(i).getThumbnailVO() != null) {
				jsonThumnail = mapper.writeValueAsString(cVo.get(i).getThumbnailVO());
			}
			if (cVo.get(i).getProfileVO() != null) {
				jsonProfile = mapper.writeValueAsString(cVo.get(i).getProfileVO());
			}
			if (cVo.get(i).getButtonVO() != null) {
				jsonButton = buttonToJsonString3(skillCode, cVo.get(i).getButtonVO());
			}

			ccVo.setSkillCode(skillCode);
			ccVo.setCcSeq(i + 1);
			ccVo.setResSeq(resSeq);
			ccVo.setLevel2(level2);
			ccVo.setDescription(cVo.get(i).getDescription());
			ccVo.setPrice(cVo.get(i).getPrice());
			ccVo.setDiscountAction(cVo.get(i).getDiscountAction());

			ccVo.setDiscount(cVo.get(i).getDiscount());
			ccVo.setDiscountRate(cVo.get(i).getDiscountRate());
			ccVo.setDiscountedPrice(cVo.get(i).getDiscountedPrice());

			ccVo.setThumbnail(jsonThumnail);
			ccVo.setProfile(jsonProfile);
			ccVo.setButton(jsonButton);

			ccVoList.add(ccVo);
		}
		return ccVoList;
	}

	public ListCardVO makeLCVO(String skillCode, ListCardVO lVo, int resSeq, String subSystem)
			throws JsonProcessingException {
		ListCardVO lcVoList = new ListCardVO();

		ObjectMapper mapper = new ObjectMapper();
		String jsonHeader = "[]";
		String jsonItems = "[]";
		String jsonButtons = "[]";

		if (lVo.getHeaderVO() != null) {
			jsonHeader = mapper.writeValueAsString(lVo.getHeaderVO());
		}
		if (lVo.getItemsVO() != null) {
			jsonItems = mapper.writeValueAsString(lVo.getItemsVO());
		}
		if (lVo.getButtonVO() != null) {
			jsonButtons = buttonToJsonString2(skillCode, lVo.getButtonVO());
		}
		lcVoList.setSkillCode(skillCode);
		lcVoList.setHeader(jsonHeader);
		lcVoList.setItems(jsonItems);
		lcVoList.setButton(jsonButtons);
		lcVoList.setResSeq(resSeq);
		lcVoList.setSubSystem(subSystem);
		return lcVoList;
	}

	/**
	 * Quick Replies Vo 생성
	 * 
	 * @param skillCode
	 * @param qrListVo
	 * @return
	 */
	public List<QuickRepliesVO> makeQRVO(String skillCode, List<QuickRepliesVO> qrListVo, String subSystem) {
		List<QuickRepliesVO> qrVoList = new ArrayList<QuickRepliesVO>();

		for (int i = 0; i < qrListVo.size(); i++) {
			QuickRepliesVO qrVo = new QuickRepliesVO();

			String label = qrListVo.get(i).getLabel();
			String action = qrListVo.get(i).getAction();
			String blockId = qrListVo.get(i).getBlockId();
			String message = qrListVo.get(i).getMessageText();

			qrVo.setSkillCode(skillCode);
			qrVo.setLabel(label);
			qrVo.setBlockId(blockId);
			qrVo.setMessageText(message);
			qrVo.setSubSystem(subSystem);
			qrVo.setQrSeq(i + 1);

			switch (action) {
			case Definition.MSG_BTN_TYPE_BL:// 블록 연결
				qrVo.setAction("block");
				break;
			case Definition.MSG_BTN_TYPE_MS:// 메세지 전송
				qrVo.setAction("message");
				break;
			default:
				break;
			}
			qrVoList.add(qrVo);
		}

		return qrVoList;
	}

	/**
	 * Basic Card Button --> Json String
	 */
	public String buttonToJsonString(String skillCode, List<BasicCardButtons> list) {
		List<Map<String, Object>> bcButtonList = new ArrayList<>();

		for (int i = 0; i < list.size(); i++) {

			Map<String, Object> btn = new HashMap<String, Object>();

			String label = list.get(i).getLabel();
			String action = list.get(i).getAction();
			String btn_link = list.get(i).getLink();

			btn.put("skillCode", skillCode);
			btn.put("label", label);
			btn.put("action", action);

			switch (action) {
			case Definition.MSG_BTN_TYPE_BL:// 블록 연결
				btn.put(Definition.SKILL_BL_MSG, btn_link);
				break;
			case Definition.MSG_BTN_TYPE_UL:// URI 링크
				btn.put(Definition.SKILL_UL_MSG, btn_link);
				break;
			case Definition.MSG_BTN_TYPE_TL:// 전화
				btn.put(Definition.SKILL_TL_MSG, btn_link);
				break;
			case Definition.MSG_BTN_TYPE_MS:// 메세지 전송
				btn.put(Definition.SKILL_MS_MSG, btn_link);
				break;
			case Definition.MSG_BTN_TYPE_AL:// 옵션 플러그인
				btn.put(Definition.SKILL_UL_MSG, btn_link);
				break;
			default:
				break;
			}
			bcButtonList.add(btn);

		}
		return jsonutil.listmap_to_json_string(bcButtonList);
	}

	/**
	 * List Card Button --> Json String
	 */
	public String buttonToJsonString2(String skillCode, List<ListCardButtons> list) {
		List<Map<String, Object>> lcButtonList = new ArrayList<>();

		for (int i = 0; i < list.size(); i++) {

			Map<String, Object> btn = new HashMap<String, Object>();

			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>> skillCode : " + skillCode);


			String label = list.get(i).getLabel();
			String action = list.get(i).getAction();
			String btn_link = list.get(i).getLink();

			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>> action : " + action);
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>> btn_link : " + btn_link);

			btn.put("skillCode", skillCode);
			btn.put("label", label);
			btn.put("action", action);

			switch (action) {
			case Definition.MSG_BTN_TYPE_BL:// 블록 연결
				btn.put(Definition.SKILL_BL_MSG, btn_link);
				break;
			case Definition.MSG_BTN_TYPE_UL:// URI 링크
				btn.put(Definition.SKILL_UL_MSG, btn_link);
				break;
			case Definition.MSG_BTN_TYPE_TL:// 전화
				btn.put(Definition.SKILL_TL_MSG, btn_link);
				break;
			case Definition.MSG_BTN_TYPE_MS:// 메세지 전송
				btn.put(Definition.SKILL_MS_MSG, btn_link);
				break;
			case Definition.MSG_BTN_TYPE_AL:// 옵션 플러그인
				btn.put(Definition.SKILL_UL_MSG, btn_link);
				break;
			default:
				break;
			}
			lcButtonList.add(btn);

		}
		return jsonutil.listmap_to_json_string(lcButtonList);
	}

	/**
	 * Commerce Card Button --> Json String
	 */
	public String buttonToJsonString3(String skillCode, List<CommerceCardButtons> list) {
		List<Map<String, Object>> lcButtonList = new ArrayList<>();

		for (int i = 0; i < list.size(); i++) {

			Map<String, Object> btn = new HashMap<String, Object>();

			String label = list.get(i).getLabel();
			String action = list.get(i).getAction();
			String btn_link = list.get(i).getLink();

			btn.put("skillCode", skillCode);
			btn.put("label", label);
			btn.put("action", action);

			switch (action) {
			case Definition.MSG_BTN_TYPE_BL:// 블록 연결
				btn.put(Definition.SKILL_BL_MSG, btn_link);
				break;
			case Definition.MSG_BTN_TYPE_UL:// URI 링크
				btn.put(Definition.SKILL_UL_MSG, btn_link);
				break;
			case Definition.MSG_BTN_TYPE_TL:// 전화
				btn.put(Definition.SKILL_TL_MSG, btn_link);
				break;
			case Definition.MSG_BTN_TYPE_MS:// 메세지 전송
				btn.put(Definition.SKILL_MS_MSG, btn_link);
				break;
			case Definition.MSG_BTN_TYPE_AL:// 옵션 플러그인
				btn.put(Definition.SKILL_UL_MSG, btn_link);
				break;
			default:
				break;
			}
			lcButtonList.add(btn);

		}
		return jsonutil.listmap_to_json_string(lcButtonList);
	}

	public String makeImageUrl(BasicCardVO bVo) {

		HashMap<String, Object> urlMap = new HashMap<String, Object>();
		urlMap.put("imageUrl", bVo.getImageUrlLink());

		return thumnailToJsonString(urlMap);
	}

	public List<BasicCardVO> setBCVO(List<BasicCardVO> vo)
			throws JsonParseException, JsonMappingException, IOException {
		List<BasicCardVO> bcListVo = new ArrayList<BasicCardVO>();
		ObjectMapper mapper = new ObjectMapper();

		for (int i = 0; i < vo.size(); i++) {
			BasicCardVO bcVo = new BasicCardVO();

			bcVo.setSkillCode(vo.get(i).getSkillCode());
			bcVo.setTitle(vo.get(i).getTitle());
			bcVo.setDescription(vo.get(i).getDescription());
			bcVo.setResSeq(vo.get(i).getResSeq());
			bcVo.setBcSeq(vo.get(i).getBcSeq());
			bcVo.setOptnAt(vo.get(i).getOptnAt());

			if (!(vo.get(i).getThumbnail() == null)) {
				String thumbnail = vo.get(i).getThumbnail();
				bcVo.setThumbnailVO(mapper.readValue(thumbnail, new TypeReference<Map<String, Object>>() {
				})); // .replaceAll("\'","\"") 2019-12-26 수정 ( ' ==>> " )
				bcVo.setThumbnail("");
			}

			if (!(vo.get(i).getButton() == null)) {
				String button = vo.get(i).getButton();
				bcVo.setButtonVO(JsonStringToButton(button)); // .replaceAll("\'","\"") 2019-12-26 수정 ( ' ==>> " )
				bcVo.setButton("");
			}

			bcListVo.add(bcVo);
		}

		return bcListVo;
	}

	public List<CommerceCardVO> setCCVO(List<CommerceCardVO> vo)
			throws JsonParseException, JsonMappingException, IOException {
		List<CommerceCardVO> ccListVo = new ArrayList<CommerceCardVO>();
		ObjectMapper mapper = new ObjectMapper();

		for (int i = 0; i < vo.size(); i++) {
			CommerceCardVO ccVo = new CommerceCardVO();

			ccVo.setSkillCode(vo.get(i).getSkillCode());
			ccVo.setCcSeq(vo.get(i).getCcSeq());
			ccVo.setResSeq(vo.get(i).getResSeq());
			ccVo.setDescription(vo.get(i).getDescription());
			ccVo.setPrice(vo.get(i).getPrice());
			ccVo.setDiscount(vo.get(i).getDiscount());

			if (Integer.parseInt(String.valueOf(vo.get(i).getDiscountRate())) != 0) {
				ccVo.setDiscountAction("dr");
			} else {
				ccVo.setDiscountAction("dp");
			}

			ccVo.setDiscountedPrice(vo.get(i).getDiscountedPrice());
			ccVo.setDiscountRate(vo.get(i).getDiscountRate());

			if (!(vo.get(i).getThumbnail() == null)) {
				ccVo.setThumbnailVO(
						mapper.readValue(vo.get(i).getThumbnail(), new TypeReference<List<CommerceCardThumbnails>>() {
						}));
				ccVo.setThumbnail("");
			}
			if (!(vo.get(i).getProfile() == null)) {
				ccVo.setProfileVO(mapper.readValue(vo.get(i).getProfile(), CommerceCardProfile.class));
				ccVo.setProfile("");
			}
			if (!(vo.get(i).getButton() == null)) {
				ccVo.setButtonVO(JsonStringToButton3(vo.get(i).getButton()));
				ccVo.setButton("");
			}
			ccListVo.add(ccVo);
		}
		return ccListVo;
	}

	public ListCardVO setLCVO(ListCardVO vo) throws JsonParseException, JsonMappingException, IOException {
		ListCardVO lcListVo = new ListCardVO();
		ObjectMapper mapper = new ObjectMapper();

		lcListVo.setSkillCode(vo.getSkillCode());
		lcListVo.setResSeq(vo.getResSeq());

		if (!(vo.getHeader() == null)) {
			lcListVo.setHeaderVO(mapper.readValue(vo.getHeader(), ListCardHeader.class));
			lcListVo.setHeader("");
		}
		if (!(vo.getItems() == null)) {
			lcListVo.setItemsVO(mapper.readValue(vo.getItems(), new TypeReference<List<ListCardItems>>() {
			}));
			lcListVo.setItems("");
		}
		if (!(vo.getButton() == null)) {
			lcListVo.setButtonVO(JsonStringToButton2(vo.getButton()));
			lcListVo.setButton("");
		}
		return lcListVo;
	}

	/**
	 * thumnail --> Json String
	 */
	public String thumnailToJsonString(HashMap<String, Object> dataset) {
		return jsonutil.map_to_json_string(dataset);
	}

	/**
	 * Json String --> thumnail
	 */
	public Map<String, Object> JsonStringToThumbnail(String jsonStr) {
		return jsonutil.JsonStringToMap(jsonStr);
	}

	/**
	 * Json String --> Basic Card Button
	 */
	public List<BasicCardButtons> JsonStringToButton(String jsonStr) {
		List<BasicCardButtons> list = new ArrayList<BasicCardButtons>();
		JSONArray jsonArray = new JSONArray(jsonStr);

		try {
			if (jsonArray != null) {
				int jsonSize = jsonArray.length();
				for (int i = 0; i < jsonSize; i++) {
					Map<String, Object> map = JsonUtil.getMapFromJsonObject((JSONObject) jsonArray.get(i));
					BasicCardButtons button = new BasicCardButtons();

					String skillcode = String.valueOf(map.get("skillCode"));
					String label = String.valueOf(map.get("label"));
					String action = String.valueOf(map.get("action"));
					String link = "";

					button.setSkillCode(skillcode);
					button.setLabel(label);
					button.setAction(action);
					switch (action) {
					case Definition.MSG_BTN_TYPE_BL:// 블록 연결
						link = String.valueOf(map.get(Definition.SKILL_BL_MSG));
						break;
					case Definition.MSG_BTN_TYPE_UL:// URI 링크
						link = String.valueOf(map.get(Definition.SKILL_UL_MSG));
						break;
					case Definition.MSG_BTN_TYPE_TL:// 전화
						link = String.valueOf(map.get(Definition.SKILL_TL_MSG));
						break;
					case Definition.MSG_BTN_TYPE_MS:// 메세지 전송
						link = String.valueOf(map.get(Definition.SKILL_MS_MSG));
						break;
					case Definition.MSG_BTN_TYPE_AL:// 메세지 전송
						link = String.valueOf(map.get(Definition.SKILL_UL_MSG));
						break;
					default:
						break;
					}
					button.setLink(link);
					list.add(button);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Json String --> List Card Button
	 */
	public List<ListCardButtons> JsonStringToButton2(String jsonStr) {
		List<ListCardButtons> list = new ArrayList<ListCardButtons>();
		JSONArray jsonArray = new JSONArray(jsonStr);

		try {
			if (jsonArray != null) {
				int jsonSize = jsonArray.length();
				for (int i = 0; i < jsonSize; i++) {
					Map<String, Object> map = JsonUtil.getMapFromJsonObject((JSONObject) jsonArray.get(i));
					ListCardButtons button = new ListCardButtons();

					String skillcode = String.valueOf(map.get("skillCode"));
					String label = String.valueOf(map.get("label"));
					String action = String.valueOf(map.get("action"));
					String link = "";

					button.setSkillCode(skillcode);
					button.setLabel(label);
					button.setAction(action);
					switch (action) {
					case Definition.MSG_BTN_TYPE_BL:// 블록 연결
						link = String.valueOf(map.get(Definition.SKILL_BL_MSG));
						break;
					case Definition.MSG_BTN_TYPE_UL:// URI 링크
						link = String.valueOf(map.get(Definition.SKILL_UL_MSG));
						break;
					case Definition.MSG_BTN_TYPE_TL:// 전화
						link = String.valueOf(map.get(Definition.SKILL_TL_MSG));
						break;
					case Definition.MSG_BTN_TYPE_MS:// 메세지 전송
						link = String.valueOf(map.get(Definition.SKILL_MS_MSG));
						break;
					case Definition.MSG_BTN_TYPE_AL:// 옵션플러그인
						link = String.valueOf(map.get(Definition.SKILL_UL_MSG));
						break;
					default:
						break;
					}
					button.setLink(link);
					list.add(button);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Json String --> Commerce Card Button
	 */
	public List<CommerceCardButtons> JsonStringToButton3(String jsonStr) {
		List<CommerceCardButtons> list = new ArrayList<CommerceCardButtons>();
		JSONArray jsonArray = new JSONArray(jsonStr);

		try {
			if (jsonArray != null) {
				int jsonSize = jsonArray.length();
				for (int i = 0; i < jsonSize; i++) {
					Map<String, Object> map = JsonUtil.getMapFromJsonObject((JSONObject) jsonArray.get(i));
					CommerceCardButtons button = new CommerceCardButtons();

					String skillcode = String.valueOf(map.get("skillCode"));
					String label = String.valueOf(map.get("label"));
					String action = String.valueOf(map.get("action"));
					String link = "";

					button.setSkillCode(skillcode);
					button.setLabel(label);
					button.setAction(action);
					switch (action) {
					case Definition.MSG_BTN_TYPE_BL:// 블록 연결
						link = String.valueOf(map.get(Definition.SKILL_BL_MSG));
						break;
					case Definition.MSG_BTN_TYPE_UL:// URI 링크
						link = String.valueOf(map.get(Definition.SKILL_UL_MSG));
						break;
					case Definition.MSG_BTN_TYPE_TL:// 전화
						link = String.valueOf(map.get(Definition.SKILL_TL_MSG));
						break;
					case Definition.MSG_BTN_TYPE_MS:// 메세지 전송
						link = String.valueOf(map.get(Definition.SKILL_MS_MSG));
						break;
					case Definition.MSG_BTN_TYPE_AL:// 옵션플러그인
						link = String.valueOf(map.get(Definition.SKILL_UL_MSG));
						break;
					default:
						break;
					}
					button.setLink(link);
					list.add(button);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
