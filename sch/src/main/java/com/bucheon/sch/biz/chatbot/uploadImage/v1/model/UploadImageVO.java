package com.bucheon.sch.biz.chatbot.uploadImage.v1.model;

import com.bucheon.sch.db.entity.chatbot.UploadImage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UploadImageVO {

	/* SEQ */
	private Long imageSeq;
	
	/* 이미지ID */
	private String imageId;
	
	/* 이미지명 */
	private String imageName;
	
	/* 이미지파일명 */
	private String imageFilename;
	
	/* 이미지파일명 체크용 */
	private String imageFilename_bk;
	
	/* 이미지서버파일명 */
	private String imageFileservername;
	
	/* 이미지 default YN */
	private String imageDefaultYn;
	
	/* 이미지 속성 */
	private String imageAttribute;
	
	/* 검색  */
	private String searchSelect;
	private String searchString;

	/* 페이지사이즈 */
	private int size;

	/* 페이지 */
	private int page;

	public UploadImage ofMenu(){
		UploadImage m = new UploadImage();

		if (this.imageSeq!=null) {
			m.setImageSeq(this.imageSeq);
		}

		m.setImageId(this.imageId);
		m.setImageName(this.imageName);
		m.setImageFilename(this.imageFilename);
		m.setImageFileservername(this.imageFileservername);
		m.setImageDefaultYn(this.imageDefaultYn);
		m.setImageAttribute(this.imageAttribute);

		return m;
	}
	
}
