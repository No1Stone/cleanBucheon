package com.bucheon.sch.biz.metabus.metaUser.v1.controller;

import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaTrashTypeReq;
import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaTrashTypeVO;
import com.bucheon.sch.biz.metabus.metaUser.v1.service.MetaTrashTypeService;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.sch.biz.operation.menu.v1.service.MenuService;
import com.bucheon.sch.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.sch.biz.util.service.UtilService;
import com.bucheon.sch.db.entity.metabus.MetaTrashType;
import com.bucheon.sch.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;

@Controller
@RequestMapping(path = "/metaBus/metaTrashType")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MetaTrashTypeController {

    private final Logger logger = LoggerFactory.getLogger(MetaTrashTypeController.class);
    private final UtilService utilService;
    private final PermissionService permissionService;
    private final MetaTrashTypeService metaTrashTypeService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/metaBus/metaTrashType/info");
    }

    //기본틀
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(MetaTrashTypeReq dto, HttpServletRequest request){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(37L);

        return new ModelAndView("metaBus/metaTrashType/metaTrashTypeManage")
                .addObject("metaTrashTypeListPage", metaTrashTypeService.MetaTrashSelectListPagingService(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(MetaTrashTypeReq dto){
        return new ModelAndView("metaBus/metaTrashType/metaTrashTypeListAjax")
                .addObject("metaTrashTypeList",metaTrashTypeService.MetaTrashSelectListService(dto))
                .addObject("metaTrashTypeListPage", metaTrashTypeService.MetaTrashSelectListPagingService(dto))
                ;
    }

    //최초 입력 페이지 팝업  VO셋팅페이지
    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm(){
        MetaTrashTypeVO ttu = new MetaTrashTypeVO();
        return new ModelAndView("metaBus/metaTrashType/metaTrashTypeManagePopupAjax")
                .addObject("ttuVo", ttu)
                ;

    }

    //게시글 상세보기
    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView modifyForm(Long seq){
        MetaTrashType ttu = metaTrashTypeService.MetaTrashSelectOneService(seq);
        return new ModelAndView("metaBus/metaTrashType/metaTrashTypeManagePopupAjax")
                .addObject("ttuVo", ttu)
                ;
    }

    //저장처리
    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(MetaTrashTypeVO dto) throws IllegalStateException, IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        metaTrashTypeService.MetaTrashSaveService(dto);
        map.put("result", "success");
        return map;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(MetaTrashTypeVO dto) throws IllegalStateException, IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        metaTrashTypeService.MetaTrashSaveService(dto);
        map.put("result", "success");
        return map;
    }

    //삭제처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(Long[] trashSeq){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        metaTrashTypeService.UpdateTrashTypeDelService(trashSeq);
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

}
