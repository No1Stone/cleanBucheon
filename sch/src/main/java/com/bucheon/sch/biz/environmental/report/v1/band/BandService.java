package com.bucheon.sch.biz.environmental.report.v1.band;

import com.bucheon.sch.biz.environmental.report.v1.band.model.BandAuthor;
import com.bucheon.sch.biz.environmental.report.v1.band.model.BandList;
import com.bucheon.sch.biz.environmental.report.v1.band.model.BandPotos;
import com.bucheon.sch.biz.environmental.report.v1.band.resModel.BandBoardComment;
import com.bucheon.sch.biz.environmental.report.v1.band.resModel.BandBoardList;
import com.bucheon.sch.biz.environmental.report.v1.band.resModel.NaverAddresses;
import com.bucheon.sch.biz.environmental.report.v1.band.type.BandListType;
import com.bucheon.sch.biz.environmental.report.v1.controller.TestEntity;
import com.bucheon.sch.biz.util.model.ConnectSumLogVO;
import com.bucheon.sch.biz.util.service.ConnectSumService;
import com.bucheon.sch.common.type.ProcessType;
import com.bucheon.sch.common.type.ReceptionType;
import com.bucheon.sch.common.util.RestCall;
import com.bucheon.sch.db.entity.environmental.Report;
import com.bucheon.sch.db.entity.environmental.ReportLog;
import com.bucheon.sch.db.entity.environmental.band.TB_BandBoardList;
import com.bucheon.sch.db.entity.environmental.band.TB_BandBoardListComment;
import com.bucheon.sch.db.entity.etc.DongBandDamdang;
import com.bucheon.sch.db.entity.etc.DongDamdang;
import com.bucheon.sch.db.entity.operation.Admin;
import com.bucheon.sch.db.repository.environmental.ReportImgRepository;
import com.bucheon.sch.db.repository.environmental.ReportLogRepository;
import com.bucheon.sch.db.repository.environmental.ReportRepository;
import com.bucheon.sch.db.repository.environmental.band.TB_BandBoardListCommentRepository;
import com.bucheon.sch.db.repository.environmental.band.TB_BandBoardListPotoRepository;
import com.bucheon.sch.db.repository.environmental.band.TB_BandBoardListRepository;
import com.bucheon.sch.db.repository.etc.DongBandDamdangRepository;
import com.bucheon.sch.db.repository.etc.DongDamdangRepository;
import com.bucheon.sch.db.repository.etc.DongNameRepository;
import com.bucheon.sch.db.repository.operation.AdminRepository;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
@EnableScheduling
@RequiredArgsConstructor
public class BandService {

    private final Logger logger = LoggerFactory.getLogger(BandService.class);
    private final ModelMapper modelMapper;
    private final TB_BandBoardListRepository tb_BandBoardListRepository;
    private final TB_BandBoardListPotoRepository tb_BandBoardListPotoRepository;
    private final TB_BandBoardListCommentRepository tb_BandBoardListCommentRepository;
    private final ReportRepository reportRepository;
    private final ReportLogRepository reportLogRepository;
    private final ReportImgRepository reportImgRepository;
    private final DongNameRepository dongNameRepository;
    private final DongDamdangRepository dongDamdangRepository;
    private final DongBandDamdangRepository dongBandDamdangRepository;
    private final AdminRepository adminRepository;
    private final ConnectSumService connectSumService;

    @Value("${naver.cloud.schema}")
    private String naveropenapiSchema;
    @Value("${naver.cloud.url}")
    private String naveropenapiUrl;
    @Value("${naver.band.schema}")
    private String bandSchema;
    @Value("${naver.band.url}")
    private String bandUrl;

    @Value("${naver.band.accessToken}")
    private String accessToken;

    @Value("${naver.band.accessTokenDev2}")
    private String accessTokenDev2;
    @Value("${naver.band.testBandkey.bucheonTest}")
    private String bucheonTest;
    @Value("${naver.band.testBandkey.sosabonTest}")
    private String sosabonTest;

    @Value("${naver.band.accessTokenProd}")
    private String accessTokenProd;
    @Value("${naver.band.prodBandkey.deasan}")
    private String deasan;
    @Value("${naver.band.prodBandkey.beoman}")
    private String beoman;
    @Value("${naver.band.prodBandkey.bucheon}")
    private String bucheon;
    @Value("${naver.band.prodBandkey.sang}")
    private String sang;
    @Value("${naver.band.prodBandkey.seonggok}")
    private String seonggok;
    @Value("${naver.band.prodBandkey.sosabon}")
    private String sosabon;
    @Value("${naver.band.prodBandkey.sinjoong}")
    private String sinjoong;
    @Value("${naver.band.prodBandkey.simgok}")
    private String simgok;
    @Value("${naver.band.prodBandkey.ojeong}")
    private String ojeong;
    @Value("${naver.band.prodBandkey.joong}")
    private String joong;


    @Value("${naver.cloud.prod.id}")
    private String cloudId;
    @Value("${naver.cloud.prod.secret}")
    private String cloudsecret;

    //밴드 자료 가져오기 스케줄
    @Transactional
    public void BandBoardGet() {
        //String accessKey = accessTokenProd;
        String accessKey = accessToken;
        List<BandBoardList> bbl = new ArrayList<>();
        JsonToModel.builder(RestCall.get(BandURL.builder().schema(bandSchema + "://")
                        .baseURL(bandUrl)
                        .api("/v2.1/bands")
                        .param("access_token", accessKey)
                        .build()))
                .ofObject("result_data")
                .ofArrayModel("bands", BandList.class)
                .stream().forEach(e -> {
                    JsonToModel.builder(RestCall.get(BandURL.builder().schema(bandSchema + "://")
                                    .baseURL(bandUrl)
                                    .api("/v2/band/posts")
                                    .param("access_token", accessKey)
                                    .param("band_key", e.getBand_key())
                                    .param("locale", "ko_KR")
                                    .build()))
                            .ofObject("result_data")
                            .ofArrayModel("items", BandBoardList.class)
                            .stream().forEach(z -> {
                                z.setBandName(e.getName());
                                bbl.add(z);
                            });
                });
        logger.info("bbl - {}", new Gson().toJson(bbl));
        bbl.stream().forEach(e ->
        {
            if (!tb_BandBoardListRepository.existsByPostKey(e.getPost_key())) {
                if (!reportRepository.existsBySourcePk(e.getPost_key())){
                    String areaName = "";

                    //테스트
                    if(accessToken.equals(accessTokenDev2)) {
                        if (e.getBand_key().equals(sosabonTest)) {
                            areaName = "소사본동";
                        } else if (e.getBand_key().equals(bucheonTest)) {
                            areaName = "부천동";
                        }
                    }

                    //운영
                    if(accessToken.equals(accessTokenProd)) {
                        if (e.getBand_key().equals(deasan)) {
                            areaName = "대산동";
                        } else if (e.getBand_key().equals(beoman)) {
                            areaName = "범안동";
                        } else if (e.getBand_key().equals(bucheon)) {
                            areaName = "부천동";
                        } else if (e.getBand_key().equals(sang)) {
                            areaName = "상동";
                        } else if (e.getBand_key().equals(seonggok)) {
                            areaName = "성곡동";
                        } else if (e.getBand_key().equals(sosabon)) {
                            areaName = "소사본동";
                        } else if (e.getBand_key().equals(sinjoong)) {
                            areaName = "신중동";
                        } else if (e.getBand_key().equals(simgok)) {
                            areaName = "심곡동";
                        } else if (e.getBand_key().equals(ojeong)) {
                            areaName = "오정동";
                        } else if (e.getBand_key().equals(joong)) {
                            areaName = "중동";
                        }
                    }

                    Report repoResult = reportRepository.save(e.ofReport(areaName));
                    logger.info("리포트저장 들어옴");
                    if (e.getPhotos() != null) {
                        e.ofReportImg(repoResult.getReportSeq()).stream().forEach(g -> reportImgRepository.save(g));
                        logger.info("사진저장 들어옴");
                    }

                    /////// 커넥션 카운팅 추가
                    try {
                        ConnectSumLogVO connectSumLogVO = new ConnectSumLogVO();
                        connectSumLogVO.setConnectDate(LocalDate.now());
                        connectSumLogVO.setUserId(e.getPost_key());
                        connectSumLogVO.setConnectType("B");
                        connectSumService.ConnectCount(connectSumLogVO);
                        System.out.println("일일 카운팅 밴드 성공");
                    } catch (Exception ee) {
                        System.out.println("일일 카운팅 밴드 오류남");
                    }
                }
            }
            tb_BandBoardListRepository.save(e.getTB_BandBoardList());
//            if (e.getLatest_comments() != null) {
//                e.getTB_BandBoardListComment().stream().forEach(g -> tb_BandBoardListCommentRepository.save(g));
//            }
            if (e.getPhotos() != null) {
                e.getBandBoardListPoto().stream().forEach(g -> tb_BandBoardListPotoRepository.save(g));
            }


        });

    }


    public void BoardStatusUpdate() {

    }
    //@Scheduled(cron = "* 0/2 * * * ?")
    public Object BoardCommentget() {
        List<TB_BandBoardList> aa = tb_BandBoardListRepository
                .findByProcessStatusAndCommentCountGreaterThanOrderByCreatedAtDesc(ProcessType.P.getName(), 0) ;


        String accessKey = accessToken;
        List<BandBoardComment> bbcc = new ArrayList<>();
        List<BandBoardComment> bbcd = new ArrayList<>();
        aa.stream().forEach(e -> {
            try {
                JsonToModel.builder(RestCall.get(BandURL.builder().schema(bandSchema + "://")
                                .baseURL(bandUrl)
                                .api("/v2/band/post/comments")
                                .param("access_token", accessKey)
                                .param("band_key", e.getBandKey())
                                .param("post_key", e.getPostKey())
                                .param("sort", "-created_at")
                                .build()))
                        .ofObject("result_data")
                        .ofArrayModel("items", BandBoardComment.class)
                        .stream().forEach(f -> {
                            if (f.getProcessStatus().equals(ProcessType.C.getName())) {
                                bbcc.add(f);
                                tb_BandBoardListRepository.UpdateProcessStatus(ProcessType.C.getName(), f.getPost_key());
                                reportRepository.UpdateReportStatusWhereSourcePk(ProcessType.C.getName(), f.getPost_key());
                            } else {
                                bbcd.add(f);
                            }
                        });
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });

        logger.info("aaaaaaaaaaa - - - - {}", new Gson().toJson(aa));
        logger.info("bbcc 완료  됨 - - - - {}", new Gson().toJson(bbcc));
        logger.info("bbcd 완료안됨 - - - - {}", new Gson().toJson(bbcd));

        return aa;
    }



    //
    //챗봇문의에 대한 청소 담당자가 #완료시 자동처리
    //@Scheduled(cron = "* 0/2 * * * ?")
    public void ReprotCommentProc() {

        List<Report> aa = reportRepository.findAllByReportReceptionAndReportStatusOrReportStatusAndDelYnAndSourcePkIsNotNullOrderByReportSeqDesc(ReceptionType.C.getName(), ProcessType.D.getName(),ProcessType.P.getName(), "N" );

        String accessKey = accessToken;
        List<BandBoardComment> bbcc = new ArrayList<>();
        List<BandBoardComment> bbcd = new ArrayList<>();
        aa.stream().forEach(e -> {
            try {
                JsonToModel.builder(RestCall.get(BandURL.builder().schema(bandSchema + "://")
                                .baseURL(bandUrl)
                                .api("/v2/band/post/comments")
                                .param("access_token", accessKey)
                                .param("band_key", e.getBandKey())
                                .param("post_key", e.getSourcePk())
                                .param("sort", "-created_at")
                                .build()))
                        .ofObject("result_data")
                        .ofArrayModel("items", BandBoardComment.class)
                        .stream().filter(h -> Objects.nonNull(h))
                        .forEach(f -> {

                            logger.info("---------- 밴드 코멘트 가져오기 시작 -------------------");
                            //logger.info("f : {}", new Gson().toJson(f));
                            logger.info("band_key : {}", f.getBand_key());
                            logger.info("post_key : {}", f.getPost_key());
                            logger.info("comment_key : {}", f.getComment_key());
                            logger.info("cnt : {}", tb_BandBoardListCommentRepository.countByCommentKeyAndPostKey(f.getComment_key(), f.getPost_key()));

                            //없으면 저장
                            if(tb_BandBoardListCommentRepository.countByCommentKeyAndPostKey(f.getComment_key(), f.getPost_key()) == 0){

                                TB_BandBoardListComment tb = new TB_BandBoardListComment();
                                tb.setPostKey(f.getPost_key());
                                tb.setCommentKey(f.getComment_key());
                                tb.setBody(f.getContent());
                                tb.setCreatedAt(f.getCreated_at());
                                BandAuthor author = f.getAuthor();
                                tb.setName(author.getName());
                                tb.setUserKey(author.getUser_key());
                                tb.setDescription(author.getDescription());
                                tb.setProfileImageUrl(author.getProfile_image_url());
                                BandPotos photo = f.getPhoto();
                                tb.setUrl(photo.getUrl());
                                tb.setWidth(photo.getWidth());
                                tb.setHeight(photo.getHeight());

                                logger.info("코멘트 : {}", new Gson().toJson(tb));
                                tb_BandBoardListCommentRepository.save(tb);
                            }

                            Report report = reportRepository.findBySourcePk(f.getPost_key());
                            DongBandDamdang dongBandDamdang = dongBandDamdangRepository.findByDong(report.getAreaName()).orElseThrow(IllegalArgumentException::new);

                            logger.info("---------------- f.getPost_key() : {}", f.getPost_key());
                            logger.info("---------------- f.getContent() : {}", f.getContent());
                            logger.info("---------------- f.getAuthor().toString() : {}", new Gson().toJson(f.getAuthor()));
                            logger.info("---------------- dongBandDamdang.getDamdang() : {} : {}", dongBandDamdang.getDong(), dongBandDamdang.getDamdang());

                            BandAuthor author2 = f.getAuthor();
                            String[] damdangArr = dongBandDamdang.getDamdang().split(",");

                            for(String damdangName : damdangArr){
                                if(f.getContent().contains("#완료") && new Gson().toJson(f.getAuthor()).contains(damdangName.trim())) {
                                    f.setProcessStatus(ProcessType.C.getName());
                                    logger.info("처리완료 : {}", f.getPost_key());
                                    break;
                                } else if(f.getContent().contains("#처리불가") && new Gson().toJson(f.getAuthor()).contains(damdangName.trim())){
                                    f.setProcessStatus(ProcessType.R.getName());
                                    logger.info("처리불가 : {}", f.getPost_key());
                                    break;
                                } else {
                                    f.setProcessStatus(ProcessType.P.getName());
                                    logger.info("처리중 : {}", f.getPost_key());
                                }

                                logger.info("----------------f.getProcessStatus : {}", f.getProcessStatus());
                            }


                            try {
                                if (f.getProcessStatus().equals(ProcessType.C.getName())) {
                                    bbcc.add(f);
                                    report.setReportStatus(ProcessType.C.getName());
                                    tb_BandBoardListRepository.UpdateProcessStatus(ProcessType.C.getName(), f.getPost_key());
                                    reportRepository.UpdateReportStatusWhereSourcePk(ProcessType.C.getName(), f.getPost_key());
                                    reportRepository.UpdateComDtWhereSourcePk(LocalDateTime.now(), f.getPost_key());

                                    //담당자를 구한다
                                    DongDamdang dongDamdang = dongDamdangRepository.findByDong(report.getAreaName()).orElseThrow(IllegalArgumentException::new);
                                    Admin admin = adminRepository.findByAdminSeq(dongDamdang.getDamdang()).orElseThrow(IllegalArgumentException::new);

                                    //로그 저장
                                    String msg = "";
                                    if(report.getReportStatus().equals(ProcessType.D.getName())){
                                        msg = "접수하였습니다";
                                    } else if(report.getReportStatus().equals(ProcessType.C.getName())){
                                        msg = "담당자 " + admin.getAdminName() + "님께서 처리완료로 변경하였습니다.";
                                    } else if(report.getReportStatus().equals(ProcessType.P.getName())){
                                        msg = "담당자 " + admin.getAdminName() + "님께서 처리중으로 변경하였습니다.";
                                    } else if(report.getReportStatus().equals(ProcessType.R.getName())){
                                        msg = "담당자 " + admin.getAdminName() + "님께서 처리불가로 변경하였습니다.";
                                    }

                                    int cnt = reportLogRepository.countByReportSeq(report.getReportSeq());
                                    String rn = String.valueOf(Timestamp.valueOf(LocalDateTime.now()).getTime());
                                    if(cnt > 0) {
                                        ReportLog reportLog = reportLogRepository.findTop1ByReportSeqOrderByLogSeqDesc(report.getReportSeq()).orElseThrow(IllegalArgumentException::new);
                                        rn = reportLog.getReceptionNum();
                                    }
                                    ReportLog r = new ReportLog();
                                    r.setReportSeq(report.getReportSeq());
                                    r.setLogTime(LocalDateTime.now());
                                    r.setReceptionNum(rn);
                                    r.setReportStatus(report.getReportStatus());
                                    r.setReportDamdang(admin.getAdminSeq());
                                    r.setMsg(msg);
                                    r.setReportPointRejectReason(report.getReportPointRejectReason());

                                    ReportLog reportLogResult = reportLogRepository.save(r);


                                } else {
                                    bbcd.add(f);
                                }

                                logger.info("---------- 밴드 코멘트 가져오기 끝 -------------------");
                            } catch (Exception exception) {

                            }

                        });
            } catch (Exception exception) {

            }
        });

        logger.info("전체리스트 - - - - {}", new Gson().toJson(aa));
        logger.info("됨 - - - - {}", new Gson().toJson(bbcc));
        logger.info("안됨 - - - - {}", new Gson().toJson(bbcd));
    }


    public String SendBandPost(String addr, String contents) {
        String accessKey = accessToken;
//        String bandKey = BandBucheonDongValidation(adress);
        String bandKey = "";

        if(accessToken.equals(accessTokenProd)) {
            if (addr.equals("대산동")) {
                bandKey = deasan;
            } else if (addr.equals("범안동")) {
                bandKey = beoman;
            } else if (addr.equals("부천동")) {
                bandKey = bucheon;
            } else if (addr.equals("상동")) {
                bandKey = sang;
            } else if (addr.equals("성곡동")) {
                bandKey = seonggok;
            } else if (addr.equals("소사본동")) {
                bandKey = sosabon;
            } else if (addr.equals("신중동")) {
                bandKey = sinjoong;
            } else if (addr.equals("심곡동")) {
                bandKey = simgok;
            } else if (addr.equals("오정동")) {
                bandKey = ojeong;
            } else if (addr.equals("중동")) {
                bandKey = joong;
            } else {
                bandKey = bucheon;
            }
        }

        if(accessToken.equals(accessTokenDev2)) {
            if (addr.equals("대산동")) {
                bandKey = sosabonTest;
            }
            else if (addr.equals("범안동")) {
                bandKey = sosabonTest;
            }
            else if (addr.equals("부천동")) {
                bandKey = sosabonTest;
            }
            else if (addr.equals("상동")) {
                bandKey = sosabonTest;
            }
            else if (addr.equals("성곡동")) {
                bandKey = sosabonTest;
            }
            else if (addr.equals("소사본동")) {
                bandKey = bucheonTest;
            }
            else if (addr.equals("신중동")) {
                bandKey = bucheonTest;
            }
            else if (addr.equals("심곡동")) {
                bandKey = bucheonTest;
            }
            else if (addr.equals("오정동")) {
                bandKey = bucheonTest;
            }
            else if (addr.equals("중동")) {
                bandKey = bucheonTest;
            }
        }


        logger.info("contents = {}", contents);
        String enContents = URLEncoder.encode(contents, Charset.forName("UTF-8"));
        String s = RestCall.postRes(BandURL.builder().schema(bandSchema + "://")
                .baseURL(bandUrl)
                .api("/v2.2/band/post/create")
                .param("access_token", accessKey)
                .param("band_key", bandKey)
                .param("content", enContents)
                .param("do_push", "true")
                .param("type", "a")
                .build(), "");
        return s;
    }

    public String newAdressConvertoldAdress(String address) {
        logger.info("cloudId - {}", cloudId);
        logger.info("cloudsecret - {}", cloudsecret);

        String newAdress = address.replaceAll(" ", "");
        List<NaverAddresses> res = JsonToModel.builder(RestCall.getSetHeaderNaverCloud(
                        BandURL.builder().schema(naveropenapiSchema + "://")
                                .baseURL(naveropenapiUrl)
                                .api("/map-geocode/v2/geocode")
                                .param("query", newAdress)
                                .build(), cloudId, cloudsecret)
                )
                .ofArrayModel("addresses", NaverAddresses.class);

        logger.info("aaa = {}", new Gson().toJson(res));
        logger.info("aaa = {}", new Gson().toJson(res.get(0).getJibunAddress()));

        if (res.size() > 0) {
            if (res.get(0).getJibunAddress() != null) {
                return res.get(0).getJibunAddress();
            }
            ;
        }
        return "";
    }


    public String BandBucheonDongValidation(String address) {
        String dongName = newAdressConvertoldAdress(address);
        logger.info("band key get - {}", dongName);
        String resBandKey = "";


        //심공동 구행정구역
        if (dongName.contains("심곡동") || dongName.contains("심곡1동")
                || dongName.contains("심곡2동") || dongName.contains("심곡3동")
                || dongName.contains("원미2동") || dongName.contains("소사동")) {
            resBandKey = BandListType.클린부천심곡동심곡123동원미2동소사동.getBand_key();
        }

        //부천동 행정구역
        if (dongName.contains("부천동") || dongName.contains("원미1동")
                || dongName.contains("역곡1동") || dongName.contains("역곡2동")
                || dongName.contains("춘의동") || dongName.contains("도당동")) {
            resBandKey = BandListType.클린부천부천동원미1동역곡동춘의동도당동.getBand_key();
        }
        //신중동
        if (dongName.contains("약대동") || dongName.contains("신중동")
                || dongName.contains("중1동") || dongName.contains("중2동")
                || dongName.contains("중3동") || dongName.contains("중4동")) {
            resBandKey = BandListType.클린부천신중동약대동중1234동.getBand_key();
        }
        //찾아보자
        //중동
        if (dongName.contains("중동") || dongName.contains("상동")) {

        }
        //상동
        if (dongName.contains("상1동") || dongName.contains("상2동") || dongName.contains("상3동")) {

        }
        //대산동
        if (dongName.contains("대산동") || dongName.contains("심곡본1동") || dongName.contains("심곡본동")
                || dongName.contains("송내1동") || dongName.contains("송내2동")) {
            resBandKey = BandListType.클린부천대산동심곡본심곡본1송내12.getBand_key();
        }
        //소사본동
        if (dongName.contains("소사본동") || dongName.contains("소사본3동")) {
            resBandKey = BandListType.클린부천소사본동소사본동소사본3동.getBand_key();
        }
        //범안동
        if (dongName.contains("범안동") || dongName.contains("범박동") || dongName.contains("괴안동") || dongName.contains("역곡3동")) {

        }
        //성곡동
        if (dongName.contains("성곡동") || dongName.contains("고강본동") || dongName.contains("고강1동")) {
        }
        //오정동
        if (dongName.contains("오정동") || dongName.contains("원종1동") || dongName.contains("원종2동")
                || dongName.contains("오정동") || dongName.contains("신흥동")) {
            resBandKey = BandListType.클린부천오정동오정원종12신흥.getBand_key();
        }

        return resBandKey;
    }


    public void postSend() {

        logger.info("test date -= {}", new Gson().toJson(new TestEntity()));

        RestCall.post(bandSchema + "://" + bandUrl + "/v2.2/band/post/create", new Gson().toJson(new TestEntity()));

    }
}



