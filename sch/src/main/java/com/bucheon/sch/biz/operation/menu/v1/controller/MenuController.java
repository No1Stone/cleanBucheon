package com.bucheon.sch.biz.operation.menu.v1.controller;

import com.bucheon.sch.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuReq;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuUpdate;
import com.bucheon.sch.db.entity.operation.Admin;
import com.bucheon.sch.biz.operation.menu.v1.service.MenuService;
import com.bucheon.sch.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.sch.biz.util.service.UtilService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(path = "/operation/menu")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MenuController {

    private final Logger logger = LoggerFactory.getLogger(MenuController.class);
    private final MenuService menuService;
    private final UtilService utilService;
    private final PermissionService permissionService;

    @Value("${domain.file}")
    private String path;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/operation/menu/menuManage");
    }

    //기본틀
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(MenuReq dto, HttpServletRequest request){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(13L);

        return new ModelAndView("operation/menu/menuManage")
                .addObject("menuPaging", menuService.MenuSelectListPagingInfoService(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(MenuReq dto){
        return new ModelAndView("operation/menu/menuListAjax")
                .addObject("menuList", menuService.MenuSelectListService(dto))
                .addObject("menuPaging", menuService.MenuSelectListPagingInfoService(dto));

    }

    //최초 입력 페이지 팝업 
    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm(){

        MenuReq dto = new MenuReq();
        MenuRes te = new MenuRes();

        return new ModelAndView("operation/menu/menuManagePopupAjax")
                .addObject("menuOne", te)
                .addObject("pmenu1List", menuService.PmenuSelectListService(dto))
                .addObject("pmenu2List", null)
                ;
    }

    //수정 입력 페이지 팝업
    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView modifyForm(Long seq){

        MenuReq dto = new MenuReq();
        MenuReq dto2 = new MenuReq();
        MenuRes te = menuService.MenuSelectOneService(seq);
        dto.setPmenuSeq(te.getPmenuSeq());

        return new ModelAndView("operation/menu/menuManagePopupAjax")
                .addObject("menuOne", menuService.MenuSelectOneService(seq))
                .addObject("pmenu1List", menuService.PmenuSelectListService(dto2))
                .addObject("pmenu2List", menuService.CmenuSelectListService(dto))
                ;
    }

    //저장처리
    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(@RequestParam("uploadfile") MultipartFile[] uploadfile, MenuUpdate dto) throws IllegalStateException, IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        try {
            List<String> fileName = utilService.FileSaveService(uploadfile, "menu");
            dto.setMenuIcon(fileName.get(0));
        } catch (Exception e){
            //
        }

        map.put("result", menuService.MenuSaveService(dto));
        return map;
    }

    //하위메뉴 찾기
    @RequestMapping(path = "/cmenu", method = RequestMethod.POST)
    @ResponseBody
    public Object cMenuInfo(MenuReq dto){

        Object res = menuService.CmenuSelectListService(dto);

        return res;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(@RequestParam("uploadfile") MultipartFile[] uploadfile, MenuUpdate dto) throws IllegalStateException, IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        try {
            List<String> fileName = utilService.FileSaveService(uploadfile, "menu");
            dto.setMenuIcon(fileName.get(0));
        } catch (Exception e){
            //
        }
        map.put("result", menuService.MenuSaveService(dto));
        return map;
    }

    //석제처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(Long[] menuSeq){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", menuService.UpdateMenuDelService(menuSeq));
        return map;
    }

    @RequestMapping(path = "/file")
    @ResponseBody
    public List<String> fileUpload(@RequestParam MultipartFile[] uploadfile) throws IllegalStateException, IOException {
        return menuService.UploadService(uploadfile);
    }

    //왼쪽 사이드 메뉴
    @RequestMapping(path = "/topAdminInfo", method = RequestMethod.GET)
    public ModelAndView topAdminInfoAjax(){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();

        return new ModelAndView("operation/menu/topAdminAjax")
                .addObject("adminInfo", admin)
                ;

    }

    //왼쪽 사이드 메뉴
    @RequestMapping(path = "/sideMenu", method = RequestMethod.GET)
    public ModelAndView sideMenuAjax(int selectMenu1, int selectMenu2){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();

        List<MenuPermissionRes> kk = permissionService.PermissionMenuSelectLv1LiService(admin.getPermissionSeq());
        Object[] kkk = new Object[kk.size()];
        for(int i=0; i < permissionService.PermissionMenuSelectLv1LiService(admin.getPermissionSeq()).size(); i++){

            //System.out.println(">>>>>>>>>>>> getMenuSeq() : " + kk.get(i).getMenuSeq());
            kkk[i] = permissionService.PermissionMenuSelectLv2LiService(admin.getPermissionSeq(), kk.get(i).getMenuSeq());
        }

        return new ModelAndView("operation/menu/sideMenuAjax")
                .addObject("userMenuList", permissionService.PermissionMenuSelectLv1LiService(admin.getPermissionSeq()))
                .addObject("userMenuList2_0", kkk[0])
                .addObject("userMenuList2_1", kkk[1])
                .addObject("userMenuList2_2", kkk[2])
                .addObject("userMenuList2_3", kkk[3])
                .addObject("userMenuList2_4", kkk[4])
                //.addObject("userMenuList2_5", kkk[5])
                .addObject("selectMenu1",selectMenu1)
                .addObject("selectMenu2",selectMenu2)
                ;

    }

}
