package com.bucheon.sch.biz.environmental.report.v1.band.resModel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NaverAddresses {
    private String roadAddress;
    private String jibunAddress;
    private String englishAddress;
    List<AddressElement> addressElements;
    private String x;
    private String y;
    private String distance;
}
