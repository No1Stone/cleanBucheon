package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CarouselItemsButtons {
	
	private String action;
	
	private String label;
	
	private String link;
	
	private String messageText;
	
	private String webLinkUrl;
	
	private String blockId;
	
	private String phoneNumber;
	
	private String osLink;
	
}
