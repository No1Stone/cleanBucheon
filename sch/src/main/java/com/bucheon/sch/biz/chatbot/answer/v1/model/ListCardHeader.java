package com.bucheon.sch.biz.chatbot.answer.v1.model;

import lombok.Data;

@Data
public class ListCardHeader {
	
	private String title;
	
	private String imageUrl;
}
