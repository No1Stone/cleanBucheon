package com.bucheon.sch.biz.metabus.metaUser.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetaTrashTypeUserRes {

    private String userId;
    private String nickName;
    private Long trashSeq;
    private String trashName;
    private String trashAct;
    private int trashPoint;
    private Long cleanCnt;
    private int point;

}
