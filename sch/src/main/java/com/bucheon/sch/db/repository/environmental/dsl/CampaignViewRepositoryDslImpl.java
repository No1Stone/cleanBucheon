package com.bucheon.sch.db.repository.environmental.dsl;

import com.bucheon.sch.biz.environmental.campaign.v1.model.CampaignExcelDto;
import com.bucheon.sch.biz.environmental.campaign.v1.model.CampaignReq;
import com.bucheon.sch.common.type.ReceptionType;
import com.bucheon.sch.db.entity.environmental.QCampaignView;
import com.querydsl.core.types.ConstantImpl;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class CampaignViewRepositoryDslImpl implements CampaignViewRepositoryDsl {


    private final JPAQueryFactory queryFactory;
    private QCampaignView qCampaignView = QCampaignView.campaignView;
    private final Logger logger = LoggerFactory.getLogger(CampaignViewRepositoryDslImpl.class);

    //EXCEL 다운
    @Override
    public  List<CampaignExcelDto> findExcelList(CampaignReq dto) throws NullPointerException, NumberFormatException {
        LocalDateTime yyyymm = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }

        logger.info("re P---- {}",ReceptionType.P.getName());
        logger.info("re B---- {}",ReceptionType.B.getName());
        logger.info("re C---- {}",ReceptionType.C.getName());


        var aa = queryFactory.select(
                        Projections.fields(CampaignExcelDto.class,

                                qCampaignView.campaignNum,
                                qCampaignView.trashName,
                                qCampaignView.campaignStatusName,

                                qCampaignView.areaName,
                                qCampaignView.addrNew,
                                qCampaignView.addrOld,

                                qCampaignView.tRegUserName,
                                qCampaignView.tRegUserPhone,
                                qCampaignView.tRegUserEmail,

                                qCampaignView.tCampaignProcessName,
                                qCampaignView.tRegDt,
                                qCampaignView.tComDt,

                                qCampaignView.tMileage,
                                qCampaignView.tMileageStatusName,
                                qCampaignView.tMileageRejectReason,

                                qCampaignView.cRegUserName,
                                qCampaignView.cRegUserPhone,
                                qCampaignView.cRegUserEmail,

                                qCampaignView.cCampaignProcessName,
                                qCampaignView.cRegDt,
                                qCampaignView.cComDt,

                                qCampaignView.cMileage,
                                qCampaignView.cMileageStatusName,
                                qCampaignView.cMileageRejectReason
                        )
                )
                .from(qCampaignView)
                .where(
                        Expressions.stringTemplate(
                                        "DATE_FORMAT({0}, {1})"
                                        , qCampaignView.tRegDt
                                        , ConstantImpl.create("%Y-%m")).eq(dto.getSelectedExcelYYYYMM()),
                        eqAdministrativeDivision(dto.getSessionAdministrativeDivision())
                        //                        qCampaign.campaignReception.notLike(ReceptionType.A.getName()),

                )
                //.limit(dto.getSize())
                //.offset(dto.getPage()*dto.getSize())
                .orderBy(qCampaignView.tRegDt.desc())
                .fetch();
        return aa;
    }

    private BooleanExpression eqAdministrativeDivision(String st) {

        logger.info("getSessionAdministrativeDivision - {}", st);
        if (StringUtils.isEmptyOrWhitespace(st) || st.equals("전체")) {
            return null;
        }
        return qCampaignView.areaName.eq(st);
    }


}
