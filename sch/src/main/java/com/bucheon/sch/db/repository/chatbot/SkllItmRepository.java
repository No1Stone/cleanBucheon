package com.bucheon.sch.db.repository.chatbot;

import com.bucheon.sch.db.entity.chatbot.SkllItm;
import com.bucheon.sch.db.entity.chatbot.id.SkllItmId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface SkllItmRepository extends JpaRepository<SkllItm, SkllItmId> {

    @Transactional
    @Modifying
    @Query(
            value = "delete from tb_skll_itm where SKLL_CODE =:answerSeq "
            , nativeQuery = true
    )
    int DeleteAnswerSkllItmCode(@Param("answerSeq") String answerSeq);

}
