package com.bucheon.sch.db.repository.chatbot.dsl;

import com.bucheon.sch.biz.chatbot.answer.v1.model.SimpleImageVO;
import com.bucheon.sch.db.entity.chatbot.QSkll;
import com.bucheon.sch.db.entity.chatbot.QSkllImage;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

@Aspect
@RequiredArgsConstructor
public class SkllImageRepositoryDslImpl implements SkllImageRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QSkll qSkll = QSkll.skll;
    private QSkllImage qSkllImage = QSkllImage.skllImage;

    @Override
    public SimpleImageVO findBySkllSiCodeAndRspnsOrdr(String code, int reqSeq) {

        SimpleImageVO result = queryFactory.select(
                        Projections.bean(SimpleImageVO.class,
                                qSkllImage.skllCode.as("skillCode"),
                                qSkllImage.imageUrl.as("imageUrl"),
                                qSkllImage.imageDc.as("imageDc"),
                                qSkllImage.rspnsOrdr.as("resSeq")
                        )
                )
                .from(qSkllImage)
                .where(eqId(code), eqRspnsOrder(reqSeq))
                .fetchOne();
        return result;

    }

    private BooleanExpression eqId(String id) {
        if(StringUtils.isEmptyOrWhitespace(id)){
            return null;
        }
        return qSkllImage.skllCode.eq(String.valueOf(id));
    }

   private BooleanExpression eqRspnsOrder(int reqSeq) {
        if(reqSeq == 0){
            return null;
        }
        return qSkllImage.rspnsOrdr.eq(reqSeq);
   }

}
