package com.bucheon.sch.db.repository.environmental.dsl;

import com.bucheon.sch.biz.environmental.campaign.v1.controller.test14;
import com.bucheon.sch.biz.environmental.complaint.v1.model.ComplaintsReq;
import com.bucheon.sch.db.entity.environmental.Petition;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface PetitionRepositoryDsl {

    @Transactional(readOnly = true)
    List<Petition> DynamicSelect(ComplaintsReq dto);

    @Transactional(readOnly = true)
    QueryResults<Petition> DynamicSelectPaging(ComplaintsReq dto);

    @Transactional(readOnly = true)
    List<test14> DslTest1(ComplaintsReq dto);

}
