package com.bucheon.sch.db.repository.chatbot;

import com.bucheon.sch.db.entity.chatbot.SkllCard;
import com.bucheon.sch.db.repository.chatbot.dsl.SkllCardRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface SkllCardRepository extends JpaRepository<SkllCard, String>, SkllCardRepositoryDsl {

    Optional<SkllCard> findBySkllCodeAndRspnsOrdr(String code, int reqSeq);

    @Transactional
    @Modifying
    @Query(
            value = "delete from tb_skll_card where SKLL_CODE =:answerSeq "
            , nativeQuery = true
    )
    int DeleteAnswerSkllCardCode(@Param("answerSeq") String answerSeq);

    @Transactional
    @Modifying
    @Query(
            value = "insert into tb_skll_card (SKLL_CODE ,SJ ,THUMB, PROFL, SOCTY, BTTON ,BC_ORDR ,RSPNS_ORDR ,DC ,BTTON_OPTN_AT ,PROFL_CRTFC_AT)" +
                    " values (:skllCode, :sj, :thumb, :profl, :socty, :btton, :bcOrdr, :rspnsOrdr ,:dc ,:bttonOptnAt ,:proflCrtfcAt) "
            , nativeQuery = true
    )
    void SaveAnswerSkllCardCode(@Param("skllCode") String skllCode,
                                @Param("sj") String sj,
                                @Param("dc") String dc,
                                @Param("thumb") String thumb,
                                @Param("profl") String profl,
                                @Param("socty") String socty,
                                @Param("btton") String btton,
                                @Param("bcOrdr") int bcOrdr,
                                @Param("rspnsOrdr") int rspnsOrdr,
                                @Param("bttonOptnAt") String bttonOptnAt,
                                @Param("proflCrtfcAt") String proflCrtfcAt

    );
}
