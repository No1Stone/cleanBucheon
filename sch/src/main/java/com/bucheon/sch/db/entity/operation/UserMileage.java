package com.bucheon.sch.db.entity.operation;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_user_mileage")
@Getter
@Setter
@NoArgsConstructor
public class UserMileage {

    @Id
    @Column(name = "user_id", nullable = true)
    private String userId;
    @Column(name = "mileage_sum", nullable = true)
    private int mileageSum;
    @Column(name = "mileage_seq", nullable = true)
    private Long mileageSeq;

    @Builder
    UserMileage(
            String userId,
            int mileageSum,
            Long mileageSeq
    ) {
        this.userId = userId;
        this.mileageSum = mileageSum;
        this.mileageSeq = mileageSeq;
    }


}
