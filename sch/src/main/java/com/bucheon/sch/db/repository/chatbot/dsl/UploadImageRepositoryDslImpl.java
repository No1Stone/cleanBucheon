package com.bucheon.sch.db.repository.chatbot.dsl;

import com.bucheon.sch.biz.chatbot.uploadImage.v1.model.UploadImageVO;
import com.bucheon.sch.db.entity.chatbot.QUploadImage;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class UploadImageRepositoryDslImpl implements UploadImageRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QUploadImage qUploadImage = QUploadImage.uploadImage;


    @Override
    public List<UploadImageVO> SelectListUploadImages(UploadImageVO dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        List<UploadImageVO> result = queryFactory.select(
                Projections.bean(UploadImageVO.class,
                                qUploadImage.imageSeq,
                                qUploadImage.imageId,
                                qUploadImage.imageName,
                                qUploadImage.imageFilename,
                                qUploadImage.imageFileservername,
                                qUploadImage.imageDefaultYn,
                                qUploadImage.imageAttribute
                        )
                )
                .from(qUploadImage)
                .where(eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()))
                .orderBy(qUploadImage.imageSeq.desc())
                .offset(dto.getPage()*dto.getSize())
                .limit(dto.getSize())
                .fetch();
        return result;
    }

    @Override
    public QueryResults<UploadImageVO> SelectListUploadImagesPaging(UploadImageVO dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        QueryResults<UploadImageVO> result = queryFactory.select(
                        Projections.bean(UploadImageVO.class,
                                qUploadImage.imageSeq,
                                qUploadImage.imageId,
                                qUploadImage.imageName,
                                qUploadImage.imageFilename,
                                qUploadImage.imageFileservername,
                                qUploadImage.imageDefaultYn,
                                qUploadImage.imageAttribute
                        )
                )
                .from(qUploadImage)
                .where(eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()))
                .orderBy(qUploadImage.imageSeq.desc())
                .offset(dto.getPage()*dto.getSize())
                .limit(dto.getSize())
                .fetchResults();
        return result;

    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchsearchSelect(String searchSelect, String searchString) {

        try {

            if (searchSelect.equals("searchName")) {
                return eqName(searchString);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqName(String name) {
        if(StringUtils.isEmptyOrWhitespace(name)){
        return null;
        }
        return qUploadImage.imageName.contains(name);
    }

}
