package com.bucheon.sch.db.repository.metabus;

import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaAllRankingRes;
import com.bucheon.sch.db.entity.metabus.MetaUser;
import com.bucheon.sch.db.repository.metabus.dsl.MetaUserRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface MetaUserRepository extends JpaRepository<MetaUser, String>, MetaUserRepositoryDsl {

    @Transactional(readOnly = true)
    int countByUserId(String userId);
    @Transactional(readOnly = true)
    int countByProviderAndSnsId(String provider, String snsId);
    @Transactional(readOnly = true)
    int countBySnsId(String snsId);
    @Transactional(readOnly = true)
    int countByNickname(String nickName);
    @Transactional(readOnly = true)
    int countByEmail(String email);
    @Transactional(readOnly = true)
    int countByCleantownUid(String mid);



    @Transactional(readOnly = true)
    Optional<MetaUser> findByUserId(String userId);
    @Transactional(readOnly = true)
    Optional<MetaUser> findBySnsId(String snsId);
    @Transactional(readOnly = true)
    Optional<MetaUser> findByNickname(String nickname);
    @Transactional(readOnly = true)
    Optional<MetaUser> findByEmail(String email);
    @Transactional(readOnly = true)
    Optional<MetaUser> findByProviderAndSnsId(String provider, String snsId);
    @Transactional(readOnly = true)
    Optional<MetaUser> findByDeviceToken(String device);

    @Transactional(readOnly = true)
    List<MetaUser> findAllByDelYn(String del);
    @Transactional(readOnly = true)
    List<MetaUser> findAllByDeviceTokenNotNull();


    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_user set nickname = :nickname where userId =:userId "
            , nativeQuery = true
    )
    int updateNickname(@Param("nickname") String nickname, @Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_user set email = :email where userId =:userId "
            , nativeQuery = true
    )
    int updateEmail(@Param("email") String email, @Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_user set provider = :provider where userId =:userId "
            , nativeQuery = true
    )
    int updateProvider(@Param("provider") String provider, @Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_user set snsId = :snsId where userId =:userId "
            , nativeQuery = true
    )
    int updateSnsId(@Param("snsId") String snsId, @Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_user set avatarType = :avatarType where userId =:userId "
            , nativeQuery = true
    )
    int updateAvatarType(@Param("avatarType") int avatarType, @Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_user set cleanPoint = cleanPoint + :cleanPoint where userId =:userId "
            , nativeQuery = true
    )
    int updateCleanPoint(@Param("cleanPoint") int cleanPoint, @Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_user set deviceToken = :deviceToken where userId =:userId "
            , nativeQuery = true
    )
    int updateDeviceToken(@Param("deviceToken") String deviceToken, @Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_user set cleantownUid = :cleantownUid where userId =:userId "
            , nativeQuery = true
    )
    int updateCleantownUid(@Param("cleantownUid") String cleantownUid, @Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_user set delYn = :delYn where userId =:userId "
            , nativeQuery = true
    )
    int updateDelYn(@Param("delYn") String delYn, @Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_user set updatedAt = now() where userId =:userId "
            , nativeQuery = true
    )
    int updateUpdatedAt(@Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_user set lastConnectAt = now() where userId =:userId "
            , nativeQuery = true
    )
    int updateLastConnect( @Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_user set delYn = :delYn where userId =:userId "
            , nativeQuery = true
    )
    int UpdateMetaUserDelYn(@Param("delYn") String delYn, @Param("userId") String userId);

    //랭킹관련
    @Query(
            value = "SELECT " +
                    "@Rownum \\:= @Rownum + 1 AS RankNum, " +
                    "aa.* FROM ( " +
                        "SELECT " +
                        "a.user_id as userId, b.nickname, sum(a.point) AS sumPoint " +
                        "FROM " +
                        "tb_meta_user_point_history a " +
                        "LEFT OUTER JOIN tb_meta_user b ON a.user_id = b.userId " +
                        "WHERE DATE_FORMAT(a.reg_dt, '%Y-%m') =:yyyymm " +
                        "GROUP BY user_id " +
                    ") aa, (SELECT @Rownum \\:= 0) rownumT " +
                    "ORDER BY aa.sumPoint DESC " +
                    "LIMIT 0, 100;"
            , nativeQuery = true
    )
    List<MetaAllRankingRes> selectAllRankingList(@Param("yyyymm") String yyyymm);

    //나의 랭킹관련
    @Query(
            value = "SELECT * FROM " +
                    "( " +
                    "   SELECT " +
                    "       @Rownum \\:= @Rownum + 1 AS RankNum, " +
                    "       aa.* FROM ( " +
                    "           SELECT " +
                    "               a.user_id as userId, b.nickname, sum(a.point) AS sumPoint " +
                    "           FROM " +
                    "               tb_meta_user_point_history a " +
                    "                   LEFT OUTER JOIN tb_meta_user b ON a.user_id = b.userId " +
                    "           WHERE DATE_FORMAT(a.reg_dt, '%Y-%m') =:yyyymm " +
                    "           GROUP BY user_id " +
                    "       ) aa, (SELECT @Rownum \\:= 0) rownumT " +
                    "     ORDER BY aa.sumPoint DESC " +
                    ") a3 " +
                    "where a3.userId =:userId " +
                    "limit 0,1 " +
                    ";"
            , nativeQuery = true
    )
    MetaAllRankingRes selectMyRankingList(@Param("userId") String userId, @Param("yyyymm") String yyyymm);

    //관리자 랭킹관련
    @Query(
            value = "SELECT " +
                    "@Rownum \\:= @Rownum + 1 AS RankNum, " +
                    "aa.* FROM ( " +
                    "SELECT " +
                    "a.user_id as userId, b.nickname, b.lastConnectAt, sum(a.point) AS sumPoint " +
                    "FROM " +
                    "tb_meta_user_point_history a " +
                    "LEFT OUTER JOIN tb_meta_user b ON a.user_id = b.userId " +
                    "WHERE DATE_FORMAT(a.reg_dt, '%Y-%m') =:yyyymm " +
                    "GROUP BY user_id " +
                    ") aa, (SELECT @Rownum \\:= 0) rownumT " +
                    "ORDER BY aa.sumPoint DESC " +
                    "LIMIT :start, :size " +
                    ";"
            , nativeQuery = true
    )
    List<MetaAllRankingRes> selectAdminAllRankingList(@Param("yyyymm") String yyyymm, @Param("start") int start, @Param("size") int size);

}
