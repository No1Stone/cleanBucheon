package com.bucheon.sch.db.entity.operation.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class AdminId implements Serializable {
    private Long adminSeq;
    private Long permissionSeq;
}
