package com.bucheon.sch.db.entity.environmental;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_campaign")
@Getter
@Setter
@NoArgsConstructor
public class Campaign {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "campaign_seq", nullable = true)
    private Long campaignSeq;
    @Column(name = "campaign_num", nullable = true)
    private String campaignNum;
    @Column(name = "campaign_status", nullable = true)
    private String campaignStatus;
    @Column(name = "campaign_process", nullable = true)
    private String campaignProcess;
    @Column(name = "campaign_process_reject_reason", nullable = true)
    private String campaignProcessRejectReason;
    @Column(name = "reg_user_did", nullable = true)
    private String regUserDid;
    @Column(name = "trash_type", nullable = true)
    private Long trashType;
    @Column(name = "location_x", nullable = true)
    private String locationX;
    @Column(name = "location_y", nullable = true)
    private String locationY;
    @Column(name = "addr_new", nullable = true)
    private String addrNew;
    @Column(name = "addr_old", nullable = true)
    private String addrOld;
    @Column(name = "area_name", nullable = true)
    private String areaName;
    @Column(name = "mileage", nullable = true)
    private String mileage;
    @Column(name = "mileage_status", nullable = true)
    private String mileageStatus;
    @Column(name = "mileage_reject_reason", nullable = true)
    private String mileageRejectReason;
    @Column(name = "del_yn", nullable = true)
    private String delYn;
    @Column(name = "com_dt", nullable = true)
    private LocalDateTime comDt;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;

    @Builder
    Campaign(Long campaignSeq,
             String campaignNum,
             String campaignStatus,
             String campaignProcess,
             String campaignProcessRejectReason,
             String regUserDid,
             Long trashType,
             String locationX,
             String locationY,
             String addrNew,
             String addrOld,
             String areaName,
             String mileage,
             String mileageStatus,
             String mileageRejectReason,
             String delYn,
             LocalDateTime comDt,
             LocalDateTime regDt
    ) {
        this.campaignSeq = campaignSeq;
        this.campaignNum = campaignNum;
        this.campaignStatus = campaignStatus;
        this.campaignProcess = campaignProcess;
        this.campaignProcessRejectReason = campaignProcessRejectReason;
        this.regUserDid = regUserDid;
        this.trashType = trashType;
        this.locationX = locationX;
        this.locationY = locationY;
        this.addrNew = addrNew;
        this.addrOld = addrOld;
        this.areaName = areaName;
        this.mileage = mileage;
        this.mileageStatus = mileageStatus;
        this.mileageRejectReason = mileageRejectReason;
        this.delYn = delYn;
        this.comDt = comDt;
        this.regDt = regDt;

    }


}
