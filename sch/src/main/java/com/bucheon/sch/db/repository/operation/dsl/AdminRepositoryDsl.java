package com.bucheon.sch.db.repository.operation.dsl;

import com.bucheon.sch.biz.operation.admin.v1.model.AdminReq;
import com.bucheon.sch.db.entity.operation.Admin;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AdminRepositoryDsl {

    @Transactional(readOnly = true)
    List<Admin> AdminSelectList(AdminReq dto);

    @Transactional(readOnly = true)
    QueryResults<Admin> AdminSelectListPaging(AdminReq dto);
}
