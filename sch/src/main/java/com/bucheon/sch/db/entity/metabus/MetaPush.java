package com.bucheon.sch.db.entity.metabus;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_meta_push")
@Getter
@Setter
@NoArgsConstructor
public class MetaPush {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "send_seq", nullable = true)
    private Long sendSeq;
    @Column(name = "send_type", nullable = true)
    private String sendType;
    @Column(name = "result_type", nullable = true)
    private String resultType;
    @Column(name = "title", nullable = true)
    private String title;
    @Column(name = "body", nullable = true)
    private String body;
    @Column(name = "send_cnt", nullable = true)
    private int sendCnt;
    @Column(name = "reg_seq", nullable = true)
    private Long regSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "send_dt", nullable = true)
    private LocalDateTime sendDt;

    @Builder
    MetaPush(
            Long sendSeq,
            String sendType,
            String resultType,
            String title,
            String body,
            int sendCnt,
            Long regSeq,
            LocalDateTime regDt,
            LocalDateTime sendDt
            ) {
        this.sendSeq = sendSeq;
        this.sendType = sendType;
        this.resultType = resultType;
        this.title = title;
        this.body = body;
        this.sendCnt = sendCnt;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.sendDt = sendDt;
    }

}
