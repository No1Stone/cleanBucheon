package com.bucheon.sch.db.repository.environmental.dsl;


import com.bucheon.sch.biz.environmental.campaign.v1.model.CampaignReq;
import com.bucheon.sch.biz.environmental.campaign.v1.model.CampaignRes;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CampaignRepositoryDsl {

    @Transactional(readOnly = true)
    List<CampaignRes> SelectListCampaign(CampaignReq dto);
    @Transactional(readOnly = true)
    QueryResults<CampaignRes> SelectListCampaignPaging(CampaignReq dto);
//    @Transactional(readOnly = true)
//    List<Report> SelectAppListReport(ReportListReq dto);
//    @Transactional(readOnly = true)
//    QueryResults<Report> SelectAppListReportPaging(ReportListReq dto);
//
//    //    @Transactional(readOnly = true)
////    List<Report> SelectListDetailReport();

}
