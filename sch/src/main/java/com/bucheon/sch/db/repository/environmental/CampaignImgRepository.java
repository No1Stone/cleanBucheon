package com.bucheon.sch.db.repository.environmental;

import com.bucheon.sch.db.entity.environmental.CampaignImg;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CampaignImgRepository extends JpaRepository<CampaignImg, Long> {

    boolean existsByCampaignSeq(Long seq);

    List<CampaignImg> findByCampaignSeq(Long reSeq);

}
