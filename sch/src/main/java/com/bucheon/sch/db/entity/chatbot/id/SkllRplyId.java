package com.bucheon.sch.db.entity.chatbot.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SkllRplyId implements Serializable {
    private String skllCode;
    private int qrOrdr;
}
