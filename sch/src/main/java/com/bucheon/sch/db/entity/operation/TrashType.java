package com.bucheon.sch.db.entity.operation;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_trash_type")
@Getter
@Setter
@NoArgsConstructor
public class TrashType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trash_seq", nullable = true)
    private Long trashSeq;
    @Column(name = "trash_name", nullable = true)
    private String trashName;
    @Column(name = "declaration_mileage", nullable = true)
    private int declarationMileage;
    @Column(name = "process_mileage", nullable = true)
    private int processMileage;
    @Column(name = "icon_url", nullable = true)
    private String iconUrl;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "del_yn", nullable = true)
    private String delYn;
    @Column(name = "reg_seq", nullable = true)
    private Long regSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_seq", nullable = true)
    private Long modSeq;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;


}
