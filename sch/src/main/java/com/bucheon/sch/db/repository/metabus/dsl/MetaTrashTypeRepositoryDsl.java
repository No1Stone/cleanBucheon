package com.bucheon.sch.db.repository.metabus.dsl;


import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaTrashTypeReq;
import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaTrashTypeUserRes;
import com.bucheon.sch.db.entity.metabus.MetaTrashType;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MetaTrashTypeRepositoryDsl {

    @Transactional(readOnly = true)
    List<MetaTrashTypeUserRes> selectListMetaUser(String userId);

    @Transactional(readOnly = true)
    List<MetaTrashType> MetaTrashTypeSelectList(MetaTrashTypeReq dto);
    @Transactional(readOnly = true)
    QueryResults<MetaTrashType> MetaTrashTypeSelectListPaging(MetaTrashTypeReq dto);
}
