package com.bucheon.sch.db.repository.chatbot.dsl;

import com.bucheon.sch.biz.chatbot.answer.v1.model.SimpleImageVO;
import org.springframework.transaction.annotation.Transactional;

public interface SkllImageRepositoryDsl {

    @Transactional(readOnly = true)
    SimpleImageVO findBySkllSiCodeAndRspnsOrdr(String code, int reqSeq);
}
