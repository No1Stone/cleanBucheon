package com.bucheon.sch.db.entity.environmental;

import com.bucheon.sch.db.entity.environmental.id.ProcessId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "process")
@Getter
@Setter
@NoArgsConstructor
@IdClass(ProcessId.class)
public class Process {
    @Id
    @Column(name = "peti_no", nullable = true)
    private String petiNo;

    @Column(name = "civil_no", nullable = true)
    private String civilNo;
    @Column(name = "reg_date_time", nullable = true)
    private String regDateTime;
    @Column(name = "duty_id", nullable = true)
    private String dutyId;
    @Column(name = "duty_dept_code", nullable = true)
    private String dutyDeptCode;
    @Column(name = "duty_name", nullable = true)
    private String dutyName;
    @Column(name = "do_date_time", nullable = true)
    private String doDateTime;
    @Column(name = "status_code", nullable = true)
    private String statusCode;
    @Id
    @Column(name = "status_name", nullable = true)
    private String statusName;
    @Column(name = "answer", nullable = true)
    private String answer;

    @Builder
    Process(String petiNo,
            String civilNo,
            String regDateTime,
            String dutyId,
            String dutyDeptCode,
            String dutyName,
            String doDateTime,
            String statusCode,
            String statusName,
            String answer
    ) {
        this.petiNo = petiNo;
        this.civilNo = civilNo;
        this.regDateTime = regDateTime;
        this.dutyId = dutyId;
        this.dutyDeptCode = dutyDeptCode;
        this.dutyName = dutyName;
        this.doDateTime = doDateTime;
        this.statusCode = statusCode;
        this.statusName = statusName;
        this.answer = answer;
    }

}
