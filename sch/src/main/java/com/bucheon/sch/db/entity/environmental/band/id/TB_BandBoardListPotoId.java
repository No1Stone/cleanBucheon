package com.bucheon.sch.db.entity.environmental.band.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TB_BandBoardListPotoId implements Serializable {

    private String postKey;
    private String photoKey;



}
