package com.bucheon.sch.db.repository.chatbot.dsl;

import com.bucheon.sch.biz.chatbot.answer.v1.model.QuickRepliesVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SkllRplyRepositoryDsl {

    @Transactional(readOnly = true)
    List<QuickRepliesVO> selectQuickReplies(String code);
}
