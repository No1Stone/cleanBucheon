package com.bucheon.sch.db.repository.metabus.dsl;


import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaUserPointHistoryReq;
import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaUserPointHistoryRes;
import com.bucheon.sch.db.entity.metabus.MetaUserPointHistory;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MetaUserPointHistoryRepositoryDsl {

    @Transactional(readOnly = true)
    List<MetaUserPointHistory> SelectListMyMetaUserPointHistory(MetaUserPointHistoryReq dto);
    @Transactional(readOnly = true)
    QueryResults<MetaUserPointHistory> SelectListMyMetaUserPointHistoryPaging(MetaUserPointHistoryReq dto);
    @Transactional(readOnly = true)
    List<MetaUserPointHistory> SelectListMyMetaUserPointHistoryAll(MetaUserPointHistoryReq dto);

    @Transactional(readOnly = true)
    List<MetaUserPointHistoryRes> MetaUserPointHistorySelectList(MetaUserPointHistoryReq dto);
    @Transactional(readOnly = true)
    QueryResults<MetaUserPointHistoryRes> MetaUserPointHistorySelectListPaging(MetaUserPointHistoryReq dto);

}
