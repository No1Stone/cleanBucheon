package com.bucheon.sch.db.repository.environmental;

import com.bucheon.sch.db.entity.environmental.Complaints;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComplaintsRepository extends JpaRepository<Complaints, Long> {
}
