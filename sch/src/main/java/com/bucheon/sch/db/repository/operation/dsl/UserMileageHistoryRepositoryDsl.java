package com.bucheon.sch.db.repository.operation.dsl;

import com.bucheon.sch.biz.operation.mileage.v1.model.UserMileageHistoryVO;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserMileageHistoryRepositoryDsl {

    @Transactional(readOnly = true)
    List<UserMileageHistoryVO> UserMileageHistorySelectList(UserMileageHistoryVO dto);


    @Transactional(readOnly = true)
    QueryResults<UserMileageHistoryVO> UserMileageHistorySelectListPaging(UserMileageHistoryVO dto);
}
