package com.bucheon.sch.db.repository.metabus.dsl;


import com.bucheon.sch.biz.metabus.metaPush.v1.model.MetaPushReq;
import com.bucheon.sch.db.entity.metabus.MetaPush;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MetaPushRepositoryDsl {

    @Transactional(readOnly = true)
    List<MetaPush> SelectListMetaPush(MetaPushReq dto);

    @Transactional(readOnly = true)
    QueryResults<MetaPush> SelectListMetaPushPaging(MetaPushReq dto);


}
