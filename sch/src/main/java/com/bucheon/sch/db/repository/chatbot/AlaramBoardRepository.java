package com.bucheon.sch.db.repository.chatbot;

import com.bucheon.sch.db.entity.chatbot.TB_AlaramBoard;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface AlaramBoardRepository extends JpaRepository<TB_AlaramBoard, Long> {

    List<TB_AlaramBoard> findByPhoneNumberOrderByAlaramBoardDtDesc(String phone);
    List<TB_AlaramBoard> findByPhoneNumberAndAlaramBoardDtGreaterThanAndAlaramBoardDtLessThanOrderByAlaramBoardDtDesc(String phone, LocalDateTime sdate, LocalDateTime edate, Pageable pageable);


}
