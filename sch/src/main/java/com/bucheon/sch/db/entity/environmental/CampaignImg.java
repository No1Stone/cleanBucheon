package com.bucheon.sch.db.entity.environmental;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_campaign_img")
@Getter
@Setter
@NoArgsConstructor
public class CampaignImg {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "campaign_img_seq", nullable = true)
    private Long campaignImgSeq;
    @Column(name = "campaign_seq", nullable = true)
    private Long campaignSeq;
    @Column(name = "campaign_img", nullable = true)
    private String campaignImg;

    @Builder
    CampaignImg(Long campaignImgSeq,
                Long campaignSeq,
                String campaignImg) {
        this.campaignImgSeq = campaignImgSeq;
        this.campaignSeq = campaignSeq;
        this.campaignImg = campaignImg;
    }

}
