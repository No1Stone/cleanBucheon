package com.bucheon.sch.db.repository.operation;

import com.bucheon.sch.db.entity.operation.PersonalInfoLog;
import com.bucheon.sch.db.repository.operation.dsl.PersonalInfoLogRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PersonalInfoLogRepository extends JpaRepository<PersonalInfoLog, Long> , PersonalInfoLogRepositoryDsl {

    Optional<PersonalInfoLog> findByLogSeq(Long seq);
    Boolean existsByLogSeq(Long seq);
}
