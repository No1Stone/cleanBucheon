package com.bucheon.sch.db.repository.metabus;

import com.bucheon.sch.db.entity.metabus.MetaBoardAtt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MetaBoardAttRepository extends JpaRepository<MetaBoardAtt, Long> {

    boolean existsByBoardSeq(Long seq);


    List<MetaBoardAtt> findByBoardSeq(Long reSeq);

    int countByBoardSeq(Long seq);

    @Transactional
    int deleteByBoardSeq(Long reSeq);

    @Transactional
    int deleteByBoardAttSeq(Long reSeq);



}
