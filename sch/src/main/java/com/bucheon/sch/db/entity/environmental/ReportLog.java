package com.bucheon.sch.db.entity.environmental;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_report_log")
@Getter
@Setter
@NoArgsConstructor
public class ReportLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "log_seq", nullable = true)
    private Long logSeq;
    @Column(name = "log_time", nullable = true)
    private LocalDateTime logTime;
    @Column(name = "report_seq", nullable = true)
    private Long reportSeq;
    @Column(name = "reception_num", nullable = true)
    private String receptionNum;
    @Column(name = "report_status", nullable = true)
    private String reportStatus;
    @Column(name = "report_damdang", nullable = true)
    private Long reportDamdang;
    @Column(name = "msg", nullable = true)
    private String msg;
    @Column(name = "rejcet_reason", nullable = true)
    private String reportPointRejectReason;


    @Builder
    ReportLog(Long logSeq,
              LocalDateTime logTime,
              Long reportSeq,
              String receptionNum,
              String reportStatus,
              Long reportDamdang,
              String msg,
              String reportPointRejectReason
    ) {
        this.logSeq = logSeq;
        this.logTime = logTime;
        this.reportSeq = reportSeq;
        this.receptionNum = receptionNum;
        this.reportDamdang = reportDamdang;
        this.msg = msg;
        this.reportPointRejectReason = reportPointRejectReason;
    }

}
