package com.bucheon.sch.db.repository.metabus;

import com.bucheon.sch.db.entity.metabus.MetaPush;
import com.bucheon.sch.db.repository.metabus.dsl.MetaPushRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

public interface MetaPushRepository extends JpaRepository<MetaPush, String>, MetaPushRepositoryDsl {

    @Transactional
    int deleteBySendSeq(Long seq);

    @Transactional(readOnly = true)
    List<MetaPush> findByResultTypeAndSendDtBetween(String type, LocalDateTime sdate, LocalDateTime edate);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_push set result_type = 'C' where send_seq =:sendSeq "
            , nativeQuery = true
    )
    int UpdateResultType(@Param("sendSeq") Long sendSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_push set send_cnt = :sendCnt where send_seq =:sendSeq "
            , nativeQuery = true
    )
    int UpdateSendCnt(@Param("sendCnt") int sendCnt, @Param("sendSeq") Long sendSeq);


}
