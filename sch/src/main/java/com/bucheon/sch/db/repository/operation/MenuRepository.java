package com.bucheon.sch.db.repository.operation;

import com.bucheon.sch.db.entity.operation.Menu;
import com.bucheon.sch.db.repository.operation.dsl.MenuRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface MenuRepository extends JpaRepository<Menu, Long> , MenuRepositoryDsl {
    List<Menu> findByUseYnAndDelYn(String use, String del);
    List<Menu> findByMenuLevel(int level);

    List<Menu> findByMenuSeqInAndUseYnAndDelYnAndMenuLevel(List<Long> munus, String use, String del, int levle);
    List<Menu> findByMenuSeqInAndUseYnAndDelYn(List<Long> munus, String use, String del);

    Optional<Menu> findByMenuSeqAndDelYnOrderByRegDtDesc(Long seq, String del);



    @Transactional
    @Modifying
    @Query(
            value = "update tb_menu set menu_name = :menuName where menu_seq =:menuSeq "
            , nativeQuery = true
    )
    int updateMenuName(@Param("menuName") String menuName, @Param("menuSeq") Long menuSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_menu set menu_level = :menuLevel where menu_seq =:menuSeq "
            , nativeQuery = true
    )
    int updateMenuLevle(@Param("menuLevel") int menuLevel, @Param("menuSeq") Long menuSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_menu set pmenu_seq = :pmenuSeq where menu_seq =:menuSeq "
            , nativeQuery = true
    )
    int updateMenuPmenu(@Param("pmenuSeq") Long pmenuSeq, @Param("menuSeq") Long menuSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_menu set menu_url = :menuUrl where menu_seq =:menuSeq "
            , nativeQuery = true
    )
    int updateMenuURL(@Param("menuUrl") String menuUrl, @Param("menuSeq") Long menuSeq);


    @Transactional
    @Modifying
    @Query(
            value = "update tb_menu set menu_icon = :menuIcon where menu_seq =:menuSeq "
            , nativeQuery = true
    )
    int updateMenuIcon(@Param("menuIcon") String menuIcon, @Param("menuSeq") Long menuSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_menu set menu_desc = :menuDesc where menu_seq =:menuSeq "
            , nativeQuery = true
    )
    int updateMenuDesc(@Param("menuDesc") String menuDesc, @Param("menuSeq") Long menuSeq);


    @Transactional
    @Modifying
    @Query(
            value = "update tb_menu set order_num = :orderNum where menu_seq =:menuSeq "
            , nativeQuery = true
    )
    int updateMenuOrderNum(@Param("orderNum") int orderNum, @Param("menuSeq") Long menuSeq);


    @Transactional
    @Modifying
    @Query(
            value = "update tb_menu set use_yn = :useYn where menu_seq =:menuSeq "
            , nativeQuery = true
    )
    int updateMenuUseYn(@Param("useYn") String useYn, @Param("menuSeq") Long menuSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_menu set del_yn = :delYn where menu_seq =:menuSeq "
            , nativeQuery = true
    )
    int updateMenuDelYn(@Param("delYn") String delYn, @Param("menuSeq") Long menuSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_menu set mod_seq = :modSeq, mod_dt = now() where menu_seq =:menuSeq "
            , nativeQuery = true
    )
    int updateMod(@Param("modSeq") Long modSeq, @Param("menuSeq") Long menuSeq);



}
