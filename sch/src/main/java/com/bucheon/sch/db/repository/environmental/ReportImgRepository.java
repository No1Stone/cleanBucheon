package com.bucheon.sch.db.repository.environmental;

import com.bucheon.sch.db.entity.environmental.ReportImg;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReportImgRepository extends JpaRepository<ReportImg, Long> {

    boolean existsByReportSeq(Long seq);

    List<ReportImg> findByReportSeq(Long reSeq);




}
