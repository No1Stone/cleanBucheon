package com.bucheon.sch.db.entity.etc.id;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
public class ConnectSumLogId implements Serializable {
    private LocalDate connectDate;
    private String userId;
}
