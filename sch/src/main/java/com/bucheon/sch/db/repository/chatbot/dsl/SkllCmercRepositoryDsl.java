package com.bucheon.sch.db.repository.chatbot.dsl;

import com.bucheon.sch.biz.chatbot.answer.v1.model.CommerceCardVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SkllCmercRepositoryDsl {

    @Transactional(readOnly = true)
    List<CommerceCardVO> selectSkllCcCodeAndRspnsOrdr(String code, int reqSeq);
}
