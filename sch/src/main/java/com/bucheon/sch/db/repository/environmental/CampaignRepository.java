package com.bucheon.sch.db.repository.environmental;

import com.bucheon.sch.db.entity.environmental.Campaign;
import com.bucheon.sch.db.repository.environmental.dsl.CampaignRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

public interface CampaignRepository extends JpaRepository<Campaign, Long>, CampaignRepositoryDsl {


    @Transactional(readOnly = true)
    Campaign findByCampaignSeq(Long seq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set campaign_process = :campaignProcess where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateCampaignProcess(@Param("campaignProcess") String campaignProcess, @Param("campaignSeq") Long campaignSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set campaign_process_reject_reason = :campaignProcessRejectReason where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateCampaignProcessRejectReason(@Param("campaignProcessRejectReason") String campaignProcessRejectReason, @Param("campaignSeq") Long campaignSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set mileage = :mileage where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateMileage(@Param("mileage") int mileage, @Param("campaignSeq") Long campaignSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set mileage_status = :mileageStatus where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateMileageStatus(@Param("mileageStatus") String mileageStatus, @Param("campaignSeq") Long campaignSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set mileage_reject_reason = :mileageRejectReason where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateMileageRejectReason(@Param("mileageRejectReason") String mileageRejectReason, @Param("campaignSeq") Long campaignSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set com_dt = :comDt where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateComDt(@Param("comDt") LocalDateTime comDt, @Param("campaignSeq") Long campaignSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set del_yn = :delYn where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateCampaignDel(@Param("delYn") String delYn, @Param("campaignSeq") Long campaignSeq);

    Optional<Campaign> findByCampaignSeqAndDelYnOrderByRegDtDesc(Long seq, String del);
}
