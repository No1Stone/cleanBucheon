package com.bucheon.sch.db.repository.etc;

import com.bucheon.sch.db.entity.etc.CmnCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CmnCodeRepository extends JpaRepository<CmnCode, Long> {

    Optional<CmnCode> findByColumnAndCode(String column, String code);
}
