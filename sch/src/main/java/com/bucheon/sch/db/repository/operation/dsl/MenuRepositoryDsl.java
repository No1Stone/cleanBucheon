package com.bucheon.sch.db.repository.operation.dsl;

import com.bucheon.sch.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuReq;
import com.bucheon.sch.db.entity.operation.Menu;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MenuRepositoryDsl {
    @Transactional(readOnly = true)
    List<Menu> menuListSelect(MenuReq dto);
    @Transactional(readOnly = true)
    List<Menu> menuUseListSelect(MenuReq dto);
    @Transactional(readOnly = true)
    List<MenuPermissionRes> menuUserListSelect(Long seq);
    @Transactional(readOnly = true)
    List<Menu> pmenuListSelect(MenuReq dto);
    @Transactional(readOnly = true)
    List<Menu> cmenuListSelect(MenuReq dto);
    QueryResults<Menu> SelectListMenuPaging(MenuReq dto);


    @Transactional(readOnly = true)
    List<MenuPermissionRes> menuUserLv1ListSelect(Long seq);
    @Transactional(readOnly = true)
    List<MenuPermissionRes> menuUserLv2ListSelect(Long seq, Long pseq);
    @Transactional(readOnly = true)
    List<MenuPermissionRes> menuUserLv3ListSelect(Long seq, Long pseq);


}
