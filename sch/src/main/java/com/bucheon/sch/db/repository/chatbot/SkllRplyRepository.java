package com.bucheon.sch.db.repository.chatbot;

import com.bucheon.sch.db.entity.chatbot.SkllRply;
import com.bucheon.sch.db.entity.chatbot.id.SkllRplyId;
import com.bucheon.sch.db.repository.chatbot.dsl.SkllRplyRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface SkllRplyRepository extends JpaRepository<SkllRply, SkllRplyId>, SkllRplyRepositoryDsl {

    @Transactional
    @Modifying
    @Query(
            value = "delete from tb_skll_rply where SKLL_CODE =:answerSeq "
            , nativeQuery = true
    )
    int DeleteAnswerSkllRplyCode(@Param("answerSeq") String answerSeq);
}
