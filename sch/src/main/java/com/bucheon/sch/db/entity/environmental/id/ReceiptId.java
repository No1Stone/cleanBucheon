package com.bucheon.sch.db.entity.environmental.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ReceiptId implements Serializable {
    /**
     * 부천시 민원 접수 데이터
     */
    private String petiNo;
    private String civilNo;

}
