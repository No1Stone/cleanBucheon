package com.bucheon.sch.db.repository.environmental.band;

import com.bucheon.sch.db.entity.environmental.band.TB_BandBoardListPoto;
import com.bucheon.sch.db.entity.environmental.band.id.TB_BandBoardListPotoId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TB_BandBoardListPotoRepository extends JpaRepository<TB_BandBoardListPoto, TB_BandBoardListPotoId> {



}
