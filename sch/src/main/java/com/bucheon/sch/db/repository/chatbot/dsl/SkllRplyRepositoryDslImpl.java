package com.bucheon.sch.db.repository.chatbot.dsl;

import com.bucheon.sch.biz.chatbot.answer.v1.model.QuickRepliesVO;
import com.bucheon.sch.db.entity.chatbot.QSkll;
import com.bucheon.sch.db.entity.chatbot.QSkllRply;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class SkllRplyRepositoryDslImpl implements SkllRplyRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QSkll qSkll = QSkll.skll;
    private QSkllRply qSkllRply = QSkllRply.skllRply;

    @Override
    public List<QuickRepliesVO> selectQuickReplies(String code) {

        List<QuickRepliesVO> result = queryFactory.select(
                        Projections.bean(QuickRepliesVO.class,
                                qSkllRply.skllCode.as("skillCode"),
                                qSkllRply.lbl.as("label"),
                                qSkllRply.actn.as("action"),
                                qSkllRply.questMessage.as("messageText"),
                                qSkllRply.blckOrginlId.as("blockId"),
                                qSkllRply.qrOrdr.as("qrSeq")
                        )
                )
                .from(qSkllRply)
                .where(eqId(code))
                .fetch();
        return result;

    }

    private BooleanExpression eqId(String id) {
        if(StringUtils.isEmptyOrWhitespace(id)){
            return null;
        }
        return qSkllRply.skllCode.eq(id);
    }

}
