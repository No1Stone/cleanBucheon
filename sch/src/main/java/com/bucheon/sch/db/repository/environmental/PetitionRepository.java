package com.bucheon.sch.db.repository.environmental;

import com.bucheon.sch.db.entity.environmental.Petition;
import com.bucheon.sch.db.repository.environmental.dsl.PetitionRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PetitionRepository extends JpaRepository<Petition, String> , PetitionRepositoryDsl {

    List<Petition> findByPetiNoIn(List<String> peNo);


}
