package com.bucheon.sch.db.repository.operation;

import com.bucheon.sch.db.entity.operation.MileageHistory;
import com.bucheon.sch.db.entity.operation.id.MileageHistoryId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MileageHistoryRepository extends JpaRepository<MileageHistory, MileageHistoryId> {
}
