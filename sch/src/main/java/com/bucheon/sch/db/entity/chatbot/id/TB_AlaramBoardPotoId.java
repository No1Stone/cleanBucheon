package com.bucheon.sch.db.entity.chatbot.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TB_AlaramBoardPotoId implements Serializable {
    private Long boardSeqId;
    private String fileName;
}
