package com.bucheon.sch.db.repository.metabus.dsl;

import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaUserReq;
import com.bucheon.sch.db.entity.metabus.MetaUser;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MetaUserRepositoryDsl {

    @Transactional(readOnly = true)
    List<MetaUser> MetaUserListSelect(MetaUserReq dto);

    @Transactional(readOnly = true)
    QueryResults<MetaUser> MetaUserListSelectPaging(MetaUserReq dto);

}
