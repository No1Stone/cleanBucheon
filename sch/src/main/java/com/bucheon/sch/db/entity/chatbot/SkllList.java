package com.bucheon.sch.db.entity.chatbot;

import com.bucheon.sch.db.entity.chatbot.id.SkllListId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_skll_list")
@Getter
@Setter
@NoArgsConstructor
@IdClass(SkllListId.class)
public class SkllList {

    @Id
    @Column(name = "skll_code", nullable = true)
    private String skllCode;
    @Column(name = "hder", nullable = true)
    private String hder;
    @Column(name = "list", nullable = true)
    private String list;
    @Column(name = "btton", nullable = true)
    private String btton;
    @Id
    @Column(name = "rspns_ordr", nullable = true)
    private int rspnsOrdr;
    @Column(name = "btton_optn_at", nullable = true)
    private String bttonOptnAt;

    @Builder
    SkllList(
            String skllCode,
             String hder,
             String list,
             String btton,
             int rspnsOrdr,
             String bttonOptnAt
    ) {
        this.skllCode = skllCode;
        this.hder = hder;
        this.list = list;
        this.btton = btton;
        this.rspnsOrdr = rspnsOrdr;
        this.bttonOptnAt = bttonOptnAt;
    }

}
