package com.bucheon.sch.db.entity.etc;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_dong_band_damdang")
@Getter
@Setter
@NoArgsConstructor
public class DongBandDamdang {

    @Id
    @Column(name = "dong", nullable = true)
    private String dong;
    @Column(name = "damdang", nullable = true)
    private String damdang;
    @Column(name = "damdang_phone", nullable = true)
    private String damdangPhone;


    @Builder
    DongBandDamdang(
            String dong,
            String damdang,
            String damdangPhone
            ) {
        this.dong = dong;
        this.damdang = damdang;
        this.damdangPhone = damdangPhone;
    }

}
