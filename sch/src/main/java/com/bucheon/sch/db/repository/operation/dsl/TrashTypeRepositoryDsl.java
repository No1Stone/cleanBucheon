package com.bucheon.sch.db.repository.operation.dsl;

import com.bucheon.sch.biz.operation.point.v1.model.TrashTypeReq;
import com.bucheon.sch.db.entity.operation.TrashType;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TrashTypeRepositoryDsl {


    @Transactional(readOnly = true)
    List<TrashType> TrashTypeSelectList(TrashTypeReq dto);
    @Transactional(readOnly = true)
    QueryResults<TrashType> TrashTypeSelectListPaging(TrashTypeReq dto);

}
