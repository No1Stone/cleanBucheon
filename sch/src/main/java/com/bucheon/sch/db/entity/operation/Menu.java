package com.bucheon.sch.db.entity.operation;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_menu")
@Getter
@Setter
@NoArgsConstructor
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "menu_seq", nullable = true)
    private Long menuSeq;
    @Column(name = "menu_name", nullable = true)
    private String menuName;
    @Column(name = "menu_level", nullable = true)
    private int menuLevel;
    @Column(name = "pmenu_seq", nullable = true)
    private Long pmenuSeq;
    @Column(name = "menu_url", nullable = true)
    private String menuUrl;
    @Column(name = "menu_icon", nullable = true)
    private String menuIcon;
    @Column(name = "menu_desc", nullable = true)
    private String menuDesc;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "del_yn", nullable = true)
    private String delYn;
    @Column(name = "order_num", nullable = true)
    private int orderNum;
    @Column(name = "reg_seq", nullable = true)
    private Long regSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_seq", nullable = true)
    private Long modSeq;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    Menu(Long menuSeq,
         String menuName,
         int menuLevel,
         Long pmenuSeq,
         String menuUrl,
         String menuIcon,
         String menuDesc,
         String useYn,
         String delYn,
         int orderNum,
         Long regSeq,
         LocalDateTime regDt,
         Long modSeq,
         LocalDateTime modDt
    ) {
        this.menuSeq = menuSeq;
        this.menuName = menuName;
        this.menuLevel = menuLevel;
        this.pmenuSeq = pmenuSeq;
        this.menuUrl = menuUrl;
        this.menuIcon = menuIcon;
        this.menuDesc = menuDesc;
        this.useYn = useYn;
        this.delYn = delYn;
        this.orderNum = orderNum;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.modSeq = modSeq;
        this.modDt = modDt;
    }
}
