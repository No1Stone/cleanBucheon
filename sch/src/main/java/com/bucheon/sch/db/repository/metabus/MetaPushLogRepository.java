package com.bucheon.sch.db.repository.metabus;

import com.bucheon.sch.db.entity.metabus.MetaPushLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface MetaPushLogRepository extends JpaRepository<MetaPushLog, String> {

    @Transactional
    int countBySendSeqAndUserId(Long seq, String id);
}
