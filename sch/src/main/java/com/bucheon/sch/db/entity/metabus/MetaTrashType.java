package com.bucheon.sch.db.entity.metabus;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_meta_trash_type")
@Getter
@Setter
@NoArgsConstructor
public class MetaTrashType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trash_seq", nullable = true)
    private Long trashSeq;
    @Column(name = "trash_name", nullable = true)
    private String trashName;
    @Column(name = "trash_act", nullable = true)
    private String trashAct;
    @Column(name = "trash_point", nullable = true)
    private int trashPoint;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "del_yn", nullable = true)
    private String delYn;
    @Column(name = "reg_seq", nullable = true)
    private Long regSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_seq", nullable = true)
    private Long modSeq;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    MetaTrashType(
            Long trashSeq,
            String trashName,
            String trashAct,
            int trashPoint,
            String useYn,
            String delYn,
            Long regSeq,
            LocalDateTime regDt,
            Long modSeq,
            LocalDateTime modDt
    ) {
        this.trashSeq = trashSeq;
        this.trashName = trashName;
        this.trashAct = trashAct;
        this.trashPoint = trashPoint;
        this.useYn = useYn;
        this.delYn = delYn;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.modSeq = modSeq;
        this.modDt = modDt;
    }
}
