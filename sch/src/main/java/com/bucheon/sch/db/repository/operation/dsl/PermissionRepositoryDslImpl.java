package com.bucheon.sch.db.repository.operation.dsl;

import com.bucheon.sch.biz.operation.permission.v1.model.PermissionReq;
import com.bucheon.sch.db.entity.operation.Permission;
import com.bucheon.sch.db.entity.operation.QPermission;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class PermissionRepositoryDslImpl implements PermissionRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QPermission qPermission = QPermission.permission;

    @Override
    public List<Permission> PermissionSelectList(PermissionReq dto) {


        if(dto.getSize() == 0){
            dto.setSize(10);
        }

        LocalDateTime sr = null;
        LocalDateTime er = null;
        LocalDateTime sm = null;
        LocalDateTime em = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSregDt()) && !StringUtils.isEmptyOrWhitespace(dto.getEregDt())) {
            sr = LocalDate.parse(dto.getSregDt(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEregDt(), format).atStartOfDay().plusDays(1L);
        }
        if (!StringUtils.isEmptyOrWhitespace(dto.getSmodDt()) && !StringUtils.isEmptyOrWhitespace(dto.getEmodDt())) {
            sm = LocalDate.parse(dto.getSmodDt(), format).atStartOfDay();
            em = LocalDate.parse(dto.getEmodDt(), format).atStartOfDay().plusDays(1L);
        }

        List<Permission> result = queryFactory.selectFrom(qPermission)
                .where(
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        qPermission.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qPermission.permissionSeq.desc())
                .fetch();
        return result;
    }

    @Override
    public QueryResults<Permission> PermissionSelectListPaging(PermissionReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        LocalDateTime sr = null;
        LocalDateTime er = null;
        LocalDateTime sm = null;
        LocalDateTime em = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSregDt()) && !StringUtils.isEmptyOrWhitespace(dto.getEregDt())) {
            sr = LocalDate.parse(dto.getSregDt(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEregDt(), format).atStartOfDay().plusDays(1L);
        }
        if (!StringUtils.isEmptyOrWhitespace(dto.getSmodDt()) && !StringUtils.isEmptyOrWhitespace(dto.getEmodDt())) {
            sm = LocalDate.parse(dto.getSmodDt(), format).atStartOfDay();
            em = LocalDate.parse(dto.getEmodDt(), format).atStartOfDay().plusDays(1L);
        }

        QueryResults<Permission> result = queryFactory.selectFrom(qPermission)
                .where(
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        qPermission.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qPermission.permissionSeq.desc())
                .fetchResults();
        return result;
    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchsearchSelect(String searchSelect, String searchString) {

        try {

            if (searchSelect.equals("searchName")) {
                return eqPermissionName(searchString);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqPermissionSeq(Long permissionSeq) {
        if (permissionSeq == null) {
            return null;
        }
        return qPermission.permissionSeq.eq(permissionSeq);
    }

    private BooleanExpression eqPermissionName(String permissionName) {
        if (StringUtils.isEmptyOrWhitespace(permissionName)) {
            return null;
        }
        return qPermission.permissionName.contains(permissionName);
    }

    private BooleanExpression eqRegSeq(Long regSeq) {
        if (regSeq == null) {
            return null;
        }
        return qPermission.regSeq.eq(regSeq);
    }

    private BooleanExpression eqModSeq(Long modSeq) {
        if (modSeq == null) {
            return null;
        }
        return qPermission.modSeq.eq(modSeq);
    }

    private BooleanExpression eqRegDt(LocalDateTime sregDt, LocalDateTime eregDt) {
        if (sregDt == null && eregDt == null) {
            return null;
        }
        return qPermission.regDt.between(sregDt, eregDt);
    }

    private BooleanExpression eqModDt(LocalDateTime smodDt, LocalDateTime emodDt) {
        if (smodDt == null && emodDt == null) {
            return null;
        }
        return qPermission.regDt.between(smodDt, emodDt);
    }


}
