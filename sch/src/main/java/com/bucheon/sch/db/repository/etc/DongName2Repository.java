package com.bucheon.sch.db.repository.etc;

import com.bucheon.sch.db.entity.etc.DongName2;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface DongName2Repository extends JpaRepository<DongName2, String> {

    @Transactional(readOnly = true)
    @Query(
            value = "select distinct hname from tb_dong_name2 where dong_seq != 1"
            , nativeQuery = true
    )
    List<String> selectHDongName2();

    @Transactional(readOnly = true)
    Optional<DongName2> findByBgCode(String s);

}
