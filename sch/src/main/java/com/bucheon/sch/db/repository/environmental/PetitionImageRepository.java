package com.bucheon.sch.db.repository.environmental;

import com.bucheon.sch.db.entity.environmental.PetitionImage;
import com.bucheon.sch.db.entity.environmental.id.PetitionImageId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetitionImageRepository extends JpaRepository<PetitionImage, PetitionImageId> {

}
