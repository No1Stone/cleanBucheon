package com.bucheon.sch.db.repository.environmental;

import com.bucheon.sch.db.entity.environmental.CampaignView;
import com.bucheon.sch.db.repository.environmental.dsl.CampaignViewRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface CampaignViewRepository extends JpaRepository<CampaignView, Long>, CampaignViewRepositoryDsl {


    @Transactional(readOnly = true)
    CampaignView findByCampaignNum(Long seq);

}
