package com.bucheon.sch.db.repository.chatbot;

import com.bucheon.sch.db.entity.chatbot.SkllImage;
import com.bucheon.sch.db.entity.chatbot.id.SkllImageId;
import com.bucheon.sch.db.repository.chatbot.dsl.SkllImageRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface SkllImageRepository extends JpaRepository<SkllImage, SkllImageId>, SkllImageRepositoryDsl {

    Optional<SkllImage> findBySkllCodeAndRspnsOrdr(String code, int reqSeq);

    @Transactional
    @Modifying
    @Query(
            value = "delete from tb_skll_image where SKLL_CODE =:answerSeq "
            , nativeQuery = true
    )
    int DeleteAnswerSkllImageCode(@Param("answerSeq") String answerSeq);
}
