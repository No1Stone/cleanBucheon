package com.bucheon.sch.db.entity.operation.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class MileageHistoryId implements Serializable {

    private Long mileageSeq;
    private Long adminSeq;

}
