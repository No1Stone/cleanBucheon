package com.bucheon.sch.db.repository.operation.dsl;

import com.bucheon.sch.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.sch.biz.operation.menu.v1.model.MenuReq;
import com.bucheon.sch.db.entity.operation.Menu;
import com.bucheon.sch.db.entity.operation.QMenu;
import com.bucheon.sch.db.entity.operation.QPermissionMenu;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.time.format.DateTimeFormatter;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class MenuRepositoryDslImpl implements MenuRepositoryDsl{

    private final JPAQueryFactory queryFactory;
    private QMenu qMenu = QMenu.menu;
    private QPermissionMenu qPermissionMenu = QPermissionMenu.permissionMenu;

    @Override
    public List<Menu> menuListSelect(MenuReq dto) {

        if(dto.getSize() == 0){
            dto.setSize(10);
        }

        List<Menu> result = queryFactory.selectFrom(qMenu)
                .where(eqMenuSeq(dto.getMenuSeq()),
                        eqMenuName(dto.getMenuName()),
                        eqMenuLevel(dto.getMenuLevel()),
                        eqPmenuSeq(dto.getPmenuSeq()),
                        eqMenuUrl(dto.getMenuUrl()),
                        eqUseYn(dto.getUseYn()),
                        eqOrderNum(dto.getOrderNum()),
                        qMenu.delYn.eq("N")
                        )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qMenu.orderNum.asc())
                .fetch();

        return result;
    }

    @Override
    public List<Menu> menuUseListSelect(MenuReq dto) {

        List<Menu> result = queryFactory.selectFrom(qMenu)
                .where( eqUseYn("Y"),
                        qMenu.delYn.eq("N")
                )
                .orderBy(qMenu.orderNum.asc())
                .fetch();

        return result;
    }

    @Override
    public List<MenuPermissionRes> menuUserListSelect(Long seq) {

        List<MenuPermissionRes> result =
                queryFactory.select(Projections.bean(MenuPermissionRes.class,
                                qMenu.menuSeq,
                                qMenu.menuName,
                                qMenu.menuUrl,
                                qMenu.pmenuSeq,
                                qMenu.menuIcon,
                                qMenu.menuLevel,
                        ExpressionUtils.as(
                                JPAExpressions.select(qPermissionMenu.useYn)
                                        .from(qPermissionMenu)
                                        .where(qPermissionMenu.permissionSeq.eq(seq), qPermissionMenu.menuSeq.eq(qMenu.menuSeq)),
                                "useYn")
                        ))
                        .from(qMenu)
                .where( eqUseYn("Y"),
                        qMenu.delYn.eq("N")
                )
                .orderBy(qMenu.orderNum.asc())
                .fetch();

        return result;
    }



    @Override
    public List<MenuPermissionRes> menuUserLv1ListSelect(Long seq) {

        List<MenuPermissionRes> result =
                queryFactory.select(Projections.bean(MenuPermissionRes.class,
                                qMenu.menuSeq,
                                qMenu.menuName,
                                qMenu.menuUrl,
                                qMenu.pmenuSeq,
                                qMenu.menuIcon,
                                qMenu.menuLevel,
                                ExpressionUtils.as(
                                        JPAExpressions.select(qPermissionMenu.useYn)
                                                .from(qPermissionMenu)
                                                .where(qPermissionMenu.permissionSeq.eq(seq), qPermissionMenu.menuSeq.eq(qMenu.menuSeq)),
                                        "useYn")
                        ))
                        .from(qMenu)
                        .where( eqUseYn("Y"),
                                qMenu.menuLevel.eq(1),
                                qMenu.delYn.eq("N")
                        )
                        .orderBy(qMenu.orderNum.asc())
                        .fetch();

        return result;
    }

    @Override
    public List<MenuPermissionRes> menuUserLv2ListSelect(Long seq, Long pseq) {

        List<MenuPermissionRes> result =
                queryFactory.select(Projections.bean(MenuPermissionRes.class,
                                qMenu.menuSeq,
                                qMenu.menuName,
                                qMenu.menuUrl,
                                qMenu.pmenuSeq,
                                qMenu.menuIcon,
                                qMenu.menuLevel,
                                ExpressionUtils.as(
                                        JPAExpressions.select(qPermissionMenu.useYn)
                                                .from(qPermissionMenu)
                                                .where(qPermissionMenu.permissionSeq.eq(seq), qPermissionMenu.menuSeq.eq(qMenu.menuSeq)),
                                        "useYn")
                        ))
                        .from(qMenu)
                        .where( eqUseYn("Y"),
                                qMenu.menuLevel.eq(2),
                                qMenu.pmenuSeq.eq(pseq),
                                qMenu.delYn.eq("N")
                        )
                        .orderBy(qMenu.orderNum.asc())
                        .fetch();

        return result;
    }

    @Override
    public List<MenuPermissionRes> menuUserLv3ListSelect(Long seq, Long pseq) {

        List<MenuPermissionRes> result =
                queryFactory.select(Projections.bean(MenuPermissionRes.class,
                                qMenu.menuSeq,
                                qMenu.menuName,
                                qMenu.menuUrl,
                                qMenu.pmenuSeq,
                                qMenu.menuIcon,
                                qMenu.menuLevel,
                                ExpressionUtils.as(
                                        JPAExpressions.select(qPermissionMenu.useYn)
                                                .from(qPermissionMenu)
                                                .where(qPermissionMenu.permissionSeq.eq(seq), qPermissionMenu.menuSeq.eq(qMenu.menuSeq)),
                                        "useYn")
                        ))
                        .from(qMenu)
                        .where( eqUseYn("Y"),
                                qMenu.menuLevel.eq(3),
                                qMenu.pmenuSeq.eq(pseq),
                                qMenu.delYn.eq("N")
                        )
                        .orderBy(qMenu.orderNum.asc())
                        .fetch();

        return result;
    }




    @Override
    public QueryResults<Menu> SelectListMenuPaging(MenuReq dto) {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        if(dto.getSize() == 0){
            dto.setSize(10);
        }

        var aa = queryFactory.select(qMenu).from(qMenu)
                .where(eqMenuSeq(dto.getMenuSeq()),
                        eqMenuName(dto.getMenuName()),
                        eqMenuLevel(dto.getMenuLevel()),
                        eqPmenuSeq(dto.getPmenuSeq()),
                        eqMenuUrl(dto.getMenuUrl()),
                        eqUseYn(dto.getUseYn()),
                        eqOrderNum(dto.getOrderNum()),
                        qMenu.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qMenu.orderNum.asc())
                .fetchResults();

        return aa;
    }

    @Override
    public List<Menu> pmenuListSelect(MenuReq dto) {

        List<Menu> result = queryFactory.selectFrom(qMenu)
                .where(eqMenuSeq(dto.getMenuSeq()),
                        eqMenuName(dto.getMenuName()),
                        eqMenuLevel(dto.getMenuLevel()),
                        eqPmenuSeq(dto.getPmenuSeq()),
                        eqMenuUrl(dto.getMenuUrl()),
                        eqUseYn(dto.getUseYn()),
                        eqOrderNum(dto.getOrderNum()),
                        qMenu.delYn.eq("N")
                )
                .where(qMenu.menuLevel.eq(1))
                .orderBy(qMenu.orderNum.asc())
                .fetch();

        return result;
    }

    @Override
    public List<Menu> cmenuListSelect(MenuReq dto) {

        List<Menu> result = queryFactory.selectFrom(qMenu)
                .where(eqMenuSeq(dto.getMenuSeq()),
                        eqMenuName(dto.getMenuName()),
                        eqMenuLevel(dto.getMenuLevel()),
                        eqPmenuSeq(dto.getPmenuSeq()),
                        eqMenuUrl(dto.getMenuUrl()),
                        eqUseYn(dto.getUseYn()),
                        eqOrderNum(dto.getOrderNum()),
                        qMenu.delYn.eq("N")
                )
                .where(qMenu.pmenuSeq.eq(dto.getPmenuSeq()), qMenu.menuLevel.eq(2))
                .orderBy(qMenu.orderNum.asc())
                .fetch();

        return result;
    }

    private BooleanExpression eqMenuSeq(Long menuSeq){
        if(menuSeq == null){
            return null;
        }
        return qMenu.menuSeq.eq(menuSeq);
    }
    private BooleanExpression eqMenuName(String menuName){
        if(StringUtils.isEmptyOrWhitespace(menuName)){
            return null;
        }
        return qMenu.menuName.contains(menuName);
    }
    private BooleanExpression eqMenuLevel(int level){
        if(level == 0 ){
            return null;
        }
        return qMenu.menuLevel.eq(level);
    }
    private BooleanExpression eqPmenuSeq(Long pmenuSeq){
        if(pmenuSeq == null ){
            return null;
        }

        return qMenu.pmenuSeq.eq(pmenuSeq);
    }
    private BooleanExpression eqMenuUrl(String menuUrl){
        if(StringUtils.isEmptyOrWhitespace(menuUrl)){
            return null;
        }
//        var url = Expressions.stringTemplate("lower({0})",qMenu.menuUrl).contains(menuUrl.toLowerCase());
        return qMenu.menuUrl.toLowerCase().contains(menuUrl.toLowerCase());
    }
    private BooleanExpression eqUseYn(String yn){
        if(StringUtils.isEmptyOrWhitespace(yn)){
            return null;
        }
        return qMenu.useYn.eq(yn);
    }
    private BooleanExpression eqOrderNum(int orderNum){

        if(orderNum == 0 ){
            return null;
        }
        return qMenu.orderNum.eq(orderNum);
    }

}
