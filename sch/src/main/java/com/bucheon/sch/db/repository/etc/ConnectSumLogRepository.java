package com.bucheon.sch.db.repository.etc;

import com.bucheon.sch.db.entity.etc.ConnectSumLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

public interface ConnectSumLogRepository extends JpaRepository<ConnectSumLog, String> {


    @Transactional(readOnly = true)
    int countByConnectDateAndUserId(LocalDate date, String userId);

    @Transactional(readOnly = true)
    Optional<ConnectSumLog> findByConnectDateAndUserId(LocalDate date, String userId);
}
