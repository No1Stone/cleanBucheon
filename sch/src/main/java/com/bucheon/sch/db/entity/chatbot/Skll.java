package com.bucheon.sch.db.entity.chatbot;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "tb_skll")
@Getter
@Setter
@NoArgsConstructor
public class Skll {
    @Id
    @Column(name = "skll_code", nullable = true)
    private String skllCode;
    @Column(name = "skll_nm", nullable = true)
    private String skllNm;
    @Column(name = "skll_url", nullable = true)
    private String skllUrl;
    @Column(name = "rspns_ty", nullable = true)
    private String rspnsTy;
    @Column(name = "regist_de", nullable = true)
    private LocalDate registDe;
    @Column(name = "updt_de", nullable = true)
    private LocalDate updtDe;
    @Column(name = "skll_catgory", nullable = true)
    private String skllCatgory;
    @Column(name = "skll_level", nullable = true)
    private String skllLevel;
    @Column(name = "rspns_ty_1", nullable = true)
    private String rspnsTy1;
    @Column(name = "rspns_ty_2", nullable = true)
    private String rspnsTy2;
    @Column(name = "rspns_ty_3", nullable = true)
    private String rspnsTy3;
    @Column(name = "skll_level_1", nullable = true)
    private String skllLevel1;
    @Column(name = "skll_level_2", nullable = true)
    private String skllLevel2;
    @Column(name = "skll_level_3", nullable = true)
    private String skllLevel3;
    @Column(name = "skll_level_4", nullable = true)
    private String skllLevel4;
    @Column(name = "skll__url", nullable = true)
    private String skllUrl2;
    @Column(name = "skll_ctgry", nullable = true)
    private String skllCtgry;
    @Column(name = "profl_crtfc_at", nullable = true)
    private String proflCrtfcAt;

    @Builder
    Skll(
            String skllCode,
            String skllNm,
            String skllUrl,
            String rspnsTy,
            LocalDate registDe,
            LocalDate updtDe,
            String skllCatgory,
            String skllLevel,
            String rspnsTy1,
            String rspnsTy2,
            String rspnsTy3,
            String skllLevel1,
            String skllLevel2,
            String skllLevel3,
            String skllLevel4,
            String skllUrl2,
            String skllCtgry,
            String proflCrtfcAt
    ) {
        this.skllCode = skllCode;
        this.skllNm = skllNm;
        this.skllUrl = skllUrl;
        this.rspnsTy = rspnsTy;
        this.registDe = registDe;
        this.updtDe = updtDe;
        this.skllCatgory = skllCatgory;
        this.skllLevel = skllLevel;
        this.rspnsTy1 = rspnsTy1;
        this.rspnsTy2 = rspnsTy2;
        this.rspnsTy3 = rspnsTy3;
        this.skllLevel1 = skllLevel1;
        this.skllLevel2 = skllLevel2;
        this.skllLevel3 = skllLevel3;
        this.skllLevel4 = skllLevel4;
        this.skllUrl2 = skllUrl2;
        this.skllCtgry = skllCtgry;
        this.proflCrtfcAt = proflCrtfcAt;
    }

}
