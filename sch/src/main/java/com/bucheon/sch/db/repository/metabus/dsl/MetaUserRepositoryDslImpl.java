package com.bucheon.sch.db.repository.metabus.dsl;

import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaUserReq;
import com.bucheon.sch.db.entity.metabus.MetaUser;
import com.bucheon.sch.db.entity.metabus.QMetaUser;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.util.LinkedList;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class MetaUserRepositoryDslImpl implements MetaUserRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QMetaUser qMetaUser = QMetaUser.metaUser;


    @Override
    public List<MetaUser> MetaUserListSelect(MetaUserReq dto) {
        
        if(dto.getSize() == 0){
            dto.setSize(10);
        }

        var result = queryFactory.selectFrom(qMetaUser)
                .where(

                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        eqSearchDelYn(dto.getDelYn())
                )
                .orderBy(sorterOrder(dto.getOrderStatus()).stream().toArray(OrderSpecifier[]::new))
                .limit(dto.getSize())
                .offset(dto.getPage()* dto.getSize())
                .fetch();
        return result;
    }

    @Override
    public QueryResults<MetaUser> MetaUserListSelectPaging(MetaUserReq dto) {

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        var result = queryFactory.selectFrom(qMetaUser)
                .where(

                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        eqSearchDelYn(dto.getDelYn())
                )
                .limit(dto.getSize())
                .offset(dto.getPage()* dto.getSize())
                .fetchResults();
        return result;
    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchsearchSelect(String searchSelect, String searchString) {

        try {

            if (searchSelect.equals("searchName")) {
                return eqName(searchString);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqName(String name){
        if (StringUtils.isEmptyOrWhitespace(name)) {
            return null;
        }
        return qMetaUser.nickname.contains(name);
    }
    private BooleanExpression eqSearchDelYn(String del){
        if (StringUtils.isEmptyOrWhitespace(del)) {
            return null;
        }
        return qMetaUser.delYn.eq(del);
    }


    //정렬 확인
   private List<OrderSpecifier> sorterOrder(String orderStatus) {

        List<OrderSpecifier> orderBy = new LinkedList<>();

        if (orderStatus == null) {
            orderBy.add(qMetaUser.delYn.asc());
            orderBy.add(qMetaUser.nickname.asc());
        }

       if (orderStatus.equals("orderCreateAt")) {
           orderBy.add(qMetaUser.createdAt.desc());
           orderBy.add(qMetaUser.nickname.asc());
           orderBy.add(qMetaUser.delYn.asc());
       }

       if (orderStatus.equals("orderLastConnectAt")) {
           orderBy.add(qMetaUser.lastConnectAt.desc());
           orderBy.add(qMetaUser.nickname.asc());
           orderBy.add(qMetaUser.delYn.asc());
       }

       if (orderStatus.equals("orderPoint")) {
           orderBy.add(qMetaUser.cleanPoint.desc());
           orderBy.add(qMetaUser.nickname.asc());
           orderBy.add(qMetaUser.delYn.asc());
       }

       if (orderStatus.equals("orderDelYAndLastConnectAt")) {
           orderBy.add(qMetaUser.delYn.desc());
           orderBy.add(qMetaUser.lastConnectAt.desc());
           orderBy.add(qMetaUser.nickname.asc());
       }

       if (orderStatus.equals("orderDelYAndLastConnectAt")) {
           orderBy.add(qMetaUser.delYn.asc());
           orderBy.add(qMetaUser.lastConnectAt.desc());
           orderBy.add(qMetaUser.nickname.asc());
       }

        return orderBy;
    }
}
