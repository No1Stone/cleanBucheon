package com.bucheon.sch.db.entity.etc;

import com.bucheon.sch.db.entity.etc.id.ConnectSumLogId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "tb_connect_sum_log")
@Getter
@Setter
@NoArgsConstructor
@IdClass(ConnectSumLogId.class)
public class ConnectSumLog {

    @Id
    @Column(name = "connect_date", nullable = true)
    private LocalDate connectDate;
    @Column(name = "connect_type", nullable = true)
    private String connectType;
    @Id
    @Column(name = "user_id", nullable = true)
    private String userId;

    @Builder
    ConnectSumLog(
            LocalDate connectDate,
            String connectType,
            String userId
            ) {
        this.connectDate = connectDate;
        this.connectType = connectType;
        this.userId = userId;
    }

}
