package com.bucheon.sch.db.repository.metabus;

import com.bucheon.sch.db.entity.metabus.MetaTimeAttact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface MetaTimeAttactRepository extends JpaRepository<MetaTimeAttact, String> {

    @Transactional(readOnly = true)
    Optional<MetaTimeAttact> findTop1ByUseYnAndDelYn(String use, String del);

}
