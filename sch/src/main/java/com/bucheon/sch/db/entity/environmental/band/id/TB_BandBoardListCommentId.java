package com.bucheon.sch.db.entity.environmental.band.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TB_BandBoardListCommentId implements Serializable {
    private String postKey;
    private long createdAt;

}
