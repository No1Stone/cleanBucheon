package com.bucheon.sch.db.entity.metabus;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_meta_user_vote_history")
@Getter
@Setter
@NoArgsConstructor
public class MetaUserVoteHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vote_seq", nullable = true)
    private Long voteSeq;
    @Column(name = "user_id", nullable = true)
    private String userId;
    @Column(name = "vote_type", nullable = true)
    private String voteType;
    @Column(name = "board_type", nullable = true)
    private String boardType;
    @Column(name = "board_seq", nullable = true)
    private Long boardSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;


    @Builder
    MetaUserVoteHistory(
            Long voteSeq,
            String userId,
            String voteType,
            String boardType,
            Long boardSeq,
            LocalDateTime regDt
    ) {
        this.voteSeq = voteSeq;
        this.userId = userId;
        this.voteType = voteType;
        this.boardType = boardType;
        this.boardSeq = boardSeq;
        this.regDt = regDt;
    }


}
