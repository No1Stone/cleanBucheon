package com.bucheon.sch.db.repository.chatbot.dsl;

import com.bucheon.sch.biz.chatbot.uploadImage.v1.model.UploadImageVO;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UploadImageRepositoryDsl {

    @Transactional(readOnly = true)
    List<UploadImageVO> SelectListUploadImages(UploadImageVO dto);

    @Transactional(readOnly = true)
    QueryResults<UploadImageVO> SelectListUploadImagesPaging(UploadImageVO dto);
}
