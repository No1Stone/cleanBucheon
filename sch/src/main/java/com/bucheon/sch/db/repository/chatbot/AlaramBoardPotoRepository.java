package com.bucheon.sch.db.repository.chatbot;

import com.bucheon.sch.db.entity.chatbot.TB_AlaramBoardPoto;
import com.bucheon.sch.db.entity.chatbot.id.TB_AlaramBoardPotoId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlaramBoardPotoRepository extends JpaRepository<TB_AlaramBoardPoto, TB_AlaramBoardPotoId> {

    List<TB_AlaramBoardPoto> findByBoardSeqId(Long seq);

}
