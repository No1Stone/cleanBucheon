package com.bucheon.sch.db.repository.etc;

import com.bucheon.sch.db.entity.etc.DongName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface DongNameRepository extends JpaRepository<DongName, String> {

    @Transactional(readOnly = true)
    @Query(
            value = "select distinct admin from tb_dong_name"
            , nativeQuery = true
    )
    List<String> selectBubDongName();

    @Transactional(readOnly = true)
    Optional<DongName> findByName(String s);
}
