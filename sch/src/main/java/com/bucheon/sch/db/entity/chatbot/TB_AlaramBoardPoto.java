package com.bucheon.sch.db.entity.chatbot;

import com.bucheon.sch.db.entity.chatbot.id.TB_AlaramBoardPotoId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_alaram_board_poto")
@Getter
@Setter
@NoArgsConstructor
@IdClass(TB_AlaramBoardPotoId.class)
public class TB_AlaramBoardPoto {

    @Id
    @Column(name = "board_seq_id", nullable = true)
    private Long boardSeqId;
    @Column(name = "class_url", nullable = true)
    private String classUrl;
    @Id
    @Column(name = "file_name", nullable = true)
    private String fileName;
    @Column(name = "file_size", nullable = true)
    private String fileSize;
    @Column(name = "file_type", nullable = true)
    private String fileType;

    @Builder
    public TB_AlaramBoardPoto(Long boardSeqId, String classUrl, String fileName, String fileSize, String fileType) {
        this.boardSeqId = boardSeqId;
        this.classUrl = classUrl;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileType = fileType;
    }

    /*
    create table tb_alaram_board_poto(
board_seq_id bigint,
class_url text,
file_name varchar(255),
file_size varchar(100),
file_type varchar(50),
primary key( board_seq_id,file_name  )
);
     */
}
