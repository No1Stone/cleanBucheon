package com.bucheon.sch.db.entity.operation.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class PermissionMenuId implements Serializable {
    private Long permissionSeq;
    private Long menuSeq;
}
