package com.bucheon.sch.db.repository.chatbot.dsl;

import com.bucheon.sch.biz.chatbot.answer.v1.model.AnswerReq;
import com.bucheon.sch.biz.chatbot.answer.v1.model.SkillManageRVO;
import com.bucheon.sch.db.entity.chatbot.QSkll;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class SkllRepositoryDslImpl implements SkllRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QSkll qSkll = QSkll.skll;


    @Override
    public List<SkillManageRVO> SkllSelectList(AnswerReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        List<SkillManageRVO> result = queryFactory.select(
                Projections.bean(SkillManageRVO.class,
                                qSkll.skllCode.as("skillCode"),
                                Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qSkll.updtDe, "%Y-%m-%d").as("updtDe"),
                                Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qSkll.registDe, "%Y-%m-%d").as("registDe"),
                                //qSkll.updtDe.as("updtDe"),
                                //qSkll.registDe.as("registDe"),
                                qSkll.skllNm.as("skillName"),
                                qSkll.skllLevel.as("skillLevel"),
                                qSkll.skllLevel1.as("skillLevel1"),
                                qSkll.skllLevel2.as("skillLevel2"),
                                qSkll.skllLevel3.as("skillLevel3"),
                                qSkll.skllLevel4.as("skillLevel4"),
                                qSkll.rspnsTy.as("resType"),
                                qSkll.rspnsTy1.as("resType1"),
                                qSkll.rspnsTy2.as("resType2"),
                                qSkll.rspnsTy3.as("resType3")
                        )
                )
                .from(qSkll)
                .where(eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()))
                .orderBy(qSkll.registDe.desc())
                .offset(dto.getPage()*dto.getSize())
                .limit(dto.getSize())
                .fetch();
        return result;
    }

    @Override
    public QueryResults<SkillManageRVO> SkllSelectListPaging(AnswerReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        QueryResults<SkillManageRVO> result = queryFactory.select(
                        Projections.bean(SkillManageRVO.class,
                                qSkll.skllCode.as("skillCode"),
                                //qSkll.updtDe.as("updtDe"),
                                //qSkll.registDe.as("registDe"),
                                qSkll.skllNm.as("skillName"),
                                qSkll.skllLevel.as("skillLevel"),
                                qSkll.skllLevel1.as("skillLevel1"),
                                qSkll.skllLevel2.as("skillLevel2"),
                                qSkll.skllLevel3.as("skillLevel3"),
                                qSkll.skllLevel4.as("skillLevel4"),
                                qSkll.rspnsTy.as("resType"),
                                qSkll.rspnsTy1.as("resType1"),
                                qSkll.rspnsTy2.as("resType2"),
                                qSkll.rspnsTy3.as("resType3")
                        )
                )
                .from(qSkll)
                .where(eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()))
                .orderBy(qSkll.registDe.desc())
                .offset(dto.getPage()*dto.getSize())
                .limit(dto.getSize())
                .fetchResults();
        return result;

    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchsearchSelect(String searchSelect, String searchString) {

        try {

            if (searchSelect.equals("searchCode")) {
                return eqId(searchString);
            } else if (searchSelect.equals("searchName")) {
                return eqName(searchString);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqId(String id) {
        if(StringUtils.isEmptyOrWhitespace(id)){
            return null;
        }
        return qSkll.skllCode.eq(id);
    }

    private BooleanExpression eqName(String name) {
        if(StringUtils.isEmptyOrWhitespace(name)){
        return null;
        }
        return qSkll.skllNm.contains(name);
    }

}
