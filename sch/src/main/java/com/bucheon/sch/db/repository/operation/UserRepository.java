package com.bucheon.sch.db.repository.operation;

import com.bucheon.sch.db.entity.operation.User;
import com.bucheon.sch.db.repository.operation.dsl.UserRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, String> , UserRepositoryDsl {

    @Transactional(readOnly = true)
    Optional<User> findByDid(String did);

    @Transactional(readOnly = true)
    Optional<User> findByChatbotUserId(String chatbotUserId);

    @Transactional(readOnly = true)
    int countByDid(String did);

    @Transactional(readOnly = true)
    int countByChatbotUserId(String chatbotUserId);

    @Transactional(readOnly = true)
    int countByMetaUserIdAndMetaUserIdNotNull(String metaUserId);
}
