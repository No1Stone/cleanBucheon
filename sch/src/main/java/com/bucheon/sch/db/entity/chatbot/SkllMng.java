package com.bucheon.sch.db.entity.chatbot;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_skll_mng")
@Getter
@Setter
@NoArgsConstructor
public class SkllMng {

    @Id
    @Column(name = "skll_code", nullable = true)
    private String skllCode;
    @Column(name = "skll_nm", nullable = true)
    private String skllNm;
    @Column(name = "devlop_id", nullable = true)
    private String devlopId;
    @Column(name = "oper_id", nullable = true)
    private String operId;

    @Builder
    SkllMng(
            String skllCode,
            String skllNm,
            String devlopId,
            String operId
    ) {
        this.skllCode = skllCode;
        this.skllNm = skllNm;
        this.devlopId = devlopId;
        this.operId = operId;
    }
}
