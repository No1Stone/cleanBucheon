package com.bucheon.sch.db.repository.chatbot.dsl;

import com.bucheon.sch.biz.chatbot.answer.v1.model.BasicCardVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SkllCardRepositoryDsl {

    @Transactional(readOnly = true)
    List<BasicCardVO> selectSkllBcCodeAndRspnsOrdr(String code, int reqSeq);
}
