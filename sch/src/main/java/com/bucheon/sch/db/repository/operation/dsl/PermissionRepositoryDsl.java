package com.bucheon.sch.db.repository.operation.dsl;

import com.bucheon.sch.biz.operation.permission.v1.model.PermissionReq;
import com.bucheon.sch.db.entity.operation.Permission;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PermissionRepositoryDsl  {

    @Transactional(readOnly = true)
    List<Permission> PermissionSelectList(PermissionReq dto);


    @Transactional(readOnly = true)
    QueryResults<Permission> PermissionSelectListPaging(PermissionReq dto);
}
