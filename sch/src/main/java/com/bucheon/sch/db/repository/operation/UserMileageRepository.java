package com.bucheon.sch.db.repository.operation;

import com.bucheon.sch.db.entity.operation.UserMileage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface UserMileageRepository extends JpaRepository<UserMileage, String> {

    @Transactional(readOnly = true)
    int countByUserId(String userId);

    @Transactional(readOnly = true)
    Optional<UserMileage> findByUserId(String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_user_mileage set mileage_sum = mileage_sum + :mileageSum where user_id =:userId"
            , nativeQuery = true
    )
    int updateUserMileageSum(@Param("mileageSum") int mileageSum, @Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_user_mileage set mileage_seq = :mileageSeq where user_id =:userId"
            , nativeQuery = true
    )
    int updateUserMileageSeq(@Param("mileageSeq") Long mileageSeq, @Param("userId") String userId);
}
