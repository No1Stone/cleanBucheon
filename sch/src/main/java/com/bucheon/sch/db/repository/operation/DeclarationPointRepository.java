package com.bucheon.sch.db.repository.operation;

import com.bucheon.sch.db.entity.operation.DeclarationPoint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface DeclarationPointRepository extends JpaRepository<DeclarationPoint, Long> {

    @Transactional
    @Modifying
    @Query(
            value = "update tb_declaration_point set declaration_point = :declarationPoint, mod_seq = :modSeq, mod_dt = now() where declaration_seq =:declarationSeq "
            , nativeQuery = true
    )
    int updateDeclarationPoint(@Param("declarationPoint") int declarationPoint, @Param("modSeq") Long modSeq, @Param("declarationSeq") Long declarationSeq);
}
