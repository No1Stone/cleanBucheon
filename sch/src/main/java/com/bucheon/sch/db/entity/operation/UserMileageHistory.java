package com.bucheon.sch.db.entity.operation;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_user_mileage_history")
@Getter
@Setter
@NoArgsConstructor
public class UserMileageHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mileage_seq", nullable = true)
    private Long mileageSeq;
    @Column(name = "user_id", nullable = true)
    private String userId;
    @Column(name = "report_seq", nullable = true)
    private Long reportSeq;
    @Column(name = "mileage", nullable = true)
    private int mileage;
    @Column(name = "mileage_type", nullable = true)
    private String mileageType;
    @Column(name = "reason", nullable = true)
    private String reason;
    @Column(name = "complaints_dt", nullable = true)
    private LocalDateTime complaintsDt;
    @Column(name = "admin_seq", nullable = true)
    private Long adminSeq;
    @Column(name = "send_date", nullable = true)
    private LocalDateTime sendDate;



    @Builder
    UserMileageHistory(
            Long mileageSeq,
            String userId,
            Long reportSeq,
            int mileage,
            String mileageType,
            String reason,
            LocalDateTime complaintsDt,
            Long adminSeq,
            LocalDateTime sendDate
    ) {
        this.mileageSeq = mileageSeq;
        this.userId = userId;
        this.reportSeq = reportSeq;
        this.mileage = mileage;
        this.mileageType = mileageType;
        this.reason = reason;
        this.complaintsDt = complaintsDt;
        this.adminSeq = adminSeq;
        this.sendDate = sendDate;
    }


}
