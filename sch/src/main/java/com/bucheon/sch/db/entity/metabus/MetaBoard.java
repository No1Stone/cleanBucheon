package com.bucheon.sch.db.entity.metabus;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_meta_board")
@Getter
@Setter
@NoArgsConstructor
public class MetaBoard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "board_seq", nullable = true)
    private Long boardSeq;
    @Column(name = "board_type", nullable = true)
    private String boardType;
    @Column(name = "board_title", nullable = true)
    private String boardTitle;
    @Column(name = "board_content", nullable = true)
    private String boardContent;
    @Column(name = "vote1", nullable = true)
    private int vote1;
    @Column(name = "vote2", nullable = true)
    private int vote2;
    @Column(name = "vote3", nullable = true)
    private int vote3;
    @Column(name = "sdate", nullable = true)
    private LocalDateTime sdate;
    @Column(name = "edate", nullable = true)
    private LocalDateTime edate;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "del_yn", nullable = true)
    private String delYn;
    @Column(name = "reg_seq", nullable = true)
    private Long regSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_seq", nullable = true)
    private Long modSeq;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;



    @Builder
    MetaBoard(
            Long boardSeq,
            String boardType,
            String boardTitle,
            String boardContent,
            int vote1,
            int vote2,
            int vote3,
            LocalDateTime sdate,
            LocalDateTime edate,
            String useYn,
            String delYn,
            Long regSeq,
            LocalDateTime regDt,
            Long modSeq,
            LocalDateTime modDt
            ) {
        this.boardSeq = boardSeq;
        this.boardType = boardType;
        this.boardTitle = boardTitle;
        this.boardContent = boardContent;
        this.vote1 = vote1;
        this.vote2 = vote2;
        this.vote3 = vote3;
        this.sdate = sdate;
        this.edate = edate;
        this.useYn = useYn;
        this.delYn = delYn;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.modSeq = modSeq;
        this.modDt = modDt;
    }

}
