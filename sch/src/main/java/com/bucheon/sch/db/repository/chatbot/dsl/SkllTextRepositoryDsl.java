package com.bucheon.sch.db.repository.chatbot.dsl;

import com.bucheon.sch.biz.chatbot.answer.v1.model.SimpleTextVO;
import org.springframework.transaction.annotation.Transactional;

public interface SkllTextRepositoryDsl {

    @Transactional(readOnly = true)
    SimpleTextVO findBySkllStCodeAndRspnsOrdr(String code, int reqSeq);
}
