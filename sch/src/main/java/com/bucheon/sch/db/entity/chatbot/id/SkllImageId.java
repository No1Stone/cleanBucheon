package com.bucheon.sch.db.entity.chatbot.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SkllImageId implements Serializable {
    private String skllCode;
    private int rspnsOrdr;
}
