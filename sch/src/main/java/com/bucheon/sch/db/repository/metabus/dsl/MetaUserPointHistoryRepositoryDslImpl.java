package com.bucheon.sch.db.repository.metabus.dsl;

import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaUserPointHistoryReq;
import com.bucheon.sch.biz.metabus.metaUser.v1.model.MetaUserPointHistoryRes;
import com.bucheon.sch.db.entity.metabus.MetaUserPointHistory;
import com.bucheon.sch.db.entity.metabus.QMetaTrashType;
import com.bucheon.sch.db.entity.metabus.QMetaUser;
import com.bucheon.sch.db.entity.metabus.QMetaUserPointHistory;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class MetaUserPointHistoryRepositoryDslImpl implements MetaUserPointHistoryRepositoryDsl {


    private final JPAQueryFactory queryFactory;
    private QMetaUserPointHistory qMetaUserPointHistory = QMetaUserPointHistory.metaUserPointHistory;
    private QMetaUser qMetaUser = QMetaUser.metaUser;
    private QMetaTrashType qMetaTrashType = QMetaTrashType.metaTrashType;

    private final Logger logger = LoggerFactory.getLogger(MetaUserPointHistoryRepositoryDslImpl.class);

    ////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<MetaUserPointHistory> SelectListMyMetaUserPointHistory(MetaUserPointHistoryReq dto) {

        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
//        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
//            sc = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
//            ec = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
//        }
//        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
//            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
//            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
//        }

        var aa =
                queryFactory.selectFrom(qMetaUserPointHistory)
                        .leftJoin(qMetaUser).on(qMetaUser.userId.eq(qMetaUserPointHistory.userId))
                        .leftJoin(qMetaTrashType).on(qMetaTrashType.trashSeq.eq(qMetaUserPointHistory.trashSeq))
                        .where(
                                eqSearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                                eqMyHistory(dto.getUserId())
                        )
                        .limit(dto.getSize())
                        .offset(dto.getPage()*dto.getSize())
                        .orderBy(qMetaUserPointHistory.regDt.desc())
                        .fetch();
        return aa;
    }

    @Override
    public QueryResults<MetaUserPointHistory> SelectListMyMetaUserPointHistoryPaging(MetaUserPointHistoryReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }

        QueryResults<MetaUserPointHistory> aa =
                queryFactory.selectFrom(qMetaUserPointHistory)
                        .leftJoin(qMetaUser).on(qMetaUser.userId.eq(qMetaUserPointHistory.userId))
                        .leftJoin(qMetaTrashType).on(qMetaTrashType.trashSeq.eq(qMetaUserPointHistory.trashSeq))
                        .where(
                                eqSearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                                eqMyHistory(dto.getUserId())
                        )
                        .limit(dto.getSize())
                        .offset(dto.getPage()*dto.getSize())
                        .orderBy(qMetaUserPointHistory.regDt.desc())
                        .fetchResults();

        return aa;
    }

    @Override
    public List<MetaUserPointHistory> SelectListMyMetaUserPointHistoryAll(MetaUserPointHistoryReq dto) {

        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        int seachSize = 0;
        if(dto.equals("1")){
            seachSize = 30;
        }
        else if(dto.equals("3")){
            seachSize = 92;
        }
        else if(dto.equals("6")){
            seachSize = 183;
        }
        else if(dto.equals("12")){
            seachSize = 365;
        }

        LocalDate searchSdate = LocalDate.now().minusDays(seachSize);
        LocalDate searchEdate = LocalDate.now();
        String searchSdateString = searchSdate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String searchEdateString = searchEdate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));


        var aa =
                queryFactory.selectFrom(qMetaUserPointHistory)
                        .leftJoin(qMetaUser).on(qMetaUser.userId.eq(qMetaUserPointHistory.userId))
                        .leftJoin(qMetaTrashType).on(qMetaTrashType.trashSeq.eq(qMetaUserPointHistory.trashSeq))
                        .where(
                                eqSearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                                eqMyHistory(dto.getUserId()),
                                eqRegDate(searchSdateString, searchEdateString)
                        )
                        .orderBy(qMetaUserPointHistory.regDt.desc())
                        .fetch();
        return aa;
    }

    /////////////////////////////////////////////////////////////////////////////////////////

    ////// 통합관리자에서 사용 //////////////////////////////////////////////////////////////////////////////

    @Override
    public List<MetaUserPointHistoryRes> MetaUserPointHistorySelectList(MetaUserPointHistoryReq dto) {


        if(dto.getSize() == 0){
            dto.setSize(10);
        }

        List<MetaUserPointHistoryRes> result = queryFactory.select(
                        Projections.fields(MetaUserPointHistoryRes.class,
                                qMetaUserPointHistory.pointSeq,
                                qMetaUserPointHistory.userId,
                                ExpressionUtils.as(
                                        JPAExpressions.select(qMetaUser.nickname)
                                                .from(qMetaUser)
                                                .where(qMetaUser.userId.eq(qMetaUserPointHistory.userId)),
                                        "userNickname"),
                                qMetaUserPointHistory.point,
                                qMetaUserPointHistory.accumulatedPoint,
                                qMetaUserPointHistory.processLocation,
                                qMetaUserPointHistory.trashSeq,
                                ExpressionUtils.as(
                                        JPAExpressions.select(qMetaTrashType.trashName)
                                                .from(qMetaTrashType)
                                                .where(qMetaTrashType.trashSeq.eq(qMetaUserPointHistory.trashSeq)),
                                        "trashName"),
                                qMetaUserPointHistory.reason,
                                qMetaUserPointHistory.regDt
                        )
                )
                .from(qMetaUserPointHistory)
                .leftJoin(qMetaUser).on(qMetaUser.userId.eq(qMetaUserPointHistory.userId))
                .where(
                        eqSearchSelect2(dto.getSearchSelect(), dto.getSearchString(), dto.getSearchSdate(), dto.getSearchEdate())
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qMetaUserPointHistory.pointSeq.desc())
                .fetch();

        return result;
    }

    @Override
    public QueryResults<MetaUserPointHistoryRes> MetaUserPointHistorySelectListPaging(MetaUserPointHistoryReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        QueryResults<MetaUserPointHistoryRes> result = queryFactory.select(
                        Projections.fields(MetaUserPointHistoryRes.class,
                                qMetaUserPointHistory.pointSeq,
                                qMetaUserPointHistory.userId,
                                ExpressionUtils.as(
                                        JPAExpressions.select(qMetaUser.nickname)
                                                .from(qMetaUser)
                                                .where(qMetaUser.userId.eq(qMetaUserPointHistory.userId)),
                                        "userNickname"),
                                qMetaUserPointHistory.point,
                                qMetaUserPointHistory.accumulatedPoint,
                                qMetaUserPointHistory.processLocation,
                                qMetaUserPointHistory.trashSeq,
                                ExpressionUtils.as(
                                        JPAExpressions.select(qMetaTrashType.trashName)
                                                .from(qMetaTrashType)
                                                .where(qMetaTrashType.trashSeq.eq(qMetaUserPointHistory.trashSeq)),
                                        "trashSeqName"),
                                qMetaUserPointHistory.reason,
                                qMetaUserPointHistory.regDt
                        )
                )
                .from(qMetaUserPointHistory)
                .leftJoin(qMetaUser).on(qMetaUser.userId.eq(qMetaUserPointHistory.userId))
                .where(
                        eqSearchSelect2(dto.getSearchSelect(), dto.getSearchString(), dto.getSearchSdate(), dto.getSearchEdate())
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qMetaUserPointHistory.pointSeq.desc())
                .fetchResults();
        return result;
    }

    ////// 통합관리자에서 사용 //////////////////////////////////////////////////////////////////////////////

    //전체 검색 종류 확인
    private BooleanExpression eqSearchSelect(String searchSelect, String searchString) {

        try {

            if (searchSelect.equals("searchName")) {
                return eqRegNickname(searchString);
            } else if (searchSelect.equals("searchLocation")) {
                return eqRegProcessLocation(searchString);
            } else if (searchSelect.equals("searchReg")) {
                return eqRegReason(searchString);
//            } else if (searchSelect.equals("searchLocation")) {
//                return eqCampaignAddrold(searchString);
//            } else if (searchSelect.equals("searchReg")) {
//                return eqRegdt(sr, er);
//            } else if (searchSelect.equals("searchComplete")) {
//                return eqComdt(sc, ec);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchSelect2(String searchSelect, String searchString, String searchSdate, String searchEdate) {

        try {

            if (searchSelect.equals("searchName")) {
                return eqRegNickname(searchString);
            } else if (searchSelect.equals("searchLocation")) {
                return eqRegProcessLocation(searchString);
            } else if (searchSelect.equals("searchReg")) {
                return eqRegDate(searchSdate, searchEdate);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }


    private BooleanExpression eqMyHistory(String userId) {

        logger.info("getSessionAdministrativeDivision - {}", userId);
        if (StringUtils.isEmptyOrWhitespace(userId)) {
            return null;
        }
        return qMetaUserPointHistory.userId.eq(userId);
    }

    private BooleanExpression eqRegNickname(String st) {
        if (StringUtils.isEmptyOrWhitespace(st)) {
            return null;
        }
        return qMetaUser.nickname.contains(st);
    }

    private BooleanExpression eqRegProcessLocation(String st) {
        if (StringUtils.isEmptyOrWhitespace(st)) {
            return null;
        }
        return qMetaUserPointHistory.processLocation.contains(st);
    }

    private BooleanExpression eqRegReason(String st) {
        if (StringUtils.isEmptyOrWhitespace(st)) {
            return null;
        }
        return qMetaUserPointHistory.reason.contains(st);
    }

    private BooleanExpression eqRegDate(String sdate, String edate) {
        if (StringUtils.isEmptyOrWhitespace(sdate)) {
            return null;
        }
        if (StringUtils.isEmptyOrWhitespace(edate)) {
            return null;
        }

        return Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaUserPointHistory.regDt)
                .between(
                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",sdate),
                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",edate)
                ) ;
    }
}
