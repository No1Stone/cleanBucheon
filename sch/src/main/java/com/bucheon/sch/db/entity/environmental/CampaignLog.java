package com.bucheon.sch.db.entity.environmental;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_campaign_log")
@Getter
@Setter
@NoArgsConstructor
public class CampaignLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "log_seq", nullable = true)
    private Long logSeq;
    @Column(name = "log_time", nullable = true)
    private LocalDateTime logTime;
    @Column(name = "campaign_seq", nullable = true)
    private Long campaignSeq;
    @Column(name = "campaign_num", nullable = true)
    private String campaignNum;
    @Column(name = "campaign_status", nullable = true)
    private String campaignStatus;
    @Column(name = "campaign_damdang", nullable = true)
    private Long campaignDamdang;
    @Column(name = "msg", nullable = true)
    private String msg;
    @Column(name = "rejcet_reason", nullable = true)
    private String rejectReason;


    @Builder
    CampaignLog(Long logSeq,
                LocalDateTime logTime,
                Long campaignSeq,
                String campaignNum,
                String campaignStatus,
                Long campaignDamdang,
                String msg,
                String rejectReason
    ) {
        this.logSeq = logSeq;
        this.logTime = logTime;
        this.campaignSeq = campaignSeq;
        this.campaignNum = campaignNum;
        this.campaignDamdang = campaignDamdang;
        this.msg = msg;
        this.rejectReason = rejectReason;
    }

}
