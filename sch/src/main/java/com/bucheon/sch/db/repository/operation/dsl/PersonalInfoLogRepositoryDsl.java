package com.bucheon.sch.db.repository.operation.dsl;

import com.bucheon.sch.biz.operation.personalInfo.v1.model.PersonalInfoLogReq;
import com.bucheon.sch.db.entity.operation.PersonalInfoLog;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PersonalInfoLogRepositoryDsl {

    @Transactional(readOnly = true)
    List<PersonalInfoLog> PersonalInfoLogSelectList(PersonalInfoLogReq dto);
    @Transactional(readOnly = true)
    QueryResults<PersonalInfoLog> PersonalInfoLogSelectListPaging(PersonalInfoLogReq dto);

}
