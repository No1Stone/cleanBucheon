package com.bucheon.sch.db.entity.environmental;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_campaign_view")
@Getter
@Setter
@NoArgsConstructor
public class CampaignView {

    @Id
    @Column(name = "campaign_num", nullable = true)
    private String campaignNum;
    @Column(name = "trash_type", nullable = true)
    private Long trashType;
    @Column(name = "trash_name", nullable = true)
    private String trashName;
    @Column(name = "campaign_status_name", nullable = true)
    private String campaignStatusName;
    @Column(name = "area_name", nullable = true)
    private String areaName;
    @Column(name = "addr_new", nullable = true)
    private String addrNew;
    @Column(name = "addr_old", nullable = true)
    private String addrOld;
    @Column(name = "location_x", nullable = true)
    private String locationX;
    @Column(name = "location_y", nullable = true)
    private String locationY;
    @Column(name = "t_campaign_seq", nullable = true)
    private Long tCampaignSeq;
    @Column(name = "t_reg_user_id", nullable = true)
    private String tRegUserid;
    @Column(name = "t_reg_user_name", nullable = true)
    private String tRegUserName;
    @Column(name = "t_reg_user_phone", nullable = true)
    private String tRegUserPhone;
    @Column(name = "t_reg_user_email", nullable = true)
    private String tRegUserEmail;
    @Column(name = "t_campaign_process", nullable = true)
    private String tCampaignProcess;
    @Column(name = "t_campaign_process_name", nullable = true)
    private String tCampaignProcessName;
    @Column(name = "t_reg_dt", nullable = true)
    private LocalDateTime tRegDt;
    @Column(name = "t_com_dt", nullable = true)
    private LocalDateTime tComDt;
    @Column(name = "t_mileage", nullable = true)
    private int tMileage;
    @Column(name = "t_mileage_status", nullable = true)
    private String tMileageStatus;
    @Column(name = "t_mileage_status_name", nullable = true)
    private String tMileageStatusName;
    @Column(name = "t_mileage_reject_reason", nullable = true)
    private String tMileageRejectReason;
    @Column(name = "c_campaign_seq", nullable = true)
    private Long cCampaignSeq;
    @Column(name = "c_reg_user_id", nullable = true)
    private String cRegUserid;
    @Column(name = "c_reg_user_name", nullable = true)
    private String cRegUserName;
    @Column(name = "c_reg_user_phone", nullable = true)
    private String cRegUserPhone;
    @Column(name = "c_reg_user_email", nullable = true)
    private String cRegUserEmail;
    @Column(name = "c_campaign_process", nullable = true)
    private String cCampaignProcess;
    @Column(name = "c_campaign_process_name", nullable = true)
    private String cCampaignProcessName;
    @Column(name = "c_reg_dt", nullable = true)
    private LocalDateTime cRegDt;
    @Column(name = "c_com_dt", nullable = true)
    private LocalDateTime cComDt;
    @Column(name = "c_mileage", nullable = true)
    private int cMileage;
    @Column(name = "c_mileage_status", nullable = true)
    private String cMileageStatus;
    @Column(name = "c_mileage_status_name", nullable = true)
    private String cMileageStatusName;
    @Column(name = "c_mileage_reject_reason", nullable = true)
    private String cMileageRejectReason;




    @Builder
    CampaignView(String campaignNum,
                 Long trashType,
                 String trashName,
                 String campaignStatusName,
                 String areaName,
                 String addrNew,
                 String addrOld,
                 String locationX,
                 String locationY,
                 Long tCampaignSeq,
                 String tRegUserid,
                 String tRegUserName,
                 String tRegUserPhone,
                 String tRegUserEmail,
                 String tCampaignProcess,
                 String tCampaignProcessName,
                 LocalDateTime tRegDt,
                 LocalDateTime tComDt,
                 int tMileage,
                 String tMileageStatus,
                 String tMileageStatusName,
                 String tMileageRejectReason,
                 Long cCampaignSeq,
                 String cRegUserid,
                 String cRegUserName,
                 String cRegUserPhone,
                 String cRegUserEmail,
                 String cCampaignProcess,
                 String cCampaignProcessName,
                 LocalDateTime cRegDt,
                 LocalDateTime cComDt,
                 int cMileage,
                 String cMileageStatus,
                 String cMileageStatusName,
                 String cMileageRejectReason
    ) {
        this.campaignNum = campaignNum;
        this.trashType = trashType;
        this.trashName = trashName;
        this.campaignStatusName = campaignStatusName;
        this.areaName = areaName;
        this.addrNew = addrNew;
        this.addrOld = addrOld;
        this.locationX = locationX;
        this.locationY = locationY;
        this.tCampaignSeq = tCampaignSeq;
        this.tRegUserid = tRegUserid;
        this.tRegUserName = tRegUserName;
        this.tRegUserPhone = tRegUserPhone;
        this.tRegUserEmail = tRegUserEmail;
        this.tCampaignProcess = tCampaignProcess;
        this.tCampaignProcessName = tCampaignProcessName;
        this.tRegDt = tRegDt;
        this.tComDt = tComDt;
        this.tMileage = tMileage;
        this.tMileageStatus = tMileageStatus;
        this.tMileageStatusName = tMileageStatusName;
        this.tMileageRejectReason = tMileageRejectReason;
        this.cCampaignSeq = cCampaignSeq;
        this.cRegUserid = cRegUserid;
        this.cRegUserName = cRegUserName;
        this.cRegUserPhone = cRegUserPhone;
        this.cRegUserEmail = cRegUserEmail;
        this.cCampaignProcess = cCampaignProcess;
        this.cCampaignProcessName = cCampaignProcessName;
        this.cRegDt = cRegDt;
        this.cComDt = cComDt;
        this.cMileage = cMileage;
        this.cMileageStatus = cMileageStatus;
        this.cMileageStatusName = cMileageStatusName;
        this.cMileageRejectReason = cMileageRejectReason;
    }


}
