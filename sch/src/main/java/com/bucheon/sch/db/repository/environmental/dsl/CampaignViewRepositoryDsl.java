package com.bucheon.sch.db.repository.environmental.dsl;


import com.bucheon.sch.biz.environmental.campaign.v1.model.CampaignExcelDto;
import com.bucheon.sch.biz.environmental.campaign.v1.model.CampaignReq;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CampaignViewRepositoryDsl {

    @Transactional(readOnly = true)
    List<CampaignExcelDto> findExcelList(CampaignReq dto);

}
