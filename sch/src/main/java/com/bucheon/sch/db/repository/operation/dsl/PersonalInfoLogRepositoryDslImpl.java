package com.bucheon.sch.db.repository.operation.dsl;

import com.bucheon.sch.biz.operation.personalInfo.v1.model.PersonalInfoLogReq;
import com.bucheon.sch.db.entity.operation.PersonalInfoLog;
import com.bucheon.sch.db.entity.operation.QAdmin;
import com.bucheon.sch.db.entity.operation.QPersonalInfoLog;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class PersonalInfoLogRepositoryDslImpl implements  PersonalInfoLogRepositoryDsl{

    private final JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(PersonalInfoLogRepositoryDslImpl.class);
    private QPersonalInfoLog qPersonalInfoLog = QPersonalInfoLog.personalInfoLog;
    private QAdmin qAdmin = QAdmin.admin;


    @Override
    public List<PersonalInfoLog> PersonalInfoLogSelectList(PersonalInfoLogReq dto) {

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        List<PersonalInfoLog> result =  queryFactory.selectFrom(qPersonalInfoLog)
                .leftJoin(qAdmin).on(qPersonalInfoLog.adminSeq.eq(qAdmin.adminSeq))
                .where(eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()))
                .offset(dto.getPage() * dto.getSize())
                .limit(dto.getSize())
                .orderBy(qPersonalInfoLog.logSeq.desc())
                .fetch();
        return result;
    }
    @Override
    public QueryResults<PersonalInfoLog> PersonalInfoLogSelectListPaging(PersonalInfoLogReq dto) {

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        QueryResults<PersonalInfoLog> result = queryFactory.selectFrom(qPersonalInfoLog)
                .leftJoin(qAdmin).on(qPersonalInfoLog.adminSeq.eq(qAdmin.adminSeq))
                .where(eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()))
                .offset(dto.getPage() * dto.getSize())
                .limit(dto.getSize())
                .orderBy(qPersonalInfoLog.logSeq.desc())
                .fetchResults();
        return result;
    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchsearchSelect(String searchSelect, String searchString) {

        try {

            if (searchSelect.equals("searchName")) {
                return eqAdminName(searchString);
            } else if (searchSelect.equals("searchMenu")) {
                return eqMenuName(searchString);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqAdminName(String name) {
        if (StringUtils.isEmptyOrWhitespace(name)) {
            return null;
        }
        return qAdmin.adminName.eq(name);
    }

    private BooleanExpression eqMenuName(String name) {
        if (StringUtils.isEmptyOrWhitespace(name)) {
            return null;
        }
        return qPersonalInfoLog.menuName.contains(name);
    }



}
