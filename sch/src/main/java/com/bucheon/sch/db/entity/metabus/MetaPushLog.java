package com.bucheon.sch.db.entity.metabus;

import com.bucheon.sch.db.entity.metabus.Id.MetaPushLogId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_meta_push_log")
@Getter
@Setter
@NoArgsConstructor
@IdClass(MetaPushLogId.class)
public class MetaPushLog {

    @Id
    @Column(name = "send_seq", nullable = true)
    private Long sendSeq;
    @Id
    @Column(name = "userId", nullable = true)
    private String userId;
    @Column(name = "device_token", nullable = true)
    private String deviceToken;
    @Column(name = "result_code", nullable = true)
    private String resultCode;
    @Column(name = "send_dt", nullable = true)
    private LocalDateTime sendDt;

    @Builder
    MetaPushLog(
            Long sendSeq,
            String userId,
            String deviceToken,
            String resultCode,
            LocalDateTime sendDt
            ) {
        this.sendSeq = sendSeq;
        this.userId = userId;
        this.deviceToken = deviceToken;
        this.resultCode = resultCode;
        this.sendDt = sendDt;
    }

}
