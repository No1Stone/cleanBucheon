package com.bucheon.sch.db.entity.environmental;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_report_img")
@Getter
@Setter
@NoArgsConstructor
public class ReportImg {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "report_img_seq", nullable = true)
    private Long reportImgSeq;
    @Column(name = "report_seq", nullable = true)
    private Long reportSeq;
    @Column(name = "report_img", nullable = true)
    private String reportImg;

    @Builder
    ReportImg(Long reportImgSeq,
              Long reportSeq,
              String reportImg) {
        this.reportImgSeq = reportImgSeq;
        this.reportSeq = reportSeq;
        this.reportImg = reportImg;
    }

}
