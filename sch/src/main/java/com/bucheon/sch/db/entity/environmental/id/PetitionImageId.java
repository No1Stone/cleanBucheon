package com.bucheon.sch.db.entity.environmental.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class PetitionImageId implements Serializable {
    private String uuid;
    private String petiNo;
}
