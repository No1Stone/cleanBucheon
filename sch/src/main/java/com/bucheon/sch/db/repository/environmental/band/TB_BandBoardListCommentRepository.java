package com.bucheon.sch.db.repository.environmental.band;

import com.bucheon.sch.db.entity.environmental.band.TB_BandBoardListComment;
import com.bucheon.sch.db.entity.environmental.band.id.TB_BandBoardListCommentId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TB_BandBoardListCommentRepository extends JpaRepository<TB_BandBoardListComment, TB_BandBoardListCommentId> {

    int countByCommentKeyAndPostKey(String commentKey, String postKey);

}
