package com.bucheon.sch.db.repository.environmental;

import com.bucheon.sch.db.entity.environmental.Process;
import com.bucheon.sch.db.entity.environmental.id.ProcessId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessRepository extends JpaRepository<Process, ProcessId> {
}
