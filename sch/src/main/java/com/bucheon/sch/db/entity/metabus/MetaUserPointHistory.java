package com.bucheon.sch.db.entity.metabus;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_meta_user_point_history")
@Getter
@Setter
@NoArgsConstructor
public class MetaUserPointHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "point_seq", nullable = true)
    private Long pointSeq;
    @Column(name = "user_seq", nullable = true)
    private Long userSeq;
    @Column(name = "user_id", nullable = true)
    private String userId;
    @Column(name = "point", nullable = true)
    private int point;
    @Column(name = "accumulated_point", nullable = true)
    private int accumulatedPoint;
    @Column(name = "trash_seq", nullable = true)
    private Long trashSeq;
    @Column(name = "reason", nullable = true)
    private String reason;
    @Column(name = "process_location", nullable = true)
    private String processLocation;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;


    @Builder
    MetaUserPointHistory(
            Long pointSeq,
            Long userSeq,
            String userId,
            int point,
            int accumulatedPoint,
            Long trashSeq,
            String processLocation,
            String reason,
            LocalDateTime regDt
    ) {
        this.pointSeq = pointSeq;
        this.userSeq = userSeq;
        this.userId = userId;
        this.point = point;
        this.accumulatedPoint = accumulatedPoint;
        this.trashSeq = trashSeq;
        this.processLocation = processLocation;
        this.reason = reason;
        this.regDt = regDt;
    }


}
