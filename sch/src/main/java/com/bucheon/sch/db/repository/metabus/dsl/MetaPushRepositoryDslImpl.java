package com.bucheon.sch.db.repository.metabus.dsl;

import com.bucheon.sch.biz.metabus.metaPush.v1.model.MetaPushReq;
import com.bucheon.sch.db.entity.metabus.MetaPush;
import com.bucheon.sch.db.entity.metabus.QMetaPush;
import com.bucheon.sch.db.entity.operation.QAdmin;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class MetaPushRepositoryDslImpl implements MetaPushRepositoryDsl {


    private final JPAQueryFactory queryFactory;
    private QMetaPush qMetaPush = QMetaPush.metaPush;
    private QAdmin qAdmin = QAdmin.admin;
    private final Logger logger = LoggerFactory.getLogger(MetaPushRepositoryDslImpl.class);

    /// 일반 게시판 ////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<MetaPush> SelectListMetaPush(MetaPushReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }

        var aa = queryFactory.select(qMetaPush).from(qMetaPush)
                .where(
                        eqMetaSendType(dto.getSearchSendType()),
                        eqMetaResultType(dto.getSearchResultType()),
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString(), sr, er)
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qMetaPush.regDt.desc())
                .fetch();
        return aa;
    }

    @Override
    public QueryResults<MetaPush> SelectListMetaPushPaging(MetaPushReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        var aa = queryFactory.select(qMetaPush).from(qMetaPush)
                .where(
                        eqMetaSendType(dto.getSearchSendType()),
                        eqMetaResultType(dto.getSearchResultType()),
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString(), sr, er)
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .fetchResults();

        return aa;
    }
    /// 일반 게시판 ////////////////////////////////////////////////////////////////////////////////////////

    //전체 검색 종류 확인
    private BooleanExpression eqSearchsearchSelect(String searchSelect, String searchString,
                                                   LocalDateTime sr, LocalDateTime er) {

        try {

            if (searchSelect.equals("searchTitle")) {
                return eqMetaPushTitle(searchString);
            } else if (searchSelect.equals("searchBody")) {
                return eqMetaPushBody(searchString);
            } else if (searchSelect.equals("searchReg")) {
                return eqRegdt(sr, er);
            } else if (searchSelect.equals("searchSend")) {
                return eqSenddt(sr, er);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqMetaSendType(String s) {
        if (StringUtils.isEmptyOrWhitespace(s)) {
            return null;
        }
        return qMetaPush.sendType.eq(s);
    }

    private BooleanExpression eqMetaResultType(String s) {
        if (StringUtils.isEmptyOrWhitespace(s)) {
            return null;
        }
        return qMetaPush.resultType.eq(s);
    }

    private BooleanExpression eqRegdt(LocalDateTime sdate, LocalDateTime edate) {
        if (sdate == null && edate == null) {
            return null;
        }
        return qMetaPush.regDt.between(sdate, edate);
    }

    private BooleanExpression eqSenddt(LocalDateTime sdate, LocalDateTime edate) {
        if (sdate == null && edate == null) {
            return null;
        }
        return qMetaPush.sendDt.between(sdate, edate);
    }

    private BooleanExpression eqMetaPushTitle(String s) {
        if (StringUtils.isEmptyOrWhitespace(s)) {
            return null;
        }
        return qMetaPush.title.toLowerCase().contains(s);
    }

    private BooleanExpression eqMetaPushBody(String s) {
        if (StringUtils.isEmptyOrWhitespace(s)) {
            return null;
        }
        return qMetaPush.body.toLowerCase().contains(s);
    }


}
