package com.bucheon.sch.db.repository.metabus;

import com.bucheon.sch.db.entity.metabus.MetaTrashType;
import com.bucheon.sch.db.repository.metabus.dsl.MetaTrashTypeRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface MetaTrashTypeRepository extends JpaRepository<MetaTrashType, String>, MetaTrashTypeRepositoryDsl {

    @Transactional(readOnly = true)
    Optional<MetaTrashType> findByTrashSeq(Long seq);

    @Transactional(readOnly = true)
    Optional<MetaTrashType> findByTrashName(String snsId);

    @Transactional(readOnly = true)
    List<MetaTrashType> findAllByUseYnAndDelYn(String use, String del);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_trash_type set use_yn = :useYn where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateTrashTypeUseYn(@Param("useYn") String useYn, @Param("trashSeq") Long trashSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_trash_type set del_yn = :delYn where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateTrashTypeDelYn(@Param("delYn") String delYn, @Param("trashSeq") Long trashSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_trash_type set trash_name = :trashName where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateTrashTypeName(@Param("trashName") String trashName, @Param("trashSeq") Long trashSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_trash_type set trash_act = :trashAct where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateTrashAct(@Param("trashAct") String trashAct, @Param("trashSeq") Long trashSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_trash_type set trash_point = :trashPoint where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateTrashPoint(@Param("trashPoint") int trashPoint, @Param("trashSeq") Long trashSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_trash_type set mod_seq = :modSeq, mod_dt = now() where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateMod(@Param("modSeq") Long modSeq, @Param("trashSeq") Long trashSeq);

}
