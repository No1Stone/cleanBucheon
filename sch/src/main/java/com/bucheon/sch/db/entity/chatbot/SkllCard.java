package com.bucheon.sch.db.entity.chatbot;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_skll_card")
@Getter
@Setter
@NoArgsConstructor
public class SkllCard {

    @Id
    @Column(name = "skll_code", nullable = true)
    private String skllCode;
    @Column(name = "sj", nullable = true)
    private String sj;
    @Column(name = "thumb", nullable = true)
    private String thumb;
    @Column(name = "profl", nullable = true)
    private String profl;
    @Column(name = "socty", nullable = true)
    private String socty;
    @Column(name = "btton", nullable = true)
    private String btton;
    @Column(name = "bc_ordr", nullable = true)
    private int bcOrdr;
    @Column(name = "rspns_ordr", nullable = true)
    private int rspnsOrdr;
    @Column(name = "dc", nullable = true)
    private String dc;
    @Column(name = "btton_optn_at", nullable = true)
    private String bttonOptnAt;
    @Column(name = "profl_crtfc_at", nullable = true)
    private String proflCrtfcAt;


    @Builder
    SkllCard(
            String skllCode,
            String sj,
            String thumb,
            String profl,
            String socty,
            String btton,
            int bcOrdr,
            int rspnsOrdr,
            String dc,
            String bttonOptnAt,
            String proflCrtfcAt
    ) {
        this.skllCode=skllCode;
        this.sj=sj;
        this.thumb=thumb;
        this.profl=profl;
        this.socty=socty;
        this.btton=btton;
        this.bcOrdr=bcOrdr;
        this.rspnsOrdr=rspnsOrdr;
        this.dc=dc;
        this.bttonOptnAt=bttonOptnAt;
        this.proflCrtfcAt=proflCrtfcAt;
    }

}
