package com.bucheon.sch.db.entity.operation;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_personal_info_log")
@Getter
@Setter
@NoArgsConstructor
public class PersonalInfoLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "log_seq", nullable = true)
    private Long logSeq;
    @Column(name = "log_time", nullable = true)
    private LocalDateTime logTime;
    @Column(name = "admin_seq", nullable = true)
    private Long adminSeq;
    @Column(name = "menu_name", nullable = true)
    private String menuName;
    @Column(name = "menu_han_name", nullable = true)
    private String menuHanName;
    @Column(name = "seq", nullable = true)
    private Long seq;
    @Column(name = "did", nullable = true)
    private String did;
    @Column(name = "reason", nullable = true)
    private String reason;


    @Builder
    PersonalInfoLog(Long logSeq,
                    LocalDateTime logTime,
                    Long adminSeq,
                    String menuName,
                    String menuHanName,
                    Long seq,
                    String did,
                    String reason
    ) {
        this.logSeq = logSeq;
        this.logTime = logTime;
        this.adminSeq = adminSeq;
        this.menuName = menuName;
        this.menuHanName = menuHanName;
        this.seq = seq;
        this.did = did;
        this.reason = reason;
    }

}
