package com.bucheon.sch.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSkll is a Querydsl query type for Skll
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSkll extends EntityPathBase<Skll> {

    private static final long serialVersionUID = -2079711465L;

    public static final QSkll skll = new QSkll("skll");

    public final StringPath proflCrtfcAt = createString("proflCrtfcAt");

    public final DatePath<java.time.LocalDate> registDe = createDate("registDe", java.time.LocalDate.class);

    public final StringPath rspnsTy = createString("rspnsTy");

    public final StringPath rspnsTy1 = createString("rspnsTy1");

    public final StringPath rspnsTy2 = createString("rspnsTy2");

    public final StringPath rspnsTy3 = createString("rspnsTy3");

    public final StringPath skllCatgory = createString("skllCatgory");

    public final StringPath skllCode = createString("skllCode");

    public final StringPath skllCtgry = createString("skllCtgry");

    public final StringPath skllLevel = createString("skllLevel");

    public final StringPath skllLevel1 = createString("skllLevel1");

    public final StringPath skllLevel2 = createString("skllLevel2");

    public final StringPath skllLevel3 = createString("skllLevel3");

    public final StringPath skllLevel4 = createString("skllLevel4");

    public final StringPath skllNm = createString("skllNm");

    public final StringPath skllUrl = createString("skllUrl");

    public final StringPath skllUrl2 = createString("skllUrl2");

    public final DatePath<java.time.LocalDate> updtDe = createDate("updtDe", java.time.LocalDate.class);

    public QSkll(String variable) {
        super(Skll.class, forVariable(variable));
    }

    public QSkll(Path<? extends Skll> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSkll(PathMetadata metadata) {
        super(Skll.class, metadata);
    }

}

