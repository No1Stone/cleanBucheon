package com.bucheon.sch.db.entity.environmental;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCampaignImg is a Querydsl query type for CampaignImg
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCampaignImg extends EntityPathBase<CampaignImg> {

    private static final long serialVersionUID = -460566013L;

    public static final QCampaignImg campaignImg1 = new QCampaignImg("campaignImg1");

    public final StringPath campaignImg = createString("campaignImg");

    public final NumberPath<Long> campaignImgSeq = createNumber("campaignImgSeq", Long.class);

    public final NumberPath<Long> campaignSeq = createNumber("campaignSeq", Long.class);

    public QCampaignImg(String variable) {
        super(CampaignImg.class, forVariable(variable));
    }

    public QCampaignImg(Path<? extends CampaignImg> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCampaignImg(PathMetadata metadata) {
        super(CampaignImg.class, metadata);
    }

}

