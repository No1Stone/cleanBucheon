package com.bucheon.sch.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSkllList is a Querydsl query type for SkllList
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSkllList extends EntityPathBase<SkllList> {

    private static final long serialVersionUID = 625664085L;

    public static final QSkllList skllList = new QSkllList("skllList");

    public final StringPath btton = createString("btton");

    public final StringPath bttonOptnAt = createString("bttonOptnAt");

    public final StringPath hder = createString("hder");

    public final StringPath list = createString("list");

    public final NumberPath<Integer> rspnsOrdr = createNumber("rspnsOrdr", Integer.class);

    public final StringPath skllCode = createString("skllCode");

    public QSkllList(String variable) {
        super(SkllList.class, forVariable(variable));
    }

    public QSkllList(Path<? extends SkllList> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSkllList(PathMetadata metadata) {
        super(SkllList.class, metadata);
    }

}

