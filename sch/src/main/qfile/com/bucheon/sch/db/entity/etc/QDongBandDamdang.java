package com.bucheon.sch.db.entity.etc;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDongBandDamdang is a Querydsl query type for DongBandDamdang
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDongBandDamdang extends EntityPathBase<DongBandDamdang> {

    private static final long serialVersionUID = -401629709L;

    public static final QDongBandDamdang dongBandDamdang = new QDongBandDamdang("dongBandDamdang");

    public final StringPath damdang = createString("damdang");

    public final StringPath damdangPhone = createString("damdangPhone");

    public final StringPath dong = createString("dong");

    public QDongBandDamdang(String variable) {
        super(DongBandDamdang.class, forVariable(variable));
    }

    public QDongBandDamdang(Path<? extends DongBandDamdang> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDongBandDamdang(PathMetadata metadata) {
        super(DongBandDamdang.class, metadata);
    }

}

