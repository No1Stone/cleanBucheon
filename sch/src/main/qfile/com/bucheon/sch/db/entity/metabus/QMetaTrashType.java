package com.bucheon.sch.db.entity.metabus;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMetaTrashType is a Querydsl query type for MetaTrashType
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMetaTrashType extends EntityPathBase<MetaTrashType> {

    private static final long serialVersionUID = -1026642534L;

    public static final QMetaTrashType metaTrashType = new QMetaTrashType("metaTrashType");

    public final StringPath delYn = createString("delYn");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modSeq = createNumber("modSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regSeq = createNumber("regSeq", Long.class);

    public final StringPath trashAct = createString("trashAct");

    public final StringPath trashName = createString("trashName");

    public final NumberPath<Integer> trashPoint = createNumber("trashPoint", Integer.class);

    public final NumberPath<Long> trashSeq = createNumber("trashSeq", Long.class);

    public final StringPath useYn = createString("useYn");

    public QMetaTrashType(String variable) {
        super(MetaTrashType.class, forVariable(variable));
    }

    public QMetaTrashType(Path<? extends MetaTrashType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMetaTrashType(PathMetadata metadata) {
        super(MetaTrashType.class, metadata);
    }

}

