package com.bucheon.sch.db.entity.metabus;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMetaUserVoteHistory is a Querydsl query type for MetaUserVoteHistory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMetaUserVoteHistory extends EntityPathBase<MetaUserVoteHistory> {

    private static final long serialVersionUID = -231302521L;

    public static final QMetaUserVoteHistory metaUserVoteHistory = new QMetaUserVoteHistory("metaUserVoteHistory");

    public final NumberPath<Long> boardSeq = createNumber("boardSeq", Long.class);

    public final StringPath boardType = createString("boardType");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final StringPath userId = createString("userId");

    public final NumberPath<Long> voteSeq = createNumber("voteSeq", Long.class);

    public final StringPath voteType = createString("voteType");

    public QMetaUserVoteHistory(String variable) {
        super(MetaUserVoteHistory.class, forVariable(variable));
    }

    public QMetaUserVoteHistory(Path<? extends MetaUserVoteHistory> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMetaUserVoteHistory(PathMetadata metadata) {
        super(MetaUserVoteHistory.class, metadata);
    }

}

