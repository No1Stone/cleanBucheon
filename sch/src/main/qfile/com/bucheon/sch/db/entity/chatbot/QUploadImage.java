package com.bucheon.sch.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QUploadImage is a Querydsl query type for UploadImage
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUploadImage extends EntityPathBase<UploadImage> {

    private static final long serialVersionUID = 928314811L;

    public static final QUploadImage uploadImage = new QUploadImage("uploadImage");

    public final StringPath imageAttribute = createString("imageAttribute");

    public final StringPath imageDefaultYn = createString("imageDefaultYn");

    public final StringPath imageFilename = createString("imageFilename");

    public final StringPath imageFileservername = createString("imageFileservername");

    public final StringPath imageId = createString("imageId");

    public final StringPath imageName = createString("imageName");

    public final NumberPath<Long> imageSeq = createNumber("imageSeq", Long.class);

    public QUploadImage(String variable) {
        super(UploadImage.class, forVariable(variable));
    }

    public QUploadImage(Path<? extends UploadImage> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUploadImage(PathMetadata metadata) {
        super(UploadImage.class, metadata);
    }

}

