package com.bucheon.sch.db.entity.operation;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDeclarationPoint is a Querydsl query type for DeclarationPoint
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDeclarationPoint extends EntityPathBase<DeclarationPoint> {

    private static final long serialVersionUID = -387939331L;

    public static final QDeclarationPoint declarationPoint1 = new QDeclarationPoint("declarationPoint1");

    public final NumberPath<Integer> declarationPoint = createNumber("declarationPoint", Integer.class);

    public final NumberPath<Long> declarationSeq = createNumber("declarationSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modSeq = createNumber("modSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regSeq = createNumber("regSeq", Long.class);

    public QDeclarationPoint(String variable) {
        super(DeclarationPoint.class, forVariable(variable));
    }

    public QDeclarationPoint(Path<? extends DeclarationPoint> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDeclarationPoint(PathMetadata metadata) {
        super(DeclarationPoint.class, metadata);
    }

}

