package com.bucheon.sch.db.entity.metabus;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMetaNoticePopup is a Querydsl query type for MetaNoticePopup
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMetaNoticePopup extends EntityPathBase<MetaNoticePopup> {

    private static final long serialVersionUID = -198516292L;

    public static final QMetaNoticePopup metaNoticePopup = new QMetaNoticePopup("metaNoticePopup");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modSeq = createNumber("modSeq", Long.class);

    public final StringPath popupImageUrl = createString("popupImageUrl");

    public final NumberPath<Long> popupSeq = createNumber("popupSeq", Long.class);

    public final StringPath popupType = createString("popupType");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regSeq = createNumber("regSeq", Long.class);

    public final StringPath useYn = createString("useYn");

    public QMetaNoticePopup(String variable) {
        super(MetaNoticePopup.class, forVariable(variable));
    }

    public QMetaNoticePopup(Path<? extends MetaNoticePopup> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMetaNoticePopup(PathMetadata metadata) {
        super(MetaNoticePopup.class, metadata);
    }

}

