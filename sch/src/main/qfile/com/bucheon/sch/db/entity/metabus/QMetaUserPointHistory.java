package com.bucheon.sch.db.entity.metabus;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMetaUserPointHistory is a Querydsl query type for MetaUserPointHistory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMetaUserPointHistory extends EntityPathBase<MetaUserPointHistory> {

    private static final long serialVersionUID = -711155641L;

    public static final QMetaUserPointHistory metaUserPointHistory = new QMetaUserPointHistory("metaUserPointHistory");

    public final NumberPath<Integer> accumulatedPoint = createNumber("accumulatedPoint", Integer.class);

    public final NumberPath<Integer> point = createNumber("point", Integer.class);

    public final NumberPath<Long> pointSeq = createNumber("pointSeq", Long.class);

    public final StringPath processLocation = createString("processLocation");

    public final StringPath reason = createString("reason");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> trashSeq = createNumber("trashSeq", Long.class);

    public final StringPath userId = createString("userId");

    public final NumberPath<Long> userSeq = createNumber("userSeq", Long.class);

    public QMetaUserPointHistory(String variable) {
        super(MetaUserPointHistory.class, forVariable(variable));
    }

    public QMetaUserPointHistory(Path<? extends MetaUserPointHistory> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMetaUserPointHistory(PathMetadata metadata) {
        super(MetaUserPointHistory.class, metadata);
    }

}

