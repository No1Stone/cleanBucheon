package com.bucheon.sch.db.entity.metabus;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMetaTimeAttact is a Querydsl query type for MetaTimeAttact
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMetaTimeAttact extends EntityPathBase<MetaTimeAttact> {

    private static final long serialVersionUID = 432631158L;

    public static final QMetaTimeAttact metaTimeAttact = new QMetaTimeAttact("metaTimeAttact");

    public final StringPath delYn = createString("delYn");

    public final StringPath etime = createString("etime");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modSeq = createNumber("modSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regSeq = createNumber("regSeq", Long.class);

    public final StringPath stime = createString("stime");

    public final NumberPath<Integer> timeMultiplication = createNumber("timeMultiplication", Integer.class);

    public final NumberPath<Long> timeSeq = createNumber("timeSeq", Long.class);

    public final StringPath useYn = createString("useYn");

    public QMetaTimeAttact(String variable) {
        super(MetaTimeAttact.class, forVariable(variable));
    }

    public QMetaTimeAttact(Path<? extends MetaTimeAttact> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMetaTimeAttact(PathMetadata metadata) {
        super(MetaTimeAttact.class, metadata);
    }

}

