package com.bucheon.sch.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSkllItm is a Querydsl query type for SkllItm
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSkllItm extends EntityPathBase<SkllItm> {

    private static final long serialVersionUID = -1780935157L;

    public static final QSkllItm skllItm = new QSkllItm("skllItm");

    public final StringPath btton = createString("btton");

    public final StringPath bttonOptnAt = createString("bttonOptnAt");

    public final StringPath dc = createString("dc");

    public final StringPath head = createString("head");

    public final StringPath imageSj = createString("imageSj");

    public final StringPath itmList = createString("itmList");

    public final StringPath itmOrdr = createString("itmOrdr");

    public final StringPath proflCrtfcAt = createString("proflCrtfcAt");

    public final StringPath rspnsOrdr = createString("rspnsOrdr");

    public final StringPath seoultalkBtton = createString("seoultalkBtton");

    public final StringPath sj = createString("sj");

    public final StringPath skllCode = createString("skllCode");

    public final StringPath thumb = createString("thumb");

    public QSkllItm(String variable) {
        super(SkllItm.class, forVariable(variable));
    }

    public QSkllItm(Path<? extends SkllItm> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSkllItm(PathMetadata metadata) {
        super(SkllItm.class, metadata);
    }

}

