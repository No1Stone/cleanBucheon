package com.bucheon.sch.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSkllText is a Querydsl query type for SkllText
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSkllText extends EntityPathBase<SkllText> {

    private static final long serialVersionUID = 625898724L;

    public static final QSkllText skllText = new QSkllText("skllText");

    public final StringPath btton = createString("btton");

    public final NumberPath<Integer> rspnsOrdr = createNumber("rspnsOrdr", Integer.class);

    public final StringPath skllCode = createString("skllCode");

    public final StringPath text = createString("text");

    public QSkllText(String variable) {
        super(SkllText.class, forVariable(variable));
    }

    public QSkllText(Path<? extends SkllText> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSkllText(PathMetadata metadata) {
        super(SkllText.class, metadata);
    }

}

