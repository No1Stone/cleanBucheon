package com.bucheon.sch.db.entity.environmental;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCampaignLog is a Querydsl query type for CampaignLog
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCampaignLog extends EntityPathBase<CampaignLog> {

    private static final long serialVersionUID = -460563068L;

    public static final QCampaignLog campaignLog = new QCampaignLog("campaignLog");

    public final NumberPath<Long> campaignDamdang = createNumber("campaignDamdang", Long.class);

    public final StringPath campaignNum = createString("campaignNum");

    public final NumberPath<Long> campaignSeq = createNumber("campaignSeq", Long.class);

    public final StringPath campaignStatus = createString("campaignStatus");

    public final NumberPath<Long> logSeq = createNumber("logSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> logTime = createDateTime("logTime", java.time.LocalDateTime.class);

    public final StringPath msg = createString("msg");

    public final StringPath rejectReason = createString("rejectReason");

    public QCampaignLog(String variable) {
        super(CampaignLog.class, forVariable(variable));
    }

    public QCampaignLog(Path<? extends CampaignLog> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCampaignLog(PathMetadata metadata) {
        super(CampaignLog.class, metadata);
    }

}

