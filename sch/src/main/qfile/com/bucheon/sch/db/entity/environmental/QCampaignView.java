package com.bucheon.sch.db.entity.environmental;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCampaignView is a Querydsl query type for CampaignView
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCampaignView extends EntityPathBase<CampaignView> {

    private static final long serialVersionUID = -1392261019L;

    public static final QCampaignView campaignView = new QCampaignView("campaignView");

    public final StringPath addrNew = createString("addrNew");

    public final StringPath addrOld = createString("addrOld");

    public final StringPath areaName = createString("areaName");

    public final StringPath campaignNum = createString("campaignNum");

    public final StringPath campaignStatusName = createString("campaignStatusName");

    public final StringPath cCampaignProcess = createString("cCampaignProcess");

    public final StringPath cCampaignProcessName = createString("cCampaignProcessName");

    public final NumberPath<Long> cCampaignSeq = createNumber("cCampaignSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> cComDt = createDateTime("cComDt", java.time.LocalDateTime.class);

    public final NumberPath<Integer> cMileage = createNumber("cMileage", Integer.class);

    public final StringPath cMileageRejectReason = createString("cMileageRejectReason");

    public final StringPath cMileageStatus = createString("cMileageStatus");

    public final StringPath cMileageStatusName = createString("cMileageStatusName");

    public final DateTimePath<java.time.LocalDateTime> cRegDt = createDateTime("cRegDt", java.time.LocalDateTime.class);

    public final StringPath cRegUserEmail = createString("cRegUserEmail");

    public final StringPath cRegUserid = createString("cRegUserid");

    public final StringPath cRegUserName = createString("cRegUserName");

    public final StringPath cRegUserPhone = createString("cRegUserPhone");

    public final StringPath locationX = createString("locationX");

    public final StringPath locationY = createString("locationY");

    public final StringPath tCampaignProcess = createString("tCampaignProcess");

    public final StringPath tCampaignProcessName = createString("tCampaignProcessName");

    public final NumberPath<Long> tCampaignSeq = createNumber("tCampaignSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> tComDt = createDateTime("tComDt", java.time.LocalDateTime.class);

    public final NumberPath<Integer> tMileage = createNumber("tMileage", Integer.class);

    public final StringPath tMileageRejectReason = createString("tMileageRejectReason");

    public final StringPath tMileageStatus = createString("tMileageStatus");

    public final StringPath tMileageStatusName = createString("tMileageStatusName");

    public final StringPath trashName = createString("trashName");

    public final NumberPath<Long> trashType = createNumber("trashType", Long.class);

    public final DateTimePath<java.time.LocalDateTime> tRegDt = createDateTime("tRegDt", java.time.LocalDateTime.class);

    public final StringPath tRegUserEmail = createString("tRegUserEmail");

    public final StringPath tRegUserid = createString("tRegUserid");

    public final StringPath tRegUserName = createString("tRegUserName");

    public final StringPath tRegUserPhone = createString("tRegUserPhone");

    public QCampaignView(String variable) {
        super(CampaignView.class, forVariable(variable));
    }

    public QCampaignView(Path<? extends CampaignView> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCampaignView(PathMetadata metadata) {
        super(CampaignView.class, metadata);
    }

}

