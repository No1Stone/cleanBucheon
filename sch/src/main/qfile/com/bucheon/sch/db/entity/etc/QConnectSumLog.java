package com.bucheon.sch.db.entity.etc;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QConnectSumLog is a Querydsl query type for ConnectSumLog
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QConnectSumLog extends EntityPathBase<ConnectSumLog> {

    private static final long serialVersionUID = 151087497L;

    public static final QConnectSumLog connectSumLog = new QConnectSumLog("connectSumLog");

    public final DatePath<java.time.LocalDate> connectDate = createDate("connectDate", java.time.LocalDate.class);

    public final StringPath connectType = createString("connectType");

    public final StringPath userId = createString("userId");

    public QConnectSumLog(String variable) {
        super(ConnectSumLog.class, forVariable(variable));
    }

    public QConnectSumLog(Path<? extends ConnectSumLog> path) {
        super(path.getType(), path.getMetadata());
    }

    public QConnectSumLog(PathMetadata metadata) {
        super(ConnectSumLog.class, metadata);
    }

}

