package com.bucheon.sch.db.entity.environmental;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCampaign is a Querydsl query type for Campaign
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCampaign extends EntityPathBase<Campaign> {

    private static final long serialVersionUID = 1688791456L;

    public static final QCampaign campaign = new QCampaign("campaign");

    public final StringPath addrNew = createString("addrNew");

    public final StringPath addrOld = createString("addrOld");

    public final StringPath areaName = createString("areaName");

    public final StringPath campaignNum = createString("campaignNum");

    public final StringPath campaignProcess = createString("campaignProcess");

    public final StringPath campaignProcessRejectReason = createString("campaignProcessRejectReason");

    public final NumberPath<Long> campaignSeq = createNumber("campaignSeq", Long.class);

    public final StringPath campaignStatus = createString("campaignStatus");

    public final DateTimePath<java.time.LocalDateTime> comDt = createDateTime("comDt", java.time.LocalDateTime.class);

    public final StringPath delYn = createString("delYn");

    public final StringPath locationX = createString("locationX");

    public final StringPath locationY = createString("locationY");

    public final StringPath mileage = createString("mileage");

    public final StringPath mileageRejectReason = createString("mileageRejectReason");

    public final StringPath mileageStatus = createString("mileageStatus");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final StringPath regUserDid = createString("regUserDid");

    public final NumberPath<Long> trashType = createNumber("trashType", Long.class);

    public QCampaign(String variable) {
        super(Campaign.class, forVariable(variable));
    }

    public QCampaign(Path<? extends Campaign> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCampaign(PathMetadata metadata) {
        super(Campaign.class, metadata);
    }

}

