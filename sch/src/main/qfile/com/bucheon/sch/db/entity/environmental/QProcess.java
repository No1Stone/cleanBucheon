package com.bucheon.sch.db.entity.environmental;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProcess is a Querydsl query type for Process
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProcess extends EntityPathBase<Process> {

    private static final long serialVersionUID = 1134945247L;

    public static final QProcess process = new QProcess("process");

    public final StringPath answer = createString("answer");

    public final StringPath civilNo = createString("civilNo");

    public final StringPath doDateTime = createString("doDateTime");

    public final StringPath dutyDeptCode = createString("dutyDeptCode");

    public final StringPath dutyId = createString("dutyId");

    public final StringPath dutyName = createString("dutyName");

    public final StringPath petiNo = createString("petiNo");

    public final StringPath regDateTime = createString("regDateTime");

    public final StringPath statusCode = createString("statusCode");

    public final StringPath statusName = createString("statusName");

    public QProcess(String variable) {
        super(Process.class, forVariable(variable));
    }

    public QProcess(Path<? extends Process> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProcess(PathMetadata metadata) {
        super(Process.class, metadata);
    }

}

