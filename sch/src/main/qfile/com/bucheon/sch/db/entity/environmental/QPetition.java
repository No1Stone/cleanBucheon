package com.bucheon.sch.db.entity.environmental;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPetition is a Querydsl query type for Petition
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPetition extends EntityPathBase<Petition> {

    private static final long serialVersionUID = -1974924850L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPetition petition = new QPetition("petition");

    public final StringPath address1 = createString("address1");

    public final StringPath address2 = createString("address2");

    public final StringPath cellPhone = createString("cellPhone");

    public final DateTimePath<java.time.LocalDateTime> createdDate = createDateTime("createdDate", java.time.LocalDateTime.class);

    public final StringPath did = createString("did");

    public final StringPath dong = createString("dong");

    public final StringPath modifyReason = createString("modifyReason");

    public final StringPath name = createString("name");

    public final StringPath petiNo = createString("petiNo");

    public final ListPath<PetitionImage, QPetitionImage> petitionImages = this.<PetitionImage, QPetitionImage>createList("petitionImages", PetitionImage.class, QPetitionImage.class, PathInits.DIRECT2);

    public final ListPath<Process, QProcess> processes = this.<Process, QProcess>createList("processes", Process.class, QProcess.class, PathInits.DIRECT2);

    public final StringPath reason = createString("reason");

    public final QReceipt receipts;

    public final StringPath regDateTime = createString("regDateTime");

    public final StringPath title = createString("title");

    public final StringPath zipCode = createString("zipCode");

    public QPetition(String variable) {
        this(Petition.class, forVariable(variable), INITS);
    }

    public QPetition(Path<? extends Petition> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPetition(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPetition(PathMetadata metadata, PathInits inits) {
        this(Petition.class, metadata, inits);
    }

    public QPetition(Class<? extends Petition> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.receipts = inits.isInitialized("receipts") ? new QReceipt(forProperty("receipts")) : null;
    }

}

