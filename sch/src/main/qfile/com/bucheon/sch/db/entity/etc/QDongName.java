package com.bucheon.sch.db.entity.etc;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDongName is a Querydsl query type for DongName
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDongName extends EntityPathBase<DongName> {

    private static final long serialVersionUID = -1199387287L;

    public static final QDongName dongName = new QDongName("dongName");

    public final StringPath admin = createString("admin");

    public final StringPath name = createString("name");

    public QDongName(String variable) {
        super(DongName.class, forVariable(variable));
    }

    public QDongName(Path<? extends DongName> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDongName(PathMetadata metadata) {
        super(DongName.class, metadata);
    }

}

