package com.bucheon.sch.db.entity.environmental;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QReportImg is a Querydsl query type for ReportImg
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QReportImg extends EntityPathBase<ReportImg> {

    private static final long serialVersionUID = 502101855L;

    public static final QReportImg reportImg1 = new QReportImg("reportImg1");

    public final StringPath reportImg = createString("reportImg");

    public final NumberPath<Long> reportImgSeq = createNumber("reportImgSeq", Long.class);

    public final NumberPath<Long> reportSeq = createNumber("reportSeq", Long.class);

    public QReportImg(String variable) {
        super(ReportImg.class, forVariable(variable));
    }

    public QReportImg(Path<? extends ReportImg> path) {
        super(path.getType(), path.getMetadata());
    }

    public QReportImg(PathMetadata metadata) {
        super(ReportImg.class, metadata);
    }

}

