package cleantown.bucheon.go.kr.batch.db.entity.metabus;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMetaPushLog is a Querydsl query type for MetaPushLog
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMetaPushLog extends EntityPathBase<MetaPushLog> {

    private static final long serialVersionUID = 1828302989L;

    public static final QMetaPushLog metaPushLog = new QMetaPushLog("metaPushLog");

    public final StringPath deviceToken = createString("deviceToken");

    public final StringPath resultCode = createString("resultCode");

    public final DateTimePath<java.time.LocalDateTime> sendDt = createDateTime("sendDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> sendSeq = createNumber("sendSeq", Long.class);

    public final StringPath userId = createString("userId");

    public QMetaPushLog(String variable) {
        super(MetaPushLog.class, forVariable(variable));
    }

    public QMetaPushLog(Path<? extends MetaPushLog> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMetaPushLog(PathMetadata metadata) {
        super(MetaPushLog.class, metadata);
    }

}

