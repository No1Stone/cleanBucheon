package cleantown.bucheon.go.kr.batch.db.entity.band;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QReport is a Querydsl query type for Report
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QReport extends EntityPathBase<Report> {

    private static final long serialVersionUID = -177808920L;

    public static final QReport report = new QReport("report");

    public final StringPath areaName = createString("areaName");

    public final StringPath bandKey = createString("bandKey");

    public final DateTimePath<java.time.LocalDateTime> comDt = createDateTime("comDt", java.time.LocalDateTime.class);

    public final StringPath delYn = createString("delYn");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final StringPath receptionNum = createString("receptionNum");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final StringPath regUserId = createString("regUserId");

    public final StringPath reportAddrNew = createString("reportAddrNew");

    public final StringPath reportAddrOld = createString("reportAddrOld");

    public final StringPath reportContent = createString("reportContent");

    public final StringPath reportLocationX = createString("reportLocationX");

    public final StringPath reportLocationY = createString("reportLocationY");

    public final StringPath reportPoint = createString("reportPoint");

    public final StringPath reportPointCompleteYn = createString("reportPointCompleteYn");

    public final StringPath reportPointRejectReason = createString("reportPointRejectReason");

    public final StringPath reportReception = createString("reportReception");

    public final NumberPath<Long> reportSeq = createNumber("reportSeq", Long.class);

    public final StringPath reportStatus = createString("reportStatus");

    public final StringPath reportStatusRejectReason = createString("reportStatusRejectReason");

    public final NumberPath<Long> reportType = createNumber("reportType", Long.class);

    public final StringPath reportUserEmail = createString("reportUserEmail");

    public final StringPath reportUserName = createString("reportUserName");

    public final StringPath reportUserPhone = createString("reportUserPhone");

    public final StringPath sourcePk = createString("sourcePk");

    public QReport(String variable) {
        super(Report.class, forVariable(variable));
    }

    public QReport(Path<? extends Report> path) {
        super(path.getType(), path.getMetadata());
    }

    public QReport(PathMetadata metadata) {
        super(Report.class, metadata);
    }

}

