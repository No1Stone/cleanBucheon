package cleantown.bucheon.go.kr.batch.db.entity.band;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTB_BandBoardListPoto is a Querydsl query type for TB_BandBoardListPoto
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTB_BandBoardListPoto extends EntityPathBase<TB_BandBoardListPoto> {

    private static final long serialVersionUID = -688018132L;

    public static final QTB_BandBoardListPoto tB_BandBoardListPoto = new QTB_BandBoardListPoto("tB_BandBoardListPoto");

    public final NumberPath<Integer> commentCount = createNumber("commentCount", Integer.class);

    public final NumberPath<Long> createdAt = createNumber("createdAt", Long.class);

    public final StringPath description = createString("description");

    public final NumberPath<Integer> emotionCount = createNumber("emotionCount", Integer.class);

    public final NumberPath<Integer> height = createNumber("height", Integer.class);

    public final BooleanPath isVideoThumbnail = createBoolean("isVideoThumbnail");

    public final StringPath name = createString("name");

    public final StringPath photoAlbumKey = createString("photoAlbumKey");

    public final StringPath photoKey = createString("photoKey");

    public final StringPath postKey = createString("postKey");

    public final StringPath profileImageUrl = createString("profileImageUrl");

    public final StringPath role = createString("role");

    public final StringPath url = createString("url");

    public final StringPath userKey = createString("userKey");

    public final NumberPath<Integer> width = createNumber("width", Integer.class);

    public QTB_BandBoardListPoto(String variable) {
        super(TB_BandBoardListPoto.class, forVariable(variable));
    }

    public QTB_BandBoardListPoto(Path<? extends TB_BandBoardListPoto> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTB_BandBoardListPoto(PathMetadata metadata) {
        super(TB_BandBoardListPoto.class, metadata);
    }

}

