package cleantown.bucheon.go.kr.batch.db.entity.metabus;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMetaPush2 is a Querydsl query type for MetaPush2
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMetaPush2 extends EntityPathBase<MetaPush2> {

    private static final long serialVersionUID = -1191392293L;

    public static final QMetaPush2 metaPush2 = new QMetaPush2("metaPush2");

    public final StringPath body = createString("body");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regSeq = createNumber("regSeq", Long.class);

    public final StringPath resultType = createString("resultType");

    public final NumberPath<Integer> sendCnt = createNumber("sendCnt", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> sendDt = createDateTime("sendDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> sendSeq = createNumber("sendSeq", Long.class);

    public final StringPath sendType = createString("sendType");

    public final StringPath title = createString("title");

    public QMetaPush2(String variable) {
        super(MetaPush2.class, forVariable(variable));
    }

    public QMetaPush2(Path<? extends MetaPush2> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMetaPush2(PathMetadata metadata) {
        super(MetaPush2.class, metadata);
    }

}

