package cleantown.bucheon.go.kr.batch.biz.band.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NaverAddresses {
    private String roadAddress;
    private String jibunAddress;
    private String englishAddress;
    List<AddressElement> addressElements;
    private String x;
    private String y;
    private String distance;
}
