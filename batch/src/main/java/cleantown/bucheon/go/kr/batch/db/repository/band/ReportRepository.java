package cleantown.bucheon.go.kr.batch.db.repository.band;

import cleantown.bucheon.go.kr.batch.db.entity.band.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

public interface ReportRepository extends JpaRepository<Report, Long> {

    @Transactional(readOnly = true)
    Report findByReportSeq(Long seq);

    @Transactional(readOnly = true)
    Report findBySourcePk(String sourcePk);

    @Transactional(readOnly = true)
    int countByReportReceptionAndReportStatusAndDelYn(String reception, String status, String delYn);

    @Transactional(readOnly = true)
    List<Report> findAllByReportReceptionAndReportStatusAndDelYnOrderByReportSeqDesc(String reception, String status, String delYn);

    @Transactional(readOnly = true)
    List<Report> findAllByReportReceptionAndReportStatusOrReportStatusAndDelYnAndSourcePkIsNotNullOrderByReportSeqDesc(String type, String status1, String status2, String delYn);

    boolean existsByBandKey(String bandKey);
    boolean existsBySourcePk(String postKey);


    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set del_yn = :delYn where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateReportDel(@Param("delYn") String delYn, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set report_status = :reportStatus where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateReportStatus(@Param("reportStatus") String reportStatus, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set report_status_reject_reason = :reportStatusRejectReason where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateReportStatusRejectReason(@Param("reportStatusRejectReason") String reportStatusRejectReason, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set com_dt = :comDt where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateComDt(@Param("comDt") LocalDateTime comDt, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set mod_dt = :modDt where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateModDt(@Param("modDt") LocalDateTime modDt, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set report_point = :reportPoint where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateReportPoint(@Param("reportPoint") int reportMilege, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set report_point_reject_reason = :reportPointRejectReason where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateReportPointRejectReason(@Param("reportPointRejectReason") String reportPointRejectReason, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set report_point_complete_yn = :reportPointCompleteYn where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateReportPointCompleteYn(@Param("reportPointCompleteYn") String reportPointCompleteYn, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set report_status = :reportStatus where source_pk =:sourcePk "
            , nativeQuery = true
    )
    int UpdateReportStatusWhereSourcePk(@Param("reportStatus") String reportStatus, @Param("sourcePk") String sourcePk);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set com_dt = :comDt where source_pk =:sourcePk "
            , nativeQuery = true
    )
    int UpdateComDtWhereSourcePk(@Param("comDt") LocalDateTime comDt, @Param("sourcePk") String sourcePk);

}
