package cleantown.bucheon.go.kr.batch.biz.band.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BandAuthor {

    private String name;
    private String description;
    private String role;
    private String profile_image_url;
    private String user_key;


}
