package cleantown.bucheon.go.kr.batch.biz.fcm.service;

import cleantown.bucheon.go.kr.batch.db.entity.metabus.MetaUser;
import cleantown.bucheon.go.kr.batch.db.repository.metabus.MetaUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class MetaUserService {

    private final MetaUserRepository metaUserRepository;

    public List<MetaUser> MetaUserSelectDeviceTokenHasListService() {

        return metaUserRepository.selectDeviceTokenNotNull();
    }
}
