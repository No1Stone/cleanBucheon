package cleantown.bucheon.go.kr.batch.biz.fcm.model;

import cleantown.bucheon.go.kr.batch.db.entity.metabus.MetaPushLog;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetaPushLogVO {

    private Long sendSeq;
    private String userId;
    private String deviceToken;
    private String resultCode;
    private LocalDateTime sendDt;

    public MetaPushLog ofMetaPushLog(){
        MetaPushLog m = new MetaPushLog();
        m.setSendSeq(this.sendSeq);
        m.setUserId(this.userId);
        m.setDeviceToken(this.deviceToken);
        m.setResultCode(this.resultCode);
        m.setSendDt(this.sendDt);
        return m;
    }

}
