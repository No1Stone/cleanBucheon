package cleantown.bucheon.go.kr.batch.db.repository.metabus;

import cleantown.bucheon.go.kr.batch.db.entity.metabus.MetaPush;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface MetaPushRepository extends JpaRepository<MetaPush, String> {

    @Transactional
    int deleteBySendSeq(Long seq);

}
