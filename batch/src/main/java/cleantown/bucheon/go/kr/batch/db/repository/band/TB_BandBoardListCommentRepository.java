package cleantown.bucheon.go.kr.batch.db.repository.band;

import cleantown.bucheon.go.kr.batch.db.entity.band.Id.TB_BandBoardListCommentId;
import cleantown.bucheon.go.kr.batch.db.entity.band.TB_BandBoardListComment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TB_BandBoardListCommentRepository extends JpaRepository<TB_BandBoardListComment, TB_BandBoardListCommentId> {

    int countByCommentKeyAndPostKey(String commentKey, String postKey);

}
