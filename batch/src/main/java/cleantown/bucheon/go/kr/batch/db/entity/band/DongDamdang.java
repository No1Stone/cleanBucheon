package cleantown.bucheon.go.kr.batch.db.entity.band;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_dong_damdang")
@Getter
@Setter
@NoArgsConstructor
public class DongDamdang {

    @Id
    @Column(name = "dong", nullable = true)
    private String dong;
    @Column(name = "damdang", nullable = true)
    private Long damdang;


    @Builder
    DongDamdang(
            String dong,
            Long damdang
            ) {
        this.dong = dong;
        this.damdang = damdang;
    }

}
