package cleantown.bucheon.go.kr.batch.biz.band.jobs;

import cleantown.bucheon.go.kr.batch.biz.band.model.*;
import cleantown.bucheon.go.kr.batch.biz.band.service.ConnectSumService;
import cleantown.bucheon.go.kr.batch.biz.common.util.RestCall;
import cleantown.bucheon.go.kr.batch.db.entity.band.Report;
import cleantown.bucheon.go.kr.batch.db.entity.band.TB_BandBoardList;
import cleantown.bucheon.go.kr.batch.db.repository.band.ReportImgRepository;
import cleantown.bucheon.go.kr.batch.db.repository.band.ReportRepository;
import cleantown.bucheon.go.kr.batch.db.repository.band.TB_BandBoardListRepository;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.*;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManagerFactory;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class BandPostGetConfig {

    private final JobBuilderFactory jobBuilderFactory; // Job 빌더 생성용
    private final StepBuilderFactory stepBuilderFactory; // Step 빌더 생성용
    private final EntityManagerFactory em; // JPA 빌더

    private List<BandBoardList> collectData = new ArrayList<>(); //Rest로 가져온 데이터를 리스트에 넣는다.
    private boolean checkRestCall = false; //RestAPI 호출여부 판단
    private int nextIndex = 0;//리스트의 데이터를 하나씩 인덱스를 통해 가져온다.

    @Autowired
    TB_BandBoardListRepository tb_BandBoardListRepository;

    @Autowired
    ReportRepository reportRepository;

    @Autowired
    ReportImgRepository reportImgRepository;

    @Autowired
    ConnectSumService connectSumService;

    @Autowired
    TB_BandBoardListRepository tB_BandBoardListRepository;

    @Value("${naver.cloud.schema}")
    private String naveropenapiSchema;
    @Value("${naver.cloud.url}")
    private String naveropenapiUrl;
    @Value("${naver.band.schema}")
    private String bandSchema;
    @Value("${naver.band.url}")
    private String bandUrl;

    @Value("${naver.band.accessToken}")
    private String accessToken;

    @Value("${naver.band.accessTokenDev2}")
    private String accessTokenDev2;
    @Value("${naver.band.testBandkey.bucheonTest}")
    private String bucheonTest;
    @Value("${naver.band.testBandkey.sosabonTest}")
    private String sosabonTest;

    @Value("${naver.band.accessTokenProd}")
    private String accessTokenProd;
    @Value("${naver.band.prodBandkey.deasan}")
    private String deasan;
    @Value("${naver.band.prodBandkey.beoman}")
    private String beoman;
    @Value("${naver.band.prodBandkey.bucheon}")
    private String bucheon;
    @Value("${naver.band.prodBandkey.sang}")
    private String sang;
    @Value("${naver.band.prodBandkey.seonggok}")
    private String seonggok;
    @Value("${naver.band.prodBandkey.sosabon}")
    private String sosabon;
    @Value("${naver.band.prodBandkey.sinjoong}")
    private String sinjoong;
    @Value("${naver.band.prodBandkey.simgok}")
    private String simgok;
    @Value("${naver.band.prodBandkey.ojeong}")
    private String ojeong;
    @Value("${naver.band.prodBandkey.joong}")
    private String joong;

    private int chunkSize = 1;


    @Bean
    public Job bandPostGetJob(){
        return jobBuilderFactory.get("bandPostGetJob")
                .start(bandPostGetStep())
                .build();
    }

    @Bean
    public Step bandPostGetStep(){
        return stepBuilderFactory.get("bandPostGetStep")
                .<BandBoardList, TB_BandBoardList>chunk(chunkSize)
                .reader(bandPostGetReader())
                .processor(bandPostGetProcessor())
                .writer(bandPostGetWriter())
                .build();
    }

    @Bean
    public ItemReader<BandBoardList> bandPostGetReader() {

        return new ItemReader<BandBoardList>() {
            @Override
            public BandBoardList read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

                    String accessKey = accessTokenDev2;
                    List<BandBoardList> bbl = new ArrayList<>();
                    JsonToModel.builder(RestCall.get(BandURL.builder().schema(bandSchema + "://")
                                    .baseURL(bandUrl)
                                    .api("/v2.1/bands")
                                    .param("access_token", accessKey)
                                    .build()))
                            .ofObject("result_data")
                            .ofArrayModel("bands", BandList.class)
                            .stream().forEach(e -> {
                                JsonToModel.builder(RestCall.get(BandURL.builder().schema(bandSchema + "://")
                                                .baseURL(bandUrl)
                                                .api("/v2/band/posts")
                                                .param("access_token", accessKey)
                                                .param("band_key", e.getBand_key())
                                                .param("locale", "ko_KR")
                                                .build()))
                                        .ofObject("result_data")
                                        .ofArrayModel("items", BandBoardList.class)
                                        .stream().forEach(z -> {
                                            int cnt = tB_BandBoardListRepository.countByPostKey(z.getPost_key());
                                            if(cnt == 0) {
                                                z.setBandName(e.getName());
                                                bbl.add(z);
                                            }
                                        });
                            });
                collectData = bbl;
                log.info("bbl - {}", new Gson().toJson(bbl));
                log.info("bbl.size - {}", collectData.size());
                //    checkRestCall = true;//다음 read() 부터는 재호출 방지하기 위해 true로 변경

                BandBoardList nextCollect = null; //ItemReader는 반복문으로 동작한다. 하나씩 Writer로 전달해야 한다.

                if (nextIndex < collectData.size()) {//전체 리스트에서 하나씩 추출해서, 하나씩 Writer로 전달
                    nextCollect = collectData.get(nextIndex);
                    log.info("----------------------------------------");
                    log.info(new Gson().toJson(nextCollect));
                    log.info("----------------------------------------");
                    nextIndex++;
                }
                return nextCollect;//DTO 하나씩 반환한다. Rest 호출시 데이터가 없으면 null로 반환


            }
        };
    }

    @Bean
    public ItemProcessor<BandBoardList, TB_BandBoardList> bandPostGetProcessor(){
        return bandBoardList -> {

            if (!tb_BandBoardListRepository.existsByPostKey(bandBoardList.getPost_key())) {
                if (!reportRepository.existsBySourcePk(bandBoardList.getPost_key())){
                    String areaName = "";

                    //테스트
                    if(accessToken.equals(accessTokenDev2)) {
                        if (bandBoardList.getBand_key().equals(sosabonTest)) {
                            areaName = "소사본동";
                        } else if (bandBoardList.getBand_key().equals(bucheonTest)) {
                            areaName = "부천동";
                        }
                    }

                    //운영
                    if(accessToken.equals(accessTokenProd)) {
                        if (bandBoardList.getBand_key().equals(deasan)) {
                            areaName = "대산동";
                        } else if (bandBoardList.getBand_key().equals(beoman)) {
                            areaName = "범안동";
                        } else if (bandBoardList.getBand_key().equals(bucheon)) {
                            areaName = "부천동";
                        } else if (bandBoardList.getBand_key().equals(sang)) {
                            areaName = "상동";
                        } else if (bandBoardList.getBand_key().equals(seonggok)) {
                            areaName = "성곡동";
                        } else if (bandBoardList.getBand_key().equals(sosabon)) {
                            areaName = "소사본동";
                        } else if (bandBoardList.getBand_key().equals(sinjoong)) {
                            areaName = "신중동";
                        } else if (bandBoardList.getBand_key().equals(simgok)) {
                            areaName = "심곡동";
                        } else if (bandBoardList.getBand_key().equals(ojeong)) {
                            areaName = "오정동";
                        } else if (bandBoardList.getBand_key().equals(joong)) {
                            areaName = "중동";
                        }
                    }

                    Report repoResult = reportRepository.save(bandBoardList.ofReport(areaName));
                    log.info("리포트저장 들어옴");
                    if (bandBoardList.getPhotos() != null) {
                        bandBoardList.ofReportImg(repoResult.getReportSeq()).stream().forEach(g -> reportImgRepository.save(g));
                        log.info("사진저장 들어옴");
                    }

                    /////// 커넥션 카운팅 추가
                    try {
                        ConnectSumLogVO connectSumLogVO = new ConnectSumLogVO();
                        connectSumLogVO.setConnectDate(LocalDate.now());
                        connectSumLogVO.setUserId(bandBoardList.getPost_key());
                        connectSumLogVO.setConnectType("B");
                        connectSumService.ConnectCount(connectSumLogVO);
                        System.out.println("일일 카운팅 밴드 성공");
                    } catch (Exception ee) {
                        System.out.println("일일 카운팅 밴드 오류남");
                    }
                }
            }


            //업로드 자료를 넘긴다.
            return new TB_BandBoardList(
                    bandBoardList.getContent(),
                    bandBoardList.getPost_key(),
                    bandBoardList.getComment_count(),
                    bandBoardList.getCreated_at(),
                    bandBoardList.getEmotion_count(),
                    bandBoardList.getBand_key(),
                    bandBoardList.getName(),
                    bandBoardList.getDescription(),
                    bandBoardList.getRole(),
                    bandBoardList.getProfile_image_url(),
                    bandBoardList.getUser_key(),
                    bandBoardList.getBandName(),
                    bandBoardList.getProcessStatus()
                   );

        };
    }

    @Bean
    public JpaItemWriter<TB_BandBoardList> bandPostGetWriter(){
        JpaItemWriter<TB_BandBoardList> jpaItemWriter = new JpaItemWriter<>();
        jpaItemWriter.setEntityManagerFactory(em);
        return jpaItemWriter;
    }

}
