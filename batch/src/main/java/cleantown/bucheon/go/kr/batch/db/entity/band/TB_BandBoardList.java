package cleantown.bucheon.go.kr.batch.db.entity.band;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_band_board_list")
@Getter
@Setter
@NoArgsConstructor
public class TB_BandBoardList {

    @Column(name = "content", nullable = true)
    private String content;
    @Id
    @Column(name = "post_key", nullable = true)
    private String postKey;
    @Column(name = "comment_count", nullable = true)
    private int commentCount;
    @Column(name = "created_at", nullable = true)
    private long createdAt;
    @Column(name = "emotion_count", nullable = true)
    private int emotionCount;
    @Column(name = "band_key", nullable = true)
    private String bandKey;
    @Column(name = "name", nullable = true)
    private String name;
    @Column(name = "description", nullable = true)
    private String description;
    @Column(name = "role", nullable = true)
    private String role;
    @Column(name = "profile_image_url", nullable = true)
    private String profileImageUrl;
    @Column(name = "user_key", nullable = true)
    private String userKey;
    @Column(name = "band_name", nullable = true)
    private String bandName;
    @Column(name = "process_status", nullable = true)
    private String processStatus;


    @Builder
    public TB_BandBoardList(
            String content,
            String postKey,
            int commentCount,
            long createdAt,
            int emotionCount,
            String bandKey,
            String name,
            String description,
            String role,
            String profileImageUrl,
            String userKey,
            String bandName,
            String processStatus
    ) {
        this.content = content;
        this.postKey = postKey;
        this.commentCount = commentCount;
        this.createdAt = createdAt;
        this.emotionCount = emotionCount;
        this.bandKey = bandKey;
        this.name = name;
        this.description = description;
        this.role = role;
        this.profileImageUrl = profileImageUrl;
        this.userKey = userKey;
        this.bandName = bandName;
        this.processStatus = processStatus;
    }
}
