package cleantown.bucheon.go.kr.batch.biz.band.model;

import cleantown.bucheon.go.kr.batch.db.entity.etc.ConnectSumLog;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConnectSumLogVO {

    LocalDate connectDate;
    String connectType;
    String userId;


    public ConnectSumLog ofConnectSumLog(){
        ConnectSumLog c = new ConnectSumLog();

        c.setConnectDate(this.connectDate);
        c.setConnectType(this.connectType);
        c.setUserId(this.userId);
        return c;
    }

}
