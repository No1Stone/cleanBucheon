package cleantown.bucheon.go.kr.batch.db.repository.metabus;

import cleantown.bucheon.go.kr.batch.db.entity.metabus.MetaPush2;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MetaPush2Repository extends JpaRepository<MetaPush2, String> {

}
