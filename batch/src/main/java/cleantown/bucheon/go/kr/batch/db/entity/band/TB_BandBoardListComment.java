package cleantown.bucheon.go.kr.batch.db.entity.band;

import cleantown.bucheon.go.kr.batch.db.entity.band.Id.TB_BandBoardListCommentId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_band_board_list_comment")
@Getter
@Setter
@NoArgsConstructor
@IdClass(TB_BandBoardListCommentId.class)
public class TB_BandBoardListComment {
    @Id
    @Column(name = "post_key", nullable = true)
    private String postKey;
    @Column(name = "comment_key", nullable = true)
    private String commentKey;
    @Column(name = "body", nullable = true)
    private String body;
    @Id
    @Column(name = "created_at", nullable = true)
    private long createdAt;
    @Column(name = "name", nullable = true)
    private String name;
    @Column(name = "description", nullable = true)
    private String description;
    @Column(name = "profile_image_url", nullable = true)
    private String profileImageUrl;
    @Column(name = "user_key", nullable = true)
    private String userKey;
    @Column(name = "url", nullable = true)
    private String url;
    @Column(name = "width", nullable = true)
    private int width;
    @Column(name = "height", nullable = true)
    private int height;

    @Builder
    public TB_BandBoardListComment(
            String postKey,
            String commentKey,
            String body,
            long createdAt,
            String name,
            String description,
            String profileImageUrl,
            String userKey,
            String url,
            int width,
            int height
    ) {
        this.postKey = postKey;
        this.commentKey = commentKey;
        this.body = body;
        this.createdAt = createdAt;
        this.name = name;
        this.description = description;
        this.profileImageUrl = profileImageUrl;
        this.userKey = userKey;
        this.url = url;
        this.width = width;
        this.height = height;
    }

}
