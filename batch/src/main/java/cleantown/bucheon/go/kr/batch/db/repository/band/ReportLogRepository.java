package cleantown.bucheon.go.kr.batch.db.repository.band;

import cleantown.bucheon.go.kr.batch.db.entity.band.ReportLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ReportLogRepository extends JpaRepository<ReportLog, Long> {

    Optional<ReportLog> findTop1ByReportSeqOrderByLogSeqDesc(Long seq);
    int countByReportSeq(Long seq);
    List<ReportLog> findByReportSeqOrderByLogSeqDesc(Long seq);
    Boolean existsByReportSeq(Long seq);
}
