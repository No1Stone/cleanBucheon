package cleantown.bucheon.go.kr.batch.schedulers;

import cleantown.bucheon.go.kr.batch.biz.band.jobs.BandCommentPostGetConfig;
import cleantown.bucheon.go.kr.batch.biz.band.jobs.BandPostGetConfig;
import cleantown.bucheon.go.kr.batch.biz.fcm.jobs.FcmConfig;
import cleantown.bucheon.go.kr.batch.biz.tutorial.TutorialConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class TutorialScheduler {

    @Autowired
    private FcmConfig fcmConfig;  // fcmJob

    @Autowired
    private TutorialConfig tutorialConfig;  // tutorialJob

    @Autowired
    private BandPostGetConfig bandPostGetConfig;  // BandPostJob

    @Autowired
    private BandCommentPostGetConfig bandCommentPostGetConfig;  // BandCommentPostJob

    @Autowired
    private JobLauncher jobLauncher;

    // 10분마다 실행
//    @Scheduled(cron = "0 */10 * * * *")
//    public void executeJob () {
//        try {
//            jobLauncher.run(
//                    fcmConfig.fcmJob(),
//                    new JobParametersBuilder()
//                            .addString("datetime", LocalDateTime.now().toString())
//                    .toJobParameters()  // job parameter 설정
//            );
//        } catch (JobExecutionException ex) {
//            System.out.println(ex.getMessage());
//            ex.printStackTrace();
//        }
//    }

    //밴드 자료 가져오기 스케줄
    @Scheduled(cron = "0 35 7-17 * * ?")
    public void executeJob3 () {
        try {
            jobLauncher.run(
                    bandPostGetConfig.bandPostGetJob(),
                    new JobParametersBuilder()
                            .addString("datetime", LocalDateTime.now().toString())
                            .toJobParameters()  // job parameter 설정
            );
        } catch (JobExecutionException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    //밴드 자료 가져오기 스케줄
    @Scheduled(cron = "0 15 7-17 * * ?")
    public void executeJob4 () {
        try {
            jobLauncher.run(
                    bandCommentPostGetConfig.bandCommentPostGetJob(),
                    new JobParametersBuilder()
                            .addString("datetime", LocalDateTime.now().toString())
                            .toJobParameters()  // job parameter 설정
            );
        } catch (JobExecutionException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    // 7초마다 실행
//    @Scheduled(fixedDelay = 7 * 1000L)
//    public void executeJob2 () {
//        try {
//            jobLauncher.run(
//                    tutorialConfig.tutorialJob(),
//                    new JobParametersBuilder()
//                            .addString("datetime", LocalDateTime.now().toString())
//                            .toJobParameters()  // job parameter 설정
//            );
//        } catch (JobExecutionException ex) {
//            System.out.println(ex.getMessage());
//            ex.printStackTrace();
//        }
//    }

}