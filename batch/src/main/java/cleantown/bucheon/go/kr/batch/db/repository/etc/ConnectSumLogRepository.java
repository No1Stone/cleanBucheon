package cleantown.bucheon.go.kr.batch.db.repository.etc;

import cleantown.bucheon.go.kr.batch.db.entity.etc.ConnectSumLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

public interface ConnectSumLogRepository extends JpaRepository<ConnectSumLog, String> {


    @Transactional(readOnly = true)
    int countByConnectDateAndUserId(LocalDate date, String userId);

    @Transactional(readOnly = true)
    Optional<ConnectSumLog> findByConnectDateAndUserId(LocalDate date, String userId);
}
