package cleantown.bucheon.go.kr.batch.db.repository.band;

import cleantown.bucheon.go.kr.batch.db.entity.band.ReportImg;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReportImgRepository extends JpaRepository<ReportImg, Long> {

    boolean existsByReportSeq(Long seq);

    List<ReportImg> findByReportSeq(Long reSeq);




}
