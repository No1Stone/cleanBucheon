package cleantown.bucheon.go.kr.batch.db.entity.etc.Id;

import lombok.Data;

import java.io.Serializable;

@Data
public class AdminId implements Serializable {
    private Long adminSeq;
    private Long permissionSeq;
}
