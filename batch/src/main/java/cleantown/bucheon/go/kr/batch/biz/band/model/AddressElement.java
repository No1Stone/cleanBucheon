package cleantown.bucheon.go.kr.batch.biz.band.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressElement {
    private String[] types;
    private String longName;
    private String shortName;
    private String code;
}
