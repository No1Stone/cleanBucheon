package cleantown.bucheon.go.kr.batch.db.entity.band.Id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TB_BandBoardListCommentId implements Serializable {
    private String postKey;
    private long createdAt;

}
