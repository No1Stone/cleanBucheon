package cleantown.bucheon.go.kr.batch.db.repository.etc;

import cleantown.bucheon.go.kr.batch.db.entity.etc.Admin;
import cleantown.bucheon.go.kr.batch.db.entity.etc.Id.AdminId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminRepository extends JpaRepository<Admin, AdminId> {

    Optional<Admin> findByAdminSeq(Long seq);

}
