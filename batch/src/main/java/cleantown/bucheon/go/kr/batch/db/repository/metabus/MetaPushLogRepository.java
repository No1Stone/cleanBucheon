package cleantown.bucheon.go.kr.batch.db.repository.metabus;

import cleantown.bucheon.go.kr.batch.db.entity.metabus.MetaPushLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface MetaPushLogRepository extends JpaRepository<MetaPushLog, String> {

    @Transactional
    int countBySendSeqAndUserId(Long seq, String id);

    @Transactional
    int countBySendSeq(Long seq);
}
