package cleantown.bucheon.go.kr.batch.db.repository.band;

import cleantown.bucheon.go.kr.batch.db.entity.band.Id.TB_BandBoardListPotoId;
import cleantown.bucheon.go.kr.batch.db.entity.band.TB_BandBoardListPoto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TB_BandBoardListPotoRepository extends JpaRepository<TB_BandBoardListPoto, TB_BandBoardListPotoId> {



}
