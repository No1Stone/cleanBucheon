package cleantown.bucheon.go.kr.batch.biz.band.service;

import cleantown.bucheon.go.kr.batch.biz.band.model.ConnectSumLogVO;
import cleantown.bucheon.go.kr.batch.biz.band.model.ConnectSumVO;
import cleantown.bucheon.go.kr.batch.db.repository.etc.ConnectSumLogRepository;
import cleantown.bucheon.go.kr.batch.db.repository.etc.ConnectSumRepository;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ConnectSumService {

    private final Logger logger = LoggerFactory.getLogger(ConnectSumService.class);
    private final ConnectSumRepository connectSumRepository;
    private final ConnectSumLogRepository connectSumLogRepository;

    //// 접속시 추가  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Object ConnectCount(ConnectSumLogVO dto){

        Map<String, Object> result = new HashMap<>();

        int cnt = connectSumLogRepository.countByConnectDateAndUserId(dto.getConnectDate(), dto.getUserId());

        if(cnt == 0){
            try {
                //로그에 집어 넣는다
                connectSumLogRepository.save(dto.ofConnectSumLog());

                //상황에 맞게 업데이트 시킨다
                int cnt2 = connectSumRepository.countByConnectDate(dto.getConnectDate());
                if(cnt2 == 0){
                    ConnectSumVO connectSumVO = new ConnectSumVO();
                    connectSumVO.setConnectDate(dto.getConnectDate());
                    connectSumVO.setBand(0);
                    connectSumVO.setChatbot(0);
                    connectSumVO.setMetabus(0);
                    connectSumVO.setSelf(0);
                    connectSumVO.setCleanApp(0);

                    try {
                        connectSumRepository.save(connectSumVO.ofConnectSum());
                        result.put("result", "success");
                        result.put("reason", "일일 집계 확인 테이블생성");
                        logger.info("-- 일일 집계 확인 테이블생성 : {}", new Gson().toJson(result));
                    } catch (Exception e) {
                        result.put("result", "fail");
                        result.put("reason", "connectSumVO input Not");
                        logger.info("-- 일일 집계 확인 테이블생성 오류 : {}", new Gson().toJson(result));
                        return result;
                    }

                }

                if(dto.getConnectType().equals("A")){
                    try {
                        connectSumRepository.UpdateCountA(dto.getConnectDate());
                        result.put("result", "success");
                        result.put("reason", "connectSumRepository.UpdateCountA");
                        logger.info("-- 일일 집계 업데이트 : {}", new Gson().toJson(result));
                    } catch (Exception e) {
                        result.put("result", "fail");
                        result.put("reason", "connectSumRepository.UpdateCountA");
                        logger.info("-- 일일 집계 업데이트 : {}", new Gson().toJson(result));
                        return result;
                    }
                }
                else if(dto.getConnectType().equals("B")){
                    try {
                        connectSumRepository.UpdateCountB(dto.getConnectDate());
                        result.put("result", "success");
                        result.put("reason", "connectSumRepository.UpdateCountB");
                        logger.info("-- 일일 집계 업데이트 : {}", new Gson().toJson(result));
                    } catch (Exception e) {
                        result.put("result", "fail");
                        result.put("reason", "connectSumRepository.UpdateCountB");
                        logger.info("-- 일일 집계 업데이트 : {}", new Gson().toJson(result));
                        return result;
                    }
                }
                else if(dto.getConnectType().equals("C")){
                    try {
                        connectSumRepository.UpdateCountC(dto.getConnectDate());
                        result.put("result", "success");
                        result.put("reason", "connectSumRepository.UpdateCountC");
                        logger.info("-- 일일 집계 업데이트 : {}", new Gson().toJson(result));
                    } catch (Exception e) {
                        result.put("result", "fail");
                        result.put("reason", "connectSumRepository.UpdateCountC");
                        logger.info("-- 일일 집계 업데이트 : {}", new Gson().toJson(result));
                        return result;
                    }
                }
                else if(dto.getConnectType().equals("M")){
                    try {
                        connectSumRepository.UpdateCountM(dto.getConnectDate());
                        result.put("result", "success");
                        result.put("reason", "connectSumRepository.UpdateCountM");
                        logger.info("-- 일일 집계 업데이트 : {}", new Gson().toJson(result));
                    } catch (Exception e) {
                        result.put("result", "fail");
                        result.put("reason", "connectSumRepository.UpdateCountM");
                        logger.info("-- 일일 집계 업데이트 : {}", new Gson().toJson(result));
                        return result;
                    }
                }
                else if(dto.getConnectType().equals("V")){
                    try {
                        connectSumRepository.UpdateCountV(dto.getConnectDate());
                        result.put("result", "success");
                        result.put("reason", "connectSumRepository.UpdateCountV");
                        logger.info("-- 일일 집계 업데이트 : {}", new Gson().toJson(result));
                    } catch (Exception e) {
                        result.put("result", "fail");
                        result.put("reason", "connectSumRepository.UpdateCountV");
                        logger.info("-- 일일 집계 업데이트 : {}", new Gson().toJson(result));
                        return result;
                    }
                }



            } catch (Exception e){
                result.put("result", "fail");
                result.put("reason", " connectSumLog input Not");
                logger.info("-- 일일 집계 업데이트 : {}", new Gson().toJson(result));
                return result;
            }
        }
        else {
            result.put("result", "fail");
            logger.info("-- 일일 집계 업데이트 : {}", new Gson().toJson(result));
            return result.put("reason", "already");
        }

        return result;
    }
}
