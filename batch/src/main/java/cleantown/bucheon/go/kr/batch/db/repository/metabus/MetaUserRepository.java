package cleantown.bucheon.go.kr.batch.db.repository.metabus;

import cleantown.bucheon.go.kr.batch.db.entity.metabus.MetaUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface MetaUserRepository extends JpaRepository<MetaUser, String> {

    @Transactional(readOnly = true)
    int countByUserId(String userId);
    @Transactional(readOnly = true)
    int countByProviderAndSnsId(String provider, String snsId);
    @Transactional(readOnly = true)
    int countBySnsId(String snsId);
    @Transactional(readOnly = true)
    int countByNickname(String nickName);
    @Transactional(readOnly = true)
    int countByEmail(String email);
    @Transactional(readOnly = true)
    int countByCleantownUid(String mid);
    @Transactional(readOnly = true)
    int countByUserIdAndCleantownUid(String userId, String mid);

    @Transactional(readOnly = true)
    Optional<MetaUser> findByUserId(String userId);
    @Transactional(readOnly = true)
    Optional<MetaUser> findBySnsId(String snsId);
    @Transactional(readOnly = true)
    Optional<MetaUser> findByNickname(String nickname);
    @Transactional(readOnly = true)
    Optional<MetaUser> findByEmail(String email);
    @Transactional(readOnly = true)
    Optional<MetaUser> findByProviderAndSnsId(String provider, String snsId);
    @Transactional(readOnly = true)
    Optional<MetaUser> findByDeviceToken(String device);

    @Transactional(readOnly = true)
    List<MetaUser> findAllByDelYn(String del);
    @Transactional(readOnly = true)
    List<MetaUser> findAllByDeviceTokenNotNull();
    
    @Transactional(readOnly = true)
    @Query(
            value = "select " +
                    "             userId as user_id, " +
                    "             avatarType as avatar_type, " +
                    "             cleanPoint as clean_point, " +
                    "             cleantownUid as cleantown_uid, " +
                    "             createdAt as created_at, " +
                    "             delYn as del_yn, " +
                    "             deviceToken as device_token, " +
                    "             email, " +
                    "             lastConnectAt as last_connect_at, " +
                    "             nickname, " +
                    "             provider, " +
                    "             snsId as sns_id, " +
                    "             updatedAt as updated_at  " +
                    "        from " +
                    "            tb_meta_user  " +
                    "        where " +
                    "             deviceToken is not null;",
            nativeQuery = true
    )
    List<MetaUser> selectDeviceTokenNotNull();

}
