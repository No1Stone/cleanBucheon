package cleantown.bucheon.go.kr.batch.biz.fcm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RequestDTO {

    String targetToken;
    String title;
    String body;
    Long sendSeq;


}
