package cleantown.bucheon.go.kr.batch.db.entity.etc;

import cleantown.bucheon.go.kr.batch.db.entity.etc.Id.AdminId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_admin")
@Getter
@Setter
@NoArgsConstructor
@IdClass(AdminId.class)
public class Admin {

    @Id
    @Column(name = "admin_seq", nullable = true)
    private Long adminSeq;
    @Column(name = "admin_id", nullable = true)
    private String adminId;
    @Column(name = "admin_name", nullable = true)
    private String adminName;
    @Column(name = "admin_password", nullable = true)
    private String adminPassword;
    @Column(name = "admin_phone", nullable = true)
    private String adminPhone;
    @Column(name = "admin_email", nullable = true)
    private String adminEmail;
    @Column(name = "administrative_division", nullable = true)
    private String administrativeDivision;
    @Id
    @Column(name = "permission_seq", nullable = true)
    private Long permissionSeq;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "del_yn", nullable = true)
    private String delYn;
    @Column(name = "reg_seq", nullable = true)
    private Long regSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_seq", nullable = true)
    private Long modSeq;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "super_yn")
    private String superYn;


    @Builder
    Admin(
            Long adminSeq,
            String adminId,
            String adminName,
            String adminPassword,
            String adminPhone,
            String adminEmail,
            String administrativeDivision,
            Long permissionSeq,
            String useYn,
            Long regSeq,
            LocalDateTime regDt,
            Long modSeq,
            LocalDateTime modDt,
            String superYn
            ) {
        this.adminSeq = adminSeq;
        this.adminId = adminId;
        this.adminName = adminName;
        this.adminPassword = adminPassword;
        this.adminPhone = adminPhone;
        this.adminEmail = adminEmail;
        this.administrativeDivision = administrativeDivision;
        this.permissionSeq = permissionSeq;
        this.useYn = useYn;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.modSeq = modSeq;
        this.modDt = modDt;
        this.superYn = superYn;
    }

}
