package cleantown.bucheon.go.kr.batch.biz.band.model;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class BandBoardListComment {
    /**
     * 본물을 조회했을때 나오는 댓글 VO
     */


    private String post_key;
    private String body;
    private long created_at;
    private String name;
    private String description;
    private String profile_image_url;
    private String user_key;
    private Author author;

    public BandBoardListComment() {
    }

    public BandBoardListComment(String post_key, String body, long created_at, String name, String description, String profile_image_url, String user_key, Author author) {
        this.post_key = post_key;
        this.body = body;
        this.created_at = created_at;
        this.name = name;
        this.description = description;
        this.profile_image_url = profile_image_url;
        this.user_key = user_key;
        this.author = author;
    }

    public BandBoardListComment(Author author) {
        this.name = author.getName();
        this.description = author.getDescription();
        this.profile_image_url = author.getProfile_image_url();
        this.user_key = author.getUser_key();
    }
}
