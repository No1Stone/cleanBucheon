package cleantown.bucheon.go.kr.batch.biz.fcm.jobs;

import cleantown.bucheon.go.kr.batch.biz.fcm.model.RequestDTO;
import cleantown.bucheon.go.kr.batch.biz.fcm.service.FCMService2;
import cleantown.bucheon.go.kr.batch.db.entity.metabus.MetaPush;
import cleantown.bucheon.go.kr.batch.db.entity.metabus.MetaPush2;
import cleantown.bucheon.go.kr.batch.db.repository.metabus.MetaPushLogRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.batch.item.database.builder.JpaPagingItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManagerFactory;
import java.time.LocalDateTime;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class FcmConfig {

    private final JobBuilderFactory jobBuilderFactory; // Job 빌더 생성용
    private final StepBuilderFactory stepBuilderFactory; // Step 빌더 생성용
    private final EntityManagerFactory em; // JPA 빌더

    @Autowired
    MetaPushLogRepository metaPushLogRepository;

    @Autowired
    FCMService2 fcmService2;

    private int chunkSize = 10;

    @Bean
    public Job fcmJob(){
        return jobBuilderFactory.get("fcmJob")
                .start(fcmStep())
                .build();
    }

    @Bean
    public Step fcmStep(){
        return stepBuilderFactory.get("fcmStep")
                .<MetaPush, MetaPush2>chunk(chunkSize)
                .reader(fcmReader())
                .processor(fcmProcessor())
                .writer(fcmWriter())
                .build();
    }

    @Bean
    public JpaPagingItemReader<MetaPush> fcmReader(){

        LocalDateTime sdateTime = LocalDateTime.now().minusMinutes(6);
        LocalDateTime edateTime = LocalDateTime.now().plusMinutes(6);
        //log.info("SELECT m FROM MetaPush m where m.sendDt BETWEEN " + sdateTime + " and " + edateTime + " AND m.resultType = 'W' order by sendSeq asc");

        return new JpaPagingItemReaderBuilder<MetaPush>()
                .name("fcmReader")
                .entityManagerFactory(em)
                .pageSize(chunkSize)
                .queryString("SELECT m FROM MetaPush m where m.sendDt BETWEEN '"+ sdateTime  +"' and '"+ edateTime  +"' AND m.resultType = 'W' order by sendSeq asc")
                .build();
    }

    @Bean
    public ItemProcessor<MetaPush, MetaPush2> fcmProcessor(){
        return metaPush -> {

            RequestDTO data = new RequestDTO();
            data.setTitle(metaPush.getTitle());
            data.setBody(metaPush.getBody());
            data.setSendSeq(metaPush.getSendSeq());

            //인원 리스트를 구한다
            fcmService2.multipleSendToToken(data, LocalDateTime.now());

            //푸쉬 발송 로그 합계 숫자를 구한다
            int cnt = metaPushLogRepository.countBySendSeq(metaPush.getSendSeq());

            //업로드 자료를 넘긴다.
            return new MetaPush2(
                    metaPush.getSendSeq(),
                    metaPush.getSendType(),
                    "C",
                    metaPush.getTitle(),
                    metaPush.getBody(),
                    cnt,
                    metaPush.getRegSeq(),
                    metaPush.getRegDt(),
                    metaPush.getSendDt()
            );

        };
    }

    @Bean
    public JpaItemWriter<MetaPush2> fcmWriter(){
        JpaItemWriter<MetaPush2> jpaItemWriter = new JpaItemWriter<>();
        jpaItemWriter.setEntityManagerFactory(em);
        return jpaItemWriter;
    }

}
