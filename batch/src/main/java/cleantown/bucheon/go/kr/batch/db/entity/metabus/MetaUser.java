package cleantown.bucheon.go.kr.batch.db.entity.metabus;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_meta_user")
@Getter
@Setter
@NoArgsConstructor
public class MetaUser {

    @Id
    @Column(name = "userId", nullable = true)
    private String userId;
    @Column(name = "nickname", nullable = true)
    private String nickname;
    @Column(name = "email", nullable = true)
    private String email;
    @Column(name = "provider", nullable = true)
    private String provider;
    @Column(name = "snsId", nullable = true)
    private String snsId;
    @Column(name = "avatarType", nullable = true)
    private int avatarType;
    @Column(name = "cleanPoint", nullable = true)
    private int cleanPoint;
    @Column(name = "deviceToken", nullable = true)
    private String deviceToken;
    @Column(name = "cleantownUid", nullable = true)
    private String cleantownUid;
    @Column(name = "delYn", nullable = true)
    private String delYn;
    @Column(name = "createdAt", nullable = true)
    private LocalDateTime createdAt;
    @Column(name = "updatedAt", nullable = true)
    private LocalDateTime updatedAt;
    @Column(name = "lastConnectAt", nullable = true)
    private LocalDateTime lastConnectAt;


    @Builder
    MetaUser(
            String userId,
            String nickname,
            String email,
            String provider,
            String snsId,
            int avatarType,
            int cleanPoint,
            String deviceToken,
            String cleantownUid,
            String delYn,
            LocalDateTime createdAt,
            LocalDateTime updatedAt,
            LocalDateTime lastConnectAt
            ) {
        this.userId = userId;
        this.nickname = nickname;
        this.email = email;
        this.provider = provider;
        this.snsId = snsId;
        this.avatarType = avatarType;
        this.cleanPoint = cleanPoint;
        this.deviceToken = deviceToken;
        this.cleantownUid = cleantownUid;
        this.delYn = delYn;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.lastConnectAt = lastConnectAt;
    }

}
