package cleantown.bucheon.go.kr.batch.db.repository.band;

import cleantown.bucheon.go.kr.batch.db.entity.band.DongDamdang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface DongDamdangRepository extends JpaRepository<DongDamdang, Long> {

    Optional<DongDamdang> findByDong(String dong);
    int countByDong(String dong);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_dong_damdang set damdang = :damdang where dong =:dong "
            , nativeQuery = true
    )
    int updateDamdang(@Param("damdang") Long damdang, @Param("dong") String dong);

}
