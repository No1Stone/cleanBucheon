package cleantown.bucheon.go.kr.batch.db.entity.etc.Id;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
public class ConnectSumLogId implements Serializable {
    private LocalDate connectDate;
    private String userId;
}
