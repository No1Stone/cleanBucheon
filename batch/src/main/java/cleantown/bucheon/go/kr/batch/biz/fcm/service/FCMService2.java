package cleantown.bucheon.go.kr.batch.biz.fcm.service;

import cleantown.bucheon.go.kr.batch.biz.fcm.model.MetaPushLogVO;
import cleantown.bucheon.go.kr.batch.biz.fcm.model.RequestDTO;
import cleantown.bucheon.go.kr.batch.db.entity.metabus.MetaPushLog;
import cleantown.bucheon.go.kr.batch.db.entity.metabus.MetaUser;
import cleantown.bucheon.go.kr.batch.db.repository.metabus.MetaPushLogRepository;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.*;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class FCMService2 {

    private final Logger logger = LoggerFactory.getLogger(FCMService2.class);
    private final MetaUserService metaUserService;
    private final MetaPushLogRepository metaPushLogRepository;
    private FirebaseMessaging instance;

    @Value("${project.properties.firebase-create-scoped}")
    String fireBaseCreateScoped;

    @Value("${project.properties.firebase-topic}")
    String topic;

    @PostConstruct
    public void firebaseSetting() throws IOException {

        ClassPathResource cpr = new ClassPathResource("firebase/firebase_service_key.json");
        byte[] bdata = FileCopyUtils.copyToByteArray(cpr.getInputStream());
        String jsonTxt = new String(bdata, StandardCharsets.UTF_8);
        logger.info("firebase_service_key : {}", jsonTxt);

        GoogleCredentials googleCredentials = GoogleCredentials.fromStream(new ClassPathResource("firebase/firebase_service_key.json").getInputStream())
                .createScoped((Arrays.asList(fireBaseCreateScoped)));
        FirebaseOptions secondaryAppConfig = FirebaseOptions.builder()
                .setCredentials(googleCredentials)
                .build();
        FirebaseApp app = FirebaseApp.initializeApp(secondaryAppConfig);
        this.instance = FirebaseMessaging.getInstance(app);
    }


    public Map<String , Object> multipleSendToToken(RequestDTO data, LocalDateTime sendDt)
            throws FirebaseMessagingException {
        Map<String, Object> result = new HashMap<>();
        List<String> tokenList2 = metaUserService.MetaUserSelectDeviceTokenHasListService().stream().map(e -> e.getDeviceToken()).collect(Collectors.toList());
        Map<String, String> userList = metaUserService.MetaUserSelectDeviceTokenHasListService().stream().collect(Collectors.toMap(MetaUser::getDeviceToken, MetaUser::getUserId));
        logger.info(">> tokenList2 : {}", new Gson().toJson(tokenList2));

        MulticastMessage message = MulticastMessage.builder()
                .setNotification(Notification.builder()
                        .setTitle(data.getTitle())
                        .setBody(data.getBody())
                        .build())
                .addAllTokens(tokenList2)
                .build();


        BatchResponse response = FirebaseMessaging.getInstance()
                .sendMulticast(message);

        List<SendResponse> responses = response.getResponses();
        List<String> failedTokens = new ArrayList<>();
        List<MetaPushLogVO> metaPushLog = new ArrayList<>();
        for (int i = 0; i < responses.size(); i++) {
            MetaPushLogVO m = new MetaPushLogVO();

            if (!responses.get(i).isSuccessful()) {
                failedTokens.add(tokenList2.get(i));

                m.setDeviceToken(tokenList2.get(i));
                m.setResultCode("F");
                m.setSendDt(sendDt);
                metaPushLog.add(m);
            } else {
                m.setDeviceToken(tokenList2.get(i));
                m.setResultCode("S");
                m.setSendDt(sendDt);
                metaPushLog.add(m);
            }

            MetaPushLog mp = new MetaPushLog();
            mp.setSendDt(m.getSendDt());
            mp.setUserId(userList.get(m.getDeviceToken()));
            mp.setResultCode(m.getResultCode());
            mp.setSendSeq(data.getSendSeq());
            mp.setDeviceToken(m.getDeviceToken());
            metaPushLogRepository.save(mp);
            result.put("metaPushLogVOList", metaPushLog);
        }

        if (response.getFailureCount() > 0) {
            result.put("result", "fail");

            System.out.println("List of tokens that caused failures: "
                    + failedTokens);
        } else {
            result.put("result", "suceess");
        }

        return result;
    }


    public String sendMessage(Message message) throws FirebaseMessagingException {
        return this.instance.send(message);
    }


}
