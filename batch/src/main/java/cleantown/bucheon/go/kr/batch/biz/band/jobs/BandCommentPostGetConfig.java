package cleantown.bucheon.go.kr.batch.biz.band.jobs;

import cleantown.bucheon.go.kr.batch.biz.band.model.*;
import cleantown.bucheon.go.kr.batch.biz.band.service.ConnectSumService;
import cleantown.bucheon.go.kr.batch.biz.common.util.RestCall;
import cleantown.bucheon.go.kr.batch.db.entity.band.*;
import cleantown.bucheon.go.kr.batch.db.entity.etc.Admin;
import cleantown.bucheon.go.kr.batch.db.repository.band.*;
import cleantown.bucheon.go.kr.batch.db.repository.etc.AdminRepository;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.*;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManagerFactory;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class BandCommentPostGetConfig {

    private final JobBuilderFactory jobBuilderFactory; // Job 빌더 생성용
    private final StepBuilderFactory stepBuilderFactory; // Step 빌더 생성용
    private final EntityManagerFactory em; // JPA 빌더

    private List<BandBoardComment> collectData = new ArrayList<>(); //Rest로 가져온 데이터를 리스트에 넣는다.
    private boolean checkRestCall = false; //RestAPI 호출여부 판단
    private int nextIndex = 0;//리스트의 데이터를 하나씩 인덱스를 통해 가져온다.

    @Autowired
    TB_BandBoardListRepository tb_BandBoardListRepository;

    @Autowired
    ReportRepository reportRepository;

    @Autowired
    ReportImgRepository reportImgRepository;

    @Autowired
    ConnectSumService connectSumService;

    @Autowired
    TB_BandBoardListRepository tB_BandBoardListRepository;

    @Autowired
    TB_BandBoardListCommentRepository tb_BandBoardListCommentRepository;

    @Autowired
    DongBandDamdangRepository dongBandDamdangRepository;

    @Autowired
    DongDamdangRepository dongDamdangRepository;

    @Autowired
    ReportLogRepository reportLogRepository;

    @Autowired
    AdminRepository adminRepository;

    @Value("${naver.cloud.schema}")
    private String naveropenapiSchema;
    @Value("${naver.cloud.url}")
    private String naveropenapiUrl;
    @Value("${naver.band.schema}")
    private String bandSchema;
    @Value("${naver.band.url}")
    private String bandUrl;

    @Value("${naver.band.accessToken}")
    private String accessToken;

    @Value("${naver.band.accessTokenDev2}")
    private String accessTokenDev2;
    @Value("${naver.band.testBandkey.bucheonTest}")
    private String bucheonTest;
    @Value("${naver.band.testBandkey.sosabonTest}")
    private String sosabonTest;

    @Value("${naver.band.accessTokenProd}")
    private String accessTokenProd;
    @Value("${naver.band.prodBandkey.deasan}")
    private String deasan;
    @Value("${naver.band.prodBandkey.beoman}")
    private String beoman;
    @Value("${naver.band.prodBandkey.bucheon}")
    private String bucheon;
    @Value("${naver.band.prodBandkey.sang}")
    private String sang;
    @Value("${naver.band.prodBandkey.seonggok}")
    private String seonggok;
    @Value("${naver.band.prodBandkey.sosabon}")
    private String sosabon;
    @Value("${naver.band.prodBandkey.sinjoong}")
    private String sinjoong;
    @Value("${naver.band.prodBandkey.simgok}")
    private String simgok;
    @Value("${naver.band.prodBandkey.ojeong}")
    private String ojeong;
    @Value("${naver.band.prodBandkey.joong}")
    private String joong;

    private int chunkSize = 10;


    @Bean
    public Job bandCommentPostGetJob(){
        return jobBuilderFactory.get("bandCommentPostGetJob")
                .start(bandCommentPostGetStep())
                .build();
    }

    @Bean
    public Step bandCommentPostGetStep(){
        return stepBuilderFactory.get("bandCommentPostGetStep")
                .<BandBoardComment, TB_BandBoardListComment>chunk(chunkSize)
                .reader(bandCommentPostGetReader())
                .processor(bandCommentPostGetProcessor())
                .writer(bandCommentPostGetWriter())
                .build();
    }

    @Bean
    public ItemReader<BandBoardComment> bandCommentPostGetReader() {

        return new ItemReader<BandBoardComment>() {
            @Override
            public BandBoardComment read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

                List<Report> aa = reportRepository.findAllByReportReceptionAndReportStatusOrReportStatusAndDelYnAndSourcePkIsNotNullOrderByReportSeqDesc("C", "D", "P", "N" );

                String accessKey = accessToken;
                List<BandBoardComment> bbc = new ArrayList<>();

                aa.stream().forEach(e -> {
                    try {
                        JsonToModel.builder(RestCall.get(BandURL.builder().schema(bandSchema + "://")
                                        .baseURL(bandUrl)
                                        .api("/v2/band/post/comments")
                                        .param("access_token", accessKey)
                                        .param("band_key", e.getBandKey())
                                        .param("post_key", e.getSourcePk())
                                        .param("sort", "-created_at")
                                        .build()))
                                .ofObject("result_data")
                                .ofArrayModel("items", BandBoardComment.class)
                                .stream().filter(h -> Objects.nonNull(h))
                                .forEach(f -> {

                                    //없으면 저장
                                    if(tb_BandBoardListCommentRepository.countByCommentKeyAndPostKey(f.getComment_key(), f.getPost_key()) == 0) {
                                        bbc.add(f);
                                    }

                                });
                    } catch (Exception exception) {

                    }
                });

                collectData = bbc;
                log.info("bbl - {}", new Gson().toJson(bbc));
                log.info("bbl.size - {}", collectData.size());
                //    checkRestCall = true;//다음 read() 부터는 재호출 방지하기 위해 true로 변경

                BandBoardComment nextCollect = null; //ItemReader는 반복문으로 동작한다. 하나씩 Writer로 전달해야 한다.

                if (nextIndex < collectData.size()) {//전체 리스트에서 하나씩 추출해서, 하나씩 Writer로 전달
                    nextCollect = collectData.get(nextIndex);
                    log.info("----------------------------------------");
                    log.info(new Gson().toJson(nextCollect));
                    log.info("----------------------------------------");
                    nextIndex++;
                }
                return nextCollect;//DTO 하나씩 반환한다. Rest 호출시 데이터가 없으면 null로 반환


            }
        };
    }

    @Bean
    public ItemProcessor<BandBoardComment, TB_BandBoardListComment> bandCommentPostGetProcessor(){
        return bandBoardComment -> {

            log.info("--------------- 댓글 처리 시작 ------------------");

            //report_status 변경 처리

            Report report = reportRepository.findBySourcePk(bandBoardComment.getPost_key());
            DongBandDamdang dongBandDamdang = dongBandDamdangRepository.findByDong(report.getAreaName()).orElseThrow(IllegalArgumentException::new);

            log.info("---------------- f.getPost_key() : {}", bandBoardComment.getPost_key());
            log.info("---------------- f.getContent() : {}", bandBoardComment.getContent());
            log.info("---------------- f.getAuthor().toString() : {}", new Gson().toJson(bandBoardComment.getAuthor()));
            log.info("---------------- dongBandDamdang.getDamdang() : {} : {}", dongBandDamdang.getDong(), dongBandDamdang.getDamdang());

            String[] damdangArr = dongBandDamdang.getDamdang().split(",");

            for(String damdangName : damdangArr){
                if(bandBoardComment.getContent().contains("#완료") && new Gson().toJson(bandBoardComment.getAuthor()).contains(damdangName.trim())) {
                    bandBoardComment.setProcessStatus("C");
                    log.info("처리완료 : {}", bandBoardComment.getPost_key());
                    break;
                } else if(bandBoardComment.getContent().contains("#처리불가") && new Gson().toJson(bandBoardComment.getAuthor()).contains(damdangName.trim())){
                    bandBoardComment.setProcessStatus("R");
                    log.info("처리불가 : {}", bandBoardComment.getPost_key());
                    break;
                } else {
                    bandBoardComment.setProcessStatus("P");
                    log.info("처리중 : {}", bandBoardComment.getPost_key());
                }

                log.info("----------------f.getProcessStatus : {}", bandBoardComment.getProcessStatus());
            }

            try {
                if (bandBoardComment.getProcessStatus().equals("C")) {
                    report.setReportStatus("C");
                    tb_BandBoardListRepository.UpdateProcessStatus("C", bandBoardComment.getPost_key());
                    reportRepository.UpdateReportStatusWhereSourcePk("C", bandBoardComment.getPost_key());
                    reportRepository.UpdateComDtWhereSourcePk(LocalDateTime.now(), bandBoardComment.getPost_key());

                    //담당자를 구한다
                    DongDamdang dongDamdang = dongDamdangRepository.findByDong(report.getAreaName()).orElseThrow(IllegalArgumentException::new);
                    Admin admin = adminRepository.findByAdminSeq(dongDamdang.getDamdang()).orElseThrow(IllegalArgumentException::new);

                    //로그 저장
                    String msg = "";
                    if(report.getReportStatus().equals("D")){
                        msg = "접수하였습니다";
                    } else if(report.getReportStatus().equals("C")){
                        msg = "담당자 " + admin.getAdminName() + "님께서 처리완료로 변경하였습니다.";
                    } else if(report.getReportStatus().equals("P")){
                        msg = "담당자 " + admin.getAdminName() + "님께서 처리중으로 변경하였습니다.";
                    } else if(report.getReportStatus().equals("R")){
                        msg = "담당자 " + admin.getAdminName() + "님께서 처리불가로 변경하였습니다.";
                    }

                    int cnt = reportLogRepository.countByReportSeq(report.getReportSeq());
                    String rn = String.valueOf(Timestamp.valueOf(LocalDateTime.now()).getTime());
                    if(cnt > 0) {
                        ReportLog reportLog = reportLogRepository.findTop1ByReportSeqOrderByLogSeqDesc(report.getReportSeq()).orElseThrow(IllegalArgumentException::new);
                        rn = reportLog.getReceptionNum();
                    }
                    ReportLog r = new ReportLog();
                    r.setReportSeq(report.getReportSeq());
                    r.setLogTime(LocalDateTime.now());
                    r.setReceptionNum(rn);
                    r.setReportStatus(report.getReportStatus());
                    r.setReportDamdang(admin.getAdminSeq());
                    r.setMsg(msg);
                    r.setReportPointRejectReason(report.getReportPointRejectReason());

                    ReportLog reportLogResult = reportLogRepository.save(r);


                } else {
                    log.info("실패 : {}", new Gson().toJson(bandBoardComment));
                }

                log.info("---------- 밴드 코멘트 가져오기 끝 -------------------");
            } catch (Exception exception) {

            }



            log.info("--------------- 댓글 처리 끝 ------------------");

            //댓글 저장하도록 자료를 넘긴다
            BandAuthor author = bandBoardComment.getAuthor();
            BandPotos potos = bandBoardComment.getPhoto();

            //댓글 저장 자료를 넘긴다.
            return new TB_BandBoardListComment(
                    bandBoardComment.getPost_key(),
                    bandBoardComment.getComment_key(),
                    bandBoardComment.getContent(),
                    bandBoardComment.getCreated_at(),
                    author.getName(),
                    author.getDescription(),
                    author.getProfile_image_url(),
                    author.getUser_key(),
                    potos.getUrl(),
                    potos.getWidth(),
                    potos.getHeight()
                   );

        };
    }

    @Bean
    public JpaItemWriter<TB_BandBoardListComment> bandCommentPostGetWriter(){
        //댓글 저장
        JpaItemWriter<TB_BandBoardListComment> jpaItemWriter = new JpaItemWriter<>();
        jpaItemWriter.setEntityManagerFactory(em);
        return jpaItemWriter;
    }

}
