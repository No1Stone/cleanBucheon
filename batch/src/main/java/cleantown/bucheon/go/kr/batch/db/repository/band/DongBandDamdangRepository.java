package cleantown.bucheon.go.kr.batch.db.repository.band;

import cleantown.bucheon.go.kr.batch.db.entity.band.DongBandDamdang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface DongBandDamdangRepository extends JpaRepository<DongBandDamdang, Long> {

    Optional<DongBandDamdang> findByDong(String dong);
    int countByDong(String dong);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_dong_band_damdang set damdang = :damdang where dong =:dong "
            , nativeQuery = true
    )
    int updateDamdang(@Param("damdang") String damdang, @Param("dong") String dong);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_dong_band_damdang set damdang_phone = :damdangPhone where dong =:dong "
            , nativeQuery = true
    )
    int updateDamdangPhone(@Param("damdangPhone") String damdangPhone, @Param("dong") String dong);

}
