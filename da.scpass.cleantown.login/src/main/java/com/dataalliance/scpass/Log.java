package com.dataalliance.scpass;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;



public class Log {
	static final Logger logger = LoggerFactory.getLogger(Log.class);
	
	
	public static void error(Exception e){
		StackTraceElement[] ste = e.getStackTrace();
		StringBuffer sb = new StringBuffer();
		for(int i=0; i < ste.length; i++){
			sb.append(ste[i].toString()+"\n");
		}
		logger.error(e.toString()+", "+sb.toString());
	}
	public static void error(Object message){
		logger.error(message.toString());
	}
	
	public static void warn(Object message){
		logger.warn(message.toString());
	}

	public static void info(Object message){
		logger.info(message.toString());
	}
	
	public static void debug(Object message){
		logger.debug(message.toString());
	}
	
	public static void debug(Object message, Object obj){
		logger.debug(message.toString(), obj);
	}
	
	
	public static void trace(Object message){
		logger.trace(message.toString());
	}
	
	public static void setLevel(Level level){
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		ch.qos.logback.classic.Logger logbackLogger = loggerContext.getLogger(Log.class);
		logbackLogger.setLevel(level);
	}
	public static Level getLevel(){
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		ch.qos.logback.classic.Logger logbackLogger = loggerContext.getLogger(Log.class);
		return logbackLogger.getLevel();
	}
}
