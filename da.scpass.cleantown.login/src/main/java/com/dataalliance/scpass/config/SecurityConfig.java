package com.dataalliance.scpass.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.cors.CorsUtils;

import com.dataalliance.scpass.security.AuthProvider;
import com.dataalliance.scpass.security.filter.AuthenticationFilter;
import com.dataalliance.scpass.security.handler.AuthFailureHandler;
import com.dataalliance.scpass.security.handler.AuthLogoutSuccessHandler;
import com.dataalliance.scpass.security.handler.AuthSuccessHandler;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired private AuthProvider authProvider;
	@Autowired private AuthLogoutSuccessHandler authLogoutSuccessHandler;
	@Autowired private AuthSuccessHandler authSuccessHandler;
	@Autowired private AuthFailureHandler authFailureHandler;
	
	
	@Override
    public void configure(WebSecurity web) throws Exception {
		web.httpFirewall(allowUrlEncodedSlashHttpFirewall());
        web.ignoring().antMatchers("/css/**","/js/**", "/images/**", "/secret/**", "/auth/**", "/rest/**");
    }
    
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf().disable()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER) 
			.and()
			.authorizeRequests().requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
			.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
			.antMatchers("/", "/logout", "/error").permitAll()
			.antMatchers("/cleantown/**").hasAnyAuthority(new String[] {"ROLE_USER"})
			.anyRequest().authenticated()
			.and()
			.formLogin().loginPage("/")
			.and()
			.logout().permitAll()
			.deleteCookies("JSESSIONID")
	        .invalidateHttpSession(true)
			//.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
			.logoutSuccessHandler(authLogoutSuccessHandler)
			.and()
			.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class);
		
		
    }
	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider);
	}
	
	
	@Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
	@Bean
	@Order(0)
	public RequestContextListener requestContextListener() {
	    return new RequestContextListener();
	}
	
	@Bean
	public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
	    DefaultHttpFirewall firewall = new DefaultHttpFirewall();
	    firewall.setAllowUrlEncodedSlash(true);
	    return firewall;
	}
	
	
	
	@Bean
    public AuthenticationFilter authenticationFilter() throws Exception {
		AuthenticationFilter authenticationFilter = new AuthenticationFilter();
        authenticationFilter.setAuthenticationSuccessHandler(authSuccessHandler);
        authenticationFilter.setAuthenticationFailureHandler(authFailureHandler);
        authenticationFilter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login", "POST"));
        authenticationFilter.setAuthenticationManager(authenticationManagerBean());
        return authenticationFilter;
    }

}
