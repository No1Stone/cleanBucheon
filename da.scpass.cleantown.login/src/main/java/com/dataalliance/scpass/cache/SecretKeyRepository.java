package com.dataalliance.scpass.cache;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

@Repository
public class SecretKeyRepository {

	@Cacheable(value = "findSecretKeyCache", key = "#kid", unless = "#result == null")
    public SecretKey findSecretKey(String kid) {
        return null;
    }

    @Cacheable(value = "findSecretKeyCache", key = "#kid")
    public SecretKey addSecretKey(String kid, String secretKey) {
    	SecretKey skey = new SecretKey();
    	skey.setKid(kid);
    	skey.setSecretKey(secretKey);
        return skey;
    }
}
