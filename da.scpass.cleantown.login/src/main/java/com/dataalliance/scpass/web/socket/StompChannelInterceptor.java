package com.dataalliance.scpass.web.socket;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.stereotype.Component;

import com.dataalliance.scpass.Log;


@Component
public class StompChannelInterceptor implements ChannelInterceptor {
 
    @Override
    public void postSend(Message<?> message, MessageChannel channel, boolean sent) {
 
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(message);
 
        // ignore non-STOMP messages like heartbeat messages
        if(sha.getCommand() == null) {
            return;
        }
 
        String sessionId = sha.getSessionId();
 
        switch(sha.getCommand()) {
            case CONNECT:
            	Log.debug("[SCPASS] STOMP Connect [sessionId: " + sessionId + "]");
                break;
            case CONNECTED:
            	Log.debug("[SCPASS] Connected [sessionId: " + sessionId + "]");
                break;
            case DISCONNECT:
            	Log.debug("[SCPASS] Disconnect [sessionId: " + sessionId + "]");
                break;
            default:
                break;
 
        }
    }

}
