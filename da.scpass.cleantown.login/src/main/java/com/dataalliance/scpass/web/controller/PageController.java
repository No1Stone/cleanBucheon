package com.dataalliance.scpass.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DevicePlatform;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.dataalliance.scpass.Log;

@Controller
public class PageController {
	/**
	 * INDEX PAGE
	 * @param device
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/")
	public ModelAndView indexPage(Device device, HttpServletRequest request){
		if(device != null) {
			if(device.isMobile()) {
				DevicePlatform platform = device.getDevicePlatform();
				request.setAttribute("platformName", platform.name());
				request.setAttribute("isMobile", "true");		
			}else {
				request.setAttribute("isMobile", "false");
				request.setAttribute("platformName", "desktop");
			}
		}else {
			request.setAttribute("isMobile", "false");
			request.setAttribute("platformName", "desktop");
		}
		return new ModelAndView("index");
	}
	
	/**
	 * MAIN PAGE
	 * @param device
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/cleantown/main")
	public ModelAndView mainPage(Device device, HttpServletRequest request){
		if(device != null) {
			if(device.isMobile()) {
				DevicePlatform platform = device.getDevicePlatform();
				request.setAttribute("platformName", platform.name());
				request.setAttribute("isMobile", "true");		
			}else {
				request.setAttribute("isMobile", "false");
				request.setAttribute("platformName", "desktop");
			}
		}else {
			request.setAttribute("isMobile", "false");
			request.setAttribute("platformName", "desktop");
		}
		return new ModelAndView("cleantown/main");
	}
	
	/**
	 * 로그아웃
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/cleantown/logout")
	public String mangerLogout(HttpServletRequest request, Authentication  auth){
		try {
			SecurityContextHolder.getContext().setAuthentication(null);
		    request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
		}catch(Exception e){
			Log.error(e);
			
		}
		return "redirect:/";
	}
}

