package com.dataalliance.scpass.web.socket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.core.MessageSendingOperations;
import org.springframework.stereotype.Service;

@Service
public class MessageBrokerService implements MessageService{

	private final MessageSendingOperations<String> brokerMessagingTemplate;
	
	@Autowired
	public MessageBrokerService(MessageSendingOperations<String> brokerMessagingTemplate) {
		this.brokerMessagingTemplate = brokerMessagingTemplate;
	}
	
	
	@Override
	public void sendAuth(String message) {
		this.brokerMessagingTemplate.convertAndSend("/queue/auth", message);
	}


}
