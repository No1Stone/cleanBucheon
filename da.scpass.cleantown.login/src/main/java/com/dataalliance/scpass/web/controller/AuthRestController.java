package com.dataalliance.scpass.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dataalliance.scpass.Log;
import com.dataalliance.scpass.SCPassAuth;
import com.dataalliance.scpass.api.MileageRequestVO;
import com.dataalliance.scpass.api.RestException;
import com.dataalliance.scpass.api.SCPassRestClientService;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/rest")
public class AuthRestController {

	
	 @Autowired
	 private SCPassRestClientService scpassRestClient;
	 
	 @GetMapping(path = "/get/userinfo/{did}")
	 public Object getUserInfo(@PathVariable String did) {
		 
		System.out.println("========================================================");
	 	System.out.println("[SCPassAuth.pass.getAccessToken()] "+ SCPassAuth.pass.getAccessToken());
	 	System.out.println("[SCPassAuth.pass.getSymmetricKey()] "+  SCPassAuth.pass.getSymmetricKey());
	 	System.out.println("[SCPassAuth.pass.getClientDID()] "+ SCPassAuth.pass.getClientDID());
	 	System.out.println("[did] "+ did);
		Log.debug("========================================================");
		 
		 
	        try{

	            String userinfo = scpassRestClient.getUserInfo(SCPassAuth.pass.getAccessToken(), SCPassAuth.pass.getSymmetricKey(), SCPassAuth.pass.getClientDID(), did);
	            return userinfo;

	        }catch (RestException rex){
	            return "not found";
	        }
	    }
	 
	 @GetMapping(path = "/get/phone/{phone}")
	 public Object getUserInfo2(@PathVariable String phone) {
		 
		System.out.println("========================================================");
	 	System.out.println("[SCPassAuth.pass.getAccessToken()] "+ SCPassAuth.pass.getAccessToken());
	 	System.out.println("[SCPassAuth.pass.getSymmetricKey()] "+  SCPassAuth.pass.getSymmetricKey());
	 	System.out.println("[SCPassAuth.pass.getClientDID()] "+ SCPassAuth.pass.getClientDID());
	 	System.out.println("[phone] "+ phone);
		Log.debug("========================================================");
		 
		 
	        try{

	            String userinfo = scpassRestClient.getUserInfo2(SCPassAuth.pass.getAccessToken(), SCPassAuth.pass.getSymmetricKey(), SCPassAuth.pass.getClientDID(), phone);
	            return userinfo;

	        }catch (RestException rex){
	            return "not found";
	        }
	    }
	 
	 @PostMapping(path = "/get/mileage")
	 public Object getMileage(@RequestBody MileageRequestVO mileageRequestVO) {
		 
		   System.out.println("========================================================");
		 	System.out.println("[SCPassAuth.pass.getAccessToken()] "+ SCPassAuth.pass.getAccessToken());
		 	System.out.println("[SCPassAuth.pass.getSymmetricKey()] "+  SCPassAuth.pass.getSymmetricKey());
		 	System.out.println("[SCPassAuth.pass.getClientDID()] "+ SCPassAuth.pass.getClientDID());
		 	System.out.println("[mileageRequestVO] "+ mileageRequestVO);
			Log.debug("========================================================");
	
	        try{
	
	            String responseStr = scpassRestClient.getMileageBalance(SCPassAuth.wallet,SCPassAuth.pass.getAccessToken(), SCPassAuth.pass.getSymmetricKey(), SCPassAuth.pass.getClientDID(),mileageRequestVO.getDid());
	            return responseStr;
	
	        }catch (RestException rex){
	            return "error";
	        }
	    }
	 
	 @PostMapping(path = "/put/mileage")
	 public Object tMileage(@RequestBody MileageRequestVO mileageRequestVO) {
		 
		   System.out.println("========================================================");
		 	System.out.println("[SCPassAuth.pass.getAccessToken()] "+ SCPassAuth.pass.getAccessToken());
		 	System.out.println("[SCPassAuth.pass.getSymmetricKey()] "+  SCPassAuth.pass.getSymmetricKey());
		 	System.out.println("[SCPassAuth.pass.getClientDID()] "+ SCPassAuth.pass.getClientDID());
		 	System.out.println("[mileageRequestVO] "+ mileageRequestVO);
			Log.debug("========================================================");
	
	        try{
	
	            String responseStr = scpassRestClient.chargeMileage(SCPassAuth.wallet,SCPassAuth.pass.getAccessToken(), SCPassAuth.pass.getSymmetricKey(), SCPassAuth.pass.getClientDID(),mileageRequestVO.getDid(),"cleantown",mileageRequestVO.getCharge());
	            return responseStr;
	
	        }catch (RestException rex){
	            return "error";
	        }
	    }
	 
	 
	 
	 
	 
	 
}
