package com.dataalliance.scpass.web.socket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class MessageBrokerConfigurer implements WebSocketMessageBrokerConfigurer{
	
	@Bean
    public StompChannelInterceptor stompChannelInterceptor() {
        return new StompChannelInterceptor();
    }
   
	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint(new String[] {"/auth/sockjs"}).setAllowedOrigins(new String[] {"http://localhost:8080", "http://192.168.0.7:8080"}).withSockJS();
	}
	
	@Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
		registration.interceptors(stompChannelInterceptor());
    };
 
    @Override
    public void configureClientOutboundChannel(ChannelRegistration registration) {
        registration.taskExecutor().corePoolSize(8);
        registration.interceptors(stompChannelInterceptor());
        
    }
    @Override
	public void configureMessageBroker(MessageBrokerRegistry config) {
		config.enableSimpleBroker("/queue/");
		config.setApplicationDestinationPrefixes(new String[] {"/auth"});
	}

}
