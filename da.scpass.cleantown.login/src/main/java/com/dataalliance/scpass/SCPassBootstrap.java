package com.dataalliance.scpass;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.dataalliance.scpass.api.RestException;
import com.dataalliance.scpass.api.SCPassRestClientService;
import com.dataalliance.scpass.bc.KeyWallet;
import com.dataalliance.scpass.bc.crypto.KeystoreException;
import com.dataalliance.scpass.bc.data.Bytes;
import com.dataalliance.scpass.exception.OAuthException;
import com.dataalliance.scpass.exception.SEPException;

@Component
public class SCPassBootstrap {
	@Value("${da.scpass.url}")
	private String platformURL;
	@Value("${da.oauth.url}")
	private String oauthURL;
	@Value("${da.oauth.username}")
	private String username;
	@Value("${da.oauth.password}")
	private String password;
	@Value("${da.wallet.keystore.password}")
	private String keystorePass;

	@Value("${da.scpass.label}")
	private String label;

	@Autowired
	private SCPassRestClientService scpassRestClient;

	public void start() {
		Log.info("==========================================================");
		Log.info("BOOTING CLEANTOWN");
		Log.info("==========================================================");

		try {
			InputStream keystore = new ClassPathResource("cleantown.keystore", this.getClass().getClassLoader())
					.getInputStream();
			SCPassService service = new SCPassService(platformURL, oauthURL, true);
			// Wallet load
			KeyWallet wallet = service.loadKeywallet(keystore, keystorePass);

			if (wallet != null) {
				// 서버<->플랫폼 간 상호인증
				SCPass pass = service.connect(wallet, username, password, label);
				// 서버에서 따로 저장해야 한다.
				SCPassAuth.wallet = wallet;
				SCPassAuth.pass = pass;

				Log.debug("========================================================");
				Log.debug("[WALLET ADDRESS] " + wallet.getAddress().toString());
				Log.debug("[WALLET PUBLIC KEY] " + wallet.getPublicKey().toHexString(false));
				Log.debug("[CLEANTOWN DID] " + pass.getClientDID());
				Log.debug("[CITYPASS DID] " + pass.getPlatformDID());
				Log.debug("[SYMMETRIC KEY] " + pass.getSymmetricKey());
				Log.debug("[ACCESS TOKEN] " + pass.getAccessToken().toString());
				Log.debug("========================================================");

				// String did = "did:dxd:0000177e95c0fe3bf2e2cfd6d51ebd7dec50fe28b534c42883f7";
				// //did입력
				// String mileageType = "cleantown";
				// int chargeMileage = 1000;
				//
				// try {
				// String res = scpassRestClient.chargeMileage(wallet, pass.getAccessToken(),
				// pass.getSymmetricKey(), pass.getClientDID(), did, mileageType,
				// chargeMileage);
				// Log.debug(">>>>>>>>>>>>>>>>>>>>>" + res);
				//
				// res = scpassRestClient.getMileageBalance(wallet, pass.getAccessToken(),
				// pass.getSymmetricKey(), pass.getClientDID(), did);
				// Log.debug("-----------------" + res);
				//
				// res = scpassRestClient.getMileageHistory(wallet, pass.getAccessToken(),
				// pass.getSymmetricKey(), pass.getClientDID(), did, "charge", 1, 10);
				// Log.debug("********************" + res);
				// } catch (RestException e) {
				// e.printStackTrace();
				// }
			}

		} catch (IOException e) { // Wallet Keystore load 오류
			e.printStackTrace();
		} catch (KeystoreException e) { // Wallet Keystore load 오류
			e.printStackTrace();
		} catch (OAuthException e) { // OAUTH2.0 인증 오류
			e.printStackTrace();
		} catch (SEPException e) { // 플랫폼 API 오류
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void stop() {
		try {
		} catch (Exception e) {
			Log.error(e);
		} finally {
			Log.info("==========================================================");
			Log.info("EXITING CLEANTOWN");
			Log.info("==========================================================");
		}
	}
}
