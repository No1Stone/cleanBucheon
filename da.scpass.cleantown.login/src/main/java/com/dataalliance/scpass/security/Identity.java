package com.dataalliance.scpass.security;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class Identity implements UserDetails{
	private static final long serialVersionUID = 1L;
	private String did;
	private String username;
	private String phone;
	private String email;
	private String picture;
	private String status;
	
	private boolean enabled = true; // 계정이 사용가능한 계정인지를 리턴한다(true이면 사용가능한 계정인지를 의미)
	private boolean accountNonLocked = true; //계정이 잠겨있지 않았는지를 리턴한다.(true이면 있음)
	private boolean accountNonExpired = true; //계정이 만료되지 않았는지를 리턴한다(true이면 만료되지 않음을 의미) 
	private boolean credentialsNonExpired = true; // 계정의 비밀번호가 만료되지 않았는지를 리턴한다(true이면 비밀번호가 만료되지 않음을 의미)
	
	private List<GrantedAuthority> authoritys;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authoritys;
	}
	public void setAuthoritys(List<GrantedAuthority> authoritys) {
		this.authoritys = authoritys;
	}
	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}
	@Override
	public String getPassword() {
		return null;
	}
	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}
	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}
	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}
	@Override
	public boolean isEnabled() {
		return enabled;
	}
	public String getDid() {
		return did;
	}
	public void setDid(String did) {
		this.did = did;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
