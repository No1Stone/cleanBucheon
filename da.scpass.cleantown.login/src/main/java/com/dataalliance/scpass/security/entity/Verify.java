package com.dataalliance.scpass.security.entity;

public class Verify {
	private String did;
	private String signature;
	private String nextNonce;
	public String getDid() {
		return did;
	}
	public void setDid(String did) {
		this.did = did;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getNextNonce() {
		return nextNonce;
	}
	public void setNextNonce(String nextNonce) {
		this.nextNonce = nextNonce;
	}
	
	
}
