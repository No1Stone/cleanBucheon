package com.dataalliance.scpass.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.dataalliance.scpass.Log;
import com.dataalliance.scpass.security.Identity;



@Component
public class AuthSuccessHandler extends SimpleUrlAuthenticationSuccessHandler{

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		if(authentication.getPrincipal() instanceof Identity) {
			Identity user = (Identity)authentication.getPrincipal();
			Log.debug("[CITYPASS] AuthenticationSuccess   "+user.toString());
		}
		getRedirectStrategy().sendRedirect(request, response, "/");
		super.onAuthenticationSuccess(request, response, authentication);
	} 

}
