package com.dataalliance.scpass.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class AuthLogoutSuccessHandler implements LogoutSuccessHandler{
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		if(authentication == null){
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.sendRedirect(request.getContextPath()+"/");
		}else{
			response.setStatus(HttpServletResponse.SC_OK);
			response.sendRedirect(request.getContextPath()+"/");
		}
		
	}

}
