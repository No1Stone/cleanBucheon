package com.dataalliance.scpass.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.dataalliance.scpass.Log;


@Component
public class AuthFailureHandler extends SimpleUrlAuthenticationFailureHandler{

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
		
		Log.debug("[SCPASS] Authentication Failure ["+exception.getClass().getName()+", "+exception.getMessage()+"]");
		if (exception.getMessage().equals("Bad credentials")) {
	        //response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			getRedirectStrategy().sendRedirect(request, response, "/");
			//super.onAuthenticationFailure(request, response, exception);
	        
//	        RestResponse rest = new RestResponse();
//	        rest.setResult("fail");
//			rest.setCode(401);
//			rest.setError("error.badcredentials");
//	        response.getWriter().print(new ObjectMapper().writeValueAsString(rest));
	    } else {
	        super.onAuthenticationFailure(request, response, exception);
	    }
	    
	}

}
