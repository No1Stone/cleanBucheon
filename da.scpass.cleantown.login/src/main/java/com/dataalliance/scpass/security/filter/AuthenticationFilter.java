package com.dataalliance.scpass.security.filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.dataalliance.scpass.Log;
import com.dataalliance.scpass.security.Identity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	public static final String SPRING_SECURITY_FORM_DID_KEY = "did";
	public static final String SPRING_SECURITY_FORM_PHONE_KEY = "phone";
	public static final String SPRING_SECURITY_FORM_EMAIL_KEY = "email";
	public static final String SPRING_SECURITY_FORM_PICTURE_KEY = "picture";

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		if (!request.getMethod().equals("POST")) {
			throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
		}

		String username = obtainUsername(request);
		String did = request.getParameter(SPRING_SECURITY_FORM_DID_KEY);
		String phone = request.getParameter(SPRING_SECURITY_FORM_PHONE_KEY);
		String email = request.getParameter(SPRING_SECURITY_FORM_EMAIL_KEY);
		String picture = request.getParameter(SPRING_SECURITY_FORM_PICTURE_KEY);
		if (username == null) {
			username = (String)request.getAttribute(SPRING_SECURITY_FORM_USERNAME_KEY);
			if(username == null) {
				username = "";	
			}
			
		}

		
		if (did == null) {
			did = "";
		}
		if (phone == null) {
			phone = "";
		}
		if (email == null) {
			email = "";
		}
		if (picture == null) {
			picture = "";
		}
		username = username.trim();
		
		List<GrantedAuthority> authoritys = new ArrayList<GrantedAuthority>();
		authoritys.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		
		Log.debug("[CLEAN TOWN] AuthenticationFilter did="+did+" username="+username);
		
		
		Map<String ,Object> map = new HashMap<String, Object>();
		map.put("name", username);
		map.put("phone", phone);
		map.put("email", email);
		map.put("picture", picture);
		String identityCard = "";
		try {
			identityCard = new ObjectMapper().writeValueAsString(map);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Identity identity = new Identity();
		identity.setUsername(username);
		identity.setDid(did);
		identity.setStatus("activate");
		identity.setAuthoritys(authoritys);
		UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(identity, did, authoritys);

		setDetails(request, authRequest);
		
		return this.getAuthenticationManager().authenticate(authRequest);
		
	}

}
