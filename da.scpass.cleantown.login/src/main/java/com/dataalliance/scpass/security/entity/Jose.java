package com.dataalliance.scpass.security.entity;

public class Jose {
	//대칭키의 id
	private String kid;
	//JWE암호화된 데이터
	private String data;
	
	public String getKid() {
		return kid;
	}
	public void setKid(String kid) {
		this.kid = kid;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
