package com.dataalliance.scpass.security.controller;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.dataalliance.scpass.Log;
import com.dataalliance.scpass.SCPassAuth;
import com.dataalliance.scpass.SCPassService;
import com.dataalliance.scpass.api.SCPassRestClientService;
import com.dataalliance.scpass.auth.AuthData;
import com.dataalliance.scpass.cache.SecretKey;
import com.dataalliance.scpass.cache.SecretKeyRepository;
import com.dataalliance.scpass.security.Identity;
import com.dataalliance.scpass.security.entity.AuthInfo;
import com.dataalliance.scpass.security.entity.Jose;
import com.dataalliance.scpass.security.entity.Secret;
import com.dataalliance.scpass.security.entity.Verify;
import com.dataalliance.scpass.utils.ByteUtils;
import com.dataalliance.scpass.web.socket.MessageBrokerService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class SecretController {
	
	@Value("${da.oauth.url}")
    private String oauthURL;
	@Value("${da.scpass.url}")
    private String platformURL;
	
	@Value("${da.scpass.label}")
	private String label;

	@Autowired private SecretKeyRepository secretKeyRepository;
	@Autowired private MessageBrokerService messageBrokerService;
	@Autowired private SCPassRestClientService scpassRestClient;

	@Autowired AuthenticationManager authenticationManager;
	@Autowired private ServletContext servletContext;
	
	private SCPassService scpassService;
	
	
	@PostConstruct
    public void init() {
		scpassService = new SCPassService(platformURL, oauthURL, true);
    }

	/**
	 * 1. 깨끗한마을에서 상호인증 정보 생성
	 * @return
	 */
	@RequestMapping(value="/secret/data", method=RequestMethod.GET, produces = "application/json;charset=UTF-8") 
	public ResponseEntity<?> dataSecret(HttpServletRequest request) {
		try {
			Log.debug("======================================================");
			Log.debug("1. 깨끗한마을에서 상호인증 정보 생성");
			
			HttpRequest httpRequest = new ServletServerHttpRequest(request); //request is HttpServletRequest
			UriComponents uriComponents = UriComponentsBuilder.fromHttpRequest(httpRequest).build();
			
			// 사용자 구분용 인증키(로그인시 구분)
			byte[] keyBytes = new byte[32];
			SecureRandom random = new SecureRandom();
	    	random.nextBytes(keyBytes);
	    	String authKey = byteToHex(keyBytes);
	    	
			// 시티패스 앱이 인증요청할 깨끗한마을 주소
			String callbackUrl = uriComponents.getScheme()+"://"+request.getServerName();
			if(request.getServerPort() > 0 && request.getServerPort() != 80) {
				callbackUrl += ":" + request.getServerPort();
			}
			callbackUrl += servletContext.getContextPath()+"/secret/create/"+authKey;
			// 깨끗한마을 인증데이터 생성
			AuthData authData = new AuthData();
			authData.setDid(SCPassAuth.pass.getClientDID());
			authData.setCallback(callbackUrl);
			authData.setType("login");
			authData.setPortalCode("CleanTown");
			// 시티패스 플랫폼에 깨끗한 마을 인증데이터를 등록하고 인증코드를 받는다
			// 인증코드 만료시간(10분)
			String authCode = scpassService.addAuthData(label, SCPassAuth.pass.getSymmetricKey(), SCPassAuth.pass.getAccessToken(), authData);
			Log.debug("[SECRET DATA AUTH CODE] "+authCode);
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("authCode", authCode);
			data.put("authKey", authKey); // 웹 페이지에서 사용자 구분용 키
			
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(data), HttpStatus.OK) ;
		}catch(Exception e) {
			Log.error(e);
			return new ResponseEntity<String>("{}", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	/**
	 * 2. 시티패스앱에서 상호인증 생성 요청
	 * @param authKey 사용자 구분 인증키
	 * @param secret 앱에서 보내는 인증 정보
	 * @return
	 */
	@RequestMapping(value="/secret/create/{authKey}", method=RequestMethod.POST, produces = "application/json;charset=UTF-8") 
	public ResponseEntity<?> createSecret(@PathVariable("authKey") String authKey, @RequestBody Secret secret, HttpServletRequest request) {
		try {
			Log.debug("======================================================");
			Log.debug("2. 시티패스앱에서 상호인증 생성 요청");
			if(StringUtils.isBlank(secret.getDid())) {
				return new ResponseEntity<String>("{}", HttpStatus.BAD_REQUEST);
			}
			// 시티패스 앱과 깨끗한마을간 상호인증을 위한 대칭키 생성
			// (앱과 플랫폼은 암호화 통신을 하므로 대칭키를 생성 한다.)
			// EHCache를 이용하여 대칭키 저장(만료시간은 알아서 설정)
			// 샘플코드에서는 EHCache를 사용하였지만 REDIS등 다른 캐쉬를 사용해도 됨
			SecretKey secretKey = secretKeyRepository.findSecretKey(secret.getDid());
			if(secretKey == null) {
				String userAgent = request.getHeader("user-agent");
				byte[] symmetricKey = null;
				 
				if(userAgent != null && (userAgent.toLowerCase().indexOf("ios") != -1 || userAgent.toLowerCase().indexOf("iphone") != -1 || userAgent.toLowerCase().indexOf("ipod") != -1)) {
					 symmetricKey = scpassService.generateSymmetricKeyIOS(SCPassAuth.wallet, SCPassAuth.pass.getAccessToken(), secret.getDid(), secret.getNonce());
				 }else {
					 symmetricKey = scpassService.generateSymmetricKeyAndroid(SCPassAuth.wallet, SCPassAuth.pass.getAccessToken(), secret.getDid(), secret.getNonce());
				 }
				
				secretKey = secretKeyRepository.addSecretKey(secret.getDid(), ByteUtils.byteToHex(symmetricKey));
			}
			// 깨끗한마을 난수 생성
			byte[] nonceBytes = new byte[32];
			SecureRandom random = new SecureRandom();
	    	random.nextBytes(nonceBytes);
	    	String authNonce = byteToHex(nonceBytes);
	    	
	    	
	    	// 깨끗한마을 상호인증 검증 주소
	    	HttpRequest httpRequest = new ServletServerHttpRequest(request); //request is HttpServletRequest
			UriComponents uriComponents = UriComponentsBuilder.fromHttpRequest(httpRequest).build();
			
			String callbackUrl = uriComponents.getScheme()+"://"+request.getServerName();
			if(request.getServerPort() > 0 && request.getServerPort() != 80) {
				callbackUrl += ":" + request.getServerPort();
			}
			callbackUrl += servletContext.getContextPath()+"/secret/verify/"+authKey+"?nonce="+authNonce;
			
			Log.debug("[CLEANTOWN] CREATE SECRETKEY="+secretKey.getSecretKey());
			Log.debug("[CLEANTOWN] CREATE AUTHKEY="+authKey);
			Log.debug("[CLEANTOWN] CREATE AUTHNONCE="+authNonce);
			Log.debug("[CLEANTOWN] CREATE CALLBACK="+callbackUrl);
			
			Map<String, String> map = new HashMap<String, String>();
			map.put("did", SCPassAuth.pass.getClientDID());
			map.put("nonce", authNonce);
			map.put("callback", callbackUrl);
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(map), HttpStatus.OK) ;
		}catch(Exception e) {
			Log.error(e);
			return new ResponseEntity<String>("{}", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	/**
	 * 3. 시티패스 앱 DID 상호인증 증명
	 * @param authKey 사용자 구분용 인증키
	 * @param nonce 깨끗한마을 난수
	 * @param jose 사용자 인증 정보
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/secret/verify/{authKey}", method=RequestMethod.POST, produces = "application/json;charset=UTF-8") 
	public ResponseEntity<?> verifySecret(
			@PathVariable(name = "authKey") String authKey, 
			@RequestParam(name = "nonce") String nonce, 
			@RequestBody Jose jose, HttpServletRequest request) {
		try {
			Log.debug("======================================================");
			Log.debug("3. 시티패스 앱 DID 상호인증 증명");
			if(StringUtils.isBlank(jose.getKid()) || StringUtils.isBlank(jose.getData())) {
				return new ResponseEntity<String>("{}", HttpStatus.BAD_REQUEST);
			}
			// 대칭키 캐쉬에서 가져오기
			SecretKey secretKey = secretKeyRepository.findSecretKey(jose.getKid());
			if(secretKey == null) {
				return new ResponseEntity<String>("{}", HttpStatus.UNAUTHORIZED);
			}
			Log.debug("[CLEANTOWN] VERIFY AUTH NONCE="+nonce); // 깨끗한마을 난수
			Log.debug("[CLEANTOWN] VERIFY AUTH KEY="+authKey); // 사용자 구분용 인증키 
			Log.debug("[CLEANTOWN] VERIFY USER DID="+jose.getKid());  // 사용자 DID
			Log.debug("[CLEANTOWN] VERIFY AUTH DATA="+jose.getData()); // 사용자 인증데이터
			Log.debug("[CLEANTOWN] VERIFY SECRETKEY="+secretKey.getSecretKey()); //대칭키
		
			// 사용자 인증데이터 복호화
			String decryptData = scpassService.decrypt(secretKey.getSecretKey(), jose.getData());
			Log.debug("[CLEANTOWN] VERIFY decryptData : "+decryptData); // 사용자 DID, 사용자 signature, 사용자 난수(nextNonce)
			// 시티패스 앱 Signature 검증
			ObjectMapper mapper = new ObjectMapper();
			Verify verify = mapper.readValue(decryptData, Verify.class);
			if(scpassService.verifySignature(SCPassAuth.pass.getAccessToken(), verify.getDid(), nonce, verify.getSignature())) {
				Log.debug("[CLEANTOWN] VERIFY = true");
				//사용자 정보를 전달할 url
				HttpRequest httpRequest = new ServletServerHttpRequest(request); //request is HttpServletRequest
				UriComponents uriComponents = UriComponentsBuilder.fromHttpRequest(httpRequest).build();
				
				String callbackUrl = uriComponents.getScheme()+"://"+request.getServerName();

				if(request.getServerPort() > 0 && request.getServerPort() != 80) {
					callbackUrl += ":" + request.getServerPort();
				}
				callbackUrl += servletContext.getContextPath()+"/secret/info/"+authKey;
				
				// 사용자 정보 요청 응답
				String sig = scpassService.getSignature(SCPassAuth.wallet, verify.getNextNonce());
				Map<String ,Object> map = new HashMap<String, Object>();
				map.put("result", 200);
				map.put("signature", sig); // 깨끗한마을 Signature
				map.put("did", SCPassAuth.pass.getClientDID()); // 깨끗한마을 DID
				map.put("callback", callbackUrl); // 사용자 정보를 받을 주차플랫폼의 url
				map.put("requested", Arrays.asList("name","phone"));  // 사용자 정보 요청 목록
				String json = mapper.writeValueAsString(map);
				// 데이터 암호화 ( Android, iOS 암호화 알고리즘이 달라 암호화 데이터를 파싱하여 구분)
				Log.debug("[CLEANTOWN] "+json);
				String jweData = scpassService.encrypt(jose.getData(), secretKey.getSecretKey(), json);

				Map<String ,Object> datamap = new HashMap<String, Object>();
				datamap.put("data", jweData);
				return new ResponseEntity<String>(mapper.writeValueAsString(datamap), HttpStatus.OK) ;
			}else {
				Log.debug("[CLEANTOWN] VERIFY = false");
				return new ResponseEntity<String>("{\"result\":400}", HttpStatus.BAD_REQUEST) ;	
			}
			
		}catch(Exception e) {
			Log.error(e);
			return new ResponseEntity<String>("{}", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	/**
	 *  4. 사용자 정보 받기
	 * 상호인증으로 시티패스 앱에서 직접 사용자 정보를 받을 수 있다.
	 * 더많은 사용자 정보는 시티패스 플랫폼 REST API를 이용하여 요청
	 * @param authKey 사용자 구분용 인증키
	 * @param jose 사용자 정보 
	 * @return
	 */
	@RequestMapping(value="/secret/info/{authKey}", method= RequestMethod.POST, produces = "application/json;charset=UTF-8") 
	public ResponseEntity<?> info(@PathVariable("authKey") String authKey, @RequestBody Jose jose, HttpServletRequest request, HttpServletResponse response) {
		try {
			Log.debug("======================================================");
			Log.debug("4. 사용자 정보 받기");
			if(StringUtils.isBlank(jose.getKid()) || StringUtils.isBlank(jose.getData())) {
				return new ResponseEntity<String>("{}", HttpStatus.BAD_REQUEST);
			}
			// 대칭키 캐쉬에서 가져오기
			SecretKey secretKey = secretKeyRepository.findSecretKey(jose.getKid());
			if(secretKey == null) {
				return new ResponseEntity<String>("{}", HttpStatus.UNAUTHORIZED);
			}
			// 사용자 정보 복호화
			String decryptData = scpassService.decrypt(secretKey.getSecretKey(), jose.getData());
			Log.debug("[SECRET ACCOUNT INFO]"+decryptData);
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> map = mapper.readValue(decryptData, new TypeReference<Map<String, Object>>() {});
			Map<String, Object> userInfo = (Map<String, Object>)map.get("userInfo");
			
			AuthInfo info = new AuthInfo();
			info.setResult("success");
			info.setDid(jose.getKid()); // 사용자 DID
			info.setKey(authKey); // 사용자 구분용 인증키(세션연결때 사용)
			info.setPhone(userInfo.get("phone").toString()); // 사용자 전화번호
			info.setUsername(userInfo.get("name").toString()); // 사용자 이름
			messageBrokerService.sendAuth(mapper.writeValueAsString(info));
			Log.debug("[CLEANTOWN] User Info from APP "+mapper.writeValueAsString(info));
			
			// 플랫폼 사용자 정보 조회(더많은 정보를 받아 볼 수 있다)
			String uInfo = scpassRestClient.getUserInfo(SCPassAuth.pass.getAccessToken(), SCPassAuth.pass.getSymmetricKey(), SCPassAuth.pass.getClientDID(), jose.getKid());
			Log.debug("[CLEANTOWN] User Info from Platform "+uInfo);
			
			return new ResponseEntity<String>("{}", HttpStatus.OK);
		}catch(Exception e) {
			Log.error(e);
			return new ResponseEntity<String>("{}", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	/**
	 *  5.깨끗한마을 사용자 인증 설정
	 *  예제이며 깨끗한마을의 인증 시스템 사용
	 *  여기서 깨끗한 마을 로그인 처리를 한다.
	 * @param info
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/secret/auth", method= RequestMethod.POST, produces = "application/json;charset=UTF-8") 
	public ResponseEntity<?> auth(@RequestBody AuthInfo info, HttpServletRequest request) {
		try {
			Log.debug("======================================================");
			Log.debug("5. 깨끗한마을 사용자 인증 설정");
			Identity identity = new Identity(); 
			identity.setDid(info.getDid());
			identity.setUsername(info.getUsername());
			identity.setPhone(info.getPhone());
			identity.setPicture(info.getPicture());
			
			List<GrantedAuthority> authoritys = new ArrayList<>();
			authoritys.add(new SimpleGrantedAuthority("ROLE_USER"));
			identity.setAuthoritys(authoritys);
			
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(identity, identity.getPassword());
			Authentication authentication = authenticationManager.authenticate(token);
		    SecurityContextHolder.getContext().setAuthentication(authentication);
		    
		    request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
	        
		    Log.debug("[CLEANTOWN] SESSION "+ request.getSession().getId());
		    Map<String ,Object> data = new HashMap<String, Object>();
			data.put("result", "success");
			
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(data), HttpStatus.OK);
		}catch(Exception e) {
			Log.error(e);
			return new ResponseEntity<String>("{}", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	/**
	 * 세션에 설정된 사용자 정보 조회
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/cleantown/principal", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> getPrincipal(Authentication  auth){
		try{
			if(auth == null) {
				Log.debug("Auth null");
			}
			Map<String, Object> data = new HashMap<String, Object>();
			Map<String, Object> authInfo = new HashMap<String, Object>();
			if(auth != null && auth.getPrincipal() != null) {
				if(auth.getPrincipal() instanceof Identity) {
					Identity identity = (Identity)auth.getPrincipal();
					authInfo.put("did", identity.getDid());
					authInfo.put("username", identity.getUsername());
					authInfo.put("phone", identity.getPhone());
				}
			}
			data.put("identity", authInfo);
			return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(data), HttpStatus.OK);	
			
		}catch(Exception e){
			Log.error(e);
			return new ResponseEntity<String>("{}", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	public String byteToHex(byte[] inBytes){
		int startIndex = 0;
		int endIndex = inBytes.length;
		byte newByte = 0x00;
		int i,hexIndex; 
		String hexChars = "0123456789ABCDEF";
		StringBuffer outBuffer = new StringBuffer(endIndex - startIndex);
		if ( inBytes == null || endIndex <= startIndex ) return (String)null;
		for ( i = startIndex; i < endIndex; i++){
			newByte = (byte)(inBytes[i] & 0xF0);  
			newByte = (byte)(newByte >>> 4);     
			newByte = (byte)(newByte & 0x0F); 
			hexIndex = (int)newByte; 
			outBuffer.append(hexChars.substring(hexIndex,hexIndex + 1));
 
			newByte = (byte)(inBytes[i] & 0x0F);
			hexIndex = (int)newByte;
			outBuffer.append(hexChars.substring(hexIndex,hexIndex + 1));
             
			outBuffer.append("");
		}
		return outBuffer.toString();
	}
}
