package com.dataalliance.scpass.security.entity;

public class AccessInfo {
	private String page;
	private long time;
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	
	
}
