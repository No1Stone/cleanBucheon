package com.dataalliance.scpass.security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.dataalliance.scpass.Log;


@Component
public class AuthProvider implements AuthenticationProvider  {

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		Identity user = (Identity)authentication.getPrincipal();
		Log.debug("[SCPASS] AuthProvider "+user.getDid());

		return new UsernamePasswordAuthenticationToken(user, user.getDid(), user.getAuthorities());
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
