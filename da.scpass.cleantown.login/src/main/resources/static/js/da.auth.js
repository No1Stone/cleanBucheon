(function($) {
$.client = $.da.client();	// Ajax, WebSocket class
$.scpass = {
	isMobile : false, platformName : 'desktop',
	sub : null, authKey : null,
	authorize : function(isMobile, platformName){
		this.isMobile = isMobile == 'true' ? true : false;
		this.platformName = platformName;
		if(isMobile == 'true'){
			
			$('#but-login').click(function(){
				$.scpass.startAuth();
			});
			$.scpass.startAuth();
        }
	},
	startAuth : function(){
		$.client.getAuthData(true, function(res){ // get auth data 
			$.scpass.authKey = res.authKey; // 사용자 구분용 키
			 // in app 연결
   		 	if($.scpass.platformName == 'ANDROID'){
   		 		 location.href='intent://portal?data='+res.authCode+'#Intent;scheme=sso;package=com.da.citypass.test;end';
            }else if($.scpass.platformName == 'IOS'){
                 location.href='com.da.citypass://portal?data='+res.authCode;
            }
            $.scpass.monitoring(); // 상호인증 모니터링 세션연결을 위해 websocket을 사용
		});
	},
	
	monitoring : function(){ // WebSocket Connect
		$.client.connect(function(res){
			if(res.result == 'server.connected'){ 
				$.scpass.live();
			}
		});
	},
	stop : function(){ // WebSocket disconnect
		if($.scpass.sub != null){
			$.client.unlivestream($.scpass.sub.id);
			$.scpass.sub = null;
		}
	 
	},
	live : function(){ // WebSocket subscribe
		$.scpass.sub = $.client.livestream('/queue/auth', function(msg){
			if(msg.result == 'success' && msg.key == $.scpass.authKey){
				$.client.authSession({ // 세션 연결
            		'did' : msg.did,
            		'phone' : msg.phone,
            		'username' : msg.username
            	}, true, function(res){
        			if(res.result == 'success'){
        				$.scpass.stop();
        				location.href = '/cleantown/main';	
        			}
        		});
				
			}else{
				alret('접근 권한이 없습니다.');
			}
			
		});	
		
	},
	getPrincipal : function(){ // 사용자 정보 
		$('#but-logout').click(function(){
			location.href = '/cleantown/logout';
		});
		$.client.getPrincipal(true, function(res){ 
   		 	var identity = res.identity;
   		 	$('#user-info').html(identity.did+", "+identity.username+", "+identity.phone);
   		 	
		});
		
	}
	
};	
	
})(jQuery);