/*
   Copyright (C) 2021
   @author: BJK
   @since: 2021.03
   @desc : Ajax, WebSocket client 
 */
(function() {
	'use strict';
	var Client, DA;
	
  	
	Client = (function() {
  		var c;
  		function Client() {
      		this.subscriptions = {};
      		c = this;
      		$(window).bind('beforeunload',function(){
		    	if(c.stomp != null && c.stomp.connected){
//		    		$.each(c.subscriptions, function(index, value) {
//		    		    value.unsubscribe();
//		    		}); 
//		    		c.stomp.disconnect();
		    	}
		    });
      		
    	};
    	Client.prototype.ajax = function(reqData, callback){
			if(reqData.type.toUpperCase() == 'GET'){
				$.ajax({
    				method: 'GET',
    				url: reqData.url,
    				async : reqData.async,
    				contentType : 'application/json;charset=UTF-8', 
    	            data : reqData.data,
    				dataType: 'json',
    				error: function(xhr, textStatus, errorThrown) { 
    					if(callback !== undefined && callback !== null){
							callback(xhr.responseJSON);
						} 
    	            },
    	            success : function (data) {
    	            	if(callback !== undefined && callback !== null) {
    	            		callback(data);
    	            	}
    				}
    			});
    			
    		}else if(reqData.type.toUpperCase() == 'POST'){
    			$.ajax({
    				method: 'POST',
    				url: reqData.url,
    				async : reqData.async,
    				dataType: 'json',
    	            data : JSON.stringify(reqData.data),
    	            contentType : 'application/json;charset=UTF-8', 
    	            error: function(xhr, textStatus, errorThrown) { 
    	            	if(callback !== undefined && callback !== null){
							callback(xhr.responseJSON);
						} 
    	            },
    	            success : function (data, textStatus, jqXHR) {
						if(callback !== undefined && callback !== null){
    	            		callback(data);
    	            	}
    				}
    			});
    		}else if(reqData.type.toUpperCase() == 'PUT'){
    			$.ajax({
    				method : 'PUT',
    				url: reqData.url,
    				async : reqData.async,
    				data : JSON.stringify(reqData.data),
    				dataType: 'json',
    				contentType : 'application/json;charset=UTF-8', 
    				error: function(xhr, textStatus, errorThrown) { 
    	            	if(callback !== undefined && callback !== null){
							callback(xhr.responseJSON);
						} 
    	            },
    	            success : function (data) {
    	            	if(callback !== undefined && callback !== null){
    	            		callback(data);
    	            	}
    				}
    			});
    		}else if(reqData.type.toUpperCase() == 'DELETE'){
    			$.ajax({
    				method : 'DELETE',
    				url: reqData.url,
    				async : reqData.async,
    				contentType : 'application/json;charset=UTF-8', 
    	            data : reqData.data,
    	            dataType: 'json',
    	            error: function(xhr, textStatus, errorThrown) { 
    					if(callback !== undefined && callback !== null){
							callback(xhr.responseJSON);
						} 
    	            },
    	            success : function (data) {
    	            	if(callback !== undefined && callback !== null){
    	            		callback(data);
    	            	}
    				}
    			});
    		}
    	
    	};
    	Client.prototype.connect = function(callback) {
    		if(c.stomp == undefined){
    			var sock = new SockJS(DA.context+'/auth/sockjs');
    		  	c.stomp = Stomp.over(sock);
    		  	c.stomp.debug = null;
    		    c.stomp.connect({}, function(frame) {
    		    	if(callback !== undefined && callback !== null) callback({result : 'server.connected'});
    		    });
    		    sock.onclose = function(event) {
    		    	if(callback !== undefined && callback !== null) callback({result : 'server.disconnected'});
    		    };
    		}else{
    			if(callback !== undefined && callback !== null) callback({result : 'server.connected'});
    		}
    	};
		
    	Client.prototype.disconnect = function() {
    		if(c.stomp != null && this.stomp.connected){
		    	c.stomp.disconnect();
		    }
      
    	};
    	
    	Client.prototype.livestream  = function(endpoint, callback){
    		if(c.stomp != null && c.stomp.connected){
	    		var item = c.stomp.subscribe(endpoint, function(message) {
	    			if(callback !== undefined && callback !== null){
	    				var body = $.parseJSON(message.body);
	    				callback(body);
	    			}
		    	});
	    		item.endpoint = endpoint;
	    		c.subscriptions[item.id] = item;
	    		return item;
			}else{
				setTimeout(c.livestream, 200, endpoint, callback);
			}
    	};
    	Client.prototype.unlivestream  = function(id){
			if(c.stomp != null && c.stomp.connected){
				delete c.subscriptions[id];
	    		c.stomp.unsubscribe(id);
			}
    	};
    	Client.prototype.getAuthData = function(async, callback){
    		this.ajax({
    			'type' : 'GET',
    			'async' : async == undefined ? true : async,
    	    	'url': DA.context+'/secret/data', 
    			'data' : {}
    		}, callback);
    	};
    	Client.prototype.authSession = function(reqData, async, callback){
    		this.ajax({
    			'type' : 'POST',
    			'async' : async == undefined ? true : async,
    	    	'url': DA.context+'/secret/auth', 
    			'data' : {
    				'did' : reqData.did,
    				'username' : reqData.username,
    				'phone' : reqData.phone
    			}
    		}, callback);
    	};
    	Client.prototype.getPrincipal = function(async, callback){
    		this.ajax({
    			'type' : 'GET',
    			'async' : async == undefined ? true : async,
    	    	'url': DA.context+'/cleantown/principal', 
    			'data' : {}
    		}, callback);
    	};
    	
    	
    	return Client;
  	})();
	
   
  	
		
    DA = {
    	version:'1.0',
    	context : '',
    	client: function(context) {
    		this.context = context == undefined ? "" : context;
			return new Client();
    	},
    };
  	
  	if ( typeof module === "object" && module && typeof module.exports === "object" ) {
		module.exports = DA;
	} else {
		window.DA = window.$.da = DA;
		if ( typeof define === "function" && define.amd ) {
			define( "da", [], function () { return DA; } );
		}
	}
  	
		
})();