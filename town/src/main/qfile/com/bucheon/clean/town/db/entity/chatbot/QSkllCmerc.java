package com.bucheon.clean.town.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSkllCmerc is a Querydsl query type for SkllCmerc
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSkllCmerc extends EntityPathBase<SkllCmerc> {

    private static final long serialVersionUID = -496163886L;

    public static final QSkllCmerc skllCmerc = new QSkllCmerc("skllCmerc");

    public final StringPath btton = createString("btton");

    public final StringPath bttonOptnAt = createString("bttonOptnAt");

    public final StringPath cmercOrdr = createString("cmercOrdr");

    public final StringPath crncy = createString("crncy");

    public final StringPath dscnt = createString("dscnt");

    public final StringPath dscntPc = createString("dscntPc");

    public final StringPath dscntRt = createString("dscntRt");

    public final StringPath goodsDc = createString("goodsDc");

    public final StringPath pc = createString("pc");

    public final StringPath profl = createString("profl");

    public final StringPath rspnsOrdr = createString("rspnsOrdr");

    public final StringPath skllCode = createString("skllCode");

    public final StringPath thumb = createString("thumb");

    public QSkllCmerc(String variable) {
        super(SkllCmerc.class, forVariable(variable));
    }

    public QSkllCmerc(Path<? extends SkllCmerc> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSkllCmerc(PathMetadata metadata) {
        super(SkllCmerc.class, metadata);
    }

}

