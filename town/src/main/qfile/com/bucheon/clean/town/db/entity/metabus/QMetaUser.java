package com.bucheon.clean.town.db.entity.metabus;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMetaUser is a Querydsl query type for MetaUser
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMetaUser extends EntityPathBase<MetaUser> {

    private static final long serialVersionUID = 1880125478L;

    public static final QMetaUser metaUser = new QMetaUser("metaUser");

    public final NumberPath<Integer> avatarType = createNumber("avatarType", Integer.class);

    public final NumberPath<Integer> cleanPoint = createNumber("cleanPoint", Integer.class);

    public final StringPath cleantownUid = createString("cleantownUid");

    public final DateTimePath<java.time.LocalDateTime> createdAt = createDateTime("createdAt", java.time.LocalDateTime.class);

    public final StringPath delYn = createString("delYn");

    public final StringPath deviceToken = createString("deviceToken");

    public final StringPath email = createString("email");

    public final DateTimePath<java.time.LocalDateTime> lastConnectAt = createDateTime("lastConnectAt", java.time.LocalDateTime.class);

    public final StringPath nickname = createString("nickname");

    public final StringPath provider = createString("provider");

    public final StringPath snsId = createString("snsId");

    public final DateTimePath<java.time.LocalDateTime> updatedAt = createDateTime("updatedAt", java.time.LocalDateTime.class);

    public final StringPath userId = createString("userId");

    public QMetaUser(String variable) {
        super(MetaUser.class, forVariable(variable));
    }

    public QMetaUser(Path<? extends MetaUser> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMetaUser(PathMetadata metadata) {
        super(MetaUser.class, metadata);
    }

}

