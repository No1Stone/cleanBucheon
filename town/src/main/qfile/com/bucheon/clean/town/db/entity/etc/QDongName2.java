package com.bucheon.clean.town.db.entity.etc;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDongName2 is a Querydsl query type for DongName2
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDongName2 extends EntityPathBase<DongName2> {

    private static final long serialVersionUID = -1676044698L;

    public static final QDongName2 dongName2 = new QDongName2("dongName2");

    public final StringPath bgCode = createString("bgCode");

    public final StringPath bname = createString("bname");

    public final NumberPath<Long> dongSeq = createNumber("dongSeq", Long.class);

    public final StringPath hDivCode = createString("hDivCode");

    public final StringPath hgCode = createString("hgCode");

    public final StringPath hname = createString("hname");

    public QDongName2(String variable) {
        super(DongName2.class, forVariable(variable));
    }

    public QDongName2(Path<? extends DongName2> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDongName2(PathMetadata metadata) {
        super(DongName2.class, metadata);
    }

}

