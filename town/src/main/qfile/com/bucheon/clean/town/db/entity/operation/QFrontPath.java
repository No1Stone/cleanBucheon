package com.bucheon.clean.town.db.entity.operation;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QFrontPath is a Querydsl query type for FrontPath
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFrontPath extends EntityPathBase<FrontPath> {

    private static final long serialVersionUID = 226396356L;

    public static final QFrontPath frontPath = new QFrontPath("frontPath");

    public final StringPath pathName = createString("pathName");

    public final NumberPath<Long> pathSeq = createNumber("pathSeq", Long.class);

    public final StringPath pathUrl = createString("pathUrl");

    public QFrontPath(String variable) {
        super(FrontPath.class, forVariable(variable));
    }

    public QFrontPath(Path<? extends FrontPath> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFrontPath(PathMetadata metadata) {
        super(FrontPath.class, metadata);
    }

}

