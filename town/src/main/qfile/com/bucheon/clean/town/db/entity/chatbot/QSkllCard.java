package com.bucheon.clean.town.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSkllCard is a Querydsl query type for SkllCard
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSkllCard extends EntityPathBase<SkllCard> {

    private static final long serialVersionUID = -1262942422L;

    public static final QSkllCard skllCard = new QSkllCard("skllCard");

    public final NumberPath<Integer> bcOrdr = createNumber("bcOrdr", Integer.class);

    public final StringPath btton = createString("btton");

    public final StringPath bttonOptnAt = createString("bttonOptnAt");

    public final StringPath dc = createString("dc");

    public final StringPath profl = createString("profl");

    public final StringPath proflCrtfcAt = createString("proflCrtfcAt");

    public final NumberPath<Integer> rspnsOrdr = createNumber("rspnsOrdr", Integer.class);

    public final StringPath sj = createString("sj");

    public final StringPath skllCode = createString("skllCode");

    public final StringPath socty = createString("socty");

    public final StringPath thumb = createString("thumb");

    public QSkllCard(String variable) {
        super(SkllCard.class, forVariable(variable));
    }

    public QSkllCard(Path<? extends SkllCard> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSkllCard(PathMetadata metadata) {
        super(SkllCard.class, metadata);
    }

}

