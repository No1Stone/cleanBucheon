package com.bucheon.clean.town.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSkllImage is a Querydsl query type for SkllImage
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSkllImage extends EntityPathBase<SkllImage> {

    private static final long serialVersionUID = -490626943L;

    public static final QSkllImage skllImage = new QSkllImage("skllImage");

    public final StringPath imageDc = createString("imageDc");

    public final StringPath imageUrl = createString("imageUrl");

    public final NumberPath<Integer> rspnsOrdr = createNumber("rspnsOrdr", Integer.class);

    public final StringPath skllCode = createString("skllCode");

    public QSkllImage(String variable) {
        super(SkllImage.class, forVariable(variable));
    }

    public QSkllImage(Path<? extends SkllImage> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSkllImage(PathMetadata metadata) {
        super(SkllImage.class, metadata);
    }

}

