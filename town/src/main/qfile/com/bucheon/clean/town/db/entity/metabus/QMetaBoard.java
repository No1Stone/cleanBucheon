package com.bucheon.clean.town.db.entity.metabus;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMetaBoard is a Querydsl query type for MetaBoard
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMetaBoard extends EntityPathBase<MetaBoard> {

    private static final long serialVersionUID = -1863322133L;

    public static final QMetaBoard metaBoard = new QMetaBoard("metaBoard");

    public final StringPath boardContent = createString("boardContent");

    public final NumberPath<Long> boardSeq = createNumber("boardSeq", Long.class);

    public final StringPath boardTitle = createString("boardTitle");

    public final StringPath boardType = createString("boardType");

    public final StringPath delYn = createString("delYn");

    public final DateTimePath<java.time.LocalDateTime> edate = createDateTime("edate", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modSeq = createNumber("modSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regSeq = createNumber("regSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> sdate = createDateTime("sdate", java.time.LocalDateTime.class);

    public final StringPath useYn = createString("useYn");

    public final NumberPath<Integer> vote1 = createNumber("vote1", Integer.class);

    public final NumberPath<Integer> vote2 = createNumber("vote2", Integer.class);

    public final NumberPath<Integer> vote3 = createNumber("vote3", Integer.class);

    public QMetaBoard(String variable) {
        super(MetaBoard.class, forVariable(variable));
    }

    public QMetaBoard(Path<? extends MetaBoard> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMetaBoard(PathMetadata metadata) {
        super(MetaBoard.class, metadata);
    }

}

