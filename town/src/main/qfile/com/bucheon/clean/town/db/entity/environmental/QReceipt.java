package com.bucheon.clean.town.db.entity.environmental;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QReceipt is a Querydsl query type for Receipt
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QReceipt extends EntityPathBase<Receipt> {

    private static final long serialVersionUID = -2060312283L;

    public static final QReceipt receipt = new QReceipt("receipt");

    public final StringPath ancId = createString("ancId");

    public final StringPath ancName = createString("ancName");

    public final StringPath ancRegDateTime = createString("ancRegDateTime");

    public final StringPath busiBranCode = createString("busiBranCode");

    public final StringPath busiBranCodeName = createString("busiBranCodeName");

    public final StringPath civilNo = createString("civilNo");

    public final StringPath petiNo = createString("petiNo");

    public final StringPath regDateTime = createString("regDateTime");

    public QReceipt(String variable) {
        super(Receipt.class, forVariable(variable));
    }

    public QReceipt(Path<? extends Receipt> path) {
        super(path.getType(), path.getMetadata());
    }

    public QReceipt(PathMetadata metadata) {
        super(Receipt.class, metadata);
    }

}

