package com.bucheon.clean.town.db.entity.operation;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPersonalInfoLog is a Querydsl query type for PersonalInfoLog
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPersonalInfoLog extends EntityPathBase<PersonalInfoLog> {

    private static final long serialVersionUID = 365780908L;

    public static final QPersonalInfoLog personalInfoLog = new QPersonalInfoLog("personalInfoLog");

    public final NumberPath<Long> adminSeq = createNumber("adminSeq", Long.class);

    public final StringPath did = createString("did");

    public final NumberPath<Long> logSeq = createNumber("logSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> logTime = createDateTime("logTime", java.time.LocalDateTime.class);

    public final StringPath menuHanName = createString("menuHanName");

    public final StringPath menuName = createString("menuName");

    public final StringPath reason = createString("reason");

    public final NumberPath<Long> seq = createNumber("seq", Long.class);

    public QPersonalInfoLog(String variable) {
        super(PersonalInfoLog.class, forVariable(variable));
    }

    public QPersonalInfoLog(Path<? extends PersonalInfoLog> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPersonalInfoLog(PathMetadata metadata) {
        super(PersonalInfoLog.class, metadata);
    }

}

