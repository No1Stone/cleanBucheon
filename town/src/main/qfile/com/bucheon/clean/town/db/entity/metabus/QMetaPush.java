package com.bucheon.clean.town.db.entity.metabus;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMetaPush is a Querydsl query type for MetaPush
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMetaPush extends EntityPathBase<MetaPush> {

    private static final long serialVersionUID = 1879978869L;

    public static final QMetaPush metaPush = new QMetaPush("metaPush");

    public final StringPath body = createString("body");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regSeq = createNumber("regSeq", Long.class);

    public final StringPath resultType = createString("resultType");

    public final NumberPath<Integer> sendCnt = createNumber("sendCnt", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> sendDt = createDateTime("sendDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> sendSeq = createNumber("sendSeq", Long.class);

    public final StringPath sendType = createString("sendType");

    public final StringPath title = createString("title");

    public QMetaPush(String variable) {
        super(MetaPush.class, forVariable(variable));
    }

    public QMetaPush(Path<? extends MetaPush> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMetaPush(PathMetadata metadata) {
        super(MetaPush.class, metadata);
    }

}

