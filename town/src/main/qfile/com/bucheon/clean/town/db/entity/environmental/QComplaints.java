package com.bucheon.clean.town.db.entity.environmental;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QComplaints is a Querydsl query type for Complaints
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QComplaints extends EntityPathBase<Complaints> {

    private static final long serialVersionUID = -134879461L;

    public static final QComplaints complaints = new QComplaints("complaints");

    public final StringPath boardSeq = createString("boardSeq");

    public final StringPath complaintsContent = createString("complaintsContent");

    public final StringPath complaintsDt = createString("complaintsDt");

    public final StringPath complaintsIn_charge = createString("complaintsIn_charge");

    public final StringPath complaintsStatus = createString("complaintsStatus");

    public final StringPath complaintsTitle = createString("complaintsTitle");

    public final StringPath complaintsType = createString("complaintsType");

    public QComplaints(String variable) {
        super(Complaints.class, forVariable(variable));
    }

    public QComplaints(Path<? extends Complaints> path) {
        super(path.getType(), path.getMetadata());
    }

    public QComplaints(PathMetadata metadata) {
        super(Complaints.class, metadata);
    }

}

