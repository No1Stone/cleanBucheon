package com.bucheon.clean.town.db.entity.metabus;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMetaBoardAtt is a Querydsl query type for MetaBoardAtt
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMetaBoardAtt extends EntityPathBase<MetaBoardAtt> {

    private static final long serialVersionUID = -2072264522L;

    public static final QMetaBoardAtt metaBoardAtt = new QMetaBoardAtt("metaBoardAtt");

    public final StringPath boardAtt = createString("boardAtt");

    public final NumberPath<Integer> boardAttOrder = createNumber("boardAttOrder", Integer.class);

    public final StringPath boardAttOri = createString("boardAttOri");

    public final StringPath boardAttPath = createString("boardAttPath");

    public final NumberPath<Long> boardAttSeq = createNumber("boardAttSeq", Long.class);

    public final NumberPath<Long> boardAttSize = createNumber("boardAttSize", Long.class);

    public final StringPath boardAttType = createString("boardAttType");

    public final NumberPath<Long> boardSeq = createNumber("boardSeq", Long.class);

    public QMetaBoardAtt(String variable) {
        super(MetaBoardAtt.class, forVariable(variable));
    }

    public QMetaBoardAtt(Path<? extends MetaBoardAtt> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMetaBoardAtt(PathMetadata metadata) {
        super(MetaBoardAtt.class, metadata);
    }

}

