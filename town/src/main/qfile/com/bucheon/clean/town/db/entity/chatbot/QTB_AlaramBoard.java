package com.bucheon.clean.town.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTB_AlaramBoard is a Querydsl query type for TB_AlaramBoard
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTB_AlaramBoard extends EntityPathBase<TB_AlaramBoard> {

    private static final long serialVersionUID = 112621839L;

    public static final QTB_AlaramBoard tB_AlaramBoard = new QTB_AlaramBoard("tB_AlaramBoard");

    public final StringPath agreePersonInfoCollection = createString("agreePersonInfoCollection");

    public final StringPath agreePersonInfoHandling = createString("agreePersonInfoHandling");

    public final DateTimePath<java.time.LocalDateTime> alaramBoardDt = createDateTime("alaramBoardDt", java.time.LocalDateTime.class);

    public final ListPath<TB_AlaramBoardPoto, QTB_AlaramBoardPoto> alaramBoardPotos = this.<TB_AlaramBoardPoto, QTB_AlaramBoardPoto>createList("alaramBoardPotos", TB_AlaramBoardPoto.class, QTB_AlaramBoardPoto.class, PathInits.DIRECT2);

    public final StringPath bandProcess = createString("bandProcess");

    public final NumberPath<Long> boardSeqId = createNumber("boardSeqId", Long.class);

    public final StringPath chatbotUserId = createString("chatbotUserId");

    public final StringPath contents = createString("contents");

    public final StringPath email = createString("email");

    public final StringPath feedbackDetail = createString("feedbackDetail");

    public final StringPath feedbackStatus = createString("feedbackStatus");

    public final StringPath gridx = createString("gridx");

    public final StringPath gridy = createString("gridy");

    public final StringPath name = createString("name");

    public final StringPath newAddress = createString("newAddress");

    public final StringPath oldAddress = createString("oldAddress");

    public final StringPath phoneNumber = createString("phoneNumber");

    public final StringPath reportType = createString("reportType");

    public QTB_AlaramBoard(String variable) {
        super(TB_AlaramBoard.class, forVariable(variable));
    }

    public QTB_AlaramBoard(Path<? extends TB_AlaramBoard> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTB_AlaramBoard(PathMetadata metadata) {
        super(TB_AlaramBoard.class, metadata);
    }

}

