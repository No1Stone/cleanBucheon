package com.bucheon.clean.town.db.entity.operation;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTrashType is a Querydsl query type for TrashType
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTrashType extends EntityPathBase<TrashType> {

    private static final long serialVersionUID = 1283893608L;

    public static final QTrashType trashType = new QTrashType("trashType");

    public final NumberPath<Integer> declarationMileage = createNumber("declarationMileage", Integer.class);

    public final StringPath delYn = createString("delYn");

    public final StringPath iconUrl = createString("iconUrl");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modSeq = createNumber("modSeq", Long.class);

    public final NumberPath<Integer> processMileage = createNumber("processMileage", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regSeq = createNumber("regSeq", Long.class);

    public final StringPath trashName = createString("trashName");

    public final NumberPath<Long> trashSeq = createNumber("trashSeq", Long.class);

    public final StringPath useYn = createString("useYn");

    public QTrashType(String variable) {
        super(TrashType.class, forVariable(variable));
    }

    public QTrashType(Path<? extends TrashType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTrashType(PathMetadata metadata) {
        super(TrashType.class, metadata);
    }

}

