package com.bucheon.clean.town.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSkllMng is a Querydsl query type for SkllMng
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSkllMng extends EntityPathBase<SkllMng> {

    private static final long serialVersionUID = -733466740L;

    public static final QSkllMng skllMng = new QSkllMng("skllMng");

    public final StringPath devlopId = createString("devlopId");

    public final StringPath operId = createString("operId");

    public final StringPath skllCode = createString("skllCode");

    public final StringPath skllNm = createString("skllNm");

    public QSkllMng(String variable) {
        super(SkllMng.class, forVariable(variable));
    }

    public QSkllMng(Path<? extends SkllMng> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSkllMng(PathMetadata metadata) {
        super(SkllMng.class, metadata);
    }

}

