package com.bucheon.clean.town.db.entity.etc;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDongDamdang is a Querydsl query type for DongDamdang
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDongDamdang extends EntityPathBase<DongDamdang> {

    private static final long serialVersionUID = -351302107L;

    public static final QDongDamdang dongDamdang = new QDongDamdang("dongDamdang");

    public final NumberPath<Long> damdang = createNumber("damdang", Long.class);

    public final StringPath dong = createString("dong");

    public QDongDamdang(String variable) {
        super(DongDamdang.class, forVariable(variable));
    }

    public QDongDamdang(Path<? extends DongDamdang> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDongDamdang(PathMetadata metadata) {
        super(DongDamdang.class, metadata);
    }

}

