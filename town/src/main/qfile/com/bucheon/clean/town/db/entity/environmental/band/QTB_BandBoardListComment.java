package com.bucheon.clean.town.db.entity.environmental.band;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTB_BandBoardListComment is a Querydsl query type for TB_BandBoardListComment
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTB_BandBoardListComment extends EntityPathBase<TB_BandBoardListComment> {

    private static final long serialVersionUID = -524324581L;

    public static final QTB_BandBoardListComment tB_BandBoardListComment = new QTB_BandBoardListComment("tB_BandBoardListComment");

    public final StringPath body = createString("body");

    public final StringPath commentKey = createString("commentKey");

    public final NumberPath<Long> createdAt = createNumber("createdAt", Long.class);

    public final StringPath description = createString("description");

    public final NumberPath<Integer> height = createNumber("height", Integer.class);

    public final StringPath name = createString("name");

    public final StringPath postKey = createString("postKey");

    public final StringPath profileImageUrl = createString("profileImageUrl");

    public final StringPath url = createString("url");

    public final StringPath userKey = createString("userKey");

    public final NumberPath<Integer> width = createNumber("width", Integer.class);

    public QTB_BandBoardListComment(String variable) {
        super(TB_BandBoardListComment.class, forVariable(variable));
    }

    public QTB_BandBoardListComment(Path<? extends TB_BandBoardListComment> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTB_BandBoardListComment(PathMetadata metadata) {
        super(TB_BandBoardListComment.class, metadata);
    }

}

