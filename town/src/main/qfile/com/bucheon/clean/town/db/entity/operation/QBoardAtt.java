package com.bucheon.clean.town.db.entity.operation;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBoardAtt is a Querydsl query type for BoardAtt
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBoardAtt extends EntityPathBase<BoardAtt> {

    private static final long serialVersionUID = 1891016773L;

    public static final QBoardAtt boardAtt1 = new QBoardAtt("boardAtt1");

    public final StringPath boardAtt = createString("boardAtt");

    public final StringPath boardAttOri = createString("boardAttOri");

    public final StringPath boardAttPath = createString("boardAttPath");

    public final NumberPath<Long> boardAttSeq = createNumber("boardAttSeq", Long.class);

    public final NumberPath<Long> boardAttSize = createNumber("boardAttSize", Long.class);

    public final StringPath boardAttType = createString("boardAttType");

    public final NumberPath<Long> boardSeq = createNumber("boardSeq", Long.class);

    public QBoardAtt(String variable) {
        super(BoardAtt.class, forVariable(variable));
    }

    public QBoardAtt(Path<? extends BoardAtt> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBoardAtt(PathMetadata metadata) {
        super(BoardAtt.class, metadata);
    }

}

