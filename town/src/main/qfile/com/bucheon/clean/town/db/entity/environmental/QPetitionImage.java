package com.bucheon.clean.town.db.entity.environmental;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPetitionImage is a Querydsl query type for PetitionImage
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPetitionImage extends EntityPathBase<PetitionImage> {

    private static final long serialVersionUID = -1054754838L;

    public static final QPetitionImage petitionImage = new QPetitionImage("petitionImage");

    public final StringPath content = createString("content");

    public final StringPath fileName = createString("fileName");

    public final StringPath petiNo = createString("petiNo");

    public final StringPath uuid = createString("uuid");

    public QPetitionImage(String variable) {
        super(PetitionImage.class, forVariable(variable));
    }

    public QPetitionImage(Path<? extends PetitionImage> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPetitionImage(PathMetadata metadata) {
        super(PetitionImage.class, metadata);
    }

}

