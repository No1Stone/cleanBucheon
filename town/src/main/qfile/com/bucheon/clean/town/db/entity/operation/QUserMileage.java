package com.bucheon.clean.town.db.entity.operation;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QUserMileage is a Querydsl query type for UserMileage
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserMileage extends EntityPathBase<UserMileage> {

    private static final long serialVersionUID = 1186067413L;

    public static final QUserMileage userMileage = new QUserMileage("userMileage");

    public final NumberPath<Long> mileageSeq = createNumber("mileageSeq", Long.class);

    public final NumberPath<Integer> mileageSum = createNumber("mileageSum", Integer.class);

    public final StringPath userId = createString("userId");

    public QUserMileage(String variable) {
        super(UserMileage.class, forVariable(variable));
    }

    public QUserMileage(Path<? extends UserMileage> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUserMileage(PathMetadata metadata) {
        super(UserMileage.class, metadata);
    }

}

