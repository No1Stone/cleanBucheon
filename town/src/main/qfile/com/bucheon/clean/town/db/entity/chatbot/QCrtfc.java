package com.bucheon.clean.town.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCrtfc is a Querydsl query type for Crtfc
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCrtfc extends EntityPathBase<Crtfc> {

    private static final long serialVersionUID = 1084116608L;

    public static final QCrtfc crtfc = new QCrtfc("crtfc");

    public final StringPath authtype = createString("authtype");

    public final StringPath birthdate = createString("birthdate");

    public final StringPath ci = createString("ci");

    public final DatePath<java.time.LocalDate> crtfcDe = createDate("crtfcDe", java.time.LocalDate.class);

    public final StringPath di = createString("di");

    public final StringPath enctime = createString("enctime");

    public final StringPath gender = createString("gender");

    public final StringPath mobileNo = createString("mobileNo");

    public final StringPath name = createString("name");

    public final StringPath nationalinfo = createString("nationalinfo");

    public final StringPath sitecode = createString("sitecode");

    public final StringPath userId = createString("userId");

    public QCrtfc(String variable) {
        super(Crtfc.class, forVariable(variable));
    }

    public QCrtfc(Path<? extends Crtfc> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCrtfc(PathMetadata metadata) {
        super(Crtfc.class, metadata);
    }

}

