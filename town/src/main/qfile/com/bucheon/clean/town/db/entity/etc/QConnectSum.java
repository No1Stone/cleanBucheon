package com.bucheon.clean.town.db.entity.etc;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QConnectSum is a Querydsl query type for ConnectSum
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QConnectSum extends EntityPathBase<ConnectSum> {

    private static final long serialVersionUID = -1938162466L;

    public static final QConnectSum connectSum = new QConnectSum("connectSum");

    public final NumberPath<Integer> band = createNumber("band", Integer.class);

    public final NumberPath<Integer> chatbot = createNumber("chatbot", Integer.class);

    public final NumberPath<Integer> cleanApp = createNumber("cleanApp", Integer.class);

    public final DatePath<java.time.LocalDate> connectDate = createDate("connectDate", java.time.LocalDate.class);

    public final NumberPath<Integer> metabus = createNumber("metabus", Integer.class);

    public final NumberPath<Integer> self = createNumber("self", Integer.class);

    public QConnectSum(String variable) {
        super(ConnectSum.class, forVariable(variable));
    }

    public QConnectSum(Path<? extends ConnectSum> path) {
        super(path.getType(), path.getMetadata());
    }

    public QConnectSum(PathMetadata metadata) {
        super(ConnectSum.class, metadata);
    }

}

