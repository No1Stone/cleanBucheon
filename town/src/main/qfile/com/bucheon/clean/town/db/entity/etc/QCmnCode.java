package com.bucheon.clean.town.db.entity.etc;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCmnCode is a Querydsl query type for CmnCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCmnCode extends EntityPathBase<CmnCode> {

    private static final long serialVersionUID = 978708116L;

    public static final QCmnCode cmnCode = new QCmnCode("cmnCode");

    public final StringPath code = createString("code");

    public final NumberPath<Long> codeSeq = createNumber("codeSeq", Long.class);

    public final StringPath column = createString("column");

    public final StringPath name = createString("name");

    public QCmnCode(String variable) {
        super(CmnCode.class, forVariable(variable));
    }

    public QCmnCode(Path<? extends CmnCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCmnCode(PathMetadata metadata) {
        super(CmnCode.class, metadata);
    }

}

