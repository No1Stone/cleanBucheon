package com.bucheon.clean.town.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSkllRply is a Querydsl query type for SkllRply
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSkllRply extends EntityPathBase<SkllRply> {

    private static final long serialVersionUID = -1262481307L;

    public static final QSkllRply skllRply = new QSkllRply("skllRply");

    public final StringPath actn = createString("actn");

    public final StringPath blckOrginlId = createString("blckOrginlId");

    public final StringPath lbl = createString("lbl");

    public final NumberPath<Integer> qrOrdr = createNumber("qrOrdr", Integer.class);

    public final StringPath questMessage = createString("questMessage");

    public final StringPath skllCode = createString("skllCode");

    public QSkllRply(String variable) {
        super(SkllRply.class, forVariable(variable));
    }

    public QSkllRply(Path<? extends SkllRply> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSkllRply(PathMetadata metadata) {
        super(SkllRply.class, metadata);
    }

}

