package com.bucheon.clean.town.db.entity.operation;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QUserMileageHistory is a Querydsl query type for UserMileageHistory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserMileageHistory extends EntityPathBase<UserMileageHistory> {

    private static final long serialVersionUID = -963019009L;

    public static final QUserMileageHistory userMileageHistory = new QUserMileageHistory("userMileageHistory");

    public final NumberPath<Long> adminSeq = createNumber("adminSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> complaintsDt = createDateTime("complaintsDt", java.time.LocalDateTime.class);

    public final NumberPath<Integer> mileage = createNumber("mileage", Integer.class);

    public final NumberPath<Long> mileageSeq = createNumber("mileageSeq", Long.class);

    public final StringPath mileageType = createString("mileageType");

    public final StringPath reason = createString("reason");

    public final NumberPath<Long> reportSeq = createNumber("reportSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> sendDate = createDateTime("sendDate", java.time.LocalDateTime.class);

    public final StringPath userId = createString("userId");

    public QUserMileageHistory(String variable) {
        super(UserMileageHistory.class, forVariable(variable));
    }

    public QUserMileageHistory(Path<? extends UserMileageHistory> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUserMileageHistory(PathMetadata metadata) {
        super(UserMileageHistory.class, metadata);
    }

}

