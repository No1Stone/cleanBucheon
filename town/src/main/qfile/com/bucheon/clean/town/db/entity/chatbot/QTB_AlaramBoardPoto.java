package com.bucheon.clean.town.db.entity.chatbot;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTB_AlaramBoardPoto is a Querydsl query type for TB_AlaramBoardPoto
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTB_AlaramBoardPoto extends EntityPathBase<TB_AlaramBoardPoto> {

    private static final long serialVersionUID = 1707828841L;

    public static final QTB_AlaramBoardPoto tB_AlaramBoardPoto = new QTB_AlaramBoardPoto("tB_AlaramBoardPoto");

    public final NumberPath<Long> boardSeqId = createNumber("boardSeqId", Long.class);

    public final StringPath classUrl = createString("classUrl");

    public final StringPath fileName = createString("fileName");

    public final StringPath fileSize = createString("fileSize");

    public final StringPath fileType = createString("fileType");

    public QTB_AlaramBoardPoto(String variable) {
        super(TB_AlaramBoardPoto.class, forVariable(variable));
    }

    public QTB_AlaramBoardPoto(Path<? extends TB_AlaramBoardPoto> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTB_AlaramBoardPoto(PathMetadata metadata) {
        super(TB_AlaramBoardPoto.class, metadata);
    }

}

