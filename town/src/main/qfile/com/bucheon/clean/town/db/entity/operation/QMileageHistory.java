package com.bucheon.clean.town.db.entity.operation;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMileageHistory is a Querydsl query type for MileageHistory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMileageHistory extends EntityPathBase<MileageHistory> {

    private static final long serialVersionUID = 1258769236L;

    public static final QMileageHistory mileageHistory = new QMileageHistory("mileageHistory");

    public final NumberPath<Long> adminSeq = createNumber("adminSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> complaintsDt = createDateTime("complaintsDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> mileage = createNumber("mileage", Long.class);

    public final NumberPath<Long> mileageSeq = createNumber("mileageSeq", Long.class);

    public final StringPath mileageType = createString("mileageType");

    public final StringPath reportSeq = createString("reportSeq");

    public final DateTimePath<java.time.LocalDateTime> sendDate = createDateTime("sendDate", java.time.LocalDateTime.class);

    public final StringPath userId = createString("userId");

    public QMileageHistory(String variable) {
        super(MileageHistory.class, forVariable(variable));
    }

    public QMileageHistory(Path<? extends MileageHistory> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMileageHistory(PathMetadata metadata) {
        super(MileageHistory.class, metadata);
    }

}

