package com.bucheon.clean.town.db.entity.environmental.band;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTB_BandBoardList is a Querydsl query type for TB_BandBoardList
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTB_BandBoardList extends EntityPathBase<TB_BandBoardList> {

    private static final long serialVersionUID = -268872540L;

    public static final QTB_BandBoardList tB_BandBoardList = new QTB_BandBoardList("tB_BandBoardList");

    public final StringPath bandKey = createString("bandKey");

    public final StringPath bandName = createString("bandName");

    public final NumberPath<Integer> commentCount = createNumber("commentCount", Integer.class);

    public final StringPath content = createString("content");

    public final NumberPath<Long> createdAt = createNumber("createdAt", Long.class);

    public final StringPath description = createString("description");

    public final NumberPath<Integer> emotionCount = createNumber("emotionCount", Integer.class);

    public final StringPath name = createString("name");

    public final StringPath postKey = createString("postKey");

    public final StringPath processStatus = createString("processStatus");

    public final StringPath profileImageUrl = createString("profileImageUrl");

    public final StringPath role = createString("role");

    public final StringPath userKey = createString("userKey");

    public QTB_BandBoardList(String variable) {
        super(TB_BandBoardList.class, forVariable(variable));
    }

    public QTB_BandBoardList(Path<? extends TB_BandBoardList> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTB_BandBoardList(PathMetadata metadata) {
        super(TB_BandBoardList.class, metadata);
    }

}

