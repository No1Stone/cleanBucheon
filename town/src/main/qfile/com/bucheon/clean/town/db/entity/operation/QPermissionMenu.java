package com.bucheon.clean.town.db.entity.operation;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPermissionMenu is a Querydsl query type for PermissionMenu
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPermissionMenu extends EntityPathBase<PermissionMenu> {

    private static final long serialVersionUID = 726111928L;

    public static final QPermissionMenu permissionMenu = new QPermissionMenu("permissionMenu");

    public final NumberPath<Long> menuSeq = createNumber("menuSeq", Long.class);

    public final NumberPath<Long> permissionSeq = createNumber("permissionSeq", Long.class);

    public final StringPath useYn = createString("useYn");

    public QPermissionMenu(String variable) {
        super(PermissionMenu.class, forVariable(variable));
    }

    public QPermissionMenu(Path<? extends PermissionMenu> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPermissionMenu(PathMetadata metadata) {
        super(PermissionMenu.class, metadata);
    }

}

