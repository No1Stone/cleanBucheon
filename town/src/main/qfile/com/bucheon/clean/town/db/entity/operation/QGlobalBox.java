package com.bucheon.clean.town.db.entity.operation;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QGlobalBox is a Querydsl query type for GlobalBox
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QGlobalBox extends EntityPathBase<GlobalBox> {

    private static final long serialVersionUID = 486689278L;

    public static final QGlobalBox globalBox = new QGlobalBox("globalBox");

    public final StringPath boardTitle = createString("boardTitle");

    public final NumberPath<Long> boxSeq = createNumber("boxSeq", Long.class);

    public final StringPath delYn = createString("delYn");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modSeq = createNumber("modSeq", Long.class);

    public final NumberPath<Long> pathSeq = createNumber("pathSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regSeq = createNumber("regSeq", Long.class);

    public final StringPath useYn = createString("useYn");

    public QGlobalBox(String variable) {
        super(GlobalBox.class, forVariable(variable));
    }

    public QGlobalBox(Path<? extends GlobalBox> path) {
        super(path.getType(), path.getMetadata());
    }

    public QGlobalBox(PathMetadata metadata) {
        super(GlobalBox.class, metadata);
    }

}

