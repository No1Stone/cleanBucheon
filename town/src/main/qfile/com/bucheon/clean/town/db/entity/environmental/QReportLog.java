package com.bucheon.clean.town.db.entity.environmental;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QReportLog is a Querydsl query type for ReportLog
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QReportLog extends EntityPathBase<ReportLog> {

    private static final long serialVersionUID = -1032847075L;

    public static final QReportLog reportLog = new QReportLog("reportLog");

    public final NumberPath<Long> logSeq = createNumber("logSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> logTime = createDateTime("logTime", java.time.LocalDateTime.class);

    public final StringPath msg = createString("msg");

    public final StringPath receptionNum = createString("receptionNum");

    public final NumberPath<Long> reportDamdang = createNumber("reportDamdang", Long.class);

    public final StringPath reportPointRejectReason = createString("reportPointRejectReason");

    public final NumberPath<Long> reportSeq = createNumber("reportSeq", Long.class);

    public final StringPath reportStatus = createString("reportStatus");

    public QReportLog(String variable) {
        super(ReportLog.class, forVariable(variable));
    }

    public QReportLog(Path<? extends ReportLog> path) {
        super(path.getType(), path.getMetadata());
    }

    public QReportLog(PathMetadata metadata) {
        super(ReportLog.class, metadata);
    }

}

