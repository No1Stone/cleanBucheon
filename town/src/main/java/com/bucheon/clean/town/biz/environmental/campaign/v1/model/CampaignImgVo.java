package com.bucheon.clean.town.biz.environmental.campaign.v1.model;

import com.bucheon.clean.town.db.entity.environmental.CampaignImg;
import com.bucheon.clean.town.db.entity.environmental.CampaignImg;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CampaignImgVo {

    private Long campaignSeq;
    private String campaignImg;

    public CampaignImg ofCampaignImg(){
        CampaignImg r = new CampaignImg();
        r.setCampaignSeq(this.campaignSeq);
        r.setCampaignImg(this.campaignImg);
        return r;
    }

}
