package com.bucheon.clean.town.biz.operation.board.v1.service;

import com.bucheon.clean.town.biz.operation.board.v1.model.GlobalBoxReq;
import com.bucheon.clean.town.biz.operation.board.v1.model.GlobalBoxRes;
import com.bucheon.clean.town.biz.operation.board.v1.model.GlobalBoxVO;
import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.FrontPath;
import com.bucheon.clean.town.db.entity.operation.GlobalBox;
import com.bucheon.clean.town.db.repository.operation.AdminRepository;
import com.bucheon.clean.town.db.repository.operation.FrontPathRepository;
import com.bucheon.clean.town.db.repository.operation.GlobalBoxRepository;
import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GlobalBoxService {

    private final Logger logger = LoggerFactory.getLogger(GlobalBoxService.class);
    private final HttpServletRequest httpServletRequest;
    private final GlobalBoxRepository globalBoxRepository;
    private final AdminRepository adminRepository;
    private final FrontPathRepository frontPathRepository;
    private final ModelMapper modelMapper;


    public List<GlobalBoxRes> GlobalBoxSelectListService(GlobalBoxReq dto) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        logger.info("dto  - - - -{}", new Gson().toJson(dto));
        Map<Long, String> tt = adminRepository.findAll()
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
        //logger.info("ttmap - {}", tt);
        //logger.info("getSessionAdministrativeDivision - {}", dto.getSessionAdministrativeDivision());
        List<GlobalBoxRes> boardRes = globalBoxRepository.SelectListGlobalBox(dto)
                .stream().map(e -> modelMapper.map(e, GlobalBoxRes.class))
                .peek(f -> {

                    String pathName = "";
                    String pathUrl = "";
                    try {
                        FrontPath fp = frontPathRepository.findByPathSeq(f.getPathSeq()).orElseThrow(IllegalArgumentException::new);
                        logger.info("fp - {}", new Gson().toJson(fp));
                        pathName = fp.getPathName();
                        pathUrl = fp.getPathUrl();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    f.setPathName(pathName);
                    f.setPathUrl(pathUrl);
                    f.setRegName(tt.get(f.getRegSeq()));
                    f.setModName(tt.get(f.getModSeq()));
                })
                .collect(Collectors.toList());
        return boardRes;
    }

    public Paging GlobalBoxSelectListPagingInfoService(GlobalBoxReq dto) {
        QueryResults<GlobalBox> result = globalBoxRepository.SelectListGlobalBoxPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public GlobalBoxRes GlobalBoxSelectOneService(Long seq) {
        Map<Long, String> tt = adminRepository.findAll()
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
        logger.info(">>>>>>>>>>>>>>>>>>>>> tt : " + tt);

        GlobalBoxRes result = modelMapper.map(globalBoxRepository.findByBoxSeqAndDelYn(seq, "N").orElseThrow(IllegalArgumentException::new), GlobalBoxRes.class);
        result.setRegName(tt.get(result.getRegSeq()));
        result.setModName(tt.get(result.getModSeq()));

        return result;
    }

    @Transactional
    public int GlobalBoxUpdateDelService(Long[] boardSeql) {
        List<Integer> result = Arrays.stream(boardSeql).map(e -> globalBoxRepository
                .UpdateBoxDel(YnType.Y.getName(), e)).collect(Collectors.toList());
        return result.size();
    }

    @Transactional
    public int GlobalBoxUpdateService(GlobalBoxVO dto) {
        int result = 0;
        if (dto.getBoxSeq() != null) {
            globalBoxRepository.UpdateTitle(dto.getBoardTitle(), dto.getBoxSeq());
            globalBoxRepository.UpdatePathSeq(dto.getPathSeq(), dto.getBoxSeq());
            globalBoxRepository.UpdateBoxUse(dto.getUseYn(), dto.getBoxSeq());

            result++;
        }
        return result;
    }

    @Transactional
    public GlobalBox GlobalBoxSaveService(GlobalBoxVO dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;

        if (dto.getBoxSeq() == null) {
            dto.setRegSeq(admin.getAdminSeq());
        }
        if (dto.getDelYn() == null) {
            dto.setDelYn("N");
        }
        dto.setModSeq(admin.getAdminSeq());

        GlobalBox box = globalBoxRepository.save(dto.ofBox());

        return box;
    }
    
    public int GlobalBoxCountOneService(Long seq) {
        return globalBoxRepository.countByBoxSeqAndDelYn(seq, "N");
    }

    public List<FrontPath> FrontPathSelectListService(){
        return frontPathRepository.findAll();
    }

}
