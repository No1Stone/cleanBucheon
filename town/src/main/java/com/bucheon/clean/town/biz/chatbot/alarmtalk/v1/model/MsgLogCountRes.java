package com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model;

public interface MsgLogCountRes {

    int getTotal();
    int getSuccess();
    int getSucessRate();
    int getFail();
    int getFailRate();
    int getKakao();
    int getKakaoRate();
    int getSms();
    int getSmsRate();

}
