package com.bucheon.clean.town.biz.keyword.v1.model;

import lombok.Data;

@Data
public class WordReq {

    private int page;
    private int size;
    private int totalSize;
    private String searchState;
    private String searchCode;
    private String searchWord1;
    private String searchWord2;
    private String searchWord3;
    private String searchWord4;

}
