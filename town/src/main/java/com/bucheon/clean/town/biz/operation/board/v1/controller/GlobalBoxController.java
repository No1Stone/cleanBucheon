package com.bucheon.clean.town.biz.operation.board.v1.controller;

import com.bucheon.clean.town.biz.operation.board.v1.model.GlobalBoxReq;
import com.bucheon.clean.town.biz.operation.board.v1.model.GlobalBoxRes;
import com.bucheon.clean.town.biz.operation.board.v1.model.GlobalBoxVO;
import com.bucheon.clean.town.biz.operation.board.v1.service.GlobalBoxService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.GlobalBox;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.HashMap;

@Controller
@RequestMapping(path = "/operation/globalbox")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class GlobalBoxController {

    private final Logger logger = LoggerFactory.getLogger(GlobalBoxController.class);
    private final GlobalBoxService globalBoxService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        return new ModelAndView("redirect:/board/globalbox/info");
    }

    //기본틀 
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(GlobalBoxReq dto) throws IllegalStateException, Exception {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        String pageTitle = "";
        MenuRes menu = menuService.MenuSelectOneService(44L);

        return new ModelAndView("operation/globalbox/boxManage")
                .addObject("boxPaging", globalBoxService.GlobalBoxSelectListPagingInfoService(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(GlobalBoxReq dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        String sessionAdministrativeDivision = ((Admin) principal).getAdministrativeDivision();

        return new ModelAndView("operation/globalbox/boxListAjax")
                .addObject("boxList", globalBoxService.GlobalBoxSelectListService(dto))
                .addObject("boxPaging", globalBoxService.GlobalBoxSelectListPagingInfoService(dto))
                ;
    }

    //최초 입력 페이지 팝업 - 필요없음
    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm(String boardType) {

        GlobalBoxRes box = new GlobalBoxRes();

        return new ModelAndView("operation/globalbox/boxManagePopupAjax")
                .addObject("boxOne", box)
                .addObject("frontPathList", globalBoxService.FrontPathSelectListService())
                ;
    }

    //상세보기 팝업
    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView viewForm(Long seq) {

        GlobalBoxRes box = globalBoxService.GlobalBoxSelectOneService(seq);

        return new ModelAndView("operation/globalbox/boxManagePopupAjax")
                .addObject("boxOne", box)
                .addObject("frontPathList", globalBoxService.FrontPathSelectListService())
                ;
    }

    //저장처리
    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(GlobalBoxVO dto) throws IllegalStateException, IOException {

        HashMap<String, Object> map = new HashMap<String, Object>();

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        try {
            GlobalBox box = globalBoxService.GlobalBoxSaveService(dto);
            map.put("result", "success");
        } catch (Exception e) {
            map.put("result", "fail");
        }

        return map;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(GlobalBoxVO dto) {

        HashMap<String, Object> map = new HashMap<String, Object>();

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        //내용 업데이트
        try {
            globalBoxService.GlobalBoxUpdateService(dto);
            map.put("result", "success");
        } catch (Exception e) {
            map.put("result", "fail");
        }

        return map;
    }

    //삭제처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(Long[] boxSeq) {

        HashMap<String, Object> map = new HashMap<String, Object>();

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        try {
            globalBoxService.GlobalBoxUpdateDelService(boxSeq);
            map.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "fail");
        }
        return map;
    }


}
