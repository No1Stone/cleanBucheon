package com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model;

import com.bucheon.clean.town.db.entity.environmental.ReportLog;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AlaramBoardRes {
    private Long boardSeqId;
    private String phoneNumber;
    private String reportType;

    //
    private String receptionNum;
    private String reportName;
    private String reportImg;

    private String newAddress;
    private String oldAddress;
    private String gridx;
    private String gridy;
    private String contents;
    private LocalDateTime alaramBoardDt;
    private String bandProcess;
    private String feedbackStatus;
    private String feedbackDetail;
    private String reportStatus;
    private String reportStatusName;
    private String reportDamdangName;
    private LocalDateTime comDt;
    private String email;
    private String name;
    private List<ChatbotPoto> chatbotPotos;
    private List<ReportLog> reportLogs;


}
