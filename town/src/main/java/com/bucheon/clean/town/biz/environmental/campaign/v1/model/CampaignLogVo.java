package com.bucheon.clean.town.biz.environmental.campaign.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CampaignLogVo {

    private Long logSeq;
    private Long campaignSeq;
    private LocalDateTime logTime;
    private String receptionNum;
    private String campaignStatus;
    private String campaignDamdang;
    private String msg;
    private String rejcetReason;


}
