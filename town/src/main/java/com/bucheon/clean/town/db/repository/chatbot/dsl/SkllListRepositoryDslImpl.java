package com.bucheon.clean.town.db.repository.chatbot.dsl;

import com.bucheon.clean.town.biz.chatbot.answer.v1.model.CommerceCardVO;
import com.bucheon.clean.town.biz.chatbot.answer.v1.model.ListCardVO;
import com.bucheon.clean.town.db.entity.chatbot.QSkll;
import com.bucheon.clean.town.db.entity.chatbot.QSkllCmerc;
import com.bucheon.clean.town.db.entity.chatbot.QSkllList;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class SkllListRepositoryDslImpl implements SkllListRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QSkll qSkll = QSkll.skll;
    private QSkllList qSkllList = QSkllList.skllList;

    @Override
    public ListCardVO selectSkllCcCodeAndRspnsOrdr(String code, int reqSeq) {

        ListCardVO result = queryFactory.select(
                        Projections.bean(ListCardVO.class,
                                qSkllList.skllCode.as("skillCode"),
                                qSkllList.btton.as("button"),
                                qSkllList.hder.as("header"),
                                qSkllList.list.as("items"),
                                qSkllList.rspnsOrdr.as("reqSeq"),
                                qSkllList.bttonOptnAt.as("bttonOptnAt")
                        )
                )
                .from(qSkllList)
                .where(eqId(code), eqRspnsOrder(reqSeq))
                .fetchOne();
        return result;

    }

    private BooleanExpression eqId(String id) {
        if(StringUtils.isEmptyOrWhitespace(id)){
            return null;
        }
        return qSkllList.skllCode.eq(id);
    }

   private BooleanExpression eqRspnsOrder(int reqSeq) {
        if(reqSeq == 0){
            return null;
        }
        return qSkllList.rspnsOrdr.eq(reqSeq);
    }

}
