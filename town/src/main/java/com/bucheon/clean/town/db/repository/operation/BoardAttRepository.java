package com.bucheon.clean.town.db.repository.operation;

import com.bucheon.clean.town.db.entity.operation.BoardAtt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BoardAttRepository extends JpaRepository<BoardAtt, Long> {

    boolean existsByBoardSeq(Long seq);


    List<BoardAtt> findByBoardSeq(Long reSeq);

    int countByBoardSeq(Long seq);

    @Transactional
    int deleteByBoardSeq(Long reSeq);

    @Transactional
    int deleteByBoardAttSeq(Long reSeq);


}
