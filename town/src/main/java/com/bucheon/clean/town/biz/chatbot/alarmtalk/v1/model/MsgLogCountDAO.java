package com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.List;

@Component
public class MsgLogCountDAO {

    private final Logger logger = LoggerFactory.getLogger(MsgLogCountDAO.class);

    @Autowired
    EntityManager em;

    public List<MsgLogCountRes> countMsg (AlaramReq dto){

        String qry = "WITH alram_log_tot as( " +
                "SELECT  " +
                "   MSG_SEQ, " +
                "   MSG_NOTICETALK_TMP_KEY, " +
                "   SENT_DATE, " +
                "   CALL_TO, " +
                "   MSG_PAY_CODE, " +
                "   RSLT_CODE2 " +
                "FROM MSG_LOG_" + dto.getYyyymm() + " " +
                "WHERE SENT_DATE BETWEEN STR_TO_DATE(CONCAT('" + dto.getSdate() + "',' 000000'), '%Y%m%d%H%i%s') AND STR_TO_DATE(CONCAT('" + dto.getEdate() + "',' 235959'), '%Y%m%d%H%i%s') " +
                "       and RSLT_CODE2 is not null " +
                ")," +
                "alram_log_sucess as( " +
                "   SELECT * " +
                "   FROM alram_log_tot " +
                "   WHERE RSLT_CODE2 ='0' " +
                ") " +
                "SELECT  " +
                "   A.total AS total " +
                "   , B.success AS success " +
                "   , ifnull(round(B.success/A.total*100,2),0) AS sucessRate " +
                "   , C.fail AS fail " +
                "   , ifnull(round(C.fail/A.total*100,2),0) AS failRate " +
                "   , D.kakao AS kakao " +
                "   , ifnull(round(D.kakao/B.success*100,2),0) AS kakaoRate " +
                "   , E.sms AS sms " +
                "   , ifnull(round(E.sms/B.success*100,2),0) AS smsRate " +
                "FROM " +
                "   (SELECT count(SENT_DATE) AS total FROM alram_log_tot) A " +
                "   ,(SELECT count(SENT_DATE) AS success FROM alram_log_sucess) B " +
                "   ,(SELECT count(SENT_DATE) AS fail FROM alram_log_tot WHERE RSLT_CODE2 !='0') C " +
                "   ,(SELECT count(SENT_DATE) AS kakao FROM alram_log_sucess WHERE MSG_PAY_CODE ='NOT') D " +
                "   ,(SELECT count(SENT_DATE) AS sms FROM alram_log_sucess WHERE MSG_PAY_CODE ='LMS' OR MSG_PAY_CODE ='MMS') E " +
                ";";
        logger.info("qry : {}", qry);

        //String sql = String.format(qry, yyyymm, sdate, edate);
        //logger.info("sql : {}", sql);

        List<MsgLogCountRes> res = em.createNativeQuery(qry).getResultList();

        return res;
    }
}
