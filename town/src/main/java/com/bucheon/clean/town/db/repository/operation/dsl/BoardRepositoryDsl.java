package com.bucheon.clean.town.db.repository.operation.dsl;


import com.bucheon.clean.town.biz.operation.board.v1.model.BoardReq;
import com.bucheon.clean.town.db.entity.operation.Board;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface BoardRepositoryDsl {

    @Transactional(readOnly = true)
    List<Board> SelectListBoard(BoardReq dto);
    @Transactional(readOnly = true)
    QueryResults<Board> SelectListBoardPaging(BoardReq dto);
    @Transactional(readOnly = true)
    Optional<Board> selectType(String type);

}
