package com.bucheon.clean.town.biz.operation.permission.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PermissionMenuRes {

    private Long permissionSeq;
    private Long menuSeq;
    private String useYn;

}
