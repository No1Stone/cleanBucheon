package com.bucheon.clean.town.db.repository.operation;

import com.bucheon.clean.town.db.entity.operation.MileageHistory;
import com.bucheon.clean.town.db.entity.operation.id.MileageHistoryId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MileageHistoryRepository extends JpaRepository<MileageHistory, MileageHistoryId> {
}
