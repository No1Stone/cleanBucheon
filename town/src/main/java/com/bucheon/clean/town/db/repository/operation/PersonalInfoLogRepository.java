package com.bucheon.clean.town.db.repository.operation;

import com.bucheon.clean.town.db.entity.environmental.ReportLog;
import com.bucheon.clean.town.db.entity.operation.PersonalInfoLog;
import com.bucheon.clean.town.db.repository.operation.dsl.PersonalInfoLogRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PersonalInfoLogRepository extends JpaRepository<PersonalInfoLog, Long> , PersonalInfoLogRepositoryDsl {

    Optional<PersonalInfoLog> findByLogSeq(Long seq);
    Boolean existsByLogSeq(Long seq);
}
