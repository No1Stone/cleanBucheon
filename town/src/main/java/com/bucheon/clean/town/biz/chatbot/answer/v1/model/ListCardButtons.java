package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ListCardButtons {

	/* 스킬 코드 */
	private String skillCode;
	
	private String action;
	
	private String label;
	
	private String link;
	
	private String messageText;
	
	private String webLinkUrl;
	
	private String blockId;
	
	private String phoneNumber;
	
	private String osLink;
}
