package com.bucheon.clean.town.biz.operation.bandDamdang.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BandDamdangReq {


    private String dong;
    private String damdang;
    private String damdangPhone;

}
