package com.bucheon.clean.town.db.repository.operation;

import com.bucheon.clean.town.db.entity.operation.GlobalBox;
import com.bucheon.clean.town.db.repository.operation.dsl.GlobalBoxRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface GlobalBoxRepository extends JpaRepository<GlobalBox, String>, GlobalBoxRepositoryDsl {

    @Transactional(readOnly = true)
    Optional<GlobalBox> findByBoxSeqAndDelYn(Long seq, String s);
    @Transactional(readOnly = true)
    int countByBoxSeqAndDelYn(Long seq, String s);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_global_box set board_title = :boardTitle where box_seq =:boxSeq "
            , nativeQuery = true
    )
    int UpdateTitle(@Param("boardTitle") String boardTitle, @Param("boxSeq") Long boxSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_global_box set path_seq = :pathSeq where box_seq =:boxSeq "
            , nativeQuery = true
    )
    int UpdatePathSeq(@Param("pathSeq") Long pathSeq, @Param("boxSeq") Long boxSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_global_box set del_yn = :delYn where box_seq =:boxSeq "
            , nativeQuery = true
    )
    int UpdateBoxDel(@Param("delYn") String delYn, @Param("boxSeq") Long boxSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_global_box set use_yn = :useYn where box_seq =:boxSeq "
            , nativeQuery = true
    )
    int UpdateBoxUse(@Param("useYn") String useYn, @Param("boxSeq") Long boxSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_global_box set mod_seq = :admin, mod_dt = now() where box_seq =:boxSeq "
            , nativeQuery = true
    )
    int UpdateBoxMod(@Param("admin") Long admin, @Param("boxSeq") Long boxSeq);

}
