package com.bucheon.clean.town.biz.chatbot.answer.v1.model.skll;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SkllRes {

    private String skllCode;
    private String skllNm;
    private String registDe;
    private String updtDe;
    private String skllLevel1;
    private String skllLevel2;
    private String skllLevel3;
    private String skllLevel4;
    private String rspnsTy1;
    private String rspnsTy2;
    private String rspnsTy3;

}
