package com.bucheon.clean.town.db.repository.environmental;

import com.bucheon.clean.town.db.entity.environmental.PetitionImage;
import com.bucheon.clean.town.db.entity.environmental.id.PetitionImageId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetitionImageRepository extends JpaRepository<PetitionImage, PetitionImageId> {

}
