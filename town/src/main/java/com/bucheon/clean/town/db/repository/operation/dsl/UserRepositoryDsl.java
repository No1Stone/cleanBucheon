package com.bucheon.clean.town.db.repository.operation.dsl;

import com.bucheon.clean.town.biz.operation.user.v1.model.UserReq;
import com.bucheon.clean.town.db.entity.operation.User;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserRepositoryDsl {

    @Transactional(readOnly = true)
    List<User> UserListSelect(UserReq dto);

    @Transactional(readOnly = true)
    QueryResults<User> UserListSelectPaging(UserReq dto);

}
