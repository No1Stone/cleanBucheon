package com.bucheon.clean.town.db.entity.environmental;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "petition")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Petition {
    /**
     *부천시 민원 신청 데이터
     */

    @Id
    @Column(name = "peti_no", nullable = true)
    private String petiNo;
    @Column(name = "reg_date_time", nullable = true)
    private String regDateTime;
    @Column(name = "title", nullable = true)
    private String title;
    @Column(name = "reason", nullable = true)
    private String reason;
    @Column(name = "name", nullable = true)
    private String name;
    @Column(name = "zip_code", nullable = true)
    private String zipCode;
    @Column(name = "address1", nullable = true)
    private String address1;
    @Column(name = "address2", nullable = true)
    private String address2;
    @Column(name = "cell_phone", nullable = true)
    private String cellPhone;
    @Column(name = "did", nullable = true)
    private String did;
    @Column(name = "dong", nullable = true)
    private String dong;
    @Column(name = "modify_reason", nullable = true)
    private String modifyReason;
    @Column(name = "created_date", nullable = true)
    private LocalDateTime createdDate;

    @Builder
    Petition(String petiNo,
             String regDateTime,
             String title,
             String reason,
             String name,
             String zipCode,
             String address1,
             String address2,
             String cellPhone,
             String did,
             String dong,
             String modifyReason,
             LocalDateTime createdDate
    ) {
        this.petiNo = petiNo;
        this.regDateTime = regDateTime;
        this.title = title;
        this.reason = reason;
        this.name = name;
        this.zipCode = zipCode;
        this.address1 = address1;
        this.address2 = address2;
        this.cellPhone = cellPhone;
        this.did = did;
        this.dong = dong;
        this.modifyReason = modifyReason;
        this.createdDate = createdDate;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = PetitionImage.class)
    @JoinColumn(name = "peti_no", referencedColumnName = "peti_no")
    private List<PetitionImage> petitionImages;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Process.class)
    @JoinColumn(name = "peti_no", referencedColumnName = "peti_no")
    private List<Process> processes;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Receipt.class)
    @JoinColumn(name = "peti_no", referencedColumnName = "peti_no")
    private Receipt receipts;

}
