package com.bucheon.clean.town.biz.metabus.metaUser.v1.model;

import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.db.entity.metabus.MetaTrashType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MetaTrashTypeVO {

    private Long trashSeq;
    private String trashName;
    private String trashAct;
    private int trashPoint;
    private String useYn;
    private String delYn;
    private Long regSeq;
    private LocalDateTime regDt;
    private Long modSeq;
    private LocalDateTime modDt;

    public MetaTrashType ofTrashType(){
        MetaTrashType tt = new MetaTrashType();
        tt.setTrashName(this.trashName);
        tt.setTrashAct(this.trashAct);
        tt.setTrashPoint(this.trashPoint);
        tt.setUseYn(YnType.Y.getName());
        tt.setDelYn(YnType.N.getName());
        tt.setRegSeq(this.regSeq);
        tt.setRegDt(LocalDateTime.now());
        tt.setModSeq(this.modSeq);
        tt.setModDt(LocalDateTime.now());
        return tt;
    }

}
