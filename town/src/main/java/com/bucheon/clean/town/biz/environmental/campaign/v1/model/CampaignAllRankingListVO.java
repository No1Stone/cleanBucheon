package com.bucheon.clean.town.biz.environmental.campaign.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CampaignAllRankingListVO {

    int RankNum;
    String regUserDid;
    String name;
    int sumMileage;
}
