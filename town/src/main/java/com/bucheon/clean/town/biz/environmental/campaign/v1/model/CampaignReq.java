package com.bucheon.clean.town.biz.environmental.campaign.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CampaignReq {

    private int page;
    private int size;

    //검색상태 - T:쓰레기접수, P:청소중 , C:청소완료
    private String searchStatus;
    //검색상태 - D:접수, P:처리중, C:처리
    private String searchProcess;
    //검색 쓰레기 유형
    private Long searchTrashType;
    //검색 쓰레기 유형
    private String searchMileageComplete;
    //검색유형 - searchContent: 내용, searchType: 유형, searchLocation: 위치, searchName: 접수자, searchReg: 접수일 기준 검색, searchComplete: 처리일기준 검색
    private String searchSelect;
    //검색유형
    private String searchString;
    //검색상태 - D:접수, P:처리중, C:처리

    //기간검색 시작일
    private String sdate;
    //기간검색 종료일
    private String edate;
    //액셀 년월
    private String selectedExcelYYYYMM;
    //캠페인 상태
    private String reportReception;
    //주소
    private String reportAddrOld;
    //행정구역
    private String sessionAdministrativeDivision;
    private String scomDt;
    private String ecomDt;
    private String sregDt;
    private String eregDt;
    private String regUserDid;
    private String month;

}
