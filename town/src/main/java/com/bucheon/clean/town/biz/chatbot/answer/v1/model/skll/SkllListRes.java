package com.bucheon.clean.town.biz.chatbot.answer.v1.model.skll;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SkllListRes {

    private String skllCode;
    private String hder;
    private String list;
    private String btton;
    private int rspnsOrdr;
    private String bttonOptnAt;
    private String text;
}
