package com.bucheon.clean.town.db.repository.environmental;

import com.bucheon.clean.town.db.entity.environmental.Report;
import com.bucheon.clean.town.db.repository.environmental.dsl.ReportRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ReportRepository extends JpaRepository<Report, Long>, ReportRepositoryDsl {

    @Transactional(readOnly = true)
    Report findByReportSeq(Long seq);

    @Transactional(readOnly = true)
    Report findBySourcePk(String sourcePk);

    @Transactional(readOnly = true)
    int countByReportReceptionAndReportStatusAndDelYn(String reception, String status, String delYn);

    @Transactional(readOnly = true)
    List<Report> findAllByReportReceptionAndReportStatusAndDelYnOrderByReportSeqDesc(String reception, String status, String delYn);

    @Transactional(readOnly = true)
    List<Report> findAllByReportReceptionAndReportStatusOrReportStatusAndDelYnAndSourcePkIsNotNullOrderByReportSeqDesc(String type, String status1, String status2, String delYn);

    boolean existsByBandKey(String bandKey);
    boolean existsBySourcePk(String postKey);


    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set del_yn = :delYn where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateReportDel(@Param("delYn") String delYn, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set report_status = :reportStatus where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateReportStatus(@Param("reportStatus") String reportStatus, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set report_status_reject_reason = :reportStatusRejectReason where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateReportStatusRejectReason(@Param("reportStatusRejectReason") String reportStatusRejectReason, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set com_dt = :comDt where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateComDt(@Param("comDt") LocalDateTime comDt, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set mod_dt = :modDt where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateModDt(@Param("modDt") LocalDateTime modDt, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set report_point = :reportPoint where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateReportPoint(@Param("reportPoint") int reportMilege, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set report_point_reject_reason = :reportPointRejectReason where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateReportPointRejectReason(@Param("reportPointRejectReason") String reportPointRejectReason, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set report_point_complete_yn = :reportPointCompleteYn where report_seq =:reportSeq "
            , nativeQuery = true
    )
    int UpdateReportPointCompleteYn(@Param("reportPointCompleteYn") String reportPointCompleteYn, @Param("reportSeq") Long reportSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set report_status = :reportStatus where source_pk =:sourcePk "
            , nativeQuery = true
    )
    int UpdateReportStatusWhereSourcePk(@Param("reportStatus") String reportStatus, @Param("sourcePk") String sourcePk);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_report set com_dt = :comDt where source_pk =:sourcePk "
            , nativeQuery = true
    )
    int UpdateComDtWhereSourcePk(@Param("comDt") LocalDateTime comDt, @Param("sourcePk") String sourcePk);


    Optional<Report> findByReportSeqAndDelYnOrderByRegDtDesc(Long seq, String del);


    //////////////////////////////////////////////////////////////////////////////////////
    // 메인 숫자 카운트

    //각각의 전체 미처리 또는 처리 건수
    int countByReportReceptionAndReportStatusOrReportStatusAndDelYn(String reception, String reportStatus1, String reportStatus2, String delYn);

    //////챗봇 오늘 등록수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countChatbotToday(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //챗봇 기간별 미처리 건수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND report_status in ('D', 'P') AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countChatbotP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    //챗봇 기간별 미처리 건수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE report_reception = 'C' AND report_status in ('D', 'P') AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countChatbotP2();

    //챗봇 기간별 미처리 건수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND report_status in ('C', 'R') AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countChatbotC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);


    //////밴드 오늘 등록 수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countBandToday(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //밴드 기간별 미처리 건수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND report_status in ('D', 'P') AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countBandP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    //밴드 기간별 미처리 건수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE report_reception = 'B' AND report_status in ('D', 'P') AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countBandP2();

    //밴드 기간별 처리 건수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND report_status in ('C', 'R') AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countBandC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);


    //////전화 오늘 등록 수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countPhoneToday(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //전화 기간별 미처리 건수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND report_status in ('D', 'P') AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countPhoneP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    //전화 기간별 미처리 건수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE report_reception = 'P' AND report_status in ('D', 'P') AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countPhoneP2();

    //전화 기간별 처리 건수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND report_status in ('C', 'R') AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countPhoneC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);


    //////방문 오늘 등록 수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countVisitToday(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //방문 기간별 미처리 건수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND report_status in ('D', 'P') AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countVisitP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    //방문 기간별 미처리 건수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE report_reception = 'V' AND report_status in ('D', 'P') AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countVisitP2();


    //방문 기간별 처리 건수
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND report_status in ('C', 'R') AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countVisitC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //////////



    //////////////////////////////////////////////////////////////////////////////////////
    // 심곡동 챗봇/밴드/전화/방문 숫자
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND area_name = '심곡동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSimgocC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND area_name = '심곡동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSimgocB(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND area_name = '심곡동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSimgocP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND area_name = '심곡동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSimgocV(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //////////////////////////////////////////////////////////////////////////////////////
    // 부천동 챗봇/밴드/전화/방문 숫자
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND area_name = '부천동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countBucheonC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND area_name = '부천동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countBucheonB(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND area_name = '부천동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countBucheonP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND area_name = '부천동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countBucheonV(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //////////////////////////////////////////////////////////////////////////////////////
    // 신중동 챗봇/밴드/전화/방문 숫자
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND area_name = '신중동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSinjoongC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND area_name = '신중동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSinjoongB(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND area_name = '신중동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSinjoongP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND area_name = '신중동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSinjoongV(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //////////////////////////////////////////////////////////////////////////////////////
    // 중동 챗봇/밴드/전화/방문 숫자
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND area_name = '중동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countJoongC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND area_name = '중동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countJoongB(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND area_name = '중동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countJoongP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND area_name = '중동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countJoongV(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //////////////////////////////////////////////////////////////////////////////////////
    // 상동 챗봇/밴드/전화/방문 숫자
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND area_name = '상동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSangC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND area_name = '상동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSangB(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND area_name = '상동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSangP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND area_name = '상동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSangV(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //////////////////////////////////////////////////////////////////////////////////////
    // 대산동 챗봇/밴드/전화/방문 숫자
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND area_name = '대산동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countDeasanC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND area_name = '대산동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countDeasanB(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND area_name = '대산동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countDeasanP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND area_name = '대산동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countDeasanV(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //////////////////////////////////////////////////////////////////////////////////////
    // 소사본동 챗봇/밴드/전화/방문 숫자
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND area_name = '소사본동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSosabonC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND area_name = '소사본동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSosabonB(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND area_name = '소사본동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSosabonP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND area_name = '소사본동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSosabonV(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //////////////////////////////////////////////////////////////////////////////////////
    // 범안동 챗봇/밴드/전화/방문 숫자
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND area_name = '범안동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countBumanC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND area_name = '범안동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countBumanB(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND area_name = '범안동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countBumanP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND area_name = '범안동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countBumanV(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

    //////////////////////////////////////////////////////////////////////////////////////
    // 성곡동 챗봇/밴드/전화/방문 숫자
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND area_name = '성곡동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSunggocC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND area_name = '성곡동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSunggocB(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND area_name = '성곡동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSunggocP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND area_name = '성곡동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countSunggocV(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);


    //////////////////////////////////////////////////////////////////////////////////////
    // 오정동 챗봇/밴드/전화/방문 숫자
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'C' AND area_name = '오정동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countOjungC(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'B' AND area_name = '오정동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countOjungB(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'P' AND area_name = '오정동' AND del_yn = 'N'"
            , nativeQuery = true
    )
    int countOjungP(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);
    @Query(
            value = "SELECT COUNT(*) FROM tb_report WHERE date_format(reg_dt, '%Y-%m-%d') between :sdate2 And :edate2  AND report_reception = 'V' AND area_name = '오정동' AND del_yn = 'N' "
            , nativeQuery = true
    )
    int countOjungV(@Param("sdate2") LocalDate sdate2, @Param("edate2") LocalDate edate2);

}
