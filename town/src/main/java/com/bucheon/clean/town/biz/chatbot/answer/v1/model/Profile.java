package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class Profile {

	private String imageUrl;
	private int width;
	private int height;
	private String title;

}
