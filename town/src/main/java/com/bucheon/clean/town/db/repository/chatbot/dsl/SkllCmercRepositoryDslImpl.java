package com.bucheon.clean.town.db.repository.chatbot.dsl;

import com.bucheon.clean.town.biz.chatbot.answer.v1.model.BasicCardVO;
import com.bucheon.clean.town.biz.chatbot.answer.v1.model.CommerceCardVO;
import com.bucheon.clean.town.db.entity.chatbot.QSkll;
import com.bucheon.clean.town.db.entity.chatbot.QSkllCard;
import com.bucheon.clean.town.db.entity.chatbot.QSkllCmerc;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class SkllCmercRepositoryDslImpl implements SkllCmercRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QSkll qSkll = QSkll.skll;
    private QSkllCmerc qSkllCmerc = QSkllCmerc.skllCmerc;

    @Override
    public List<CommerceCardVO> selectSkllCcCodeAndRspnsOrdr(String code, int reqSeq) {

        List<CommerceCardVO> result = queryFactory.select(
                        Projections.bean(CommerceCardVO.class,
                                qSkllCmerc.skllCode.as("skillCode"),
                                qSkllCmerc.goodsDc.as("description"),
                                qSkllCmerc.pc.as("price"),
                                qSkllCmerc.dscnt.as("discount"),
                                qSkllCmerc.crncy.as("discountAction"),
                                qSkllCmerc.dscntRt.as("discountRate"),
                                qSkllCmerc.dscntPc.as("discountedPrice"),
                                qSkllCmerc.thumb.as("thumbnail"),
                                qSkllCmerc.profl.as("profile"),
                                qSkllCmerc.btton.as("button"),
                                qSkllCmerc.cmercOrdr.as("cmercOrdr"),
                                qSkllCmerc.rspnsOrdr.as("rspnsOrdr"),
                                qSkllCmerc.bttonOptnAt.as("bttonOptnAt")
                        )
                )
                .from(qSkllCmerc)
                .where(eqId(code), eqRspnsOrder(reqSeq))
                .orderBy(qSkllCmerc.cmercOrdr.desc())
                .fetch();
        return result;

    }

    private BooleanExpression eqId(String id) {
        if(StringUtils.isEmptyOrWhitespace(id)){
            return null;
        }
        return qSkllCmerc.skllCode.eq(id);
    }

   private BooleanExpression eqRspnsOrder(int reqSeq) {
        if(reqSeq == 0){
            return null;
        }
        return qSkllCmerc.rspnsOrdr.eq(String.valueOf(reqSeq));
    }

}
