package com.bucheon.clean.town.biz.operation.mileage.v1.model;

import com.bucheon.clean.town.db.entity.chatbot.UploadImage;
import com.bucheon.clean.town.db.entity.operation.UserMileage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserMileageVO {

	/* ID */
	private String userId;
	
	/* 포인트 최종값 */
	private int mileageSum;
	
	/* 포인트 최종 적용 tb_report */
	private Long mileageSeq;
	
	/* 검색  */
	private String searchValue;

	/* 페이지사이즈 */
	private int size;

	/* 페이지 */
	private int page;

	public UserMileage ofUserMileage(){
		UserMileage u = new UserMileage();

		u.setUserId(this.userId);
		if (this.mileageSeq != null) {
			u.setMileageSeq(this.mileageSeq);
		} else {
			u.setMileageSeq(0L);
		}
		u.setMileageSum(this.mileageSum);

		return u;
	}
	
}
