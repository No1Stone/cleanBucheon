package com.bucheon.clean.town.biz.operation.point.v1.controller;

import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.clean.town.biz.operation.point.v1.model.TrashTypeReq;
import com.bucheon.clean.town.biz.operation.point.v1.model.TrashTypeUpdate;
import com.bucheon.clean.town.biz.operation.point.v1.service.PointService;
import com.bucheon.clean.town.biz.util.service.UtilService;
import com.bucheon.clean.town.db.entity.operation.TrashType;
import com.bucheon.clean.town.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(path = "/operation/point")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PointController {

    private final Logger logger = LoggerFactory.getLogger(PointController.class);
    private final PointService pointService;
    private final UtilService utilService;
    private final PermissionService permissionService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/operation/point/info");
    }

    //기본틀
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(TrashTypeReq dto, HttpServletRequest request){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(15L);

        return new ModelAndView("operation/point/pointManage")
                .addObject("pointListPage", pointService.TrashSelectListPagingService(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(TrashTypeReq dto){
        return new ModelAndView("operation/point/pointListAjax")
                .addObject("pointList",pointService.TrashSelectListService(dto))
                .addObject("pointListPage", pointService.TrashSelectListPagingService(dto))
                ;
    }

    //최초 입력 페이지 팝업  VO셋팅페이지
    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm(){
        TrashTypeUpdate ttu = new TrashTypeUpdate();
        return new ModelAndView("operation/point/pointManagePopupAjax")
                .addObject("ttuVo", ttu)
                ;

    }

    //게시글 상세보기
    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView modifyForm(TrashTypeUpdate dto){
        TrashType ttu = pointService.TrashSelectOneService(dto.getTrashSeq());
        return new ModelAndView("operation/point/pointManagePopupAjax")
                .addObject("ttuVo", ttu)
                ;
    }

    //저장처리
    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(@RequestParam("uploadfile") MultipartFile[] uploadfile, TrashTypeUpdate dto) throws IllegalStateException, IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        try {
            List<String> fileName = utilService.FileSaveService(uploadfile, "trash");
            dto.setIconUrl(fileName.get(0));
        } catch (Exception e){
            //
        }
        pointService.TrashSaveService(dto);
        map.put("result", "success");
        return map;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(@RequestParam("uploadfile") MultipartFile[] uploadfile, TrashTypeUpdate dto) throws IllegalStateException, IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        try {
            List<String> fileName = utilService.FileSaveService(uploadfile, "trash");
            dto.setIconUrl(fileName.get(0));
        } catch (Exception e){
            //
        }
        pointService.TrashSaveService(dto);
        map.put("result", "success");
        return map;
    }

    //삭제처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(Long[] trashSeq){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        pointService.UpdatePointDelService(trashSeq);
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    @RequestMapping(path = "/file")
    @ResponseBody
    public List<String> fileUpload(@RequestParam MultipartFile[] uploadfile) throws IllegalStateException, IOException {
       return pointService.UploadService(uploadfile);
    }

}
