package com.bucheon.clean.town.biz.environmental.campaign.v1.model;

import com.bucheon.clean.town.common.type.PaymentType;
import com.bucheon.clean.town.common.type.ProcessType;
import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.db.entity.environmental.Campaign;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CampaignVO {

    private Long campaignSeq;
    private String campaignNum; //접수번호
    private String campaignStatus;
    private String campaignProcess;
    private String campaignProcessRejectReason;
    private Long trashType;
    private String addrOld;
    private String addrNew;
    private String areaName;
    private String locationX;
    private String locationY;
    private String regUserDid;
    private String delYn;
    private int mileage;
    private String mileageStatus;
    private String mileageRejectReason;
    private LocalDateTime comDt;
    private LocalDateTime regDt;

}
