package com.bucheon.clean.town.db.repository.etc;

import com.bucheon.clean.town.db.entity.etc.ConnectSum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ConnectSumRepository extends JpaRepository<ConnectSum, String> {

    @Transactional(readOnly = true)
    int countByConnectDate(LocalDate date);

    @Transactional(readOnly = true)
    Optional<ConnectSum> findByConnectDate(LocalDate date);

    @Transactional(readOnly = true)
    List<ConnectSum> findByConnectDateBetween(LocalDate sdate, LocalDate edate);

    @Transactional
    @Query(
            value = "select sum(clean_app) from tb_connect_sum where date_format(now(), '%Y-%m-%d') between date_format(:sdate, '%Y-%m-%d') and date_format(:edate, '%Y-%m-%d')"
            , nativeQuery = true
    )
    int SelectSumA(@Param("sdate") LocalDate sdate, @Param("edate") LocalDate edate);

    @Transactional
    @Query(
            value = "select sum(band) from tb_connect_sum where date_format(now(), '%Y-%m-%d') between date_format(:sdate, '%Y-%m-%d') and date_format(:edate, '%Y-%m-%d')"
            , nativeQuery = true
    )
    int SelectSumB(@Param("sdate") LocalDate sdate, @Param("edate") LocalDate edate);

    @Transactional
    @Query(
            value = "select sum(chatbot) from tb_connect_sum where date_format(now(), '%Y-%m-%d') between date_format(:sdate, '%Y-%m-%d') and date_format(:edate, '%Y-%m-%d')"
            , nativeQuery = true
    )
    int SelectSumC(@Param("sdate") LocalDate sdate, @Param("edate") LocalDate edate);

    @Transactional
    @Query(
            value = "select sum(metabus) from tb_connect_sum where date_format(now(), '%Y-%m-%d') between date_format(:sdate, '%Y-%m-%d') and date_format(:edate, '%Y-%m-%d')"
            , nativeQuery = true
    )
    int SelectSumM(@Param("sdate") LocalDate sdate, @Param("edate") LocalDate edate);

    @Transactional
    @Query(
            value = "select sum(self) from tb_connect_sum where date_format(now(), '%Y-%m-%d') between date_format(:sdate, '%Y-%m-%d') and date_format(:edate, '%Y-%m-%d')"
            , nativeQuery = true
    )
    int SelectSumV(@Param("sdate") LocalDate sdate, @Param("edate") LocalDate edate);




    @Transactional
    @Modifying
    @Query(
            value = "update tb_connect_sum set clean_app = clean_app + 1 where connect_date =:date "
            , nativeQuery = true
    )
    int UpdateCountA(@Param("date") LocalDate date);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_connect_sum set band = band + 1 where connect_date =:date "
            , nativeQuery = true
    )
    int UpdateCountB(@Param("date") LocalDate date);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_connect_sum set chatbot = chatbot + 1 where connect_date =:date "
            , nativeQuery = true
    )
    int UpdateCountC(@Param("date") LocalDate date);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_connect_sum set self = self + 1 where connect_date =:date "
            , nativeQuery = true
    )
    int UpdateCountV(@Param("date") LocalDate date);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_connect_sum set metabus = metabus + 1 where connect_date =:date "
            , nativeQuery = true
    )
    int UpdateCountM(@Param("date") LocalDate date);

}
