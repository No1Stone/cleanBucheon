package com.bucheon.clean.town.biz.operation.user.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserReq {

    private int page;
    private int size;
    private String did;
    private String chatbotUserId;
    private String metaUserId;
    private String name;
    private String phone;
    private String email;
    private String sregDt;
    private String eregDt;
    private int addPoint;
    private String reasonPoint;

    //검색유형
    private String searchSelect;
    //검색유형
    private String searchString;


}
