package com.bucheon.clean.town.db.repository.chatbot;

import com.bucheon.clean.town.db.entity.chatbot.SkllMng;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface SkllMngRepository extends JpaRepository<SkllMng, String> {

    @Transactional
    @Modifying
    @Query(
            value = "delete from tb_skll_mng where SKLL_CODE =:answerSeq "
            , nativeQuery = true
    )
    int DeleteAnswerSkllMngCode(@Param("answerSeq") String answerSeq);

}
