package com.bucheon.clean.town.biz.operation.permission.v1.controller;

import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuReq;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.permission.v1.model.PermissionReq;
import com.bucheon.clean.town.biz.operation.permission.v1.model.PermissionRes;
import com.bucheon.clean.town.biz.operation.permission.v1.model.PermissionUpdate;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.PermissionMenu;
import com.bucheon.clean.town.db.repository.operation.MenuRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(path = "/operation/permission")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PermissionController {

    private final Logger logger = LoggerFactory.getLogger(PermissionController.class);
    private final PermissionService permissionService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/operation/permission/permissionManage");
    }

    //기본틀
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(PermissionReq dto, HttpServletRequest request){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(14L);

       return new ModelAndView("operation/permission/permissionManage")
                .addObject("permiPaging", permissionService.PermissionListSelectPagingService(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(PermissionReq dto){
        return new ModelAndView("operation/permission/permissionListAjax")
                .addObject("permiList", permissionService.PermissionListSelectService(dto))
                .addObject("permiPaging", permissionService.PermissionListSelectPagingService(dto));

    }

    //최초 입력 페이지 팝업
    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm(){

       return new ModelAndView("operation/permission/permissionManagePopupAjax")
               .addObject("permiOne", new PermissionRes())
               .addObject("permissionMenuList",null)
               .addObject("menuList",menuService.UseMenuSelectListService(new MenuReq()))
                ;
    }

    //수정 입력 페이지 팝업
    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView modifyForm(Long seq){
        return new ModelAndView("operation/permission/permissionManagePopupAjax")
                .addObject("permiOne",permissionService.PermissionSelectOneService(seq))
                .addObject("permissionMenuList",permissionService.PermissionMenuSelectLiService(seq))
                .addObject("menuList",menuService.UseMenuSelectListService(new MenuReq()))
                ;
    }

    //저장처리
    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(PermissionUpdate dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        Long permissionSeq = permissionService.PermissionSaveService(dto);

        permissionService.PermissionMenuDelService(permissionSeq);

        List<Long> ll = dto.getPermissionMenuArr();
        for (int i=0; i<ll.size();i++) {

            PermissionMenu p = new PermissionMenu();
            p.setPermissionSeq(permissionSeq);
            p.setMenuSeq(ll.get(i));
            p.setUseYn("Y");

            permissionService.PermissionMenuSaveService(p);
            //System.out.println(">>>>>>>>>>>>>>>>" + ll.get(i));
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(PermissionUpdate dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        permissionService.PermissionSaveService(dto);

        permissionService.PermissionMenuDelService(dto.getPermissionSeq());

        List<Long> ll = dto.getPermissionMenuArr();
        for (int i=0; i<ll.size();i++) {

            PermissionMenu p = new PermissionMenu();
            p.setPermissionSeq(dto.getPermissionSeq());
            p.setMenuSeq(ll.get(i));
            p.setUseYn("Y");

            permissionService.PermissionMenuSaveService(p);
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //삭제처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(Long[] permissionSeq){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        permissionService.UpdatePermissionDelService(permissionSeq);

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

}
