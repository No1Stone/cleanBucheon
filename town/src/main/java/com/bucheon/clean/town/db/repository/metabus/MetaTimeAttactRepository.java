package com.bucheon.clean.town.db.repository.metabus;

import com.bucheon.clean.town.db.entity.metabus.MetaTimeAttact;
import com.bucheon.clean.town.db.entity.metabus.MetaTrashType;
import com.bucheon.clean.town.db.repository.metabus.dsl.MetaTrashTypeRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface MetaTimeAttactRepository extends JpaRepository<MetaTimeAttact, String> {

    @Transactional(readOnly = true)
    Optional<MetaTimeAttact> findTop1ByUseYnAndDelYn(String use, String del);

}
