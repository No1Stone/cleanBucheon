package com.bucheon.clean.town.biz.chatbot.answer.v1.model.skll;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SkllCardRes {

    private String skllCode;
    private String sj;
    private String thumb;
    private String profl;
    private String socty;
    private String btton;
    private String bcOrdr;
    private int rspnsOrdr;
    private String dc;
    private String bttonOptnAt;
    private String proflCrtfcAt;
    private String text;


}
