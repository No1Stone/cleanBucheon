package com.bucheon.clean.town.db.entity.environmental.id;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
public class ReceiptId implements Serializable {
    /**
     * 부천시 민원 접수 데이터
     */
    private String petiNo;
    private String civilNo;

}
