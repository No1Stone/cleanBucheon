package com.bucheon.clean.town.common.config;

import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.repository.operation.AdminRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RequiredArgsConstructor
@Component
public class AuthSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private final AdminRepository adminRepository;


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {

        //로그인 성공 시 추가로 로그인 시간을 넣어준다
        //현재 관련 로직과 테이블 없음
        //adminRepository.updateLastLogin(authentication.getName(), LocalDateTime.now());

        HttpSession session = request.getSession();
        String sessionAdminId = authentication.getName();
        Admin admin = adminRepository.findByAdminId(sessionAdminId);
        String sessionAdminName = admin.getAdminName();

        System.out.println(">>>>>>>>>>>>>>>>> sesseionId : " + sessionAdminId);
        System.out.println(">>>>>>>>>>>>>>>>> sesseionName : " + sessionAdminName);

        session.setAttribute("sesseionId", sessionAdminId);
        session.setAttribute("sesseionName", sessionAdminName);

        setDefaultTargetUrl("/home/info");
        super.onAuthenticationSuccess(request, response, authentication);
    }

}
