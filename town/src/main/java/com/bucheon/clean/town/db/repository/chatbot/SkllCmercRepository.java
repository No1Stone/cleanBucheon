package com.bucheon.clean.town.db.repository.chatbot;

import com.bucheon.clean.town.db.entity.chatbot.SkllCmerc;
import com.bucheon.clean.town.db.entity.chatbot.id.SkllCmercId;
import com.bucheon.clean.town.db.repository.chatbot.dsl.SkllCmercRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface SkllCmercRepository extends JpaRepository<SkllCmerc, SkllCmercId>, SkllCmercRepositoryDsl {

    @Transactional
    @Modifying
    @Query(
            value = "delete from tb_skll_cmerc where SKLL_CODE =:answerSeq "
            , nativeQuery = true
    )
    int DeleteAnswerSkllCmercCode(@Param("answerSeq") String answerSeq);

}
