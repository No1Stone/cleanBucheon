package com.bucheon.clean.town.biz.environmental.report.v1.band.model;


public class BandBoardList {
    private String content;
    private String post_key;
    private int comment_count;
    private long created_at;
    private int emotion_count;
    private String band_key;
    private String name;
    private String description;
    private String role;
    private String profile_image_url;
    private String user_key;

}
