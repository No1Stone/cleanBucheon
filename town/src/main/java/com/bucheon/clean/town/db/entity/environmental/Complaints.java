package com.bucheon.clean.town.db.entity.environmental;

import lombok.*;
import org.hibernate.tuple.GeneratedValueGeneration;

import javax.persistence.*;

@Entity
@Table(name = "tb_complaints")
@Getter@Setter
@NoArgsConstructor
public class Complaints {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "board_seq", nullable = true)
    private String boardSeq;
    @Column(name = "complaints_status", nullable = true)
    private String complaintsStatus;
    @Column(name = "complaints_title", nullable = true)
    private String complaintsTitle;
    @Column(name = "complaints_content", nullable = true)
    private String complaintsContent;
    @Column(name = "complaints_type", nullable = true)
    private String complaintsType;
    @Column(name = "complaints_in_charge", nullable = true)
    private String complaintsIn_charge;
    @Column(name = "complaints_dt", nullable = true)
    private String complaintsDt;

    @Builder
    Complaints(String boardSeq,
               String complaintsStatus,
               String complaintsTitle,
               String complaintsContent,
               String complaintsType,
               String complaintsIn_charge,
               String complaintsDt
    ) {
        this.boardSeq = boardSeq;
        this.complaintsStatus = complaintsStatus;
        this.complaintsTitle = complaintsTitle;
        this.complaintsContent = complaintsContent;
        this.complaintsType = complaintsType;
        this.complaintsIn_charge = complaintsIn_charge;
        this.complaintsDt = complaintsDt;
    }

}
