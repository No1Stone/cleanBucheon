package com.bucheon.clean.town.biz.keyword.v1.service.interestWord;

import com.bucheon.clean.town.biz.keyword.v1.mapper.InterestWordMapper;
import com.bucheon.clean.town.biz.keyword.v1.model.InterestWord;
import com.bucheon.clean.town.biz.keyword.v1.model.WordReq;
import com.bucheon.clean.town.common.util.Paging;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class InterestwordServiceImpl implements InterestwordService {

	private final InterestWordMapper interestWordMapper;

	public Paging getPage(WordReq dto) {
		Paging pa = interestWordMapper.getTotalSize(dto);
		pa.setTotalPage((pa.getTotalSize() % dto.getSize() > 0) ? pa.getTotalSize() / dto.getSize() + 1 : pa.getTotalSize() / dto.getSize());
		pa.setSize(dto.getSize());
		pa.setPage(dto.getPage());
		return pa;
	}

	public List<InterestWord> getList(WordReq dto) {
		List<InterestWord> result = interestWordMapper.getList(dto);
		return result;
	}

	public InterestWord getOne(InterestWord dto) {
		InterestWord result = interestWordMapper.getOne(dto);
		return result;
	}

	public int save(InterestWord interestWord) {
		return interestWordMapper.save(interestWord);
	}

	public int update(InterestWord interestWord) {
		return interestWordMapper.update(interestWord);
	}

	public int delete(String[] word1) {
		int result = 0;

		for(int i=0; i<word1.length; i++){
			InterestWord word = new InterestWord();
			String[] param = word1[i].split("/");
			word.setWord1(param[0]);
			word.setWord2(param[1]);
			word.setBusiBranCode(param[2]);

			result = result + interestWordMapper.delete(word);
		}

		return result;
	}
	  
}
