package com.bucheon.clean.town.biz.environmental.complaint.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ComplaintsReq {

    private int page;
    private int size;
    private String comTitle;
    private String comType;
    private String comProcess;
    private String comOffice;
    private String comOfficeUser;
    private String sdate;
    private String edate;
    private String sproDt;
    private String eproDt;
    private String sregDt;
    private String eregDt;


    private String searchStatus;
    private String searchTrashType;
    private String searchSelect;
    private String searchString;

}
