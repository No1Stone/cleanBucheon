package com.bucheon.clean.town.db.repository.environmental;

import com.bucheon.clean.town.db.entity.environmental.Process;
import com.bucheon.clean.town.db.entity.environmental.id.ProcessId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessRepository extends JpaRepository<Process, ProcessId> {
}
