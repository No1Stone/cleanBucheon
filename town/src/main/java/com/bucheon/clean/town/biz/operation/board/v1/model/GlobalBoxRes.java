package com.bucheon.clean.town.biz.operation.board.v1.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GlobalBoxRes {

    private Long boxSeq;
    private String boardTitle;
    private Long pathSeq;
    private String pathName;
    private String pathUrl;
    private String useYn;
    private Long regSeq;
    private String regName;
    private LocalDateTime regDt;
    private Long modSeq;
    private String modName;
    private LocalDateTime modDt;

}
