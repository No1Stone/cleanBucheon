package com.bucheon.clean.town.biz.util.service;

import com.bucheon.clean.town.biz.operation.point.v1.model.TrashTypeRes;
import com.bucheon.clean.town.common.util.ShortURL;
import com.bucheon.clean.town.db.repository.operation.TrashTypeRepository;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UtilService {

    private final Logger logger = LoggerFactory.getLogger(UtilService.class);
    private final HttpServletRequest httpServletRequest;
    private final TrashTypeRepository trashTypeRepository;
    private final ModelMapper modelmapper;

    @Value("${domain.url}")
    private String domainPath;
    @Value("${domain.file}")
    private String filePath;
    @Value("${domain.basepath}")
    private String fileBasePath;
    @Value("${naver.developer.client.id}")
    private String naverDevId;
    @Value("${naver.developer.client.secret}")
    private String naverDevSecret;
    @Value("${naver.developer.url}")
    private String naverDevUrl;

    public ResponseEntity<Resource> imgGetService(String imgPath) throws IOException {
        String basepath = fileBasePath;
        logger.info("---path : {}", basepath + imgPath );
        Resource recorce = new FileSystemResource(basepath + imgPath);
        if (!recorce.exists()) {
            return new ResponseEntity<Resource>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders header = new HttpHeaders();
        Path filePath = null;
        try {
            filePath = Paths.get(basepath + imgPath);
            header.add("Content-Type", Files.probeContentType(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<Resource>(recorce, header, HttpStatus.OK);
    }


    public List<TrashTypeRes> trashSelectService() {
        return trashTypeRepository.findAllByUseYnAndDelYn("Y", "N")
                .stream().map(e -> modelmapper.map(e, TrashTypeRes.class))
                .collect(Collectors.toList());
    }

//    public ResponseEntity<Resource> BaseImgGetService(String imgPath) throws IOException {
//        String abPath = new ClassPathResource("/static/img/manager/trashImg/"+imgPath).getFile().getAbsolutePath();
//        Resource recorce = new FileSystemResource(abPath);
//        if(!recorce.exists()){
//            return new ResponseEntity<Resource>(HttpStatus.NOT_FOUND);
//        }
//        HttpHeaders header = new HttpHeaders();
//        Path filePath = null;
//        try{
//            filePath = Paths.get(abPath);
//            header.add("Content-Type", Files.probeContentType(filePath));
//        }
//        catch (IOException e){
//            e.printStackTrace();
//        }
//        return new ResponseEntity<Resource>(recorce, header, HttpStatus.OK);
//    }


//    public ResponseEntity<Resource> ImgSaveAndDownloadService(String dir,String imgPath) throws IOException {
//        String abPath = new ClassPathResource("/static/img/"+dir+"/"+imgPath).getFile().getAbsolutePath();
////        String Path = new ClassPathResource("/static/img/"+dir+"/"+imgPath).getFile().getPath();
////        String canPath = new ClassPathResource("/static/img/"+dir+"/"+imgPath).getFile().getCanonicalPath();
//        logger.info("abPath  = {}",abPath);
////        logger.info("Path    = {}",Path);
////        logger.info("canPath = {}",canPath);
//        Resource recorce = new FileSystemResource(abPath);
//        if(!recorce.exists()){
//            return new ResponseEntity<Resource>(HttpStatus.NOT_FOUND);
//        }
//        HttpHeaders header = new HttpHeaders();
//        Path filePath = null;
//        try{
//            filePath = Paths.get(abPath);
//            header.add("Content-Type", Files.probeContentType(filePath));
//        }
//        catch (IOException e){
//            e.printStackTrace();
//        }
//        return new ResponseEntity<Resource>(recorce, header, HttpStatus.OK);
//    }


    public List<String> FileSaveService(MultipartFile[] uploadfile, String boardName) throws IllegalStateException, IOException {
        List<String> ll = new ArrayList<>();
        String nn = "";
        for (MultipartFile mf : uploadfile) {
            if (!mf.isEmpty()) {
                nn = UUID.randomUUID().toString();
                File newFileName = new File(nn + "_" + mf.getOriginalFilename());
                logger.info("path - - {}", newFileName.getPath());
                Path directory = Paths.get(filePath + boardName).toAbsolutePath().normalize();
                // directory 해당 경로까지 디렉토리를 모두 만든다.
                Files.createDirectories(directory);
                // 파일명을 바르게 수정한다.
                String fileName = StringUtils.cleanPath(nn + "_" + mf.getOriginalFilename()).replaceAll(" ", "");
                // 파일명에 '..' 문자가 들어 있다면 오류를 발생하고 아니라면 진행(해킹및 오류방지)
                Assert.state(!fileName.contains(".."), "Name of file cannot contain '..'");
                // 파일을 저장할 경로를 Path 객체로 받는다.
                Path targetPath = directory.resolve(fileName).normalize();
                // 파일이 이미 존재하는지 확인하여 존재한다면 오류를 발생하고 없다면 저장한다.
                Assert.state(!Files.exists(targetPath), fileName + " File alerdy exists.");
                mf.transferTo(targetPath);
//                ll.add("http://192.168.0.42:9506/admin/img/"+boardName+"/"+fileName);
                ll.add(domainPath + boardName + "/" + fileName);
                logger.info("upload file -{}", new Gson().toJson(ll));
            }
        }
        return ll;
    }

    public List<Map<String, Object>> FileSaveService2(MultipartFile[] uploadfile, String boardName) throws IllegalStateException, IOException {
        List<Map<String, Object>> ll = new ArrayList<>();
        String nn = "";
        int i = 0;
        for (MultipartFile mf : uploadfile) {

            if (!mf.isEmpty()) {
                nn = UUID.randomUUID().toString();
                File newFileName = new File(nn + "_" + mf.getOriginalFilename());
                logger.info("path - - {}", newFileName.getPath());
                Path directory = Paths.get(filePath + boardName).toAbsolutePath().normalize();
                // directory 해당 경로까지 디렉토리를 모두 만든다.
                Files.createDirectories(directory);
                // 파일명을 바르게 수정한다.
                String fileName = StringUtils.cleanPath(nn + "_" + mf.getOriginalFilename()).replaceAll(" ", "");
                // 파일명에 '..' 문자가 들어 있다면 오류를 발생하고 아니라면 진행(해킹및 오류방지)
                Assert.state(!fileName.contains(".."), "Name of file cannot contain '..'");
                // 파일을 저장할 경로를 Path 객체로 받는다.
                Path targetPath = directory.resolve(fileName).normalize();
                // 파일이 이미 존재하는지 확인하여 존재한다면 오류를 발생하고 없다면 저장한다.
                Assert.state(!Files.exists(targetPath), fileName + " File alerdy exists.");
                mf.transferTo(targetPath);
//                ll.add("http://192.168.0.42:9506/admin/img/"+boardName+"/"+fileName);

                Map<String, Object> map = new HashMap<>();
                String link = domainPath + boardName + "/" + fileName;
                map.put("fileName", fileName);
                map.put("link", link);
                map.put("num", i);

                ll.add(map);
                logger.info("upload file -{}", new Gson().toJson(ll));
            }
            i++;
        }
        return ll;
    }


    public String CreateShortURLService(String PageURL) throws UnsupportedEncodingException {
        String originalURL = PageURL;

        originalURL = URLEncoder.encode(originalURL,"UTF-8");

        String apiURL = naverDevUrl + "/v1/util/shorturl?url=" + originalURL;
        String clientId = naverDevId; //애플리케이션 클라이언트 아이디값
        String clientSecret = naverDevSecret; //애플리케이션 클라이언트 시크릿값

        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("X-Naver-Client-Id", clientId);
        requestHeaders.put("X-Naver-Client-Secret", clientSecret);
        String responseBody = ShortURL.get(apiURL,requestHeaders);

        Gson gson = new Gson();
        String jsonObject = new Gson().toJson(responseBody);
        JsonElement jsonElement = gson.fromJson(responseBody, JsonElement.class);
        jsonElement = jsonElement.getAsJsonObject().get("result");

        String result = String.valueOf(jsonElement.getAsJsonObject().get("url"));
        result = result.substring(1);
        result = result.substring(0,result.length()-1);

        return result;
    }

}
