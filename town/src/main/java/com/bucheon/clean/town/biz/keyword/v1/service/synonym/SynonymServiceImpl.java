package com.bucheon.clean.town.biz.keyword.v1.service.synonym;

import com.bucheon.clean.town.biz.keyword.v1.mapper.SynonymMapper;
import com.bucheon.clean.town.biz.keyword.v1.mapper.SynonymMapper;
import com.bucheon.clean.town.biz.keyword.v1.model.Synonym;
import com.bucheon.clean.town.biz.keyword.v1.model.WordReq;
import com.bucheon.clean.town.common.util.Paging;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class SynonymServiceImpl implements SynonymService {

	private final SynonymMapper synonymMapper;

	public Paging getPage(WordReq dto) {
		Paging pa = synonymMapper.getTotalSize(dto);
		pa.setTotalPage((pa.getTotalSize() % dto.getSize() > 0) ? pa.getTotalSize() / dto.getSize() + 1 : pa.getTotalSize() / dto.getSize());
		pa.setSize(dto.getSize());
		pa.setPage(dto.getPage());
		return pa;
	}

	public List<Synonym> getList(WordReq dto) {
		List<Synonym> result = synonymMapper.getList(dto);
		return result;
	}

	public Synonym getOne(Synonym dto) {
		Synonym result = synonymMapper.getOne(dto);
		return result;
	}

	public int save(Synonym synonym) {
		return synonymMapper.save(synonym);
	}

	public int update(Synonym synonym) {
		return synonymMapper.update(synonym);
	}

	public int delete(String[] representativeWord1) {
		int result = 0;

		for(int i=0; i<representativeWord1.length; i++){
			Synonym word = new Synonym();
			String[] param = representativeWord1[i].split("/");
			word.setRepresentativeWord1(param[0]);
			word.setRepresentativeWord2(param[1]);
			word.setOriginWord1(param[2]);
			word.setOriginWord2(param[3]);
			word.setBusiBranCode(param[4]);

			result = result + synonymMapper.delete(word);
		}

		return result;
	}
	  
}
