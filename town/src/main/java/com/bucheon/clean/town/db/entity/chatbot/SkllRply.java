package com.bucheon.clean.town.db.entity.chatbot;

import com.bucheon.clean.town.db.entity.chatbot.id.SkllRplyId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_skll_rply")
@Getter
@Setter
@NoArgsConstructor
@IdClass(SkllRplyId.class)
public class SkllRply {

    @Id
    @Column(name = "skll_code", nullable = true)
    private String skllCode;
    @Column(name = "lbl", nullable = true)
    private String lbl;
    @Column(name = "actn", nullable = true)
    private String actn;
    @Column(name = "quest_message", nullable = true)
    private String questMessage;
    @Column(name = "blck_orginl_id", nullable = true)
    private String blckOrginlId;
    @Id
    @Column(name = "qr_ordr", nullable = true)
    private int qrOrdr;

    @Builder
    SkllRply(
            String skllCode,
            String lbl,
            String actn,
            String questMessage,
            String blckOrginlId,
            int qrOrdr
    ) {
        this.skllCode = skllCode;
        this.lbl = lbl;
        this.actn = actn;
        this.questMessage = questMessage;
        this.blckOrginlId = blckOrginlId;
        this.qrOrdr = qrOrdr;
    }
}
