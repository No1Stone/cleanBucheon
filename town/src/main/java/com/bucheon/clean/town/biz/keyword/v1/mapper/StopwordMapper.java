package com.bucheon.clean.town.biz.keyword.v1.mapper;

import com.bucheon.clean.town.biz.keyword.v1.model.Stopword;
import com.bucheon.clean.town.biz.keyword.v1.model.WordReq;
import com.bucheon.clean.town.common.util.Paging;
import com.querydsl.core.QueryResults;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StopwordMapper {

    public Paging getTotalSize(WordReq dto);

    public List<Stopword> getList(WordReq dto);

    public Stopword getOne(String word);

    public int save(Stopword stopword);
    
    public int update(Stopword stopword);
    
    public int delete(String word);
    
}
