package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import java.util.List;
import java.util.Map;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 스킬관리  > Basic Card VO
 * @author user
 *
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BasicCardVO {
	
	/* 스킬 코드 */
	private String skillCode;
	
	/* 타이틀 */
	private String title;
	
	/* 상세내용 */
	private String description;
	
	/* 상단의 이미지 명 또는 이미지 경로 */
	private String imageUrlLink;
	
	/* 상단의 이미지 명 또는 이미지 경로 */
	private String thumbnail;
	
	/* 	카드의 프로필 정보 */
	private String profile;
	
	/* 카드의 소셜 정보 */
	private String social;
	
	/* 버튼 정보 String Type */
	private String button;
	
	/* response sequence */
	private int resSeq;
	
	/* Carousel sequence */
	private int bcSeq;
	
	/* 상단의 이미지 명 또는 이미지 경로 VO */
	private Map<String,Object> thumbnailVO;
	
	/* 	카드의 프로필 VO */
	private BasicCardProfile profileVO;
	
	/* 카드의 소셜 VO */
	private BasicCardSocial socialVO;
	
	/* 버튼 정보 VO */
	private List<BasicCardButtons> buttonVO;
	
	/* 공공예약 구분하는 값 */
	private String subSystem;
	
	private String optnAt;
	private String proflCrtfcAt;

	@Builder
	public BasicCardVO(
			String skillCode,
			String title,
			String description,
			String imageUrlLink,
			String thumbnail,
			String profile,
			String social,
			String button,
			int resSeq,
			int bcSeq,
			Map<String,Object> thumbnailVO,
			BasicCardProfile profileVO,
			BasicCardSocial socialVO,
			List<BasicCardButtons> buttonVO,
			String subSystem,
			String optnAt,
			String proflCrtfcAt
	){
		this.skillCode=skillCode;
		this.title=title;
		this.description=description;
		this.imageUrlLink=imageUrlLink;
		this.thumbnail=thumbnail;
		this.profile=profile;
		this.social=social;
		this.button=button;
		this.resSeq=resSeq;
		this.bcSeq=bcSeq;
		this.thumbnailVO=thumbnailVO;
		this.profileVO=profileVO;
		this.socialVO=socialVO;
		this.buttonVO=buttonVO;
		this.subSystem=subSystem;
		this.optnAt=optnAt;
		this.proflCrtfcAt=proflCrtfcAt;
	}
	
}
