package com.bucheon.clean.town.db.entity.environmental.id;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
public class PetitionImageId implements Serializable {
    private String uuid;
    private String petiNo;
}
