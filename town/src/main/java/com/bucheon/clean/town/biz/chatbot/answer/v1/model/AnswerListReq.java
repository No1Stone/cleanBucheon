package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnswerListReq {


    private int page;
    private int size;
    // searchName : 스킬명검색, searchCode : 코드검색
    private String searchSelect;
    // 검색어
    private String searchString;


}
