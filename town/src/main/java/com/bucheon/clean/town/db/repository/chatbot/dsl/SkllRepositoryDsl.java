package com.bucheon.clean.town.db.repository.chatbot.dsl;

import com.bucheon.clean.town.biz.chatbot.answer.v1.model.AnswerReq;
import com.bucheon.clean.town.biz.chatbot.answer.v1.model.SkillManageRVO;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SkllRepositoryDsl {

    @Transactional(readOnly = true)
    List<SkillManageRVO> SkllSelectList(AnswerReq dto);

    @Transactional(readOnly = true)
    QueryResults<SkillManageRVO> SkllSelectListPaging(AnswerReq dto);
}
