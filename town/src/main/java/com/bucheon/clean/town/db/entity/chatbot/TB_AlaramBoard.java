package com.bucheon.clean.town.db.entity.chatbot;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "tb_alaram_board")
@Getter
@Setter
@NoArgsConstructor
public class TB_AlaramBoard {
    @Id
//    @GeneratedValue(
////            strategy = GenerationType.IDENTITY
//            generator = "board_seq"
//    )
    @Column(name = "board_seq_id", nullable = true)
    private Long boardSeqId;
    @Column(name = "chatbot_user_id", nullable = true)
    private String chatbotUserId;
    @Column(name = "phone_number", nullable = true)
    private String phoneNumber;
    @Column(name = "report_type", nullable = true)
    private String reportType;
    @Column(name = "new_address", nullable = true)
    private String newAddress;
    @Column(name = "old_address", nullable = true)
    private String oldAddress;
    @Column(name = "grid_x", nullable = true)
    private String gridx;
    @Column(name = "grid_y", nullable = true)
    private String gridy;
    @Column(name = "contents", nullable = true)
    private String contents;
    @Column(name = "agree_person_info_collection", nullable = true)
    private String agreePersonInfoCollection;
    @Column(name = "agree_person_info_handling", nullable = true)
    private String agreePersonInfoHandling;
    @Column(name = "alaram_board_dt", nullable = true)
    private LocalDateTime alaramBoardDt;
    @Column(name = "band_process", nullable = true)
    private String bandProcess;
    @Column(name = "feedback_status", nullable = true)
    private String feedbackStatus;
    @Column(name = "feedback_detail", nullable = true)
    private String feedbackDetail;
    @Column(name = "email", nullable = true)
    private String email;
    @Column(name = "name", nullable = true)
    private String name;


    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = TB_AlaramBoardPoto.class)
    @JoinColumn(name = "board_seq_id", referencedColumnName = "board_seq_id")
    private List<TB_AlaramBoardPoto> alaramBoardPotos;


    /*
    CREATE SEQUENCE board_seq
INCREMENT BY 1
START WITH 1
MINVALUE   1
MAXVALUE  9223372036854775806
NOCYCLE
NOCACHE
;
     */
    /*
create table tb_alaram_board(
board_seq_id bigint,
phone_number varchar(15),
name varchar(50),
email varchar(100),
report_type varchar(1),
new_address varchar(255),
old_address varchar(255),
grid_x varchar(255),
grid_y varchar(255),
contents text,
agree_person_info_collection varchar(1),
agree_person_info_handling varchar(1),
alaram_board_dt Timestamp,
band_process varchar(1),
feedback_status varchar(1),
feedback_detail varchar(1),
primary key( board_seq_id )
);
     */
}
