package com.bucheon.clean.town.db.repository.environmental.dsl;


import com.bucheon.clean.town.biz.environmental.report.v1.model.ReportExcelDto;
import com.bucheon.clean.town.biz.environmental.report.v1.model.ReportListReq;
import com.bucheon.clean.town.db.entity.environmental.Report;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ReportRepositoryDsl {

    @Transactional(readOnly = true)
    List<Report> SelectListReport(ReportListReq dto);
    @Transactional(readOnly = true)
    QueryResults<Report> SelectListReportPaging(ReportListReq dto);
    @Transactional(readOnly = true)
    List<Report> SelectAppListReport(ReportListReq dto);
    @Transactional(readOnly = true)
    QueryResults<Report> SelectAppListReportPaging(ReportListReq dto);

    //    @Transactional(readOnly = true)
//    List<Report> SelectListDetailReport();
    @Transactional(readOnly = true)
    List<ReportExcelDto> findExcelList(ReportListReq dto);

}
