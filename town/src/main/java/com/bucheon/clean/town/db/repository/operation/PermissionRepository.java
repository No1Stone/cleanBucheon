package com.bucheon.clean.town.db.repository.operation;

import com.bucheon.clean.town.biz.operation.permission.v1.model.PermissionRes;
import com.bucheon.clean.town.db.entity.operation.Permission;
import com.bucheon.clean.town.db.repository.operation.dsl.PermissionRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface PermissionRepository extends JpaRepository<Permission, Long>, PermissionRepositoryDsl {

    Optional<Permission> findByPermissionSeqAndDelYn(Long seq, String del);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_permission set permission_name = :permissionName where permission_seq =:permissionSeq "
            , nativeQuery = true
    )
    int updatePermissionName(@Param("permissionName") String permissionName, @Param("permissionSeq") Long permissionSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_permission set use_yn = :useYn where permission_seq =:permissionSeq "
            , nativeQuery = true
    )
    int updateUseYn(@Param("useYn") String useYn, @Param("permissionSeq") Long permissionSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_permission set del_yn = :delYn where permission_seq =:permissionSeq "
            , nativeQuery = true
    )
    int updatePermissionDelYn(@Param("delYn") String delYn, @Param("permissionSeq") Long permissionSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_permission set mod_seq = :modSeq, mod_dt = now() where permission_seq =:permissionSeq "
            , nativeQuery = true
    )
    int updateMod(@Param("modSeq") Long modSeq, @Param("permissionSeq") Long permissionSeq);

}
