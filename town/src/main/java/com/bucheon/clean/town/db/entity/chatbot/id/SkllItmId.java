package com.bucheon.clean.town.db.entity.chatbot.id;

import lombok.Data;

import java.io.Serializable;
@Data
public class SkllItmId implements Serializable {
    private String skllCode;
    private String itmOrdr;
    private String rspnsOrdr;

}
