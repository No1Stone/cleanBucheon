package com.bucheon.clean.town.db.repository.environmental;

import com.bucheon.clean.town.db.entity.environmental.CampaignImg;
import com.bucheon.clean.town.db.entity.environmental.ReportImg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CampaignImgRepository extends JpaRepository<CampaignImg, Long> {

    boolean existsByCampaignSeq(Long seq);

    List<CampaignImg> findByCampaignSeq(Long reSeq);

}
