package com.bucheon.clean.town.biz.environmental.campaign.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CampaignRes {

    private Long campaignSeq;
    private String campaignNum; //접수번호
    private String campaignStatus;
    private String campaignStatusName;
    private String campaignProcess;
    private String campaignProcessName;
    private String campaignProcessRejectReason;
    private Long trashType;
    private String trashTypeName;
    private String addrOld;
    private String addrNew;
    private String areaName;
    private String locationX;
    private String locationY;
    private String regUserDid;
    private String userName;
    private String userPhone;
    private String userEmail;
    private String delYn;
    private String mileage;
    private String mileageStatus;
    private String mileageStatusName;
    private String mileageRejectReason;
    private LocalDateTime comDt;
    private LocalDateTime regDt;
    private List<String> campaignImgs;

}
