package com.bucheon.clean.town.biz.environmental.report.v1.model;

import com.bucheon.clean.town.db.entity.environmental.ReportLog;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReportListRes {

    private Long reportSeq;
    private String receptionNum; //접수번호
    //내용
    private String reportContent;
    //유형 트래쉬타입
    private Long reportType;
    private String reportTypeName;
    private int declarationMileage;
    private int processMileage;
    //챗봇/밴드/어플
    private String reportReception;
    private String reportReceptionName;
    //주소
    private String reportAddrOld;
    //접수상태
    private String reportStatus;
    private String reportStatusName;
    private String reportStatusRejectReason;
    private String areaName;
    private String reportLocationX;
    private String reportLocationY;
    private String reportAddrNew;
    private String reportUserName;
    private String reportUserPhone;
    private String reportUserEmail;
    private String delYn;
    private String regUserId;
    private String sourcePk;
    private LocalDateTime comDt;
    private LocalDateTime regDt;
    private LocalDateTime modDt;
    private List<String> reportImgs;
    private String comDts;
    private String regDts;

    //ci
    private String ci;
    //지급포인트
    private String reportPoint;
    //지급거부이유
    private String reportPointRejectReason;
    //해당 건에 대한 포인트 지급 여부
    private String reportPointCompleteYn;
    private String reportPointPaymentName;

}
