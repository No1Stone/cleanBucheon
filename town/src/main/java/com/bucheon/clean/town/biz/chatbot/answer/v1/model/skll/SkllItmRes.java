package com.bucheon.clean.town.biz.chatbot.answer.v1.model.skll;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SkllItmRes {

    private String skllCode;
    private String thumb;
    private String head;
    private String imageSj;
    private String itmList;
    private String sj;
    private String dc;
    private String btton;
    private String itmOrdr;
    private String rspnsOrdr;
    private String bttonOptnAt;
    private String proflCrtfcAt;
    private String seoultalkBtton;
    private String text;

}
