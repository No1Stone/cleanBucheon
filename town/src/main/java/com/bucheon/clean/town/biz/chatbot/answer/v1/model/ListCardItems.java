package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import java.util.Map;

import lombok.Data;

@Data
public class ListCardItems {

	private String title;
	
	private String description;
	
	private String imageUrl;
	
	private Map<String,Object> link;
	
	//private int lcSeq;
	
}
