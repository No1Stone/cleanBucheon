package com.bucheon.clean.town.biz.environmental.campaign.v1.controller;

import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignReq;
import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignRes;
import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignVO;
import com.bucheon.clean.town.biz.environmental.campaign.v1.service.CampaignService;
import com.bucheon.clean.town.biz.environmental.report.v1.model.*;
import com.bucheon.clean.town.biz.environmental.report.v1.service.ReportService;
import com.bucheon.clean.town.biz.operation.declarationPoint.v1.service.DeclarationPointService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.mileage.v1.service.UserMileageService;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.clean.town.biz.operation.personalInfo.v1.service.PersonalInfoLogService;
import com.bucheon.clean.town.biz.operation.point.v1.service.PointService;
import com.bucheon.clean.town.biz.operation.user.v1.service.UserService;
import com.bucheon.clean.town.common.type.CampaignStatus;
import com.bucheon.clean.town.common.util.ExcelXlsView;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.TrashType;
import com.bucheon.clean.town.db.entity.operation.User;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/environmental/campaign")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CampaginController {

    private final Logger logger = LoggerFactory.getLogger(CampaginController.class);
    private final CampaignService campaignService;
    private final DeclarationPointService declarationPointService;
    private final ReportService reportService;
    private final UserService userService;
    private final PointService pointService;
    private final UserMileageService userMileageService;
    private final PermissionService permissionService;
    private final MenuService menuService;
    private final PersonalInfoLogService personalInfoLogService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        return new ModelAndView("redirect:/environmental/campaign/info");
    }

    //기본틀 
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(CampaignReq dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(22L);

        List<String> excelMonth = new ArrayList<>();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        return new ModelAndView("environmental/campaign/campaignManage")
                .addObject("campaignPaging", campaignService.CampaginSelectListPagingInfoService(dto))
                .addObject("adminInfo",admin)
                .addObject("trashTypeList",pointService.TrashSelectUseAllListService())
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(CampaignReq dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        String sessionAdministrativeDivision = ((Admin) principal).getAdministrativeDivision();
        dto.setSessionAdministrativeDivision(sessionAdministrativeDivision);

        return new ModelAndView("environmental/campaign/campaignListAjax")
                .addObject("campaignList", campaignService.CampaginSelectListService(dto))
                .addObject("campaignPaging", campaignService.CampaginSelectListPagingInfoService(dto))
                ;
    }

    //최초 입력 페이지 팝업 - 필요없음
//    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
//    public ModelAndView writeForm() {
//        return new ModelAndView("environmental/campaign/campaignManagePopupAjax")
//                .addObject("trashList", reportService.SelectTrashType())
//                ;
//    }

    //수정 입력 페이지 팝업
    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView modifyForm() {
        return new ModelAndView("environmental/campaign/campaignManagePopupAjax");
    }

    //상세보기 팝업
    @RequestMapping(path = "/viewForm", method = RequestMethod.POST)
    public ModelAndView viewForm(Long seq) throws Exception {

        //챗봇, 밴드, 전화 접수 내용
        CampaignRes campaignRes = campaignService.CampaignSelectOneService(seq);

        User user = new User();
        if(campaignRes.getRegUserDid() != null && !campaignRes.getRegUserDid().equals("")){
            try{
                user = userService.UserSelectOneService(campaignRes.getRegUserDid());
            } catch (Exception e) {
            }
        }

        int declarationPoint = 0;
        TrashType trashType = pointService.TrashSelectOneService(campaignRes.getTrashType());
        if(campaignRes.getCampaignStatus().equals(CampaignStatus.T.getName())){
            declarationPoint = trashType.getDeclarationMileage();
        } else if(campaignRes.getCampaignStatus().equals(CampaignStatus.C.getName())){
            declarationPoint = trashType.getProcessMileage();
        }

        //logger.info("-- declarationPoint : {}", declarationPoint);
        //줄바꿈
        val nlString = System.getProperty("line.separator").toString();

        return new ModelAndView("environmental/campaign/campaignManageViewAjax")
                .addObject("campaignOne", campaignRes)
                .addObject("campaignLogList", campaignService.CampaignLogSelectService(campaignRes.getCampaignSeq()))
                .addObject("trashList", reportService.SelectTrashType())
                .addObject("declarationPoint", declarationPoint)
                .addObject("userOne", user)
                .addObject("nlString", nlString)
                ;
    }

    //저장처리
//    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
//    @ResponseBody
//    public Object insertInfo(ReportSave dto) {
//
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        if(principal == "anonymousUser"){
//            return new ModelAndView("redirect:/login/info");
//        }
//
//        reportService.ReportSaveService(dto);
//        HashMap<String, Object> map = new HashMap<String, Object>();
//        map.put("result", "success");
//        return map;
//    }

//    수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(CampaignVO dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        //내용 업데이트
        campaignService.UpdateCampaign(dto);
        CampaignRes campaignRes = campaignService.CampaignSelectOneService(dto.getCampaignSeq());

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //삭제처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(Long[] campaignSeq) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        campaignService.UpdateCampaignDelService(campaignSeq);
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //완료처리
    @RequestMapping(path = "/completeInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object completeInfo(Long[] seq) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        campaignService.UpdateCampaignCompleteService(seq);
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //상세보기 팝업
    @RequestMapping(path = "/viewPersonalInfo", method = RequestMethod.POST)
    public ModelAndView viewPersonalInfo(Long seq, String pageType) throws Exception {

        //로그 저장장
        PersonalSearchReq dto = new PersonalSearchReq();

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;

        String menu = "Campaign";
        String menuhan = "캠페인 활동 관리";
        String reason = admin.getAdminName() +  " : 캠페인 활동 접수자 확인";


        dto.setLogTime(LocalDateTime.now());
        dto.setAdminSeq(admin.getAdminSeq());
        dto.setMenuName(menu);
        dto.setMenuHanName(menuhan);
        dto.setSeq(seq);
        dto.setReason(reason);
        personalInfoLogService.PersonalSearchSaveService(dto);

        CampaignRes campaignRes = campaignService.CampaignSelectOneService(seq);

        User user = userService.UserSelectOneService3(campaignRes.getRegUserDid());


        return new ModelAndView("operation/personalInfo/personalInfoPopViewAjax")
                .addObject("userName", user.getName())
                .addObject("userPhone", user.getPhone())
                .addObject("userEmail", user.getEmail())
                ;
    }

    //EXCEL DOWN
    @PostMapping("/downExcel")
    public ModelAndView getExcelDownList(CampaignReq dto) {
        Map<String, Object> excelData = null;
        try {
            excelData = campaignService.downExcelList(dto);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView(new ExcelXlsView(), excelData);
    }

}
