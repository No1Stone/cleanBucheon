package com.bucheon.clean.town.db.repository.operation.dsl;

import com.bucheon.clean.town.biz.operation.permission.v1.model.PermissionReq;
import com.bucheon.clean.town.db.entity.operation.Permission;
import com.querydsl.core.QueryResults;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PermissionRepositoryDsl  {

    @Transactional(readOnly = true)
    List<Permission> PermissionSelectList(PermissionReq dto);


    @Transactional(readOnly = true)
    QueryResults<Permission> PermissionSelectListPaging(PermissionReq dto);
}
