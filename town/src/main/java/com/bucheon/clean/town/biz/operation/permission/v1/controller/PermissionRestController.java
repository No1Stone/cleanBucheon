package com.bucheon.clean.town.biz.operation.permission.v1.controller;

import com.bucheon.clean.town.biz.operation.permission.v1.model.PermissionReq;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/operation/permission/rest")
@RequiredArgsConstructor
public class PermissionRestController {

    private final Logger logger = LoggerFactory.getLogger(PermissionRestController.class);
    private final PermissionService permissionService;

    @PostMapping(path = "/test1")
    public Object PermissionListSelectController(@RequestBody PermissionReq dto){
        return  permissionService.PermissionListSelectService(dto);
    }

}
