package com.bucheon.clean.town.db.entity.metabus.Id;

import lombok.Data;

import java.io.Serializable;

@Data
public class MetaPushLogId implements Serializable {
    /**
     * 부천시 민원 접수 데이터
     */
    private Long sendSeq;
    private String userId;

}
