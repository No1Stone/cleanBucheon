package com.bucheon.clean.town.db.repository.chatbot;

import com.bucheon.clean.town.db.entity.chatbot.SkllText;
import com.bucheon.clean.town.db.entity.chatbot.id.SkllTextId;
import com.bucheon.clean.town.db.repository.chatbot.dsl.SkllTextRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface SkllTextRepository extends JpaRepository<SkllText, SkllTextId>, SkllTextRepositoryDsl {

    Optional<SkllText> findBySkllCodeAndRspnsOrdr(String skllCode, int RspnsOr);

    @Transactional
    @Modifying
    @Query(
            value = "delete from tb_skll_text where SKLL_CODE =:answerSeq "
            , nativeQuery = true
    )
    int DeleteAnswerSkllTextCode(@Param("answerSeq") String answerSeq);

}
