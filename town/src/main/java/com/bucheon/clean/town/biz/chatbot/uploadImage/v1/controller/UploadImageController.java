package com.bucheon.clean.town.biz.chatbot.uploadImage.v1.controller;

import com.bucheon.clean.town.biz.chatbot.uploadImage.v1.model.UploadImageVO;
import com.bucheon.clean.town.biz.chatbot.uploadImage.v1.service.UploadImageService;
import com.bucheon.clean.town.biz.environmental.report.v1.service.ReportService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.clean.town.biz.operation.point.v1.model.TrashTypeUpdate;
import com.bucheon.clean.town.biz.util.service.UtilService;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.repository.chatbot.UploadImageRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/chatbot/uploadImage")
@RequiredArgsConstructor
public class UploadImageController {

    @Value("${domain.web_url}")
    private String upload_path_webUrl;

    @Value("${domain.file_domain}")
    private String upload_domain;

    @Value("${domain.service_domain}")
    private String service_domain;

    private final Logger logger = LoggerFactory.getLogger(UploadImageController.class);
    private final UploadImageService uploadImageService;
    private final UtilService utilService;
    private final PermissionService permissionService;
    private final MenuService menuService;


    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/chatbot/uploadImage/info");
    }

    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(UploadImageVO dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(23L);

        return new ModelAndView("chatbot/uploadImage/UploadImageManage")
                .addObject("imagePaging", uploadImageService.UploadImageSelectListPagingInfoService(dto))
                .addObject("adminInfo", admin)
                .addObject("selectMenu",menu)
                ;
    }

    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView infoAjax(@ModelAttribute("dto") UploadImageVO dto){

        return new ModelAndView("chatbot/uploadImage/UploadImageListAjax")
                .addObject("imageList", uploadImageService.UploadImageSelectListService(dto) )
                .addObject("imagePaging", uploadImageService.UploadImageSelectListPagingInfoService(dto))
                .addObject("upload_path_webUrl", upload_path_webUrl + "")
                .addObject("upload_domain", upload_domain)
                .addObject("service_domain", service_domain)
                ;
    }

    //최초 입력 페이지 팝업 - 필요없음
    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm() {
        return new ModelAndView("chatbot/uploadImage/UploadImageManagePopupAjax")
                .addObject("UploadImageVO", new UploadImageVO())
                ;
    }

    //수정 페이지 팝업 - 필요없음
    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView modifyForm(UploadImageVO dto) {
        return new ModelAndView("chatbot/uploadImage/UploadImageManagePopupAjax")
                .addObject("UploadImageVO", uploadImageService.UploadImageSelectOneService(dto.getImageSeq()) )
                ;
    }

    //저장처리
    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(@RequestParam("uploadfile") MultipartFile[] uploadfile, UploadImageVO dto) throws IllegalStateException, IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();

        try {
            List<String> fileName = utilService.FileSaveService(uploadfile, "chatbot");
            dto.setImageFilename(uploadfile[0].getOriginalFilename());
            dto.setImageFileservername(fileName.get(0));
            dto.setSize(fileName.size());
            dto.setImageId(uploadImageService.getImageIdFromDB());
        } catch (Exception e){
            //
        }
        uploadImageService.UploadImageSaveService(dto);
        map.put("result", "success");
        return map;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(@RequestParam("uploadfile") MultipartFile[] uploadfile, UploadImageVO dto) throws IllegalStateException, IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();

        try {
            List<String> fileName = utilService.FileSaveService(uploadfile, "chatbot");
            dto.setImageFilename(uploadfile[0].getOriginalFilename());
            dto.setImageFileservername(fileName.get(0));
            dto.setSize(fileName.size());
        } catch (Exception e){
            //
        }
        uploadImageService.UploadImageSaveService(dto);
        map.put("result", "success");
        return map;
    }

    //삭제처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(Long imageSeq){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        //System.out.println("?>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + imageSeq);

        uploadImageService.UploadImageDelService(imageSeq);
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }
    
}
