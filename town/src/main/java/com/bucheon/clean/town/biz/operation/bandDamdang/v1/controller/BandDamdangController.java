package com.bucheon.clean.town.biz.operation.bandDamdang.v1.controller;

import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminReq;
import com.bucheon.clean.town.biz.operation.admin.v1.service.AdminService;
import com.bucheon.clean.town.biz.operation.bandDamdang.v1.model.BandDamdangReq;
import com.bucheon.clean.town.biz.operation.bandDamdang.v1.service.BandDamdangService;
import com.bucheon.clean.town.biz.operation.damdang.v1.model.DamdangReq;
import com.bucheon.clean.town.biz.operation.damdang.v1.service.DamdangService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
@RequestMapping(path = "/operation/bandDamdang")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BandDamdangController {

    private final Logger logger = LoggerFactory.getLogger(BandDamdangController.class);
    private final AdminService adminService;
    private final MenuService menuService;
    private final BandDamdangService bandDamdangService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/operation/bandDamdang/bandDamdangManage");
    }

    //기본틀
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(AdminReq dto, HttpServletRequest request) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(33L);

        return new ModelAndView("operation/bandDamdang/bandDamdangManage")
                .addObject("damdangList", bandDamdangService.BandDamdangSelectListService())
                .addObject("adminList", adminService.AdminSelectAllListService())
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(BandDamdangReq dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result",   bandDamdangService.BandDamdangUpdateAndSave(dto));
        return map;
    }

}
