package com.bucheon.clean.town.biz.metabus.metaBoard.v1.service;

import com.bucheon.clean.town.biz.metabus.metaBoard.v1.model.*;
import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.metabus.MetaBoard;
import com.bucheon.clean.town.db.entity.metabus.MetaNoticePopup;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.repository.metabus.MetaBoardAttRepository;
import com.bucheon.clean.town.db.repository.metabus.MetaBoardRepository;
import com.bucheon.clean.town.db.repository.metabus.MetaNoticePopupRepository;
import com.bucheon.clean.town.db.repository.operation.AdminRepository;
import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MetaNoticePopupService {

    private final Logger logger = LoggerFactory.getLogger(MetaNoticePopupService.class);
    private final HttpServletRequest httpServletRequest;
    private final MetaNoticePopupRepository metaNoticePopupRepository;
    private final AdminRepository adminRepository;
    private final ModelMapper modelMapper;



    public MetaNoticePopupVO MetaNoticePopupSelectOneService(String type) {
        MetaNoticePopupVO result = modelMapper.map(metaNoticePopupRepository.findByPopupType(type).orElseThrow(IllegalArgumentException::new), MetaNoticePopupVO.class);
        logger.info(">>>>>> result : {}",  new Gson().toJson(result));

        return result;
    }

    @Transactional
    public int UpdateMetaNoticePopup(MetaNoticePopupVO dto) {
        int result = 0;
        if(dto.getPopupType() != null) {

            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            Admin admin = (Admin) principal;

            metaNoticePopupRepository.UpdateMetaPopupImage(dto.getPopupImageUrl(), dto.getPopupType());
            metaNoticePopupRepository.UpdateMetaPopupUse(dto.getUseYn(), dto.getPopupType());
            metaNoticePopupRepository.UpdateMetaMod(admin.getAdminSeq(), dto.getPopupType());

        }
        return result;
    }
}
