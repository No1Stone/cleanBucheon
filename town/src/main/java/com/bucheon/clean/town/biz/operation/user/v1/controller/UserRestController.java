package com.bucheon.clean.town.biz.operation.user.v1.controller;

import com.bucheon.clean.town.biz.operation.user.v1.model.UserReq;
import com.bucheon.clean.town.biz.operation.user.v1.service.UserService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/user/rest")
@RequiredArgsConstructor
public class UserRestController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;

    @PostMapping(path = "/test1")
    public Object test1(@RequestBody UserReq dto){

        return userService.UserSelectListService(dto);
    }

    @PostMapping(path = "/test2")
    public Object test2(@RequestBody UserReq dto){

        return userService.UserSelectListPagingService(dto);
    }
}
