package com.bucheon.clean.town.db.repository.chatbot.dsl;

import com.bucheon.clean.town.biz.chatbot.answer.v1.model.BasicCardVO;
import com.bucheon.clean.town.biz.chatbot.answer.v1.model.CommerceCardVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SkllCmercRepositoryDsl {

    @Transactional(readOnly = true)
    List<CommerceCardVO> selectSkllCcCodeAndRspnsOrdr(String code, int reqSeq);
}
