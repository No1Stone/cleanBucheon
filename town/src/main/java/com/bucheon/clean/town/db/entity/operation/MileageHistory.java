package com.bucheon.clean.town.db.entity.operation;

import com.bucheon.clean.town.db.entity.operation.id.MileageHistoryId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_mileage_history")
@Getter
@Setter
@NoArgsConstructor
@IdClass(MileageHistoryId.class)
public class MileageHistory {

    @Id
    @GeneratedValue(generator = "admin_mileage_seq")
    @Column(name = "mileage_seq", nullable = true)
    private Long mileageSeq;
    @Column(name = "user_id", nullable = true)
    private String userId;
    @Column(name = "report_seq", nullable = true)
    private String reportSeq;
    @Column(name = "mileage", nullable = true)
    private Long mileage;
    @Column(name = "mileage_type", nullable = true)
    private String mileageType;
    @Column(name = "complaints_dt", nullable = true)
    private LocalDateTime complaintsDt;
    @Column(name = "admin_seq", nullable = true)
    private Long adminSeq;
    @Column(name = "send_date", nullable = true)
    private LocalDateTime sendDate;


    @Builder
    MileageHistory(
            Long mileageSeq,
            String userId,
            String reportSeq,
            Long mileage,
            String mileageType,
            LocalDateTime complaintsDt,
            Long adminSeq,
            LocalDateTime sendDate

    ) {
        this.mileageSeq = mileageSeq;
        this.userId = userId;
        this.reportSeq = reportSeq;
        this.mileage = mileage;
        this.mileageType = mileageType;
        this.complaintsDt = complaintsDt;
        this.adminSeq = adminSeq;
        this.sendDate = sendDate;
    }
}
