package com.bucheon.clean.town.db.repository.metabus;

import com.bucheon.clean.town.db.entity.metabus.MetaUserVoteHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

public interface MetaUserVoteHistoryRepository extends JpaRepository<MetaUserVoteHistory, String> {

    @Query(
            value = "select count(*) from tb_meta_user_vote_history where user_id =:userId and board_seq =:boardSeq and date_format(reg_dt, '%Y-%m-%d') =:date"
            , nativeQuery = true
    )
    int countByUserIdAndBoardSeqAndRegDt(@Param("userId") String userId, @Param("boardSeq") Long boardSeq, @Param("date") LocalDate date);
}
