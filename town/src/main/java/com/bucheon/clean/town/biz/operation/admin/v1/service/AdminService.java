package com.bucheon.clean.town.biz.operation.admin.v1.service;

import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminReq;
import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminRes;
import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminUpdate;
import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.Permission;
import com.bucheon.clean.town.db.repository.operation.AdminRepository;
import com.bucheon.clean.town.db.repository.operation.MenuRepository;
import com.bucheon.clean.town.db.repository.operation.PermissionMenuRepository;
import com.bucheon.clean.town.db.repository.operation.PermissionRepository;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AdminService {

    private final Logger logger = LoggerFactory.getLogger(AdminService.class);
    private final HttpServletRequest httpServletRequest;
    private final AdminRepository adminRepository;
    private final MenuRepository menuRepository;
    private final PermissionRepository permissionRepository;
    private final PermissionMenuRepository permissionMenuRepository;
    private final ModelMapper modelMapper;
    private final PasswordEncoder encoder;

    @Transactional(readOnly = true)
    public Object AdminSelectListService(AdminReq dto) {
        var permission = permissionRepository.findAll()
                .stream().collect(Collectors.toMap(Permission::getPermissionSeq, Permission::getPermissionName));
        var result = adminRepository.AdminSelectList(dto)
                .stream().map(e -> modelMapper.map(e, AdminRes.class))
                .peek(f -> {
                    f.setPermission(permission.get(f.getPermissionSeq()));
                    f.setMenuPermission(menuRepository.findByMenuSeqInAndUseYnAndDelYnAndMenuLevel
                                    (permissionMenuRepository.findByPermissionSeqAndUseYn(f.getPermissionSeq(), "Y").stream().map(e -> e.getMenuSeq())
                                            .collect(Collectors.toList()), "Y", "N", 1)
                            .stream().map(e -> e.getMenuName()).collect(Collectors.joining(", ")));
                })
                .collect(Collectors.toList());
        return result;
    }

    @Transactional(readOnly = true)
    public Paging AdminSelectListPagingService(AdminReq dto) {
        QueryResults<Admin> result = adminRepository.AdminSelectListPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    @Transactional(readOnly = true)
    public Object AdminSelectAllListService() {
        var result = adminRepository.findByUseYnAndDelYnOrderByAdminNameAsc("Y","N")
                .stream().map(e -> modelMapper.map(e, AdminRes.class))
                .collect(Collectors.toList());
        return result;
    }

    @Transactional(readOnly = true)
    public Object AdminSelectOneService(Long seq) {

        Admin admin = adminRepository.findByAdminSeq(seq).orElseThrow(() -> new IllegalArgumentException());

        AdminUpdate ad = modelMapper.map(admin, AdminUpdate.class);
        ad.setAdminPassword("********");
        ad.setPermission(permissionRepository.findById(ad.getPermissionSeq())
                .orElseThrow(() -> new IllegalArgumentException()).getPermissionName());
        return ad;
    }

    @Transactional(readOnly = true)
    public String idcheck(String id) {
        String isId = "Y";
        if (adminRepository.existsByAdminId(id)) {
            isId = "Y";
        } else {
            isId = "N";
        }
        return isId;
    }

    @Transactional
    public String AdminUpdateAndSave(AdminUpdate dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;

        int count = 0;
        int up = 0;
        if (dto.getAdminSeq() == null) {
            dto.setRegSeq(admin.getAdminSeq());
            dto.setModSeq(admin.getAdminSeq());
            dto.setRegDt(LocalDateTime.now());
            dto.setModDt(LocalDateTime.now());

            Admin a = adminRepository.save(dto.ofAdmin(encoder));
            if (a.getAdminSeq() != null) {
                count = 1;
            }
        } else {

            if (!StringUtils.isEmptyOrWhitespace(dto.getAdminName())) {
                up = adminRepository.updateAdminName(dto.getAdminName(), dto.getAdminSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getAdminPassword(encoder)) && dto.getAdminPassword(encoder) != null) {
                up = adminRepository.updateAdminPassword(dto.getAdminPassword(encoder), dto.getAdminSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getAdminEmail())) {
                up = adminRepository.updateAdminEmail(dto.getAdminEmail(), dto.getAdminSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getAdministrativeDivision())) {
                up = adminRepository.updateAdministrativeDivision(dto.getAdministrativeDivision(), dto.getAdminSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getAdminPhone())) {
                up = adminRepository.updateAdminPhone(dto.getAdminPhone(), dto.getAdminSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getSuperYn())) {
                up = adminRepository.updateAdminSuperYn(dto.getSuperYn(), dto.getAdminSeq());
                count += up;
            }
            if (dto.getPermissionSeq() != null) {
                up = adminRepository.updateAdminPermissionSeq(dto.getPermissionSeq(), dto.getAdminSeq());
                count += up;
            }
            if (dto.getDelYn() != null) {
                for(Long e : dto.getDelYn()){
                    up = adminRepository.UpdateAdminDelYn(YnType.Y.getName(), e);
                }
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getUseYn())) {
                up = adminRepository.UpdateAdminDelYn(dto.getUseYn(), dto.getAdminSeq());
                count += up;
            }

            //총정리
            if (count > 0) {
                dto.setModSeq(admin.getAdminSeq());
                adminRepository.UpdateMod(dto.getModSeq(), dto.getAdminSeq());
            }
        }
        if (count > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    public int UpdateAdminDelService(Long[] reportSeql) {
        List<Integer> result =  Arrays.stream(reportSeql).map(e -> adminRepository
                .UpdateAdminDelYn(YnType.Y.getName(), e)).collect(Collectors.toList());
        return result.size();
    }

    public Admin AdminSelectDamdangOneService(String areaName) {
        Admin admin = adminRepository.selectAdministrativeDivision(areaName).orElseThrow(() -> new IllegalArgumentException());

        return admin;
    }

}
