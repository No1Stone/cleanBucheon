package com.bucheon.clean.town.db.entity.operation;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_declaration_point")
@Getter
@Setter
@NoArgsConstructor
public class DeclarationPoint {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "declaration_seq", nullable = true)
    private Long declarationSeq;
    @Column(name = "declaration_point", nullable = true)
    private int declarationPoint;
    @Column(name = "reg_seq", nullable = true)
    private Long regSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_seq", nullable = true)
    private Long modSeq;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    DeclarationPoint(
            Long declarationSeq,
            int declarationPoint,
            Long regSeq,
            LocalDateTime regDt,
            Long modSeq,
            LocalDateTime modDt
    ) {
        this.declarationSeq = declarationSeq;
        this.declarationPoint = declarationPoint;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.modSeq = modSeq;
        this.modDt = modDt;
    }


}
