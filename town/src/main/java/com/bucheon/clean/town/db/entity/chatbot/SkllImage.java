package com.bucheon.clean.town.db.entity.chatbot;

import com.bucheon.clean.town.db.entity.chatbot.id.SkllCmercId;
import com.bucheon.clean.town.db.entity.chatbot.id.SkllImageId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_skll_image")
@Getter
@Setter
@NoArgsConstructor
@IdClass(SkllImageId.class)
public class SkllImage {

    @Id
    @Column(name = "skll_code", nullable = true)
    private String skllCode;
    @Column(name = "image_url", nullable = true)
    private String imageUrl;
    @Column(name = "image_dc", nullable = true)
    private String imageDc;
    @Id
    @Column(name = "rspns_ordr", nullable = true)
    private int rspnsOrdr;

    @Builder
    SkllImage(
            String skllCode,
            String imageUrl,
            String imageDc,
            int rspnsOrdr
            ) {
        this.skllCode=skllCode;
        this.imageUrl=imageUrl;
        this.imageDc=imageDc;
        this.rspnsOrdr=rspnsOrdr;
    }

}
