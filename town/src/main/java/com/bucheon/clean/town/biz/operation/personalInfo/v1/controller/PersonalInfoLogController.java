package com.bucheon.clean.town.biz.operation.personalInfo.v1.controller;

import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.mileage.v1.model.UserMileageHistoryVO;
import com.bucheon.clean.town.biz.operation.mileage.v1.service.UserMileageHistoryService;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.clean.town.biz.operation.personalInfo.v1.model.PersonalInfoLogReq;
import com.bucheon.clean.town.biz.operation.personalInfo.v1.service.PersonalInfoLogService;
import com.bucheon.clean.town.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "/operation/personalInfo")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PersonalInfoLogController {

    private final Logger logger = LoggerFactory.getLogger(PersonalInfoLogController.class);
    private final PersonalInfoLogService personalInfoLogService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/operation/personalInfo/info");
    }

    //기본틀
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(PersonalInfoLogReq dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(34L);

        return new ModelAndView("operation/personalInfo/personalInfoLogManage")
                .addObject("personalInfoLogPaging", personalInfoLogService.PersonalInfoLogListSelectPagingService(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(PersonalInfoLogReq dto){
        return new ModelAndView("operation/personalInfo/personalInfoLogListAjax")
                .addObject("personalInfoLogList", personalInfoLogService.PersonalInfoLogListSelectService(dto))
                .addObject("personalInfoLogPaging", personalInfoLogService.PersonalInfoLogListSelectPagingService(dto))
                ;

    }


}
