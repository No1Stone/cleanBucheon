package com.bucheon.clean.town.db.repository.metabus;

import com.bucheon.clean.town.db.entity.metabus.MetaBoardAtt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MetaBoardAttRepository extends JpaRepository<MetaBoardAtt, Long> {

    boolean existsByBoardSeq(Long seq);


    List<MetaBoardAtt> findByBoardSeq(Long reSeq);

    //첨부파일 리스트 가져오기
    List<MetaBoardAtt> findByBoardSeqOrderByBoardAttOrderAsc(Long reSeq);

    //첨부파일 리스트 가져오기
    List<MetaBoardAtt> findByBoardSeqAndBoardAttOrderGreaterThanEqualOrderByBoardAttOrderAsc(Long reSeq, int order);

    //썸네일 가져오기
    MetaBoardAtt findByBoardSeqAndBoardAttOrder(Long reSeq, int order);

    MetaBoardAtt findFirstByBoardSeqOrderByBoardAttOrderDesc(Long reSeq);

    int countByBoardSeq(Long seq);

    @Transactional
    int deleteByBoardSeq(Long reSeq);

    @Transactional
    int deleteByBoardAttSeq(Long reSeq);

    @Transactional
    @Modifying
    @Query( value = "update tb_meta_board_att set board_att_order = :boardAttOrder where board_att_seq =:boardAttSeq "
            , nativeQuery = true
    )
    int updateBoardAttOrder(@Param("boardAttSeq") Long boardAttSeq, @Param("boardAttOrder") int boardAttOrder);


}
