package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 스킬관리  > 스킬매니저 RVO
 * @author user
 *
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SkillManageRVO {

	/* 번호 */
	private String rownum;

	/* 스킬명 */
	private String skillName;

	/* 스킬 코드 */
	private String skillCode;

	/* 응답타입 */
	private String resType;

	/* 응답타입 */
	private String resType1;

	/* 응답타입 */
	private String resType2;

	/* 응답타입 */
	private String resType3;

	/* 등록 날짜 */
	private String regDate;
	private String registDe;

	/* 수정 날짜 */
	private String upDate;
	private String updtDe;

	/* 답변 카테고리 */
	private String skillcategory;

	/* 답변 카테고리 */
	private String skillUrl;

	private int resSeq;

	/* 답변 레벨 */
	private String skillLevel;
	private String skillLevel1;
	private String skillLevel2;
	private String skillLevel3;
	private String skillLevel4;

	/* updateSkillSTExcel */
	private String text;

	/* updateSkillBCExcel */
	private String title;
	private String thumbnail;
	private String profile;
	private String social;
	private String button;
	private String description;
	private String bcSeq;
	private String optnAt;
	private String proflCrtfcAt;

	/* updateSkillQRExcel */
	private String label;
	private String action;
	private String messagetext;
	private String blockid;
	private String qrSeq;

	/* updateSkillSIExcel */
	private String imageurl;
	private String alttext;


	/* updateSkillCCExcel */
	private String header;
	private String items;

	/* updateSkillLCExcel */
	private String price;
	private String discount;
	private String currency;
	private String discountrate;
	private String discountedprice;
	private String ccSeq;

	/* 스킬명 */
	private String blockName;

	/* 서브시스템 */
	private String subSystem;

	@Builder
	public SkillManageRVO(
			String rownum		,
			String skillName 	,
			String skillCode 	,
			String resType 		,
			String resType1 	,
			String resType2 	,
			String resType3 	,
			String regDate 		,
			String registDe 	,
			String upDate 		,
			String updtDe 		,
			String skillcategory 	,
			String skillUrl 	,
			int    resSeq 		,
			String skillLevel 	,
			String skillLevel1 	,
			String skillLevel2 	,
			String skillLevel3 	,
			String skillLevel4 	,
			String text 		,
			String title 		,
			String thumbnail 	,
			String profile 		,
			String social 		,
			String button 		,
			String description 	,
			String bcSeq 		,
			String optnAt 		,
			String proflCrtfcAt 	,
			String label 		,
			String action 		,
			String messagetext 	,
			String blockid 		,
			String qrSeq 		,
			String imageurl 	,
			String alttext 		,
			String header 		,
			String items 		,
			String price 		,
			String discount 	,
			String currency 	,
			String discountrate 	,
			String discountedprice 	,
			String ccSeq 		,
			String blockName 	,
			String subSystem
			) {
			this.rownum		 = 	rownum		;
			this.skillName 	 = 	skillName 	;
			this.skillCode 	 = 	skillCode 	;
			this.resType 		 = 	resType 	;
			this.resType1 	 = 	resType1 	;
			this.resType2 	 = 	resType2 	;
			this.resType3 	 = 	resType3 	;
			this.regDate 		 = 	regDate 	;
			this.registDe 	 = 	registDe 	;
			this.upDate 		 = 	upDate 		;
			this.updtDe 		 = 	updtDe 		;
			this.skillcategory 	 = 	skillcategory 	;
			this.skillUrl 	 = 	skillUrl 	;
			this.resSeq 		 = 	resSeq 		;
			this.skillLevel 	 = 	skillLevel 	;
			this.skillLevel1 	 = 	skillLevel1 	;
			this.skillLevel2 	 = 	skillLevel2 	;
			this.skillLevel3 	 = 	skillLevel3 	;
			this.skillLevel4 	 = 	skillLevel4 	;
			this.text 		 = 	text 		;
			this.title 		 = 	title 		;
			this.thumbnail 	 = 	thumbnail 	;
			this.profile 		 = 	profile 	;
			this.social 		 = 	social 		;
			this.button 		 = 	button 		;
			this.description 	 = 	description 	;
			this.bcSeq 		 = 	bcSeq 		;
			this.optnAt 		 = 	optnAt 		;
			this.proflCrtfcAt 	 = 	proflCrtfcAt 	;
			this.label 		 = 	label 		;
			this.action 		 = 	action 		;
			this.messagetext 	 = 	messagetext 	;
			this.blockid 		 = 	blockid 	;
			this.qrSeq 		 = 	qrSeq 		;
			this.imageurl 	 = 	imageurl 	;
			this.alttext 		 = 	alttext 	;
			this.header 		 = 	header 		;
			this.items 		 = 	items 		;
			this.price 		 = 	price 		;
			this.discount 	 = 	discount 	;
			this.currency 	 = 	currency 	;
			this.discountrate 	 = 	discountrate 	;
			this.discountedprice 	 = 	discountedprice ;
			this.ccSeq 		 = 	ccSeq 		;
			this.blockName 	 = 	blockName 	;
			this.subSystem 	 = 	subSystem 	;
	}
}
