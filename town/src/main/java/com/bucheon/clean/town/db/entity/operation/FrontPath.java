package com.bucheon.clean.town.db.entity.operation;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_front_path")
@Getter
@Setter
@NoArgsConstructor
public class FrontPath {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "path_seq", nullable = true)
    private Long pathSeq;
    @Column(name = "path_name", nullable = true)
    private String pathName;
    @Column(name = "path_url", nullable = true)
    private String pathUrl;

    @Builder
    FrontPath(
            Long pathSeq,
            String pathName,
            String pathUrl
    ) {
        this.pathSeq = pathSeq;
        this.pathName = pathName;
        this.pathUrl = pathUrl;
    }

}
