package com.bucheon.clean.town.biz.util.model;

import com.bucheon.clean.town.db.entity.etc.ConnectSum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConnectSumVO {

    LocalDate connectDate;
    String connectDateString;
    String connectType;
    int cleanApp;
    int metabus;
    int chatbot;
    int band;
    int self;

    public ConnectSum ofConnectSum(){
        ConnectSum c = new ConnectSum();
        c.setConnectDate(this.connectDate);
        c.setCleanApp(this.cleanApp);
        c.setBand(this.band);
        c.setChatbot(this.chatbot);
        c.setMetabus(this.metabus);
        c.setSelf(this.self);

        return c;
    }
}
