package com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.controller;

import com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model.AlaramReq;
import com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.service.AlaramtalkService;
import com.bucheon.clean.town.biz.environmental.report.v1.model.ReportListReq;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.service.MetaUserService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.clean.town.common.util.ExcelXlsView;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Controller
@RequestMapping(path = "/chatbot/alarmtalk")
@RequiredArgsConstructor
public class AlaramtalkController {

    private final Logger logger = LoggerFactory.getLogger(AlaramtalkController.class);
    private final AlaramtalkService alaramtalkService;
    private final MetaUserService metaUserService;
    private final MenuService menuService;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/chatbot/alarmtalk/alarmtalkManage");
    }

    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(AlaramReq dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(9L);

        if (dto == null) {

            LocalDate now = LocalDate.now();
            String yyyymm = now.format(DateTimeFormatter.ofPattern("yyyyMM"));

            dto.setYyyymm(yyyymm);
            dto.setSdate(yyyymm+"01");
            dto.setEdate(yyyymm+"31");
            dto.setPage(0);
            dto.setSize(10);
        }
        else {
            if(dto.getSize() == 0){
                dto.setSize(10);
            }
            if (dto.getYyyymm() == null) {
                LocalDate now = LocalDate.now();
                String yyyymm = now.format(DateTimeFormatter.ofPattern("yyyyMM"));

                dto.setYyyymm(yyyymm);
                dto.setSdate(yyyymm+"01");
                dto.setEdate(yyyymm+"31");
            } else {
                dto.setSdate(dto.getYyyymm()+"01");
                dto.setEdate(dto.getYyyymm()+"31");
            }
        }

        return new ModelAndView("chatbot/alarmtalk/alarmtalkManage")
                .addObject("alarmtalkPaging", alaramtalkService.selectMsgLogPagingInfoService(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                .addObject("yyyymm", dto.getYyyymm())
                ;
    }

    @RequestMapping(path = "/infoAjax", method = RequestMethod.POST)
    public ModelAndView infoAjax(AlaramReq dto){

        logger.info("-- dto : {}", new Gson().toJson(dto));

        if (dto == null) {

            LocalDate now = LocalDate.now();
            String yyyymm = now.format(DateTimeFormatter.ofPattern("yyyyMM"));

            dto.setYyyymm(yyyymm);
            dto.setSdate(yyyymm+"01");
            dto.setEdate(yyyymm+"31");
            dto.setPage(0);
            dto.setSize(10);
        }
        else {
            if(dto.getSize() == 0){
                dto.setSize(10);
            }
            if (dto.getYyyymm() == null) {
                LocalDate now = LocalDate.now();
                String yyyymm = now.format(DateTimeFormatter.ofPattern("yyyyMM"));

                dto.setYyyymm(yyyymm);
                dto.setSdate(yyyymm+"01");
                dto.setEdate(yyyymm+"31");
            } else {
                dto.setSdate(dto.getYyyymm()+"01");
                dto.setEdate(dto.getYyyymm()+"31");
            }
        }

        return new ModelAndView("chatbot/alarmtalk/alarmtalkInfoAjax")
                .addObject("alarmtalkInfo", alaramtalkService.selectMsgLogCountService(dto))
                ;
    }

    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(AlaramReq dto){

        logger.info("-- dto : {}", new Gson().toJson(dto));

        if (dto == null) {

            LocalDate now = LocalDate.now();
            String yyyymm = now.format(DateTimeFormatter.ofPattern("yyyyMM"));

            dto.setYyyymm(yyyymm);
            dto.setSdate(yyyymm+"01");
            dto.setEdate(yyyymm+"31");
            dto.setPage(0);
            dto.setSize(10);
        }
        else {
            if(dto.getSize() == 0){
                dto.setSize(10);
            }
            if (dto.getYyyymm() == null) {
                LocalDate now = LocalDate.now();
                String yyyymm = now.format(DateTimeFormatter.ofPattern("yyyyMM"));

                dto.setYyyymm(yyyymm);
                dto.setSdate(yyyymm+"01");
                dto.setEdate(yyyymm+"31");
            } else {
                dto.setSdate(dto.getYyyymm()+"01");
                dto.setEdate(dto.getYyyymm()+"31");
            }
        }

        return new ModelAndView("chatbot/alarmtalk/alarmtalkListAjax")
                .addObject("alarmtalkList", alaramtalkService.selectMsgLogService(dto))
                .addObject("alarmtalkPaging", alaramtalkService.selectMsgLogPagingInfoService(dto))
                ;
    }

    //EXCEL DOWN
    @PostMapping("/downExcel")
    public ModelAndView getExcelDownList(AlaramReq dto) {

        if (dto == null) {

            LocalDate now = LocalDate.now();
            String yyyymm = now.format(DateTimeFormatter.ofPattern("yyyyMM"));

            dto.setYyyymm(yyyymm);
            dto.setSdate(yyyymm+"01");
            dto.setEdate(yyyymm+"31");
            dto.setPage(0);
            dto.setSize(10);
        }
        else {
            if(dto.getSize() == 0){
                dto.setSize(10);
            }
            if (dto.getYyyymm() == null || dto.getYyyymm().equals("")) {
                LocalDate now = LocalDate.now();
                String yyyymm = now.format(DateTimeFormatter.ofPattern("yyyyMM"));

                dto.setYyyymm(yyyymm);
                dto.setSdate(yyyymm+"01");
                dto.setEdate(yyyymm+"31");
            } else {
                dto.setSdate(dto.getYyyymm()+"01");
                dto.setEdate(dto.getYyyymm()+"31");
            }
        }

        Map<String, Object> excelData = null;
        try {
            excelData = alaramtalkService.downExcelList(dto);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView(new ExcelXlsView(), excelData);
    }

    @RequestMapping(path = "/test44")
    public void test444() {

        AlaramReq dto = new AlaramReq();
        dto.setYyyymm("202207");
        dto.setSdate("20220701");
        dto.setEdate("20220731");
        dto.setPage(0);
        dto.setSize(10);

        alaramtalkService.selectMsgLogService(dto);

        //return msgLogCountVO;
    }

}
