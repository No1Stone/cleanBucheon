package com.bucheon.clean.town.biz.metabus.metaUser.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetaTrashTypeReq {

    private int page;
    private int size;
    private Long trashSeq;
    private String trashName;

}
