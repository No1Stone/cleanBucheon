package com.bucheon.clean.town.db.repository.metabus.dsl;

import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.MetaUserReq;
import com.bucheon.clean.town.biz.operation.user.v1.model.UserReq;
import com.bucheon.clean.town.db.entity.metabus.MetaUser;
import com.bucheon.clean.town.db.entity.operation.User;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MetaUserRepositoryDsl {

    @Transactional(readOnly = true)
    List<MetaUser> MetaUserListSelect(MetaUserReq dto);

    @Transactional(readOnly = true)
    QueryResults<MetaUser> MetaUserListSelectPaging(MetaUserReq dto);

}
