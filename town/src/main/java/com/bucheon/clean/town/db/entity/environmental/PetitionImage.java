package com.bucheon.clean.town.db.entity.environmental;

import com.bucheon.clean.town.db.entity.environmental.id.PetitionImageId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "petition_image")
@Getter
@Setter
@NoArgsConstructor
@IdClass(PetitionImageId.class)
public class PetitionImage {
    /**
     * 부천시 민원 신청 데이터
     */
    @Id
    @Column(name = "uuid", nullable = true)
    private String uuid;
    @Id
    @Column(name = "peti_no", nullable = true)
    private String petiNo;
    @Column(name = "file_name", nullable = true)
    private String fileName;
    @Column(name = "content", nullable = true)
    private String content;


    @Builder
    PetitionImage(
            String uuid,
            String petiNo,
            String fileName,
            String content
    ) {
        this.uuid = uuid;
        this.petiNo = petiNo;
        this.fileName = fileName;
        this.content = content;
    }


}
