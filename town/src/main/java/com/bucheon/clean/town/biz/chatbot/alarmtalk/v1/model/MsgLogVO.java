package com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MsgLogVO {

    private Long msgSeq;
    private String msgNoticetalkTmpKey;
    private String sentDate;
    private String callTo;
    private String sendType;
    private String sendRslt;
    private String rsltCode;
    private String rsltText;

}