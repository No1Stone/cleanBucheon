package com.bucheon.clean.town.db.entity.etc;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_dong_name")
@Getter
@Setter
@NoArgsConstructor
public class DongName {

    @Id
    @Column(name = "name", nullable = true)
    private String name;
    @Column(name = "admin", nullable = true)
    private String admin;


    @Builder
    DongName(
            String name,
            String admin
            ) {
        this.name = name;
        this.admin = admin;
    }

}
