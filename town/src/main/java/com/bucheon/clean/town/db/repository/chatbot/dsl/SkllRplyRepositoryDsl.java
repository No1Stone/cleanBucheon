package com.bucheon.clean.town.db.repository.chatbot.dsl;

import com.bucheon.clean.town.biz.chatbot.answer.v1.model.QuickRepliesVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SkllRplyRepositoryDsl {

    @Transactional(readOnly = true)
    List<QuickRepliesVO> selectQuickReplies(String code);
}
