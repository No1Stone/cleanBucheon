package com.bucheon.clean.town.biz.metabus.metaUser.v1.model;

import com.bucheon.clean.town.db.entity.metabus.MetaUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MetaUserPointReq {

    private String userId;
    private Long trashSeq;
    private String processLocation;

}
