package com.bucheon.clean.town.biz.keyword.v1.mapper;

import com.bucheon.clean.town.biz.keyword.v1.model.InterestWord;
import com.bucheon.clean.town.biz.keyword.v1.model.Stopword;
import com.bucheon.clean.town.biz.keyword.v1.model.WordReq;
import com.bucheon.clean.town.common.util.Paging;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface InterestWordMapper {

    public Paging getTotalSize(WordReq dto);

    public List<InterestWord> getList(WordReq dto);

    public InterestWord getOne(InterestWord dto);

    public Integer save(InterestWord pair);

    public Integer update(InterestWord pair);
    
    public Integer delete(InterestWord pair);
}