package com.bucheon.clean.town.biz.environmental.complaint.v1.controller;

import com.bucheon.clean.town.biz.environmental.complaint.v1.model.ComplaintsReq;
import com.bucheon.clean.town.biz.environmental.complaint.v1.service.ComplaintService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.clean.town.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(path = "/environmental/complaint")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ComplaintController {

    private final Logger logger = LoggerFactory.getLogger(ComplaintController.class);
    private final ComplaintService complaintService;
    private final PermissionService permissionService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/environmental/complaint/menuManage");
    }

    //기본틀 
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(ComplaintsReq dto, HttpServletRequest request){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(4L);

        return new ModelAndView("environmental/complaint/complaintManage")
                .addObject("petitionPaging",complaintService.ReportSelectListPagingInfoService(dto))
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(ComplaintsReq dto){
        return new ModelAndView("environmental/complaint/complaintListAjax")
                .addObject("petitionList",complaintService.ReportSelectListInfoService(dto))
                .addObject("petitionPaging",complaintService.ReportSelectListPagingInfoService(dto))
                ;
    }

    //상세보기 팝업
    @RequestMapping(path = "/viewForm", method = RequestMethod.POST)
    public ModelAndView viewForm(String petiNo) throws Exception {

        //줄바꿈
        val nlString = System.getProperty("line.separator").toString();

        return new ModelAndView("environmental/complaint/complaintManageViewAjax")
                .addObject("petitionOne",complaintService.ReportSelectOneService(petiNo))
                .addObject("nlString", nlString)
                ;
    }

    //저장처리 - 필요없음
    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    public ModelAndView insertInfo(){
        return new ModelAndView("environmental/complaint/insertInfo");
    }

    //수정처리 - 필요없음
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    public ModelAndView updateInfo(){
        return new ModelAndView("environmental/complaint/updateInfo");
    }

    //삭제처리 - 필요없음
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    public ModelAndView deleteInfo(){
        return new ModelAndView("environmental/complaint/deleteInfo");
    }

    @RequestMapping(path = "/editor", method = RequestMethod.GET)
    public ModelAndView testEditor() {
        return new ModelAndView("environmental/complaint/testEditor");
    }

}
