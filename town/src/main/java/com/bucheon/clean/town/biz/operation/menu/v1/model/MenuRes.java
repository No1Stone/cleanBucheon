package com.bucheon.clean.town.biz.operation.menu.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MenuRes {

    private Long menuSeq;
    private String menuName;
    private int menuLevel;
    private Long pmenuSeq;
    private String pmenuName;
    private String menuUrl;
    private String menuIcon;
    private String menuDesc;
    private String useYn;
    private String useYnName;
    private int orderNum;

}
