package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import java.awt.Button;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class ItemCard {

	private ArrayList<Item> itemList;
	private String title;
	private String description;
	private String itemListAlignment;
	private String buttonLayout;
	private Thumbnail thumbnail;
	private String head;
	private Profile profile;
	private ImageTitle imageTitle;
	private ItemListSummary itemListSummary;
	private ArrayList<Button> buttons;

}
