package com.bucheon.clean.town.biz.operation.personalInfo.v1.service;

import com.bucheon.clean.town.biz.environmental.report.v1.model.PersonalSearchReq;
import com.bucheon.clean.town.biz.operation.mileage.v1.model.MileageRequestVO;
import com.bucheon.clean.town.biz.operation.mileage.v1.model.UserMileageVO;
import com.bucheon.clean.town.biz.operation.personalInfo.v1.model.PersonalInfoLogReq;
import com.bucheon.clean.town.biz.operation.personalInfo.v1.model.PersonalInfoLogVO;
import com.bucheon.clean.town.common.util.Masking;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.PersonalInfoLog;
import com.bucheon.clean.town.db.entity.operation.UserMileageHistory;
import com.bucheon.clean.town.db.repository.environmental.ReportRepository;
import com.bucheon.clean.town.db.repository.operation.AdminRepository;
import com.bucheon.clean.town.db.repository.operation.PersonalInfoLogRepository;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PersonalInfoLogService {

    private final Logger logger = LoggerFactory.getLogger(PersonalInfoLogService.class);
    private final HttpServletRequest httpServletRequest;
    private final PersonalInfoLogRepository personalInfoLogRepository;
    private final AdminRepository adminRepository;
    private final ReportRepository reportRepository;
    private final ModelMapper modelMapper;

    public List<PersonalInfoLogVO> PersonalInfoLogListSelectService(PersonalInfoLogReq dto){
        var damdang = adminRepository.findByUseYnAndDelYnOrderByAdminNameAsc("Y","N")
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
        return personalInfoLogRepository.PersonalInfoLogSelectList(dto)
                .stream().map(e -> modelMapper.map(e, PersonalInfoLogVO.class))
                .peek(f -> {
                    try {
                        f.setAdminName(damdang.get(f.getAdminSeq()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(f.getMenuName().equals("Chatbot")
                            || f.getMenuName().equals("Band")
                            || f.getMenuName().equals("Phone")
                            || f.getMenuName().equals("Visit")
                    ) {
                        f.setContents(reportRepository.findByReportSeq(f.getSeq()).getReportContent());
                    } else if(f.getMenuName().equals("UserInfo")) {
                        f.setContents("깨끗한 마을 사용자 정보 확인");
                    }
                    if(f.getMenuName().equals("Campaign")) {
                        f.setContents("캠페인 활동 접수자 정보 확인");
                    }
                })
                .collect(Collectors.toList());
    }

    public Paging PersonalInfoLogListSelectPagingService(PersonalInfoLogReq dto) {

        QueryResults<PersonalInfoLog> result = personalInfoLogRepository.PersonalInfoLogSelectListPaging(dto);

        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }


    public PersonalInfoLog PersonalSearchSaveService(PersonalSearchReq dto) {
        return personalInfoLogRepository.save(dto.ofPersonalInfoLog());
    }
}
