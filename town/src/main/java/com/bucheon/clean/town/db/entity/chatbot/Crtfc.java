package com.bucheon.clean.town.db.entity.chatbot;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "tb_crtfc")
@Getter
@Setter
@NoArgsConstructor
public class Crtfc {

    @Id
    @Column(name = "USRID", nullable = true )
    private String userId;
    @Column(name = "CRTFC_DE", nullable = true)
    private LocalDate crtfcDe;
    @Column(name = "ENCTIME", nullable = true)
    private String enctime;
    @Column(name = "SITECODE", nullable = true)
    private String sitecode;
    @Column(name = "AUTHTYPE", nullable = true)
    private String authtype;
    @Column(name = "NAME", nullable = true)
    private String name;
    @Column(name = "BIRTHDATE", nullable = true)
    private String birthdate;
    @Column(name = "GENDER", nullable = true)
    private String gender;
    @Column(name = "NATIONALINFO", nullable = true)
    private String nationalinfo;
    @Column(name = "MOBILE_NO", nullable = true)
    private String mobileNo;
    @Column(name = "CI", nullable = true)
    private String ci;
    @Column(name = "DI", nullable = true)
    private String di;



    @Builder
    Crtfc(
            String userId,
            LocalDate crtfcDe,
            String enctime,
            String sitecode,
            String authtype,
            String name,
            String birthdate,
            String gender,
            String nationalinfo,
            String mobileNo,
            String ci,
            String di
    ) {
        this.userId = userId;
        this.crtfcDe = crtfcDe;
        this.enctime = enctime;
        this.sitecode = sitecode;
        this.authtype = authtype;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.nationalinfo = nationalinfo;
        this.mobileNo = mobileNo;
        this.ci = ci;
        this.di = di;
    }
}
