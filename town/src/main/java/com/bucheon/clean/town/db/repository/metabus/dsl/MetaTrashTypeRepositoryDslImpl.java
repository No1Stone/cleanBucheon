package com.bucheon.clean.town.db.repository.metabus.dsl;

import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.MetaTrashTypeReq;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.MetaTrashTypeUserRes;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.clean.town.biz.operation.point.v1.model.TrashTypeReq;
import com.bucheon.clean.town.db.entity.metabus.*;
import com.bucheon.clean.town.db.entity.operation.TrashType;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class MetaTrashTypeRepositoryDslImpl implements MetaTrashTypeRepositoryDsl {


    private final JPAQueryFactory queryFactory;
    private QMetaTrashType qMetaTrashType = QMetaTrashType.metaTrashType;
    private QMetaUserPointHistory qMetaUserPointHistory = QMetaUserPointHistory.metaUserPointHistory;
    private QMetaUser qMetaUser = QMetaUser.metaUser;
    private final Logger logger = LoggerFactory.getLogger(MetaTrashTypeRepositoryDslImpl.class);

    @Override
    public List<MetaTrashType> MetaTrashTypeSelectList(MetaTrashTypeReq dto) {

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        List<MetaTrashType> result =  queryFactory.selectFrom(qMetaTrashType)
                .where(eqMetaTrashName(dto.getTrashName()), qMetaTrashType.delYn.eq("N"))
                .offset(dto.getPage() * dto.getSize())
                .limit(dto.getSize())
                .fetch();
        return result;
    }
    @Override
    public QueryResults<MetaTrashType> MetaTrashTypeSelectListPaging(MetaTrashTypeReq dto) {

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        QueryResults<MetaTrashType> result = queryFactory.selectFrom(qMetaTrashType)
                .where(eqMetaTrashName(dto.getTrashName()), qMetaTrashType.delYn.eq("N"))
                .offset(dto.getPage() * dto.getSize())
                .limit(dto.getSize())
                .fetchResults();
        return result;
    }


    @Override
    public List<MetaTrashTypeUserRes> selectListMetaUser(String userId){

        List<MetaTrashTypeUserRes> result =
                queryFactory.select(Projections.bean(MetaTrashTypeUserRes.class,
                                qMetaTrashType.trashSeq,
                                qMetaTrashType.trashName,
                                qMetaTrashType.trashPoint,
                                Expressions.as(Expressions.constant(userId), "userId"),
                                ExpressionUtils.as(
                                        JPAExpressions.select(qMetaUser.nickname)
                                                .from(qMetaUser)
                                                .where(qMetaUser.userId.eq(userId)),
                                        "nickName"),
                                ExpressionUtils.as(
                                JPAExpressions.select(qMetaUserPointHistory.trashSeq.count())
                                        .from(qMetaUserPointHistory)
                                        .where(qMetaUserPointHistory.userId.eq(userId), qMetaUserPointHistory.trashSeq.eq(qMetaTrashType.trashSeq)),
                                        "cleanCnt")
                        ))
                        .from(qMetaTrashType)
                        .where(qMetaTrashType.delYn.eq("N"))
                        .orderBy(qMetaTrashType.trashSeq.asc())
                        .fetch();

        return result;

    }

    private BooleanExpression eqMetaTrashName(String trashName) {
        if (StringUtils.isEmptyOrWhitespace(trashName)) {
            return null;
        }
        return qMetaTrashType.trashName.contains(trashName);
    }
}
