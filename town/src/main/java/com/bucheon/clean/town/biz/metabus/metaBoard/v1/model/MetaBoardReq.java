package com.bucheon.clean.town.biz.metabus.metaBoard.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetaBoardReq {

    private int page;
    private int size;
    private Long boardSeq;
    private String orderStatus;
    private String boardType;
    private String boardTitle;
    private String boardContent;

    private String searchSelect;
    private String searchString;
    private String searchBoardType;
    private String sdate;
    private String edate;


}
