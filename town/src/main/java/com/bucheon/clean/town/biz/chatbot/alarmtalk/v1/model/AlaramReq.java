package com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AlaramReq {

    private int page;
    private int size;

    private String yyyymm;
    private String sdate;
    private String edate;
    private String searchString;

}
