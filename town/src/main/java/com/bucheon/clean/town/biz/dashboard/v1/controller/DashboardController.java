package com.bucheon.clean.town.biz.dashboard.v1.controller;

import com.bucheon.clean.town.biz.environmental.complaint.v1.model.ComplaintsReq;
import com.bucheon.clean.town.biz.environmental.complaint.v1.service.ComplaintService;
import com.bucheon.clean.town.biz.environmental.report.v1.service.ReportService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.clean.town.biz.util.service.ConnectSumService;
import com.bucheon.clean.town.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.List;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

@Controller
@RequestMapping(path = "/home")
@RequiredArgsConstructor
public class DashboardController {

    private final Logger logger = LoggerFactory.getLogger(DashboardController.class);

    private final PermissionService permissionService;
    private final ReportService reportService;
    private final ComplaintService complaintService;
    private final ConnectSumService connectSumService;
    private final MenuService menuService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ModelAndView index(){

        return new ModelAndView("redirect:/home/info");
    }

    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(String sdate, String edate){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(1L);

        LocalDate sdate1 = LocalDate.now();
        if(sdate != null) {
            try {
                sdate1 = LocalDate.parse(sdate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        LocalDate edate1 = LocalDate.now();
        if(edate != null) {
            try {
                edate1 = LocalDate.parse(edate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        LocalDate sdate2 = LocalDate.now();
        LocalDate edate2 = LocalDate.now();

        LocalDate sdate3 = LocalDate.parse(edate1.withDayOfMonth(1)+"");
        LocalDate edate3 = LocalDate.parse(edate1.withDayOfMonth(edate1.lengthOfMonth())+"");

        logger.info("sdate3 : " + sdate3);
        logger.info("edate3 : " + edate3);

//        LocalDate sdate3 = LocalDate.parse("2022-06-01");
//        LocalDate edate3 = LocalDate.parse("2022-06-30");


        return new ModelAndView("dashboard/dashboard")
                .addObject("sdate", sdate1)
                .addObject("edate", edate1)
                .addObject("chatNew", reportService.getChatBotNew(sdate2, edate2))
                .addObject("chatP",reportService.getChatbotP2())
                .addObject("chatC",reportService.getChatbotC(sdate2, edate2))
                .addObject("bandNew",reportService.getBandNew(sdate2, edate2))
                .addObject("bandP",reportService.getBandP2())
                .addObject("bandC",reportService.getBandC(sdate2, edate2))
                .addObject("phoneNew",reportService.getPhoneNew(sdate2, edate2))
                .addObject("phoneP",reportService.getPhoneP2())
                .addObject("phoneC",reportService.getPhoneC(sdate2, edate2))
                .addObject("visitNew",reportService.getVisitNew(sdate2, edate2))
                .addObject("visitP",reportService.getVisitP2())
                .addObject("visitC",reportService.getVisitC(sdate2, edate2))
                .addObject("sinmunNew",complaintService.getSinmunNew(sdate2, edate2))
                .addObject("sinmunP",complaintService.getSinmunP2())
                .addObject("sinmunC",complaintService.getSinmunC(sdate2, edate2))
                .addObject("simgocC",reportService.getSimgocC(sdate3, edate3))
                .addObject("simgocB",reportService.getSimgocB(sdate3, edate3))
                .addObject("simgocP",reportService.getSimgocP(sdate3, edate3))
                .addObject("simgocV",reportService.getSimgocV(sdate3, edate3))
                .addObject("bucheonC",reportService.getBucheonC(sdate3, edate3))
                .addObject("bucheonB",reportService.getBucheonB(sdate3, edate3))
                .addObject("bucheonP",reportService.getBucheonP(sdate3, edate3))
                .addObject("bucheonV",reportService.getBucheonV(sdate3, edate3))
                .addObject("sinjoongC",reportService.getSinjoongC(sdate3, edate3))
                .addObject("sinjoongB",reportService.getSinjoongB(sdate3, edate3))
                .addObject("sinjoongP",reportService.getSinjoongP(sdate3, edate3))
                .addObject("sinjoongV",reportService.getSinjoongV(sdate3, edate3))
                .addObject("joongC",reportService.getJoongC(sdate3, edate3))
                .addObject("joongB",reportService.getJoongB(sdate3, edate3))
                .addObject("joongP",reportService.getJoongP(sdate3, edate3))
                .addObject("joongV",reportService.getJoongV(sdate3, edate3))
                .addObject("sangC",reportService.getSangC(sdate3, edate3))
                .addObject("sangB",reportService.getSangB(sdate3, edate3))
                .addObject("sangP",reportService.getSangP(sdate3, edate3))
                .addObject("sangV",reportService.getSangV(sdate3, edate3))
                .addObject("deaSanC",reportService.getDeaSanC(sdate3, edate3))
                .addObject("deaSanB",reportService.getDeaSanB(sdate3, edate3))
                .addObject("deaSanP",reportService.getDeaSanP(sdate3, edate3))
                .addObject("deaSanV",reportService.getDeaSanV(sdate3, edate3))
                .addObject("sosabonC",reportService.getSosabonC(sdate3, edate3))
                .addObject("sosabonB",reportService.getSosabonB(sdate3, edate3))
                .addObject("sosabonP",reportService.getSosabonP(sdate3, edate3))
                .addObject("sosabonV",reportService.getSosabonV(sdate3, edate3))
                .addObject("bumanC",reportService.getBumanC(sdate3, edate3))
                .addObject("bumanB",reportService.getBumanB(sdate3, edate3))
                .addObject("bumanP",reportService.getBumanP(sdate3, edate3))
                .addObject("bumanV",reportService.getBumanV(sdate3, edate3))
                .addObject("sunggocC",reportService.getSunggocC(sdate3, edate3))
                .addObject("sunggocB",reportService.getSunggocB(sdate3, edate3))
                .addObject("sunggocP",reportService.getSunggocP(sdate3, edate3))
                .addObject("sunggocV",reportService.getSunggocV(sdate3, edate3))
                .addObject("ojungC",reportService.getOjungC(sdate3, edate3))
                .addObject("ojungB",reportService.getOjungB(sdate3, edate3))
                .addObject("ojungP",reportService.getOjungP(sdate3, edate3))
                .addObject("ojungV",reportService.getOjungV(sdate3, edate3))
                .addObject("connectSumList",connectSumService.SelectListConnectSumLogService())
                .addObject("sdate222",LocalDate.now().minusDays(10))
                .addObject("edate222",LocalDate.now())
                .addObject("connectSumA",connectSumService.SelectConnectSumAService())
                .addObject("connectSumB",connectSumService.SelectConnectSumBService())
                .addObject("connectSumC",connectSumService.SelectConnectSumCService())
                .addObject("connectSumM",connectSumService.SelectConnectSumMService())
                .addObject("connectSumV",connectSumService.SelectConnectSumVService())
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;

    }

    @RequestMapping(path = "dashboard/infoAjax", method = RequestMethod.POST)
    public ModelAndView infoAjax(){
        return new ModelAndView("dashboard/dashboardInfoAjax");
    }

}
