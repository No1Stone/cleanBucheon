package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import java.util.List;

import lombok.Data;

@Data
public class ListCardVO {
	
	/* 스킬 코드 */
	private String skillCode;
	
	private String header;
	
	private String items;
	
	private String button;
	
	/* response sequence */
	private int resSeq;
	
	private ListCardHeader headerVO;
	
	private List<ListCardItems> itemsVO;
	
	private List<ListCardButtons> buttonVO;
	
	private String subSystem;
}
