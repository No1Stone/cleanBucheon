package com.bucheon.clean.town.db.repository.operation;

import com.bucheon.clean.town.db.entity.operation.FrontPath;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface FrontPathRepository extends JpaRepository<FrontPath, Long> {

    @Transactional(readOnly = true)
    Optional<FrontPath> findByPathSeq(Long seq);
    @Transactional(readOnly = true)
    List<FrontPath> findAll();
}
