package com.bucheon.clean.town.biz.login.login.v1.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final Logger logger = LoggerFactory.getLogger(LoginService.class);
    private final HttpServletRequest httpServletRequest;


}
