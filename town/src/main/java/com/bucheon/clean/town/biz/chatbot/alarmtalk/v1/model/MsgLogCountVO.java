package com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MsgLogCountVO {

    private int total;
    private int success;
    private float successRate;
    private int fail;
    private float failRate;
    private int kakao;
    private float kakaoRate;
    private int sms;
    private float smsRate;

}