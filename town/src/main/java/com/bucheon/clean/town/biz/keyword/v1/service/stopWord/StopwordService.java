package com.bucheon.clean.town.biz.keyword.v1.service.stopWord;

import com.bucheon.clean.town.biz.keyword.v1.model.Stopword;
import com.bucheon.clean.town.biz.keyword.v1.model.WordReq;
import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminReq;
import com.bucheon.clean.town.common.util.Paging;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface StopwordService {

	Paging getPage(WordReq dto);

	List<Stopword> getList(WordReq dto);

	Stopword getOne(String word);

	String save(Stopword stopword);

	int update(Stopword stopword);

	int delete(String[] words);

}