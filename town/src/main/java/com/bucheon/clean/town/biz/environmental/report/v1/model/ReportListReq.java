package com.bucheon.clean.town.biz.environmental.report.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReportListReq {

    private int page;
    private int size;

    //검색상태 - D:접수, P:처리중, C:처리
    private String searchStatus;
    //검색 쓰레기 유형
    private String searchTrashType;
    //검색 쓰레기 유형
    private String searchPointComplete;
    //검색유형 - searchContent: 내용, searchType: 유형, searchLocation: 위치, searchName: 접수자, searchReg: 접수일 기준 검색, searchComplete: 처리일기준 검색
    private String searchSelect;
    //검색유형
    private String searchString;
    //페이지 타입
    private String pageType;
    //기간검색 시작일
    private String sdate;
    //기간검색 종료일
    private String edate;
    //액셀 년월
    private String selectedExcelYYYYMM;
    //내용
    private String reportContent;
    //유형 트래쉬타입
    private Long reportType;
    //챗봇/밴드/어플
    private String reportReception;
    //주소
    private String reportAddrOld;
    //접수상태
    private String reportStatus;
    //행정구역
    private String sessionAdministrativeDivision;
    private String scomDt;
    private String ecomDt;
    private String sregDt;
    private String eregDt;
    private String processStatus;

}
