package com.bucheon.clean.town.biz.operation.board.v1.controller;

import com.bucheon.clean.town.biz.operation.board.v1.model.BoardAttVO;
import com.bucheon.clean.town.biz.operation.board.v1.model.BoardReq;
import com.bucheon.clean.town.biz.operation.board.v1.model.BoardRes;
import com.bucheon.clean.town.biz.operation.board.v1.model.BoardVO;
import com.bucheon.clean.town.biz.operation.board.v1.service.BoardService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.user.v1.service.UserService;
import com.bucheon.clean.town.biz.util.service.UtilService;
import com.bucheon.clean.town.db.entity.metabus.MetaBoardAtt;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.Board;
import com.bucheon.clean.town.db.entity.operation.BoardAtt;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/operation/board")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BoardController {

    private final Logger logger = LoggerFactory.getLogger(BoardController.class);
    private final BoardService boardService;
    private final UserService userService;
    private final UtilService utilService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        return new ModelAndView("redirect:/board/notice/info/A");
    }

    //기본틀 
    @RequestMapping(path = "/info/{type}", method = RequestMethod.GET)
    public ModelAndView info(BoardReq dto, @PathVariable String type) throws IllegalStateException, Exception {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        String pageTitle = "";
        MenuRes menu = menuService.MenuSelectOneService(42L);

        if(type.equals("notice")){ pageTitle = "공지사항"; }

        dto.setBoardType(type);

        return new ModelAndView("operation/board/boardManage")
                .addObject("boardPaging", boardService.BoardSelectListPagingInfoService(dto))
                .addObject("adminInfo",admin)
                .addObject("boardType",type)
                .addObject("pageTitle",pageTitle)
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(BoardReq dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        String sessionAdministrativeDivision = ((Admin) principal).getAdministrativeDivision();

        return new ModelAndView("operation/board/boardListAjax")
                .addObject("boardList", boardService.BoardSelectListService(dto))
                .addObject("boardPaging", boardService.BoardSelectListPagingInfoService(dto))
                ;
    }

    //최초 입력 페이지 팝업 - 필요없음
    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm(String boardType) {

        BoardRes board = new BoardRes();
        board.setBoardType(boardType);

        return new ModelAndView("operation/board/boardManagePopupAjax")
                .addObject("boardOne", board)
                ;
    }

    //상세보기 팝업
    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView viewForm(Long seq) {

        BoardRes board = boardService.BoardSelectOneService(seq);
        List<BoardAtt> boardAttList = boardService.BoardAttSelectListService(seq);

        return new ModelAndView("operation/board/boardManagePopupAjax")
                .addObject("boardOne", board)
                .addObject("boardAttList", boardAttList)
                ;
    }

    //저장처리
    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(@RequestParam("uploadfile") MultipartFile[] uploadfile, BoardVO dto) throws IllegalStateException, IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        Board board = boardService.BoardSaveService(dto);

        try {
            BoardAttVO boardAttVO = new BoardAttVO();

            List<Map<String, Object>> fileNameList = utilService.FileSaveService2(uploadfile, "board");

            int i = 0;
            for(Map<String, Object> fileName : fileNameList) {
                boardAttVO.setBoardAtt((String) fileName.get("link"));
                boardAttVO.setBoardAttOri(uploadfile[i].getOriginalFilename());
                boardAttVO.setBoardAttType(uploadfile[i].getContentType());
                boardAttVO.setBoardAttSize(uploadfile[i].getSize());
                boardAttVO.setBoardAttPath("/board/" + (String) fileName.get("fileName"));
                boardAttVO.setBoardSeq(board.getBoardSeq());
                boardService.BoardAttSaveService(boardAttVO);
                i++;
            }
        } catch (Exception e){
            //
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(@RequestParam("uploadfile") MultipartFile[] uploadfile, BoardVO dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        //내용 업데이트
        boardService.UpdateBoard(dto);

        BoardRes board = boardService.BoardSelectOneService(dto.getBoardSeq());

        if(uploadfile != null || !uploadfile.equals("")) {

            try {
                BoardAttVO boardAttVO = new BoardAttVO();

                List<Map<String, Object>> fileNameList = utilService.FileSaveService2(uploadfile, "board");

                int i = 0;
                for(Map<String, Object> fileName : fileNameList) {
                    boardAttVO.setBoardAtt((String) fileName.get("link"));
                    boardAttVO.setBoardAttOri(uploadfile[i].getOriginalFilename());
                    boardAttVO.setBoardAttType(uploadfile[i].getContentType());
                    boardAttVO.setBoardAttSize(uploadfile[i].getSize());
                    boardAttVO.setBoardAttPath("/board/" + (String) fileName.get("fileName"));
                    boardAttVO.setBoardSeq(board.getBoardSeq());
                    boardService.BoardAttSaveService(boardAttVO);
                    i++;
                }
            } catch (Exception e) {
                //
            }
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //삭제처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(Long[] boardSeq) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        boardService.UpdateBoardDelService(boardSeq);
        boardService.UpdateBoardAttDelService(boardSeq);


        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //첨부파일 삭제처리
    @RequestMapping(path = "/deleteAtt", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteAtt(Long boardAttSeq) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        logger.info(">>> boardAttSeq : {}", boardAttSeq);

        HashMap<String, Object> map = new HashMap<String, Object>();
        try {
            boardService.DeleteAtt(boardAttSeq);
            map.put("result", "success");
        } catch (Exception e){
            map.put("result", "fail");
        }

        return map;
    }

    //첨부파일 삭제처리
    @RequestMapping(path = "/downAtt", method = RequestMethod.POST)
    @ResponseBody
    public Object downAtt(String imgPath) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        logger.info(">>> boardAttSeq : {}", imgPath);

        HashMap<String, Object> map = new HashMap<String, Object>();
        try {
            utilService.imgGetService(imgPath);
            map.put("result", "success");
        } catch (Exception e){
            map.put("result", "fail");
        }

        return map;
    }

}
