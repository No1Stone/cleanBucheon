package com.bucheon.clean.town.db.repository.environmental.dsl;

import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignExcelDto;
import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignReq;
import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignRes;
import com.bucheon.clean.town.common.type.ReceptionType;
import com.bucheon.clean.town.db.entity.environmental.Campaign;
import com.bucheon.clean.town.db.entity.environmental.QCampaign;
import com.bucheon.clean.town.db.entity.operation.QTrashType;
import com.bucheon.clean.town.db.entity.operation.QUser;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class CampaignRepositoryDslImpl implements CampaignRepositoryDsl {


    private final JPAQueryFactory queryFactory;
    private QCampaign qCampaign = QCampaign.campaign;
    private QTrashType qTrashType = QTrashType.trashType;
    private QUser qUser = QUser.user;
    private final Logger logger = LoggerFactory.getLogger(CampaignRepositoryDslImpl.class);

    @Override
    public List<CampaignRes> SelectListCampaign(CampaignReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sc = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            ec = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }

        logger.info("re P---- {}",ReceptionType.P.getName());
        logger.info("re B---- {}",ReceptionType.B.getName());
        logger.info("re C---- {}",ReceptionType.C.getName());
        var aa =
                queryFactory.select(Projections.bean(CampaignRes.class,
                                        qCampaign.campaignSeq,
                                        qCampaign.campaignNum,
                                        qCampaign.campaignStatus,
                                        new CaseBuilder()
                                                .when(qCampaign.campaignStatus.eq("T")).then("쓰레기접수")
                                                .when(qCampaign.campaignStatus.eq("C")).then("청소완료")
                                                .otherwise("")
                                                .as("campaignStatusName"),
                                        qCampaign.campaignProcess,
                                        new CaseBuilder()
                                                .when(qCampaign.campaignProcess.eq("D")).then("접수")
                                                .when(qCampaign.campaignProcess.eq("P")).then("처리중")
                                                .when(qCampaign.campaignProcess.eq("C")).then("완료")
                                                .when(qCampaign.campaignProcess.eq("R")).then("처리불가")
                                                .otherwise("")
                                                .as("campaignProcessName"),
                                        qCampaign.trashType,
                                        ExpressionUtils.as(
                                                JPAExpressions.select(qTrashType.trashName)
                                                        .from(qTrashType)
                                                        .where(qTrashType.trashSeq.eq(qCampaign.trashType)),
                                                "trashTypeName"),
                                        qCampaign.addrOld,
                                        qCampaign.addrNew,
                                        qCampaign.areaName,
                                        qCampaign.locationX,
                                        qCampaign.locationY,
                                        qCampaign.regUserDid,
                                        ExpressionUtils.as(
                                                JPAExpressions.select(qUser.name)
                                                        .from(qUser)
                                                        .where(qUser.did.eq(qCampaign.regUserDid)),
                                                "userName"),
                                        ExpressionUtils.as(
                                                JPAExpressions.select(qUser.phone)
                                                        .from(qUser)
                                                        .where(qUser.did.eq(qCampaign.regUserDid)),
                                                "userPhone"),
                                        ExpressionUtils.as(
                                                JPAExpressions.select(qUser.email)
                                                        .from(qUser)
                                                        .where(qUser.did.eq(qCampaign.regUserDid)),
                                                "userEmail"),
                                        qCampaign.mileage,
                                        new CaseBuilder()
                                                .when(qCampaign.mileageStatus.eq("D")).then("접수")
                                                .when(qCampaign.mileageStatus.eq("C")).then("처리완료")
                                                .when(qCampaign.mileageStatus.eq("R")).then("지급불가")
                                                .otherwise("")
                                                .as("mileageStatusName"),
                                        qCampaign.mileageRejectReason,
                                        qCampaign.regDt,
                                        qCampaign.comDt
                        )
                        )
                .from(qCampaign)
                .leftJoin(qUser).on(qUser.did.eq(qCampaign.regUserDid))
                .where(
                        eqTrashType(dto.getSearchTrashType()),
                        eqCampaignStatus(dto.getSearchStatus()),
                        eqCampaignProcess(dto.getSearchProcess()),
                        eqReporPointComplete(dto.getSearchMileageComplete()),
                        eqSearchSelect(dto.getSearchSelect(), dto.getSearchString(), sc, ec, sr, er),
                        eqAdministrativeDivision(dto.getSessionAdministrativeDivision()),
//                        qCampaign.campaignReception.notLike(ReceptionType.A.getName()),
                        qCampaign.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qCampaign.campaignSeq.desc())
                .fetch();
        return aa;
    }

    @Override
    public QueryResults<CampaignRes> SelectListCampaignPaging(CampaignReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sc = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            ec = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        logger.info("re P---- {}",ReceptionType.P.getName());
        logger.info("re B---- {}",ReceptionType.B.getName());
        logger.info("re C---- {}",ReceptionType.C.getName());
        var aa =
                queryFactory.select(
                                Projections.bean(CampaignRes.class, qCampaign)).from(qCampaign)
                .leftJoin(qUser).on(qUser.did.eq(qCampaign.regUserDid))
                .where(
                        eqTrashType(dto.getSearchTrashType()),
                        eqCampaignStatus(dto.getSearchStatus()),
                        eqCampaignProcess(dto.getSearchProcess()),
                        eqReporPointComplete(dto.getSearchMileageComplete()),
                        eqSearchSelect(dto.getSearchSelect(), dto.getSearchString(), sc, ec, sr, er),
                        eqAdministrativeDivision(dto.getSessionAdministrativeDivision()),
//                        qCampaign.campaignReception.notLike(ReceptionType.A.getName()),
                        qCampaign.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qCampaign.campaignSeq.desc())
                .fetchResults();

        return aa;
    }

    @Override
    public List<CampaignRes> MySelectListCampaign(CampaignReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sc = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            ec = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }

        int seachSize = 0;
        if(dto.getMonth().equals("1")){
            seachSize = 30;
        }
        else if(dto.getMonth().equals("3")){
            seachSize = 92;
        }
        else if(dto.getMonth().equals("6")){
            seachSize = 183;
        }
        else if(dto.getMonth().equals("12")){
            seachSize = 365;
        }

        LocalDateTime searchSdate = LocalDateTime.now().minusDays(seachSize);
        LocalDateTime searchEdate = LocalDateTime.now();

        logger.info("re P---- {}",ReceptionType.P.getName());
        logger.info("re B---- {}",ReceptionType.B.getName());
        logger.info("re C---- {}",ReceptionType.C.getName());
        var aa =
                queryFactory.select(Projections.bean(CampaignRes.class,
                                        qCampaign.campaignSeq,
                                        qCampaign.campaignNum,
                                        qCampaign.campaignStatus,
                                        new CaseBuilder()
                                                .when(qCampaign.campaignStatus.eq("T")).then("쓰레기접수")
                                                .when(qCampaign.campaignStatus.eq("C")).then("청소완료")
                                                .otherwise("")
                                                .as("campaignStatusName"),
                                        qCampaign.campaignProcess,
                                        new CaseBuilder()
                                                .when(qCampaign.campaignProcess.eq("D")).then("접수")
                                                .when(qCampaign.campaignProcess.eq("P")).then("처리중")
                                                .when(qCampaign.campaignProcess.eq("C")).then("완료")
                                                .when(qCampaign.campaignProcess.eq("R")).then("처리불가")
                                                .otherwise("")
                                                .as("campaignProcessName"),
                                        qCampaign.trashType,
                                        ExpressionUtils.as(
                                                JPAExpressions.select(qTrashType.trashName)
                                                        .from(qTrashType)
                                                        .where(qTrashType.trashSeq.eq(qCampaign.trashType)),
                                                "trashTypeName"),
                                        qCampaign.addrOld,
                                        qCampaign.addrNew,
                                        qCampaign.areaName,
                                        qCampaign.locationX,
                                        qCampaign.locationY,
                                        qCampaign.regUserDid,
                                        ExpressionUtils.as(
                                                JPAExpressions.select(qUser.name)
                                                        .from(qUser)
                                                        .where(qUser.did.eq(qCampaign.regUserDid)),
                                                "userName"),
                                        ExpressionUtils.as(
                                                JPAExpressions.select(qUser.phone)
                                                        .from(qUser)
                                                        .where(qUser.did.eq(qCampaign.regUserDid)),
                                                "userPhone"),
                                        ExpressionUtils.as(
                                                JPAExpressions.select(qUser.email)
                                                        .from(qUser)
                                                        .where(qUser.did.eq(qCampaign.regUserDid)),
                                                "userEmail"),
                                        qCampaign.mileage,
                                        new CaseBuilder()
                                                .when(qCampaign.mileageStatus.eq("D")).then("접수")
                                                .when(qCampaign.mileageStatus.eq("C")).then("처리완료")
                                                .when(qCampaign.mileageStatus.eq("R")).then("지급불가")
                                                .otherwise("")
                                                .as("mileageStatusName"),
                                        qCampaign.mileageRejectReason,
                                        qCampaign.regDt,
                                        qCampaign.comDt
                                )
                        )
                        .from(qCampaign)
                        .leftJoin(qUser).on(qUser.did.eq(qCampaign.regUserDid))
                        .where(
                                eqTrashType(dto.getSearchTrashType()),
                                eqCampaignStatus(dto.getSearchStatus()),
                                eqCampaignProcess(dto.getSearchProcess()),
                                eqReporPointComplete(dto.getSearchMileageComplete()),
                                eqSearchSelect(dto.getSearchSelect(), dto.getSearchString(), sc, ec, sr, er),
                                qCampaign.regUserDid.eq(dto.getRegUserDid()),
//                        qCampaign.campaignReception.notLike(ReceptionType.A.getName()),
                                eqRegdt(searchSdate, searchEdate),
                                qCampaign.delYn.eq("N")
                        )
                        .limit(dto.getSize())
                        .offset(dto.getPage()*dto.getSize())
                        .orderBy(qCampaign.campaignSeq.desc())
                        .fetch();
        return aa;
    }

    @Override
    public QueryResults<CampaignRes> MySelectListCampaignPaging(CampaignReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sc = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            ec = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        int seachSize = 0;
        if(dto.equals("1")){
            seachSize = 30;
        }
        else if(dto.equals("3")){
            seachSize = 92;
        }
        else if(dto.equals("6")){
            seachSize = 183;
        }
        else if(dto.equals("12")){
            seachSize = 365;
        }

        LocalDateTime searchSdate = LocalDateTime.now().minusDays(seachSize);
        LocalDateTime searchEdate = LocalDateTime.now();

        logger.info("re P---- {}",ReceptionType.P.getName());
        logger.info("re B---- {}",ReceptionType.B.getName());
        logger.info("re C---- {}",ReceptionType.C.getName());
        var aa =
                queryFactory.select(
                                Projections.bean(CampaignRes.class, qCampaign)).from(qCampaign)
                        .leftJoin(qUser).on(qUser.did.eq(qCampaign.regUserDid))
                        .where(
                                eqTrashType(dto.getSearchTrashType()),
                                eqCampaignStatus(dto.getSearchStatus()),
                                eqCampaignProcess(dto.getSearchProcess()),
                                eqReporPointComplete(dto.getSearchMileageComplete()),
                                eqSearchSelect(dto.getSearchSelect(), dto.getSearchString(), sc, ec, sr, er),
                                qCampaign.regUserDid.eq(dto.getRegUserDid()),
//                        qCampaign.campaignReception.notLike(ReceptionType.A.getName()),
                                eqRegdt(searchSdate, searchEdate),
                                qCampaign.delYn.eq("N")
                        )
                        .limit(dto.getSize())
                        .offset(dto.getPage()*dto.getSize())
                        .orderBy(qCampaign.campaignSeq.desc())
                        .fetchResults();

        return aa;
    }

    @Override
    public List<CampaignRes> MySelectListCampaignAll(CampaignReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sc = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            ec = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }

        logger.info("re P---- {}",ReceptionType.P.getName());
        logger.info("re B---- {}",ReceptionType.B.getName());
        logger.info("re C---- {}",ReceptionType.C.getName());
        var aa =
                queryFactory.select(Projections.bean(CampaignRes.class,
                                        qCampaign.campaignSeq,
                                        qCampaign.campaignNum,
                                        qCampaign.campaignStatus,
                                        new CaseBuilder()
                                                .when(qCampaign.campaignStatus.eq("T")).then("쓰레기접수")
                                                .when(qCampaign.campaignStatus.eq("C")).then("청소완료")
                                                .otherwise("")
                                                .as("campaignStatusName"),
                                        qCampaign.campaignProcess,
                                        new CaseBuilder()
                                                .when(qCampaign.campaignProcess.eq("D")).then("접수")
                                                .when(qCampaign.campaignProcess.eq("P")).then("처리중")
                                                .when(qCampaign.campaignProcess.eq("C")).then("완료")
                                                .when(qCampaign.campaignProcess.eq("R")).then("처리불가")
                                                .otherwise("")
                                                .as("campaignProcessName"),
                                        qCampaign.trashType,
                                        ExpressionUtils.as(
                                                JPAExpressions.select(qTrashType.trashName)
                                                        .from(qTrashType)
                                                        .where(qTrashType.trashSeq.eq(qCampaign.trashType)),
                                                "trashTypeName"),
                                        qCampaign.addrOld,
                                        qCampaign.addrNew,
                                        qCampaign.areaName,
                                        qCampaign.locationX,
                                        qCampaign.locationY,
                                        qCampaign.regUserDid,
                                        ExpressionUtils.as(
                                                JPAExpressions.select(qUser.name)
                                                        .from(qUser)
                                                        .where(qUser.did.eq(qCampaign.regUserDid)),
                                                "userName"),
                                        ExpressionUtils.as(
                                                JPAExpressions.select(qUser.phone)
                                                        .from(qUser)
                                                        .where(qUser.did.eq(qCampaign.regUserDid)),
                                                "userPhone"),
                                        ExpressionUtils.as(
                                                JPAExpressions.select(qUser.email)
                                                        .from(qUser)
                                                        .where(qUser.did.eq(qCampaign.regUserDid)),
                                                "userEmail"),
                                        qCampaign.mileage,
                                        new CaseBuilder()
                                                .when(qCampaign.mileageStatus.eq("D")).then("접수")
                                                .when(qCampaign.mileageStatus.eq("C")).then("처리완료")
                                                .when(qCampaign.mileageStatus.eq("R")).then("지급불가")
                                                .otherwise("")
                                                .as("mileageStatusName"),
                                        qCampaign.mileageRejectReason,
                                        qCampaign.regDt,
                                        qCampaign.comDt
                                )
                        )
                        .from(qCampaign)
                        .leftJoin(qUser).on(qUser.did.eq(qCampaign.regUserDid))
                        .where(
                                eqTrashType(dto.getSearchTrashType()),
                                eqCampaignStatus(dto.getSearchStatus()),
                                eqCampaignProcess(dto.getSearchProcess()),
                                eqReporPointComplete(dto.getSearchMileageComplete()),
                                eqSearchSelect(dto.getSearchSelect(), dto.getSearchString(), sc, ec, sr, er),
                                qCampaign.regUserDid.eq(dto.getRegUserDid()),
//                        qCampaign.campaignReception.notLike(ReceptionType.A.getName()),
                                qCampaign.delYn.eq("N")
                        )
                        .orderBy(qCampaign.campaignSeq.desc())
                        .fetch();
        return aa;
    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchSelect(String searchSelect, String searchString,
                                                   LocalDateTime sc, LocalDateTime ec, LocalDateTime sr, LocalDateTime er) {

        try {

            if (searchSelect.equals("searchName")) {
                return eqRegName(searchString);
            } else if (searchSelect.equals("searchAreaName")) {
                return eqAreaName(searchString);
            } else if (searchSelect.equals("searchNum")) {
                return eqNum(searchString);
            } else if (searchSelect.equals("searchLocation")) {
                return eqCampaignAddrold(searchString);
            } else if (searchSelect.equals("searchReg")) {
                return eqRegdt(sr, er);
            } else if (searchSelect.equals("searchComplete")) {
                return eqComdt(sc, ec);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqRegdt(LocalDateTime sdate, LocalDateTime edate) {
        if (sdate == null && edate == null) {
            return null;
        }
        return qCampaign.regDt.between(sdate, edate);
    }

    private BooleanExpression eqComdt(LocalDateTime sdate, LocalDateTime edate) {
        if (sdate == null && edate == null) {
            return null;
        }
        return qCampaign.comDt.between(sdate, edate);
    }

    private BooleanExpression eqCampaignStatus(String campaignStatus) {
        if (StringUtils.isEmptyOrWhitespace(campaignStatus)) {
            return null;
        }
        return qCampaign.campaignStatus.eq(campaignStatus);
    }

    private BooleanExpression eqCampaignProcess(String process) {
        if (StringUtils.isEmptyOrWhitespace(process)) {
            return null;
        }
        return qCampaign.campaignProcess.eq(process);
    }

    private BooleanExpression eqReporPointComplete(String s) {
        if (StringUtils.isEmptyOrWhitespace(s)) {
            return null;
        }
        return qCampaign.mileageStatus.eq(s);
    }

    private BooleanExpression eqTrashType(Long seq) {
        if (seq == null) {
            return null;
        }
        return qCampaign.trashType.eq(seq);
    }

    private BooleanExpression eqCampaignAddrold(String campaignAddrOld) {
        if (StringUtils.isEmptyOrWhitespace(campaignAddrOld)) {
            return null;
        }
        return qCampaign.addrOld.contains(campaignAddrOld).or(qCampaign.addrNew.contains(campaignAddrOld));
    }

    private BooleanExpression eqProcessStatus(String st) {
        if (StringUtils.isEmptyOrWhitespace(st)) {
        return null;
        }
        return qCampaign.campaignStatus.eq(st);
    }

    private BooleanExpression eqAreaName(String st) {
        if (StringUtils.isEmptyOrWhitespace(st)) {
            return null;
        }
        return qCampaign.areaName.eq(st);
    }

    private BooleanExpression eqNum(String st) {
        if (StringUtils.isEmptyOrWhitespace(st)) {
            return null;
        }
        return qCampaign.campaignNum.contains(st);
    }

    private BooleanExpression eqRegName(String st) {
        if (StringUtils.isEmptyOrWhitespace(st)) {
            return null;
        }
        return qUser.name.contains(st);
    }

    private BooleanExpression eqAdministrativeDivision(String st) {

        logger.info("getSessionAdministrativeDivision - {}", st);
        if (StringUtils.isEmptyOrWhitespace(st) || st.equals("전체")) {
            return null;
        }
        return qCampaign.areaName.eq(st);
    }



//    @Override
//    public List<Campaign> SelectListDetailCampaign() {
//        return null;
//    }


//
//    private BooleanExpression valLike(SongCateEnum cate, String val) {
//
//        //검색 때문에 추가
//        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");
//
//        if (cate !=null && val != null && val.length() > 0) {
//            switch (cate) {
//                case All:
//                    return songDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase())
//                            .or(songDetailCodeEntity.description.toLowerCase().contains(val.toLowerCase()));
//                case Value:
//                    return songDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase());
//                case Description:
//                    return songDetailCodeEntity.description.toLowerCase().contains(val.toLowerCase());
//                case RegUsrName:
//                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
//                case ModUsrName:
//                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
//            }
//        }
//        return null;
//    }
}
