package com.bucheon.clean.town.biz.environmental.campaign.v1.model;

public interface CampaignAllRankingRes {

    int getRankNum();
    String getRegUserDid();
    String getName();
    int getSumMileage();
}
