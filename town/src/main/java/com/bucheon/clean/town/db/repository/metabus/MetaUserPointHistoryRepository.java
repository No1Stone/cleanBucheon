package com.bucheon.clean.town.db.repository.metabus;

import com.bucheon.clean.town.db.entity.metabus.MetaUserPointHistory;
import com.bucheon.clean.town.db.repository.metabus.dsl.MetaUserPointHistoryRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MetaUserPointHistoryRepository extends JpaRepository<MetaUserPointHistory, String>, MetaUserPointHistoryRepositoryDsl {

    Optional<MetaUserPointHistory> findTop1ByUserIdOrderByUserSeqDesc(String userId);
    int countByUserId(String userId);

}
