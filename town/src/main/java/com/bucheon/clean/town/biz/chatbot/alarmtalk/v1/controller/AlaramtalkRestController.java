package com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.controller;

import com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model.*;
import com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.service.AlaramtalkService;
import com.bucheon.clean.town.db.entity.chatbot.TB_AlaramBoard;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/chatbot/alarmtalk/rest")
@RequiredArgsConstructor
public class AlaramtalkRestController {

    private final Logger logger = LoggerFactory.getLogger(AlaramtalkRestController.class);
    private final AlaramtalkService alaramtalkService;

    @Autowired
    private MsgLogCountDAO msgLogCountDAO;

    @PostMapping(path = "/save")
    public Map<String, Object> TalkGetController(@RequestBody ChatbotData dto) throws Exception {
        Map<String, Object> result = new HashMap<>();
        logger.info("dto = {}", new Gson().toJson(dto));
        String res = alaramtalkService.SaveService(dto);
        result.put("result", res);

        return result;
    }

    @GetMapping(path = "/info/{seq}")
    public AlaramBoardRes select(@PathVariable(name = "seq") Long seq) {
        return alaramtalkService.TB_AlaramBoardSelectService(seq);
    }

    @GetMapping(path = "/info-phone-d/{phone}/{sdate}/{edate}/{page}")
    public List<AlaramBoardRes> selectPhone(@PathVariable(name = "phone") String phone,
                                                  @PathVariable("sdate") String sdate,
                                                  @PathVariable("edate") String edate,
                                                  @PathVariable("page") int page
    ) {
        PageRequest pageRequest = PageRequest.of(page, 10);
        return alaramtalkService.TB_AlaramBoardSelectPhoneService(phone, sdate, edate, pageRequest);
    }

    @GetMapping(path = "/info-id-d/{chatbotUserId}/{sdate}/{edate}/{page}")
    public List<AlaramBoardRes> selectChatbotUserId(@PathVariable(name = "chatbotUserId") String chatbotUserId,
                                            @PathVariable("sdate") String sdate,
                                            @PathVariable("edate") String edate,
                                            @PathVariable("page") int page
    ) {
        PageRequest pageRequest = PageRequest.of(page, 10);
        return alaramtalkService.TB_AlaramBoardSelectChatbotUserIdService(chatbotUserId, sdate, edate, pageRequest);
    }




    @GetMapping(path = "/test11")
    public TB_AlaramBoard test1111() {

        return alaramtalkService.jsonvaluechk();
    }

    @GetMapping(path = "/test22")
    public String test222(AlramSendVO dto) {
        String aa = "";
        alaramtalkService.AlramSendService(dto);

        return aa;
    }

    @GetMapping(path = "/test33")
    public MsgLogCountVO test333() {

        AlaramReq dto = new AlaramReq();
        dto.setYyyymm("202207");
        dto.setSdate("20220701");
        dto.setEdate("20220731");
        dto.setPage(0);
        dto.setSize(10);

        MsgLogCountVO msgLogCountVO = alaramtalkService.selectMsgLogCountService(dto);

        return msgLogCountVO;
    }

    @GetMapping(path = "/test44")
    public void test444() {

        AlaramReq dto = new AlaramReq();
        dto.setYyyymm("202207");
        dto.setSdate("20220701");
        dto.setEdate("20220731");
        dto.setPage(0);
        dto.setSize(10);

        alaramtalkService.selectMsgLogService(dto);

        //return msgLogCountVO;
    }




}
