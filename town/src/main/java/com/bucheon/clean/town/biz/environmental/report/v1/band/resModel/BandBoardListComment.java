package com.bucheon.clean.town.biz.environmental.report.v1.band.resModel;

import com.bucheon.clean.town.biz.environmental.report.v1.band.model.BandAuthor;
import com.bucheon.clean.town.biz.environmental.report.v1.band.model.BandPotos;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class BandBoardListComment {
    /**
     * 본물을 조회했을때 나오는 댓글 VO
     */


    private String post_key;
    private String comment_key;
    private String body;
    private long created_at;
    private String name;
    private String description;
    private String profile_image_url;
    private String user_key;
    private String url;
    private int width;
    private int height;
    private Author author;
    private Photo photo;

    public BandBoardListComment() {
    }

    public BandBoardListComment(String post_key, String comment_key, String body, long created_at, String name, String description, String profile_image_url, String user_key, String url, int width, int height, Author author, Photo photo) {
        this.post_key = post_key;
        this.comment_key = comment_key;
        this.body = body;
        this.created_at = created_at;
        this.name = name;
        this.description = description;
        this.profile_image_url = profile_image_url;
        this.user_key = user_key;
        this.url = url;
        this.width = width;
        this.height = height;
        this.author = author;
        this.photo = photo;
    }

    public BandBoardListComment(BandAuthor author) {
        this.name = author.getName();
        this.description = author.getDescription();
        this.profile_image_url = author.getProfile_image_url();
        this.user_key = author.getUser_key();
    }

    public BandBoardListComment(BandPotos photo) {
        this.url = photo.getUrl();
        this.width = photo.getWidth();
        this.height = photo.getHeight();
    }
}
