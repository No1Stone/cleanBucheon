package com.bucheon.clean.town.db.repository.metabus;

import com.bucheon.clean.town.db.entity.metabus.MetaBoard;
import com.bucheon.clean.town.db.entity.metabus.MetaNoticePopup;
import com.bucheon.clean.town.db.repository.metabus.dsl.MetaBoardRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface MetaNoticePopupRepository extends JpaRepository<MetaNoticePopup, String> {

    @Transactional(readOnly = true)
    Optional<MetaNoticePopup> findByPopupTypeAndUseYn(String type, String useYn);
    Optional<MetaNoticePopup> findByPopupType(String type);
    int countByPopupTypeAndUseYn(String type, String useYn);



    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_notice_popup set popup_image_url = :popupImageUrl where popup_type =:popupType "
            , nativeQuery = true
    )
    int UpdateMetaPopupImage(@Param("popupImageUrl") String popupImageUrl, @Param("popupType") String popupType);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_notice_popup set use_yn = :useYn where popup_type =:popupType "
            , nativeQuery = true
    )
    int UpdateMetaPopupUse(@Param("useYn") String useYn, @Param("popupType") String popupType);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_notice_popup set mod_seq = :modSeq, mod_dt = now() where popup_type =:popupType "
            , nativeQuery = true
    )
    int UpdateMetaMod(@Param("modSeq") Long modSeq, @Param("popupType") String popupType);

}
