package com.bucheon.clean.town.biz.operation.menu.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MenuPermissionRes {

    private Long menuSeq;
    private Long pmenuSeq;
    private String menuName;
    private String menuDesc;
    private String menuUrl;
    private String menuIcon;
    private int menuLevel;
    private String useYn;

}
