package com.bucheon.clean.town.db.entity.etc;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_dong_name2")
@Getter
@Setter
@NoArgsConstructor
public class DongName2 {

    @Id
    @Column(name = "dong_seq", nullable = true)
    private Long dongSeq;
    @Column(name = "hname", nullable = true)
    private String hname;
    @Column(name = "bname", nullable = true)
    private String bname;
    @Column(name = "h_div_code", nullable = true)
    private String hDivCode;
    @Column(name = "hg_code", nullable = true)
    private String hgCode;
    @Column(name = "bg_code", nullable = true)
    private String bgCode;


    @Builder
    DongName2(
            Long dongSeq,
            String hname,
            String bname,
            String hDivCode,
            String hgCode,
            String bgCode
            ) {
        this.dongSeq = dongSeq;
        this.hname = hname;
        this.bname = bname;
        this.hDivCode = hDivCode;
        this.hgCode = hgCode;
        this.bgCode = bgCode;
    }

}
