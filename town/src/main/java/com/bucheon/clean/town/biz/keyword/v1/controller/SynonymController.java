package com.bucheon.clean.town.biz.keyword.v1.controller;

import com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.controller.AlaramtalkController;
import com.bucheon.clean.town.biz.keyword.v1.model.Synonym;
import com.bucheon.clean.town.biz.keyword.v1.model.WordReq;
import com.bucheon.clean.town.biz.keyword.v1.service.synonym.SynonymService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;

@Controller
@RequestMapping(path = "/synonym")
@RequiredArgsConstructor
public class SynonymController {

    private final Logger logger = LoggerFactory.getLogger(AlaramtalkController.class);
    private final MenuService menuService;
    private final SynonymService synonymService;
    private final GlobalAttributeAdviser globalAttributeAdviser;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/keyword/synonym/synonymManage");
    }

    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(WordReq dto, HttpServletRequest request) throws IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(47L);

        dto.setPage(0);
        dto.setSize(10);
        dto.setSearchCode("전체");
        dto.setSearchWord1("");
        dto.setSearchWord2("");
        dto.setSearchWord3("");
        dto.setSearchWord4("");

        return new ModelAndView("keyword/synonym/synonymManage")
                .addObject("paging",synonymService.getPage(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                .addObject("busiBranCodeMap", globalAttributeAdviser.getCodeMap())
                ;
    }

    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(WordReq dto){
        return new ModelAndView("keyword/synonym/synonymListAjax")
                .addObject("list",synonymService.getList(dto))
                .addObject("paging",synonymService.getPage(dto));
    }

    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm() throws IOException {
        return new ModelAndView("keyword/synonym/synonymManagePopupAjax")
                .addObject("word", new Synonym())
                .addObject("busiBranCodeMap", globalAttributeAdviser.getCodeMap())
                ;
    }

    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView modifyForm(Synonym dto) throws IOException {
        return new ModelAndView("keyword/synonym/synonymManagePopupAjax")
                .addObject("word", synonymService.getOne(dto))
                .addObject("busiBranCodeMap", globalAttributeAdviser.getCodeMap())
                ;
    }

    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(Synonym synonym){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", synonymService.save(synonym));
        return map;
    }

    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(Synonym synonym){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", synonymService.update(synonym));
        return map;
    }

    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(String[] representativeWord1){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result",  synonymService.delete(representativeWord1));
        return map;
    }
}
