package com.bucheon.clean.town.db.repository.environmental;

import com.bucheon.clean.town.db.entity.environmental.Complaints;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComplaintsRepository extends JpaRepository<Complaints, Long> {
}
