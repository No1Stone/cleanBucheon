package com.bucheon.clean.town.db.repository.operation.dsl;

import com.bucheon.clean.town.biz.operation.user.v1.model.UserReq;
import com.bucheon.clean.town.db.entity.operation.QUser;
import com.bucheon.clean.town.db.entity.operation.User;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class UserRepositoryDslImpl implements UserRepositoryDsl{

    private final JPAQueryFactory queryFactory;
    private QUser qUser = QUser.user;


    @Override
    public List<User> UserListSelect(UserReq dto) {
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSregDt()) && !StringUtils.isEmptyOrWhitespace(dto.getEregDt())) {
            sr = LocalDate.parse(dto.getSregDt(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEregDt(), format).atStartOfDay().plusDays(1L);
        }
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>> 여기");

        var result = queryFactory.selectFrom(qUser)
                .where(eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        qUser.name.ne(""))
                .orderBy(qUser.createDate.desc())
                .limit(dto.getSize())
                .offset(dto.getPage()* dto.getSize())
                .fetch();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>> 여기");
        return result;
    }

    @Override
    public QueryResults<User> UserListSelectPaging(UserReq dto) {
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSregDt()) && !StringUtils.isEmptyOrWhitespace(dto.getEregDt())) {
            sr = LocalDate.parse(dto.getSregDt(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEregDt(), format).atStartOfDay().plusDays(1L);
        }
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        var result = queryFactory.selectFrom(qUser)
                .where(eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        qUser.name.ne("")
                )
                .orderBy(qUser.createDate.desc())
                .limit(dto.getSize())
                .offset(dto.getPage()* dto.getSize())
                .fetchResults();
        return result;
    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchsearchSelect(String searchSelect, String searchString) {

        try {

            if (searchSelect.equals("searchName")) {
                return eqName(searchString);
            } else if (searchSelect.equals("searchPhone")) {
                return eqPhone(searchString);
            } else if (searchSelect.equals("searchEmail")) {
                return eqEmail(searchString);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqName(String name){
        if (StringUtils.isEmptyOrWhitespace(name)) {
            return null;
        }
        return qUser.name.contains(name);
    }
    private BooleanExpression eqPhone(String phone){
        if (StringUtils.isEmptyOrWhitespace(phone)) {
            return null;
        }
        return qUser.phone.contains(phone);
    }
    private BooleanExpression eqEmail(String email){
        if (StringUtils.isEmptyOrWhitespace(email)) {
            return null;
        }
        return qUser.email.contains(email);
    }
    private BooleanExpression eqRegDt(LocalDateTime sr, LocalDateTime er){
        if (sr == null && er == null) {
            return null;
        }
        return qUser.createDate.between(sr, er);
    }


}
