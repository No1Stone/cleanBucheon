package com.bucheon.clean.town.biz.util.controller;

import com.bucheon.clean.town.biz.operation.point.v1.model.TrashTypeRes;
import com.bucheon.clean.town.biz.util.service.UtilService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/util")
@RequiredArgsConstructor
public class UtileRestController {
    private final UtilService utilService;
    private final Logger logger = LoggerFactory.getLogger(UtileRestController.class);
    @Value("${domain.file}")
    private String filePath;
    @Value("${domain.basepath}")
    private String basepath;

    @GetMapping(path = "/get-img/metaBoard/{imgPath}")
    public ResponseEntity imgGet(@PathVariable(name = "imgPath") String imgPath) {
        ResponseEntity rettt = null;

        logger.info("---path : {}", filePath + "/metaBoard/" + imgPath );

        try {
            rettt = utilService.imgGetService("/metaBoard/" + imgPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rettt;
    }

    @GetMapping(path = "/trash")
    public List<TrashTypeRes> trashSelect() {
        return utilService.trashSelectService();
    }

//    @GetMapping(path = "/base-img/{img}")
//    public ResponseEntity<Resource> BaseImgGetServiceController(@PathVariable(name = "img") String img) throws IOException {
//
//        logger.info("img - {}",img);
//        return utilService.BaseImgGetService(img);
//    }
//    @GetMapping(path = "/img/{dir}/{img}")
//    public ResponseEntity<Resource> ImgGetServiceController(
//            @PathVariable(name = "dir") String dir,
//            @PathVariable(name = "img") String img) throws IOException {
//        logger.info("dir - {}",dir);
//        logger.info("img - {}",img);
//        return utilService.ImgSaveAndDownloadService(dir,img);
//    }

    @GetMapping("/download")
    public void downloadFile(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {
            // 다운로드 받을 파일명을 가져온다.
            String fileName = request.getParameter("filename");
            String ori = request.getParameter("ori");

            // 다운로드 경로 (내려받을 파일경로를 설정한다.)
            String filePath2 = basepath + fileName;
            logger.info(">>>>>>>>>>>" + filePath2);

            // 경로와 파일명으로 파일 객체를 생성한다.
            File dFile  = new File(basepath, fileName);
            logger.info(">>>>>>>>>>>" + dFile);

            // 파일 길이를 가져온다.
            int fSize = (int) dFile.length();

            // 파일이 존재
            if (fSize > 0) {

                // 파일명을 URLEncoder 하여 attachment, Content-Disposition Header로 설정
                String encodedFilename = "attachment; filename*=" + "UTF-8" + "''" + URLEncoder.encode(ori, "UTF-8");

                // ContentType 설정
                response.setContentType("application/octet-stream; charset=utf-8");

                // Header 설정
                response.setHeader("Content-Disposition", encodedFilename);

                // ContentLength 설정
                response.setContentLengthLong(fSize);

                BufferedInputStream in = null;
                BufferedOutputStream out = null;

				/* BufferedInputStream
				 *
					java.io의 가장 기본 파일 입출력 클래스
					입력 스트림(통로)을 생성해줌
					사용법은 간단하지만, 버퍼를 사용하지 않기 때문에 느림
					속도 문제를 해결하기 위해 버퍼를 사용하는 다른 클래스와 같이 쓰는 경우가 많음
				*/
                in = new BufferedInputStream(new FileInputStream(dFile));

				/* BufferedOutputStream
				 *
					java.io의 가장 기본이 되는 파일 입출력 클래스
					출력 스트림(통로)을 생성해줌
					사용법은 간단하지만, 버퍼를 사용하지 않기 때문에 느림
					속도 문제를 해결하기 위해 버퍼를 사용하는 다른 클래스와 같이 쓰는 경우가 많음
				*/
                out = new BufferedOutputStream(response.getOutputStream());

                try {
                    byte[] buffer = new byte[4096];
                    int bytesRead=0;

				 	/*
						모두 현재 파일 포인터 위치를 기준으로 함 (파일 포인터 앞의 내용은 없는 것처럼 작동)
						int read() : 1byte씩 내용을 읽어 정수로 반환
						int read(byte[] b) : 파일 내용을 한번에 모두 읽어서 배열에 저장
						int read(byte[] b. int off, int len) : 'len'길이만큼만 읽어서 배열의 'off'번째 위치부터 저장
				 	*/
                    while ((bytesRead = in.read(buffer))!=-1) {
                        out.write(buffer, 0, bytesRead);
                    }

                    // 버퍼에 남은 내용이 있다면, 모두 파일에 출력
                    out.flush();
                }
                finally {
					/*
						현재 열려 in,out 스트림을 닫음
						메모리 누수를 방지하고 다른 곳에서 리소스 사용이 가능하게 만듬
					*/
                    in.close();
                    out.close();
                }
            } else {
                throw new FileNotFoundException("파일이 없습니다.");
            }
        }
        catch (Exception e) {
            logger.info(e.getMessage());
        }
    }

}
