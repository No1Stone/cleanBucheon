package com.bucheon.clean.town.db.repository.environmental.band;

import com.bucheon.clean.town.db.entity.environmental.band.TB_BandBoardListComment;
import com.bucheon.clean.town.db.entity.environmental.band.id.TB_BandBoardListCommentId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TB_BandBoardListCommentRepository extends JpaRepository<TB_BandBoardListComment, TB_BandBoardListCommentId> {

    int countByCommentKeyAndPostKey(String commentKey, String postKey);

    List<TB_BandBoardListComment> findByPostKey(String postKey);

}
