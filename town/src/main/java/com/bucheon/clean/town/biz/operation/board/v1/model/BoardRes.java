package com.bucheon.clean.town.biz.operation.board.v1.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BoardRes {

    private Long boardSeq;
    private String boardType;
    private String boardTitle;
    private String boardContent;
    private String useYn;
    private Long regSeq;
    private String boardRegName;
    private LocalDateTime regDt;
    private Long modSeq;
    private String boardModName;
    private LocalDateTime modDt;
    private List<String> boardAttList;

}
