package com.bucheon.clean.town.biz.keyword.v1.service.interestWord;

import com.bucheon.clean.town.biz.keyword.v1.model.InterestWord;
import com.bucheon.clean.town.biz.keyword.v1.model.WordReq;
import com.bucheon.clean.town.common.util.Paging;

import java.util.List;

public interface InterestwordService {

	Paging getPage(WordReq dto);

	List<InterestWord> getList(WordReq dto);

	InterestWord getOne(InterestWord dto);

	int save(InterestWord stopword);

	int update(InterestWord stopword);

	int delete(String[] word1);

}