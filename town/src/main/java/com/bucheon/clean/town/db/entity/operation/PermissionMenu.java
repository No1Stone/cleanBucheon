package com.bucheon.clean.town.db.entity.operation;

import com.bucheon.clean.town.db.entity.operation.id.PermissionMenuId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_permission_menu")
@Getter
@Setter
@NoArgsConstructor
@IdClass(PermissionMenuId.class)
public class PermissionMenu {
    @Id
    @Column(name = "permission_seq", nullable = true)
    private Long permissionSeq;
    @Id
    @Column(name = "menu_seq", nullable = true)
    private Long menuSeq;
    @Column(name = "use_yn", nullable = true)
    private String useYn;

    @Builder
    PermissionMenu(
            Long permissionSeq,
            Long menuSeq,
            String useYn
    ) {
        this.permissionSeq = permissionSeq;
        this.menuSeq = menuSeq;
        this.useYn = useYn;
    }

}
