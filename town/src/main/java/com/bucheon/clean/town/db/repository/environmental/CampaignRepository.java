package com.bucheon.clean.town.db.repository.environmental;

import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignAllRankingRes;
import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignVO;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.MetaAllRankingRes;
import com.bucheon.clean.town.db.entity.environmental.Campaign;
import com.bucheon.clean.town.db.entity.environmental.Complaints;
import com.bucheon.clean.town.db.entity.environmental.Report;
import com.bucheon.clean.town.db.repository.environmental.dsl.CampaignRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface CampaignRepository extends JpaRepository<Campaign, Long>, CampaignRepositoryDsl {


    @Transactional(readOnly = true)
    Campaign findByCampaignSeq(Long seq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set campaign_process = :campaignProcess where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateCampaignProcess(@Param("campaignProcess") String campaignProcess, @Param("campaignSeq") Long campaignSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set campaign_process_reject_reason = :campaignProcessRejectReason where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateCampaignProcessRejectReason(@Param("campaignProcessRejectReason") String campaignProcessRejectReason, @Param("campaignSeq") Long campaignSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set mileage = :mileage where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateMileage(@Param("mileage") int mileage, @Param("campaignSeq") Long campaignSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set mileage_status = :mileageStatus where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateMileageStatus(@Param("mileageStatus") String mileageStatus, @Param("campaignSeq") Long campaignSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set mileage_reject_reason = :mileageRejectReason where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateMileageRejectReason(@Param("mileageRejectReason") String mileageRejectReason, @Param("campaignSeq") Long campaignSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set com_dt = :comDt where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateComDt(@Param("comDt") LocalDateTime comDt, @Param("campaignSeq") Long campaignSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_campaign set del_yn = :delYn where campaign_seq =:campaignSeq "
            , nativeQuery = true
    )
    int UpdateCampaignDel(@Param("delYn") String delYn, @Param("campaignSeq") Long campaignSeq);

    Optional<Campaign> findByCampaignSeqAndDelYnOrderByRegDtDesc(Long seq, String del);


    //랭킹관련
    @Query(
            value = "SELECT " +
                    "@Rownum \\:= @Rownum + 1 AS RankNum, " +
                    "aa.* FROM ( " +
                    "SELECT " +
                    "a.reg_user_did as regUserDid, b.name, IFNULL(SUM(a.mileage),0) AS sumMileage " +
                    "FROM " +
                    "tb_campaign a " +
                    "LEFT OUTER JOIN tb_user b ON a.reg_user_did = b.did " +
                    "WHERE DATE_FORMAT(a.reg_dt, '%Y-%m') =:yyyymm "  + " AND a.del_yn = 'N'" +
                    "GROUP BY a.reg_user_did " +
                    ") aa, (SELECT @Rownum \\:= 0) rownumT " +
                    "ORDER BY aa.sumMileage DESC " +
                    "LIMIT 0, 100;"
            , nativeQuery = true
    )
    List<CampaignAllRankingRes> selectAllRankingList(@Param("yyyymm") String yyyymm);

    //나의 랭킹관련
    @Query(
            value = "SELECT * FROM " +
                    "( " +
                    "   SELECT " +
                    "       @Rownum \\:= @Rownum + 1 AS RankNum, " +
                    "       aa.* FROM ( " +
                    "           SELECT " +
                    "               a.reg_user_did as regUserDid, b.name, IFNULL(SUM(a.mileage),0) AS sumMileage " +
                    "           FROM " +
                    "               tb_campaign a " +
                    "                   LEFT OUTER JOIN tb_user b ON a.reg_user_did = b.did " +
                    "           WHERE DATE_FORMAT(a.reg_dt, '%Y-%m') =:yyyymm " + " AND a.del_yn = 'N'" +
                    "           GROUP BY a.reg_user_did " +
                    "       ) aa, (SELECT @Rownum \\:= 0) rownumT " +
                    "     ORDER BY aa.sumMileage DESC " +
                    ") a3 " +
                    "where a3.regUserDid =:userId " +
                    "limit 0,1 " +
                    ";"
            , nativeQuery = true
    )
    CampaignAllRankingRes selectMyRankingList(@Param("userId") String userId, @Param("yyyymm") String yyyymm);
}
