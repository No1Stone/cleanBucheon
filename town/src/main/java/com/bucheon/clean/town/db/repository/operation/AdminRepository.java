package com.bucheon.clean.town.db.repository.operation;

import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.TrashType;
import com.bucheon.clean.town.db.entity.operation.id.AdminId;
import com.bucheon.clean.town.db.repository.operation.dsl.AdminRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface AdminRepository extends JpaRepository<Admin, AdminId> , AdminRepositoryDsl {

    List<Admin> findAll();
    List<Admin> findByUseYnAndDelYnOrderByAdminNameAsc(String useYn, String delyn);
    Optional<Admin> findByAdminSeq(Long seq);
    Admin findByAdminId(String adminId);
    boolean existsByAdminId(String id);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_admin set admin_name = :adminName where admin_seq =:adminSeq "
            , nativeQuery = true
    )
    int updateAdminName(@Param("adminName") String adminName, @Param("adminSeq") Long adminSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_admin set admin_password = :adminPassword where admin_seq =:adminSeq "
            , nativeQuery = true
    )
    int updateAdminPassword(@Param("adminPassword") String adminPassword, @Param("adminSeq") Long adminSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_admin set admin_email = :adminEamil where admin_seq =:adminSeq "
            , nativeQuery = true
    )
    int updateAdminEmail(@Param("adminEamil") String superYn, @Param("adminSeq") Long adminSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_admin set administrative_division = :administrativeDivision where admin_seq =:adminSeq "
            , nativeQuery = true
    )
    int updateAdministrativeDivision(@Param("administrativeDivision") String administrativeDivision, @Param("adminSeq") Long adminSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_admin set admin_phone = :adminPhone where admin_seq =:adminSeq "
            , nativeQuery = true
    )
    int updateAdminPhone(@Param("adminPhone") String superYn, @Param("adminSeq") Long adminSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_admin set super_yn = :superYn where admin_seq =:adminSeq "
            , nativeQuery = true
    )
    int updateAdminSuperYn(@Param("superYn") String superYn, @Param("adminSeq") Long adminSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_admin set permission_seq = :permissionSeq where admin_seq =:adminSeq "
            , nativeQuery = true
    )
    int updateAdminPermissionSeq(@Param("permissionSeq") Long permissionSeq, @Param("adminSeq") Long adminSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_admin set use_yn = :useYn where admin_seq =:adminSeq "
            , nativeQuery = true
    )
    int UpdateAdminUseYn(@Param("useYn") String useYn, @Param("adminSeq") Long adminSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_admin set del_yn = :delYn where admin_seq =:adminSeq "
            , nativeQuery = true
    )
    int UpdateAdminDelYn(@Param("delYn") String delYn, @Param("adminSeq") Long adminSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_admin set mod_seq = :modSeq, mod_dt = now() where admin_seq =:adminSeq "
            , nativeQuery = true
    )
    int UpdateMod(@Param("modSeq") Long modSeq, @Param("adminSeq") Long adminSeq);

    @Query(
            value = "select * from tb_admin where administrative_division =:areaName order by admin_seq desc limit 0,1"
            , nativeQuery = true
    )
    Optional<Admin> selectAdministrativeDivision(@Param("areaName") String areaName);

}
