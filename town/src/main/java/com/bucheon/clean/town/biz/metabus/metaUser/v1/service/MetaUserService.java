package com.bucheon.clean.town.biz.metabus.metaUser.v1.service;

import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.*;
import com.bucheon.clean.town.biz.operation.user.v1.service.UserService;
import com.bucheon.clean.town.biz.util.model.ConnectSumLogVO;
import com.bucheon.clean.town.biz.util.service.ConnectSumService;
import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.common.util.Masking;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.metabus.MetaTimeAttact;
import com.bucheon.clean.town.db.entity.metabus.MetaTrashType;
import com.bucheon.clean.town.db.entity.metabus.MetaUser;
import com.bucheon.clean.town.db.entity.metabus.MetaUserPointHistory;
import com.bucheon.clean.town.db.repository.metabus.MetaTimeAttactRepository;
import com.bucheon.clean.town.db.repository.metabus.MetaTrashTypeRepository;
import com.bucheon.clean.town.db.repository.metabus.MetaUserPointHistoryRepository;
import com.bucheon.clean.town.db.repository.metabus.MetaUserRepository;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MetaUserService {

    private final Logger logger = LoggerFactory.getLogger(MetaUserService.class);
    private final HttpServletRequest httpServletRequest;
    private final MetaUserRepository metaUserRepository;
    private final MetaTimeAttactRepository metaTimeAttactRepository;
    private final MetaTrashTypeRepository metaTrashTypeRepository;
    private final MetaUserPointHistoryRepository metaUserPointHistoryRepository;
    private final UserService userService;
    private final ConnectSumService connectSumService;
    private final ModelMapper modelMapper;


    //// Web Service  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Object MetaUserSelectListService(MetaUserReq dto){
        var result = metaUserRepository.MetaUserListSelect(dto).stream()
                .map(e -> modelMapper.map(e, MetaUser.class))
                .peek(f -> {
                    try {
                        f.setEmail(Masking.emailMasking(f.getEmail()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                })
                .collect(Collectors.toList());
        return result;
    }

    public Object MetaUserSelectListPagingService(MetaUserReq dto) {
        var result = metaUserRepository.MetaUserListSelectPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public MetaUser MetaUserSelectOneService(String userId) throws Exception {
        Optional<MetaUser> user = metaUserRepository.findByUserId(userId);
        user.get().setEmail(Masking.emailMasking(user.get().getEmail()));
        return user.orElseThrow(() -> new IllegalArgumentException());
    }


    public int UpdateMetaUserDelService(String[] reportSeql) {
        List<Integer> result =  Arrays.stream(reportSeql).map(e -> metaUserRepository
                .UpdateMetaUserDelYn(YnType.Y.getName(), e)).collect(Collectors.toList());
        return result.size();
    }

    public int UpdateMetaUserUpService(String reportSeql) {
        int result =  metaUserRepository.UpdateMetaUserDelYn(YnType.N.getName(), reportSeql);
        return result;
    }

    //관리자에서 순위 리스트 보기
    public List<MetaAllRankingRes> MetaRankingSelectListService(MetaRankingReq dto) {

        String yyyymm = dto.getYyyymm();

        if(yyyymm == null){
            yyyymm = LocalDate.now().getYear() + "-" + zeroPlus(LocalDate.now().getMonthValue());
        }

        String preyyyymm = preMonth(yyyymm);
        //logger.info("yyyymm -- {}", yyyymm);
        //logger.info("preyyyymm -- {}", preyyyymm);
        List<MetaAllRankingRes> result =  metaUserRepository.selectAdminAllRankingList(preyyyymm, dto.getRankingPage(), dto.getRankingSize());
        return result;
    }


    //// Rest Service  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public MetaUserRes MetaUserSelectOneUserIdService(String userId) {
        MetaUserRes result = modelMapper.map(metaUserRepository.findByUserId(userId).orElseThrow(IllegalArgumentException::new), MetaUserRes.class);
        return result;
    }

    public MetaUserRes MetaUserSelectOneProviderSnsService(String provider, String snsId) {
        MetaUserRes result = modelMapper.map(metaUserRepository.findByProviderAndSnsId(provider, snsId).orElseThrow(IllegalArgumentException::new), MetaUserRes.class);
        return result;
    }

    public MetaUserRes MetaUserSelectOneSnsIdService(String snsId) {
        MetaUserRes result = modelMapper.map(metaUserRepository.findBySnsId(snsId).orElseThrow(IllegalArgumentException::new), MetaUserRes.class);
        return result;
    }

    public MetaUserRes MetaUserSelectOneNicknameService(String nickname) {
        MetaUserRes result = modelMapper.map(metaUserRepository.findByNickname(nickname).orElseThrow(IllegalArgumentException::new), MetaUserRes.class);
        return result;
    }

    public MetaUserRes MetaUserSelectOneEmailService(String email) {
        MetaUserRes result = modelMapper.map(metaUserRepository.findByEmail(email).orElseThrow(IllegalArgumentException::new), MetaUserRes.class);
        return result;
    }




    public int MetaUserSelectCountUserIdService(String userId) {
        return metaUserRepository.countByUserId(userId);
    }

    public int MetaUserSelectCountUserIdAndCleantwonUidService(String userId, String cleantownUid) {
        return metaUserRepository.countByUserIdAndCleantownUid(userId, cleantownUid);
    }

    public int MetaUserSelectCountProviderSnsIdService(String provider, String snsId) {
        return metaUserRepository.countByProviderAndSnsId(provider, snsId);
    }

    public int MetaUserSelectCounNicknameService(String nickname) {
        return metaUserRepository.countByNickname(nickname);
    }

    public int MetaUserSelectCountEmailService(String email) {
        return metaUserRepository.countByEmail(email);
    }

    public int MetaUserSelectCountSnsIdService(String snsId) {
        return metaUserRepository.countBySnsId(snsId);
    }




    public String MetaUserSaveService(MetaUserVO dto) {

        int count = 0;
        int up = 0;

        logger.info(" metaUserdto - - - -{}", new Gson().toJson(dto));

        int cnt = metaUserRepository.countByUserId(dto.getUserId());

        if(cnt == 0) {
            MetaUser metaUser = metaUserRepository.save(dto.ofMetaUSer());
            if (metaUser.getUserId() != null) {
                count = 1;
            }
        }
        else {

            if (!StringUtils.isEmptyOrWhitespace(dto.getNickname())) {
                up = metaUserRepository.updateNickname(dto.getNickname(), dto.getUserId());
                count += up;
            }

            if (!StringUtils.isEmptyOrWhitespace(dto.getEmail())) {
                up = metaUserRepository.updateEmail(dto.getEmail(), dto.getUserId());
                count += up;
            }

            if (!StringUtils.isEmptyOrWhitespace(dto.getProvider())) {
                up = metaUserRepository.updateProvider(dto.getProvider(), dto.getUserId());
                count += up;
            }

            if (!StringUtils.isEmptyOrWhitespace(dto.getSnsId())) {
                up = metaUserRepository.updateSnsId(dto.getSnsId(), dto.getUserId());
                count += up;
            }

            if (String.valueOf(dto.getAvatarType()) != null) {
                up = metaUserRepository.updateAvatarType(dto.getAvatarType(), dto.getUserId());
                count += up;
            }

            if (String.valueOf(dto.getDeviceToken()) != null) {
                up = metaUserRepository.updateDeviceToken(dto.getDeviceToken(), dto.getUserId());
                count += up;
            }

            if (String.valueOf(dto.getDelYn()) != null) {
                up = metaUserRepository.updateDelYn(dto.getDelYn(), dto.getUserId());
                count += up;
            }

        }

        if (count > 0) {
            up = metaUserRepository.updateUpdatedAt(dto.getUserId());
            return "success";
        } else {
            return "fail";
        }
    }

    public String MetaUserLastConnectUpdate(String userId) {

        int up = 0;
        try {

            if (!StringUtils.isEmptyOrWhitespace(userId)) {
                up = metaUserRepository.updateLastConnect(userId);
            }
        } catch (Exception e) {
            up = 0;
        }

        /////// 커넥션 카운팅 추가
        try {
            ConnectSumLogVO connectSumLogVO = new ConnectSumLogVO();
            connectSumLogVO.setConnectDate(LocalDate.now());
            connectSumLogVO.setUserId(userId);
            connectSumLogVO.setConnectType("C");
            connectSumService.ConnectCount(connectSumLogVO);

        } catch (Exception ee) {
            logger.info("일일 카운팅 메타버스 오류남");
        }

        if (up > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    @Transactional
    public Map<String, Object> MetaUserMileageSaveService(MetaUserPointReq dto, String timeAttactYn) {

        Map<String, Object> res = new HashMap<>();

        int count = 0;
        int up = 0;

        logger.info(" metaUserdto - - - -{}", new Gson().toJson(dto));

            if (dto.getTrashSeq() > 0L) {
                MetaTrashType metaTrashType = metaTrashTypeRepository.findByTrashSeq(dto.getTrashSeq()).orElseThrow(IllegalArgumentException::new);
                MetaTimeAttact t = metaTimeAttactRepository.findTop1ByUseYnAndDelYn("Y", "N").orElseThrow(IllegalArgumentException::new);

                int cnt2 = metaUserPointHistoryRepository.countByUserId(dto.getUserId());

                long max = 1L;
                int point = 0;
                int point2 = 0;
                if(cnt2 > 0) {
                    MetaUserPointHistory m2 = metaUserPointHistoryRepository.findTop1ByUserIdOrderByUserSeqDesc(dto.getUserId()).orElseThrow(IllegalArgumentException::new);
                    max = m2.getUserSeq() + 1L;

                    point = (timeAttactYn.equals("Y")) ? m2.getAccumulatedPoint()+(metaTrashType.getTrashPoint() * t.getTimeMultiplication()) : m2.getAccumulatedPoint()+metaTrashType.getTrashPoint();
                    point2 = (timeAttactYn.equals("Y")) ? (metaTrashType.getTrashPoint() * t.getTimeMultiplication()) : metaTrashType.getTrashPoint();
                } else {
                    point = (timeAttactYn.equals("Y")) ? (metaTrashType.getTrashPoint() * t.getTimeMultiplication()) : metaTrashType.getTrashPoint();
                    point2 = (timeAttactYn.equals("Y")) ? (metaTrashType.getTrashPoint() * t.getTimeMultiplication()) : metaTrashType.getTrashPoint();
                }

                //metaUser에 최종마일리지 업데이트한다
                up = metaUserRepository.updateCleanPoint(point2, dto.getUserId());


                MetaUserPointHistory m = new MetaUserPointHistory();
                m.setPoint(point2);
                m.setUserSeq(max);
                m.setAccumulatedPoint(point);
                m.setUserId(dto.getUserId());
                m.setTrashSeq(dto.getTrashSeq());
                m.setReason(metaTrashType.getTrashAct());
                m.setProcessLocation(dto.getProcessLocation());
                m.setRegDt(LocalDateTime.now());
                metaUserPointHistoryRepository.save(m);

                res.put("point", point);

                count += up;
                logger.info(">>>> count : {}", count);
            }

        if (count > 0) {
            res.put("result", "success");
        } else {
            res.put("result", "fail");
        }

        return res;
    }

    public List<MetaTrashTypeUserRes> MetaTrashtypeUserSelectOneService(String userId) {

        logger.info(">>>>>>>>>>> userID : {}", userId);

        List<MetaTrashTypeUserRes> result = metaTrashTypeRepository.selectListMetaUser(userId);
        return result;
    }

    public List<MetaTrashType> MetaTrashtypeListService() {
        return metaTrashTypeRepository.findAll();
    }

    //월별 랭킹 전체 리스트
    public List<MetaAllRankingRes> AllRankingListService(String yyyymm) {

        logger.info("------ 월별 top 100 랭킹 시작 ------");

        if(yyyymm == null){
            yyyymm = LocalDate.now().getYear() + "-" + zeroPlus(LocalDate.now().getMonthValue());
        }

        String preyyyymm = preMonth(yyyymm);
        //logger.info("yyyymm -- {}", yyyymm);
        //logger.info("preyyyymm -- {}", preyyyymm);

        List<MetaAllRankingRes> res = metaUserRepository.selectAllRankingList(preyyyymm);

        logger.info("------ 월별 top 100 랭킹 끝 ------");

        return res;
    }

    //월별 내 랭킹
    public MetaAllRankingRes MyRankingListService(String userId, String yyyymm) {

        logger.info("------ 월별 내 랭킹 끝 ------");

        if(yyyymm == null){
            yyyymm = LocalDate.now().getYear() + "-" + zeroPlus(LocalDate.now().getMonthValue());
        }

        String preyyyymm = preMonth(yyyymm);
        //logger.info("yyyymm -- {}", yyyymm);
        //logger.info("preyyyymm -- {}", preyyyymm);

        MetaAllRankingRes res = metaUserRepository.selectMyRankingList(userId, preyyyymm);
        logger.info("------ 월별 내 랭킹 끝 ------");

        return res;
    }

    //깨끗한 마을 앱 동기화
    public String CleanUserSave(MetaUserReq dto){

        logger.info(">>>> dto : {}", new Gson().toJson(dto));

        if (dto.getUserId() == null || dto.getUserId().equals("")) {
            return "not found";
        }

        //tb_meta_user에서 MetaUserId 검google-AtcoWBiRARXgWBbQjM6J3zKhF0j1/1색
        int cnt2 = MetaUserSelectCountUserIdService(dto.getUserId());
        if(cnt2 == 0){
            return "new member";
        }

        //tb_meta_user에서 CleantownUid 검색 검색
        int cnt3 = MetaUserSelectCountUserIdAndCleantwonUidService(dto.getUserId(), dto.getCleantownUid());
        if(cnt3 > 0){
            return "already member use";
        }

        //tb_user에서 CleantownUid 검색
        int cnt = userService.UsercountMetaUserId(dto.getCleantownUid());
        if(cnt > 0){

            int res = metaUserRepository.updateCleantownUid(dto.getCleantownUid(), dto.getUserId());
            logger.info(">>>> 깨끗한 마을 앱 동기화 : {}, ---- {}", res, dto.getCleantownUid());

            if(res > 0) {
                return "success";
            }
            else {
                return "fail";
            }
        } else {
            return "Not Cleantown Key";
        }

    }

    //깨끗한 마을 앱 초기화
    public String CleanUserNullSave(MetaUserReq dto){

        int res = metaUserRepository.updateCleantownUid(null, dto.getUserId());
        logger.info(">>>> 깨끗한 마을 앱 초기화 : {}", res);

        if(res > 0) {
            return "success";
        }
        else {
            return "fail";
        }
    }


    //CleantownUid 유무확인
    public int UsercountMetaUserId(String mid){

        return metaUserRepository.countByCleantownUid(mid);
    }


    public List<MetaUser> MetaUserSelectDeviceTokenHasListService() {

        return metaUserRepository.findAllByDeviceTokenNotNull();
    }




    //월 10 밑으로 0 붙이기 -> String 화
    public String zeroPlus(int k){
        if(k < 10 )
            return "0" + k;
        else
            return String.valueOf(k);
    }

    public String preMonth(String yyyymm){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        String stDate = yyyymm + "-01 01:01:01.111";
        LocalDateTime dateTime = LocalDateTime.parse(stDate, formatter);
        //logger.info("dateTime -- {}", dateTime);
        LocalDateTime predate = dateTime.minusMonths(1);
        //logger.info("dateTime 마이너스 1-- {}", predate);
        String preyyyymm = predate.getYear() + "-" + zeroPlus(predate.getMonthValue());

        return preyyyymm;
    }
}
