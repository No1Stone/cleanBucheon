package com.bucheon.clean.town.db.repository.metabus.dsl;

import com.bucheon.clean.town.biz.metabus.metaBoard.v1.model.MetaBoardReq;
import com.bucheon.clean.town.biz.metabus.metaBoard.v1.model.MetaBoardVO;
import com.bucheon.clean.town.db.entity.metabus.MetaBoard;
import com.bucheon.clean.town.db.entity.metabus.QMetaBoard;
import com.bucheon.clean.town.db.entity.operation.QAdmin;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.DateExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Aspect
@RequiredArgsConstructor
public class MetaBoardRepositoryDslImpl implements MetaBoardRepositoryDsl {


    private final JPAQueryFactory queryFactory;
    private QMetaBoard qMetaBoard = QMetaBoard.metaBoard;
    private QAdmin qAdmin = QAdmin.admin;
    private final Logger logger = LoggerFactory.getLogger(MetaBoardRepositoryDslImpl.class);

    /// 현재 반영중인 콘텐츠 ////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<MetaBoard> SelectNowListMetaBoard(MetaBoardReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }

        var aa = queryFactory.select(qMetaBoard).from(qMetaBoard)
                .where(
                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",Expressions.currentTimestamp())
                                .between(
                                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaBoard.sdate),
                                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaBoard.edate)
                                ),
                        eqMetaBoardType(dto.getSearchBoardType()),
                        qMetaBoard.useYn.eq("Y"),
                        qMetaBoard.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qMetaBoard.regDt.desc())
                .fetch();
        return aa;
    }

    @Override
    public QueryResults<MetaBoard> SelectNowListMetaBoardPaging(MetaBoardReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        var aa = queryFactory.select(qMetaBoard).from(qMetaBoard)
                .where(
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString(), sr, er),
                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",Expressions.currentTimestamp())
                                .between(
                                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaBoard.sdate),
                                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaBoard.edate)
                                ),
                        eqMetaBoardType(dto.getSearchBoardType()),
                        qMetaBoard.useYn.eq("Y"),
                        qMetaBoard.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qMetaBoard.regDt.desc())
                .fetchResults();

        return aa;
    }
    /// 현재 반영중인 콘텐츠 /////////////////////////////////////////////////////////////////////////////////////////

    /// 지난 콘텐츠 ////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<MetaBoard> SelectPastListMetaBoard(MetaBoardReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }

        var aa = queryFactory.select(qMetaBoard).from(qMetaBoard)
                .where(
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString(), sr, er),
                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",Expressions.currentTimestamp())
                                .gt(
                                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaBoard.edate)
                                ),
                        eqMetaBoardType(dto.getSearchBoardType()),
                        qMetaBoard.useYn.eq("Y"),
                        qMetaBoard.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qMetaBoard.regDt.desc())
                .fetch();
        return aa;
    }

    @Override
    public QueryResults<MetaBoard> SelectPastListMetaBoardPaging(MetaBoardReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        var aa = queryFactory.select(qMetaBoard).from(qMetaBoard)
                .where(
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString(), sr, er),
                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",Expressions.currentTimestamp())
                                .gt(
                                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaBoard.edate)
                                ),
                        eqMetaBoardType(dto.getSearchBoardType()),
                        qMetaBoard.useYn.eq("Y"),
                        qMetaBoard.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qMetaBoard.regDt.desc())
                .fetchResults();

        return aa;
    }
    /// 지난 콘텐츠 ////////////////////////////////////////////////////////////////////////////////////////

    /// 예약 콘텐츠 ////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<MetaBoard> SelectTocomeListMetaBoard(MetaBoardReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }

        var aa = queryFactory.select(qMetaBoard).from(qMetaBoard)
                .where(
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString(), sr, er),
                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",Expressions.currentTimestamp())
                                .lt(
                                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaBoard.sdate)
                                ),
                        eqMetaBoardType(dto.getSearchBoardType()),
                        qMetaBoard.useYn.eq("Y"),
                        qMetaBoard.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qMetaBoard.regDt.desc())
                .fetch();
        return aa;
    }

    @Override
    public QueryResults<MetaBoard> SelectTocomeListMetaBoardPaging(MetaBoardReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        var aa = queryFactory.select(qMetaBoard).from(qMetaBoard)
                .where(
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString(), sr, er),
                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",Expressions.currentTimestamp())
                                .lt(
                                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaBoard.sdate)
                                ),
                        eqMetaBoardType(dto.getSearchBoardType()),
                        qMetaBoard.useYn.eq("Y"),
                        qMetaBoard.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qMetaBoard.regDt.desc())
                .fetchResults();

        return aa;
    }
    /// 예약 콘텐츠 ////////////////////////////////////////////////////////////////////////////////////////

    /// 일반 게시판 ////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<MetaBoard> SelectListMetaBoard(MetaBoardReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }

        var aa = queryFactory.select(qMetaBoard).from(qMetaBoard)
                .where(
                        eqMetaBoardType(dto.getSearchBoardType()),
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString(), sr, er),
//                        qMetaBoard.reportReception.notLike(ReceptionType.A.getName()),
                        qMetaBoard.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qMetaBoard.regDt.desc())
                .fetch();
        return aa;
    }

    @Override
    public QueryResults<MetaBoard> SelectListMetaBoardPaging(MetaBoardReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        var aa = queryFactory.select(qMetaBoard).from(qMetaBoard)
                .where(
                        eqMetaBoardType(dto.getSearchBoardType()),
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString(), sr, er),
//                        qMetaBoard.reportReception.notLike(ReceptionType.A.getName()),
                        qMetaBoard.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .fetchResults();

        return aa;
    }
    /// 일반 게시판 ////////////////////////////////////////////////////////////////////////////////////////

    /// 일반 게시판 2////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public Optional<MetaBoard> selectType(String type) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        //sr = LocalDate.parse(LocalDateTime.now(), format).atStartOfDay();
        //er = LocalDate.parse(LocalDateTime.now(), format).atStartOfDay().plusDays(1L);


        var aa = queryFactory.selectFrom(qMetaBoard)
                .where(
                        qMetaBoard.boardType.eq(type),
                        qMetaBoard.useYn.eq("Y"),
                        qMetaBoard.delYn.eq("N"),
                        DateExpression.currentDate(LocalDateTime.class).between(qMetaBoard.sdate, qMetaBoard.edate)
                )
                .limit(1)
                .offset(0)
                .orderBy(qMetaBoard.regDt.desc())
                .fetchOne();
        return Optional.ofNullable(aa);
    }
    /// 일반 게시판 2 ////////////////////////////////////////////////////////////////////////////////////////

    /// 게시기간 확인 ////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public Long confirmType(MetaBoardVO dto) {

        if(dto.getBoardSeq() == null){
            dto.setBoardSeq(0L);
        }

        var aa = queryFactory.select(qMetaBoard.count()).from(qMetaBoard)
                .where(
                        eqMetaBoardType(dto.getBoardType()),
                        (
                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",dto.getSdate())
                                .between(
                                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaBoard.sdate),
                                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaBoard.edate)
                                )
                                .or(Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",dto.getEdate())
                                        .between(
                                                Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaBoard.sdate),
                                                Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qMetaBoard.edate)
                                        ))
                        ),
                        qMetaBoard.boardSeq.ne(dto.getBoardSeq()),
                        qMetaBoard.delYn.eq("N")
                )
                .fetchOne();
        return aa;
    }
    /// 게시기간 확인 ////////////////////////////////////////////////////////////////////////////////////////



    //전체 검색 종류 확인
    private BooleanExpression eqSearchsearchSelect(String searchSelect, String searchString,
                                                   LocalDateTime sr, LocalDateTime er) {

        try {

            if (searchSelect.equals("searchTitle")) {
                return eqMetaBoardTitle(searchString);
            } else if (searchSelect.equals("searchContent")) {
                return eqMetaBoardContent(searchString);
            } else if (searchSelect.equals("searchReg")) {
                return eqRegdt(sr, er);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqMetaBoardType(String s) {
        if (StringUtils.isEmptyOrWhitespace(s)) {
            return null;
        }
        return qMetaBoard.boardType.eq(s);
    }

    private BooleanExpression eqRegdt(LocalDateTime sdate, LocalDateTime edate) {
        if (sdate == null && edate == null) {
            return null;
        }
        return qMetaBoard.modDt.between(sdate, edate);
    }

    private BooleanExpression eqMetaBoardTitle(String s) {
        if (StringUtils.isEmptyOrWhitespace(s)) {
            return null;
        }
        return qMetaBoard.boardTitle.toLowerCase().contains(s);
    }

    private BooleanExpression eqMetaBoardContent(String con) {
        if (StringUtils.isEmptyOrWhitespace(con)) {
            return null;
        }
        return qMetaBoard.boardContent.toLowerCase().contains(con);
    }


}
