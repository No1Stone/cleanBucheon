package com.bucheon.clean.town.biz.metabus.metaUser.v1.controller;

import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignReq;
import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignRes;
import com.bucheon.clean.town.biz.environmental.campaign.v1.service.CampaignService;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.*;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.service.MetaTrashTypeService;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.service.MetaUserPointHistoryService;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.service.MetaUserService;
import com.bucheon.clean.town.biz.operation.user.v1.service.UserService;
import com.bucheon.clean.town.biz.util.model.ConnectSumLogVO;
import com.bucheon.clean.town.biz.util.service.ConnectSumService;
import com.bucheon.clean.town.db.entity.operation.User;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/metaBus/rest")
@RequiredArgsConstructor
public class MetaUserRestController {

    private final Logger logger = LoggerFactory.getLogger(MetaUserRestController.class);
    private final MetaUserService metaUserService;
    private final MetaTrashTypeService metaTrashTypeService;
    private final CampaignService campaignService;
    private final UserService userService;
    private final MetaUserPointHistoryService metaUserPointHistoryService;
    private final ConnectSumService connectSumService;

    /// 체크 ////////////////////////////////////////////////////////////////////////////////////

    //아이디체크
    @GetMapping(path = "/check/userId/{userId}")
    public Object getCheckUserIdInfo(@PathVariable String userId) {
        Map<String, Object> res = new HashMap<>();
        try{
            res.put("cnt", metaUserService.MetaUserSelectCountUserIdService(userId));
        }catch (Exception e){
            res.put("cnt", "not found");
        }
        return res;
    }

    //프로바이더 snsID 체크
    @GetMapping(path = "/check/{provider}/{snsId}")
    public Object getCheckProviderSnsInfo(@PathVariable String provider, @PathVariable String snsId) {
        Map<String, Object> res = new HashMap<>();
        try{
            res.put("cnt", metaUserService.MetaUserSelectCountProviderSnsIdService(provider, snsId));
        }catch (Exception e){
            res.put("cnt", "not found");
        }
        return res;
    }

    //닉네임 체크
    @GetMapping(path = "/check/nickname/{nickname}")
    public Object getCheckNicknameInfo(@PathVariable String nickname) {
        Map<String, Object> res = new HashMap<>();
        try{
            res.put("cnt", metaUserService.MetaUserSelectCounNicknameService(nickname));
        }catch (Exception e){
            res.put("cnt", "not found");
        }
        return res;
    }

    //이메일 체크
    @GetMapping(path = "/check/email/{email}")
    public Object getCheckEmailInfo(@PathVariable String email) {
        Map<String, Object> res = new HashMap<>();
        try{
            int cnt = metaUserService.MetaUserSelectCountEmailService(email);
            res.put("cnt", cnt);

            if(cnt > 0){
                //추가 요청
                MetaUserRes userinfo = metaUserService.MetaUserSelectOneEmailService(email);
                res.put("provider", userinfo.getProvider());
            }
        }catch (Exception e){
            res.put("cnt", "not found");
        }
        return res;
    }

    //SNS ID 체크
    @GetMapping(path = "/check/snsId/{snsId}")
    public Object getCheckSnsIdInfo(@PathVariable String snsId) {
        Map<String, Object> res = new HashMap<>();
        try{
            res.put("cnt", metaUserService.MetaUserSelectCountSnsIdService(snsId));
        }catch (Exception e){
            res.put("cnt", "not found");
        }
        return res;
    }



    /// 조회  ////////////////////////////////////////////////////////////////////////////////////

    @GetMapping(path = "/get/userId/{userId}")
    public Object getUserIdInfo(@PathVariable String userId) {
        try{

            MetaUserRes userinfo = metaUserService.MetaUserSelectOneUserIdService(userId);
            metaUserService.MetaUserLastConnectUpdate(userId);

            return userinfo;

        }catch (Exception e){
            return "not found";
        }
    }

    @GetMapping(path = "/get/{provider}/{snsId}")
    public Object getUserInfo2(@PathVariable String provider, @PathVariable String snsId) {
        try{

            MetaUserRes userinfo = metaUserService.MetaUserSelectOneProviderSnsService(provider, snsId);
            return userinfo;

        }catch (Exception e){
            return "not found";
        }
    }

    @GetMapping(path = "/get/snsId/{snsId}")
    public Object getUserInfo3(@PathVariable String snsId) {
        try{

            MetaUserRes userinfo = metaUserService.MetaUserSelectOneSnsIdService(snsId);
            return userinfo;

        }catch (Exception e){
            return "not found";
        }
    }

    @GetMapping(path = "/get/nickname/{nickname}")
    public Object getUserInfo4(@PathVariable String nickname) {
        try{

            MetaUserRes userinfo = metaUserService.MetaUserSelectOneNicknameService(nickname);
            return userinfo;

        }catch (Exception e){
            return "not found";
        }
    }

    @GetMapping(path = "/get/email/{email}")
    public Object getUserInfo5(@PathVariable String email) {
        try{

            MetaUserRes userinfo = metaUserService.MetaUserSelectOneNicknameService(email);
            return userinfo;

        }catch (Exception e){
            return "not found";
        }
    }

    @GetMapping(path = "/get/trashType")
    public Object getTrashtypeUserInfo() {
        try{

            MetaTrashTypeVO dto = new MetaTrashTypeVO();
            dto.setDelYn("N");
            dto.setUseYn("Y");
            List<MetaTrashTypeVO> trash = metaTrashTypeService.MetaTrashtypeSelectListService(dto);
            return trash;

        }catch (Exception e){
            return "not found";
        }
    }

    //포인트 획득 리스트
    @GetMapping(path = "/get/act/pointHistory/{userId}/{month}")
    public Object getPointHistoryInfo(@PathVariable String userId, @PathVariable String month) {

        Map<String, Object> res = new HashMap<>();

        try{

            MetaUserPointHistoryReq dto = new MetaUserPointHistoryReq();
            dto.setUserId(userId);
            dto.setMonth(month);

            List<MetaUserPointHistoryRes> list = metaUserPointHistoryService.MyMetaUserPointHistorySelectListService(dto);
            res.put("MetaUserPointHistoryRes", list);

            //페이징 처리 안해서 주석처리
            //Object pageInfo = metaUserPointHistoryService.MyMetaUserPointHistorySelectListServicePaging(dto);
            //res.put("PageInfo", pageInfo);

            return list;

        }catch (Exception e){
            res.put("result", "not found");
            return res;
        }

    }

    //메타버스 전체랭킹 순위 100까지
    @GetMapping(path = "/get/ranking/all/{month}")
    public Object getAllRankingListInfo(@PathVariable String month) {
        return metaUserService.AllRankingListService(month);
    }

    //메타버스 내 순위
    @GetMapping(path = "/get/myRanking/{userId}/{month}")
    public Object getMyRankingListInfo(@PathVariable String userId, @PathVariable String month) {
        return metaUserService.MyRankingListService(userId, month);
    }

    //깨끗한 마을 활동 리스트
    @GetMapping(path = "/get/act/campaignHistory/{userId}/{month}")
    public Object getCampaignHistoryInfo(@PathVariable String userId, @PathVariable String month) throws Exception {

        Map<String, Object> res = new HashMap<>();

        MetaUserRes metaUser = metaUserService.MetaUserSelectOneUserIdService(userId);

        if(metaUserService.UsercountMetaUserId(metaUser.getCleantownUid()) > 0) {

            User user = userService.UserSelectOneService4(metaUser.getCleantownUid());

            if(user != null) {

                CampaignReq campaignReq = new CampaignReq();
                campaignReq.setSize(10);
                campaignReq.setRegUserDid(user.getDid());
                campaignReq.setMonth(month);

                List<CampaignRes> list = campaignService.MyCampaginSelectListService(campaignReq);
                res.put("CampaignRes", list);

                //페이징 처리 안해서 주석처리
                //Object pageInfo = campaginService.MyCampaginSelectListPagingInfoService(campaignReq);
                //res.put("PageInfo", pageInfo);

                return res;
            } else {
                res.put("result", "Not Cleantown Member Search");
                return res;
            }

        } else {
            res.put("result", "Not Cleantown Mapping Key");
            return res;
        }

    }

    //깨끗한 마을 활동 리스트
    @GetMapping(path = "/get/act/campaignHistory2")
    public Object getCampaignHistoryInfo2() throws Exception {

        Map<String, Object> res = new HashMap<>();


        CampaignReq campaignReq = new CampaignReq();
        campaignReq.setSize(10);
        List<CampaignRes> list = campaignService.CampaginSelectListService(campaignReq);
        res.put("CampaignRes", list);
        res.put("result", "Not Cleantown Member Search");
        return res;

    }

    //깨끗한 마을 전체랭킹 순위 100까지
    @GetMapping(path = "/get/campaignRanking/all/{month}")
    public Object getAllCampaignRankingListInfo(@PathVariable String month) {
        return campaignService.AllRankingListService(month);
    }

    //깨끗한 마을 내 순위
    @GetMapping(path = "/get/myCampaignRanking/{userId}/{month}")
    public Object getMyCampaignRankingListInfo(@PathVariable String userId, @PathVariable String month) throws Exception {

        Map<String, Object> res = new HashMap<>();

        //연동 아이디가 있는지 먼저 판단
        MetaUserRes metaUser = metaUserService.MetaUserSelectOneUserIdService(userId);
        int cnt = metaUserService.UsercountMetaUserId(metaUser.getCleantownUid());
        if(cnt > 0) {

            //깨끗한 마을과 연동되어 있는지 판단
            int cnt2 = userService.UsercountMetaUserId(metaUser.getCleantownUid());
            if(cnt2 > 0) {
                User user = userService.UserSelectOneService4(metaUser.getCleantownUid());
                return campaignService.MyRankingListService(user.getDid(), month);
            } else {
                res.put("result", "Not Cleantown Mapping Member.");
                return res;
            }
        }
        else {
            res.put("result", "Not Cleantown Mapping Key.");
            return res;
        }
    }

    /// 등록  ////////////////////////////////////////////////////////////////////////////////////

    @PostMapping(path = "/put/user/new")
    public Object petUserNew(@RequestBody MetaUserVO dto) {
        Map<String, Object> res = new HashMap<>();

        int cnt = metaUserService.MetaUserSelectCountUserIdService(dto.getUserId());
        if(cnt > 0){
            res.put("result", "another person");
            return res;
        }

        try{
            res.put("result", metaUserService.MetaUserSaveService(dto));

        }catch (Exception e){
            res.put("result", "fail");
        }

        /////// 커넥션 카운팅 추가
        try {
            ConnectSumLogVO connectSumLogVO = new ConnectSumLogVO();
            connectSumLogVO.setConnectDate(LocalDate.now());
            connectSumLogVO.setUserId(dto.getUserId());
            connectSumLogVO.setConnectType("M");
            logger.info("connectSumLogVO : {}", new Gson().toJson(connectSumLogVO));
            connectSumService.ConnectCount(connectSumLogVO);

        } catch (Exception ee) {
            logger.info("일일 카운팅 메타버스 회원가입 오류남");
        }

        return res;
    }

    @PostMapping(path = "/put/userId/{userId}")
    public Object petUserInfo(@RequestBody MetaUserVO dto) {
        Map<String, Object> res = new HashMap<>();

        int cnt = metaUserService.MetaUserSelectCountUserIdService(dto.getUserId());
        if(cnt == 0){
            res.put("result", "new member");
            return res;
        }

        try{
            res.put("result", metaUserService.MetaUserSaveService(dto));

        }catch (Exception e){
            res.put("result", "fail");
        }
        return res;
    }

    //마일리지 등록
    @PostMapping(path = "/put/point/{timeAttactYn}")
    public Object petMileageInfo(@PathVariable String timeAttactYn, @RequestBody MetaUserPointReq dto) {
        Map<String, Object> res = new HashMap<>();

        int cnt = metaUserService.MetaUserSelectCountUserIdService(dto.getUserId());
        if(cnt == 0){
            res.put("result", "new member");
            return res;
        }

        try{

            Map<String, Object> res2 = metaUserService.MetaUserMileageSaveService(dto, timeAttactYn);

            int point = 0;
            if(timeAttactYn.equals("Y")) {
                point = (Integer) res2.get("point") * 10;
            } else {
                point = (Integer) res2.get("point");
            }

            res.put("point", point);
            res.put("result", res2.get("result"));

        }catch (Exception e){
            res.put("result", "fail");
        }
        return res;
    }


    /// 연동  ////////////////////////////////////////////////////////////////////////////////////

    //깨끗한마을 연동
    @PostMapping(path = "/put/link")
    public Object putCleantownLink(@RequestBody MetaUserReq dto) {

        Map<String, Object> res = new HashMap<>();
        res.put("result", metaUserService.CleanUserSave(dto));
        return res;
    }

    //깨끗한마을 연동해제
    @PostMapping(path = "/put/unlink")
    public Object putCleantownUnlink(@RequestBody MetaUserReq dto) {

        Map<String, Object> res = new HashMap<>();
        res.put("result", metaUserService.CleanUserNullSave(dto));
        return res;
    }

}
