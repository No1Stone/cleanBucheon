package com.bucheon.clean.town.db.repository.environmental.dsl;

import com.bucheon.clean.town.biz.environmental.report.v1.model.ReportExcelDto;
import com.bucheon.clean.town.biz.environmental.report.v1.model.ReportListReq;
import com.bucheon.clean.town.common.type.ReceptionType;
import com.bucheon.clean.town.db.entity.environmental.QReport;
import com.bucheon.clean.town.db.entity.environmental.Report;
import com.bucheon.clean.town.db.entity.operation.QTrashType;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ConstantImpl;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.core.types.dsl.DateTemplate;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class ReportRepositoryDslImpl implements ReportRepositoryDsl {


    private final JPAQueryFactory queryFactory;
    private QReport qReport = QReport.report;
    private QTrashType qTrashType = QTrashType.trashType;
    private final Logger logger = LoggerFactory.getLogger(ReportRepositoryDslImpl.class);

    @Override
    public List<Report> SelectListReport(ReportListReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sc = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            ec = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }

        logger.info("re P---- {}",ReceptionType.P.getName());
        logger.info("re B---- {}",ReceptionType.B.getName());
        logger.info("re C---- {}",ReceptionType.C.getName());
        var aa = queryFactory.select(qReport).from(qReport)
                .where(
                        eqReportType(dto.getSearchTrashType()),
                        eqReportStatus(dto.getSearchStatus()),
                        eqReporPointComplete(dto.getSearchPointComplete()),
                        eqSearchSelect(dto.getSearchSelect(), dto.getSearchString(), sc, ec, sr, er),
                        eqAdministrativeDivision(dto.getSessionAdministrativeDivision()),
//                        qReport.reportReception.notLike(ReceptionType.A.getName()),
                        qReport.reportReception.eq(dto.getPageType()),
                        qReport.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qReport.regDt.desc())
                .fetch();
        return aa;
    }

    @Override
    public QueryResults<Report> SelectListReportPaging(ReportListReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sc = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            ec = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        logger.info("re P---- {}",ReceptionType.P.getName());
        logger.info("re B---- {}",ReceptionType.B.getName());
        logger.info("re C---- {}",ReceptionType.C.getName());
        var aa = queryFactory.select(qReport).from(qReport)
                .where(
                        eqReportType(dto.getSearchTrashType()),
                        eqReportStatus(dto.getSearchStatus()),
                        eqReporPointComplete(dto.getSearchPointComplete()),
                        eqSearchSelect(dto.getSearchSelect(), dto.getSearchString(), sc, ec, sr, er),
                        eqAdministrativeDivision(dto.getSessionAdministrativeDivision()),
//                        qReport.reportReception.notLike(ReceptionType.A.getName()),
                        qReport.reportReception.eq(dto.getPageType()),
                        qReport.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .fetchResults();

        return aa;
    }

    @Override
    public List<Report> SelectAppListReport(ReportListReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sc = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            ec = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }

        logger.info("re P---- {}",ReceptionType.P.getName());
        logger.info("re B---- {}",ReceptionType.B.getName());
        logger.info("re C---- {}",ReceptionType.C.getName());
        var aa = queryFactory.select(qReport).from(qReport)
                .where(
                        eqReportType(dto.getSearchTrashType()),
                        eqReportStatus(dto.getSearchStatus()),
                        eqReporPointComplete(dto.getSearchPointComplete()),
                        eqSearchSelect(dto.getSearchSelect(), dto.getSearchString(), sc, ec, sr, er),
                        eqAdministrativeDivision(dto.getSessionAdministrativeDivision()),
//                        qReport.reportReception.notLike(ReceptionType.A.getName()),
                        qReport.reportReception.eq(ReceptionType.A.getName()),
                        qReport.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qReport.regDt.desc())
                .fetch();
        return aa;
    }

    @Override
    public QueryResults<Report> SelectAppListReportPaging(ReportListReq dto) {
        LocalDateTime sc = null;
        LocalDateTime ec = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sc = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            ec = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sr = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
        }
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        logger.info("re P---- {}",ReceptionType.P.getName());
        logger.info("re B---- {}",ReceptionType.B.getName());
        logger.info("re C---- {}",ReceptionType.C.getName());
        var aa = queryFactory.select(qReport).from(qReport)
                .where(
                        eqReportType(dto.getSearchTrashType()),
                        eqReportStatus(dto.getSearchStatus()),
                        eqReporPointComplete(dto.getSearchPointComplete()),
                        eqSearchSelect(dto.getSearchSelect(), dto.getSearchString(), sc, ec, sr, er),
                        eqAdministrativeDivision(dto.getSessionAdministrativeDivision()),
//                        qReport.reportReception.notLike(ReceptionType.A.getName()),
                        qReport.reportReception.eq(ReceptionType.A.getName()),
                        qReport.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .fetchResults();

        return aa;
    }

    //EXCEL 다운
    @Override
    public  List<ReportExcelDto> findExcelList(ReportListReq dto) throws NullPointerException, NumberFormatException {
        LocalDateTime yyyymm = null;

        if(dto.getSize() == 0){
            dto.setSize(10);
        }

        logger.info("re P---- {}",ReceptionType.P.getName());
        logger.info("re B---- {}",ReceptionType.B.getName());
        logger.info("re C---- {}",ReceptionType.C.getName());


        var aa = queryFactory.select(
                        Projections.bean(ReportExcelDto.class,
                                new CaseBuilder()
                                        .when(qReport.reportReception.eq("P")).then("전화")
                                        .when(qReport.reportReception.eq("V")).then("방문")
                                        .when(qReport.reportReception.eq("B")).then("밴드")
                                        .when(qReport.reportReception.eq("C")).then("챗봇")
                                        .otherwise("")
                                        .as("reportReception"),
                                qTrashType.trashName,
                                qReport.reportContent,
                                qReport.reportAddrNew,
                                qReport.reportAddrOld,
                                new CaseBuilder()
                                        .when(qReport.reportStatus.eq("D")).then("접수")
                                        .when(qReport.reportStatus.eq("P")).then("처리중")
                                        .when(qReport.reportStatus.eq("C")).then("완료")
                                        .when(qReport.reportStatus.eq("R")).then("처리불가")
                                        .otherwise("")
                                        .as("reportStatus"),
                                qReport.comDt,
                                qReport.regDt,
                                qReport.reportUserName,
                                qReport.reportUserPhone,
                                qReport.reportUserEmail,
                                qReport.areaName,
                                qReport.reportPoint,
                                qReport.reportPointRejectReason
                        )
                )
                .from(qReport)
                .leftJoin(qTrashType).on(qTrashType.trashSeq.eq(qReport.reportType))
                .where(
                        Expressions.stringTemplate(
                                        "DATE_FORMAT({0}, {1})"
                                        , qReport.regDt
                                        , ConstantImpl.create("%Y-%m")).eq(dto.getSelectedExcelYYYYMM()),
                        eqAdministrativeDivision(dto.getSessionAdministrativeDivision()),
                        //                        qReport.reportReception.notLike(ReceptionType.A.getName()),
                        qReport.reportReception.eq(dto.getPageType()),
                        qReport.delYn.eq("N")
                )
                //.limit(dto.getSize())
                //.offset(dto.getPage()*dto.getSize())
                .orderBy(qReport.regDt.desc())
                .fetch();
        return aa;
    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchSelect(String searchSelect, String searchString,
                                                   LocalDateTime sc, LocalDateTime ec, LocalDateTime sr, LocalDateTime er) {

        try {

            if (searchSelect.equals("searchName")) {
                return eqRegName(searchString);
            } else if (searchSelect.equals("searchAreaName")) {
                return eqAreaName(searchString);
            } else if (searchSelect.equals("searchNum")) {
                return eqNum(searchString);
            } else if (searchSelect.equals("searchContent")) {
                return eqReportContent(searchString);
            } else if (searchSelect.equals("searchLocation")) {
                return eqReportAddrold(searchString);
            } else if (searchSelect.equals("searchReg")) {
                return eqRegdt(sr, er);
            } else if (searchSelect.equals("searchComplete")) {
                return eqComdt(sc, ec);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqRegdt(LocalDateTime sdate, LocalDateTime edate) {
        if (sdate == null && edate == null) {
            return null;
        }
        return qReport.modDt.between(sdate, edate);
    }

    private BooleanExpression eqComdt(LocalDateTime sdate, LocalDateTime edate) {
        if (sdate == null && edate == null) {
            return null;
        }
        return qReport.modDt.between(sdate, edate);
    }

    private BooleanExpression eqReportStatus(String reportStatus) {
        if (StringUtils.isEmptyOrWhitespace(reportStatus)) {
            return null;
        }
        return qReport.reportStatus.eq(reportStatus);
    }

    private BooleanExpression eqReporPointComplete(String reporPointComplete) {
        if (StringUtils.isEmptyOrWhitespace(reporPointComplete)) {
            return null;
        }
        return qReport.reportPointCompleteYn.eq(reporPointComplete);
    }

    private BooleanExpression eqReportAddrold(String reportAddrOld) {
        if (StringUtils.isEmptyOrWhitespace(reportAddrOld)) {
            return null;
        }
        return qReport.reportAddrOld.contains(reportAddrOld).or(qReport.reportAddrNew.contains(reportAddrOld));
    }

    private BooleanExpression eqReportReception(String reportReception) {
        if (StringUtils.isEmptyOrWhitespace(reportReception)) {
            return null;
        }
        return qReport.reportReception.eq(reportReception);
    }

    private BooleanExpression eqReportContent(String con) {
        if (StringUtils.isEmptyOrWhitespace(con)) {
            return null;
        }
        return qReport.reportContent.toLowerCase().contains(con);
    }

    private BooleanExpression eqReportType(String type) {
        if (type == null) {
            return null;
        }
        try {
            Long ctype = Long.parseLong(type);
            return qReport.reportType.eq(ctype);
        } catch (Exception e) {
            return null;
        }

    }

    private BooleanExpression eqProcessStatus(String st) {
        if (StringUtils.isEmptyOrWhitespace(st)) {
        return null;
        }
        return qReport.reportStatus.eq(st);
    }

    private BooleanExpression eqAreaName(String st) {
        if (StringUtils.isEmptyOrWhitespace(st)) {
            return null;
        }
        return qReport.areaName.eq(st);
    }

    private BooleanExpression eqNum(String st) {
        if (StringUtils.isEmptyOrWhitespace(st)) {
            return null;
        }
        return qReport.receptionNum.eq(st);
    }

    private BooleanExpression eqRegName(String st) {
        if (StringUtils.isEmptyOrWhitespace(st)) {
            return null;
        }
        return qReport.reportUserName.eq(st);
    }

    private BooleanExpression eqAdministrativeDivision(String st) {

        logger.info("getSessionAdministrativeDivision - {}", st);
        if (StringUtils.isEmptyOrWhitespace(st) || st.equals("전체")) {
            return null;
        }
        return qReport.areaName.eq(st);
    }



//    @Override
//    public List<Report> SelectListDetailReport() {
//        return null;
//    }


//
//    private BooleanExpression valLike(SongCateEnum cate, String val) {
//
//        //검색 때문에 추가
//        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");
//
//        if (cate !=null && val != null && val.length() > 0) {
//            switch (cate) {
//                case All:
//                    return songDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase())
//                            .or(songDetailCodeEntity.description.toLowerCase().contains(val.toLowerCase()));
//                case Value:
//                    return songDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase());
//                case Description:
//                    return songDetailCodeEntity.description.toLowerCase().contains(val.toLowerCase());
//                case RegUsrName:
//                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
//                case ModUsrName:
//                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
//            }
//        }
//        return null;
//    }
}
