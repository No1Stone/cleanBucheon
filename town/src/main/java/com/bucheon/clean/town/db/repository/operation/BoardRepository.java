package com.bucheon.clean.town.db.repository.operation;

import com.bucheon.clean.town.db.entity.operation.Board;
import com.bucheon.clean.town.db.repository.operation.dsl.BoardRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface BoardRepository extends JpaRepository<Board, String>, BoardRepositoryDsl {

    @Transactional(readOnly = true)
    Optional<Board> findByBoardSeqAndDelYn(Long seq, String s);
    @Transactional(readOnly = true)
    int countByBoardSeqAndDelYn(Long seq, String s);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_board set board_title = :boardTitle where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int updateTitle(@Param("boardTitle") String boardTitle, @Param("boardSeq") Long boardSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_board set board_content = :boardContent where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int updateContent(@Param("boardContent") String boardContent, @Param("boardSeq") Long boardSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_board set del_yn = :delYn where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int UpdateBoardDel(@Param("delYn") String delYn, @Param("boardSeq") Long boardSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_board set use_yn = :useYn where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int UpdateBoardUse(@Param("useYn") String useYn, @Param("boardSeq") Long boardSeq);

}
