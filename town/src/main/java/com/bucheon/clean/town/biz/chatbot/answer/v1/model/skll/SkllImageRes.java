package com.bucheon.clean.town.biz.chatbot.answer.v1.model.skll;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SkllImageRes {
    private String skllCode;
    private String imageUrl;
    private String imageDc;
    private int rspnsOrdr;
    private String text;

}
