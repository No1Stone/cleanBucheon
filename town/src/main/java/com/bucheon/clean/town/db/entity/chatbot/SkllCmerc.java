package com.bucheon.clean.town.db.entity.chatbot;

import com.bucheon.clean.town.db.entity.chatbot.id.SkllCmercId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_skll_cmerc")
@Getter
@Setter
@NoArgsConstructor
@IdClass(SkllCmercId.class)
public class SkllCmerc {

    @Id
    @Column(name = "skll_code", nullable = true)
    private String skllCode;
    @Column(name = "goods_dc", nullable = true)
    private String goodsDc;
    @Column(name = "pc", nullable = true)
    private String pc;
    @Column(name = "dscnt", nullable = true)
    private String dscnt;
    @Column(name = "crncy", nullable = true)
    private String crncy;
    @Column(name = "dscnt_rt", nullable = true)
    private String dscntRt;
    @Column(name = "dscnt_pc", nullable = true)
    private String dscntPc;
    @Column(name = "thumb", nullable = true)
    private String thumb;
    @Column(name = "profl", nullable = true)
    private String profl;
    @Column(name = "btton", nullable = true)
    private String btton;
    @Column(name = "cmerc_ordr", nullable = true)
    private String cmercOrdr;
    @Id
    @Column(name = "rspns_ordr", nullable = true)
    private String rspnsOrdr;
    @Column(name = "btton_optn_at", nullable = true)
    private String bttonOptnAt;


    @Builder
    SkllCmerc(
            String skllCode,
            String goodsDc,
            String pc,
            String dscnt,
            String crncy,
            String dscntRt,
            String dscntPc,
            String thumb,
            String profl,
            String btton,
            String cmercOrdr,
            String rspnsOrdr,
            String bttonOptnAt
    ) {
        this.skllCode=skllCode;
        this.goodsDc=goodsDc;
        this.pc=pc;
        this.dscnt=dscnt;
        this.crncy=crncy;
        this.dscntRt=dscntRt;
        this.dscntPc=dscntPc;
        this.thumb=thumb;
        this.profl=profl;
        this.btton=btton;
        this.cmercOrdr=cmercOrdr;
        this.rspnsOrdr=rspnsOrdr;
        this.bttonOptnAt=bttonOptnAt;
    }
}
