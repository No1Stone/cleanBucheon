package com.bucheon.clean.town.db.entity.operation.id;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class MileageHistoryId implements Serializable {

    private Long mileageSeq;
    private Long adminSeq;

}
