package com.bucheon.clean.town.biz.metabus.metaPush.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetaPushReq {

    private int page;
    private int size;

    private String searchSelect;
    private String searchString;
    private String searchSendType;
    private String searchResultType;
    private String sdate;
    private String edate;


}
