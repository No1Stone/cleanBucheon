package com.bucheon.clean.town.db.repository.metabus.dsl;


import com.bucheon.clean.town.biz.metabus.metaPush.v1.model.MetaPushReq;
import com.bucheon.clean.town.db.entity.metabus.MetaPush;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MetaPushRepositoryDsl {

    @Transactional(readOnly = true)
    List<MetaPush> SelectListMetaPush(MetaPushReq dto);

    @Transactional(readOnly = true)
    QueryResults<MetaPush> SelectListMetaPushPaging(MetaPushReq dto);


}
