package com.bucheon.clean.town.biz.keyword.v1.service.synonym;

import com.bucheon.clean.town.biz.keyword.v1.model.InterestWord;
import com.bucheon.clean.town.biz.keyword.v1.model.Synonym;
import com.bucheon.clean.town.biz.keyword.v1.model.WordReq;
import com.bucheon.clean.town.common.util.Paging;

import java.util.List;

public interface SynonymService {

	Paging getPage(WordReq dto);

	List<Synonym> getList(WordReq dto);

	Synonym getOne(Synonym dto);

	int save(Synonym stopword);

	int update(Synonym stopword);

	int delete(String[] representativeWord1);

}