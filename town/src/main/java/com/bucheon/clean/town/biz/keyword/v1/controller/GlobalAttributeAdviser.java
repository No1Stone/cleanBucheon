package com.bucheon.clean.town.biz.keyword.v1.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalAttributeAdviser {

	private Map<String, String> busiBranCodeMap;

    @Value("busi_bran.txt")
    private ClassPathResource resource;

	public Map<String, String> getCodeMap() throws FileNotFoundException, IOException {
		this.busiBranCodeMap = new HashMap<>();

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()), 16 * 1024)) {
			String line;
			while ((line = reader.readLine()) != null) {
				String[] inputs = line.split(" ");
				busiBranCodeMap.put(inputs[0], inputs[1]);
			}
		}

		return this.busiBranCodeMap;
	}
}
