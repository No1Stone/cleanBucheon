package com.bucheon.clean.town.common.util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.util.Base64Utils;

/**
 * @FileName : EncDecUtil.java
 * @Project : system
 * @작성일/@작성자 : 2022. 5. 25. / choo9
 * @변경이력 :
 * @프로그램 설명 : 암복호화모듈
 */
public class EncDecUtil {

	private static final String SEPARATEKEY = "rhdrhdtjqltmdPdir2020";
	private static final String CIPHER_INSTANCE = "AES/CBC/PKCS5Padding";
	private static final int SIZE = 1000;
	private static final int LENGTH = 256;

	private static final String INSTANSE = "PBKDF2WithHmacSHA1";
	private static final String ORDER = "AES";
	private static final String ENCORDING = "UTF-8";

	/**
	 * @Method Name : encrypt
	 * @작성일 : 2021. 3. 3.
	 * @작성자 : 추윤지
	 * @변경이력 :
	 * @Method 설명 : 암호화 소스
	 * @param msg
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws UnsupportedEncodingException
	 * @throws BadPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidParameterSpecException
	 * 
	 */
	public static String encrypt(String msg)
			throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException,
			UnsupportedEncodingException, BadPaddingException, InvalidKeyException, InvalidParameterSpecException {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		byte[] saltBytes = bytes;

		// Password-Based Key Derivation function 2
		SecretKeyFactory factory = SecretKeyFactory.getInstance(INSTANSE);
		// 70000번 해시하여 256 bit 길이의 키를 만든다.
		PBEKeySpec spec = new PBEKeySpec(SEPARATEKEY.toCharArray(), saltBytes, SIZE, LENGTH);
		SecretKey secretKey = factory.generateSecret(spec);
		SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), ORDER);

		// 알고리즘/모드/패딩
		// CBC : Cipher Block Chaining Mode
		Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
		cipher.init(Cipher.ENCRYPT_MODE, secret);
		AlgorithmParameters params = cipher.getParameters();

		// Initial Vector(1단계 암호화 블록용)
		byte[] ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV();
		byte[] encryptedTextBytes = cipher.doFinal(msg.getBytes("UTF-8"));
		byte[] buffer = new byte[saltBytes.length + ivBytes.length + encryptedTextBytes.length];

		System.arraycopy(saltBytes, 0, buffer, 0, saltBytes.length);
		System.arraycopy(ivBytes, 0, buffer, saltBytes.length, ivBytes.length);
		System.arraycopy(encryptedTextBytes, 0, buffer, saltBytes.length + ivBytes.length, encryptedTextBytes.length);

//		String sEncode = Base64.getEncoder().encodeToString(buffer);
		String sEncode = Base64Utils.encodeToUrlSafeString(buffer);
		System.out.println("encrypt : " + sEncode);
		byte[] test = Base64Utils.decodeFromUrlSafeString(sEncode);
		System.out.println("derypt  : " + test.toString());

//		sEncode = sEncode.replaceAll("\\+", "%2B");
		return sEncode;
	}

	/**
	 * @Method Name : decrypt
	 * @작성일 : 2021. 3. 3.
	 * @작성자 : 추윤지
	 * @변경이력 :
	 * @Method 설명 : 복호화 소스
	 * @param msg
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws UnsupportedEncodingException
	 * @throws BadPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidParameterSpecException
	 * @throws InvalidAlgorithmParameterException
	 * 
	 */
	public static String decrypt(String msg) throws NoSuchAlgorithmException, InvalidKeySpecException,
			NoSuchPaddingException, IllegalBlockSizeException, UnsupportedEncodingException, BadPaddingException,
			InvalidKeyException, InvalidParameterSpecException, InvalidAlgorithmParameterException {
//		msg = msg.replaceAll("%2B", "\\+");
		Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
//		ByteBuffer buffer = ByteBuffer.wrap(Base64.getDecoder().decode(msg));
		ByteBuffer buffer = ByteBuffer.wrap(Base64Utils.decodeFromUrlSafeString(msg));

		byte[] saltBytes = new byte[20];
		buffer.get(saltBytes, 0, saltBytes.length);
		byte[] ivBytes = new byte[cipher.getBlockSize()];
		buffer.get(ivBytes, 0, ivBytes.length);
		byte[] encryoptedTextBytes = new byte[buffer.capacity() - saltBytes.length - ivBytes.length];
		buffer.get(encryoptedTextBytes);

		SecretKeyFactory factory = SecretKeyFactory.getInstance(INSTANSE);
		PBEKeySpec spec = new PBEKeySpec(SEPARATEKEY.toCharArray(), saltBytes, SIZE, LENGTH);
		SecretKey secretKey = factory.generateSecret(spec);
		SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), ORDER);

		cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(ivBytes));
		byte[] decryptedTextBytes = cipher.doFinal(encryoptedTextBytes);

		return new String(decryptedTextBytes, ENCORDING);
	}
}
