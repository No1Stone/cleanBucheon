package com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChatbotPoto {

    //classPathUrl
    private String classUrl;
    //fileName
    private String fileName;
    //fileSize
    private String fileSize;
    //fileTyle ex jpg, mp4, gif
    private String fileType;


}
