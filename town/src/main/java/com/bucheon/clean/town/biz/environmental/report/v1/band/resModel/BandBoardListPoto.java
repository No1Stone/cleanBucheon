package com.bucheon.clean.town.biz.environmental.report.v1.band.resModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BandBoardListPoto {

    private String post_key;
    private int height;
    private int width;
    private long created_at;
    private String url;
    private String photo_album_key;
    private String photo_key;
    private int comment_count;
    private int emotion_count;
    private boolean is_video_thumbnail;
    private String name;
    private String description;
    private String role;
    private String profile_image_url;
    private String user_key;

    public BandBoardListPoto(){};

    public BandBoardListPoto(String post_key, int height, int width, long created_at, String url, String photo_album_key, String photo_key, int comment_count, int emotion_count, boolean is_video_thumbnail, String name, String description, String role, String profile_image_url, String user_key) {
        this.post_key = post_key;
        this.height = height;
        this.width = width;
        this.created_at = created_at;
        this.url = url;
        this.photo_album_key = photo_album_key;
        this.photo_key = photo_key;
        this.comment_count = comment_count;
        this.emotion_count = emotion_count;
        this.is_video_thumbnail = is_video_thumbnail;
        this.name = name;
        this.description = description;
        this.role = role;
        this.profile_image_url = profile_image_url;
        this.user_key = user_key;
    }

    public BandBoardListPoto(Author author) {
        this.name = author.getName();
        this.description = author.getDescription();
        this.role = author.getRole();
        this.profile_image_url = author.getProfile_image_url();
        this.user_key = author.getUser_key();
    }
}
