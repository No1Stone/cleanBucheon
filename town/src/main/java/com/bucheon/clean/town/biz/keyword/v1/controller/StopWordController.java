package com.bucheon.clean.town.biz.keyword.v1.controller;

import com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.controller.AlaramtalkController;
import com.bucheon.clean.town.biz.keyword.v1.model.Stopword;
import com.bucheon.clean.town.biz.keyword.v1.model.WordReq;
import com.bucheon.clean.town.biz.keyword.v1.service.stopWord.StopwordService;
import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminReq;
import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminRes;
import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminUpdate;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.permission.v1.model.PermissionReq;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.clean.town.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
@RequestMapping(path = "/stopword")
@RequiredArgsConstructor
public class StopWordController {

    private final Logger logger = LoggerFactory.getLogger(AlaramtalkController.class);
    private final MenuService menuService;
    private final StopwordService stopwordService;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/keyword/stopWord/stopWordManage");
    }

    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(WordReq dto, HttpServletRequest request) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(36L);

        dto.setPage(0);
        dto.setSize(10);
        dto.setSearchWord1("");
        dto.setSearchState("all");

        return new ModelAndView("keyword/stopWord/stopWordManage")
                .addObject("paging",stopwordService.getPage(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(WordReq dto){
        return new ModelAndView("keyword/stopWord/stopWordListAjax")
                .addObject("list",stopwordService.getList(dto))
                .addObject("paging",stopwordService.getPage(dto));
    }

    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm(){
        return new ModelAndView("keyword/stopWord/stopWordManagePopupAjax")
                .addObject("word", new Stopword());
    }

    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView modifyForm(String word){
        return new ModelAndView("keyword/stopWord/stopWordManagePopupAjax")
                .addObject("word", stopwordService.getOne(word));
    }

    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(Stopword stopword){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", stopwordService.save(stopword));
        return map;
    }

    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(Stopword stopword){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", stopwordService.update(stopword));
        return map;
    }

    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(String[] word){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result",  stopwordService.delete(word));
        return map;
    }
}
