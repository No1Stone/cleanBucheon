package com.bucheon.clean.town.biz.util.model;

import lombok.Data;

@Data
public class CommResponse {

    private String cdResult;
    private String msgResult;
}
