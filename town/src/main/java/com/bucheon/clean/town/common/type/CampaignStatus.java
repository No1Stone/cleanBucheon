package com.bucheon.clean.town.common.type;

public enum CampaignStatus {
    T("T","쓰레기접수"),
    P("P","청소중"),
    C("C","청소완료")
    ;
    private String code;
    private String description;

    CampaignStatus(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }
    public String getDescription() {
        return this.description;
    }
    public String getName() {
        return name();
    }

}
