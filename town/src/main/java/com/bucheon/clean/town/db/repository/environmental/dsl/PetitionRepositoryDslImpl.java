package com.bucheon.clean.town.db.repository.environmental.dsl;

import com.bucheon.clean.town.biz.environmental.campaign.v1.controller.test14;
import com.bucheon.clean.town.biz.environmental.complaint.v1.model.ComplaintsReq;
import com.bucheon.clean.town.db.entity.environmental.*;
import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.DateTimeTemplate;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class PetitionRepositoryDslImpl implements PetitionRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QPetition qPetition = QPetition.petition;
    private QPetitionImage qPetitionImage = QPetitionImage.petitionImage;
    private QProcess qProcess = QProcess.process;
    private QReceipt qReceipt = QReceipt.receipt;
    private final Logger logger = LoggerFactory.getLogger(PetitionRepositoryDslImpl.class);

    @Override
    public List<Petition> DynamicSelect(ComplaintsReq dto) {

        logger.info(" peti - {}", new Gson().toJson(dto));

        if (dto.getSize() == 0) {
            dto.setSize(10);
        }

        LocalDateTime sp = null;
        LocalDateTime ep = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sp = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            ep = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
            logger.info("sp - {}", sp);
            logger.info("ep - {}", ep);
        }

//        private String comTitle;      //  qPetition.title
//        private String comType;       //  qPetition.reason
//        private String comProcess;    //  qProcess.statusName
//        private String comOffice;     //  qProcess.dutyDeptCode
//        private String comOfficeUser; //  qProcess.dutyName
//        private String sproDt;        //  qProcess.doDateTime
//        private String eproDt;        //  qProcess.doDateTime
//        private String sregDt;        //  qPetition.createdDate
//        private String eregDt;        //  qPetition.createdDate

        QueryResults<Petition> aa = queryFactory.selectFrom(qPetition)
                .leftJoin(qPetitionImage).on(qPetitionImage.petiNo.eq(qPetition.petiNo))
                .leftJoin(qProcess).on(qProcess.petiNo.eq(qPetition.petiNo))
                .leftJoin(qReceipt).on(qReceipt.petiNo.eq(qPetition.petiNo))
                .where(
                        (qReceipt.busiBranCode.contains("S1006416")
                                .or(qReceipt.busiBranCode.contains("S1006417"))
                                .or(qReceipt.busiBranCode.contains("S1006418"))
                                .or(qReceipt.busiBranCode.contains("S1006419"))
                                .or(qReceipt.busiBranCode.contains("S1006420"))
                                .or(qReceipt.busiBranCode.contains("S1006422"))),
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString(), sp, ep),
                        eqBusibranCode(dto.getSearchTrashType()),
                        eqStatus(dto.getSearchStatus())

                )
                .limit(dto.getSize())
                .offset(dto.getPage() * dto.getSize())
                .orderBy(qPetition.regDateTime.desc())
                .fetchResults();
        return aa.getResults();
    }

    @Override
    public QueryResults<Petition> DynamicSelectPaging(ComplaintsReq dto) {

        logger.info(" peti - {}", new Gson().toJson(dto));

        if (dto.getSize() == 0) {
            dto.setSize(10);
        }

        LocalDateTime sp = null;
        LocalDateTime ep = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getSdate()) && !StringUtils.isEmptyOrWhitespace(dto.getEdate())) {
            sp = LocalDate.parse(dto.getSdate(), format).atStartOfDay();
            ep = LocalDate.parse(dto.getEdate(), format).atStartOfDay().plusDays(1L);
            logger.info("sp - {}", sp);
            logger.info("ep - {}", ep);
        }

//        private String comTitle;      //  qPetition.title
//        private String comType;       //  qPetition.reason
//        private String comProcess;    //  qProcess.statusName
//        private String comOffice;     //  qProcess.dutyDeptCode
//        private String comOfficeUser; //  qProcess.dutyName
//        private String sproDt;        //  qProcess.doDateTime
//        private String eproDt;        //  qProcess.doDateTime
//        private String sregDt;        //  qPetition.createdDate
//        private String eregDt;        //  qPetition.createdDate

        QueryResults<Petition> aa = queryFactory.selectFrom(qPetition)
                .leftJoin(qPetitionImage).on(qPetitionImage.petiNo.eq(qPetition.petiNo))
                .leftJoin(qProcess).on(qProcess.petiNo.eq(qPetition.petiNo))
                .leftJoin(qReceipt).on(qReceipt.petiNo.eq(qPetition.petiNo))
                .where(
                        (qReceipt.busiBranCode.contains("S1006416")
                                .or(qReceipt.busiBranCode.contains("S1006417"))
                                .or(qReceipt.busiBranCode.contains("S1006418"))
                                .or(qReceipt.busiBranCode.contains("S1006419"))
                                .or(qReceipt.busiBranCode.contains("S1006420"))
                                .or(qReceipt.busiBranCode.contains("S1006422"))),
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString(), sp, ep),
                        eqBusibranCode(dto.getSearchTrashType()),
                        eqStatus(dto.getSearchStatus())
                )
                .limit(dto.getSize())
                .offset(dto.getPage())
                .fetchResults();
        return aa;
    }

    @Override
    public List<test14> DslTest1(ComplaintsReq dto) {


        logger.info(" peti - {}", new Gson().toJson(dto));

        if (dto.getSize() == 0) {
            dto.setSize(10);
        }

        LocalDateTime sp = null;
        LocalDateTime ep = null;
        LocalDateTime sr = null;
        LocalDateTime er = null;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (!StringUtils.isEmptyOrWhitespace(dto.getEproDt()) && !StringUtils.isEmptyOrWhitespace(dto.getSproDt())) {
            sp = LocalDate.parse(dto.getSproDt(), format).atStartOfDay();
            ep = LocalDate.parse(dto.getEproDt(), format).atStartOfDay().plusDays(1L);
            logger.info("sp - {}", sp);
            logger.info("ep - {}", ep);
        }
        if (!StringUtils.isEmptyOrWhitespace(dto.getSregDt()) && !StringUtils.isEmptyOrWhitespace(dto.getEregDt())) {
            sr = LocalDate.parse(dto.getSregDt(), format).atStartOfDay();
            er = LocalDate.parse(dto.getEregDt(), format).atStartOfDay().plusDays(1L);
            logger.info("sr - {}", sr);
            logger.info("er - {}", er);
        }

//        private String comTitle;      //  qPetition.title
//        private String comType;       //  qPetition.reason
//        private String comProcess;    //  qProcess.statusName
//        private String comOffice;     //  qProcess.dutyDeptCode
//        private String comOfficeUser; //  qProcess.dutyName
//        private String sproDt;        //  qProcess.doDateTime
//        private String eproDt;        //  qProcess.doDateTime
//        private String sregDt;        //  qPetition.createdDate
//        private String eregDt;        //  qPetition.createdDate
        var aa = queryFactory.select
                        (qPetition.reason, qProcess.statusName, qReceipt.regDateTime).from(qPetition)
                .leftJoin(qPetitionImage).on(qPetitionImage.petiNo.eq(qPetition.petiNo))
                .leftJoin(qProcess).on(qProcess.petiNo.eq(qPetition.petiNo))
                .leftJoin(qReceipt).on(qReceipt.petiNo.eq(qPetition.petiNo))
                .where(eqTitle(dto.getComTitle()),
                        eqReason(dto.getComType()),
                        eqStatusName(dto.getComProcess()),
                        eqDutyDeptCode(dto.getComOffice()),
                        eqDutyName (dto.getComOfficeUser()),
                        eqDoDateTime(sp, ep),
                        eqCreatedDate(sr, er)
                )
                .limit(dto.getSize())
                .offset(dto.getPage() * dto.getSize())
                .orderBy(qPetition.regDateTime.desc())
                .fetchResults();
        logger.info(" test Result  - {} ", new Gson().toJson(aa.getResults()));

        List<test14> al = new ArrayList<>();
        for(Tuple a : aa.getResults()){
            test14 a14 = new test14();
            a14.setA(a.get(qPetition.reason));
            a14.setB(a.get(qProcess.statusName));
            a14.setB(a.get(qReceipt.regDateTime));
            al.add(a14);
        }


        return al;
    }


    //전체 검색 종류 확인
    private BooleanExpression eqSearchsearchSelect(String searchSelect, String searchString,
                                                   LocalDateTime sp, LocalDateTime ep) {


        try {

            System.out.println(">>>>>>>> searchSelect : " + searchSelect);

            if (searchSelect.equals("searchTitle")) {
                return eqTitle(searchString);
            } else if (searchSelect.equals("searchContent")) {
                return eqReason(searchString);
            } else if (searchSelect.equals("searchRegName")) {
                return eqAncName(searchString);
            } else if (searchSelect.equals("searchCompleteName")) {
                return eqDutyName(searchString);
            } else if (searchSelect.equals("searchReg")) {
                return eqCreatedDate(sp, ep);
            } else if (searchSelect.equals("searchComplete")) {
                return eqDoDateTime(sp, ep);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }


    private BooleanExpression eqTitle(String title) {
        if (StringUtils.isEmptyOrWhitespace(title)) {
            return null;
        }
        return qPetition.title.contains(title);
    }

    private BooleanExpression eqReason(String reason) {
        if (StringUtils.isEmptyOrWhitespace(reason)) {
            return null;
        }
        return qPetition.reason.eq(reason);
    }

    private BooleanExpression eqStatusName(String statusName) {
        if (StringUtils.isEmptyOrWhitespace(statusName)) {
            return null;
        }
        return qProcess.statusName.eq(statusName);
    }

    private BooleanExpression eqDutyDeptCode(String dutyDeptCode) {
        if (StringUtils.isEmptyOrWhitespace(dutyDeptCode)) {
            return null;
        }
        return qProcess.dutyDeptCode.eq(dutyDeptCode);
    }

    private BooleanExpression eqAncName(String ancName) {
        if (StringUtils.isEmptyOrWhitespace(ancName)) {
            return null;
        }
        return qReceipt.ancName.contains(ancName);
    }

    private BooleanExpression eqDutyName(String dutyName) {
        if (StringUtils.isEmptyOrWhitespace(dutyName)) {
            return null;
        }
        return qProcess.dutyName.contains(dutyName);
    }

    private BooleanExpression eqDoDateTime(LocalDateTime sdoDateTime, LocalDateTime edoDateTime) {
        if (sdoDateTime == null && edoDateTime == null) {
            return null;
        }
        DateTimeTemplate<LocalDateTime> date =
                Expressions.dateTimeTemplate(LocalDateTime.class, "STR_TO_DATE({0},{1})",
                        qProcess.doDateTime, "%Y%m%d%H%i%s");
//                Expressions.dateTimeTemplate(LocalDateTime.class, "TIMESTAMP({0})", qProcess.doDateTime);
//        logger.info("date1 -- {}",date);
        return date.between(sdoDateTime, edoDateTime);
    }

    private BooleanExpression eqCreatedDate(LocalDateTime sCreatedDate, LocalDateTime eCreatedDate) {
        if (sCreatedDate == null && eCreatedDate == null) {
            return null;
        }
        DateTimeTemplate<LocalDateTime> date =
//        Expressions.dateTimeTemplate(LocalDateTime.class, "{0}", qPetition.createdDate);
                Expressions.dateTimeTemplate(LocalDateTime.class, "STR_TO_DATE({0},{1})",
                        qPetition.regDateTime, "%Y%m%d%H%i%s");
        logger.info("date2 -- {}", date);
        return date.between(sCreatedDate, eCreatedDate);
    }



    private BooleanExpression eqStatus(String code) {
        if (StringUtils.isEmptyOrWhitespace(code)) {
            return null;
        }
        return qProcess.statusCode.contains(code);
    }

    private BooleanExpression eqBusibranCode(String code) {
        if (StringUtils.isEmptyOrWhitespace(code)) {
            return null;
        }
        return qReceipt.busiBranCode.contains(code);
    }


}
