package com.bucheon.clean.town.biz.chatbot.answer.v1.service;

import com.bucheon.clean.town.biz.chatbot.answer.v1.model.*;
import com.bucheon.clean.town.biz.chatbot.answer.v1.model.skll.SkllCardRes;
import com.bucheon.clean.town.biz.chatbot.answer.v1.model.skll.SkllImageRes;
import com.bucheon.clean.town.biz.chatbot.answer.v1.model.skll.SkllItmRes;
import com.bucheon.clean.town.biz.chatbot.answer.v1.model.skll.SkllListRes;
import com.bucheon.clean.town.common.type.Definition;
import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.chatbot.*;
import com.bucheon.clean.town.db.repository.chatbot.*;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AnswerService {

    private final Logger logger = LoggerFactory.getLogger(AnswerService.class);
    private final HttpServletRequest httpServletRequest;
    private final ModelMapper modelMapper;

    private final SkllRepository skllRepository;
    private final SkllCardRepository skllCardRepository;
    private final SkllImageRepository skllImageRepository;
    private final SkllItmRepository skllItmRepository;
    private final SkllListRepository skllListRepository;
    private final SkllRplyRepository skllRplyRepository;
    private final SkllTextRepository skllTextRepository;
    private final SkllCmercRepository skllCmercRepository;
    private final SkllMngRepository skllMngRepository;

    @Autowired
    SkillManageVOService VOService;


    @Transactional(readOnly = true)
    public String SkllText(String code, int rspns) {
        SkllText skllTexts = skllTextRepository.findBySkllCodeAndRspnsOrdr(code, rspns)
                .orElse(null);
        return skllTexts.getText();
    }

    @Transactional(readOnly = true)
    public List<SkllCardRes> SkllCardSelectList() {
        List<SkllCardRes> skllCards = skllCardRepository.findAll()
                .stream().map(e -> modelMapper.map(e, SkllCardRes.class))
                .peek(f -> f.setText(SkllText(f.getSkllCode(), f.getRspnsOrdr())))
                .collect(Collectors.toList());
        return skllCards;
    }

    @Transactional(readOnly = true)
    public List<SkllImageRes> SkllImageSelectList() {
        List<SkllImageRes> skllImageRes = skllImageRepository.findAll()
                .stream().map(e -> modelMapper.map(e, SkllImageRes.class))
                .peek(f -> f.setText(SkllText(f.getSkllCode(), f.getRspnsOrdr())))
                .collect(Collectors.toList());
        return skllImageRes;
    }

    @Transactional(readOnly = true)
    public List<SkllItmRes> SkllItmSelectList() {
        List<SkllItmRes> skllItmRes = skllItmRepository.findAll()
                .stream().map(e -> modelMapper.map(e, SkllItmRes.class))
                .peek(f -> f.setText(SkllText(f.getSkllCode(), Integer.parseInt(f.getRspnsOrdr()))))
                .collect(Collectors.toList());
        return skllItmRes;
    }

    @Transactional(readOnly = true)
    public List<SkllListRes> SkllListSelectList() {
        List<SkllListRes> skllListRes = skllListRepository.findAll()
                .stream().map(e -> modelMapper.map(e, SkllListRes.class))
                .peek(f -> f.setText(SkllText(f.getSkllCode(), f.getRspnsOrdr())))
                .collect(Collectors.toList());
        return skllListRes;
    }

    @Transactional(readOnly = true)
    public List<SkillManageRVO> SkllSelectList(AnswerReq dto) {
//        Map<String, String> skll = skllRepository.findAll()
//                .stream().collect(Collectors.toMap(Skll::getSkillCode, Skll::getSkillName));
//        List<SkllRes> skllRes = skllRepository.SkllSelectList(dto)
//                .stream().map(e -> modelMapper.map(e, SkllRes.class))
//                .collect(Collectors.toList());
        List<SkillManageRVO> skllRes = skllRepository.SkllSelectList(dto);
        return skllRes;
    }


    @Transactional(readOnly = true)
    public Paging SkllSelectListPagingService(AnswerReq dto) {
        QueryResults<SkillManageRVO> result = skllRepository.SkllSelectListPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    @Transactional(readOnly = true)
    public Object SkllSelectOneService(String code) {

        Optional<Skll> skll = Optional.ofNullable(skllRepository.findBySkllCode(code).orElseThrow(() -> new IllegalArgumentException()));

        Optional<SkillManageRVO> skillManageRVO =
                    skll.map(p -> SkillManageRVO.builder()
                            .skillCode(p.getSkllCode())
                            .skillName(p.getSkllNm())
                            .resType(p.getRspnsTy())
                            .resType1(p.getRspnsTy1())
                            .resType2(p.getRspnsTy2())
                            .resType3(p.getRspnsTy3())
                            .registDe(String.valueOf(p.getRegistDe()))
                            .updtDe(String.valueOf(p.getUpdtDe()))
                            .skillcategory(p.getSkllCatgory())
                            //.resSeq(p.getSe())
                            .skillLevel(p.getSkllLevel())
                            .skillLevel1(p.getSkllLevel1())
                            .skillLevel2(p.getSkllLevel2())
                            .skillLevel3(p.getSkllLevel3())
                            .skillLevel4(p.getSkllLevel4())
                            .build()
                    );

        return skillManageRVO.get();
    }

    @Transactional(readOnly = true)
    public Object selectSkillSTInfo(String code, int reqSeq) {

        SimpleTextVO skllText = skllTextRepository.findBySkllStCodeAndRspnsOrdr(code, reqSeq);

        return skllText;
    }

    @Transactional(readOnly = true)
    public Object selectSkillSIInfo(String code, int reqSeq) {

        SimpleImageVO skllImage = skllImageRepository.findBySkllSiCodeAndRspnsOrdr(code, reqSeq);

        return skllImage;
    }

    @Transactional(readOnly = true)
    public Object selectSkillBCInfo(String code, int reqSeq) throws JsonParseException, JsonMappingException, IOException {

        List<BasicCardVO> bcVO = VOService.setBCVO(skllCardRepository.selectSkllBcCodeAndRspnsOrdr(code, reqSeq));

        return bcVO;
    }

    @Transactional(readOnly = true)
    public Object selectSkillCCInfo(String code, int reqSeq) throws JsonParseException, JsonMappingException, IOException {
        //List<ListCardVO> skllCard = skllCardRepository.selectSkllBcCodeAndRspnsOrdr(code, reqSeq);
        //List<CommerceCardVO> ccVO = skllCmercRepository.selectSkllCcCodeAndRspnsOrdr(code, reqSeq);
        List<CommerceCardVO> ccVO = VOService.setCCVO(skllCmercRepository.selectSkllCcCodeAndRspnsOrdr(code, reqSeq));

        return ccVO;
    }

    @Transactional(readOnly = true)
    public Object selectSkillLCInfo(String code, int reqSeq) throws JsonParseException, JsonMappingException, IOException {

        ListCardVO lcVO = VOService.setLCVO(skllListRepository.selectSkllCcCodeAndRspnsOrdr(code, reqSeq));

        return lcVO;
    }

    @Transactional(readOnly = true)
    public Object selectQuickReplies(String code) {

        List<QuickRepliesVO> qrVO = skllRplyRepository.selectQuickReplies(code);

        return qrVO;
    }







//    @Transactional(readOnly = true)
//    public List<SkllRplyRes> SkllRplySelectList(){
//        List<SkllRplyRes> skllRplyRes = skllRplyRepository.findAll()
//    }

    @Transactional
    public int DeleteAnswerService(String[] reportSeql) {
        List<Integer> result =  Arrays.stream(reportSeql).map(e -> skllRepository
                .DeleteAnswerSkllCode(e)).collect(Collectors.toList());
        List<Integer> result2 =  Arrays.stream(reportSeql).map(e -> skllCardRepository
                .DeleteAnswerSkllCardCode(e)).collect(Collectors.toList());
        List<Integer> result3 =  Arrays.stream(reportSeql).map(e -> skllCmercRepository
                .DeleteAnswerSkllCmercCode(e)).collect(Collectors.toList());
        List<Integer> result4 =  Arrays.stream(reportSeql).map(e -> skllImageRepository
                .DeleteAnswerSkllImageCode(e)).collect(Collectors.toList());
        List<Integer> result5 =  Arrays.stream(reportSeql).map(e -> skllItmRepository
                .DeleteAnswerSkllItmCode(e)).collect(Collectors.toList());
        List<Integer> result6 =  Arrays.stream(reportSeql).map(e -> skllListRepository
                .DeleteAnswerSkllListCode(e)).collect(Collectors.toList());
        List<Integer> result7 =  Arrays.stream(reportSeql).map(e -> skllMngRepository
                .DeleteAnswerSkllMngCode(e)).collect(Collectors.toList());
        List<Integer> result8 =  Arrays.stream(reportSeql).map(e -> skllRplyRepository
                .DeleteAnswerSkllRplyCode(e)).collect(Collectors.toList());
        List<Integer> result9 =  Arrays.stream(reportSeql).map(e -> skllTextRepository
                .DeleteAnswerSkllTextCode(e)).collect(Collectors.toList());
        return result.size();
    }

    @Transactional(readOnly = true)
    public int countBySkllCode(String code) {
        return skllRepository.countBySkllCode(code);
    }

    /**
     * Skill main 등록
     *
     * @param skillVo
     * @return
     */
    @Transactional
    public int InsertSkill(SkillVO skillVo) {

        int procCnt = 0;
        // skillVo 생성
        SkillVO newSkillVO = VOService.setSkillVO(skillVo);
        String skill_code = skillVo.getSkillManageRVO().getSkillCode();

        if (skill_code.length() > 0) {
            if (skill_code.charAt(0) == 'S') {
                newSkillVO.getSkillManageRVO().setSkillcategory("시나리오");
            } else if (skill_code.charAt(0) == 'B') {
                newSkillVO.getSkillManageRVO().setSkillcategory("행정민원");
            } else {
                newSkillVO.getSkillManageRVO().setSkillcategory("기타");
            }
        }

        String blockName = newSkillVO.getSkillManageRVO().getSkillName();
        blockName = blockName.substring(blockName.indexOf(" ") + 1, blockName.length());
        newSkillVO.getSkillManageRVO().setBlockName(blockName);


        Skll skll = new Skll();
        skll.setSkllCode(newSkillVO.getSkillManageRVO().getSkillCode());
        skll.setSkllUrl("");
        skll.setSkllNm(newSkillVO.getSkillManageRVO().getSkillName());
        skll.setRspnsTy(newSkillVO.getSkillManageRVO().getResType());
        skll.setRspnsTy1(newSkillVO.getSkillManageRVO().getResType1());
        skll.setRspnsTy2(newSkillVO.getSkillManageRVO().getResType2());
        skll.setRspnsTy3(newSkillVO.getSkillManageRVO().getResType3());
        skll.setRegistDe(LocalDate.now());
        skll.setUpdtDe(LocalDate.now());
        skll.setSkllCatgory(newSkillVO.getSkillManageRVO().getSkillcategory());
        skll.setSkllLevel(newSkillVO.getSkillManageRVO().getSkillLevel());
        skll.setSkllLevel1(newSkillVO.getSkillManageRVO().getSkillLevel1());
        skll.setSkllLevel2(newSkillVO.getSkillManageRVO().getSkillLevel2());
        skll.setSkllLevel3(newSkillVO.getSkillManageRVO().getSkillLevel3());
        skll.setSkllLevel4(newSkillVO.getSkillManageRVO().getSkillLevel4());
        skll.setProflCrtfcAt(newSkillVO.getSkillManageRVO().getSkillCode());

        Skll skll1 = skllRepository.save(skll);

        if (skll1 != null && skll1.getSkllCode() != null) {
            // 통계 레벨 값 insert
            String[] resType = { newSkillVO.getSkillManageRVO().getResType1(),
                    newSkillVO.getSkillManageRVO().getResType2(), newSkillVO.getSkillManageRVO().getResType3() };

            int cnt = 0;

            for (int i = 0; i < 3; i++) {
                if (!(resType[i] == null || resType[i].equals(""))) {
                    switch (resType[i]) {
                        case Definition.SKILL_TYPE_ST:
                            SkllText skllText = new SkllText();
                            skllText.setSkllCode(newSkillVO.getSimpleTextVO().get(cnt).getSkillCode());
                            skllText.setText(newSkillVO.getSimpleTextVO().get(cnt).getText());
                            skllText.setRspnsOrdr(newSkillVO.getSimpleTextVO().get(cnt).getResSeq());
                            skllTextRepository.save(skllText);
                            break;
                        case Definition.SKILL_TYPE_SI:
                            SkllImage skllImage = new SkllImage();
                            skllImage.setSkllCode(newSkillVO.getSimpleImageVO().get(cnt).getSkillCode());
                            skllImage.setImageUrl(newSkillVO.getSimpleImageVO().get(cnt).getImageUrl());
                            skllImage.setImageDc(newSkillVO.getSimpleImageVO().get(cnt).getAltText());
                            skllImage.setRspnsOrdr(newSkillVO.getSimpleImageVO().get(cnt).getResSeq());
                            skllImageRepository.save(skllImage);
                            break;
                        case Definition.SKILL_TYPE_BC:
                            //mapper.insertSkillBC(newSkillVO.getBasicCardVO().get(cnt));

                            for (int ii=0; ii < newSkillVO.getBasicCardVO().get(cnt).size(); ii++) {
                                SkllCard skllCard = new SkllCard();
                                skllCard.setSkllCode(newSkillVO.getBasicCardVO().get(cnt).get(ii).getSkillCode());
                                skllCard.setSj(newSkillVO.getBasicCardVO().get(cnt).get(ii).getTitle());
                                skllCard.setDc(newSkillVO.getBasicCardVO().get(cnt).get(ii).getDescription());
                                skllCard.setThumb(newSkillVO.getBasicCardVO().get(cnt).get(ii).getThumbnail());
                                skllCard.setProfl(newSkillVO.getBasicCardVO().get(cnt).get(ii).getProfile());
                                skllCard.setSocty(newSkillVO.getBasicCardVO().get(cnt).get(ii).getSocial());
                                skllCard.setBtton(newSkillVO.getBasicCardVO().get(cnt).get(ii).getButton());
                                skllCard.setBcOrdr(newSkillVO.getBasicCardVO().get(cnt).get(ii).getBcSeq());
                                skllCard.setRspnsOrdr(newSkillVO.getBasicCardVO().get(cnt).get(ii).getResSeq());
                                skllCard.setBttonOptnAt(newSkillVO.getBasicCardVO().get(cnt).get(ii).getOptnAt());
                                skllCard.setProflCrtfcAt(newSkillVO.getBasicCardVO().get(cnt).get(ii).getProflCrtfcAt());
                                skllCardRepository.save(skllCard);
                            }

                            break;
                        case Definition.SKILL_TYPE_CC:
                            //mapper.insertSkillCC(newSkillVO.getCommerceCardVO().get(cnt));
                            break;
                        case Definition.SKILL_TYPE_LC:
                            //mapper.insertSkillLC(newSkillVO.getListCardVO().get(cnt));
                            SkllList skllList = new SkllList();
                            skllList.setSkllCode(newSkillVO.getListCardVO().get(cnt).getSkillCode());
                            skllList.setHder(newSkillVO.getListCardVO().get(cnt).getHeader());
                            skllList.setList(newSkillVO.getListCardVO().get(cnt).getItems());
                            skllList.setBtton(newSkillVO.getListCardVO().get(cnt).getButton());
                            skllList.setRspnsOrdr(newSkillVO.getListCardVO().get(cnt).getResSeq());
                            skllList.setBttonOptnAt("N");
                            skllListRepository.save(skllList);
                            break;
                        default:
                            break;

                    }
                    if (i == 0) {
                        if (resType[0].equals(resType[1])) {
                            cnt++;
                        } else {
                            cnt = 0;
                        }
                    } else if (i == 1) {
                        if (resType[1].equals(resType[2]) || resType[0].equals(resType[2])) {
                            cnt++;
                        } else {
                            cnt = 0;
                        }
                    }
                }
            }
        }
        // Quick Replies insert
        if (newSkillVO.getQuickRepliesVO().size() > 0) {
            //mapper.insertQuickReplies(newSkillVO.getQuickRepliesVO());

            for(int i=0; i<newSkillVO.getQuickRepliesVO().size(); i++) {
                SkllRply skllRply = new SkllRply();
                skllRply.setSkllCode(newSkillVO.getQuickRepliesVO().get(i).getSkillCode());
                skllRply.setLbl(newSkillVO.getQuickRepliesVO().get(i).getLabel());
                skllRply.setActn(newSkillVO.getQuickRepliesVO().get(i).getAction());
                skllRply.setQuestMessage(newSkillVO.getQuickRepliesVO().get(i).getMessageText());
                skllRply.setBlckOrginlId(newSkillVO.getQuickRepliesVO().get(i).getBlockId());
                skllRply.setQrOrdr(Integer.valueOf(i+1));
                skllRplyRepository.save(skllRply);

            }

        }
        return procCnt;
    }

    /**
     * Skill main 등록
     *
     * @param skillVo
     * @return
     */
    @Transactional
    public int UpdateSkill(SkillVO skillVo) {

        int procCnt = 0;
        // skillVo 생성
        SkillVO newSkillVO = VOService.setSkillVO(skillVo);
        String skill_code = skillVo.getSkillManageRVO().getSkillCode();

        if (skill_code.length() > 0) {
            if (skill_code.charAt(0) == 'S') {
                newSkillVO.getSkillManageRVO().setSkillcategory("시나리오");
            } else if (skill_code.charAt(0) == 'B') {
                newSkillVO.getSkillManageRVO().setSkillcategory("행정민원");
            } else {
                newSkillVO.getSkillManageRVO().setSkillcategory("기타");
            }
        }

        String blockName = newSkillVO.getSkillManageRVO().getSkillName();
        blockName = blockName.substring(blockName.indexOf(" ") + 1, blockName.length());
        newSkillVO.getSkillManageRVO().setBlockName(blockName);


        Skll skll = skllRepository.getById(newSkillVO.getSkillManageRVO().getSkillCode());
        skll.setSkllUrl("");
        skll.setSkllNm(newSkillVO.getSkillManageRVO().getSkillName());
        skll.setRspnsTy(newSkillVO.getSkillManageRVO().getResType());
        skll.setRspnsTy1(newSkillVO.getSkillManageRVO().getResType1());
        skll.setRspnsTy2(newSkillVO.getSkillManageRVO().getResType2());
        skll.setRspnsTy3(newSkillVO.getSkillManageRVO().getResType3());
        skll.setRegistDe(LocalDate.now());
        skll.setUpdtDe(LocalDate.now());
        skll.setSkllCatgory(newSkillVO.getSkillManageRVO().getSkillcategory());
        skll.setSkllLevel(newSkillVO.getSkillManageRVO().getSkillLevel());
        skll.setSkllLevel1(newSkillVO.getSkillManageRVO().getSkillLevel1());
        skll.setSkllLevel2(newSkillVO.getSkillManageRVO().getSkillLevel2());
        skll.setSkllLevel3(newSkillVO.getSkillManageRVO().getSkillLevel3());
        skll.setSkllLevel4(newSkillVO.getSkillManageRVO().getSkillLevel4());
        skll.setProflCrtfcAt(newSkillVO.getSkillManageRVO().getSkillCode());

        Skll skll1 = skllRepository.save(skll);

        if (skll1 != null && skll1.getSkllCode() != null) {

            skllCardRepository.DeleteAnswerSkllCardCode(skll1.getSkllCode());
            skllCmercRepository.DeleteAnswerSkllCmercCode(skll1.getSkllCode());
            skllImageRepository.DeleteAnswerSkllImageCode(skll1.getSkllCode());
            skllItmRepository.DeleteAnswerSkllItmCode(skll1.getSkllCode());
            skllListRepository.DeleteAnswerSkllListCode(skll1.getSkllCode());
            skllMngRepository.DeleteAnswerSkllMngCode(skll1.getSkllCode());
            skllRplyRepository.DeleteAnswerSkllRplyCode(skll1.getSkllCode());
            skllTextRepository.DeleteAnswerSkllTextCode(skll1.getSkllCode());

            // 통계 레벨 값 insert
            String[] resType = { newSkillVO.getSkillManageRVO().getResType1(),
                    newSkillVO.getSkillManageRVO().getResType2(), newSkillVO.getSkillManageRVO().getResType3() };

            int cnt = 0;

            for (int i = 0; i < 3; i++) {
                if (!(resType[i] == null || resType[i].equals(""))) {
                    switch (resType[i]) {
                        case Definition.SKILL_TYPE_ST:
                            SkllText skllText = new SkllText();
                            skllText.setSkllCode(newSkillVO.getSimpleTextVO().get(cnt).getSkillCode());
                            skllText.setText(newSkillVO.getSimpleTextVO().get(cnt).getText());
                            skllText.setRspnsOrdr(newSkillVO.getSimpleTextVO().get(cnt).getResSeq());
                            skllTextRepository.save(skllText);
                            break;
                        case Definition.SKILL_TYPE_SI:
                            SkllImage skllImage = new SkllImage();
                            skllImage.setSkllCode(newSkillVO.getSimpleImageVO().get(cnt).getSkillCode());
                            skllImage.setImageUrl(newSkillVO.getSimpleImageVO().get(cnt).getImageUrl());
                            skllImage.setImageDc(newSkillVO.getSimpleImageVO().get(cnt).getAltText());
                            skllImage.setRspnsOrdr(newSkillVO.getSimpleImageVO().get(cnt).getResSeq());
                            skllImageRepository.save(skllImage);
                            break;
                        case Definition.SKILL_TYPE_BC:
                            //mapper.insertSkillBC(newSkillVO.getBasicCardVO().get(cnt));


                            System.out.println(">>>>>>>>>>>>>>>>>>>>>> size() : " + newSkillVO.getBasicCardVO().get(cnt).size());
                            for (int ii=0; ii < newSkillVO.getBasicCardVO().get(cnt).size(); ii++) {
                                System.out.println("ii" + ii);
                                System.out.println("cnt" + cnt);
                                System.out.println(">>>>>>>>>>>>>>>>>>>>>>  : " + newSkillVO.getBasicCardVO().get(cnt).get(ii).getTitle());

//                                SkllCard skllCard = SkllCard.builder()
//                                        .skllCode(newSkillVO.getBasicCardVO().get(cnt).get(ii).getSkillCode())
//                                        .sj(newSkillVO.getBasicCardVO().get(cnt).get(ii).getTitle())
//                                        .dc(newSkillVO.getBasicCardVO().get(cnt).get(ii).getDescription())
//                                        .thumb(newSkillVO.getBasicCardVO().get(cnt).get(ii).getThumbnail())
//                                        .profl(newSkillVO.getBasicCardVO().get(cnt).get(ii).getProfile())
//                                        .socty(newSkillVO.getBasicCardVO().get(cnt).get(ii).getSocial())
//                                        .btton(newSkillVO.getBasicCardVO().get(cnt).get(ii).getButton())
//                                        .bcOrdr(newSkillVO.getBasicCardVO().get(cnt).get(ii).getBcSeq())
//                                        .rspnsOrdr(newSkillVO.getBasicCardVO().get(cnt).get(ii).getResSeq())
//                                        .bttonOptnAt(newSkillVO.getBasicCardVO().get(cnt).get(ii).getOptnAt())
//                                        .proflCrtfcAt(newSkillVO.getBasicCardVO().get(cnt).get(ii).getProflCrtfcAt())
//                                        .build();

                                skllCardRepository.SaveAnswerSkllCardCode(newSkillVO.getBasicCardVO().get(cnt).get(ii).getSkillCode(),
                                        newSkillVO.getBasicCardVO().get(cnt).get(ii).getTitle(),
                                        newSkillVO.getBasicCardVO().get(cnt).get(ii).getDescription(),
                                        newSkillVO.getBasicCardVO().get(cnt).get(ii).getThumbnail(),
                                        newSkillVO.getBasicCardVO().get(cnt).get(ii).getProfile(),
                                        newSkillVO.getBasicCardVO().get(cnt).get(ii).getSocial(),
                                        newSkillVO.getBasicCardVO().get(cnt).get(ii).getButton(),
                                        newSkillVO.getBasicCardVO().get(cnt).get(ii).getBcSeq(),
                                        newSkillVO.getBasicCardVO().get(cnt).get(ii).getResSeq(),
                                        newSkillVO.getBasicCardVO().get(cnt).get(ii).getOptnAt(),
                                        newSkillVO.getBasicCardVO().get(cnt).get(ii).getProflCrtfcAt()
                                );

                            }

                            break;
                        case Definition.SKILL_TYPE_CC:
                            //mapper.insertSkillCC(newSkillVO.getCommerceCardVO().get(cnt));
                            break;
                        case Definition.SKILL_TYPE_LC:
                            //mapper.insertSkillLC(newSkillVO.getListCardVO().get(cnt));
                            SkllList skllList = new SkllList();
                            skllList.setSkllCode(newSkillVO.getListCardVO().get(cnt).getSkillCode());
                            skllList.setHder(newSkillVO.getListCardVO().get(cnt).getHeader());
                            skllList.setList(newSkillVO.getListCardVO().get(cnt).getItems());
                            skllList.setBtton(newSkillVO.getListCardVO().get(cnt).getButton());
                            skllList.setRspnsOrdr(newSkillVO.getListCardVO().get(cnt).getResSeq());
                            skllList.setBttonOptnAt("N");
                            skllListRepository.save(skllList);
                            break;
                        default:
                            break;

                    }
                    if (i == 0) {
                        if (resType[0].equals(resType[1])) {
                            cnt++;
                        } else {
                            cnt = 0;
                        }
                    } else if (i == 1) {
                        if (resType[1].equals(resType[2]) || resType[0].equals(resType[2])) {
                            cnt++;
                        } else {
                            cnt = 0;
                        }
                    }
                }
            }
        }
        // Quick Replies insert
        if (newSkillVO.getQuickRepliesVO().size() > 0) {
            //mapper.insertQuickReplies(newSkillVO.getQuickRepliesVO());

            for(int i=0; i<newSkillVO.getQuickRepliesVO().size(); i++) {
                int ii = i+1;
                SkllRply skllRply = new SkllRply();
                skllRply.setSkllCode(newSkillVO.getQuickRepliesVO().get(i).getSkillCode());
                skllRply.setLbl(newSkillVO.getQuickRepliesVO().get(i).getLabel());
                skllRply.setActn(newSkillVO.getQuickRepliesVO().get(i).getAction());
                skllRply.setQuestMessage(newSkillVO.getQuickRepliesVO().get(i).getMessageText());
                skllRply.setBlckOrginlId(newSkillVO.getQuickRepliesVO().get(i).getBlockId());
                skllRply.setQrOrdr(ii);
                skllRplyRepository.save(skllRply);

            }

        }
        return procCnt;
    }
}
