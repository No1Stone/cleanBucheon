package com.bucheon.clean.town.db.entity.environmental;

import com.bucheon.clean.town.db.entity.environmental.id.ReceiptId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "receipt")
@Getter
@Setter
@NoArgsConstructor
//@IdClass(ReceiptId.class)
public class Receipt {
    /**
     * 부천시 민원 접수 데이터
     */
    @Id
    @Column(name = "peti_no", nullable = true)
    private String petiNo;
//    @Id
    @Column(name = "civil_no", nullable = true)
    private String civilNo;
    @Column(name = "reg_date_time", nullable = true)
    private String regDateTime;
    @Column(name = "anc_id", nullable = true)
    private String ancId;
    @Column(name = "anc_name", nullable = true)
    private String ancName;
    @Column(name = "anc_reg_date_time", nullable = true)
    private String ancRegDateTime;
    @Column(name = "busi_bran_code", nullable = true)
    private String busiBranCode;
    @Column(name = "busi_bran_code_name", nullable = true)
    private String busiBranCodeName;


    @Builder
    Receipt(
            String petiNo,
            String civilNo,
            String regDateTime,
            String ancId,
            String ancName,
            String ancRegDateTime,
            String busiBranCode,
            String busiBranCodeName
    ) {
        this.petiNo = petiNo;
        this.civilNo = civilNo;
        this.regDateTime = regDateTime;
        this.ancId = ancId;
        this.ancName = ancName;
        this.ancRegDateTime = ancRegDateTime;
        this.busiBranCode = busiBranCode;
        this.busiBranCodeName = busiBranCodeName;
    }




}
