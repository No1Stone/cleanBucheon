package com.bucheon.clean.town.biz.login.login.v1.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "")
@RequiredArgsConstructor
public class Login2Controller {

    private final Logger logger = LoggerFactory.getLogger(Login2Controller.class);

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index2(){
        return new ModelAndView("redirect:/login/info");
    }

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public ModelAndView index3(){
        return new ModelAndView("redirect:/login/info");
    }
}
