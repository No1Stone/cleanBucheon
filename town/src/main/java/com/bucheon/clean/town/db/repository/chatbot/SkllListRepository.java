package com.bucheon.clean.town.db.repository.chatbot;

import com.bucheon.clean.town.db.entity.chatbot.SkllList;
import com.bucheon.clean.town.db.entity.chatbot.id.SkllListId;
import com.bucheon.clean.town.db.repository.chatbot.dsl.SkllListRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface SkllListRepository extends JpaRepository<SkllList, SkllListId>, SkllListRepositoryDsl {


    @Transactional
    @Modifying
    @Query(
            value = "delete from tb_skll_list where SKLL_CODE =:answerSeq "
            , nativeQuery = true
    )
    int DeleteAnswerSkllListCode(@Param("answerSeq") String answerSeq);
}
