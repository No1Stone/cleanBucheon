package com.bucheon.clean.town.biz.dashboard.v1.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@RequiredArgsConstructor
public class DashboardService {

    private final Logger logger = LoggerFactory.getLogger(DashboardService.class);
    private final HttpServletRequest httpServletRequest;


}
