package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import java.util.List;

import lombok.Data;

@Data
public class CommerceCardVO {
	
	/* 스킬 코드 */
	private String skillCode;
	
	private String description;
	
	private int price;
	
	private int discount;
	
	private String discountAction;
	
	private int discountRate;
	
	private int discountedPrice;
	
	private static final String currency = "won";
	
	private String thumbnail;
	
	private String profile;
	
	private String button;
	
	private List<CommerceCardThumbnails> thumbnailVO;
	
	private CommerceCardProfile profileVO;
	
	private List<CommerceCardButtons> buttonVO;
	
	/* response sequence */
	private int resSeq;
	
	/* Carousel sequence */
	private int ccSeq;
	
	private String level2;
}
