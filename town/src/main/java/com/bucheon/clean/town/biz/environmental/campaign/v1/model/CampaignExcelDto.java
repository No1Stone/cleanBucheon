package com.bucheon.clean.town.biz.environmental.campaign.v1.model;

import com.bucheon.clean.town.common.type.ExcelColumnName;
import com.bucheon.clean.town.common.type.ExcelDto;
import com.bucheon.clean.town.common.type.ExcelFileName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@ExcelFileName
public class CampaignExcelDto implements ExcelDto {
    @ExcelColumnName(headerName = "접수번호")
    @JsonProperty("접수번호")
    private String campaignNum;

    @ExcelColumnName(headerName = "쓰레기 유형")
    @JsonProperty("쓰레기 유형")
    private String trashName;

    @ExcelColumnName(headerName = "상태")
    @JsonProperty("상태")
    private String campaignStatusName;

    @ExcelColumnName(headerName = "행정동")
    @JsonProperty("행정동")
    private String areaName;

    @ExcelColumnName(headerName = "신주소")
    @JsonProperty("신주소")
    private String addrNew;

    @ExcelColumnName(headerName = "구주소")
    @JsonProperty("구주소")
    private String addrOld;

    @ExcelColumnName(headerName = "발견자 이름")
    @JsonProperty("발견자 이름")
    private String tRegUserName;

    @ExcelColumnName(headerName = "발견자 연락처")
    @JsonProperty("발견자 연락처")
    private String tRegUserPhone;

    @ExcelColumnName(headerName = "발견자 Email")
    @JsonProperty("발견자 Email")
    private String tRegUserEmail;

    @ExcelColumnName(headerName = "쓰레기발견 처리상태")
    @JsonProperty("쓰레기발견 처리상태")
    private String tCampaginProcessName;

    @ExcelColumnName(headerName = "쓰레기발견 등록일자")
    @JsonProperty("쓰레기발견 등록일자")
    private LocalDateTime tRegDt;

    @ExcelColumnName(headerName = "쓰레기발견 처리일자")
    @JsonProperty("쓰레기발견 처리일자")
    private LocalDateTime tComDt;

    @ExcelColumnName(headerName = "쓰레기발견 마일리지")
    @JsonProperty("쓰레기발견 마일리지")
    private int tMileage;

    @ExcelColumnName(headerName = "쓰레기발견 마일리지 지급 상태")
    @JsonProperty("쓰레기발견 마일리지 지급 상태")
    private String tMileageStatusName;

    @ExcelColumnName(headerName = "쓰레기발견 마일리지 지급 불가 사유")
    @JsonProperty("쓰레기발견 마일리지 지급 불가 사유")
    private String tMileageRejectReason;

    @ExcelColumnName(headerName = "청소참여자 이름")
    @JsonProperty("청소참여자 이름")
    private String cRegUserName;

    @ExcelColumnName(headerName = "청소참여자 연락처")
    @JsonProperty("청소참여자 연락처")
    private String cRegUserPhone;

    @ExcelColumnName(headerName = "청소참여자 Email")
    @JsonProperty("청소참여자 Email")
    private String cRegUserEmail;

    @ExcelColumnName(headerName = "청소참여 처리상태")
    @JsonProperty("청소참여 처리상태")
    private String cCampaginProcessName;

    @ExcelColumnName(headerName = "청소참여 등록일자")
    @JsonProperty("청소참여 등록일자")
    private LocalDateTime cRegDt;

    @ExcelColumnName(headerName = "청소참여 처리일자")
    @JsonProperty("청소참여 처리일자")
    private LocalDateTime cComDt;

    @ExcelColumnName(headerName = "청소참여 마일리지")
    @JsonProperty("청소참여 마일리지")
    private int cMileage;

    @ExcelColumnName(headerName = "청소참여 마일리지 지급 상태")
    @JsonProperty("청소참여 마일리지 지급 상태")
    private String cMileageStatusName;

    @ExcelColumnName(headerName = "청소참여 마일리지 지급 불가 사유")
    @JsonProperty("청소참여 마일리지 지급 불가 사유")
    private String cMileageRejectReason;
    

    @Override
    public List<String> mapToList() {

        String tRegDtString = "";
        String tComDtString = "";
        String cRegDtString = "";
        String cComDtString = "";

        try {
            tRegDtString = tRegDt.format(DateTimeFormatter.ofPattern("YYYY-MM-dd hh:mm:ss"));
        } catch (Exception e) {
            tRegDtString = "";
        }

        try {
            tComDtString = tComDt.format(DateTimeFormatter.ofPattern("YYYY-MM-dd hh:mm:ss"));
        } catch (Exception e) {
            tComDtString = "";
        }

        try {
            cRegDtString = cRegDt.format(DateTimeFormatter.ofPattern("YYYY-MM-dd hh:mm:ss"));
        } catch (Exception e) {
            cRegDtString = "";
        }

        try {
            cComDtString = cComDt.format(DateTimeFormatter.ofPattern("YYYY-MM-dd hh:mm:ss"));
        } catch (Exception e) {
            cComDtString = "";
        }


        return Arrays.asList(campaignNum, trashName, campaignStatusName,
                areaName, addrNew, addrOld,
                tRegUserName, tRegUserPhone, tRegUserEmail,
                tCampaginProcessName,  tRegDtString, tComDtString,
                String.valueOf(tMileage), tMileageStatusName, tMileageRejectReason,
                cRegUserName, cRegUserPhone, cRegUserEmail,
                cCampaginProcessName,  cRegDtString, cComDtString,
                String.valueOf(cMileage), cMileageStatusName, cMileageRejectReason
        );
    }

    @Override
    public List<Integer> mapToCoulmn() {

        return Arrays.asList(4000, 4000, 4000,
                4000, 12000, 12000,
                4000, 4000, 4000,
                4000, 6000, 6000,
                4000, 4000, 10000,
                4000, 4000, 4000,
                4000, 6000, 6000,
                4000, 4000, 10000);
    }

}
