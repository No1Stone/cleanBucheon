package com.bucheon.clean.town.biz.operation.mileage.v1.controller;

import com.bucheon.clean.town.biz.operation.mileage.v1.model.MileageRequestVO;
import com.bucheon.clean.town.biz.operation.mileage.v1.model.UserMileageHistoryVO;
import com.bucheon.clean.town.biz.operation.mileage.v1.model.UserMileageVO;
import com.bucheon.clean.town.biz.operation.mileage.v1.service.UserMileageService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/operation/userMileage/rest")
@RequiredArgsConstructor
public class UserMileageRestController {

    private final Logger logger = LoggerFactory.getLogger(UserMileageRestController.class);
    private final UserMileageService userMileageService;

    @PostMapping(path = "/test1")
    public Object UserMileageSelectListController(@RequestBody UserMileageVO dto) {

        return userMileageService.UserMileageSelectOneService(dto.getUserId());
    }


    @GetMapping(path = "/test2/{userId}")
    public Object UserMileageSelectListController(@PathVariable String userId) {

        if(userMileageService.UserMileageSelectCountService(userId) > 0) {
            return userMileageService.UserMileageSelectOneService(userId);
        } else {
            return new UserMileageVO();
        }

    }

    @PostMapping(path = "/test3")
    @ResponseBody
    public Object UserMileageSave(@RequestBody UserMileageHistoryVO dto) {

        return userMileageService.UserMileageSaveService(dto);

    }

    @PostMapping(path = "/test4")
    @ResponseBody
    public Object UserMileageApiCall(@RequestBody MileageRequestVO mileageRequestVO) {

        return userMileageService.UserMileageApiCallSelectService(mileageRequestVO);

    }

    @PostMapping(path = "/test5")
    @ResponseBody
    public Object UserMileageApiCall2(@RequestBody MileageRequestVO mileageRequestVO) {

        return userMileageService.UserMileageApiCallSaveService(mileageRequestVO);

    }
}
