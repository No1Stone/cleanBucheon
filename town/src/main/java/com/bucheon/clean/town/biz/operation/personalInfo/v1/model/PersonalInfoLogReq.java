package com.bucheon.clean.town.biz.operation.personalInfo.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonalInfoLogReq {

    private String searchSelect;
    private String searchString;
    private int page;
    private int size;
    private Long seq;
    private String did;

}
