package com.bucheon.clean.town.biz.login.login.v1.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "/login")
@RequiredArgsConstructor
public class LoginController {

    private final Logger logger = LoggerFactory.getLogger(LoginController.class);

//    @RequestMapping(path = "/info", method = RequestMethod.GET)
//    public ModelAndView a(){
//        return new ModelAndView("login/loginForm");
//    }


    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public String a(
            Model model,
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "exception", required = false) String exception){

        model.addAttribute("error", error);
        model.addAttribute("exception", exception);

        return "login/loginForm";
    }


    @RequestMapping(path = "/info-pop", method = RequestMethod.GET)
    public ModelAndView b(){
        return new ModelAndView();
    }

    @RequestMapping(path = "/info-pop", method = RequestMethod.POST)
    public ModelAndView c(){
        return new ModelAndView();
    }

    @RequestMapping(path = "/update", method = RequestMethod.POST)
    public ModelAndView d(){
        return new ModelAndView();
    }

    @RequestMapping(path = "/delete", method = RequestMethod.POST)
    public ModelAndView e(){
        return new ModelAndView();
    }

    @RequestMapping(path = "/index", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("index");
    }

}
