package com.bucheon.clean.town.biz.operation.board.v1.model;

import com.bucheon.clean.town.db.entity.operation.GlobalBox;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GlobalBoxVO {

    private Long boxSeq;
    private String boardTitle;
    private Long pathSeq;
    private String useYn;
    private String delYn;
    private Long regSeq;
    private LocalDateTime regDt;
    private Long modSeq;
    private LocalDateTime modDt;



    public GlobalBox ofBox(){
        GlobalBox m = new GlobalBox();

        if (this.boxSeq!=null) {
            m.setBoxSeq(this.boxSeq);
        }

        m.setBoardTitle(this.boardTitle);
        m.setPathSeq(this.pathSeq);
        m.setUseYn(this.useYn);
        m.setDelYn(this.delYn);
        m.setRegSeq(this.regSeq);
        m.setRegDt(LocalDateTime.now());
        m.setModSeq(this.regSeq);
        m.setModDt(LocalDateTime.now());
        return m;
    }

}
