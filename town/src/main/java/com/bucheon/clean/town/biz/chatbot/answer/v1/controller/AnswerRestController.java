package com.bucheon.clean.town.biz.chatbot.answer.v1.controller;


import com.bucheon.clean.town.biz.chatbot.answer.v1.service.AnswerService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/answer/rest")
public class AnswerRestController {

    private final Logger logger = LoggerFactory.getLogger(AnswerRestController.class);
    private final AnswerService answerService;

    @GetMapping(path = "/test1")
    public Object test1(){
       return answerService.SkllItmSelectList();
    }

    @GetMapping(path = "/test2")
    public Object test2(){
        return answerService.SkllCardSelectList();
    }

    @GetMapping(path = "/test3")
    public Object test3(){
        return answerService.SkllImageSelectList();
    }

    @GetMapping(path = "/test4")
    public Object test4(){
        return answerService.SkllListSelectList();
    }
}
