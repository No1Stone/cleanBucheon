package com.bucheon.clean.town.biz.operation.user.v1.controller;

import com.bucheon.clean.town.biz.environmental.report.v1.model.PersonalSearchReq;
import com.bucheon.clean.town.biz.environmental.report.v1.model.ReportListRes;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.mileage.v1.model.UserMileageHistoryVO;
import com.bucheon.clean.town.biz.operation.mileage.v1.model.UserMileageVO;
import com.bucheon.clean.town.biz.operation.mileage.v1.service.UserMileageHistoryService;
import com.bucheon.clean.town.biz.operation.mileage.v1.service.UserMileageService;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.clean.town.biz.operation.personalInfo.v1.service.PersonalInfoLogService;
import com.bucheon.clean.town.biz.operation.user.v1.model.UserReq;
import com.bucheon.clean.town.biz.operation.user.v1.service.UserService;
import com.bucheon.clean.town.common.type.ReceptionType;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.User;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(path = "/operation/user")
@RequiredArgsConstructor
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;
    private final UserMileageService userMileageService;
    private final UserMileageHistoryService userMileageHistoryService;
    private final PermissionService permissionService;
    private final PersonalInfoLogService personalInfoLogService;
    private final MenuService menuService;

    //주소 리다이렉트 처리 
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/operation/user/userManage");
    }

    //기본틀 
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(UserReq dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(11L);

        return new ModelAndView("operation/user/userManage")
                .addObject("userPaging", userService.UserSelectListPagingService(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(UserReq dto){
        return new ModelAndView("operation/user/userListAjax")
                .addObject("userList", userService.UserSelectListService(dto))
                .addObject("userPaging", userService.UserSelectListPagingService(dto))
                ;
    }

    //상세보기 페이지 팝업
    @RequestMapping(path = "/viewForm", method = RequestMethod.POST)
    public ModelAndView viewForm(String did) throws Exception {

        UserMileageVO um = new UserMileageVO();
        List<UserMileageHistoryVO> umhList = null;

        try {
            um = userMileageService.UserMileageSelectOneService(did);
        } catch (Exception e) {
            um.setUserId(did);
            um.setMileageSum(0);
        }

        try {
            umhList = userMileageHistoryService.UserMileageHistoryListRecentSelectService(did);
        } catch (Exception e) {
            //
        }

        return new ModelAndView("operation/user/userViewPopupAjax")
                .addObject("um", um)
                .addObject("umhList", umhList)
                .addObject("userOne", userService.UserSelectOneService(did))
                ;
    }

    //상세보기 팝업
    @RequestMapping(path = "/viewPersonalInfo", method = RequestMethod.POST)
    public ModelAndView viewPersonalInfo(String did) throws Exception {

        //로그 저장장
        PersonalSearchReq dto = new PersonalSearchReq();

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;

        String menu = "UserInfo";
        String menuhan = "깨끗한 마을 회원정보";
        String reason = admin.getAdminName() +  " : 깨끗한 마을 회원정보 확인";

        dto.setLogTime(LocalDateTime.now());
        dto.setAdminSeq(admin.getAdminSeq());
        dto.setMenuName(menu);
        dto.setMenuHanName(menuhan);
        dto.setDid(did);
        dto.setReason(reason);
        personalInfoLogService.PersonalSearchSaveService(dto);

        User user = userService.UserSelectOneService3(did);

        return new ModelAndView("operation/personalInfo/personalInfoPopViewAjax")
                .addObject("userName", user.getName())
                .addObject("userPhone", user.getPhone())
                .addObject("userEmail", user.getEmail())
                ;
    }

    //수정처리
    @RequestMapping(path = "/addMilege", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(UserReq dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        //추가 마일리지 적용 업데이트
        if(dto.getDid() != null && !dto.getDid().equals("")
                && String.valueOf(dto.getAddPoint()) != null && !String.valueOf(dto.getAddPoint()).equals("")
                && dto.getReasonPoint() != null && !dto.getReasonPoint().equals("")) {

            UserMileageHistoryVO userMileageHistoryVO = new UserMileageHistoryVO();
            userMileageHistoryVO.setUserId(dto.getDid());
            userMileageHistoryVO.setReportSeq(0L);
            userMileageHistoryVO.setMileage(dto.getAddPoint());
            userMileageHistoryVO.setMileageType("admin");
            userMileageHistoryVO.setReason(dto.getReasonPoint());
            userMileageHistoryVO.setComplaintsDt(LocalDateTime.now());
            userMileageHistoryVO.setSendDate(LocalDateTime.now());

            userMileageService.UserMileageSaveService(userMileageHistoryVO);
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

}
