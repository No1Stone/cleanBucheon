package com.bucheon.clean.town.db.repository.metabus;

import com.bucheon.clean.town.db.entity.metabus.MetaPushLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface MetaPushLogRepository extends JpaRepository<MetaPushLog, String> {

    @Transactional
    int countBySendSeqAndUserId(Long seq, String id);
}
