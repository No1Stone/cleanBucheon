package com.bucheon.clean.town.db.repository.operation.dsl;

import com.bucheon.clean.town.biz.operation.mileage.v1.model.UserMileageHistoryVO;
import com.bucheon.clean.town.db.entity.environmental.QCampaign;
import com.bucheon.clean.town.db.entity.environmental.QReport;
import com.bucheon.clean.town.db.entity.operation.QAdmin;
import com.bucheon.clean.town.db.entity.operation.QUser;
import com.bucheon.clean.town.db.entity.operation.QUserMileageHistory;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class UserMileageHistoryRepositoryDslImpl implements UserMileageHistoryRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QUserMileageHistory qUserMileageHistory = QUserMileageHistory.userMileageHistory;
    private QUser qUser = QUser.user;
    private QAdmin qAdmin = QAdmin.admin;
    private QReport qReport = QReport.report;
    private QCampaign qCampaign = QCampaign.campaign;

    @Override
    public List<UserMileageHistoryVO> UserMileageHistorySelectList(UserMileageHistoryVO dto) {


        if(dto.getSize() == 0){
            dto.setSize(10);
        }

        List<UserMileageHistoryVO> result = queryFactory.select(
                        Projections.fields(UserMileageHistoryVO.class,
                                qUserMileageHistory.mileageSeq,
                                qUserMileageHistory.userId,
                                ExpressionUtils.as(
                                        JPAExpressions.select(qUser.name)
                                                .from(qUser)
                                                .where(qUser.did.eq(qUserMileageHistory.userId)),
                                        "userName"),
                                qUserMileageHistory.reportSeq,
                                new CaseBuilder()
                                        .when(qUserMileageHistory.reportSeq.ne(0L).and(qUserMileageHistory.mileageType.eq("chatbot")))
                                            .then(  JPAExpressions.select(qReport.reportContent)
                                                            .from(qReport)
                                                            .where(qReport.reportSeq.eq(qUserMileageHistory.reportSeq)))
                                        .when(qUserMileageHistory.reportSeq.ne(0L).and(qUserMileageHistory.mileageType.eq("campaign")))
                                            .then(  JPAExpressions.select(
                                                        new CaseBuilder().when(qCampaign.campaignStatus.eq("T"))
                                                                .then("쓰레기등록")
                                                                .otherwise("청소완료")
                                                    )
                                                .from(qCampaign)
                                                .where(qCampaign.campaignSeq.eq(qUserMileageHistory.reportSeq)))
                                        .otherwise("직접지급").as("reportContent"),
                                qUserMileageHistory.mileage,
                                qUserMileageHistory.mileageType,
                                qUserMileageHistory.reason,
                                qUserMileageHistory.complaintsDt,
                                qUserMileageHistory.adminSeq,
                                ExpressionUtils.as(
                                        JPAExpressions.select(qAdmin.adminName)
                                                .from(qAdmin)
                                                .where(qAdmin.adminSeq.eq(qUserMileageHistory.adminSeq)),
                                        "adminName"),
                                qUserMileageHistory.sendDate
                                )
                )
                .from(qUserMileageHistory)
                .leftJoin(qUser).on(qUser.did.eq(qUserMileageHistory.userId))
                .where(
                        eqUserMileageHistoryUserId(dto.getUserId()),
                        eqSearchSelect(dto.getSearchSelect(), dto.getSearchString(), dto.getSearchSdate(), dto.getSearchEdate())


                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qUserMileageHistory.mileageSeq.desc())
                .fetch();

        return result;
    }

    @Override
    public QueryResults<UserMileageHistoryVO> UserMileageHistorySelectListPaging(UserMileageHistoryVO dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        QueryResults<UserMileageHistoryVO> result = queryFactory.select(
                        Projections.bean(UserMileageHistoryVO.class,
                                qUserMileageHistory.mileageSeq,
                                qUserMileageHistory.userId,
                                qUserMileageHistory.reportSeq,
                                qUserMileageHistory.mileage,
                                qUserMileageHistory.mileageType,
                                qUserMileageHistory.complaintsDt,
                                qUserMileageHistory.adminSeq,
                                qUserMileageHistory.sendDate
                        )
                )
                .from(qUserMileageHistory)
                .leftJoin(qUser).on(qUser.did.eq(qUserMileageHistory.userId))
                .where(
                        eqUserMileageHistoryUserId(dto.getUserId()),
                        eqSearchSelect(dto.getSearchSelect(), dto.getSearchString(), dto.getSearchSdate(), dto.getSearchEdate())
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qUserMileageHistory.mileageSeq.desc())
                .fetchResults();
        return result;
    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchSelect(String searchSelect, String searchString, String searchSdate, String searchEdate) {

        try {

            if (searchSelect.equals("searchName")) {
                return eqRegName(searchString);
            } else if (searchSelect.equals("searchReg")) {
                return eqRegDate(searchSdate, searchEdate);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqUserMileageHistorySeq(Long mileageSeq) {
        if (mileageSeq == null) {
            return null;
        }
        return qUserMileageHistory.mileageSeq.eq(mileageSeq);
    }

    private BooleanExpression eqUserMileageHistoryUserId(String userId) {
        if (StringUtils.isEmptyOrWhitespace(userId)) {
            return null;
        }
        return qUserMileageHistory.userId.eq(userId);
    }

    private BooleanExpression eqRegName(String st) {
        if (StringUtils.isEmptyOrWhitespace(st)) {
            return null;
        }
        return qUser.name.contains(st);
    }

    private BooleanExpression eqRegDate(String sdate, String edate) {
        if (StringUtils.isEmptyOrWhitespace(sdate)) {
            return null;
        }
        if (StringUtils.isEmptyOrWhitespace(edate)) {
            return null;
        }

        return Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",qUserMileageHistory.sendDate)
                .between(
                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",sdate),
                        Expressions.dateTimeTemplate(LocalDateTime.class,"DATE_FORMAT({0},'%Y-%m-%d')",edate)
                ) ;
    }

}
