package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import lombok.Data;

@Data
public class QuickRepliesVO {
	
	private String skillCode;

	private String label;
	
	private String action;
	
	private String link;
	
	private String messageText;
	
	private String blockId;
	
	private int qrSeq;
	
	private String subSystem;
	
}
