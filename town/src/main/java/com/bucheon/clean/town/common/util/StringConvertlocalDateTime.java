package com.bucheon.clean.town.common.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class StringConvertlocalDateTime {

    public LocalDateTime stringToDateDash(String date){
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(date, format).atStartOfDay();
    }

}
