package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 스킬관리  > Simple Image VO
 * @author user
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SimpleImageVO {

	/* 스킬 코드 */
	private String skillCode;
	
	/* 이미지 Url */
	private String imageUrl;

	/* 이미지 Text */
	private String altText;	
	
	/* 응답 순서 */
	private int resSeq;
	
	/* 공공예약 구분하는 값 */
	private String subSystem;
}
