package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class BasicCardSocial {
	
	private String like;
	
	private String comment;
	
	private String share;

}
