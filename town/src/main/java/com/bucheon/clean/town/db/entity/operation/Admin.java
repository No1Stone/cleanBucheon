package com.bucheon.clean.town.db.entity.operation;

import com.bucheon.clean.town.db.entity.operation.id.AdminId;
import com.bucheon.clean.town.db.entity.operation.id.MileageHistoryId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "tb_admin")
@Getter
@Setter
@NoArgsConstructor
@IdClass(AdminId.class)
public class Admin implements UserDetails {

    @Id
    @Column(name = "admin_seq", nullable = true)
    private Long adminSeq;
    @Column(name = "admin_id", nullable = true)
    private String adminId;
    @Column(name = "admin_name", nullable = true)
    private String adminName;
    @Column(name = "admin_password", nullable = true)
    private String adminPassword;
    @Column(name = "admin_phone", nullable = true)
    private String adminPhone;
    @Column(name = "admin_email", nullable = true)
    private String adminEmail;
    @Column(name = "administrative_division", nullable = true)
    private String administrativeDivision;
    @Id
    @Column(name = "permission_seq", nullable = true)
    private Long permissionSeq;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "del_yn", nullable = true)
    private String delYn;
    @Column(name = "reg_seq", nullable = true)
    private Long regSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_seq", nullable = true)
    private Long modSeq;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "super_yn")
    private String superYn;


    @Builder
    Admin(
            Long adminSeq,
            String adminId,
            String adminName,
            String adminPassword,
            String adminPhone,
            String adminEmail,
            String administrativeDivision,
            Long permissionSeq,
            String useYn,
            Long regSeq,
            LocalDateTime regDt,
            Long modSeq,
            LocalDateTime modDt,
            String superYn
            ) {
        this.adminSeq = adminSeq;
        this.adminId = adminId;
        this.adminName = adminName;
        this.adminPassword = adminPassword;
        this.adminPhone = adminPhone;
        this.adminEmail = adminEmail;
        this.administrativeDivision = administrativeDivision;
        this.permissionSeq = permissionSeq;
        this.useYn = useYn;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.modSeq = modSeq;
        this.modDt = modDt;
        this.superYn = superYn;
    }

    //UserDetails
    @Override
    public String getPassword() {
        return this.adminPassword;
    }

    @Override
    public String getUsername() {
        return this.adminId;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities(){

        Collection<GrantedAuthority> collectors = new ArrayList<>();
        collectors.add(() -> {
            return "";
        });

        return collectors;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


}
