package com.bucheon.clean.town.biz.operation.permission.v1.service;

import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.clean.town.biz.operation.permission.v1.model.PermissionReq;
import com.bucheon.clean.town.biz.operation.permission.v1.model.PermissionRes;
import com.bucheon.clean.town.biz.operation.permission.v1.model.PermissionUpdate;
import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.Permission;
import com.bucheon.clean.town.db.entity.operation.PermissionMenu;
import com.bucheon.clean.town.db.repository.operation.AdminRepository;
import com.bucheon.clean.town.db.repository.operation.MenuRepository;
import com.bucheon.clean.town.db.repository.operation.PermissionMenuRepository;
import com.bucheon.clean.town.db.repository.operation.PermissionRepository;
import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PermissionService {

    private final Logger logger = LoggerFactory.getLogger(PermissionService.class);
    private final HttpServletRequest httpServletRequest;
    private final PermissionRepository permissionRepository;
    private final PermissionMenuRepository permissionMenuRepository;
    private final MenuRepository menuRepository;
    private final AdminRepository adminRepository;
    private final ModelMapper modelMapper;

    public Object PermissionListSelectService(PermissionReq dto) {
        Map<Long, String> adminList = adminRepository.findAll()
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));

        logger.info("dto = {}", new Gson().toJson(dto));
        var result = permissionRepository.PermissionSelectList(dto)
                .stream().map(e -> modelMapper.map(e, PermissionRes.class))
                .peek(f -> {
                    f.setModSeqName(adminList.get(f.getModSeq()));
                    f.setRegSeqName(adminList.get(f.getRegSeq()));
                    f.setPermissionNameList(menuRepository.findByMenuSeqInAndUseYnAndDelYnAndMenuLevel
                                    (permissionMenuRepository.findByPermissionSeqAndUseYn(f.getPermissionSeq(), "Y").stream().map(e -> e.getMenuSeq())
                                            .collect(Collectors.toList()), "Y", "N", 1)
                            .stream().map(e -> e.getMenuName()).collect(Collectors.joining(", ")));
                }).collect(Collectors.toList());
        logger.info("result = {}", new Gson().toJson(result));
        return result;
    }

    public Paging PermissionListSelectPagingService(PermissionReq dto) {
//        Map<Long, String> adminList = adminRepository.findAll()
//                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));

        QueryResults<Permission> result = permissionRepository.PermissionSelectListPaging(dto);

        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public List<MenuPermissionRes> PermissionMenuSelectLiService(Long seq) {
        return menuRepository.menuUserListSelect(seq);
    }

    public List<MenuPermissionRes> PermissionMenuSelectLv1LiService(Long seq) {
        return menuRepository.menuUserLv1ListSelect(seq);
    }

    public List<MenuPermissionRes> PermissionMenuSelectLv2LiService(Long seq, Long pseq) {
        return menuRepository.menuUserLv2ListSelect(seq, pseq);
    }

    public List<MenuPermissionRes> PermissionMenuSelectLv3LiService(Long seq, Long pseq) {
        return menuRepository.menuUserLv3ListSelect(seq, pseq);
    }

    public PermissionRes PermissionSelectOneService(Long seq) {
        PermissionRes permissionResult = modelMapper.map(permissionRepository.findByPermissionSeqAndDelYn(seq, "N").orElseThrow(IllegalArgumentException::new), PermissionRes.class);
        return permissionResult;
    }

    public long PermissionSaveService(PermissionUpdate dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;

        int count = 0;
        int up = 0;
        Long permissionSeq;
        if (dto.getPermissionSeq() == null) {

            dto.setRegSeq(admin.getAdminSeq());
            dto.setModSeq(admin.getAdminSeq());
            dto.setRegDt(LocalDateTime.now());
            dto.setModDt(LocalDateTime.now());

            Permission p = permissionRepository.save(dto.ofPermission());
            permissionSeq = p.getPermissionSeq();
            //dto.ofPermissionMenu(p.getPermissionSeq()).stream().forEach(e -> permissionMenuRepository.save(e));
        } else {

            dto.setModSeq(admin.getAdminSeq());

            if (dto.getUseYn() != null) {
                up = permissionRepository.updateUseYn(dto.getUseYn(), dto.getPermissionSeq());
                count += up;
            }
            if (dto.getPermissionName() != null) {
                up = permissionRepository.updatePermissionName(dto.getPermissionName(), dto.getPermissionSeq());
                count += up;
            }

            if(count > 0){
                permissionRepository.updateMod(dto.getModSeq(), dto.getPermissionSeq());
            }


            permissionSeq = dto.getPermissionSeq();
        }

        return permissionSeq;
    }

    public void PermissionMenuDelService(Long permissionSeq) {
        permissionMenuRepository.deleteBermissionSeq(permissionSeq);
    }

    public void PermissionMenuSaveService(PermissionMenu permissionMenu) {
        permissionMenuRepository.save(permissionMenu);
    }

    public int UpdatePermissionDelService(Long[] reportSeql) {
        List<Integer> result =  Arrays.stream(reportSeql).map(e -> permissionRepository
                .updatePermissionDelYn(YnType.Y.getName(), e)).collect(Collectors.toList());
        return result.size();
    }
}
