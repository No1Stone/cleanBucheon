package com.bucheon.clean.town.db.repository.metabus;

import com.bucheon.clean.town.db.entity.metabus.MetaPush;
import com.bucheon.clean.town.db.repository.metabus.dsl.MetaPushRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface MetaPushRepository extends JpaRepository<MetaPush, String>, MetaPushRepositoryDsl {

    @Transactional
    int deleteBySendSeq(Long seq);

}
