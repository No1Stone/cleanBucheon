package com.bucheon.clean.town.biz.operation.permission.v1.model;

import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.db.entity.operation.Permission;
import com.bucheon.clean.town.db.entity.operation.PermissionMenu;

import javax.persistence.Column;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PermissionUpdate {

    private Long permissionSeq;
    private String permissionName;
    private String useYn;
    private Long[] delYn;
    private Long regSeq;
    private LocalDateTime regDt;
    private Long modSeq;
    private LocalDateTime modDt;
    private Long[] menuSeq;
    private List<Long> permissionMenuArr;
    public PermissionUpdate(){}
    public PermissionUpdate(Long permissionSeq, String permissionName, String useYn, Long[] delYn, Long regSeq, LocalDateTime regDt, Long modSeq, LocalDateTime modDt, List<Long> permissionMenuArr) {
        this.permissionSeq = permissionSeq;
        this.permissionName = permissionName;
        this.useYn = useYn;
        this.delYn = delYn;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.modSeq = modSeq;
        this.modDt = modDt;
        this.permissionMenuArr = permissionMenuArr;
    }

    public List<Long> getPermissionMenuArr() {
        return permissionMenuArr;
    }

    public void setPermissionMenuArr(List<Long> permissionMenuArr) {
        this.permissionMenuArr = permissionMenuArr;
    }

    public Long getPermissionSeq() {
        return permissionSeq;
    }

    public void setPermissionSeq(Long permissionSeq) {
        this.permissionSeq = permissionSeq;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public Long[] getDelYn() {
        return delYn;
    }

    public void setDelYn(Long[] delYn) {
        this.delYn = delYn;
    }

    public Long getRegSeq() {
        return regSeq;
    }

    public void setRegSeq(Long regSeq) {
        this.regSeq = regSeq;
    }

    public LocalDateTime getRegDt() {
        return regDt;
    }

    public void setRegDt(LocalDateTime regDt) {
        this.regDt = regDt;
    }

    public Long getModSeq() {
        return modSeq;
    }

    public void setModSeq(Long modSeq) {
        this.modSeq = modSeq;
    }

    public LocalDateTime getModDt() {
        return modDt;
    }

    public void setModDt(LocalDateTime modDt) {
        this.modDt = modDt;
    }

    public Permission ofPermission(){
        Permission p = new Permission();
        p.setPermissionName(this.permissionName);
        p.setUseYn(this.useYn);
        p.setDelYn(YnType.N.getName());
        p.setRegSeq(this.regSeq);
        p.setRegDt(LocalDateTime.now());
        p.setModSeq(this.modSeq);
        p.setModDt(LocalDateTime.now());
    return p;
    }

    public   List<PermissionMenu> ofPermissionMenu(Long permissionSeq){
        List<PermissionMenu> pms = new ArrayList<>();
        for(Long e: this.menuSeq){
            PermissionMenu pm = new PermissionMenu();
            pm.setPermissionSeq(permissionSeq);
            pm.setUseYn(YnType.Y.getName());
            pm.setMenuSeq(e);
            pms.add(pm);
        }
        return pms;
    }

}
