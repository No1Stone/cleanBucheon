package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
@Data
@EqualsAndHashCode(callSuper = false)
public class CarouselVO {
	
	private String type;
	
	private List<Object> items;

}
