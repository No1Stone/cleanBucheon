package com.bucheon.clean.town.db.repository.environmental;

import com.bucheon.clean.town.db.entity.environmental.Campaign;
import com.bucheon.clean.town.db.entity.environmental.CampaignView;
import com.bucheon.clean.town.db.repository.environmental.dsl.CampaignRepositoryDsl;
import com.bucheon.clean.town.db.repository.environmental.dsl.CampaignViewRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface CampaignViewRepository extends JpaRepository<CampaignView, Long>, CampaignViewRepositoryDsl {


    @Transactional(readOnly = true)
    CampaignView findByCampaignNum(Long seq);

}
