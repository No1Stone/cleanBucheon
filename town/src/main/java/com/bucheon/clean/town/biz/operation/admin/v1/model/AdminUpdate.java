package com.bucheon.clean.town.biz.operation.admin.v1.model;

import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.db.entity.operation.Admin;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;

public class AdminUpdate {

    private Long adminSeq;
    private String adminId;
    private String adminName;
    private String adminPassword;
    private String superYn;
    private String adminPhone;
    private String adminEmail;
    private String administrativeDivision;
    private Long permissionSeq;
    private String permission;
    private Long regSeq;
    private LocalDateTime regDt;
    private Long modSeq;
    private LocalDateTime modDt;
    private String useYn;
    private Long[] delYn;

    public AdminUpdate() {
    }

    public LocalDateTime getRegDt() {
        return regDt;
    }

    public void setRegDt(LocalDateTime regDt) {
        this.regDt = regDt;
    }

    public LocalDateTime getModDt() {
        return modDt;
    }

    public void setModDt(LocalDateTime modDt) {
        this.modDt = modDt;
    }

    public Long getRegSeq() {
        return regSeq;
    }

    public void setRegSeq(Long regSeq) {
        this.regSeq = regSeq;
    }

    public Long getModSeq() {
        return modSeq;
    }

    public void setModSeq(Long modSeq) {
        this.modSeq = modSeq;
    }

    public String getAdministrativeDivision() {
        return administrativeDivision;
    }

    public void setAdministrativeDivision(String administrativeDivision) {
        this.administrativeDivision = administrativeDivision;
    }

    public Long getAdminSeq() {
        return adminSeq;
    }

    public void setAdminSeq(Long adminSeq) {
        this.adminSeq = adminSeq;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getAdminPassword(PasswordEncoder en) {
        return en.encode(this.adminPassword);
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public String getSuperYn() {
        return superYn;
    }

    public void setSuperYn(String superYn) {
        this.superYn = superYn;
    }

    public Long getPermissionSeq() {
        return permissionSeq;
    }

    public void setPermissionSeq(Long permissionSeq) {
        this.permissionSeq = permissionSeq;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public Long[] getDelYn() {
        return delYn;
    }

    public void setDelYn(Long[] delYn) {
        this.delYn = delYn;
    }

    public String getAdminPhone() {
        return adminPhone;
    }

    public void setAdminPhone(String adminPhone) {
        this.adminPhone = adminPhone;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public Admin ofAdmin(PasswordEncoder en) {
        Admin a = new Admin();

        if (this.adminSeq!=null){
            a.setAdminSeq(this.adminSeq);
        }

        a.setAdminId(this.adminId);
        a.setAdminName(this.adminName);
        a.setAdminPassword(en.encode(this.adminPassword));
        a.setPermissionSeq(this.permissionSeq);
        a.setUseYn(YnType.Y.getName());
        a.setDelYn(YnType.N.getName());
        a.setRegSeq(this.adminSeq);
        a.setRegDt(LocalDateTime.now());
        a.setAdminEmail(this.adminEmail);
        a.setAdministrativeDivision(this.administrativeDivision);
        a.setAdminPhone(this.adminPhone);
        a.setSuperYn(this.superYn);
        a.setModSeq(this.adminSeq);
        a.setModDt(LocalDateTime.now());
        return a;
    }

    public AdminUpdate(Long adminSeq, String adminId, String adminName,
                       String adminPassword, String adminPhone, String adminEmail, String administrativeDivision, String superYn,
                       Long permissionSeq, String permission, Long regSeq , Long modSeq
            , String useYn, Long[] delYn) {
        this.adminSeq = adminSeq;
        this.adminId = adminId;
        this.adminName = adminName;
        this.adminPassword = adminPassword;
        this.adminPhone = adminPhone;
        this.adminEmail = adminEmail;
        this.administrativeDivision = administrativeDivision;
        this.superYn = superYn;
        this.permissionSeq = permissionSeq;
        this.permission = permission;
        this.regSeq = regSeq;
        this.modSeq = modSeq;
        this.useYn = useYn;
        this.delYn = delYn;
    }

}
