package com.bucheon.clean.town.db.repository.operation.dsl;

import com.bucheon.clean.town.biz.operation.board.v1.model.BoardReq;
import com.bucheon.clean.town.db.entity.operation.Board;
import com.bucheon.clean.town.db.entity.operation.QAdmin;
import com.bucheon.clean.town.db.entity.operation.QBoard;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Aspect
@RequiredArgsConstructor
public class BoardRepositoryDslImpl implements BoardRepositoryDsl {


    private final JPAQueryFactory queryFactory;
    private QBoard qBoard = QBoard.board;
    private QAdmin qAdmin = QAdmin.admin;
    private final Logger logger = LoggerFactory.getLogger(BoardRepositoryDslImpl.class);

    @Override
    public List<Board> SelectListBoard(BoardReq dto) {

        if(dto.getSize() == 0){
            dto.setSize(10);
        }


        var aa = queryFactory.select(qBoard).from(qBoard)
                .where(
                        eqBoardType(dto.getBoardType()),
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
//                        qBoard.reportReception.notLike(ReceptionType.A.getName()),
                        qBoard.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qBoard.regDt.desc())
                .fetch();
        return aa;
    }

    @Override
    public QueryResults<Board> SelectListBoardPaging(BoardReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        var aa = queryFactory.select(qBoard).from(qBoard)
                .where(
                        eqBoardType(dto.getBoardType()),
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
//                        qBoard.reportReception.notLike(ReceptionType.A.getName()),
                        qBoard.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .fetchResults();

        return aa;
    }

    @Override
    public Optional<Board> selectType(String type) {

        var aa = queryFactory.selectFrom(qBoard)
                .where(
                        qBoard.boardType.eq(type),
                        qBoard.useYn.eq("Y"),
                        qBoard.delYn.eq("N")
                )
                .limit(1)
                .offset(0)
                .orderBy(qBoard.regDt.desc())
                .fetchOne();
        return Optional.ofNullable(aa);
    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchsearchSelect(String searchSelect, String searchString) {

        try {

            if (searchSelect.equals("searchTitle")) {
                return eqBoardTitle(searchString);
            } else if (searchSelect.equals("searchContent")) {
                return eqBoardContent(searchString);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqBoardType(String s) {
        if (StringUtils.isEmptyOrWhitespace(s)) {
            return null;
        }
        return qBoard.boardType.eq(s);
    }

   private BooleanExpression eqBoardTitle(String s) {
        if (StringUtils.isEmptyOrWhitespace(s)) {
            return null;
        }
        return qBoard.boardTitle.toLowerCase().contains(s);
    }

    private BooleanExpression eqBoardContent(String con) {
        if (StringUtils.isEmptyOrWhitespace(con)) {
            return null;
        }
        return qBoard.boardContent.toLowerCase().contains(con);
    }


}
