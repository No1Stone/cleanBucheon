package com.bucheon.clean.town.biz.operation.board.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BoardReq {

    private int page;
    private int size;
    private Long boardSeq;
    private String boardType;
    private String boardTitle;
    private String boardContent;

    private String searchSelect;
    private String searchString;


}
