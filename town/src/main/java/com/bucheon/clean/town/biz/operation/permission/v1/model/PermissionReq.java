package com.bucheon.clean.town.biz.operation.permission.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PermissionReq {

    private int page;
    private int size;
    private Long permissionSeq;
    private String permissionName;
    private Long regSeq;
    private Long modSeq;
    private String sregDt;
    private String eregDt;
    private String smodDt;
    private String emodDt;
    private String searchSelect;
    private String searchString;

}
