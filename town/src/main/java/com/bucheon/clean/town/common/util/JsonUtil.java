package com.bucheon.clean.town.common.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtil {

	/**
	 * 로그 출력.
	 */
	public static Hashtable<String, Object> toJsonObj(String json) {
		String jsonStr = json;
		Hashtable<String, Object> resultTable = new Hashtable<String, Object>();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			resultTable = mapperObj.readValue(jsonStr, new TypeReference<Hashtable<String, Object>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resultTable;
	}

	public static Map<String, Object> toHashJsonObj(String json) {
		String jsonStr = json;
		Map<String, Object> resultMap = new HashMap<String, Object>();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			resultMap = mapperObj.readValue(jsonStr, new TypeReference<Map<String, Object>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resultMap;
	}

	public static Hashtable<String, Object> toHashTableJsonObj(String json) {
		String jsonStr = json;
		Hashtable<String, Object> resultTable = new Hashtable<String, Object>();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			resultTable = mapperObj.readValue(jsonStr, new TypeReference<Hashtable<String, Object>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resultTable;
	}

	public static Map<String, Object> splitToMap(String sourceString, String delimiter, String delimiter2) {
		if (isNull(sourceString))
			return null;

		String[] keyValuePairs = sourceString.split(delimiter); // split the string to create key-value pairs
		Map<String, Object> map = new HashMap<>();
		for (String pair : keyValuePairs) // iterate over the pairs
		{
			String[] entry = pair.split(delimiter2); // split the pairs to get key and value
			if (entry.length > 1) {
				map.put(entry[0].trim(), entry[1].trim()); // add them to the hashmap and trim whitespaces
			} else {
				map.put(entry[0].trim(), "");
			}
		}
		return map;
	}

	public static boolean isNull(String str) {
		return (str == null || (str.trim().length()) == 0);
	}

	// 스트링 데이터, LIST 구분자 앞 문자, LIST 구분자 , MAP 구분자, MAP KEY VALUE 구분자
	public static ArrayList<Hashtable<String, Object>> splitToListMap(String sourceString, String listDelimiter,
			String listDelimiter1, String delimiter1, String delimiter2) {
		String spilt = sourceString.replace("[", "").replace("]", "");
		if (isNull(spilt) == false) {
			ArrayList<Hashtable<String, Object>> resultTable = new ArrayList<Hashtable<String, Object>>();
			int pos = 0;
			int lastIndexOf = 0;
			while (pos >= 0) {
				pos = spilt.indexOf(listDelimiter);
				lastIndexOf = spilt.lastIndexOf("}");
				int posNextVal = pos + 1;
				if (pos != lastIndexOf) {
					if (spilt.substring(posNextVal, posNextVal + 1).matches(listDelimiter1)) {
						String indexStr = spilt.substring(0, posNextVal);
						Hashtable<String, Object> map = toJsonObj(indexStr);
						resultTable.add(map);
						spilt = spilt.substring(posNextVal + listDelimiter.length());
					}
				} else if (spilt.length() >= 0) {
					Hashtable<String, Object> map = toJsonObj(spilt);
					resultTable.add(map);
					pos = -1;
				}
			}
			return resultTable;
		} else {
			return null;
		}
	}

	// 스트링 데이터, LIST 구분자 앞 문자, LIST 구분자 , MAP 구분자, MAP KEY VALUE 구분자
	public static ArrayList<Map<String, Object>> splitToListHashMap(String sourceString, String listDelimiter,
			String listDelimiter1, String delimiter1, String delimiter2) {
		String spilt = sourceString.replace("[", "").replace("]", "");
		if (isNull(spilt) == false) {
			ArrayList<Map<String, Object>> resultMap = new ArrayList<Map<String, Object>>();
			int pos = 0;
			int lastIndexOf = 0;
			while (pos >= 0) {
				pos = spilt.indexOf(listDelimiter);
				lastIndexOf = spilt.lastIndexOf("}");
				int posNextVal = pos + 1;
				if (pos != lastIndexOf) {
					if (spilt.substring(posNextVal, posNextVal + 1).matches(listDelimiter1)) {
						String indexStr = spilt.substring(0, posNextVal);
						Map<String, Object> map = toHashJsonObj(indexStr);
						resultMap.add(map);
						spilt = spilt.substring(posNextVal + listDelimiter.length());
					}
				} else if (spilt.length() >= 0) {
					Map<String, Object> map = toHashJsonObj(spilt);
					resultMap.add(map);
					pos = -1;
				}
			}
			return resultMap;
		} else {
			return null;
		}
	}

	// 스트링 데이터, LIST 구분자 앞 문자, LIST 구분자
	public static String[] convJsonArrToStrArr(String sourceString) {
		if (isNull(sourceString) == false) {
			String[] letters = sourceString.split("\",\"");
			String[] strArr = new String[letters.length];
			int lastIndex = sourceString.indexOf("\"]");
			int letterStartIdx = 1;
			int letterEndIdx = 0;
			int currentIndex = 0;

			String sourceValue = sourceString.replace("[\"", "\"").replace("\"]", "\"");

			while (letterEndIdx < lastIndex) {

				letterEndIdx = (sourceValue.indexOf("\",\"") == -1 ? sourceValue.lastIndexOf("\"")
						: sourceValue.indexOf("\",\"")) + letterEndIdx;

				String key = "";
				int valueLength = sourceValue.length();

				if (letterEndIdx <= valueLength) {
					key = sourceValue.substring(letterStartIdx, letterEndIdx);
					strArr[currentIndex] = key;
					letterStartIdx = letterEndIdx + 3;
					letterEndIdx = letterEndIdx + 3;
					currentIndex++;
				}
			}
			return strArr;
		} else {
			return null;
		}
	}

	/**
	 * JSON String --> MAP
	 */
	public Map<String, Object> JsonStringToMap(String jsonStr) {
		Map<String, Object> retMap = new HashMap<String, Object>();

		try {
			ObjectMapper mapper = new ObjectMapper();
			retMap = mapper.readValue(jsonStr, new TypeReference<Map<String, Object>>() {
			});

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return retMap;
	}

	/**
	 * JsonArray를 List<Map<String, String>>으로 변환한다.
	 *
	 * @param jsonArray JSONArray.
	 * @return List<Map<String, Object>>.
	 */
	public static List<Map<String, Object>> getListMapFromJsonArray(JSONArray jsonArray) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		if (jsonArray != null) {
			int jsonSize = ((Map<String, Object>) jsonArray).size();
			for (int i = 0; i < jsonSize; i++) {
				Map<String, Object> map = JsonUtil.getMapFromJsonObject((JSONObject) jsonArray.get(i));
				list.add(map);
			}
		}

		return list;
	}

	/**
	 * JsonObject를 Map<String, String>으로 변환한다.
	 *
	 * @param jsonObj JSONObject.
	 * @return Map<String, Object>.
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> getMapFromJsonObject(JSONObject jsonObj) {
		Map<String, Object> map = null;

		try {

			map = new ObjectMapper().readValue(jsonObj.toString(), Map.class);

		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * JSON --> MAP
	 */
	public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
		Map<String, Object> retMap = new HashMap<String, Object>();

		if (json != null) {
			retMap = toMap(json);
		}

		return retMap;
	}

	/**
	 * JSON Object --> MAP
	 */
	public static Map<String, Object> toMap(JSONObject object) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();

		Iterator<String> keysItr = object.keys();

		while (keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}

		return map;
	}

	/**
	 * JSON Array --> LIST
	 */
	public static List<Object> toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();

		for (int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}

		return list;
	}

	/**
	 * ListMap --> JSON string
	 */
	public String listmap_to_json_string(List<Map<String, Object>> list) {
		JSONArray json_arr = new JSONArray();
		for (Map<String, Object> map : list) {
			JSONObject json_obj = new JSONObject();
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				String key = entry.getKey();
				Object value = entry.getValue();
				try {
					json_obj.put(key, value);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			json_arr.put(json_obj);
		}
		return json_arr.toString();
	}

	/**
	 * Map --> JSON String
	 */
	public String map_to_json_string(HashMap<String, Object> map) {
		JSONObject jsonObject = new JSONObject();
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			jsonObject.put(key, value);
		}

		return jsonObject.toString();
	}

	/**
	 * @title stringCapitalize
	 * @DESC 문장의 앞글자만 대문자로 변경
	 * @param (String)str
	 * @return(String)str
	 * @throws JSONException
	 */
	public static String stringCapitalize(String str) throws JSONException {
		StringBuilder b = new StringBuilder(str);
		int i = 0;
		do {
			b.replace(i, i + 1, b.substring(i, i + 1).toUpperCase());
			i = b.indexOf(" ", i) + 1;
		} while (i > 0 && i < b.length());

		return b.toString();
	}

	public ArrayList<Hashtable<String, Object>> setTextArr(String textLine, int MaxLength, String key) {

		Hashtable<String, Object> textStructure = new Hashtable<String, Object>();
		ArrayList<Hashtable<String, Object>> textArr = new ArrayList<Hashtable<String, Object>>();
		StringBuffer sb = new StringBuffer();
		int curLen = 0;
		String curChar;

		for (int i = 0; i < textLine.length(); i++) {
			curChar = textLine.substring(i, i + 1);
			curLen += curChar.getBytes().length;
			if (curLen > MaxLength) {

				textStructure = new Hashtable<String, Object>();

				textStructure.put(key, String.valueOf(sb));
				textArr.add(textStructure);
				sb = new StringBuffer();
				curLen = 0;
			} else {
				sb.append(curChar);
			}
		}
		if (sb.length() > 0) {

			textStructure = new Hashtable<String, Object>();

			textStructure.put(key, String.valueOf(sb));
			textArr.add(textStructure);
		}
		return textArr;
	}
}
