package com.bucheon.clean.town.biz.operation.admin.v1.controller;

import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminReq;
import com.bucheon.clean.town.biz.operation.admin.v1.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/admin/rest")
@RequiredArgsConstructor
public class AdminRestController {

    private final Logger logger = LoggerFactory.getLogger(AdminRestController.class);
    private final AdminService adminService;

    @PostMapping(path = "/test1")
    public Object AdminSelectListController(@RequestBody AdminReq dto) {

        return adminService.AdminSelectListService(dto);
    }


    @GetMapping(path = "/test2/{seq}")
    public Object AdminSelectListController(@PathVariable Long seq) {
        return adminService.AdminSelectOneService(seq);
    }

}
