package com.bucheon.clean.town.db.entity.environmental.band;

import com.bucheon.clean.town.db.entity.environmental.band.id.TB_BandBoardListPotoId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_band_board_list_poto")
@Getter
@Setter
@NoArgsConstructor
@IdClass(TB_BandBoardListPotoId.class)
public class TB_BandBoardListPoto {

    @Id
    @Column(name = "post_key", nullable = true)
    private String postKey;
    @Column(name = "height", nullable = true)
    private int height;
    @Column(name = "width", nullable = true)
    private int width;
    @Column(name = "created_at", nullable = true)
    private long createdAt;
    @Column(name = "url", nullable = true)
    private String url;
    @Column(name = "photo_album_key", nullable = true)
    private String photoAlbumKey;
    @Id
    @Column(name = "photo_key", nullable = true)
    private String photoKey;
    @Column(name = "comment_count", nullable = true)
    private int commentCount;
    @Column(name = "emotion_count", nullable = true)
    private int emotionCount;
    @Column(name = "is_video_thumbnail", nullable = true)
    private boolean isVideoThumbnail;
    @Column(name = "name", nullable = true)
    private String name;
    @Column(name = "description", nullable = true)
    private String description;
    @Column(name = "role", nullable = true)
    private String role;
    @Column(name = "profile_image_url", nullable = true)
    private String profileImageUrl;
    @Column(name = "user_key", nullable = true)
    private String userKey;

    @Builder
    TB_BandBoardListPoto(String postKey,
                         int height,
                         int width,
                         long createdAt,
                         String url,
                         String photoAlbumKey,
                         String photoKey,
                         int commentCount,
                         int emotionCount,
                         boolean isVideoThumbnail,
                         String name,
                         String description,
                         String role,
                         String profileImageUrl,
                         String userKey
    ) {
        this.postKey = postKey;
        this.height = height;
        this.width = width;
        this.createdAt = createdAt;
        this.url = url;
        this.photoAlbumKey = photoAlbumKey;
        this.photoKey = photoKey;
        this.commentCount = commentCount;
        this.emotionCount = emotionCount;
        this.isVideoThumbnail = isVideoThumbnail;
        this.name = name;
        this.description = description;
        this.role = role;
        this.profileImageUrl = profileImageUrl;
        this.userKey = userKey;
    }
}
