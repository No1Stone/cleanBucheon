package com.bucheon.clean.town.biz.keyword.v1.controller;

import com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.controller.AlaramtalkController;
import com.bucheon.clean.town.biz.keyword.v1.model.InterestWord;
import com.bucheon.clean.town.biz.keyword.v1.model.WordReq;
import com.bucheon.clean.town.biz.keyword.v1.service.interestWord.InterestwordService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;

@Controller
@RequestMapping(path = "/interestword")
@RequiredArgsConstructor
public class InterestWordController {

    private final Logger logger = LoggerFactory.getLogger(AlaramtalkController.class);
    private final MenuService menuService;
    private final InterestwordService interestwordService;
    private final GlobalAttributeAdviser globalAttributeAdviser;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/keyword/interestWord/interestWordManage");
    }

    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(WordReq dto, HttpServletRequest request) throws IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(46L);

        dto.setPage(0);
        dto.setSize(10);
        dto.setSearchState("all");
        dto.setSearchCode("전체");
        dto.setSearchWord1("");
        dto.setSearchWord2("");

        return new ModelAndView("keyword/interestWord/interestWordManage")
                .addObject("paging",interestwordService.getPage(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                .addObject("busiBranCodeMap", globalAttributeAdviser.getCodeMap())
                ;
    }

    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(WordReq dto){
        return new ModelAndView("keyword/interestWord/interestWordListAjax")
                .addObject("list",interestwordService.getList(dto))
                .addObject("paging",interestwordService.getPage(dto));
    }

    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm() throws IOException {
        return new ModelAndView("keyword/interestWord/interestWordManagePopupAjax")
                .addObject("word", new InterestWord())
                .addObject("busiBranCodeMap", globalAttributeAdviser.getCodeMap())
                ;
    }

    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView modifyForm(InterestWord dto) throws IOException {
        return new ModelAndView("keyword/interestWord/interestWordManagePopupAjax")
                .addObject("word", interestwordService.getOne(dto))
                .addObject("busiBranCodeMap", globalAttributeAdviser.getCodeMap())
                ;
    }

    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(InterestWord interestWord){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", interestwordService.save(interestWord));
        return map;
    }

    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(InterestWord interestWord){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result",   interestwordService.update(interestWord));
        return map;
    }

    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(String[] word1){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result",  interestwordService.delete(word1));
        return map;
    }
}
