package com.bucheon.clean.town.biz.util.controller;

import com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model.AlramSendVO;
import com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.service.AlaramtalkService;
import com.bucheon.clean.town.biz.environmental.report.v1.band.BandService;
import com.bucheon.clean.town.biz.util.service.FCMService;
import com.bucheon.clean.town.biz.util.service.UtilService;
import com.bucheon.clean.town.common.type.ProcessType;
import com.bucheon.clean.town.db.entity.environmental.Report;
import com.bucheon.clean.town.db.entity.environmental.ReportLog;
import com.bucheon.clean.town.db.entity.etc.DongDamdang;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.repository.environmental.ReportLogRepository;
import com.bucheon.clean.town.db.repository.environmental.ReportRepository;
import com.bucheon.clean.town.db.repository.environmental.band.TB_BandBoardListRepository;
import com.bucheon.clean.town.db.repository.etc.DongBandDamdangRepository;
import com.bucheon.clean.town.db.repository.etc.DongDamdangRepository;
import com.bucheon.clean.town.db.repository.operation.AdminRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/testC")
@RequiredArgsConstructor
public class TestController {

    private final Logger logger = LoggerFactory.getLogger(TestController.class);
    private final UtilService utilService;
    private final TB_BandBoardListRepository tb_BandBoardListRepository;
    private final DongBandDamdangRepository dongBandDamdangRepository;
    private final ReportRepository reportRepository;
    private final ReportLogRepository reportLogRepository;
    private final DongDamdangRepository dongDamdangRepository;
    private final AdminRepository adminRepository;
    private final BandService bandService;
    private final FCMService fcmService;


    @Value("${naver.band.schema}")
    private String bandSchema;
    @Value("${naver.band.url}")
    private String bandUrl;

    @GetMapping(path = "/")
    public ModelAndView view() {
        return new ModelAndView("test/testfileUpload");
    }

    @PostMapping(path = "/upload")
    public Object testfileUpload(@RequestParam MultipartFile[] uploadfile, Model model) throws IllegalStateException, IOException {
        utilService.FileSaveService(uploadfile, "trash");
        return new ModelAndView("test/testfileUpload");
    }

    @GetMapping(path = "/shorturl")
    @ResponseBody
    public Object shortURLReturn() throws UnsupportedEncodingException {
        return  utilService.CreateShortURLService("https://cleantown.bucheon.go.kr/admin/img/report/chatbot/20220204_131800.png__c007cb51-9701-4ab0-a349-9296b32e4aed_img.png");
    }

    @GetMapping(path = "/fu")
    @ResponseBody
    public void fu() throws UnsupportedEncodingException, InterruptedException {
        ExecutorService es = Executors.newCachedThreadPool();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Hello");
        Thread.sleep(3000);
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Exit");
    }

    @GetMapping(path = "/chatbot")
    @ResponseBody
    public void chatbot() {
        //ExecutorService es = Executors.newCachedThreadPool();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Start");
        //Thread.sleep(10000);
        int cnt = reportRepository.countByReportReceptionAndReportStatusAndDelYn("C", "D", "N");
        if(cnt > 0) {
            List<Report> reportCList = reportRepository.findAllByReportReceptionAndReportStatusAndDelYnOrderByReportSeqDesc("C", "D", "N");
            var damdang = adminRepository.findByUseYnAndDelYnOrderByAdminNameAsc("Y","N")
                    .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
            for(Report reportCOne : reportCList){

                try {

                    int dongDamdangcnt = dongDamdangRepository.countByDong(reportCOne.getAreaName());
                    if(dongDamdangcnt > 0) {

                        logger.info("- reportCOne : " + reportCOne.getAreaName());

                        //담당자를 구한다
                        DongDamdang dongDamdang = dongDamdangRepository.findByDong(reportCOne.getAreaName()).orElseThrow(IllegalArgumentException::new);
                        Admin admin = adminRepository.findByAdminSeq(dongDamdang.getDamdang()).orElseThrow(IllegalArgumentException::new);

                        //업데이트 한다
                        reportCOne.setReportStatus(ProcessType.P.getName());
                        reportRepository.UpdateReportStatus(reportCOne.getReportStatus(), reportCOne.getReportSeq());

                        //로그를 남긴다
                        String msg = "";
                        //로그 저장
                        if(reportCOne.getReportStatus().equals(ProcessType.D.getName())){
                            msg = "접수하였습니다";
                        } else if(reportCOne.getReportStatus().equals(ProcessType.C.getName())){
                            msg = "담당자 " + admin.getAdminName() + "님께서 처리완료로 변경하였습니다.";
                        } else if(reportCOne.getReportStatus().equals(ProcessType.P.getName())){
                            msg = "담당자 " + admin.getAdminName() + "님께서 처리중으로 변경하였습니다.";
                        } else if(reportCOne.getReportStatus().equals(ProcessType.R.getName())){
                            msg = "담당자 " + admin.getAdminName() + "님께서 처리불가로 변경하였습니다.";
                        }
                        logger.info("- msg : {}" + msg);

                        int cnt2 = reportLogRepository.countByReportSeq(reportCOne.getReportSeq());
                        String rn = String.valueOf(Timestamp.valueOf(LocalDateTime.now()).getTime());
                        if (cnt2 > 0) {
                            ReportLog reportLog = reportLogRepository.findTop1ByReportSeqOrderByLogSeqDesc(reportCOne.getReportSeq()).orElseThrow(IllegalArgumentException::new);
                            rn = reportLog.getReceptionNum();
                        }
                        ReportLog r2 = new ReportLog();
                        r2.setReportSeq(reportCOne.getReportSeq());
                        r2.setLogTime(LocalDateTime.now());
                        r2.setReceptionNum(rn);
                        r2.setReportStatus(reportCOne.getReportStatus());
                        r2.setReportDamdang(dongDamdang.getDamdang());
                        r2.setMsg(msg);
                        r2.setReportPointRejectReason(reportCOne.getReportPointRejectReason());

                        reportLogRepository.save(r2);
                    }

                } catch (Exception e){
                    e.printStackTrace();
                }

            }


        }
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> End");
    }

    @GetMapping(path = "/bandComentCheck")
    @ResponseBody
    public void bandComentCheck() {
        bandService.ReprotCommentProc();

    }

    @GetMapping(path = "/bandListGet")
    @ResponseBody
    public void bandListGet() {
        bandService.BandBoardGet();
    }


//    @PostMapping(path = "/alarm-test")
//    @ResponseBody
//    public String bandListGet(@RequestBody AlramSendVO dto) {
//        return alaramtalkService.AlramSendService(dto);
//    }


    @GetMapping(path = "/bandpost")
    @ResponseBody
    public String bandPost() {


        String chatContents = "[부천시 깨끗한마을]"
                + "\n"
                + "접수자 : 010****2686"
                + "\n"
                + "주소 : 경기도 부천시 석천로110번길 11"
                + "\n"
                + "일반쓰레기 및 의류 종량제없이 무단 투기\n" +
                "8월 1일 오전 일반종량제봉투 및 재활용 쓰레기와 함께 섞어 투기하였고 현재 불법투기 잔해들만 남은 상황, 어제 버려진 택배 박스에서 중동 588-2 선경빌라 1동 102호 라는 주소가 발견"
                + "\n" + "사진 : https://me2.do/5WGxPpOZ"
                + "\n" + "사진 : https://me2.do/xmizOTxY"
                + "\n" + "사진 : https://me2.do/GWoknjeH"
                ;


        return bandService.SendBandPost("중동", chatContents);
    }


    @GetMapping(path = "/getToken")
    @ResponseBody
    public String getToken() throws IOException {

        return fcmService.getAccessToken();
    }

}
