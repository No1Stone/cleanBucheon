package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
@Data
@EqualsAndHashCode(callSuper = false)
public class CarouselItems {
	
	private String title;
	
	private String description;
	
	private CarouselItemsThumbnail thumbnail;
	
	private List<Object> buttons;

}
