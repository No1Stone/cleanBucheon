package com.bucheon.clean.town.db.entity.operation.id;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class AdminId implements Serializable {
    private Long adminSeq;
    private Long permissionSeq;
}
