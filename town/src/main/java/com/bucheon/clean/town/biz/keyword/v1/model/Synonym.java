package com.bucheon.clean.town.biz.keyword.v1.model;

import lombok.*;

import java.util.Date;

@Data
public class Synonym {

	private String representativeWord1;

	private String representativeWord2;

	private String originWord1;

	private String originWord2;

	private String busiBranCode;

	private String busiBranCodeName;

	private Date startDatetime;

	private Date endDatetime;
	
}
