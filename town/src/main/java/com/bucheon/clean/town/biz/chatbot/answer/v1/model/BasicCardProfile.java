package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class BasicCardProfile {
	
	private String imageUrl;
	
	private String nickname;

}
