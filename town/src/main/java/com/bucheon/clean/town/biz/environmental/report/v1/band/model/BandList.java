package com.bucheon.clean.town.biz.environmental.report.v1.band.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BandList {

    private String name;
    private String band_key;
    private String cover;
    private int member_count;

}
