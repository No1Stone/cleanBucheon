package com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model;

import com.bucheon.clean.town.common.type.ExcelColumnName;
import com.bucheon.clean.town.common.type.ExcelDto;
import com.bucheon.clean.town.common.type.ExcelFileName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@ExcelFileName
public class MsgLogExcelDto implements ExcelDto {

    @ExcelColumnName(headerName = "번호")
    @JsonProperty("번호")
    private Long num;

    @ExcelColumnName(headerName = "TmpKey")
    @JsonProperty("TmpKey")
    private String msgNoticetalkTmpKey;

    @ExcelColumnName(headerName = "발송일자")
    @JsonProperty("발송일자")
    private String sentDate;

    @ExcelColumnName(headerName = "수신자번호")
    @JsonProperty("수신자번호")
    private String callTo;

    @ExcelColumnName(headerName = "발송타입")
    @JsonProperty("발송타입")
    private String sendType;

    @ExcelColumnName(headerName = "발송결과")
    @JsonProperty("발송결과")
    private String sendRslt;

    @ExcelColumnName(headerName = "발송결과 코드")
    @JsonProperty("발송결과 코드")
    private String rsltCode;

    @ExcelColumnName(headerName = "발송결과 상세")
    @JsonProperty("발송결과 상세")
    private String rsltText;

    @Override
    public List<String> mapToList() {

        return Arrays.asList(String.valueOf(num), msgNoticetalkTmpKey, sentDate, callTo, sendType,
                sendRslt, rsltCode, rsltText);
    }

    @Override
    public List<Integer> mapToCoulmn() {

        return Arrays.asList(4000, 20000, 8000, 8000, 8000,
                6000, 6000, 12000);
    }

}
