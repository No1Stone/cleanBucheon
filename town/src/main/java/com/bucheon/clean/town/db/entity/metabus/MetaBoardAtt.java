package com.bucheon.clean.town.db.entity.metabus;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_meta_board_att")
@Getter
@Setter
@NoArgsConstructor
public class MetaBoardAtt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "board_att_seq", nullable = true)
    private Long boardAttSeq;
    @Column(name = "board_seq", nullable = true)
    private Long boardSeq;
    @Column(name = "board_att", nullable = true)
    private String boardAtt;
    @Column(name = "board_att_order", nullable = true)
    private int boardAttOrder;
    @Column(name = "board_att_ori", nullable = true)
    private String boardAttOri;
    @Column(name = "board_att_path", nullable = true)
    private String boardAttPath;
    @Column(name = "board_att_type", nullable = true)
    private String boardAttType;
    @Column(name = "board_att_size", nullable = true)
    private Long boardAttSize;

    @Builder
    MetaBoardAtt(
            Long boardAttSeq,
            Long boardSeq,
            String boardAtt,
            int boardAttOrder,
            String boardAttOri,
            String boardAttPath,
            String boardAttType,
            Long boardAttSize
            ) {
        this.boardAttSeq = boardAttSeq;
        this.boardSeq = boardSeq;
        this.boardAtt = boardAtt;
        this.boardAttOrder = boardAttOrder;
        this.boardAttOri = boardAttOri;
        this.boardAttPath = boardAttPath;
        this.boardAttType = boardAttType;
        this.boardAttSize = boardAttSize;
    }

}
