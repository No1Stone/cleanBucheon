package com.bucheon.clean.town.biz.metabus.metaUser.v1.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MetaUserPointHistoryVO {

    private Long pointSeq;
    private String userId;
    private int point;
    private int accumulatedPoint;
    private Long trashSeq;
    private String processLocation;
    private String reason;
    private LocalDateTime regDt;

}
