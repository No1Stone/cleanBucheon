package com.bucheon.clean.town.biz.util.model;

import com.bucheon.clean.town.db.entity.etc.ConnectSumLog;
import com.bucheon.clean.town.db.entity.metabus.MetaUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConnectSumLogVO {

    LocalDate connectDate;
    String connectType;
    String userId;


    public ConnectSumLog ofConnectSumLog(){
        ConnectSumLog c = new ConnectSumLog();

        c.setConnectDate(this.connectDate);
        c.setConnectType(this.connectType);
        c.setUserId(this.userId);
        return c;
    }

}
