package com.bucheon.clean.town.biz.operation.declarationPoint.v1.service;

import com.bucheon.clean.town.biz.operation.declarationPoint.v1.model.DeclarationPointVO;
import com.bucheon.clean.town.biz.util.service.UtilService;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.DeclarationPoint;
import com.bucheon.clean.town.db.repository.operation.DeclarationPointRepository;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class DeclarationPointService {

    private final Logger logger = LoggerFactory.getLogger(DeclarationPointService.class);
    private final HttpServletRequest httpServletRequest;
    private final DeclarationPointRepository declarationPointRepository;
    private final ModelMapper modelmapper;
    private final UtilService utilService;

    public DeclarationPoint DeclarationPointSelectOneService() {
        DeclarationPoint result = declarationPointRepository.findById(1L).get();
        return result;
    }

    @Transactional
    public String DeclarationPointSaveService(DeclarationPointVO dto) throws IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;

        int count = 0;
        int up = 0;

        logger.info(" DeclarationPointVO - - - -{}", new Gson().toJson(dto));

        if (dto.getDeclarationSeq() == null) {
            //아무것도 안함
        } else {

            if (dto.getDeclarationPoint() >= 0) {
                up = declarationPointRepository.updateDeclarationPoint(dto.getDeclarationPoint(), admin.getAdminSeq(), dto.getDeclarationSeq());
                count += up;
            }
        }
        if (count > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

}
