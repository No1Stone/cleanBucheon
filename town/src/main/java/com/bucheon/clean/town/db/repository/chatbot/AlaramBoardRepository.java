package com.bucheon.clean.town.db.repository.chatbot;

import com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model.MsgLogCountRes;
import com.bucheon.clean.town.db.entity.chatbot.TB_AlaramBoard;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface AlaramBoardRepository extends JpaRepository<TB_AlaramBoard, Long> {

    List<TB_AlaramBoard> findByPhoneNumberOrderByAlaramBoardDtDesc(String phone);
    List<TB_AlaramBoard> findByPhoneNumberAndAlaramBoardDtGreaterThanAndAlaramBoardDtLessThanOrderByAlaramBoardDtDesc(String phone, LocalDateTime sdate, LocalDateTime edate, Pageable pageable);
    List<TB_AlaramBoard> findByChatbotUserIdAndAlaramBoardDtGreaterThanAndAlaramBoardDtLessThanOrderByAlaramBoardDtDesc(String chatbotUserId, LocalDateTime sdate, LocalDateTime edate, Pageable pageable);

    //나의 랭킹관련
    @Query(
            value = "WITH alram_log_tot as( " +
                    "SELECT  " +
                    "   MSG_SEQ, " +
                    "   MSG_NOTICETALK_TMP_KEY, " +
                    "   SENT_DATE, " +
                    "   CALL_TO, " +
                    "   MSG_PAY_CODE, " +
                    "   RSLT_CODE2 " +
                    "FROM :msglogTable " +
                    "WHERE SENT_DATE BETWEEN STR_TO_DATE(CONCAT(:sdate,' 000000'), '%Y%m%d%H%i%s') AND STR_TO_DATE(CONCAT(:edate,' 235959'), '%Y%m%d%H%i%s') " +
                    "and RSLT_CODE2 is not null " +
                    ")," +
                    "alram_log_sucess as( " +
                    "SELECT * " +
                    "FROM alram_log_tot " +
                    "WHERE RSLT_CODE2 ='0' " +
                    ") " +
                    "SELECT  " +
                    "   A.total AS total " +
                    "   , B.success AS success " +
                    "   , ifnull(round(B.success/A.total*100,2),0) AS sucessRate " +
                    "   , C.fail AS fail " +
                    "   , ifnull(round(C.fail/A.total*100,2),0) AS failRate " +
                    "   , D.kakao AS kakao " +
                    "   , ifnull(round(D.kakao/B.success*100,2),0) AS kakaoRate " +
                    "   , E.sms AS sms " +
                    "   , ifnull(round(E.sms/B.success*100,2),0) AS smsRate " +
                    "FROM " +
                    "   (SELECT count(SENT_DATE) AS total FROM alram_log_tot) A " +
                    "   ,(SELECT count(SENT_DATE) AS success FROM alram_log_sucess) B " +
                    "   ,(SELECT count(SENT_DATE) AS fail FROM alram_log_tot WHERE RSLT_CODE2 !='0') C " +
                    "   ,(SELECT count(SENT_DATE) AS kakao FROM alram_log_sucess WHERE MSG_PAY_CODE ='NOT') D " +
                    "   ,(SELECT count(SENT_DATE) AS sms FROM alram_log_sucess WHERE MSG_PAY_CODE ='LMS' OR MSG_PAY_CODE ='MMS') E " +
                    ";"
            , nativeQuery = true
    )
    MsgLogCountRes selectAlramCnt(@Param("msglogTable") String msglogTable, @Param("sdate") String sdate, @Param("edate") String edate);
}
