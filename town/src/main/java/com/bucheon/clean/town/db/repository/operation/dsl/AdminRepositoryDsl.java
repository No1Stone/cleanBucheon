package com.bucheon.clean.town.db.repository.operation.dsl;

import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminReq;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.QAdmin;
import com.querydsl.core.QueryResults;
import net.bytebuddy.asm.Advice;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AdminRepositoryDsl {

    @Transactional(readOnly = true)
    List<Admin> AdminSelectList(AdminReq dto);

    @Transactional(readOnly = true)
    QueryResults<Admin> AdminSelectListPaging(AdminReq dto);
}
