package com.bucheon.clean.town.db.entity.chatbot.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SkllCmercId implements Serializable {

    private String skllCode;
    private int rspnsOrdr;
}
