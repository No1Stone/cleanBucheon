package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import java.util.Map;

import lombok.Data;

@Data
public class CommerceCardThumbnails {

	private String imageUrl;
	
	private Map<String, Object> link;

}
