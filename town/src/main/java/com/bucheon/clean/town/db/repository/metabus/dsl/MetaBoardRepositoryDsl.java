package com.bucheon.clean.town.db.repository.metabus.dsl;


import com.bucheon.clean.town.biz.metabus.metaBoard.v1.model.MetaBoardReq;
import com.bucheon.clean.town.biz.metabus.metaBoard.v1.model.MetaBoardVO;
import com.bucheon.clean.town.db.entity.metabus.MetaBoard;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface MetaBoardRepositoryDsl {

    @Transactional(readOnly = true)
    List<MetaBoard> SelectListMetaBoard(MetaBoardReq dto);
    @Transactional(readOnly = true)
    QueryResults<MetaBoard> SelectListMetaBoardPaging(MetaBoardReq dto);
    @Transactional(readOnly = true)
    List<MetaBoard> SelectNowListMetaBoard(MetaBoardReq dto);
    @Transactional(readOnly = true)
    QueryResults<MetaBoard> SelectNowListMetaBoardPaging(MetaBoardReq dto);
    @Transactional(readOnly = true)
    List<MetaBoard> SelectPastListMetaBoard(MetaBoardReq dto);
    @Transactional(readOnly = true)
    QueryResults<MetaBoard> SelectPastListMetaBoardPaging(MetaBoardReq dto);
    @Transactional(readOnly = true)
    List<MetaBoard> SelectTocomeListMetaBoard(MetaBoardReq dto);
    @Transactional(readOnly = true)
    QueryResults<MetaBoard> SelectTocomeListMetaBoardPaging(MetaBoardReq dto);
    @Transactional(readOnly = true)
    Optional<MetaBoard> selectType(String type);

    @Transactional(readOnly = true)
    Long confirmType(MetaBoardVO dto);


}
