package com.bucheon.clean.town.db.repository.operation;

import com.bucheon.clean.town.db.entity.operation.TrashType;
import com.bucheon.clean.town.db.repository.operation.dsl.TrashTypeRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TrashTypeRepository extends JpaRepository<TrashType, Long>, TrashTypeRepositoryDsl {

    List<TrashType> findAllByUseYnAndDelYn(String use, String del);
    TrashType findByTrashSeqAndUseYnAndDelYn(Long seq,String use, String del);


    @Transactional
    @Modifying
    @Query(
            value = "update tb_trash_type set use_yn = :useYn where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateTrashTypeUseYn(@Param("useYn") String useYn, @Param("trashSeq") Long trashSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_trash_type set del_yn = :delYn where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateTrashTypeDelYn(@Param("delYn") String delYn, @Param("trashSeq") Long trashSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_trash_type set trash_name = :trashName where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateTrashTypeName(@Param("trashName") String trashName, @Param("trashSeq") Long trashSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_trash_type set declaration_mileage = :declarationMileage where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateTrashTypeDeclarationMileage(@Param("declarationMileage") int declarationMileage, @Param("trashSeq") Long trashSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_trash_type set process_mileage = :processMileage where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateTrashTypeProcessMileage(@Param("processMileage") int processMileage, @Param("trashSeq") Long trashSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_trash_type set icon_url = :iconUrl where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateTrashTypeIconURL(@Param("iconUrl") String iconUrl, @Param("trashSeq") Long trashSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_trash_type set mod_seq = :modSeq, mod_dt = now() where trash_seq =:trashSeq "
            , nativeQuery = true
    )
    int UpdateMod(@Param("modSeq") Long modSeq, @Param("trashSeq") Long trashSeq);

}
