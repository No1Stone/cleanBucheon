package com.bucheon.clean.town.biz.metabus.metaUser.v1.service;

import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.*;
import com.bucheon.clean.town.biz.operation.point.v1.model.TrashTypeReq;
import com.bucheon.clean.town.biz.operation.point.v1.model.TrashTypeUpdate;
import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.metabus.MetaTrashType;
import com.bucheon.clean.town.db.entity.metabus.MetaUser;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.TrashType;
import com.bucheon.clean.town.db.repository.metabus.MetaTrashTypeRepository;
import com.bucheon.clean.town.db.repository.metabus.MetaUserPointHistoryRepository;
import com.bucheon.clean.town.db.repository.metabus.MetaUserRepository;
import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MetaTrashTypeService {

    private final Logger logger = LoggerFactory.getLogger(MetaTrashTypeService.class);
    private final HttpServletRequest httpServletRequest;
    private final MetaTrashTypeRepository metaTrashTypeRepository;
    private final ModelMapper modelMapper;

    public Object MetaTrashSelectListService(MetaTrashTypeReq dto) {
        return metaTrashTypeRepository.MetaTrashTypeSelectList(dto);
    }

    public Object MetaTrashSelectListPagingService(MetaTrashTypeReq dto) {
        QueryResults<MetaTrashType> result = metaTrashTypeRepository.MetaTrashTypeSelectListPaging(dto);

        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public MetaTrashType MetaTrashSelectOneService(Long seq) {
        return metaTrashTypeRepository.findByTrashSeq(seq).orElseThrow(IllegalArgumentException::new);
    }

    @Transactional
    public Object MetaTrashSaveService(MetaTrashTypeVO dto) throws IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;

        int count = 0;
        int up = 0;

        if (dto.getTrashSeq() == null) {

            dto.setRegSeq(admin.getAdminSeq());
            dto.setModSeq(admin.getAdminSeq());
            dto.setRegDt(LocalDateTime.now());
            dto.setModDt(LocalDateTime.now());

            MetaTrashType tt = metaTrashTypeRepository.save(dto.ofTrashType());


        } else {

            dto.setModSeq(admin.getAdminSeq());

            if (dto.getTrashName() != null) {
                up = metaTrashTypeRepository.UpdateTrashTypeName(dto.getTrashName(), dto.getTrashSeq());
                count += up;
            }
            if (dto.getUseYn() != null) {
                up = metaTrashTypeRepository.UpdateTrashTypeUseYn(dto.getUseYn(), dto.getTrashSeq());
                count += up;
            }
            if (dto.getTrashAct() != null) {
                up = metaTrashTypeRepository.UpdateTrashAct(dto.getTrashAct(), dto.getTrashSeq());
                count += up;
            }
            if (dto.getTrashPoint() > 0) {
                up = metaTrashTypeRepository.UpdateTrashPoint(dto.getTrashPoint(), dto.getTrashSeq());
                count += up;
            }

            if (count > 0) {
                metaTrashTypeRepository.UpdateMod(dto.getModSeq(), dto.getTrashSeq());
            }
        }
        if (count > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    public int UpdateTrashTypeDelService(Long[] reportSeql) {
        List<Integer> result = Arrays.stream(reportSeql).map(e -> metaTrashTypeRepository
                .UpdateTrashTypeDelYn(YnType.Y.getName(), e)).collect(Collectors.toList());
        return result.size();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public List<MetaTrashTypeVO> MetaTrashtypeSelectListService(MetaTrashTypeVO dto) {
        logger.info("-------------- 메타버스 쓰레기 유형 보기 시작 --------------------");
        //logger.info("ttmap - {}", tt);
        List<MetaTrashTypeVO> res = metaTrashTypeRepository.findAllByUseYnAndDelYn(dto.getUseYn(),dto.getDelYn())
                .stream().map(e -> modelMapper.map(e, MetaTrashTypeVO.class))
                .collect(Collectors.toList());

        logger.info(">> res : {}", new Gson().toJson(res));
        logger.info("-------------- 메타버스 쓰레기 유형 보기 종료 --------------------");

        return res;
    }




}
