package com.bucheon.clean.town.biz.keyword.v1.model;

import lombok.*;

import java.util.Date;

@Data
public class Stopword {
	
	private String word;

	private Date startDatetime;

	private Date endDatetime;

	private String scope;

	private String state;

}
