package com.bucheon.clean.town.biz.chatbot.uploadImage.v1.service;

import com.bucheon.clean.town.biz.chatbot.uploadImage.v1.model.UploadImageVO;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.chatbot.UploadImage;
import com.bucheon.clean.town.db.repository.chatbot.UploadImageRepository;
import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UploadImageService {

    private final Logger logger = LoggerFactory.getLogger(UploadImageService.class);
    private final HttpServletRequest httpServletRequest;
    private final UploadImageRepository uploadImageRepository;
    private final ModelMapper modelMapper;


    @Transactional(readOnly = true)
    public List<UploadImageVO> UploadImageSelectListService(UploadImageVO dto) {
        List<UploadImageVO> uploadImageVOList = uploadImageRepository.SelectListUploadImages(dto);

        return uploadImageVOList;
    }

    @Transactional(readOnly = true)
    public Paging UploadImageSelectListPagingInfoService(UploadImageVO dto) {
        QueryResults<UploadImageVO> result = uploadImageRepository.SelectListUploadImagesPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    @Transactional(readOnly = true)
    public String getImageIdFromDB () {

        String imageId = uploadImageRepository.seleteImageIdFromUploadimage();

        return imageId;
    }

    @Transactional
    public String UploadImageSaveService(UploadImageVO dto) throws IOException {

        int count = 0;
        int up = 0;

        logger.info(" imagedto - - - -{}", new Gson().toJson(dto));

        if (dto.getImageSeq() == null) {

            UploadImage uploadImage = uploadImageRepository.save(dto.ofMenu());
            if (uploadImage.getImageSeq() != null) {
                count = 1;
            }

        } else {
            if (!StringUtils.isEmptyOrWhitespace(dto.getImageId())) {
                up = uploadImageRepository.updateImageId(dto.getImageId(), dto.getImageSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getImageName())) {
                up = uploadImageRepository.updateImageName(dto.getImageName(), dto.getImageSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getImageFilename())) {
                up = uploadImageRepository.updateImageFilename(dto.getImageFilename(), dto.getImageSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getImageFileservername())) {
                up = uploadImageRepository.updateImageFileservername(dto.getImageFileservername(), dto.getImageSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getImageDefaultYn())) {
                up = uploadImageRepository.updateImageDefaultYn(dto.getImageDefaultYn(), dto.getImageSeq());
                count += up;
            }
            if (!StringUtils.isEmptyOrWhitespace(dto.getImageAttribute())) {
                up = uploadImageRepository.updateImageAttribute(dto.getImageAttribute(), dto.getImageSeq());
                count += up;
            }

        }

        if (count > 0) {
            return "success";
        } else {
            return "fail";
        }

    }

    public UploadImageVO UploadImageSelectOneService(Long seq) {
        UploadImageVO uploadImageVO = modelMapper.map(uploadImageRepository.findByImageSeq(seq)
                .orElseThrow(IllegalArgumentException::new), UploadImageVO.class);

        return uploadImageVO;
    }


    public int UploadImageDelService(Long imageSeql) {
        int result = uploadImageRepository.deleteByImageSeq(imageSeql);
        return result;
    }

}
