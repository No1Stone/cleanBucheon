package com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Component
public class MsgLogDAO {

    private final Logger logger = LoggerFactory.getLogger(MsgLogDAO.class);

    @Autowired
    EntityManager em;

    public int confirmIsTable (AlaramReq dto, String dbname){

         String qry = "SELECT EXISTS ( " +
                 "  SELECT 1 FROM Information_schema.tables " +
                 "  WHERE table_schema = '" + dbname + "' " +
                 "  AND table_name = 'MSG_LOG_" + dto.getYyyymm() + "' " +
                 ") AS flag " +
                 ";";
        logger.info("qry : {}", qry);

        //String sql = String.format(qry, yyyymm, sdate, edate);
        //logger.info("sql : {}", sql);

        int res = (int) em.createNativeQuery(qry).getResultList().get(0);
        logger.info("is Table : {}", res);

        return res;
    }

    @Transactional
    @Modifying
    public void makeTable (AlaramReq dto){

        String qry = "CREATE TABLE `MSG_LOG_" + dto.getYyyymm() + "` ( " +
                "`USERDATA` INT(11) NULL DEFAULT NULL, " +
                "`MSG_SEQ` INT(11) NOT NULL, " +
                "`CUR_STATE` INT(11) NOT NULL, " +
                "`SENT_DATE` DATETIME NULL DEFAULT NULL, " +
                "`RSLT_DATE` DATETIME NULL DEFAULT NULL, " +
                "`REPORT_DATE` DATETIME NULL DEFAULT NULL, " +
                "`REQ_DATE` DATETIME NOT NULL, " +
                "`RSLT_CODE` INT(11) NULL DEFAULT NULL, " +
                "`RSLT_CODE2` CHAR(1) NULL DEFAULT NULL COLLATE 'utf8_general_ci', " +
                "`RSLT_NET` CHAR(3) NULL DEFAULT NULL COLLATE 'utf8_general_ci', " +
                "`CALL_TO` VARCHAR(30) NULL DEFAULT NULL COLLATE 'utf8_general_ci', " +
                "`CALL_FROM` VARCHAR(30) NULL DEFAULT NULL COLLATE 'utf8_general_ci', " +
                "`SMS_TXT` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci', " +
                "`MSG_TYPE` INT(11) NOT NULL, " +
                "`MSG_PAY_CODE` VARCHAR(3) NULL DEFAULT NULL COLLATE 'utf8_general_ci', " +
                "`CONT_SEQ` INT(11) NULL DEFAULT NULL, " +
                "`MSG_TYPE_RESEND` INT(11) NULL DEFAULT NULL, " +
                "`CENTER_SEQ_RESEND` INT(11) NULL DEFAULT NULL, " +
                "`MSG_NOTICETALK_SENDER_KEY` VARCHAR(40) NULL DEFAULT NULL COLLATE 'utf8_general_ci', " +
                "`MSG_NOTICETALK_TMP_KEY` VARCHAR(30) NULL DEFAULT NULL COLLATE 'utf8_general_ci', " +
                "`MSG_RESEND_COUNT` INT(11) NOT NULL DEFAULT '0', " +
                "`MSG_RESENDDATE` DATETIME NULL DEFAULT NULL, " +
                "`SENT_DATE_PRE` DATETIME NULL DEFAULT NULL, " +
                "`RSLT_DATE_PRE` DATETIME NULL DEFAULT NULL, " +
                "`REPORT_DATE_PRE` DATETIME NULL DEFAULT NULL, " +
                "`RSLT_CODE_PRE` INT(11) NULL DEFAULT NULL, " +
                "`RSLT_CODE2_PRE` CHAR(1) NULL DEFAULT NULL COLLATE 'utf8_general_ci', " +
                "`RSLT_NET_PRE` VARCHAR(3) NULL DEFAULT NULL COLLATE 'utf8_general_ci', " +
                "INDEX `MSG_LOG_" + dto.getYyyymm() + "_MSG_PAY_CODE_IDX` (`MSG_PAY_CODE`) USING BTREE, " +
                "INDEX `MSG_LOG_" + dto.getYyyymm() + "_RSLT_CODE2_IDX` (`RSLT_CODE2`) USING BTREE " +
                ") " +
                "COLLATE='utf8_general_ci' " +
                "ENGINE=InnoDB " +
                ";";
        logger.info("qry : {}", qry);

        int res = em.createNativeQuery(qry).executeUpdate();

        logger.info("is Table : {}", res);
    }

    public List<MsgLogRes> selectListMsg (AlaramReq dto){

        if(dto.getSize() == 0){
            dto.setSize(10);
        }

        String s = "";
        if(dto.getSearchString() != null && !dto.getSearchString().equals("")){
            s = " Where CALL_TO like '%" + dto.getSearchString() + "%' ";
        }

        String qry = "WITH alram_log_tot as(  " +
                "SELECT   " +
                "   a.MSG_SEQ,  " +
                "   a.MSG_NOTICETALK_TMP_KEY,  " +
                "   a.SENT_DATE,  " +
                "   a.CALL_TO,  " +
                "   a.MSG_PAY_CODE,  " +
                "   a.RSLT_CODE2,  " +
                "   b.content  " +
                "FROM MSG_LOG_" + dto.getYyyymm() + " a  " +
                "   LEFT OUTER JOIN msg_log_result_data b ON  a.RSLT_CODE2 = cast(b.code as char)  " +
                "WHERE SENT_DATE BETWEEN STR_TO_DATE(CONCAT('" + dto.getSdate() + "',' 000000'), '%Y%m%d%H%i%s') AND STR_TO_DATE(concat('" + dto.getEdate() + "',' 235959'), '%Y%m%d%H%i%s')  " +
                "   and RSLT_CODE2 is not null  " +
                ")  " +
                "SELECT   " +
                "   MSG_SEQ,  " +
                "   MSG_NOTICETALK_TMP_KEY,  " +
                "   date_format(SENT_DATE, '%Y-%m-%d %H:%i:%s'),  " +
                "   CALL_TO,  " +
                "   if(MSG_PAY_CODE='NOT','알림톡','문자') AS send_type,  " +
                "   if(RSLT_CODE2='0','성공','실패') AS send_rslt,  " +
                "   RSLT_CODE2 AS rslt_code,  " +
                "   content AS rslt_text  " +
                "FROM alram_log_tot " +
                s +
                "ORDER BY SENT_DATE DESC " +
                "LIMIT " + (dto.getPage() * dto.getSize()) + ", " + dto.getSize() +
                ";";
        logger.info("qry : {}", qry);

        //String sql = String.format(qry, yyyymm, sdate, edate);
        //logger.info("sql : {}", sql);

        List<MsgLogRes> res = em.createNativeQuery(qry).getResultList();

        return res;
    }

    public List<Integer> selectListMsgPaging (AlaramReq dto){

        String s = "";
        if(dto.getSearchString() != null && !dto.getSearchString().equals("")){
            s = " Where CALL_TO like '%" + dto.getSearchString() + "%' ";
        }

        String qry = "WITH alram_log_tot as(  " +
                "SELECT   " +
                "   a.MSG_SEQ,  " +
                "   a.MSG_NOTICETALK_TMP_KEY,  " +
                "   a.SENT_DATE,  " +
                "   a.CALL_TO,  " +
                "   a.MSG_PAY_CODE,  " +
                "   a.RSLT_CODE2,  " +
                "   b.content  " +
                "FROM MSG_LOG_" + dto.getYyyymm() + " a  " +
                "   LEFT OUTER JOIN msg_log_result_data b ON  a.RSLT_CODE2 = cast(b.code as char)  " +
                "WHERE SENT_DATE BETWEEN STR_TO_DATE(CONCAT('" + dto.getSdate() + "',' 000000'), '%Y%m%d%H%i%s') AND STR_TO_DATE(concat('" + dto.getEdate() + "',' 235959'), '%Y%m%d%H%i%s')  " +
                "   and RSLT_CODE2 is not null  " +
                ")  " +
                "SELECT   " +
                "   Count(MSG_SEQ) as 'cnt'  " +
                "FROM alram_log_tot" +
                s +
                ";";
        logger.info("qry : {}", qry);

        //String sql = String.format(qry, yyyymm, sdate, edate);
        //logger.info("sql : {}", sql);

        List<Integer> res = em.createNativeQuery(qry).getResultList();

        return res;
    }

    ////액셀용
    public List<MsgLogRes> selectListMsgAll (AlaramReq dto){

        if(dto.getSize() == 0){
            dto.setSize(10);
        }

        String qry = "WITH alram_log_tot as(  " +
                "SELECT   " +
                "   a.MSG_SEQ,  " +
                "   a.MSG_NOTICETALK_TMP_KEY,  " +
                "   a.SENT_DATE,  " +
                "   a.CALL_TO,  " +
                "   a.MSG_PAY_CODE,  " +
                "   a.RSLT_CODE2,  " +
                "   b.content  " +
                "FROM MSG_LOG_" + dto.getYyyymm() + " a  " +
                "   LEFT OUTER JOIN msg_log_result_data b ON  a.RSLT_CODE2 = cast(b.code as char)  " +
                "WHERE SENT_DATE BETWEEN STR_TO_DATE(CONCAT('" + dto.getSdate() + "',' 000000'), '%Y%m%d%H%i%s') AND STR_TO_DATE(concat('" + dto.getEdate() + "',' 235959'), '%Y%m%d%H%i%s')  " +
                "   and RSLT_CODE2 is not null  " +
                ")  " +
                "SELECT   " +
                "   MSG_SEQ,  " +
                "   MSG_NOTICETALK_TMP_KEY,  " +
                "   date_format(SENT_DATE, '%Y-%m-%d %H:%i:%s'),  " +
                "   CALL_TO,  " +
                "   if(MSG_PAY_CODE='NOT','알림톡','문자') AS send_type,  " +
                "   if(RSLT_CODE2='0','성공','실패') AS send_rslt,  " +
                "   RSLT_CODE2 AS rslt_code,  " +
                "   content AS rslt_text  " +
                "FROM alram_log_tot " +
                "ORDER BY SENT_DATE DESC " +
                ";";
        logger.info("qry : {}", qry);

        //String sql = String.format(qry, yyyymm, sdate, edate);
        //logger.info("sql : {}", sql);

        List<MsgLogRes> res = em.createNativeQuery(qry).getResultList();

        return res;
    }

}
