package com.bucheon.clean.town.db.repository.chatbot.dsl;

import com.bucheon.clean.town.biz.chatbot.answer.v1.model.SimpleTextVO;
import com.bucheon.clean.town.db.entity.chatbot.QSkll;
import com.bucheon.clean.town.db.entity.chatbot.QSkllText;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

@Aspect
@RequiredArgsConstructor
public class SkllTextRepositoryDslImpl implements SkllTextRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QSkll qSkll = QSkll.skll;
    private QSkllText qSkllText = QSkllText.skllText;

    @Override
    public SimpleTextVO findBySkllStCodeAndRspnsOrdr(String code, int reqSeq) {

        SimpleTextVO result = queryFactory.select(
                        Projections.bean(SimpleTextVO.class,
                                qSkllText.skllCode.as("skillCode"),
                                qSkllText.text.as("text"),
                                qSkllText.btton.as("button"),
                                qSkllText.rspnsOrdr.as("resSeq")
                        )
                )
                .from(qSkllText)
                .where(eqId(code), eqRspnsOrder(reqSeq))
                .fetchOne();
        return result;

    }

    private BooleanExpression eqId(String id) {
        if(StringUtils.isEmptyOrWhitespace(id)){
            return null;
        }
        return qSkllText.skllCode.eq(String.valueOf(id));
    }

   private BooleanExpression eqRspnsOrder(int reqSeq) {
        if(reqSeq == 0){
            return null;
        }
        return qSkllText.rspnsOrdr.eq(reqSeq);
   }

}
