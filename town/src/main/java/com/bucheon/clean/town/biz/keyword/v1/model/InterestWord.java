package com.bucheon.clean.town.biz.keyword.v1.model;

import lombok.*;

import java.util.Date;

@Data
public class InterestWord {

	private String word1;

	private String word2;

    private String busiBranCode;

    private String busiBranCodeName;

    private Date startDatetime;

    private Date endDatetime;

    private String state;
}
