package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import lombok.Data;

@Data
public class ItemCardTmp {

	String thumbnail;
	String head;
	String profile;
	String imageTitle;
	String itemList;
	String title;
	String description;
	String buttons;
	String optionAt;

}
