package com.bucheon.clean.town.biz.metabus.metaBoard.v1.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MetaNoticePopupVO {

    private Long popupSeq;
    private String popupType;
    private String popupImageUrl;
    private String useYn;
    private Long regSeq;
    private LocalDateTime regDt;
    private Long modSeq;
    private LocalDateTime modDt;

}
