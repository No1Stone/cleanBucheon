package com.bucheon.clean.town.db.entity.environmental.band.id;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
public class TB_BandBoardListPotoId implements Serializable {

    private String postKey;
    private String photoKey;



}
