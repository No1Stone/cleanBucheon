package com.bucheon.clean.town.biz.metabus.metaBoard.v1.service;

import com.bucheon.clean.town.biz.metabus.metaBoard.v1.model.*;
import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.metabus.MetaBoard;
import com.bucheon.clean.town.db.entity.metabus.MetaBoardAtt;
import com.bucheon.clean.town.db.entity.metabus.MetaUserVoteHistory;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.repository.metabus.MetaBoardAttRepository;
import com.bucheon.clean.town.db.repository.metabus.MetaBoardRepository;
import com.bucheon.clean.town.db.repository.metabus.MetaUserVoteHistoryRepository;
import com.bucheon.clean.town.db.repository.operation.AdminRepository;
import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MetaBoardService {

    private final Logger logger = LoggerFactory.getLogger(MetaBoardService.class);
    private final HttpServletRequest httpServletRequest;
    private final MetaBoardRepository metaBoardRepository;
    private final MetaBoardAttRepository metaBoardAttRepository;
    private final MetaUserVoteHistoryRepository metaUserVoteHistoryRepository;
    private final AdminRepository adminRepository;
    private final ModelMapper modelMapper;

    ///////////////////////////////////////////////////////////////////////////////////////////
    public List<MetaBoardRes> MetaBoardSelectNowListService(MetaBoardReq dto) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        logger.info("dto  - - - -{}", new Gson().toJson(dto));
        Map<Long, String> tt = adminRepository.findAll()
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
        //logger.info("ttmap - {}", tt);
        //logger.info("getSessionAdministrativeDivision - {}", dto.getSessionAdministrativeDivision());
        List<MetaBoardRes> metaBoardRes = metaBoardRepository.SelectNowListMetaBoard(dto)
                .stream().map(e -> modelMapper.map(e, MetaBoardRes.class))
                .peek(f -> {
                    f.setBoardRegName(tt.get(f.getRegSeq()));
                    f.setBoardModName(tt.get(f.getModSeq()));
                    if(f.getBoardType().equals("A")){
                        f.setBoardTypeName("공감뉴스");
                    }
                    if(f.getBoardType().equals("B")){
                        f.setBoardTypeName("카드뉴스1");
                    }
                    if(f.getBoardType().equals("C")){
                        f.setBoardTypeName("카드뉴스2");
                    }
                    if(f.getBoardType().equals("D")){
                        f.setBoardTypeName("동영상");
                    }
                    //시작일 - 종료일
                    f.setSdateName(f.getSdate().format(dtf));
                    f.setEdateName(f.getEdate().format(dtf));
                    //첨부파일 갯수
                    f.setBoardAttCnt(metaBoardAttRepository.countByBoardSeq(f.getBoardSeq()));
                })
                .collect(Collectors.toList());
        return metaBoardRes;
    }

    public Paging MetaBoardSelectNowListPagingInfoService(MetaBoardReq dto) {
        QueryResults<MetaBoard> result = metaBoardRepository.SelectNowListMetaBoardPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////////////////////////
    public List<MetaBoardRes> MetaBoardSelectPastListService(MetaBoardReq dto) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        logger.info("dto  - - - -{}", new Gson().toJson(dto));
        Map<Long, String> tt = adminRepository.findAll()
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
        //logger.info("ttmap - {}", tt);
        //logger.info("getSessionAdministrativeDivision - {}", dto.getSessionAdministrativeDivision());
        List<MetaBoardRes> metaBoardRes = metaBoardRepository.SelectPastListMetaBoard(dto)
                .stream().map(e -> modelMapper.map(e, MetaBoardRes.class))
                .peek(f -> {
                    f.setBoardRegName(tt.get(f.getRegSeq()));
                    f.setBoardModName(tt.get(f.getModSeq()));
                    if(f.getBoardType().equals("A")){
                        f.setBoardTypeName("공감뉴스");
                    }
                    if(f.getBoardType().equals("B")){
                        f.setBoardTypeName("카드뉴스1");
                    }
                    if(f.getBoardType().equals("C")){
                        f.setBoardTypeName("카드뉴스2");
                    }
                    if(f.getBoardType().equals("D")){
                        f.setBoardTypeName("동영상");
                    }

                    //시작일 - 종료일
                    f.setSdateName(f.getSdate().format(dtf));
                    f.setEdateName(f.getEdate().format(dtf));
                    //첨부파일 갯수
                    f.setBoardAttCnt(metaBoardAttRepository.countByBoardSeq(f.getBoardSeq()));
                })
                .collect(Collectors.toList());
        return metaBoardRes;
    }

    public Paging MetaBoardSelectPastListPagingInfoService(MetaBoardReq dto) {
        QueryResults<MetaBoard> result = metaBoardRepository.SelectPastListMetaBoardPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////////////////////////
    public List<MetaBoardRes> MetaBoardSelectTocomeListService(MetaBoardReq dto) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        logger.info("dto  - - - -{}", new Gson().toJson(dto));
        Map<Long, String> tt = adminRepository.findAll()
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
        //logger.info("ttmap - {}", tt);
        //logger.info("getSessionAdministrativeDivision - {}", dto.getSessionAdministrativeDivision());
        List<MetaBoardRes> metaBoardRes = metaBoardRepository.SelectTocomeListMetaBoard(dto)
                .stream().map(e -> modelMapper.map(e, MetaBoardRes.class))
                .peek(f -> {
                    f.setBoardRegName(tt.get(f.getRegSeq()));
                    f.setBoardModName(tt.get(f.getModSeq()));
                    if(f.getBoardType().equals("A")){
                        f.setBoardTypeName("공감뉴스");
                    }
                    if(f.getBoardType().equals("B")){
                        f.setBoardTypeName("카드뉴스1");
                    }
                    if(f.getBoardType().equals("C")){
                        f.setBoardTypeName("카드뉴스2");
                    }
                    if(f.getBoardType().equals("D")){
                        f.setBoardTypeName("동영상");
                    }
                    //시작일 - 종료일
                    f.setSdateName(f.getSdate().format(dtf));
                    f.setEdateName(f.getEdate().format(dtf));
                    //첨부파일 갯수
                    f.setBoardAttCnt(metaBoardAttRepository.countByBoardSeq(f.getBoardSeq()));
                })
                .collect(Collectors.toList());
        return metaBoardRes;
    }

    public Paging MetaBoardSelectTocmoeListPagingInfoService(MetaBoardReq dto) {
        QueryResults<MetaBoard> result = metaBoardRepository.SelectTocomeListMetaBoardPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////////////////////////
    public List<MetaBoardRes> MetaBoardSelectListService(MetaBoardReq dto) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        logger.info("dto  - - - -{}", new Gson().toJson(dto));
        Map<Long, String> tt = adminRepository.findAll()
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
        //logger.info("ttmap - {}", tt);
        //logger.info("getSessionAdministrativeDivision - {}", dto.getSessionAdministrativeDivision());
        List<MetaBoardRes> metaBoardRes = metaBoardRepository.SelectListMetaBoard(dto)
                .stream().map(e -> modelMapper.map(e, MetaBoardRes.class))
                .peek(f -> {
                    f.setBoardRegName(tt.get(f.getRegSeq()));
                    f.setBoardModName(tt.get(f.getModSeq()));
                    if(f.getBoardType().equals("A")){
                        f.setBoardTypeName("공감뉴스");
                    }
                    if(f.getBoardType().equals("B")){
                        f.setBoardTypeName("카드뉴스1");
                    }
                    if(f.getBoardType().equals("C")){
                        f.setBoardTypeName("카드뉴스2");
                    }
                    if(f.getBoardType().equals("D")){
                        f.setBoardTypeName("동영상");
                    }
                    //시작일 - 종료일
                    f.setSdateName(f.getSdate().format(dtf));
                    f.setEdateName(f.getEdate().format(dtf));
                    //첨부파일 갯수
                    f.setBoardAttCnt(metaBoardAttRepository.countByBoardSeq(f.getBoardSeq()));
                })
                .collect(Collectors.toList());
        return metaBoardRes;
    }

    public Paging MetaBoardSelectListPagingInfoService(MetaBoardReq dto) {
        QueryResults<MetaBoard> result = metaBoardRepository.SelectListMetaBoardPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////


    public int UpdateMetaBoardDelService(Long[] metaBoardSeql) {
        List<Integer> result = Arrays.stream(metaBoardSeql).map(e -> metaBoardRepository
                .UpdateMetaBoardDel(YnType.Y.getName(), e)).collect(Collectors.toList());
        return result.size();
    }

    public int UpdateMetaBoardAttDelService(Long[] metaBoardSeql) {
        List<Integer> result = Arrays.stream(metaBoardSeql).map(e -> metaBoardAttRepository
                .deleteByBoardSeq(e)).collect(Collectors.toList());
        return result.size();
    }

    @Transactional
    public int UpdateMetaBoard(MetaBoardVO dto) {
        int result = 0;
        if(dto.getBoardSeq() != null) {
            metaBoardRepository.updateTitle(dto.getBoardTitle(), dto.getBoardSeq());
            metaBoardRepository.updateContent(dto.getBoardContent(), dto.getBoardSeq());
            metaBoardRepository.UpdateMetaSdate(dto.getSdate(), dto.getBoardSeq());
            metaBoardRepository.UpdateMetaEdate(dto.getEdate(), dto.getBoardSeq());
            metaBoardRepository.UpdateMetaBoardUse(dto.getUseYn(), dto.getBoardSeq());

            result++;
        }
        return result;
    }

    @Transactional
    public int UpdateMetaBoardAttOrder(Long boardAttSeq, int boardAttOrder) {
       return metaBoardAttRepository.updateBoardAttOrder(boardAttSeq, boardAttOrder);
    }

    public List<MetaBoardAtt> MetaBoardAttSelectListService(Long seq) {
        List<MetaBoardAtt> res = metaBoardAttRepository.findByBoardSeqAndBoardAttOrderGreaterThanEqualOrderByBoardAttOrderAsc(seq, 1);
        return res;
    }

    public MetaBoardAtt MetaBoardAttSelectFirstService(Long seq) {
        MetaBoardAtt res = metaBoardAttRepository.findByBoardSeqAndBoardAttOrder(seq, 0);
        return res;
    }

    public int MetaBoardAttSelectMaxOrderNumService(Long seq) {
        int res = 1;

        MetaBoardAtt m = metaBoardAttRepository.findFirstByBoardSeqOrderByBoardAttOrderDesc(seq);
        if (m != null) {
            res = m.getBoardAttOrder();
        }

        return res;
    }

    public MetaBoardRes MetaBoardSelectOneService(Long seq) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Map<Long, String> tt = adminRepository.findAll()
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
        logger.info(">>>>>>>>>>>>>>>>>>>>> tt : " + tt);

        MetaBoardRes result = modelMapper.map(metaBoardRepository.findByBoardSeqAndDelYn(seq, "N").orElseThrow(IllegalArgumentException::new), MetaBoardRes.class);
        result.setBoardRegName(tt.get(result.getRegSeq()));
        result.setBoardModName(tt.get(result.getModSeq()));
        //시작일 - 종료일
        result.setSdateName(result.getSdate().format(dtf));
        result.setEdateName(result.getEdate().format(dtf));

        if (metaBoardAttRepository.existsByBoardSeq(result.getBoardSeq())) {
            result.setBoardAttList(metaBoardAttRepository.findByBoardSeqOrderByBoardAttOrderAsc(result.getBoardSeq()).stream().map(e -> e.getBoardAtt()).collect(Collectors.toList()));
        }

        if (metaBoardAttRepository.existsByBoardSeq(result.getBoardSeq())) {
            result.setBoardAttThumb(metaBoardAttRepository.findByBoardSeqAndBoardAttOrder(result.getBoardSeq(), 0));
        }

        if (metaBoardAttRepository.existsByBoardSeq(result.getBoardSeq())) {
            result.setMetaBoardAtts(metaBoardAttRepository.findByBoardSeqAndBoardAttOrderGreaterThanEqualOrderByBoardAttOrderAsc(result.getBoardSeq(), 1));
        }
//                .peek(f -> {
//                    f.setReportTypeName(tt.get(f.getReportType()));
//                    f.setReportStatusName(ProcessType.valueOf(f.getReportStatus()).getDescription());
//                    f.setReportReceptionName(ReceptionType.valueOf(f.getReportReception()).getDescription());
//                })
        try {
            //result.setReportUserName(Masking.nameMasking(reportResult.getReportUserName()));
        } catch (Exception e) {
            //아무것도 안함
        }


        return result;
    }


    public MetaBoard MetaBoardSaveService(MetaBoardVO dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;

        if(dto.getBoardSeq() == null){
            dto.setRegSeq(admin.getAdminSeq());
        }
        if(dto.getDelYn() == null){
            dto.setDelYn("N");
        }
        dto.setModSeq(admin.getAdminSeq());

        MetaBoard metaBoard = metaBoardRepository.save(dto.ofMetaBoard());

        return metaBoard;
    }

    public void MetaBoardAttSaveService(MetaBoardAttVO dto) {
        metaBoardAttRepository.save(dto.ofMetaBoardAtt());
    }

    //첨부파일 전체 지우기
    public void MetaBoardAttDelService(MetaBoardAttVO dto) {
        metaBoardAttRepository.deleteByBoardSeq(dto.getBoardSeq());
    }

    public int MetaBoardCountOneService(Long seq) {
        return  metaBoardRepository.countByBoardSeqAndDelYn(seq,"N");
    }

    public int countTodayVote(MetaUserVoteHistoryVO dto) {

        int todayVote = metaUserVoteHistoryRepository.countByUserIdAndBoardSeqAndRegDt(dto.getUserId(), dto.getBoardSeq(), LocalDate.now());

        return todayVote;
    }

    public int UpdateMetaVote(MetaUserVoteHistoryVO dto) {

        int cnt=0;

        int todayVote = countTodayVote(dto);

        if(todayVote == 0) {

            if(dto.getVoteType().equals("A")) {
                cnt = metaBoardRepository.updateVote1(dto.getBoardSeq());
            } else if(dto.getVoteType().equals("B")) {
                cnt = metaBoardRepository.updateVote2(dto.getBoardSeq());
            } else if(dto.getVoteType().equals("C")) {
                cnt = metaBoardRepository.updateVote3(dto.getBoardSeq());
            }
            MetaUserVoteHistory m = new MetaUserVoteHistory();
            m.setUserId(dto.getUserId());
            m.setVoteType(dto.getVoteType());
            m.setBoardType(dto.getBoardType());
            m.setBoardSeq(dto.getBoardSeq());
            m.setRegDt(LocalDateTime.now());
            metaUserVoteHistoryRepository.save(m);
        }

        return cnt;
    }

    //첨부파일 한개 지우기
    public int DeleteAtt(Long boardAttSeq) {

        int cnt = metaBoardAttRepository.deleteByBoardAttSeq(boardAttSeq);

        return cnt;
    }

    //카드 웹 페이지
    public MetaBoardRes MetaBoardSelectTypeOneService(String type) {

        LocalDateTime ss = LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.of(0,0,0));
        LocalDateTime es = LocalDateTime.of(LocalDate.now(), LocalTime.of(23,59,59));

        MetaBoardRes result =
                modelMapper.map(metaBoardRepository.selectType(type).orElseThrow(IllegalArgumentException::new), MetaBoardRes.class);

        if (metaBoardAttRepository.existsByBoardSeq(result.getBoardSeq())) {
            result.setBoardAttList(metaBoardAttRepository.findByBoardSeqOrderByBoardAttOrderAsc(result.getBoardSeq()).stream().map(e -> e.getBoardAtt()).collect(Collectors.toList()));
        }

        if (metaBoardAttRepository.existsByBoardSeq(result.getBoardSeq())) {
            result.setBoardAttThumb(metaBoardAttRepository.findByBoardSeqAndBoardAttOrder(result.getBoardSeq(), 0));
        }

        if (metaBoardAttRepository.existsByBoardSeq(result.getBoardSeq())) {
            result.setMetaBoardAtts(metaBoardAttRepository.findByBoardSeqAndBoardAttOrderGreaterThanEqualOrderByBoardAttOrderAsc(result.getBoardSeq(), 1));
        }


        return result;
    }

    //게기 시간 확인
    public Long ConfirmTermService(MetaBoardVO dto) {
        return metaBoardRepository.confirmType(dto);
    }

}
