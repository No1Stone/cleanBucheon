package com.bucheon.clean.town.db.entity.environmental;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_report")
@Getter
@Setter
@NoArgsConstructor
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "report_seq", nullable = true)
    private Long reportSeq;
    @Column(name = "reception_num", nullable = true)
    private String receptionNum;
    @Column(name = "report_content", nullable = true)
    private String reportContent;
    @Column(name = "report_type", nullable = true)
    private Long reportType;
    @Column(name = "report_reception", nullable = true)
    private String reportReception;
    @Column(name = "report_location_x", nullable = true)
    private String reportLocationX;
    @Column(name = "report_location_y", nullable = true)
    private String reportLocationY;
    @Column(name = "report_addr_new", nullable = true)
    private String reportAddrNew;
    @Column(name = "report_addr_old", nullable = true)
    private String reportAddrOld;
    @Column(name = "report_status", nullable = true)
    private String reportStatus;
    @Column(name = "report_status_reject_reason", nullable = true)
    private String reportStatusRejectReason;
    @Column(name = "report_user_name", nullable = true)
    private String reportUserName;
    @Column(name = "report_user_phone", nullable = true)
    private String reportUserPhone;
    @Column(name = "report_user_email", nullable = true)
    private String reportUserEmail;
    @Column(name = "del_yn", nullable = true)
    private String delYn;
    @Column(name = "reg_user_id", nullable = true)
    private String regUserId;
    @Column(name = "com_dt", nullable = true)
    private LocalDateTime comDt;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "band_key", nullable = true)
    private String bandKey;
    @Column(name = "source_pk", nullable = true)
    private String sourcePk;
    @Column(name = "area_name", nullable = true)
    private String areaName;
    @Column(name = "report_point", nullable = true)
    private String reportPoint;
    @Column(name = "report_point_reject_reason", nullable = true)
    private String reportPointRejectReason;
    @Column(name = "report_point_complete_yn", nullable = true)
    private String reportPointCompleteYn;

    @Builder
    Report(Long reportSeq,
           String receptionNum,
           String reportContent,
           Long reportType,
           String reportReception,
           String reportLocationX,
           String reportLocationY,
           String reportAddrNew,
           String reportAddrOld,
           String reportStatus,
           String reportStatusRejectReason,
           String reportUserName,
           String reportUserPhone,
           String reportUserEmail,
           String delYn,
           String regUserId,
           String bandKey,
           String sourcePk,
           String areaName,
           String reportPoint,
           String reportPointRejectReason,
           String reportPointCompleteYn,
           LocalDateTime comDt,
           LocalDateTime regDt,
           LocalDateTime modDt
    ) {
        this.reportSeq = reportSeq;
        this.receptionNum = receptionNum;
        this.reportContent = reportContent;
        this.reportType = reportType;
        this.reportReception = reportReception;
        this.reportLocationX = reportLocationX;
        this.reportLocationY = reportLocationY;
        this.reportAddrNew = reportAddrNew;
        this.reportAddrOld = reportAddrOld;
        this.reportStatus = reportStatus;
        this.reportStatusRejectReason = reportStatusRejectReason;
        this.reportUserName = reportUserName;
        this.reportUserPhone = reportUserPhone;
        this.reportUserEmail = reportUserEmail;
        this.delYn = delYn;
        this.regUserId = regUserId;
        this.bandKey = bandKey;
        this.sourcePk = sourcePk;
        this.reportPoint = reportPoint;
        this.reportPointRejectReason = reportPointRejectReason;
        this.reportPointCompleteYn = reportPointCompleteYn;
        this.areaName = areaName;
        this.comDt = comDt;
        this.regDt = regDt;
        this.modDt = modDt;

    }


}
