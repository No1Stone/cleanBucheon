package com.bucheon.clean.town.biz.environmental.report.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReportUpdate {
    private Long reportSeq;
    private String reportStatus;
    //처리불가사유
    private String reportStatusRejectReason;
    private String comDts;
    //ci
    private String ci;
    //지급여부
    private String reportPointCompleteYn;
    //지급포인트
    private String reportPoint;
    //지급불가사유
    private String reportPointRejectReason;
    //등록자 식별
    private String regUserId;
    //User DID 정보
    private String did;
    private String reportReception;
    //ci
    private Long[] dels;

    private LocalDateTime comDt;
    private LocalDateTime modDt;
}
