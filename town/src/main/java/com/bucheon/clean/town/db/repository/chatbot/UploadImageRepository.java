package com.bucheon.clean.town.db.repository.chatbot;

import com.bucheon.clean.town.db.entity.chatbot.UploadImage;
import com.bucheon.clean.town.db.repository.chatbot.dsl.UploadImageRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface UploadImageRepository extends JpaRepository<UploadImage, Long>, UploadImageRepositoryDsl {

    Optional<UploadImage> findByImageSeq(Long seq);

    @Transactional
    @Modifying
    int deleteByImageSeq(Long seq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_ccrs_uploadimage set image_id = :imageId where image_seq =:imageSeq "
            , nativeQuery = true
    )
    int updateImageId(@Param("imageId") String imageId, @Param("imageSeq") Long imageSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_ccrs_uploadimage set image_name = :imageName where image_seq =:imageSeq "
            , nativeQuery = true
    )
    int updateImageName(@Param("imageName") String imageId, @Param("imageSeq") Long imageSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_ccrs_uploadimage set image_filename = :imageFilename where image_seq =:imageSeq "
            , nativeQuery = true
    )
    int updateImageFilename(@Param("imageFilename") String imageFilename, @Param("imageSeq") Long imageSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_ccrs_uploadimage set image_fileservername = :imageFileservername where image_seq =:imageSeq "
            , nativeQuery = true
    )
    int updateImageFileservername(@Param("imageFileservername") String imageFileservername, @Param("imageSeq") Long imageSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_ccrs_uploadimage set image_default_yn = :imageDefaultYn where image_seq =:imageSeq "
            , nativeQuery = true
    )
    int updateImageDefaultYn(@Param("imageDefaultYn") String imageDefaultYn, @Param("imageSeq") Long imageSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_ccrs_uploadimage set image_attribute = :imageAttribute where image_seq =:imageSeq "
            , nativeQuery = true
    )
    int updateImageAttribute(@Param("imageAttribute") String imageAttribute, @Param("imageSeq") Long imageSeq);

    @Transactional(readOnly = true)
    @Query(
            value = "SELECT CONCAT('IMG_',LPAD(NVL(MAX(IMAGE_SEQ)+1, 1), 6, 0)) FROM tb_ccrs_uploadimage "
            , nativeQuery = true
    )
    String seleteImageIdFromUploadimage();

}
