package com.bucheon.clean.town.biz.metabus.metaUser.v1.controller;

import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.*;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.service.MetaUserPointHistoryService;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.service.MetaUserService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.db.entity.metabus.MetaUser;
import com.bucheon.clean.town.db.entity.operation.Admin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(path = "/metaBus/user")
@RequiredArgsConstructor
public class MetaUserController {

    private final Logger logger = LoggerFactory.getLogger(MetaUserController.class);
    private final MetaUserService metaUserService;
    private final MetaUserPointHistoryService metaUserPointHistoryService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/metaBus/user/info");
    }

    //기본틀
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(MetaUserReq dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(27L);

        return new ModelAndView("metaBus/user/userManage")
                .addObject("userPaging", metaUserService.MetaUserSelectListPagingService(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(MetaUserReq dto){
        return new ModelAndView("metaBus/user/userListAjax")
                .addObject("userList", metaUserService.MetaUserSelectListService(dto))
                .addObject("userPaging", metaUserService.MetaUserSelectListPagingService(dto))
                ;
    }


    //상세보기 팝업
    @RequestMapping(path = "/viewInfo", method = RequestMethod.POST)
    public ModelAndView viewInfo(String userId) throws Exception {

        MetaUser um = new MetaUser();
        List<MetaUserPointHistoryRes> umhList = null;


        um = metaUserService.MetaUserSelectOneService(userId);

        try {
            MetaUserPointHistoryReq dto = new MetaUserPointHistoryReq();
            dto.setUserId(userId);
            umhList = metaUserPointHistoryService.MyMetaUserPointHistoryRecentSelectListService(dto);
        } catch (Exception e) {
            //
        }

        return new ModelAndView("metaBus/user/userManageViewAjax")
                .addObject("um", um)
                .addObject("umhList", umhList)
                .addObject("metaUserOne", metaUserService.MetaUserSelectOneService(userId))
                ;
    }

    //랭킹보기 팝업
    @RequestMapping(path = "/rankingInfo", method = RequestMethod.POST)
    public ModelAndView rankingInfo(MetaRankingReq dto) throws Exception {

        LocalDate sdate1 = LocalDate.now();
        try {
            if(dto.getYyyymm() != null) {
                sdate1 = LocalDate.parse(dto.getSdate());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String stateString = sdate1 + "";

        if(dto.getRankingPage() == 0){
            dto.setRankingPage(1);
        }
        if(dto.getRankingSize() == 0){
            dto.setRankingSize(10);
        }
        if (dto.getYyyymm() == null) {
            dto.setYyyymm(stateString.substring(0,7));
        }

        List<MetaAllRankingRes> rankingList = null;

        try {
            rankingList = metaUserService.MetaRankingSelectListService(dto);
        } catch (Exception e) {
            //
        }

        return new ModelAndView("metaBus/user/rankingManageViewAjax")
                .addObject("sdate", sdate1)
                .addObject("rankingPage", dto.getRankingPage())
                .addObject("rankingSize", dto.getRankingSize())
                .addObject("yyyymm", dto.getYyyymm())
                .addObject("rankingList",rankingList)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/rankingListAjax", method = RequestMethod.POST)
    public ModelAndView rankingListAjax(MetaRankingReq dto){

        List<MetaAllRankingRes> rankingList = null;

        try {
            rankingList = metaUserService.MetaRankingSelectListService(dto);
        } catch (Exception e) {
            //
        }

        return new ModelAndView("metaBus/user/rankingListAjax")
                .addObject("rankingList",rankingList)
                ;
    }

    //접속불가처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(String[] userId){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result",   metaUserService.UpdateMetaUserDelService(userId));
        return map;
    }

    //접속가능처리
    @RequestMapping(path = "/upInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object upInfo(String userId){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result",   metaUserService.UpdateMetaUserUpService(userId));
        return map;
    }
}