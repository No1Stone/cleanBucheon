package com.bucheon.clean.town.db.repository.operation.dsl;

import com.bucheon.clean.town.biz.operation.personalInfo.v1.model.PersonalInfoLogReq;
import com.bucheon.clean.town.biz.operation.personalInfo.v1.model.PersonalInfoLogVO;
import com.bucheon.clean.town.biz.operation.point.v1.model.TrashTypeReq;
import com.bucheon.clean.town.biz.operation.user.v1.model.UserReq;
import com.bucheon.clean.town.db.entity.operation.PersonalInfoLog;
import com.bucheon.clean.town.db.entity.operation.TrashType;
import com.bucheon.clean.town.db.entity.operation.User;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PersonalInfoLogRepositoryDsl {

    @Transactional(readOnly = true)
    List<PersonalInfoLog> PersonalInfoLogSelectList(PersonalInfoLogReq dto);
    @Transactional(readOnly = true)
    QueryResults<PersonalInfoLog> PersonalInfoLogSelectListPaging(PersonalInfoLogReq dto);

}
