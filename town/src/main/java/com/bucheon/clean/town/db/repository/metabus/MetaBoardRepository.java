package com.bucheon.clean.town.db.repository.metabus;

import com.bucheon.clean.town.db.entity.metabus.MetaBoard;
import com.bucheon.clean.town.db.entity.metabus.MetaUser;
import com.bucheon.clean.town.db.repository.environmental.dsl.ReportRepositoryDsl;
import com.bucheon.clean.town.db.repository.metabus.dsl.MetaBoardRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

public interface MetaBoardRepository extends JpaRepository<MetaBoard, String>, MetaBoardRepositoryDsl {

    @Transactional(readOnly = true)
    Optional<MetaBoard> findByBoardSeqAndDelYn(Long seq, String s);
    @Transactional(readOnly = true)
    int countByBoardSeqAndDelYn(Long seq, String s);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_board set board_title = :boardTitle where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int updateTitle(@Param("boardTitle") String boardTitle, @Param("boardSeq") Long boardSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_board set board_content = :boardContent where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int updateContent(@Param("boardContent") String boardContent, @Param("boardSeq") Long boardSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_board set sdate = :sdate where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int UpdateMetaSdate(@Param("sdate") LocalDateTime sdate, @Param("boardSeq") Long boardSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_board set edate = :edate where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int UpdateMetaEdate(@Param("edate") LocalDateTime edate, @Param("boardSeq") Long boardSeq);



    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_board set del_yn = :delYn where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int UpdateMetaBoardDel(@Param("delYn") String delYn, @Param("boardSeq") Long boardSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_board set use_yn = :useYn where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int UpdateMetaBoardUse(@Param("useYn") String useYn, @Param("boardSeq") Long boardSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_board set vote1 = vote1 + 1 where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int updateVote1(@Param("boardSeq") Long boardSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_board set vote2 = vote2 + 1 where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int updateVote2(@Param("boardSeq") Long boardSeq);

    @Transactional
    @Modifying
    @Query(
            value = "update tb_meta_board set vote3 = vote3 + 1 where board_seq =:boardSeq "
            , nativeQuery = true
    )
    int updateVote3(@Param("boardSeq") Long boardSeq);

}
