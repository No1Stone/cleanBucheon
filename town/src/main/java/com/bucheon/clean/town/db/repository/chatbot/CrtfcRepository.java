package com.bucheon.clean.town.db.repository.chatbot;

import com.bucheon.clean.town.db.entity.chatbot.Crtfc;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CrtfcRepository extends JpaRepository<Crtfc, Long> {

    Optional<Crtfc> findByUserId(String userId);
    Optional<Crtfc> findByMobileNo(String phone);

}
