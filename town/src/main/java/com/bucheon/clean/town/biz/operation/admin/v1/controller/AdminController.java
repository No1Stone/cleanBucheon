package com.bucheon.clean.town.biz.operation.admin.v1.controller;

import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminReq;
import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminRes;
import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminUpdate;
import com.bucheon.clean.town.biz.operation.admin.v1.service.AdminService;
import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminReq;
import com.bucheon.clean.town.biz.operation.admin.v1.service.AdminService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.permission.v1.model.PermissionReq;
import com.bucheon.clean.town.biz.operation.permission.v1.model.PermissionRes;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.repository.operation.PermissionRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(path = "/operation/admin")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AdminController {

    private final Logger logger = LoggerFactory.getLogger(AdminController.class);
    private final AdminService adminService;
    private final PermissionService permissionService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        return new ModelAndView("redirect:/operation/admin/adminManage");
    }

    //기본틀
    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public ModelAndView info(AdminReq dto, HttpServletRequest request) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        MenuRes menu = menuService.MenuSelectOneService(12L);

        return new ModelAndView("operation/admin/adminManage")
                .addObject("adminPaging",adminService.AdminSelectListPagingService(dto))
                .addObject("adminInfo",admin)
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(AdminReq dto){
        return new ModelAndView("operation/admin/adminListAjax")
                .addObject("adminList",adminService.AdminSelectListService(dto))
                .addObject("adminPaging",adminService.AdminSelectListPagingService(dto))
                ;
    }

    //최초 입력 페이지 팝업 
    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm(){

        return new ModelAndView("operation/admin/adminManagePopupAjax")
                .addObject("adminOne", new AdminRes())
                .addObject("permiList", permissionService.PermissionListSelectService(new PermissionReq()))
                ;
    }

    //수정 입력 페이지 팝업
    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView modifyForm(Long seq){
        return new ModelAndView("operation/admin/adminManagePopupAjax")
                .addObject("adminOne", adminService.AdminSelectOneService(seq))
                .addObject("permiList", permissionService.PermissionListSelectService(new PermissionReq()))
                ;
    }

    //중복 아이디 검색
    @RequestMapping(value="/checkAdminId", method=RequestMethod.POST)
    @ResponseBody
    public Object checkAdminId(AdminReq dto) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("idCheck", adminService.idcheck(dto.getAdminId()));
        return map;
    }

    //저장처리
    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(AdminUpdate dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", adminService.AdminUpdateAndSave(dto));
        return map;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(AdminUpdate dto){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result",   adminService.AdminUpdateAndSave(dto));
        return map;
    }

    //삭제처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(Long[] adminSeq){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result",   adminService.UpdateAdminDelService(adminSeq));
        return map;
    }

}
