package com.bucheon.clean.town.db.entity.chatbot;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_ccrs_uploadimage")
@Getter
@Setter
@NoArgsConstructor
public class UploadImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "image_seq", nullable = true )
    private Long imageSeq;
    @Column(name = "image_id", nullable = true)
    private String imageId;
    @Column(name = "image_name", nullable = true)
    private String imageName;
    @Column(name = "image_filename", nullable = true)
    private String imageFilename;
    @Column(name = "image_fileservername", nullable = true)
    private String imageFileservername;
    @Column(name = "image_default_yn", nullable = true)
    private String imageDefaultYn;
    @Column(name = "image_attribute", nullable = true)
    private String imageAttribute;

    @Builder
    UploadImage(
            Long imageSeq,
            String imageId,
            String imageName,
            String imageFilename,
            String imageFileservername,
            String imageDefaultYn,
            String imageAttribute
    ) {
        this.imageSeq = imageSeq;
        this.imageId = imageId;
        this.imageName = imageName;
        this.imageFilename = imageFilename;
        this.imageFileservername = imageFileservername;
        this.imageDefaultYn = imageDefaultYn;
        this.imageAttribute = imageAttribute;
    }
}
