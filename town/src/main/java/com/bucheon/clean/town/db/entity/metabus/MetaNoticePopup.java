package com.bucheon.clean.town.db.entity.metabus;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_meta_notice_popup")
@Getter
@Setter
@NoArgsConstructor
public class MetaNoticePopup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "popup_seq", nullable = true)
    private Long popupSeq;
    @Column(name = "popup_type", nullable = true)
    private String popupType;
    @Column(name = "popup_image_url", nullable = true)
    private String popupImageUrl;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_seq", nullable = true)
    private Long regSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_seq", nullable = true)
    private Long modSeq;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;



    @Builder
    MetaNoticePopup(
            Long popupSeq,
            String popupType,
            String popupImageUrl,
            String useYn,
            Long regSeq,
            LocalDateTime regDt,
            Long modSeq,
            LocalDateTime modDt
            ) {
        this.popupSeq = popupSeq;
        this.popupType = popupType;
        this.popupImageUrl = popupImageUrl;
        this.useYn = useYn;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.modSeq = modSeq;
        this.modDt = modDt;
    }

}
