package com.bucheon.clean.town.biz.operation.user.v1.service;

import com.bucheon.clean.town.biz.environmental.report.v1.model.ReportListRes;
import com.bucheon.clean.town.biz.operation.user.v1.model.UserReq;
import com.bucheon.clean.town.common.util.Masking;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.operation.User;
import com.bucheon.clean.town.db.repository.operation.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);
    private final HttpServletRequest httpServletRequest;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public Object UserSelectListService(UserReq dto){
        var result = userRepository.UserListSelect(dto).stream()
                .map(e -> modelMapper.map(e, User.class))
                .peek(f -> {
                    try {
                        f.setName(Masking.nameMasking(f.getName()));
                        f.setPhone(Masking.phoneMasking(f.getPhone()));
                        f.setEmail(Masking.emailMasking(f.getEmail()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                })
                .collect(Collectors.toList());
        return result;
    }

    public Object UserSelectListPagingService(UserReq dto) {
        var result = userRepository.UserListSelectPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public User UserSelectOneService(String did) throws Exception {
        Optional<User> user = userRepository.findByDid(did);
        user.get().setName(Masking.nameMasking(user.get().getName()));
        user.get().setDid(Masking.didMasking(user.get().getDid()));
        user.get().setPhone(Masking.phoneMasking(user.get().getPhone()));
        user.get().setEmail(Masking.emailMasking(user.get().getEmail()));
        return user.orElseThrow(() -> new IllegalArgumentException());
    }

    public User UserSelectOneService4(String mid) throws Exception {
        Optional<User> user = userRepository.findByMetaUserId(mid);
        return user.orElseThrow(() -> new IllegalArgumentException());
    }

    public User UserSelectOneService2(String chatbotUserId) {
        return userRepository.findByChatbotUserId(chatbotUserId).orElseThrow(() -> new IllegalArgumentException());
    }

    public User UserSelectOneService3(String did) throws Exception {
        Optional<User> user = userRepository.findByDid(did);
        return user.orElseThrow(() -> new IllegalArgumentException());
    }

    public int UsercountMetaUserId(String mid){
        return userRepository.countByMetaUserIdAndMetaUserIdNotNull(mid);
    }


}
