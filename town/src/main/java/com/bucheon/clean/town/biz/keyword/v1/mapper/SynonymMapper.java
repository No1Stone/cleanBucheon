package com.bucheon.clean.town.biz.keyword.v1.mapper;

import com.bucheon.clean.town.biz.keyword.v1.model.InterestWord;
import com.bucheon.clean.town.biz.keyword.v1.model.Synonym;
import com.bucheon.clean.town.biz.keyword.v1.model.WordReq;
import com.bucheon.clean.town.common.util.Paging;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SynonymMapper {

    public Paging getTotalSize(WordReq dto);

    public List<Synonym> getList(WordReq dto);

    public Synonym getOne(Synonym dto);
    
    public Integer save(Synonym synonym);
    
    public Integer update(Synonym synonym);
    
    public Integer delete(Synonym synonym);

}
