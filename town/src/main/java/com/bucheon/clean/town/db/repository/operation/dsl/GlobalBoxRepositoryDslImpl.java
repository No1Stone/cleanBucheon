package com.bucheon.clean.town.db.repository.operation.dsl;

import com.bucheon.clean.town.biz.operation.board.v1.model.GlobalBoxReq;
import com.bucheon.clean.town.db.entity.operation.GlobalBox;
import com.bucheon.clean.town.db.entity.operation.QAdmin;
import com.bucheon.clean.town.db.entity.operation.QGlobalBox;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class GlobalBoxRepositoryDslImpl implements GlobalBoxRepositoryDsl {


    private final JPAQueryFactory queryFactory;
    private QGlobalBox qGlobalBox = QGlobalBox.globalBox;
    private final Logger logger = LoggerFactory.getLogger(GlobalBoxRepositoryDslImpl.class);

    @Override
    public List<GlobalBox> SelectListGlobalBox(GlobalBoxReq dto) {

        if(dto.getSize() == 0){
            dto.setSize(10);
        }


        var aa = queryFactory.select(qGlobalBox).from(qGlobalBox)
                .where(
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        qGlobalBox.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .orderBy(qGlobalBox.regDt.desc())
                .fetch();
        return aa;
    }

    @Override
    public QueryResults<GlobalBox> SelectListGlobalBoxPaging(GlobalBoxReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        var aa = queryFactory.select(qGlobalBox).from(qGlobalBox)
                .where(
                        eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        qGlobalBox.delYn.eq("N")
                )
                .limit(dto.getSize())
                .offset(dto.getPage()*dto.getSize())
                .fetchResults();

        return aa;
    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchsearchSelect(String searchSelect, String searchString) {

        try {

            if (searchSelect.equals("searchTitle")) {
                return eqGlobalBoxTitle(searchString);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

   private BooleanExpression eqGlobalBoxTitle(String s) {
        if (StringUtils.isEmptyOrWhitespace(s)) {
            return null;
        }
        return qGlobalBox.boardTitle.toLowerCase().contains(s);
    }

}
