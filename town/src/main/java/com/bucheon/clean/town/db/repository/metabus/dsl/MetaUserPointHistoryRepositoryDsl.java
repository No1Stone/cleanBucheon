package com.bucheon.clean.town.db.repository.metabus.dsl;


import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.MetaUserPointHistoryReq;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.MetaUserPointHistoryRes;
import com.bucheon.clean.town.biz.operation.mileage.v1.model.UserMileageHistoryVO;
import com.bucheon.clean.town.db.entity.metabus.MetaUserPointHistory;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MetaUserPointHistoryRepositoryDsl {

    @Transactional(readOnly = true)
    List<MetaUserPointHistory> SelectListMyMetaUserPointHistory(MetaUserPointHistoryReq dto);
    @Transactional(readOnly = true)
    QueryResults<MetaUserPointHistory> SelectListMyMetaUserPointHistoryPaging(MetaUserPointHistoryReq dto);
    @Transactional(readOnly = true)
    List<MetaUserPointHistory> SelectListMyMetaUserPointHistoryAll(MetaUserPointHistoryReq dto);

    @Transactional(readOnly = true)
    List<MetaUserPointHistoryRes> MetaUserPointHistorySelectList(MetaUserPointHistoryReq dto);
    @Transactional(readOnly = true)
    QueryResults<MetaUserPointHistoryRes> MetaUserPointHistorySelectListPaging(MetaUserPointHistoryReq dto);

}
