package com.bucheon.clean.town.biz.keyword.v1.service.stopWord;

import com.bucheon.clean.town.biz.keyword.v1.mapper.StopwordMapper;
import com.bucheon.clean.town.biz.keyword.v1.model.Stopword;
import com.bucheon.clean.town.biz.keyword.v1.model.WordReq;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class StopwordServiceImpl implements StopwordService {
	
	private final StopwordMapper stopwordMapper;

	public Paging getPage(WordReq dto) {
		Paging pa = stopwordMapper.getTotalSize(dto);
		pa.setTotalPage((pa.getTotalSize() % dto.getSize() > 0) ? pa.getTotalSize() / dto.getSize() + 1 : pa.getTotalSize() / dto.getSize());
		pa.setSize(dto.getSize());
		pa.setPage(dto.getPage());
		return pa;
	}

	public List<Stopword> getList(WordReq dto) {
		List<Stopword> result = stopwordMapper.getList(dto);
		return result;
	}

	public Stopword getOne(String word) {
		Stopword result = stopwordMapper.getOne(word);
		return result;
	}

	public String save(Stopword stopword) {
		int res = stopwordMapper.save(stopword);
		return res > 0 ? "success" : "fail";
	}

	public int update(Stopword stopword) {
		return stopwordMapper.update(stopword);
	}

	public int delete(String[] words) {
		int result = 0;

		for(String word : words){
			result = result + stopwordMapper.delete(word);
		}

		return result;
	}
	
	
	  
}
