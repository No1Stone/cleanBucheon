package com.bucheon.clean.town.biz.operation.point.v1.model;

import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.db.entity.operation.TrashType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TrashTypeUpdate {

    private Long trashSeq;
    private String trashName;
    private int declarationMileage;
    private int processMileage;
    private String iconUrl;
    private String useYn;
    private Long[] delYn;
    private Long regSeq;
    private LocalDateTime regDt;
    private Long modSeq;
    private LocalDateTime modDt;

    public TrashType ofTrashType(String fileUrl){
        TrashType tt = new TrashType();
        tt.setTrashName(this.trashName);
        tt.setDeclarationMileage(this.declarationMileage);
        tt.setProcessMileage(this.processMileage);
        tt.setIconUrl(fileUrl);
        tt.setUseYn(YnType.Y.getName());
        tt.setDelYn(YnType.N.getName());
        tt.setRegSeq(this.regSeq);
        tt.setRegDt(LocalDateTime.now());
        tt.setModSeq(this.modSeq);
        tt.setModDt(LocalDateTime.now());
    return tt;
    }
}
