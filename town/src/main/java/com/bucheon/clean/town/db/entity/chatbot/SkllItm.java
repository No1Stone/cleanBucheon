package com.bucheon.clean.town.db.entity.chatbot;

import com.bucheon.clean.town.db.entity.chatbot.id.SkllItmId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_skll_itm")
@Getter
@Setter
@NoArgsConstructor
@IdClass(SkllItmId.class)
public class SkllItm {
    @Id
    @Column(name = "skll_code", nullable = true)
    private String skllCode;
    @Column(name = "thumb", nullable = true)
    private String thumb;
    @Column(name = "head", nullable = true)
    private String head;
    @Column(name = "image_sj", nullable = true)
    private String imageSj;
    @Column(name = "itm_list", nullable = true)
    private String itmList;
    @Column(name = "sj", nullable = true)
    private String sj;
    @Column(name = "dc", nullable = true)
    private String dc;
    @Column(name = "btton", nullable = true)
    private String btton;
    @Id
    @Column(name = "itm_ordr", nullable = true)
    private String itmOrdr;
    @Id
    @Column(name = "rspns_ordr", nullable = true)
    private String rspnsOrdr;
    @Column(name = "btton_optn_at", nullable = true)
    private String bttonOptnAt;
    @Column(name = "profl_crtfc_at", nullable = true)
    private String proflCrtfcAt;
    @Column(name = "seoultalk_btton", nullable = true)
    private String seoultalkBtton;

    @Builder
    SkllItm(
            String skllCode,
            String thumb,
            String head,
            String imageSj,
            String itmList,
            String sj,
            String dc,
            String btton,
            String itmOrdr,
            String rspnsOrdr,
            String bttonOptnAt,
            String proflCrtfcAt,
            String seoultalkBtton
    ) {
        this.skllCode = skllCode;
        this.thumb = thumb;
        this.head = head;
        this.imageSj = imageSj;
        this.itmList = itmList;
        this.sj = sj;
        this.dc = dc;
        this.btton = btton;
        this.itmOrdr = itmOrdr;
        this.rspnsOrdr = rspnsOrdr;
        this.bttonOptnAt = bttonOptnAt;
        this.proflCrtfcAt = proflCrtfcAt;
        this.seoultalkBtton = seoultalkBtton;
    }

}
