package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import lombok.Data;

@Data
public class CommerceCardThumbnailsLink {

	private String web;

}
