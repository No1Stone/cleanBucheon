package com.bucheon.clean.town.common.type;

public enum YnType {
    Y("Y","사용"),
    N("N","미사용");

    private String code;
    private String val;

    YnType(String code, String val){
        this.code = code;
        this.val = val;
    }

    public String getCode(){
        return this.code;
    }
    public String getName(){
        return name();
    }
    public String getVal(){ return this.val;}


}
