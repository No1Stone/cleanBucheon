package com.bucheon.clean.town.db.repository.operation;

import com.bucheon.clean.town.db.entity.operation.PermissionMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PermissionMenuRepository extends JpaRepository<PermissionMenu, Long> {

    List<PermissionMenu> findByPermissionSeqAndUseYn(Long perSeq, String use);

    @Transactional
    @Modifying
    @Query(
            value = "delete from tb_permission_menu where permission_seq =:permissionSeq "
            , nativeQuery = true
    )
    int deleteBermissionSeq(@Param("permissionSeq") Long permissionSeq);

}
