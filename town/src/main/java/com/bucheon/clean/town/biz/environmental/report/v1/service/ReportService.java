package com.bucheon.clean.town.biz.environmental.report.v1.service;

import com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model.AlramSendVO;
import com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.service.AlaramtalkService;
import com.bucheon.clean.town.biz.environmental.complaint.v1.model.ComplaintsReq;
import com.bucheon.clean.town.biz.environmental.report.v1.model.*;
import com.bucheon.clean.town.biz.operation.bandDamdang.v1.service.BandDamdangService;
import com.bucheon.clean.town.biz.operation.mileage.v1.model.UserMileageHistoryVO;
import com.bucheon.clean.town.biz.operation.mileage.v1.service.UserMileageService;
import com.bucheon.clean.town.biz.util.model.ConnectSumLogVO;
import com.bucheon.clean.town.biz.util.service.ConnectSumService;
import com.bucheon.clean.town.common.type.PaymentType;
import com.bucheon.clean.town.common.type.ProcessType;
import com.bucheon.clean.town.common.type.ReceptionType;
import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.common.util.EncDecUtil;
import com.bucheon.clean.town.common.util.ExcelWriter;
import com.bucheon.clean.town.common.util.Masking;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.chatbot.Crtfc;
import com.bucheon.clean.town.db.entity.environmental.Report;
import com.bucheon.clean.town.db.entity.environmental.ReportLog;
import com.bucheon.clean.town.db.entity.environmental.band.TB_BandBoardListComment;
import com.bucheon.clean.town.db.entity.etc.DongBandDamdang;
import com.bucheon.clean.town.db.entity.etc.DongName;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.TrashType;
import com.bucheon.clean.town.db.entity.operation.User;
import com.bucheon.clean.town.db.repository.chatbot.CrtfcRepository;
import com.bucheon.clean.town.db.repository.environmental.PetitionRepository;
import com.bucheon.clean.town.db.repository.environmental.ReportImgRepository;
import com.bucheon.clean.town.db.repository.environmental.ReportLogRepository;
import com.bucheon.clean.town.db.repository.environmental.ReportRepository;
import com.bucheon.clean.town.db.repository.environmental.band.TB_BandBoardListCommentRepository;
import com.bucheon.clean.town.db.repository.etc.DongNameRepository;
import com.bucheon.clean.town.db.repository.operation.TrashTypeRepository;
import com.bucheon.clean.town.db.repository.operation.UserRepository;
import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ReportService {

    private final Logger logger = LoggerFactory.getLogger(ReportService.class);
    private final HttpServletRequest httpServletRequest;
    private final ReportRepository reportRepository;
    private final ReportLogRepository reportLogRepository;
    private final ReportImgRepository reportImgRepository;
    private final ModelMapper modelMapper;
    private final TrashTypeRepository trashTypeRepository;
    private final PetitionRepository petitionRepository;
    private final UserMileageService userMileageService;
    private final DongNameRepository dongNameRepository;
    private final AlaramtalkService alaramtalkService;
    private final BandDamdangService bandDamdangService;
    private final CrtfcRepository crtfcRepository;
    private final UserRepository userRepository;
    private final ConnectSumService connectSumService;
    private final TB_BandBoardListCommentRepository tB_BandBoardListCommentRepository;

    @Value("${chatbot.fromPhone}")
    String fromPhone;




    public List<ReportListRes> ReportSelectListService(ReportListReq dto) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        logger.info("dto  - - - -{}", new Gson().toJson(dto));
        Map<Long, String> tt = trashTypeRepository.findAll()
                .stream().collect(Collectors.toMap(TrashType::getTrashSeq, TrashType::getTrashName));
        logger.info("ttmap - {}", tt);
        //logger.info("getSessionAdministrativeDivision - {}", dto.getSessionAdministrativeDivision());
        List<ReportListRes> reportsRes = reportRepository.SelectListReport(dto)
                .stream().map(e -> modelMapper.map(e, ReportListRes.class))
                .peek(f -> {
                    try {
                        f.setReportUserName(Masking.nameMasking(f.getReportUserName()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    f.setReportTypeName(tt.get(f.getReportType()));
                    f.setReportStatusName(ProcessType.valueOf(f.getReportStatus()).getDescription());
                    f.setReportReceptionName(ReceptionType.valueOf(f.getReportReception()).getDescription());
                    if (f.getComDt() != null) {
                        f.setComDts(f.getComDt().format(dtf));
                    }
                    if (f.getRegDt() != null) {
                        f.setRegDts(f.getRegDt().format(dtf));
                    }
                    if(f.getReportPointCompleteYn() == null){f.setReportPointPaymentName(PaymentType.W.getDescription());}
                    else if(f.getReportPointCompleteYn().equals(PaymentType.W.getName())){f.setReportPointPaymentName(PaymentType.W.getDescription());}
                    else if(f.getReportPointCompleteYn().equals(PaymentType.C.getName())){f.setReportPointPaymentName(PaymentType.C.getDescription());}
                    else if(f.getReportPointCompleteYn().equals(PaymentType.R.getName())){f.setReportPointPaymentName(PaymentType.R.getDescription());}
                })
                .collect(Collectors.toList());
        return reportsRes;
    }

    public Paging ReportSelectListPagingInfoService(ReportListReq dto) {
        QueryResults<Report> result = reportRepository.SelectListReportPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public ReportListRes ReportSelectOneService(Long seq) {
        Map<Long, String> tt = trashTypeRepository.findAll()
                .stream().collect(Collectors.toMap(TrashType::getTrashSeq, TrashType::getTrashName));
        Map<Long, Integer> tt2 = trashTypeRepository.findAll()
                .stream().collect(Collectors.toMap(TrashType::getTrashSeq, TrashType::getDeclarationMileage));
        Map<Long, Integer> tt3 = trashTypeRepository.findAll()
                .stream().collect(Collectors.toMap(TrashType::getTrashSeq, TrashType::getProcessMileage));
        logger.info(">>>>>>>>>>>>>>>>>>>>> tt : " + tt);

        ReportListRes reportResult = modelMapper.map(reportRepository.findByReportSeqAndDelYnOrderByRegDtDesc(seq, "N").orElseThrow(IllegalArgumentException::new), ReportListRes.class);
        reportResult.setReportTypeName(tt.get(reportResult.getReportType()));
        reportResult.setDeclarationMileage(tt2.get(reportResult.getReportType()));
        reportResult.setProcessMileage(tt3.get(reportResult.getReportType()));
        reportResult.setReportStatusName(ProcessType.valueOf(reportResult.getReportStatus()).getDescription());
        reportResult.setReportReceptionName(ReceptionType.valueOf(reportResult.getReportReception()).getDescription());
        if (reportImgRepository.existsByReportSeq(reportResult.getReportSeq())) {
            reportResult.setReportImgs(reportImgRepository
                    .findByReportSeq(reportResult.getReportSeq()).stream().map(e -> e.getReportImg()).collect(Collectors.toList()));
        }
//                .peek(f -> {
//                    f.setReportTypeName(tt.get(f.getReportType()));
//                    f.setReportStatusName(ProcessType.valueOf(f.getReportStatus()).getDescription());
//                    f.setReportReceptionName(ReceptionType.valueOf(f.getReportReception()).getDescription());
//                })
        try {
            reportResult.setReportUserName(Masking.nameMasking(reportResult.getReportUserName()));
            reportResult.setReportUserPhone(Masking.phoneMasking(reportResult.getReportUserPhone()));
            reportResult.setReportUserEmail(Masking.emailMasking(reportResult.getReportUserEmail()));
        } catch (Exception e) {
            //아무것도 안함
        }


        return reportResult;
    }

    //2는 마스킹 안하는 데이터
    public ReportListRes ReportSelectOneService2(Long seq) {
        Map<Long, String> tt = trashTypeRepository.findAll()
                .stream().collect(Collectors.toMap(TrashType::getTrashSeq, TrashType::getTrashName));
        Map<Long, Integer> tt2 = trashTypeRepository.findAll()
                .stream().collect(Collectors.toMap(TrashType::getTrashSeq, TrashType::getDeclarationMileage));
        Map<Long, Integer> tt3 = trashTypeRepository.findAll()
                .stream().collect(Collectors.toMap(TrashType::getTrashSeq, TrashType::getProcessMileage));
        logger.info(">>>>>>>>>>>>>>>>>>>>> tt : " + tt);

        ReportListRes reportResult = modelMapper.map(reportRepository.findByReportSeqAndDelYnOrderByRegDtDesc(seq, "N").orElseThrow(IllegalArgumentException::new), ReportListRes.class);
        reportResult.setReportTypeName(tt.get(reportResult.getReportType()));
        reportResult.setDeclarationMileage(tt2.get(reportResult.getReportType()));
        reportResult.setProcessMileage(tt3.get(reportResult.getReportType()));
        reportResult.setReportStatusName(ProcessType.valueOf(reportResult.getReportStatus()).getDescription());
        reportResult.setReportReceptionName(ReceptionType.valueOf(reportResult.getReportReception()).getDescription());
        if (reportImgRepository.existsByReportSeq(reportResult.getReportSeq())) {
            reportResult.setReportImgs(reportImgRepository
                    .findByReportSeq(reportResult.getReportSeq()).stream().map(e -> e.getReportImg()).collect(Collectors.toList()));
        }
//                .peek(f -> {
//                    f.setReportTypeName(tt.get(f.getReportType()));
//                    f.setReportStatusName(ProcessType.valueOf(f.getReportStatus()).getDescription());
//                    f.setReportReceptionName(ReceptionType.valueOf(f.getReportReception()).getDescription());
//                })
        return reportResult;
    }

    public List<ReportLog> ReportLogSelectService(Long Seq) {
        return reportLogRepository.findByReportSeqOrderByLogSeqDesc(Seq);
    }

    public Report ReportSaveService(ReportSave dto) {

        LocalDateTime regDt = LocalDateTime.now();
        LocalDateTime modDt = LocalDateTime.now();
        String receptionNum = String.valueOf(Timestamp.valueOf(LocalDateTime.now()).getTime());
        dto.setRegDt(regDt);
        dto.setModDt(modDt);
        dto.setReceptionNum(receptionNum);

        //법정동에서 행정동 변경
        //Optional<DongName> dongName = dongNameRepository.findByName(dto.getBname());
        //dto.setAreaName(dongName.get().getAdmin());

        Report report = reportRepository.save(dto.ofReport());

        //########### 밴드 등록 생략, 알림톡 생략략
        //String sendCon = "[관리자 전화접수]" +
        //"\n" +
        // dto.getReportContent();
        //bandService.SendBandPost("", sendCon);

        //로그 저장
        String msg = "";
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;
        if(report.getReportStatus().equals(ProcessType.D.getName())){
            msg = "접수하였습니다";
        } else if(dto.getReportStatus().equals(ProcessType.C.getName())){
            msg = "담당자 " + admin.getAdminName() + "님께서 처리완료로 변경하였습니다.";
        } else if(dto.getReportStatus().equals(ProcessType.P.getName())){
            msg = "담당자 " + admin.getAdminName() + "님께서 처리중으로 변경하였습니다.";
        } else if(dto.getReportStatus().equals(ProcessType.R.getName())){
            msg = "담당자 " + admin.getAdminName() + "님께서 처리불가로 변경하였습니다.";
        }

        //로그 저장
        ReportLog r = new ReportLog();
        r.setReportSeq(report.getReportSeq());
        r.setLogTime(LocalDateTime.now());
        r.setReceptionNum(receptionNum);
        r.setReportStatus(report.getReportStatus());
        r.setReportDamdang(0L);
        r.setMsg("접수되었습니다.");
        ReportLog reportLogResult = reportLogRepository.save(r);

        /////// 커넥션 카운팅 추가
        try {
            ConnectSumLogVO connectSumLogVO = new ConnectSumLogVO();
            connectSumLogVO.setConnectDate(LocalDate.now());
            connectSumLogVO.setUserId(receptionNum);
            connectSumLogVO.setConnectType("V");
            connectSumService.ConnectCount(connectSumLogVO);

        } catch (Exception ee) {
            logger.info("일일 카운팅 방문/전화 오류남");
        }

        return report;
    }

    @Transactional
    public int UpdateReport(ReportUpdate dto) {

        //로그 저장때문에 필요
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;
        String msg = "";

        Report report = reportRepository.findByReportSeq(dto.getReportSeq());

        int result = 0;
        LocalDateTime comDt = LocalDateTime.now();
        LocalDateTime modDt = LocalDateTime.now();
        dto.setModDt(modDt);
        dto.setComDt(comDt);

        try {
            //내용만 수정
            if (dto.getReportStatus() != null && !report.getReportStatus().equals(ProcessType.C.getName()) && !report.getReportStatus().equals(ProcessType.R.getName())) {
                //원래 내용 수정
                if (!dto.getReportStatus().equals("") && dto.getReportStatus() != null) {
                    reportRepository.UpdateReportStatus(dto.getReportStatus(), dto.getReportSeq());
                    report.setReportStatus(dto.getReportStatus());
                    //처리불가면 마일리지도 지급불가 처리
                    if (dto.getReportStatus().equals(ProcessType.R.getName())) {
                        reportRepository.UpdateReportPointCompleteYn(PaymentType.R.getName(), dto.getReportSeq());
                        report.setReportPointCompleteYn(PaymentType.R.getName());
                    }
                }
                if (!dto.getReportStatusRejectReason().equals("") && dto.getReportStatusRejectReason() != null) {
                    reportRepository.UpdateReportStatusRejectReason(dto.getReportStatusRejectReason(), dto.getReportSeq());
                    report.setReportStatusRejectReason(dto.getReportStatusRejectReason());
                }
                if (dto.getReportStatus().equals(ProcessType.C.getName()) || dto.getReportStatus().equals(ProcessType.R.getName())) {
                    reportRepository.UpdateComDt(dto.getComDt(), dto.getReportSeq());
                    report.setComDt(comDt);
                }

                //수정일은 무조건 변경
                reportRepository.UpdateModDt(dto.getModDt(), dto.getReportSeq());
                report.setModDt(modDt);




                //로그 저장
                if (dto.getReportStatus().equals(ProcessType.D.getName())) {
                    msg = "접수하였습니다";
                } else if (dto.getReportStatus().equals(ProcessType.C.getName())) {
                    msg = "담당자 " + admin.getAdminName() + "님께서 처리완료로 변경하였습니다.";
                } else if (dto.getReportStatus().equals(ProcessType.P.getName())) {
                    msg = "담당자 " + admin.getAdminName() + "님께서 처리중으로 변경하였습니다.";
                } else if (dto.getReportStatus().equals(ProcessType.R.getName())) {
                    msg = "담당자 " + admin.getAdminName() + "님께서 처리불가로 변경하였습니다.";
                }

                ReportLog r = new ReportLog();
                r.setReportSeq(dto.getReportSeq());
                r.setLogTime(LocalDateTime.now());
                r.setReceptionNum(report.getReceptionNum());
                r.setReportStatus(dto.getReportStatus());
                r.setReportDamdang(admin.getAdminSeq());
                r.setMsg(msg);
                r.setReportPointRejectReason(dto.getReportPointRejectReason());

                ReportLog reportLogResult = reportLogRepository.save(r);

                result++;



                //챗봇-처리중일때만 알림톡 발송
                logger.info("확인해 보아요 1 : {}", dto.getReportReception());
                logger.info("확인해 보아요 2 : {}", dto.getReportStatus());
                if(report.getReportReception().equals(ReceptionType.C.getName()) && dto.getReportStatus().equals(ProcessType.P.getName())){

                    String formatDate = report.getModDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));
                    DongBandDamdang dongBandDamdang = bandDamdangService.BandDamdangSelectOneService(report.getAreaName()).orElseThrow(IllegalArgumentException::new);

                    AlramSendVO alramSendVO = new AlramSendVO();
                    List<String> arrDc = new ArrayList<>();
                    List<String> arrBtn = new ArrayList<>();
                    arrDc.add(report.getReceptionNum());
                    arrDc.add(formatDate);
                    arrDc.add("처리중");
                    arrDc.add(report.getAreaName());
                    arrDc.add(dongBandDamdang.getDamdangPhone());
                    arrBtn.add(EncDecUtil.encrypt(report.getRegUserId()));

                    alramSendVO.setToPhone(report.getReportUserPhone());
                    alramSendVO.setFromPhone(fromPhone);
                    alramSendVO.setAlramId("SC_002");
                    alramSendVO.setSysCode("SC");
                    alramSendVO.setArrDc(arrDc);
                    alramSendVO.setArrBtn(arrBtn);
                    logger.info("-- alramSendVO : {}", new Gson().toJson(alramSendVO));

                    try {
                        String s = alaramtalkService.AlramSendService(alramSendVO);
                        logger.info("s : {}", s);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    logger.info("챗봇 : 처리중 : {}, {}", report.getReportReception().equals(ReceptionType.C.getName()) , report.getReportStatus().equals(ProcessType.P.getName()));
                }

                //챗봇-완료일때만 알림톡 발송
                if(report.getReportReception().equals(ReceptionType.C.getName()) && dto.getReportStatus().equals(ProcessType.C.getName())){

                    String formatDate = report.getRegDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));
                    String formatDate2 = report.getComDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));
                    DongBandDamdang dongBandDamdang = bandDamdangService.BandDamdangSelectOneService(report.getAreaName()).orElseThrow(IllegalArgumentException::new);


                    AlramSendVO alramSendVO = new AlramSendVO();
                    List<String> arrDc = new ArrayList<>();
                    List<String> arrBtn = new ArrayList<>();
                    arrDc.add(report.getReceptionNum());
                    arrDc.add(formatDate);
                    arrDc.add("처리완료");
                    arrDc.add(formatDate2);
                    arrDc.add(report.getAreaName());
                    arrDc.add(dongBandDamdang.getDamdangPhone());
                    arrBtn.add(EncDecUtil.encrypt(report.getRegUserId()));

                    alramSendVO.setToPhone(report.getReportUserPhone());
                    alramSendVO.setFromPhone(fromPhone);
                    alramSendVO.setAlramId("SC_003");
                    alramSendVO.setSysCode("SC");
                    alramSendVO.setArrDc(arrDc);
                    alramSendVO.setArrBtn(arrBtn);
                    logger.info("-- alramSendVO : {}", new Gson().toJson(alramSendVO));

                    try {
                        String s = alaramtalkService.AlramSendService(alramSendVO);
                        logger.info("s : {}", s);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                //챗봇-처리불가일때만 알림톡 발송
                if(report.getReportReception().equals(ReceptionType.C.getName()) && dto.getReportStatus().equals(ProcessType.R.getName())){

                    String formatDate = report.getComDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));
                    DongBandDamdang dongBandDamdang = bandDamdangService.BandDamdangSelectOneService(report.getAreaName()).orElseThrow(IllegalArgumentException::new);

                    AlramSendVO alramSendVO = new AlramSendVO();
                    List<String> arrDc = new ArrayList<>();
                    List<String> arrBtn = new ArrayList<>();
                    arrDc.add(report.getReceptionNum());
                    arrDc.add(formatDate);
                    arrDc.add("처리불가");
                    arrDc.add(report.getAreaName());
                    arrDc.add(dongBandDamdang.getDamdangPhone());
                    arrDc.add(report.getReportStatusRejectReason());
                    arrBtn.add(EncDecUtil.encrypt(report.getRegUserId()));

                    alramSendVO.setToPhone(report.getReportUserPhone());
                    alramSendVO.setFromPhone(fromPhone);
                    alramSendVO.setAlramId("SC_004");
                    alramSendVO.setSysCode("SC");
                    alramSendVO.setArrDc(arrDc);
                    alramSendVO.setArrBtn(arrBtn);
                    logger.info("-- alramSendVO : {}", new Gson().toJson(alramSendVO));

                    try {
                        String s = alaramtalkService.AlramSendService(alramSendVO);
                        logger.info("s : {}", s);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        try {
            //마일리지만 수정
            if (
                    (report.getReportReception().equals(ReceptionType.C.getName())) &&
                            (report.getReportStatus().equals(ProcessType.C.getName()) || report.getReportStatus().equals(ProcessType.R.getName())) &&
                            (report.getReportPointCompleteYn().equals(PaymentType.W.getName()))
            ) {
                if (dto.getReportPoint() == null) {
                    dto.setReportPoint("0");
                }
                ReportListRes reportListRes = this.ReportSelectOneService(dto.getReportSeq());
                reportRepository.UpdateReportPoint(Integer.valueOf(dto.getReportPoint()), dto.getReportSeq());
                report.setReportPoint(dto.getReportPoint());
                reportRepository.UpdateReportPointRejectReason(dto.getReportPointRejectReason(), dto.getReportSeq());
                report.setReportPointRejectReason(dto.getReportPointRejectReason());
                reportRepository.UpdateReportPointCompleteYn(dto.getReportPointCompleteYn(), dto.getReportSeq());
                report.setReportPointCompleteYn(dto.getReportPointCompleteYn());

                //수정일은 무조건 변경
                reportRepository.UpdateModDt(dto.getModDt(), dto.getReportSeq());
                report.setModDt(dto.getModDt());


                //추가 마일리지 적용 업데이트
                if (dto.getDid() != null && !dto.getDid().equals("")) {

                    String reason = (dto.getReportPointCompleteYn().equals("C")) ? "챗봇불편접수 지급" : dto.getReportPointRejectReason();

                    UserMileageHistoryVO userMileageHistoryVO = new UserMileageHistoryVO();
                    userMileageHistoryVO.setUserId(dto.getDid());
                    userMileageHistoryVO.setReportSeq(dto.getReportSeq());
                    userMileageHistoryVO.setMileage(Integer.valueOf(dto.getReportPoint()));
                    userMileageHistoryVO.setMileageType("chatbot");
                    userMileageHistoryVO.setReason(reason);
                    userMileageHistoryVO.setComplaintsDt(report.getComDt());
                    userMileageHistoryVO.setSendDate(LocalDateTime.now());

                    userMileageService.UserMileageSaveService(userMileageHistoryVO);

                    //로그 저장
                    if (dto.getReportPointCompleteYn().equals(PaymentType.W.getName())) {
                        msg = "담당자 " + admin.getAdminName() + "님께서 현재상태를 저장하였습니다";
                    } else if (dto.getReportPointCompleteYn().equals(PaymentType.C.getName())) {
                        msg = "담당자 " + admin.getAdminName() + "님께서 마일리지를 지급하였습니다.";
                    } else if (dto.getReportPointCompleteYn().equals(ProcessType.R.getName())) {
                        msg = "담당자 " + admin.getAdminName() + "님께서 마일리지를 지급불가로 변경하였습니다.";
                    }

                    int cnt = reportLogRepository.countByReportSeq(dto.getReportSeq());
                    String rn = String.valueOf(Timestamp.valueOf(LocalDateTime.now()).getTime());
                    if (cnt > 0) {
                        ReportLog reportLog = reportLogRepository.findTop1ByReportSeqOrderByLogSeqDesc(dto.getReportSeq()).orElseThrow(IllegalArgumentException::new);
                        rn = reportLog.getReceptionNum();
                    }
                    ReportLog r = new ReportLog();
                    r.setReportSeq(dto.getReportSeq());
                    r.setLogTime(LocalDateTime.now());
                    r.setReceptionNum(rn);
                    r.setReportStatus(reportListRes.getReportStatus());
                    r.setReportDamdang(admin.getAdminSeq());
                    r.setMsg(msg);
                    r.setReportPointRejectReason(dto.getReportPointRejectReason());

                    ReportLog reportLogResult = reportLogRepository.save(r);

                    //챗봇-마일리지 지급완료일때만 알림톡 발송
                    /*
                    //2022-06-30 추윤지 과장이 김도현 주무관에게 확인 결과 마일리지 알림은 시티패스앱에서 발송된다고 프로세스 삭제 요청
                    if(dto.getReportReception().equals(ReceptionType.C.getName())
                            && dto.getReportStatus().equals(ProcessType.C.getName())
                            && dto.getReportPointCompleteYn().equals(PaymentType.C.getName())){

                        String formatDate = report.getModDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));
                        DongBandDamdang dongBandDamdang = bandDamdangService.BandDamdangSelectOneService(report.getAreaName()).orElseThrow(IllegalArgumentException::new);

                        AlramSendVO alramSendVO = new AlramSendVO();
                        List<String> arrDc = new ArrayList<>();
                        List<String> arrBtn = new ArrayList<>();
                        arrDc.add(report.getReceptionNum());
                        arrDc.add(formatDate);
                        arrDc.add("지급완료");
                        arrDc.add(report.getReportPoint());
                        arrDc.add(report.getAreaName());
                        arrDc.add(dongBandDamdang.getDamdangPhone());
                        arrBtn.add(EncDecUtil.encrypt(report.getRegUserId()));

                        alramSendVO.setToPhone(report.getReportUserPhone());
                        alramSendVO.setFromPhone(fromPhone);
                        alramSendVO.setAlramId("SC_005");
                        alramSendVO.setSysCode("SC");
                        alramSendVO.setArrDc(arrDc);
                        alramSendVO.setArrBtn(arrBtn);

                        try {
                            alaramtalkService.AlramSendService(alramSendVO);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    */

                    //챗봇-마일리지 지급불가일때만 알림톡 발송
                    /*
                    //2022-06-30 추윤지 과장이 김도현 주무관에게 확인 결과 마일리지 알림은 시티패스앱에서 발송된다고 프로세스 삭제 요청
                    if(dto.getReportReception().equals(ReceptionType.C.getName())
                            && dto.getReportStatus().equals(ProcessType.C.getName())
                            && dto.getReportPointCompleteYn().equals(PaymentType.R.getName())){

                        String formatDate = report.getModDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));
                        DongBandDamdang dongBandDamdang = bandDamdangService.BandDamdangSelectOneService(report.getAreaName()).orElseThrow(IllegalArgumentException::new);

                        AlramSendVO alramSendVO = new AlramSendVO();
                        List<String> arrDc = new ArrayList<>();
                        List<String> arrBtn = new ArrayList<>();
                        arrDc.add(report.getReceptionNum());
                        arrDc.add(formatDate);
                        arrDc.add("지급불가");
                        arrDc.add(report.getAreaName());
                        arrDc.add(dongBandDamdang.getDamdangPhone());
                        arrDc.add(report.getReportPointRejectReason());
                        arrBtn.add(EncDecUtil.encrypt(report.getRegUserId()));

                        alramSendVO.setToPhone(report.getReportUserPhone());
                        alramSendVO.setFromPhone(fromPhone);
                        alramSendVO.setAlramId("SC_006");
                        alramSendVO.setSysCode("SC");
                        alramSendVO.setArrDc(arrDc);
                        alramSendVO.setArrBtn(arrBtn);

                        try {
                            alaramtalkService.AlramSendService(alramSendVO);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    */

                }

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }

    public void ReportImgSaveService(ReportImgVo dto) {
        reportImgRepository.save(dto.ofReportImg());
    }

    public int UpdateReportCompleteService(Long[] reportSeql) {

        List<Integer> result = Arrays.stream(reportSeql).map(
                e -> this.UpdateAllComplete(e)).collect(Collectors.toList());
        return result.size();
    }

    public int UpdateAllComplete(Long seq) {

        Report report = reportRepository.findByReportSeq(seq);

        ReportUpdate dto = new ReportUpdate();
        dto.setReportSeq(report.getReportSeq());
        dto.setReportStatus(ProcessType.C.getName());
        dto.setReportStatusRejectReason("");

        logger.info(">>>>>>>> dto.setReportSeq(seq) : {}", dto.getReportSeq());

        return UpdateReport(dto);
    }

    public int UpdateReportDelService(Long[] reportSeql) {
        List<Integer> result = Arrays.stream(reportSeql).map(e -> reportRepository
                .UpdateReportDel(YnType.Y.getName(), e)).collect(Collectors.toList());
        return result.size();
    }

    public List<TrashType> SelectTrashType() {
        return trashTypeRepository.findAllByUseYnAndDelYn("Y", "N");
    }

    public List<ReportExcelDto> getExcelList(ReportListReq dto) {

        List<ReportExcelDto> getExcelList = reportRepository.findExcelList(dto);

        logger.info("getExcelList - {} ", getExcelList);

        return getExcelList;
    }

    //엑셀 다운로드 요청
    public Map<String, Object> downExcelList(ReportListReq dto) throws Exception {
        // 데이터 가져오기
        List<ReportExcelDto> excelList = this.getExcelList(dto);

        logger.info("excelList - {} ", excelList);

        String s = "";
        if(dto.getPageType().equals("C")){ s = "챗봇"; }
        else if(dto.getPageType().equals("B")){ s = "네이버밴드"; }
        else if(dto.getPageType().equals("P")){ s = "전화"; }
        else if(dto.getPageType().equals("V")){ s = "방문"; }

        return ExcelWriter.createExcelData(excelList, ReportExcelDto.class, s);
    }

    //댓글 가져오기


    public List<TB_BandBoardListComment> SelectReportComment(String postKey){
        return tB_BandBoardListCommentRepository.findByPostKey(postKey);

    }

    //////////////////////////////////////////////////////////////////////////////////////
    // 메인 숫자 카운트
    // 챗봇 신규 접수
    public int getChatBotNew(LocalDate sdate2, LocalDate edate2) {
        return reportRepository.countChatbotToday(sdate2, edate2);
    }

    // 챗봇 미처리 건수
    public int getChatbotP(LocalDate sdate2, LocalDate edate2) {
        return reportRepository.countChatbotP(sdate2, edate2);
    }

    // 챗봇 미처리 건수
    public int getChatbotP2() {
        return reportRepository.countChatbotP2();
    }

    // 챗봇 완료 건수
    public int getChatbotC(LocalDate sdate2, LocalDate edate2) {
        return reportRepository.countChatbotP(sdate2, edate2);
    }


    // 밴드 신규 접수
    public int getBandNew(LocalDate sdate2, LocalDate edate2) {
        return reportRepository.countBandToday(sdate2, edate2);
    }

    // 밴드 미처리 건수
    public int getBandP(LocalDate sdate2, LocalDate edate2) {
        return reportRepository.countBandP(sdate2, edate2);
    }
    // 밴드 미처리 건수
    public int getBandP2() {
        return reportRepository.countBandP2();
    }

    // 밴드 완료 건수
    public int getBandC(LocalDate sdate2, LocalDate edate2) {
        return reportRepository.countBandP(sdate2, edate2);
    }


    // 전화 신규 접수
    public int getPhoneNew(LocalDate sdate2, LocalDate edate2) {
        return reportRepository.countPhoneToday(sdate2, edate2);
    }

    // 전화 미처리 건수
    public int getPhoneP(LocalDate sdate2, LocalDate edate2) {
        return reportRepository.countPhoneP(sdate2, edate2);
    }
    // 전화 미처리 건수
    public int getPhoneP2() {
        return reportRepository.countPhoneP2();
    }

    // 전화 완료 건수
    public int getPhoneC(LocalDate sdate2, LocalDate edate2) {
        return reportRepository.countPhoneC(sdate2, edate2);
    }


    // 방문 신규 접수
    public int getVisitNew(LocalDate sdate2, LocalDate edate2) {
        return reportRepository.countVisitToday(sdate2, edate2);
    }

    // 방문 미처리 건수
    public int getVisitP(LocalDate sdate2, LocalDate edate2) {
        return reportRepository.countVisitP(sdate2, edate2);
    }
    // 방문 미처리 건수
    public int getVisitP2() {
        return reportRepository.countVisitP2();
    }


    // 방문 완료 건수
    public int getVisitC(LocalDate sdate2, LocalDate edate2) {
        return reportRepository.countVisitP(sdate2, edate2);
    }

    //////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////////////////////
    // 심곡동 챗봇/밴드/전화/방문 숫자
    public int getSimgocC(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSimgocC(sdate2, edate2); }
    public int getSimgocB(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSimgocB(sdate2, edate2); }
    public int getSimgocP(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSimgocP(sdate2, edate2); }
    public int getSimgocV(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSimgocV(sdate2, edate2); }

    // 부천동 챗봇/밴드/전화/방문 숫자
    public int getBucheonC(LocalDate sdate2, LocalDate edate2) { return reportRepository.countBucheonC(sdate2, edate2); }
    public int getBucheonB(LocalDate sdate2, LocalDate edate2) { return reportRepository.countBucheonB(sdate2, edate2); }
    public int getBucheonP(LocalDate sdate2, LocalDate edate2) { return reportRepository.countBucheonP(sdate2, edate2); }
    public int getBucheonV(LocalDate sdate2, LocalDate edate2) { return reportRepository.countBucheonV(sdate2, edate2); }

    // 신중동 챗봇/밴드/전화/방문 숫자
    public int getSinjoongC(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSinjoongC(sdate2, edate2); }
    public int getSinjoongB(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSinjoongB(sdate2, edate2); }
    public int getSinjoongP(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSinjoongP(sdate2, edate2); }
    public int getSinjoongV(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSinjoongV(sdate2, edate2); }

    // 중동 챗봇/밴드/전화/방문 숫자
    public int getJoongC(LocalDate sdate2, LocalDate edate2) { return reportRepository.countJoongC(sdate2, edate2); }
    public int getJoongB(LocalDate sdate2, LocalDate edate2) { return reportRepository.countJoongB(sdate2, edate2); }
    public int getJoongP(LocalDate sdate2, LocalDate edate2) { return reportRepository.countJoongP(sdate2, edate2); }
    public int getJoongV(LocalDate sdate2, LocalDate edate2) { return reportRepository.countJoongV(sdate2, edate2); }

    // 상동 챗봇/밴드/전화/방문 숫자
    public int getSangC(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSangC(sdate2, edate2); }
    public int getSangB(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSangB(sdate2, edate2); }
    public int getSangP(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSangP(sdate2, edate2); }
    public int getSangV(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSangV(sdate2, edate2); }

    // 대산동 챗봇/밴드/전화/방문 숫자
    public int getDeaSanC(LocalDate sdate2, LocalDate edate2) { return reportRepository.countDeasanC(sdate2, edate2); }
    public int getDeaSanB(LocalDate sdate2, LocalDate edate2) { return reportRepository.countDeasanB(sdate2, edate2); }
    public int getDeaSanP(LocalDate sdate2, LocalDate edate2) { return reportRepository.countDeasanP(sdate2, edate2); }
    public int getDeaSanV(LocalDate sdate2, LocalDate edate2) { return reportRepository.countDeasanV(sdate2, edate2); }

    // 소사본동 챗봇/밴드/전화/방문 숫자
    public int getSosabonC(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSosabonC(sdate2, edate2); }
    public int getSosabonB(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSosabonB(sdate2, edate2); }
    public int getSosabonP(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSosabonP(sdate2, edate2); }
    public int getSosabonV(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSosabonV(sdate2, edate2); }

    // 범안동 챗봇/밴드/전화/방문 숫자
    public int getBumanC(LocalDate sdate2, LocalDate edate2) { return reportRepository.countBumanC(sdate2, edate2); }
    public int getBumanB(LocalDate sdate2, LocalDate edate2) { return reportRepository.countBumanB(sdate2, edate2); }
    public int getBumanP(LocalDate sdate2, LocalDate edate2) { return reportRepository.countBumanP(sdate2, edate2); }
    public int getBumanV(LocalDate sdate2, LocalDate edate2) { return reportRepository.countBumanV(sdate2, edate2); }

    // 성곡동 챗봇/밴드/전화/방문 숫자
    public int getSunggocC(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSunggocC(sdate2, edate2); }
    public int getSunggocB(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSunggocB(sdate2, edate2); }
    public int getSunggocP(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSunggocP(sdate2, edate2); }
    public int getSunggocV(LocalDate sdate2, LocalDate edate2) { return reportRepository.countSunggocV(sdate2, edate2); }

    // 오정동 챗봇/밴드/전화/방문 숫자
    public int getOjungC(LocalDate sdate2, LocalDate edate2) { return reportRepository.countOjungC(sdate2, edate2); }
    public int getOjungB(LocalDate sdate2, LocalDate edate2) { return reportRepository.countOjungB(sdate2, edate2); }
    public int getOjungP(LocalDate sdate2, LocalDate edate2) { return reportRepository.countOjungP(sdate2, edate2); }
    public int getOjungV(LocalDate sdate2, LocalDate edate2) { return reportRepository.countOjungV(sdate2, edate2); }


    public void test14() {

        ComplaintsReq aa = new ComplaintsReq();
        var result = petitionRepository.DslTest1(aa);

        logger.info("re - {}", new Gson().toJson(result));


    }

    public void test15(String name) {

        logger.info("1 - {}", name.substring(5));
        logger.info("1 - {}", name.substring(5).indexOf("("));
        logger.info("1 - {}", name.substring(5).split("동")[0] + "동");


    }

    public void test16(String aa) {

        String bb = aa;
        bb = aa.replaceAll(" ", "");
        bb = bb.replaceAll("경기도", "");
        bb = bb.replaceAll("경기", "");
        bb = bb.replaceAll("부천시", "");
        bb = bb.split("동")[0]+"동";
//        bb = bb.replaceAll("0", "");
//        bb = bb.replaceAll("1", "");
//        bb = bb.replaceAll("2", "");
//        bb = bb.replaceAll("3", "");
//        bb = bb.replaceAll("4", "");
//        bb = bb.replaceAll("5", "");
//        bb = bb.replaceAll("6", "");
//        bb = bb.replaceAll("7", "");
//        bb = bb.replaceAll("8", "");
//        bb = bb.replaceAll("9", "");
//        bb = bb.replaceAll("-", "");

        logger.info("bb - {}", bb);
    }
}
