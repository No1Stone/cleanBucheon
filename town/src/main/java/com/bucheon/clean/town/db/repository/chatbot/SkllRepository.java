package com.bucheon.clean.town.db.repository.chatbot;

import com.bucheon.clean.town.db.entity.chatbot.Skll;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.repository.chatbot.dsl.SkllRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface SkllRepository extends JpaRepository<Skll, String>, SkllRepositoryDsl {

    Optional<Skll> findBySkllCode(String skllCode);
    Optional<Skll> findBySkllNm(String skllCode);

    int countBySkllCode(String skllCode);

    @Transactional
    @Modifying
    @Query(
            value = "delete from tb_skll where SKLL_CODE =:answerSeq "
            , nativeQuery = true
    )
    int DeleteAnswerSkllCode(@Param("answerSeq") String answerSeq);
}
