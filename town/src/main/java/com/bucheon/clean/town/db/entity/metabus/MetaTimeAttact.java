package com.bucheon.clean.town.db.entity.metabus;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_meta_time_attact")
@Getter
@Setter
@NoArgsConstructor
public class MetaTimeAttact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "time_seq", nullable = true)
    private Long timeSeq;
    @Column(name = "time_multiplication", nullable = true)
    private int timeMultiplication;
    @Column(name = "stime", nullable = true)
    private String stime;
    @Column(name = "etime", nullable = true)
    private String etime;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "del_yn", nullable = true)
    private String delYn;
    @Column(name = "reg_seq", nullable = true)
    private Long regSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_seq", nullable = true)
    private Long modSeq;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    MetaTimeAttact(
            Long timeSeq,
            int timeMultiplication,
            String stime,
            String etime,
            String useYn,
            String delYn,
            Long regSeq,
            LocalDateTime regDt,
            Long modSeq,
            LocalDateTime modDt
    ) {
        this.timeSeq = timeSeq;
        this.timeMultiplication = timeMultiplication;
        this.stime = stime;
        this.etime = etime;
        this.useYn = useYn;
        this.delYn = delYn;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.modSeq = modSeq;
        this.modDt = modDt;
    }
}
