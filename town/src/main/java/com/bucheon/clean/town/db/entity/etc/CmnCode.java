package com.bucheon.clean.town.db.entity.etc;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_cmn_code")
@Getter
@Setter
@NoArgsConstructor
public class CmnCode {

    @Id
    @Column(name = "code_seq", nullable = true)
    private Long codeSeq;
    @Column(name = "column", nullable = true)
    private String column;
    @Column(name = "code", nullable = true)
    private String code;
    @Column(name = "name", nullable = true)
    private String name;


    @Builder
    CmnCode(
            Long codeSeq,
            String column,
            String code,
            String name
            ) {
        this.codeSeq = codeSeq;
        this.column = column;
        this.code = code;
        this.name = name;
    }

}
