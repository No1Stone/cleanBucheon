package com.bucheon.clean.town.biz.environmental.report.v1.controller;

import com.bucheon.clean.town.biz.environmental.report.v1.band.BandService;
import com.bucheon.clean.town.biz.environmental.report.v1.model.*;
import com.bucheon.clean.town.biz.environmental.report.v1.service.ReportService;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.environmental.Report;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

@RestController
@RequestMapping(path = "/report/rest")
@RequiredArgsConstructor
public class ReportRestController {

    private final Logger logger = LoggerFactory.getLogger(ReportRestController.class);
    private final BandService bandService;
    private final ReportService reportService;

    @GetMapping(path = "/test111")
    public void BandInsertTest() {
    }

    @GetMapping(path = "/test2")
    public void testcase1() {
        bandService.BandBoardGet();
    }

    @GetMapping(path = "/test3")
    public Object testcase3() {
        return bandService.BoardCommentget();
    }

    @GetMapping(path = "/test4")
    public void testcase4() {
//        String adress = "경기부천시부흥로424번길25";
        String adress = bandService.BandBucheonDongValidation("경기 부천시 석천로209번길 15");
//        String contents = "abbb a밴드주소찾아서보내기테스트찾은밴드주소-" +adress+
        String texttt = "유알엘 엔코더 테슽" +
                "\n엔터 되나요" +
                "\n" +
                "테스트트트트트";
        String contents = URLEncoder.encode(texttt, Charset.forName("UTF-8"));
        bandService.SendBandPost(adress, contents);
    }

    @GetMapping(path = "/test5/{adress}")
    public void test5(@PathVariable(name = "adress") String adress) {
        String aa = bandService.BandBucheonDongValidation(adress);
        logger.info("aaaa - - -{}", aa);

    }

    @GetMapping(path = "/test6/{adress}")
    public void test6(@PathVariable(name = "adress") String adress) {
        bandService.newAdressConvertoldAdress(adress);

    }

    @GetMapping(path = "/test7")
    public void postSendTest() {
        bandService.postSend();
    }


    @PostMapping(path = "/test8")
    public List<ReportListRes> ReportSelectListTest(@RequestBody ReportListReq dto){
       return reportService.ReportSelectListService(dto);
    }

    @PostMapping(path = "/test9")
    public Paging ReportSelectListinfoPaingTest(@RequestBody ReportListReq dto){
        return reportService.ReportSelectListPagingInfoService(dto);
    }

    @GetMapping(path = "/test10/{seq}")
    public ReportListRes ReportSelectListTest(@PathVariable(name = "seq") Long seq){
        return reportService.ReportSelectOneService(seq);
    }


    @PostMapping(path = "/test11")
    public void savetest1(@RequestBody ReportSave dto){
        reportService.ReportSaveService(dto);
    }


    @PostMapping(path = "/test12")
    public void savetest12(@RequestBody ReportUpdate dto){
        reportService.UpdateReportDelService(dto.getDels());
    }


    @GetMapping(path = "/test13")
    public void savetest13(){
//        reportService.test22222();

    }

    @PostMapping(path = "/test15")
    public void test15(@RequestBody Test15Entity dto){
        reportService.test15(dto.getAa());
    }

    @PostMapping(path = "/test16")
    public void test16(@RequestBody Test15Entity dto){
        reportService.test16(dto.getAa());
    }

}
