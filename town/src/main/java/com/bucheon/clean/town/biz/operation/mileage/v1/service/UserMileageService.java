package com.bucheon.clean.town.biz.operation.mileage.v1.service;

import com.bucheon.clean.town.biz.operation.mileage.v1.model.MileageRequestVO;
import com.bucheon.clean.town.biz.operation.mileage.v1.model.UserMileageHistoryVO;
import com.bucheon.clean.town.biz.operation.mileage.v1.model.UserMileageVO;
import com.bucheon.clean.town.db.entity.operation.UserMileageHistory;
import com.bucheon.clean.town.db.repository.operation.UserMileageRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@Service
@RequiredArgsConstructor
public class UserMileageService {

    private final Logger logger = LoggerFactory.getLogger(UserMileageService.class);
    private final HttpServletRequest httpServletRequest;
    private final UserMileageRepository userMileageRepository;
    private final UserMileageHistoryService userMileageHistoryService;
    private final ModelMapper modelMapper;

    @Value("${mileage.host}")
    String mileageHost;

    public int UserMileageSelectCountService(String userId) {
        int cnt = userMileageRepository.countByUserId(userId);
        return cnt;
    }

    public UserMileageVO UserMileageSelectOneService(String userId) {
        UserMileageVO userMileageVO = modelMapper.map(userMileageRepository.findByUserId(userId)
                .orElseThrow(IllegalArgumentException::new), UserMileageVO.class);

        return userMileageVO;
    }

    public int UserMileageSaveService(UserMileageHistoryVO dto) {
        int cnt = 0;

        //마일리지 지급
        MileageRequestVO mileageRequestVO = new MileageRequestVO();
        mileageRequestVO.setMileageType("CleanTown");
        mileageRequestVO.setCharge(dto.getMileage());
        mileageRequestVO.setDid(dto.getUserId());

        //마일리지 숫자가 0보다 커야 한다
        if(dto.getMileage() > 0) {
            String kkk = UserMileageApiCallSaveService(mileageRequestVO);
            logger.info("- 마일리지 지급 : {}", kkk);

            if(kkk.equals("success")){
                int k = UserMileageSelectCountService(dto.getUserId());

                //먼저 로그부터 저장
                UserMileageHistory userMileageHistory = null;
                if(dto.getAdminSeq() == null) {
                    userMileageHistory = userMileageHistoryService.UserMileageHistorySaveService2(dto);
                } else {
                    userMileageHistory = userMileageHistoryService.UserMileageHistorySaveService(dto);
                }

                if(k==0){
                    //UserMileageHistoryVO save
                    UserMileageVO u = new UserMileageVO();
                    u.setMileageSum(userMileageHistory.getMileage());
                    u.setMileageSeq(userMileageHistory.getMileageSeq());
                    u.setUserId(userMileageHistory.getUserId());

                    userMileageRepository.save(u.ofUserMileage());

                    cnt++;
                } else {

                    UserMileageVO u = modelMapper.map(userMileageRepository.findByUserId(dto.getUserId())
                            .orElseThrow(IllegalArgumentException::new), UserMileageVO.class);
                    u.setMileageSum(userMileageHistory.getMileage());
                    u.setMileageSeq(userMileageHistory.getMileageSeq());

                    if(userMileageHistory.getMileage() >= 0){

                        userMileageRepository.updateUserMileageSum(userMileageHistory.getMileage(), u.getUserId());
                        cnt++;
                    }

                    if(userMileageHistory.getMileageSeq() >= 0){
                        userMileageRepository.updateUserMileageSeq(u.ofUserMileage().getMileageSeq(), u.getUserId());
                        cnt++;
                    }

                }
            }
        }

        return cnt;
    }


    public String UserMileageApiCallSelectService(MileageRequestVO dto) {

        String result = "";

        try {
            RestTemplate restTemplate = new RestTemplate();
            String url = mileageHost + "/rest/get/mileage";

            // Header set
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);

            // 테스트 여기서는 MileageRequestVO로 대체
            // Body set
            // MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
            // String imageFileString = fileService.getBase64String(file);
            // body.add("filename", fileName);
            // body.add("image", imageFileString);

            // Combine Message
            HttpEntity<?> requestMessage = new HttpEntity<>(dto, httpHeaders);

            // Request and getResponse
            HttpEntity<String> response = restTemplate.postForEntity(url, requestMessage, String.class);

            // Response Body 파싱
            // FlaskResponseDto 란 DTO로 받을때 사용
            // 일반 String으로 받아 처리해서 아래처럼 처리
            //ObjectMapper objectMapper = new ObjectMapper();
            //objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
            //FlaskResponseDto dto = objectMapper.readValue(response.getBody(), FlaskResponseDto.class);
            String responseStr = response.getBody();
            logger.info(">>>>>>>>>>>>>>>>>>> {}", responseStr);

            result = "success";
        } catch (Exception e) {
            result = "fail";
        }

        return result;
    }

    public String UserMileageApiCallSaveService(MileageRequestVO dto) {

        String result = "";

        try {
            RestTemplate restTemplate = new RestTemplate();
            String url = mileageHost + "/rest/put/mileage";

            // Header set
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);

            // 테스트 여기서는 MileageRequestVO로 대체
            // Body set
            // MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
            // String imageFileString = fileService.getBase64String(file);
            // body.add("filename", fileName);
            // body.add("image", imageFileString);

            // Combine Message
            HttpEntity<?> requestMessage = new HttpEntity<>(dto, httpHeaders);

            // Request and getResponse
            HttpEntity<String> response = restTemplate.postForEntity(url, requestMessage, String.class);

            // Response Body 파싱
            // FlaskResponseDto 란 DTO로 받을때 사용
            // 일반 String으로 받아 처리해서 아래처럼 처리
            //ObjectMapper objectMapper = new ObjectMapper();
            //objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
            //FlaskResponseDto dto = objectMapper.readValue(response.getBody(), FlaskResponseDto.class);
            String responseStr = response.getBody();
            logger.info(">>>>>>>>>>>>>>>>>>> {}", responseStr);

            result = "success";
        } catch (Exception e) {
            result = "fail";
        }

        return result;
    }

}
