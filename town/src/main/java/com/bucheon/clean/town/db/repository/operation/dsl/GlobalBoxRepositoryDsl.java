package com.bucheon.clean.town.db.repository.operation.dsl;


import com.bucheon.clean.town.biz.operation.board.v1.model.BoardReq;
import com.bucheon.clean.town.biz.operation.board.v1.model.GlobalBoxReq;
import com.bucheon.clean.town.db.entity.operation.Board;
import com.bucheon.clean.town.db.entity.operation.GlobalBox;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface GlobalBoxRepositoryDsl {

    @Transactional(readOnly = true)
    List<GlobalBox> SelectListGlobalBox(GlobalBoxReq dto);
    @Transactional(readOnly = true)
    QueryResults<GlobalBox> SelectListGlobalBoxPaging(GlobalBoxReq dto);
}
