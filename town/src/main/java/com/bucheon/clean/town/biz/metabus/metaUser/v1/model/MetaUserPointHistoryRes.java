package com.bucheon.clean.town.biz.metabus.metaUser.v1.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MetaUserPointHistoryRes {

    private Long pointSeq;
    private String userId;
    private String userNickname;
    private int point;
    private int accumulatedPoint;
    private Long trashSeq;
    private String trashName;
    private int cleanPoint;
    private String processLocation;
    private String reason;
    private LocalDateTime regDt;

}
