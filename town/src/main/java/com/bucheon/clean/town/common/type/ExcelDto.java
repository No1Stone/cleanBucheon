package com.bucheon.clean.town.common.type;

import java.util.List;

public interface ExcelDto {

    List<String> mapToList();
    List<Integer> mapToCoulmn();
}
