package com.bucheon.clean.town.db.entity.operation;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_board")
@Getter
@Setter
@NoArgsConstructor
public class Board {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "board_seq", nullable = true)
    private Long boardSeq;
    @Column(name = "board_type", nullable = true)
    private String boardType;
    @Column(name = "board_title", nullable = true)
    private String boardTitle;
    @Column(name = "board_content", nullable = true)
    private String boardContent;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "del_yn", nullable = true)
    private String delYn;
    @Column(name = "reg_seq", nullable = true)
    private Long regSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_seq", nullable = true)
    private Long modSeq;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;



    @Builder
    Board(
            Long boardSeq,
            String boardType,
            String boardTitle,
            String boardContent,
            LocalDateTime sdate,
            LocalDateTime edate,
            String useYn,
            String delYn,
            Long regSeq,
            LocalDateTime regDt,
            Long modSeq,
            LocalDateTime modDt
            ) {
        this.boardSeq = boardSeq;
        this.boardType = boardType;
        this.boardTitle = boardTitle;
        this.boardContent = boardContent;
        this.useYn = useYn;
        this.delYn = delYn;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.modSeq = modSeq;
        this.modDt = modDt;
    }

}
