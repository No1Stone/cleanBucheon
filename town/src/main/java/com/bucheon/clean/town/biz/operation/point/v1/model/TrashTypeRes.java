package com.bucheon.clean.town.biz.operation.point.v1.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TrashTypeRes {

    private int trashSeq;
    private String trashName;
    private int declarationMileage;
    private int processMileage;
    private String iconUrl;
    private String useYn;


}
