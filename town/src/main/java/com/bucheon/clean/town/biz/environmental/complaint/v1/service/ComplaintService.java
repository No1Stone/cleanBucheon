package com.bucheon.clean.town.biz.environmental.complaint.v1.service;

import com.bucheon.clean.town.biz.environmental.complaint.v1.model.ComplaintsReq;
import com.bucheon.clean.town.biz.environmental.complaint.v1.model.ComplaintsRes;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.service.MetaUserService;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.environmental.Petition;
import com.bucheon.clean.town.db.entity.environmental.Process;
import com.bucheon.clean.town.db.entity.environmental.Receipt;
import com.bucheon.clean.town.db.repository.environmental.PetitionImageRepository;
import com.bucheon.clean.town.db.repository.environmental.PetitionRepository;
import com.bucheon.clean.town.db.repository.environmental.ProcessRepository;
import com.bucheon.clean.town.db.repository.environmental.ReceiptRepository;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ComplaintService {

    private final Logger logger = LoggerFactory.getLogger(ComplaintService.class);
    private final HttpServletRequest httpServletRequest;
    private final PetitionRepository petitionRepository;
    private final PetitionImageRepository petitionImageRepository;
    private final ProcessRepository processRepository;
    private final ReceiptRepository receiptRepository;
    private final MetaUserService metaUserService;
    private final ModelMapper modelMapper;

    public Object ReportSelectListInfoService(ComplaintsReq dto) {
        List<Petition> li = petitionRepository.DynamicSelect(dto);
        List<ComplaintsRes> petiResult = petitionRepository.DynamicSelect(dto).stream()
                .map(e -> {
                    String dutyName = "";
                    String dutyTime = "";
                    if(e.getProcesses() != null && e.getProcesses().size() != 0){
                        dutyName = e.getProcesses().get(0).getDutyName();
                        dutyTime = e.getProcesses().get(0).getDoDateTime();
                    }

                   return ComplaintsRes.builder()
                            .petiNo(e.getPetiNo())
                            .petiTitle(e.getTitle())
                            .petiContent(e.getReason())
                            .busiBranCode(e.getReceipts().getBusiBranCode())
                            .busiBranCodeName(e.getReceipts().getBusiBranCodeName())
                            .petiStatus(e.getProcesses().stream().map(f -> f.getStatusName()).collect(Collectors.toList()))
                            .regDtTime(e.getRegDateTime())
                            .ancName(e.getReceipts().getAncName())
                            .receiptDtTime(e.getReceipts().getRegDateTime())
                            //.dutyCode(e.getProcesses().get(0).getDutyDeptCode())
                            .dutyName(dutyName)
                            //.comDtTime(LocalDateTime.parse(e.getProcesses().get(0).getDoDateTime(), DateTimeFormatter.ofPattern("yyyyMMddHHmmss")))
                            .comDtTime(dutyTime)
                            .build();
                })
                .collect(Collectors.toList());


        logger.info("======================================");
        return petiResult;
    }

    public Paging ReportSelectListPagingInfoService(ComplaintsReq dto) {
        QueryResults<Petition> result = petitionRepository.DynamicSelectPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public Petition ReportSelectOneService(String petiNo) throws Exception {

        Optional<Petition> petition = petitionRepository.findById(petiNo);
//        petition.get().setName(Masking.nameMasking(petition.get().getName()));
//        petition.get().setCellPhone(Masking.nameMasking(petition.get().getCellPhone()));

        return petition.orElseThrow(IllegalArgumentException::new);
    }


    public List<Process> getTest2() {
        return processRepository.findAll();
    }

    public List<Receipt> getTest3() {
        return receiptRepository.findAll();
    }

    // 안전신문고 신규 접수
    public int getSinmunNew(LocalDate sdate2, LocalDate edate2) {


        String sdate2tos = sdate2.getYear() + metaUserService.zeroPlus(sdate2.getMonthValue()) + metaUserService.zeroPlus(sdate2.getDayOfMonth()) + "000000";
        String edate2tos = edate2.getYear() + metaUserService.zeroPlus(edate2.getMonthValue()) + metaUserService.zeroPlus(edate2.getDayOfMonth()) + "235959";

        return petitionRepository.countSinmunToday(sdate2tos, edate2tos);
    }

    // 안전신문고 미처리 건수
    public int getSinmunP(LocalDate sdate2, LocalDate edate2) {

        String sdate2tos = sdate2.getYear() + metaUserService.zeroPlus(sdate2.getMonthValue()) + metaUserService.zeroPlus(sdate2.getDayOfMonth()) + "000000";
        String edate2tos = edate2.getYear() + metaUserService.zeroPlus(edate2.getMonthValue()) + metaUserService.zeroPlus(edate2.getDayOfMonth()) + "235959";

        return petitionRepository.countSinmunP(sdate2tos, edate2tos);
    }
    // 안전신문고 미처리 건수
    public int getSinmunP2() {
        return petitionRepository.countSinmunP2();
    }


    // 안전신문고 완료 건수
    public int getSinmunC(LocalDate sdate2, LocalDate edate2) {

        String sdate2tos = sdate2.getYear() + metaUserService.zeroPlus(sdate2.getMonthValue()) + metaUserService.zeroPlus(sdate2.getDayOfMonth()) + "000000";
        String edate2tos = edate2.getYear() + metaUserService.zeroPlus(edate2.getMonthValue()) + metaUserService.zeroPlus(edate2.getDayOfMonth()) + "235959";

        return petitionRepository.countSinmunC(sdate2tos, edate2tos);
    }
}
