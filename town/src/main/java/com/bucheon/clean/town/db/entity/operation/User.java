package com.bucheon.clean.town.db.entity.operation;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_user")
@Getter
@Setter
@NoArgsConstructor
public class User {
    @Id
    @Column(name = "did", nullable = true)
    private String did;
    @Column(name = "chatbot_user_id", nullable = true)
    private String chatbotUserId;
    @Column(name = "meta_user_id", nullable = true)
    private String metaUserId;
    @Column(name = "name", nullable = true)
    private String name;
    @Column(name = "phone", nullable = true)
    private String phone;
    @Column(name = "email", nullable = true)
    private String email;
    @Column(name = "bearer_token", nullable = true)
    private String bearerToken;
    @Column(name = "auth_token", nullable = true)
    private String authToken;
    @Column(name = "auth_key", nullable = true)
    private String authKey;
    @Column(name = "create_date", nullable = true)
    private LocalDateTime createDate;
    @Column(name = "update_date", nullable = true)
    private LocalDateTime updateDate;

    @Builder
    User(String did,
         String chatbotUserId,
         String metaUserId,
         String name,
         String phone,
         String email,
         String bearerToken,
         String authToken,
         String authKey,
         LocalDateTime createDate,
         LocalDateTime updateDate
    ) {
        this.did = did;
        this.chatbotUserId = chatbotUserId;
        this.metaUserId = metaUserId;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.bearerToken = bearerToken;
        this.authToken = authToken;
        this.authKey = authKey;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }


}
