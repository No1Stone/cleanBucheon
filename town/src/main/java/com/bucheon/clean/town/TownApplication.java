package com.bucheon.clean.town;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Statement;

@SpringBootApplication
public class TownApplication implements CommandLineRunner {
    @Autowired
    @Resource(name = "mysqlDatasource")
    private DataSource mysqlDatasource;

    @Autowired
    @Resource(name = "postDataSource")
    private DataSource postDataSource;

    public static void main(String[] args) {
        SpringApplication.run(TownApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        try (Connection connection = mysqlDatasource.getConnection()) {
            System.out.println("mysqlDatasource : " + connection.getMetaData().getURL());
        }

        try (Connection connection = postDataSource.getConnection()) {
            System.out.println("postDataSource : " +connection.getMetaData().getURL());
        }
    }

}
