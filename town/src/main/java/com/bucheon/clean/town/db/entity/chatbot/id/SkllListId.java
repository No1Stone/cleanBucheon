package com.bucheon.clean.town.db.entity.chatbot.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SkllListId implements Serializable {
    private String skllCode;
    private int rspnsOrdr;
}
