package com.bucheon.clean.town.db.repository.environmental;

import com.bucheon.clean.town.db.entity.environmental.ReportLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ReportLogRepository extends JpaRepository<ReportLog, Long> {

    Optional<ReportLog> findTop1ByReportSeqOrderByLogSeqDesc(Long seq);
    int countByReportSeq(Long seq);
    List<ReportLog> findByReportSeqOrderByLogSeqDesc(Long seq);
    Boolean existsByReportSeq(Long seq);
}
