package com.bucheon.clean.town.biz.operation.board.v1.service;

import com.bucheon.clean.town.biz.operation.board.v1.model.BoardAttVO;
import com.bucheon.clean.town.biz.operation.board.v1.model.BoardReq;
import com.bucheon.clean.town.biz.operation.board.v1.model.BoardRes;
import com.bucheon.clean.town.biz.operation.board.v1.model.BoardVO;
import com.bucheon.clean.town.common.type.YnType;
import com.bucheon.clean.town.common.util.Paging;
import com.bucheon.clean.town.db.entity.metabus.MetaBoardAtt;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.Board;
import com.bucheon.clean.town.db.entity.operation.BoardAtt;
import com.bucheon.clean.town.db.repository.operation.AdminRepository;
import com.bucheon.clean.town.db.repository.operation.BoardAttRepository;
import com.bucheon.clean.town.db.repository.operation.BoardRepository;
import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BoardService {

    private final Logger logger = LoggerFactory.getLogger(BoardService.class);
    private final HttpServletRequest httpServletRequest;
    private final BoardRepository boardRepository;
    private final BoardAttRepository boardAttRepository;
    private final AdminRepository adminRepository;
    private final ModelMapper modelMapper;


    public List<BoardRes> BoardSelectListService(BoardReq dto) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        logger.info("dto  - - - -{}", new Gson().toJson(dto));
        Map<Long, String> tt = adminRepository.findAll()
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
        //logger.info("ttmap - {}", tt);
        //logger.info("getSessionAdministrativeDivision - {}", dto.getSessionAdministrativeDivision());
        List<BoardRes> boardRes = boardRepository.SelectListBoard(dto)
                .stream().map(e -> modelMapper.map(e, BoardRes.class))
                .peek(f -> {
                    f.setBoardRegName(tt.get(f.getRegSeq()));
                    f.setBoardModName(tt.get(f.getModSeq()));
                })
                .collect(Collectors.toList());
        return boardRes;
    }

    public Paging BoardSelectListPagingInfoService(BoardReq dto) {
        QueryResults<Board> result = boardRepository.SelectListBoardPaging(dto);
        Paging pa = new Paging();
        pa.setTotalSize(result.getTotal());
        pa.setTotalPage((result.getTotal() % dto.getSize() > 0) ? result.getTotal() / dto.getSize() + 1 : result.getTotal() / dto.getSize());
        pa.setSize(dto.getSize());
        pa.setPage(dto.getPage());
        return pa;
    }

    public int UpdateBoardDelService(Long[] boardSeql) {
        List<Integer> result = Arrays.stream(boardSeql).map(e -> boardRepository
                .UpdateBoardDel(YnType.Y.getName(), e)).collect(Collectors.toList());
        return result.size();
    }

    public int UpdateBoardAttDelService(Long[] metaBoardSeql) {
        List<Integer> result = Arrays.stream(metaBoardSeql).map(e -> boardAttRepository
                .deleteByBoardSeq(e)).collect(Collectors.toList());
        return result.size();
    }

    @Transactional
    public int UpdateBoard(BoardVO dto) {
        int result = 0;
        if (dto.getBoardSeq() != null) {
            boardRepository.updateTitle(dto.getBoardTitle(), dto.getBoardSeq());
            boardRepository.updateContent(dto.getBoardContent(), dto.getBoardSeq());
            boardRepository.UpdateBoardUse(dto.getUseYn(), dto.getBoardSeq());

            result++;
        }
        return result;
    }


    public BoardRes BoardSelectOneService(Long seq) {
        Map<Long, String> tt = adminRepository.findAll()
                .stream().collect(Collectors.toMap(Admin::getAdminSeq, Admin::getAdminName));
        logger.info(">>>>>>>>>>>>>>>>>>>>> tt : " + tt);

        BoardRes result = modelMapper.map(boardRepository.findByBoardSeqAndDelYn(seq, "N").orElseThrow(IllegalArgumentException::new), BoardRes.class);
        result.setBoardRegName(tt.get(result.getRegSeq()));
        result.setBoardModName(tt.get(result.getModSeq()));

        logger.info(">>>>>>>>>>>>>>>>>>>>> 22 : " + result.getBoardSeq());
        logger.info(">>>>>>>>>>>>>>>>>>>>> 참?거짓? : " + boardAttRepository.existsByBoardSeq(result.getBoardSeq()));

        if (boardAttRepository.existsByBoardSeq(result.getBoardSeq())) {


            result.setBoardAttList(boardAttRepository
                    .findByBoardSeq(result.getBoardSeq()).stream().map(e -> e.getBoardAtt()).collect(Collectors.toList()));
        }
//                .peek(f -> {
//                    f.setReportTypeName(tt.get(f.getReportType()));
//                    f.setReportStatusName(ProcessType.valueOf(f.getReportStatus()).getDescription());
//                    f.setReportReceptionName(ReceptionType.valueOf(f.getReportReception()).getDescription());
//                })
        try {
            //result.setReportUserName(Masking.nameMasking(reportResult.getReportUserName()));
        } catch (Exception e) {
            //아무것도 안함
        }


        return result;
    }

    public List<BoardAtt> BoardAttSelectListService(Long seq) {
        List<BoardAtt> res = boardAttRepository.findByBoardSeq(seq);
        return res;
    }


    public Board BoardSaveService(BoardVO dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Admin admin = (Admin) principal;

        if (dto.getBoardSeq() == null) {
            dto.setRegSeq(admin.getAdminSeq());
        }
        if (dto.getDelYn() == null) {
            dto.setDelYn("N");
        }
        dto.setModSeq(admin.getAdminSeq());

        Board board = boardRepository.save(dto.ofBoard());

        return board;
    }

    public void BoardAttSaveService(BoardAttVO dto) {
        boardAttRepository.save(dto.ofBoardAtt());
    }

    //첨부파일 전체 지우기
    public void BoardAttDelService(BoardAttVO dto) {
        boardAttRepository.deleteByBoardSeq(dto.getBoardSeq());
    }

    //첨부파일 한개 지우기
    public int DeleteAtt(Long boardAttSeq) {

        int cnt = boardAttRepository.deleteByBoardAttSeq(boardAttSeq);

        return cnt;
    }

    public int BoardCountOneService(Long seq) {
        return boardRepository.countByBoardSeqAndDelYn(seq, "N");
    }

}
