package com.bucheon.clean.town.db.repository.environmental.band;

import com.bucheon.clean.town.db.entity.environmental.band.TB_BandBoardList;
import com.bucheon.clean.town.db.entity.environmental.band.TB_BandBoardListPoto;
import com.bucheon.clean.town.db.entity.environmental.band.id.TB_BandBoardListPotoId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TB_BandBoardListPotoRepository extends JpaRepository<TB_BandBoardListPoto, TB_BandBoardListPotoId> {



}
