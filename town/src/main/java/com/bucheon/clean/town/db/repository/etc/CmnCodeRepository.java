package com.bucheon.clean.town.db.repository.etc;

import com.bucheon.clean.town.db.entity.etc.CmnCode;
import com.bucheon.clean.town.db.entity.etc.DongName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface CmnCodeRepository extends JpaRepository<CmnCode, Long> {

    Optional<CmnCode> findByColumnAndCode(String column, String code);
}
