package com.bucheon.clean.town.db.repository.chatbot.dsl;

import com.bucheon.clean.town.biz.chatbot.answer.v1.model.ListCardVO;
import org.springframework.transaction.annotation.Transactional;

public interface SkllListRepositoryDsl {

    @Transactional(readOnly = true)
    ListCardVO selectSkllCcCodeAndRspnsOrdr(String code, int reqSeq);
}
