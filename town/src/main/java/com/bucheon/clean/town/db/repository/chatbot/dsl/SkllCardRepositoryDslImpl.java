package com.bucheon.clean.town.db.repository.chatbot.dsl;

import com.bucheon.clean.town.biz.chatbot.answer.v1.model.BasicCardVO;
import com.bucheon.clean.town.db.entity.chatbot.QSkll;
import com.bucheon.clean.town.db.entity.chatbot.QSkllCard;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class SkllCardRepositoryDslImpl implements SkllCardRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QSkll qSkll = QSkll.skll;
    private QSkllCard qSkllCard = QSkllCard.skllCard;

    @Override
    public List<BasicCardVO> selectSkllBcCodeAndRspnsOrdr(String code, int reqSeq) {

        List<BasicCardVO> result = queryFactory.select(
                        Projections.bean(BasicCardVO.class,
                                qSkllCard.skllCode.as("skillCode"),
                                qSkllCard.sj.as("title"),
                                qSkllCard.thumb.as("thumbnail"),
                                qSkllCard.profl.as("profile"),
                                qSkllCard.socty.as("social"),
                                qSkllCard.btton.as("button"),
                                qSkllCard.bcOrdr.as("bcSeq"),
                                qSkllCard.dc.as("description"),
                                qSkllCard.bttonOptnAt.as("optnAt"),
                                qSkllCard.proflCrtfcAt.as("proflCrtfcAt")
                        )
                )
                .from(qSkllCard)
                .where(eqId(code), eqRspnsOrder(reqSeq))
                .orderBy(qSkllCard.bcOrdr.desc())
                .fetch();
        return result;

    }

    private BooleanExpression eqId(String id) {
        if(StringUtils.isEmptyOrWhitespace(id)){
            return null;
        }
        return qSkllCard.skllCode.eq(String.valueOf(id));
    }

   private BooleanExpression eqRspnsOrder(int reqSeq) {
        if(reqSeq == 0){
            return null;
        }
        return qSkllCard.rspnsOrdr.eq(reqSeq);
   }

}
