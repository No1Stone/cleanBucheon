package com.bucheon.clean.town.db.repository.environmental.dsl;

import com.bucheon.clean.town.biz.environmental.campaign.v1.controller.test14;
import com.bucheon.clean.town.biz.environmental.complaint.v1.model.ComplaintsReq;
import com.bucheon.clean.town.db.entity.environmental.Petition;
import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface PetitionRepositoryDsl {

    @Transactional(readOnly = true)
    List<Petition> DynamicSelect(ComplaintsReq dto);

    @Transactional(readOnly = true)
    QueryResults<Petition> DynamicSelectPaging(ComplaintsReq dto);

    @Transactional(readOnly = true)
    List<test14> DslTest1(ComplaintsReq dto);

}
