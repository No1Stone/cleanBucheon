package com.bucheon.clean.town.db.repository.environmental.band;

import com.bucheon.clean.town.db.entity.environmental.band.TB_BandBoardList;
import com.bucheon.clean.town.db.entity.environmental.band.TB_BandBoardListComment;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TB_BandBoardListRepository extends JpaRepository<TB_BandBoardList, String> {


    List<TB_BandBoardList> findByProcessStatusAndCommentCountGreaterThanOrderByCreatedAtDesc(String status, int then);
    List<TB_BandBoardList> findTop2ByProcessStatusAndCommentCountGreaterThanOrderByCreatedAtDesc(String status, int then);
    List<TB_BandBoardList> findTop2ByProcessStatusOrderByCreatedAtDesc(String status);


    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_band_board_list set process_status = :processStatus " +
                    "where post_key = :postKey "
            ,nativeQuery = true
    )
    int UpdateProcessStatus(@Param("processStatus") String processStatus, @Param("postKey") String postKey);


    boolean existsByBandKey(String bandKey);
    boolean existsByPostKey(String postKey);

}
