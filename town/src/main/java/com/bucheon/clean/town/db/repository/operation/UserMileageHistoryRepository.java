package com.bucheon.clean.town.db.repository.operation;

import com.bucheon.clean.town.db.entity.operation.UserMileageHistory;
import com.bucheon.clean.town.db.repository.operation.dsl.UserMileageHistoryRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface UserMileageHistoryRepository extends JpaRepository<UserMileageHistory, String>, UserMileageHistoryRepositoryDsl {

    @Transactional(readOnly = true)
    int countByUserId(String userId);

    @Transactional(readOnly = true)
    Optional<UserMileageHistory> findByMileageSeq(Long mileageSeq);

}
