package com.bucheon.clean.town.db.repository.environmental;

import com.bucheon.clean.town.db.entity.environmental.CampaignLog;
import com.bucheon.clean.town.db.entity.environmental.ReportLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CampaignLogRepository extends JpaRepository<CampaignLog, Long> {

    Optional<CampaignLog> findTop1ByCampaignSeqOrderByLogSeqDesc(Long seq);
    int countByCampaignSeq(Long seq);
    List<CampaignLog> findByCampaignSeqOrderByLogSeqDesc(Long seq);
    Boolean existsByCampaignSeq(Long seq);
}
