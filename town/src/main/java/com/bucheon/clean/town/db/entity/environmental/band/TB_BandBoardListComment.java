package com.bucheon.clean.town.db.entity.environmental.band;

import com.bucheon.clean.town.db.entity.environmental.band.id.TB_BandBoardListCommentId;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_band_board_list_comment")
@Getter
@Setter
@NoArgsConstructor
@IdClass(TB_BandBoardListCommentId.class)
public class TB_BandBoardListComment {
    @Id
    @Column(name = "post_key", nullable = true)
    private String postKey;
    @Column(name = "comment_key", nullable = true)
    private String commentKey;
    @Column(name = "body", nullable = true)
    private String body;
    @Id
    @Column(name = "created_at", nullable = true)
    private long createdAt;
    @Column(name = "name", nullable = true)
    private String name;
    @Column(name = "description", nullable = true)
    private String description;
    @Column(name = "profile_image_url", nullable = true)
    private String profileImageUrl;
    @Column(name = "user_key", nullable = true)
    private String userKey;
    @Column(name = "url", nullable = true)
    private String url;
    @Column(name = "width", nullable = true)
    private int width;
    @Column(name = "height", nullable = true)
    private int height;

}
