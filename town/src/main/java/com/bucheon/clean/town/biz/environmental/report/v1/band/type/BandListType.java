package com.bucheon.clean.town.biz.environmental.report.v1.band.type;

public enum BandListType {

    클린부천대산동심곡본심곡본1송내12("AABZ5tVQ-j0kUh20kygJrUFP"),
    클린부천부천동원미1동역곡동춘의동도당동("AAA5BrV-VHGHmAoQ-Jdd40nw"),
    클린부천소사본동소사본동소사본3동("AAAnmQI6x7cck8uh31TRIY71"),
    클린부천신중동약대동중1234동("AAAC8JYvxFFTUBUboufgw0TO"),
    클린부천심곡동심곡123동원미2동소사동("AADoTHtgatKu2tcMvWauwlxv"),
    클린부천오정동오정원종12신흥("AACEH0fNIchZ0FuSpnZp-ahA")
    ;
    private String band_key;

    BandListType(String band_key) {
        this.band_key = band_key;
    }

    public String getBand_key() {
        return band_key;
    }
    public String getName() {
        return name();
    }

}
