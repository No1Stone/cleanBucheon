package com.bucheon.clean.town.biz.chatbot.alarmtalk.v1.model;

import java.time.LocalDateTime;

public interface MsgLogRes {

    Long getMsgSeq();
    String getMsgNoticetalkTmpKey();
    LocalDateTime getSentDate();
    String getCallTo();
    String getSendType();
    String getSendRslt();
    String getRsltCode();
    String getRsltText();

}
