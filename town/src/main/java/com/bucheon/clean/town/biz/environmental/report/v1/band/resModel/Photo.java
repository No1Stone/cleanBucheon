package com.bucheon.clean.town.biz.environmental.report.v1.band.resModel;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Photo {
    private String url;
    private int width;
    private int height;

}
