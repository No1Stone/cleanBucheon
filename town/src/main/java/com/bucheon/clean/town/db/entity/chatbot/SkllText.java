package com.bucheon.clean.town.db.entity.chatbot;

import com.bucheon.clean.town.db.entity.chatbot.id.SkllTextId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tb_skll_text")
@Getter
@Setter
@NoArgsConstructor
@IdClass(SkllTextId.class)
public class SkllText {
    @Id
    @Column(name = "skll_code", nullable = true)
    private String skllCode;
    @Column(name = "text", nullable = true)
    private String text;
    @Column(name = "btton", nullable = true)
    private String btton;
    @Id
    @Column(name = "rspns_ordr", nullable = true)
    private int rspnsOrdr;

    @Builder
    SkllText(
            String skllCode,
            String text,
            String btton,
            int rspnsOrdr
    ) {
        this.skllCode = skllCode;
        this.text = text;
        this.btton = btton;
        this.rspnsOrdr = rspnsOrdr;
    }

}
