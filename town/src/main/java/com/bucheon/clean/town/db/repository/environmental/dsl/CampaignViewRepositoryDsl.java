package com.bucheon.clean.town.db.repository.environmental.dsl;


import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignExcelDto;
import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignReq;
import com.bucheon.clean.town.biz.environmental.campaign.v1.model.CampaignRes;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CampaignViewRepositoryDsl {

    @Transactional(readOnly = true)
    List<CampaignExcelDto> findExcelList(CampaignReq dto);

}
