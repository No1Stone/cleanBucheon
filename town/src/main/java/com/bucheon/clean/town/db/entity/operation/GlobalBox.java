package com.bucheon.clean.town.db.entity.operation;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_global_box")
@Getter
@Setter
@NoArgsConstructor
public class GlobalBox {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "box_seq", nullable = true)
    private Long boxSeq;
    @Column(name = "board_title", nullable = true)
    private String boardTitle;
    @Column(name = "path_seq", nullable = true)
    private Long pathSeq;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "del_yn", nullable = true)
    private String delYn;
    @Column(name = "reg_seq", nullable = true)
    private Long regSeq;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_seq", nullable = true)
    private Long modSeq;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;



    @Builder
    GlobalBox(
            Long boxSeq,
            String boardTitle,
            Long pathSeq,
            String useYn,
            String delYn,
            Long regSeq,
            LocalDateTime regDt,
            Long modSeq,
            LocalDateTime modDt
            ) {
        this.boxSeq = boxSeq;
        this.boardTitle = boardTitle;
        this.pathSeq = pathSeq;
        this.useYn = useYn;
        this.delYn = delYn;
        this.regSeq = regSeq;
        this.regDt = regDt;
        this.modSeq = modSeq;
        this.modDt = modDt;
    }

}
