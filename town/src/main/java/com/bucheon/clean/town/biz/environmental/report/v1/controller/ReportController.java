package com.bucheon.clean.town.biz.environmental.report.v1.controller;

import com.bucheon.clean.town.biz.environmental.report.v1.model.*;
import com.bucheon.clean.town.biz.environmental.report.v1.service.ReportService;
import com.bucheon.clean.town.biz.operation.declarationPoint.v1.service.DeclarationPointService;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuPermissionRes;
import com.bucheon.clean.town.biz.operation.menu.v1.model.MenuRes;
import com.bucheon.clean.town.biz.operation.menu.v1.service.MenuService;
import com.bucheon.clean.town.biz.operation.mileage.v1.model.UserMileageHistoryVO;
import com.bucheon.clean.town.biz.operation.mileage.v1.service.UserMileageService;
import com.bucheon.clean.town.biz.operation.permission.v1.service.PermissionService;
import com.bucheon.clean.town.biz.operation.personalInfo.v1.service.PersonalInfoLogService;
import com.bucheon.clean.town.biz.operation.point.v1.service.PointService;
import com.bucheon.clean.town.biz.operation.user.v1.service.UserService;
import com.bucheon.clean.town.biz.util.service.UtilService;
import com.bucheon.clean.town.common.type.ReceptionType;
import com.bucheon.clean.town.common.util.ExcelXlsView;
import com.bucheon.clean.town.common.util.Masking;
import com.bucheon.clean.town.db.entity.environmental.Report;
import com.bucheon.clean.town.db.entity.environmental.band.TB_BandBoardListComment;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.DeclarationPoint;
import com.bucheon.clean.town.db.entity.operation.User;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/environmental/report")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ReportController {

    private final Logger logger = LoggerFactory.getLogger(ReportController.class);
    private final ReportService reportService;
    private final DeclarationPointService declarationPointService;
    private final UserService userService;
    private final PointService pointService;
    private final UserMileageService userMileageService;
    private final PermissionService permissionService;
    private final PersonalInfoLogService personalInfoLogService;
    private final UtilService utilService;
    private final MenuService menuService;

    //주소 리다이렉트 처리
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        return new ModelAndView("redirect:/environmental/report/info/C");
    }

    //기본틀 
    @RequestMapping(path = "/info/{type}", method = RequestMethod.GET)
    public ModelAndView info(ReportListReq dto, @PathVariable String type) throws IllegalStateException, Exception {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        String pageTitle = "";
        Long menu2 = 0L;

        if(type.equals("C")){ pageTitle = "카카오톡 챗봇 불편접수"; menu2 = 5L; }
        else if(type.equals("B")){ pageTitle = "네이버밴드 불편접수"; menu2 = 29L; }
        else if(type.equals("P")){ pageTitle = "전화 불편접수"; menu2 = 30L; }
        else if(type.equals("V")){ pageTitle = "방문 불편접수"; menu2 = 31L; }
        MenuRes menu = menuService.MenuSelectOneService(menu2);

        dto.setPageType(type);

        return new ModelAndView("environmental/report/reportManage")
                .addObject("reportPaging", reportService.ReportSelectListPagingInfoService(dto))
                .addObject("adminInfo",admin)
                .addObject("pageType",type)
                .addObject("pageTitle",pageTitle)
                .addObject("trashTypeList",pointService.TrashSelectUseAllListService())
                .addObject("selectMenu",menu)
                ;
    }

    //리스트 페이지
    @RequestMapping(path = "/listAjax", method = RequestMethod.POST)
    public ModelAndView listAjax(ReportListReq dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;
        String adminId = ((Admin) principal).getUsername();
        String sessionAdministrativeDivision = ((Admin) principal).getAdministrativeDivision();
        dto.setSessionAdministrativeDivision(sessionAdministrativeDivision);

        return new ModelAndView("environmental/report/reportListAjax")
                .addObject("reportList", reportService.ReportSelectListService(dto))
                .addObject("reportPaging", reportService.ReportSelectListPagingInfoService(dto))
                .addObject("pageType",dto.getPageType())
                ;
    }

    //최초 입력 페이지 팝업 - 필요없음
    @RequestMapping(path = "/writeForm", method = RequestMethod.POST)
    public ModelAndView writeForm(String pageType) {
        return new ModelAndView("environmental/report/reportManagePopupAjax")
                .addObject("trashList", reportService.SelectTrashType())
                .addObject("pageType", pageType)
                ;
    }

    //수정 입력 페이지 팝업
    @RequestMapping(path = "/modifyForm", method = RequestMethod.POST)
    public ModelAndView modifyForm(String pageType) {
        return new ModelAndView("environmental/report/reportManagePopupAjax")
                .addObject("trashList", reportService.SelectTrashType())
                .addObject("pageType", pageType)
                ;
    }

    //상세보기 팝업
    @RequestMapping(path = "/viewForm", method = RequestMethod.POST)
    public ModelAndView viewForm(Long seq) throws Exception {

        //접수마일리지
        DeclarationPoint declarationPoint= declarationPointService.DeclarationPointSelectOneService();

        //챗봇, 밴드, 전화 접수 내용
        ReportListRes reportListRes = reportService.ReportSelectOneService(seq);

        //user 정보 확인
        //1. did 검사
        //2. 없으면 챗봇아이디 검사
        //3. 없으면 그냥 통과
        User user = new User();
        if(reportListRes.getRegUserId() != null && !reportListRes.getRegUserId().equals("")){
            try{
                user = userService.UserSelectOneService(reportListRes.getRegUserId());
            } catch (Exception e) {
            }

            try{
                user = userService.UserSelectOneService2(reportListRes.getRegUserId());
            } catch (Exception e) {
            }
        }

        if(reportListRes.getReportUserName() == null || reportListRes.getReportUserName().equals("")){
            if(user.getName() != null && !user.getName() .equals("")) {
                reportListRes.setReportUserName(Masking.nameMasking(user.getName()));
            }
        } else {
            reportListRes.setReportUserName(Masking.nameMasking(reportListRes.getReportUserName()));
        }

        if(reportListRes.getReportUserPhone() == null || reportListRes.getReportUserPhone().equals("")){
            if(user.getPhone() != null && !user.getPhone() .equals("")) {
                reportListRes.setReportUserPhone(Masking.phoneMasking(user.getPhone()));
            }
        } else {
            reportListRes.setReportUserPhone(Masking.phoneMasking(reportListRes.getReportUserPhone()));
        }

        if(reportListRes.getReportUserEmail() == null || reportListRes.getReportUserEmail().equals("")){
            if(user.getEmail() != null && !user.getEmail() .equals("")) {
                reportListRes.setReportUserEmail(Masking.phoneMasking(user.getEmail()));
            }
        } else {
            reportListRes.setReportUserEmail(Masking.emailMasking(reportListRes.getReportUserEmail()));
        }

        //줄바꿈
        val nlString = System.getProperty("line.separator").toString();

        //댓글 가져오기
        List<TB_BandBoardListComment> reportCommentList = reportService.SelectReportComment(reportListRes.getSourcePk());

        return new ModelAndView("environmental/report/reportManageViewAjax")
                .addObject("reportOne", reportListRes)
                .addObject("reportLogList", reportService.ReportLogSelectService(reportListRes.getReportSeq()))
                .addObject("trashList", reportService.SelectTrashType())
                .addObject("declarationPoint", declarationPoint.getDeclarationPoint())
                .addObject("userOne", user)
                .addObject("nlString", nlString)
                .addObject("reportCommentList", reportCommentList)
                ;
    }

    //상세보기 팝업
    @RequestMapping(path = "/viewPersonalInfo", method = RequestMethod.POST)
    public ModelAndView viewPersonalInfo(Long seq, String pageType) {

        //로그 저장장
       PersonalSearchReq dto = new PersonalSearchReq();

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }
        Admin admin = (Admin) principal;

        String menu = "";
        String menuhan = "";
        String reason = "";
        if(pageType.equals(ReceptionType.C.getName())){
            menu = "Chatbot";
            menuhan = "카카오톡 챗봇 불편접수";
            reason = admin.getAdminName() +  " : 카카오톡 챗봇 불편접수 접수자 확인";
        }
        else if(pageType.equals(ReceptionType.B.getName())){
            menu = "Band";
            menuhan = "네이버밴드 불편접수";
            reason = admin.getAdminName() +  " : 네이버밴드 불편접수 접수자 확인";
        }
        else if(pageType.equals(ReceptionType.P.getName())){
            menu = "Phone";
            menuhan = "전화 불편접수";
            reason = admin.getAdminName() +  " : 전화 불편접수 접수자 확인";
        }
        else if(pageType.equals(ReceptionType.V.getName())){
            menu = "Visit";
            menuhan = "방문 불편접수";
            reason = admin.getAdminName() +  " : 방문 불편접수 접수자 확인";
        }
        else if(pageType.equals(ReceptionType.A.getName())){
            menu = "Campaign";
            menuhan = "캠페인참여";
            reason = admin.getAdminName() +  " : 캠페인 참여자 확인";
        }

        dto.setLogTime(LocalDateTime.now());
        dto.setAdminSeq(admin.getAdminSeq());
        dto.setMenuName(menu);
        dto.setMenuHanName(menuhan);
        dto.setSeq(seq);
        dto.setReason(reason);
        personalInfoLogService.PersonalSearchSaveService(dto);


        ReportListRes reportListRes = reportService.ReportSelectOneService2(seq);

        //user 정보 확인
        //1. did 검사
        //2. 없으면 챗봇아이디 검사
        //3. 없으면 그냥 통과
        User user = new User();
        if(reportListRes.getRegUserId() != null && !reportListRes.getRegUserId().equals("")){
            try{
                user = userService.UserSelectOneService3(reportListRes.getRegUserId());
            } catch (Exception e) {
            }

            try{
                user = userService.UserSelectOneService2(reportListRes.getRegUserId());
            } catch (Exception e) {
            }
        }

        if(reportListRes.getReportUserName() == null || reportListRes.getReportUserName().equals("")){
            reportListRes.setReportUserName(user.getName());
        }

        if(reportListRes.getReportUserPhone() == null || reportListRes.getReportUserPhone().equals("")){
            reportListRes.setReportUserPhone(user.getPhone());
        }

        if(reportListRes.getReportUserEmail() == null || reportListRes.getReportUserEmail().equals("")){
            reportListRes.setReportUserEmail(user.getEmail());
        }

        return new ModelAndView("operation/personalInfo/personalInfoPopViewAjax")
                .addObject("userName", reportListRes.getReportUserName())
                .addObject("userPhone", reportListRes.getReportUserPhone())
                .addObject("userEmail", reportListRes.getReportUserEmail())
                ;
    }

    //저장처리
    @RequestMapping(path = "/insertInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object insertInfo(@RequestParam("uploadfile") MultipartFile[] uploadfile, ReportSave dto) throws IllegalStateException, IOException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        ReportImgVo reportImgVo = new ReportImgVo();
        try {
            if(uploadfile != null) {
                List<String> fileName = utilService.FileSaveService(uploadfile, "chatbot");
                reportImgVo.setReportImg(fileName.get(0));
            }
        } catch (Exception e){
            //
        }

        Report report = reportService.ReportSaveService(dto);
        reportImgVo.setReportSeq(report.getReportSeq());
        reportService.ReportImgSaveService(reportImgVo);

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //수정처리
    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object updateInfo(ReportUpdate dto) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        //내용 업데이트
        reportService.UpdateReport(dto);
        ReportListRes reportListRes = reportService.ReportSelectOneService(dto.getReportSeq());


        if(dto.getReportPoint() == null || dto.getReportPoint().equals("")){
            dto.setReportPoint("0");
        }

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //삭제처리
    @RequestMapping(path = "/deleteInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteInfo(Long[] reportSeq) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        reportService.UpdateReportDelService(reportSeq);
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //완료처리
    @RequestMapping(path = "/completeInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object completeInfo(Long[] reportSeq) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal == "anonymousUser"){
            return new ModelAndView("redirect:/login/info");
        }

        reportService.UpdateReportCompleteService(reportSeq);
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("result", "success");
        return map;
    }

    //EXCEL DOWN
    @PostMapping("/info/downExcel")
   public ModelAndView getExcelDownList(ReportListReq dto) {

        Map<String, Object> excelData = null;
        try {
            excelData = reportService.downExcelList(dto);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView(new ExcelXlsView(), excelData);
    }
}
