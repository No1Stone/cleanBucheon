package com.bucheon.clean.town.db.repository.operation.dsl;

import com.bucheon.clean.town.biz.operation.point.v1.model.TrashTypeReq;
import com.bucheon.clean.town.db.entity.operation.QTrashType;
import com.bucheon.clean.town.db.entity.operation.TrashType;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Aspect
@RequiredArgsConstructor
public class TrashTypeRepositoryDslImpl implements  TrashTypeRepositoryDsl{

    private final JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(TrashTypeRepositoryDslImpl.class);
    private QTrashType qTrashType = QTrashType.trashType;


    @Override
    public List<TrashType> TrashTypeSelectList(TrashTypeReq dto) {

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        List<TrashType> result =  queryFactory.selectFrom(qTrashType)
                .where(eqTrashName(dto.getTrashName()), qTrashType.delYn.eq("N"))
                .offset(dto.getPage() * dto.getSize())
                .limit(dto.getSize())
                .fetch();
        return result;
    }

    @Override
    public List<TrashType> TrashTypeSelectBandNList(TrashTypeReq dto) {

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        List<TrashType> result =  queryFactory.selectFrom(qTrashType)
                .where(
                        eqTrashName(dto.getTrashName()), qTrashType.delYn.eq("N"),
                        qTrashType.trashSeq.ne(20L)
                )
                .offset(dto.getPage() * dto.getSize())
                .limit(dto.getSize())
                .fetch();
        return result;
    }

    @Override
    public QueryResults<TrashType> TrashTypeSelectListPaging(TrashTypeReq dto) {

        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        QueryResults<TrashType> result = queryFactory.selectFrom(qTrashType)
                .where(eqTrashName(dto.getTrashName()), qTrashType.delYn.eq("N"))
                .offset(dto.getPage() * dto.getSize())
                .limit(dto.getSize())
                .fetchResults();
        return result;
    }

    private BooleanExpression eqTrashName(String trashName) {
        if (StringUtils.isEmptyOrWhitespace(trashName)) {
            return null;
        }
        return qTrashType.trashName.contains(trashName);
    }



}
