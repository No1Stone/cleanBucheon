package com.bucheon.clean.town.db.repository.environmental;

import com.bucheon.clean.town.db.entity.environmental.Petition;
import com.bucheon.clean.town.db.repository.environmental.dsl.PetitionRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface PetitionRepository extends JpaRepository<Petition, String> , PetitionRepositoryDsl {

    List<Petition> findByPetiNoIn(List<String> peNo);

    //////안전신문고 오늘 등록 수
    @Query(
            value = "SELECT " +
                    "   COUNT(a.peti_no) " +
                    "FROM " +
                    "   petition a" +
                    "   left join receipt b ON a.peti_no = b.peti_no " +
                    "where " +
                    "   a.reg_date_time between :sdate2 and :edate2" +
                    "   AND  b.busi_bran_code in ('S1006416', 'S1006417', 'S1006418', 'S1006419', 'S1006420', 'S1006422')"
            , nativeQuery = true
    )
    int countSinmunToday(@Param("sdate2") String sdate2, @Param("edate2") String edate2);

    //안전신문고 기간별 미처리 건수
    @Query(
            value = "SELECT " +
                    "   COUNT(a.peti_no) " +
                    "FROM " +
                    "petition a " +
                    "left join receipt b ON a.peti_no = b.peti_no " +
                    "LEFT OUTER JOIN process c on a.peti_no = c.peti_no " +
                    "where " +
                    "a.reg_date_time between :sdate2 AND :edate2 " +
                    "AND  b.busi_bran_code in ('S1006416', 'S1006417', 'S1006418', 'S1006419', 'S1006420', 'S1006422') " +
                    "AND c.status_code = '620020';"
            , nativeQuery = true
    )
    int countSinmunP(@Param("sdate2") String sdate2, @Param("edate2") String edate2);
    //안전신문고 기간별 미처리 건수
    @Query(
            value = "SELECT " +
                    "   COUNT(a.peti_no) " +
                    "FROM " +
                    "petition a " +
                    "left join receipt b ON a.peti_no = b.peti_no " +
                    "LEFT OUTER JOIN process c on a.peti_no = c.peti_no " +
                    "where " +
                    "1=1 " +
                    "AND  b.busi_bran_code in ('S1006416', 'S1006417', 'S1006418', 'S1006419', 'S1006420', 'S1006422') " +
                    "AND c.status_code = '620020';"
            , nativeQuery = true
    )
    int countSinmunP2();


    //안전신문고 기간별 처리 건수
    @Query(
            value = "SELECT " +
                    "   COUNT(a.peti_no) " +
                    "FROM " +
                    "petition a " +
                    "left join receipt b ON a.peti_no = b.peti_no " +
                    "LEFT OUTER JOIN process c on a.peti_no = c.peti_no " +
                    "where " +
                    "a.reg_date_time between :sdate2 AND :edate2 " +
                    "AND  b.busi_bran_code in ('S1006416', 'S1006417', 'S1006418', 'S1006419', 'S1006420', 'S1006422') " +
                    "AND c.status_code = '620090';"
            , nativeQuery = true
    )
    int countSinmunC(@Param("sdate2") String sdate2, @Param("edate2") String edate2);
}
