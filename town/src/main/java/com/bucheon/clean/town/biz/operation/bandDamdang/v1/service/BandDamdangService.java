package com.bucheon.clean.town.biz.operation.bandDamdang.v1.service;

import com.bucheon.clean.town.biz.operation.bandDamdang.v1.model.BandDamdangReq;
import com.bucheon.clean.town.db.entity.etc.DongBandDamdang;
import com.bucheon.clean.town.db.repository.etc.DongBandDamdangRepository;
import com.bucheon.clean.town.db.repository.operation.AdminRepository;
import com.bucheon.clean.town.db.repository.operation.MenuRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BandDamdangService {

    private final Logger logger = LoggerFactory.getLogger(BandDamdangService.class);
    private final AdminRepository adminRepository;
    private final MenuRepository menuRepository;
    private final DongBandDamdangRepository dongBandDamdangRepository;
    private final ModelMapper modelMapper;

    @Transactional(readOnly = true)
    public Object BandDamdangSelectListService() {
        var result = dongBandDamdangRepository.findAll();
        return result;
    }

    @Transactional(readOnly = true)
    public Optional<DongBandDamdang> BandDamdangSelectOneService(String dong) {
        return dongBandDamdangRepository.findByDong(dong);
    }

    @Transactional
    public String BandDamdangUpdateAndSave(BandDamdangReq dto) {

        int count = 0;
        int up = 0;
        if (dto.getDong() == null) {
            //아무것도 안함
        } else {

            if (dto.getDamdang() != null) {
                up = dongBandDamdangRepository.updateDamdang(dto.getDamdang(), dto.getDong());
                up = dongBandDamdangRepository.updateDamdangPhone(dto.getDamdangPhone(), dto.getDong());
                count += up;
            }

        }
        if (count > 0) {
            return "success";
        } else {
            return "fail";
        }
    }


}
