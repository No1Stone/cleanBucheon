package com.bucheon.clean.town.db.repository.chatbot.dsl;

import com.bucheon.clean.town.biz.chatbot.answer.v1.model.SimpleImageVO;
import com.bucheon.clean.town.biz.chatbot.answer.v1.model.SimpleTextVO;
import org.springframework.transaction.annotation.Transactional;

public interface SkllImageRepositoryDsl {

    @Transactional(readOnly = true)
    SimpleImageVO findBySkllSiCodeAndRspnsOrdr(String code, int reqSeq);
}
