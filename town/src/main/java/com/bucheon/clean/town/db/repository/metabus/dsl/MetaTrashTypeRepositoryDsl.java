package com.bucheon.clean.town.db.repository.metabus.dsl;


import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.MetaTrashTypeReq;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.MetaTrashTypeUserRes;
import com.bucheon.clean.town.biz.metabus.metaUser.v1.model.MetaTrashTypeVO;
import com.bucheon.clean.town.biz.operation.point.v1.model.TrashTypeReq;
import com.bucheon.clean.town.db.entity.metabus.MetaTrashType;
import com.bucheon.clean.town.db.entity.operation.TrashType;
import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MetaTrashTypeRepositoryDsl {

    @Transactional(readOnly = true)
    List<MetaTrashTypeUserRes> selectListMetaUser(String userId);

    @Transactional(readOnly = true)
    List<MetaTrashType> MetaTrashTypeSelectList(MetaTrashTypeReq dto);
    @Transactional(readOnly = true)
    QueryResults<MetaTrashType> MetaTrashTypeSelectListPaging(MetaTrashTypeReq dto);
}
