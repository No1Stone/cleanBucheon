package com.bucheon.clean.town.biz.operation.mileage.v1.model;

import com.bucheon.clean.town.db.entity.operation.UserMileageHistory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserMileageHistoryVO {

	/* 마일리지 로그 SEQ */
	private Long mileageSeq;

	/* ID */
	private String userId;

	/* authKey */
	private String authKey;

	/* ID */
	private String userName;
	
	/* 포인트 등록 tb_report seq */
	private Long reportSeq;

	/* tb_report content 내용 */
	private String reportContent;
	
	/* 포인트 점수 */
	private int mileage;

	/* 포인트 타입 */
	private String mileageType;

	/* 포인트 적립이유 */
	private String reason;

	/* 접수일자 */
	private LocalDateTime complaintsDt;

	/* 포인트 등록 승인한 사람관리자 */
	private Long adminSeq;

	/* 포인트 등록 승인한 사람관리자 */
	private String adminName;

	/* 등록일 */
	private LocalDateTime sendDate;
	
	/* 검색  */
	private String searchSelect;

	/* 검색  */
	private String searchString;

	/* 기간검색  */
	private String searchSdate;
	private String searchEdate;

	/* 페이지사이즈 */
	private int size;

	/* 페이지 */
	private int page;

	public UserMileageHistory ofUserMileageHistory(){
		UserMileageHistory u = new UserMileageHistory();

		if (this.mileageSeq != null) {
			u.setMileageSeq(this.mileageSeq);
		}
		u.setUserId(this.userId);
		if (this.reportSeq != null) {
			u.setReportSeq(this.reportSeq);
		} else {
			u.setReportSeq(0L);
		}
		u.setMileage(this.mileage);
		u.setMileageType(this.mileageType);
		u.setReason(this.reason);
		u.setComplaintsDt(this.complaintsDt);
		u.setAdminSeq(this.adminSeq);
		u.setSendDate(this.sendDate);

		return u;
	}
	
}
