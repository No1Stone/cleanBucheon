package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 스킬관리  > 스킬매니저 PVO
 * @author user
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SkillManagePVO {
	
	/* 검색 스킬명 */
	private String searchSkillName;
	
	/* 검색 스킬 코드 */
	private String searchSkillCode;
	
	/* 검색 조건 */
	private String searchType;
	
	/* 스킬명 */
	private String skillName;
	
	/* 스킬 코드 */
	private String skillCode;
	
	/* 응답타입 */
	private String resType;
	
	/* 응답타입 */
	private String resType1;
	
	/* 응답타입 */
	private String resType2;
	
	/* 응답타입 */
	private String resType3;

	/* 등록 날짜 */
	private String regDate;

	/* 수정 날짜 */
	private String upDate;

	/* 답변 카테고리 */
	private String skillcategory;

	/* 답변 카테고리 */
	private String skillUrl;
	
	/* 답변 레벨 */
	private String skillLevel;
	private String skillLevel1;
	private String skillLevel2;
	private String skillLevel3;
	private String skillLevel4;

	/* updateSkillSTExcel */
	private String text;
	
	/* updateSkillBCExcel */
	private String title;
	private String thumbnail;
	private String profile;
	private String social;
	private String button;
	private String description;
	private String bcSeq;

	
	/* updateSkillQRExcel */
	private String label;
	private String action;
	private String messagetext;
	private String blockid;
	private String qrSeq;
	
	/* updateSkillSIExcel */
	private String imageurl;
	private String alttext;
	private String resSeq;
	
	/* updateSkillCCExcel */
	private String header;
	private String items;	
	
	/* updateSkillLCExcel */
	private String price;
	private String discount;
	private String currency;
	private String discountrate;
	private String discountedprice;
	private String ccSeq;
	
	/* 서브 시스템 타입*/
	private String type;

	/* 슈퍼 관리자 여부*/
	private String sdrAt;
}
