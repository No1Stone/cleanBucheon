package com.bucheon.clean.town.db.repository.operation.dsl;

import com.bucheon.clean.town.biz.operation.admin.v1.model.AdminReq;
import com.bucheon.clean.town.db.entity.operation.Admin;
import com.bucheon.clean.town.db.entity.operation.QAdmin;
import com.bucheon.clean.town.db.entity.operation.QMenu;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

@Aspect
@RequiredArgsConstructor
public class AdminRepositoryDslImpl implements AdminRepositoryDsl{

    private final JPAQueryFactory queryFactory;
    private QAdmin qAdmin = QAdmin.admin;


    @Override
    public List<Admin> AdminSelectList(AdminReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        List<Admin> result = queryFactory.selectFrom(qAdmin)
                .where(eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        qAdmin.delYn.eq("N"))
                .orderBy(qAdmin.adminSeq.desc())
                .offset(dto.getPage()*dto.getSize())
                .limit(dto.getSize())
                .fetch();
        return result;
    }

    @Override
    public QueryResults<Admin> AdminSelectListPaging(AdminReq dto) {
        if(dto.getSize() == 0){
            dto.setSize(10);
        }
        QueryResults<Admin> result = queryFactory.selectFrom(qAdmin)
                .where(eqSearchsearchSelect(dto.getSearchSelect(), dto.getSearchString()),
                        qAdmin.delYn.eq("N"))
                .orderBy(qAdmin.adminSeq.desc())
                .offset(dto.getPage()*dto.getSize())
                .limit(dto.getSize())
                .fetchResults();
        return result;

    }

    //전체 검색 종류 확인
    private BooleanExpression eqSearchsearchSelect(String searchSelect, String searchString) {

        try {

            if (searchSelect.equals("searchName")) {
                return eqAdminName(searchString);
            } else if (searchSelect.equals("searchId")) {
                return eqAdminId(searchString);
            } else if (searchSelect.equals("searchArea")) {
                return eqAreaName(searchString);
            } else return null;
        } catch (Exception e) {
            return null;
        }
    }

    private BooleanExpression eqAdminId(String id) {
        if(StringUtils.isEmptyOrWhitespace(id)){
            return null;
        }
        return qAdmin.adminId.contains(id);
    }

    private BooleanExpression eqAdminName(String name) {
        if(StringUtils.isEmptyOrWhitespace(name)){
        return null;
        }
        return qAdmin.adminName.contains(name);
    }

    private BooleanExpression eqAreaName(String name) {
        if(StringUtils.isEmptyOrWhitespace(name)){
            return null;
        }
        return qAdmin.administrativeDivision.eq(name);
    }

}
