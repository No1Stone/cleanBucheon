package com.bucheon.clean.town.biz.chatbot.answer.v1.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class Link {
	String mobile;
	String ios;
	String android;
	String pc;
	String mac;
	String win;
	String web;

}
