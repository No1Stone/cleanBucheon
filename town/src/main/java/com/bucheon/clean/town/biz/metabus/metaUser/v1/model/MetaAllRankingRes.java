package com.bucheon.clean.town.biz.metabus.metaUser.v1.model;

import java.time.LocalDateTime;

public interface MetaAllRankingRes {

    int getRankNum();
    String getUserId();
    String getNickname();
    LocalDateTime getLastConnectAt();
    int getSumPoint();

}
