package com.bucheon.clean.town.biz.metabus.metaUser.v1.model;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetaTrashTypeUserRes {

    private String userId;
    private String nickName;
    private Long trashSeq;
    private String trashName;
    private String trashAct;
    private int trashPoint;
    private Long cleanCnt;
    private int point;

}
