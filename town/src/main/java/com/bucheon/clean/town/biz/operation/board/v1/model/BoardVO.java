package com.bucheon.clean.town.biz.operation.board.v1.model;

import com.bucheon.clean.town.db.entity.operation.Board;

import java.time.LocalDateTime;

public class BoardVO {

    private Long boardSeq;
    private String boardType;
    private String boardTitle;
    private String boardContent;
    private String useYn;
    private String delYn;
    private Long regSeq;
    private LocalDateTime regDt;
    private Long modSeq;
    private LocalDateTime modDt;

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public Long getBoardSeq() {
        return boardSeq;
    }

    public void setBoardSeq(Long boardSeq) {
        this.boardSeq = boardSeq;
    }

    public String getBoardType() {
        return boardType;
    }

    public void setBoardType(String boardType) {
        this.boardType = boardType;
    }

    public String getBoardTitle() {
        return boardTitle;
    }

    public void setBoardTitle(String boardTitle) {
        this.boardTitle = boardTitle;
    }

    public String getBoardContent() {
        return boardContent;
    }

    public void setBoardContent(String boardContent) {
        this.boardContent = boardContent;
    }

    public Long getRegSeq() {
        return regSeq;
    }

    public void setRegSeq(Long regSeq) {
        this.regSeq = regSeq;
    }

    public LocalDateTime getRegDt() {
        return regDt;
    }

    public void setRegDt(LocalDateTime regDt) {
        this.regDt = regDt;
    }

    public Long getModSeq() {
        return modSeq;
    }

    public void setModSeq(Long modSeq) {
        this.modSeq = modSeq;
    }

    public LocalDateTime getModDt() {
        return modDt;
    }

    public void setModDt(LocalDateTime modDt) {
        this.modDt = modDt;
    }

    public Board ofBoard(){
        Board m = new Board();

        if (this.boardSeq!=null) {
            m.setBoardSeq(this.boardSeq);
        }

        m.setBoardType(this.boardType);
        m.setBoardTitle(this.boardTitle);
        m.setBoardContent(this.boardContent);
        m.setUseYn(this.useYn);
        m.setDelYn(this.delYn);
        m.setRegSeq(this.regSeq);
        m.setRegDt(LocalDateTime.now());
        m.setModSeq(this.modSeq);
        m.setModDt(LocalDateTime.now());
        return m;
    }

}
