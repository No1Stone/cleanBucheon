package com.bucheon.clean.town.db.entity.etc;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "tb_connect_sum")
@Getter
@Setter
@NoArgsConstructor
public class ConnectSum {

    @Id
    @Column(name = "connect_date", nullable = true)
    private LocalDate connectDate;
    @Column(name = "clean_app", nullable = true)
    private int cleanApp;
    @Column(name = "metabus", nullable = true)
    private int metabus;
    @Column(name = "chatbot", nullable = true)
    private int chatbot;
    @Column(name = "band", nullable = true)
    private int band;
    @Column(name = "self", nullable = true)
    private int self;


    @Builder
    ConnectSum(
            LocalDate connectDate,
            int cleanApp,
            int metabus,
            int chatbot,
            int band,
            int self
            ) {
        this.connectDate = connectDate;
        this.cleanApp = cleanApp;
        this.metabus = metabus;
        this.chatbot = chatbot;
        this.band = band;
        this.self = self;
    }

}
