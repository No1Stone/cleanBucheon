package com.bucheon.clean.town.biz.operation.board.v1.model;

import com.bucheon.clean.town.db.entity.metabus.MetaBoardAtt;
import com.bucheon.clean.town.db.entity.operation.BoardAtt;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BoardAttVO {

    private Long boardSeq;
    private String boardAtt;
    private String boardAttOri;
    private String boardAttPath;
    private String boardAttType;
    private Long boardAttSize;

    public BoardAtt ofBoardAtt(){
        BoardAtt m = new BoardAtt();
        m.setBoardSeq(this.boardSeq);
        m.setBoardAtt(this.boardAtt);
        m.setBoardAttOri(this.boardAttOri);
        m.setBoardAttPath(this.boardAttPath);
        m.setBoardAttType(this.boardAttType);
        m.setBoardAttSize(this.boardAttSize);
        return m;
    }

}
